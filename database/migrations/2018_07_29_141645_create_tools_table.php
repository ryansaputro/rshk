<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('vendor_tools', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_detail');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
        schema::create('vendor_tools_detail', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_tool');
            $table->string('tool_name');
            $table->integer('qty');
            $table->integer('capacity');
            $table->string('type');
            $table->string('condition');
            $table->date('additional_maker');
            $table->string('location');
            $table->string('evidance');
            $table->longtext('description');
            $table->timestamps();
        });
        schema::create('vendor_experiences', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_detail');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
        schema::create('vendor_experiences_detail', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_experience');
            $table->string('job_order');
            $table->string('location');
            $table->integer('user');
            $table->string('address');
            $table->date('contract_date');
            $table->date('contract_finish');
            $table->integer('contract_value');
            $table->string('type');
            $table->string('data_source');
            $table->timestamps();
        });
        schema::create('vendor_tax', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_detail');
            $table->string('npwp');
            $table->string('npwp_scan');
            $table->string('status');
            $table->timestamps();
        });
        schema::create('vendor_tax_detail', function (blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendor_tax_detail');
            $table->string('evidence_tax');
            $table->string('evidence_body_tax');
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_tools');
        Schema::dropIfExists('vendor_tools_detail');
        Schema::dropIfExists('vendor_experiences');
        Schema::dropIfExists('vendor_experiences_detail');
        Schema::dropIfExists('vendor_tax');
        Schema::dropIfExists('vendor_tax_detail');
    }
}
