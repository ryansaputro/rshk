<?php

use Illuminate\Database\Seeder;
use \App\Model\Category;

class CategoryOnlineShopPerangkatKomputerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $mainCategory = array(
        '4.000.000.000' => 'Computer',
        '4.000.000.001' => 'Perangkat Lunak',
        '4.000.000.002' => 'Alat Perekam',
        '4.000.000.003' => 'Alat Komunikasi',
        '4.000.000.004' => 'Perlengkapan Kantor',
        '4.000.000.005' => 'Computer Supplies',
        '4.000.000.006' => 'Server & Storage',
        '4.000.000.007' => 'Networking',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000000000';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.000.000' => 'Notebook',
        '4.000.001' => 'Desktop & Workstation',
        '4.000.002' => 'Monitor',
        '4.000.003' => 'Aksesoris',
        '4.000.004' => 'Modem',
        '4.000.005' => 'Webcam',
        '4.000.006' => 'Keyboard',
        '4.000.007' => 'Mouse',
        '4.000.008' => 'Printer',
        '4.000.009' => 'Scanner',
        '4.000.010' => 'Drawing Device',
        '4.000.011' => 'Storage',
        '4.000.012' => 'Stabilizer',
        '4.000.013' => 'UPS & Battery',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000000';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.001.000' => 'Software Operation System Licensing',
        '4.001.001' => 'Software Office Productivity Licensing',
        '4.001.002' => 'Software Developer Licensing',
        '4.001.003' => 'Software Security & Antivirus Licensing',
        '4.001.004' => 'Software Data Analysis Licensing',
        '4.001.005' => 'Software Utility Licensing',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000001';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.002.000' => 'Camcorder',
        '4.002.001' => 'Kamera Digital',
        '4.002.002' => 'Tripod',
        '4.002.003' => 'Lensa Camera',
        '4.002.004' => 'Camera Battery & Power Supply',
        '4.002.005' => 'Flash Kamera',
        '4.002.006' => 'Drone',
        '4.002.007' => 'Voice Recorder',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000002';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.003.000' => 'Smartphone',
        '4.003.001' => 'Tablet',
        '4.003.002' => 'Handy Talky & HT Accessories',
        '4.003.003' => 'IP Phone',
        '4.003.004' => 'Headset Call Center',
        '4.003.005' => 'Perangkat Teleconference',
        '4.003.006' => 'Perangkat Telepony Analogy',
        '4.003.007' => 'VOIP Gateway',
        '4.003.008' => 'PABX',
        '4.003.009' => 'GPS',
        '4.003.010' => 'Fax Server',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000003';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.004.000' => 'Mesin Absensi',
        '4.004.001' => 'Mesin Fax',
        '4.004.002' => 'Mesin Fotocopy',
        '4.004.003' => 'Mesin Ketik',
        '4.004.004' => 'Mesin Jilid',
        '4.004.005' => 'Mesin Laminating',
        '4.004.006' => 'Mesin Penghitungan Uang',
        '4.004.007' => 'Mesin Pemotong Kertas',
        '4.004.008' => 'Mesin Pengahncur Kertas',
        '4.004.009' => 'Projector/Proyektor',
        '4.004.010' => 'Layar Proyektor/Screen',
        '4.004.011' => 'Papan Tulis Electric/Copyboard',
        '4.004.012' => 'Papan Tulis Interaktif & Aksesoris',
        '4.004.013' => 'Pendukung Presentasi',
        '4.004.014' => 'Office Appliance',
        '4.004.015' => 'Conference System Audio, Video & Aksesoris',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000004';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.005.000' => 'Pita Mesin',
        '4.005.001' => 'Pita Printer',
        '4.005.002' => 'Tinta Printer',
        '4.005.003' => 'Toner Printer',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000005';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.006.000' => 'Server',
        '4.006.001' => 'Server Data Storage',
        '4.006.002' => 'Rack System & Option',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000006';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '4.007.000' => 'Access Point & Option',
        '4.007.001' => 'Network Card',
        '4.007.002' => 'Network Security',
        '4.007.002' => 'Network Accessories',
        '4.007.002' => 'Network Connector & Converter',
        '4.007.002' => 'Switch & Option',
        '4.007.002' => 'Load Balancer',
        '4.007.002' => 'Router Device & Module',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '4000000007';
        $category->status = 'Y';

        $category->save();
      } // end foreach

    }
}
