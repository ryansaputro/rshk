<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = new \App\User;
              $user->name = "Ian";
              $user->email = "ian@pringstudio.com";
              $user->password = Hash::make("123456");
              $user->api_token = str_random(100);
              $user->save();
    }
}
