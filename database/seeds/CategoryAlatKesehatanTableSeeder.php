<?php

use Illuminate\Database\Seeder;
use \App\Model\Category;

class CategoryAlatKesehatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $mainCategory = array(
        '2.000.000.000' => 'Peralatan Kimia Klinik & Toksikologi Klinik',
        '2.000.000.001' => 'Gas Medik',
        '2.000.000.002' => 'Peralatan Hematologi & Patologi',
        '2.000.000.003' => 'Peralatan Imunologi & Mickrobiologi',
        '2.000.000.004' => 'Peralatan Anastesi',
        '2.000.000.005' => 'Peralatan Kardiologi',
        '2.000.000.006' => 'Peralatan Gigi',
        '2.000.000.007' => 'Peralatan Telinga, Hidung & Tenggorokan',
        '2.000.000.008' => 'Peralatan Gastroenterologi-urologi',
        '2.000.000.009' => 'Peralatan Rumah Sakit Umum & Perorangan',
        '2.000.000.010' => 'Peralatan Neurologi',
        '2.000.000.011' => 'Peralatan Obstetrik & Ginekologi',
        '2.000.000.012' => 'Peralatan Mata',
        '2.000.000.013' => 'Peralatan Ortopedi',
        '2.000.000.014' => 'Peralatan Kesehatan Fisik',
        '2.000.000.015' => 'Peralatan Radiologi',
        '2.000.000.016' => 'Peralatan Bedah Umum & Plastik',
        '2.000.000.017' => 'Tissue & Kapas',
        '2.000.000.018' => 'Sediaan Untuk Cuci',
        '2.000.000.019' => 'Pembersih',
        '2.000.000.020' => 'Alat Perawatan Bayi',
        '2.000.000.021' => 'Antiseptika & Desinfektan',
        '2.000.000.022' => 'Pewangi',
        '2.000.000.023' => 'Pestisida Rumah Tangga',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000000000';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.000.000' => 'Sistem Tes Kimia Klinik',
        '2.000.001' => 'Peralatan Laboratorium Klinik',
        '2.000.002' => 'Sistem Tes Toksikologi Klinik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000000';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.001.000' => 'Nitorus Oxide (N2O)',
        '2.001.001' => 'Oksigen Gas',
        '2.001.002' => 'Oksigen Cair',
        '2.001.003' => 'Oksigen PSG',
        '2.001.004' => 'Nitrogen Cair',
        '2.001.005' => 'Helium Cair',
        '2.001.006' => 'Helium Gas',
        '2.001.007' => 'Karbondioksida Gas Tabung (CO2)',
        '2.001.008' => 'Medical Compressed Air (N2MIX)',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000001';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.002.000' => 'Pewarna Biological',
        '2.002.001' => 'Produk Kultur Sel & Jaringan',
        '2.002.002' => 'Peralatan & Asesori Patologi',
        '2.002.003' => 'Pereaksi Penyedia Specimen',
        '2.002.004' => 'Peralatan Hematologi Otomatis & Semi Otomatis',
        '2.002.005' => 'Peralatan Hematologi Manual',
        '2.002.006' => 'Paket & Kit Hematologi',
        '2.002.007' => 'Pereaksi Hematologi',
        '2.002.008' => 'Produk Pembuatan Sediaan Darah',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000002';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.003.000' => 'Peralatan Diagnostika',
        '2.003.001' => 'Peralatan MIkrobiologi',
        '2.003.002' => 'Preaksi Serologi',
        '2.003.003' => 'Pelengkapan & Pereaksi Lab. Imunologi',
        '2.003.004' => 'Sistem Tes Imunologi',
        '2.003.005' => 'Sistem Tes Imunologi Antigen Tumor',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000003';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.004.000' => 'Peralatan Anastesi Diagnostik',
        '2.004.001' => 'Peralatan Anastesi Pemantauan',
        '2.004.002' => 'Peralatan Anastesi Terapetik',
        '2.004.003' => 'Peralatan Anastesi Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000004';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.005.000' => 'Peralatan Kardiologi Diagnostik',
        '2.005.001' => 'Peralatan Kardiologi Pemantauan',
        '2.005.002' => 'Peralatan Kardiologi Prostetik',
        '2.005.003' => 'Peralatan Kardiologi Bedah',
        '2.005.004' => 'Peralatan Kardiologi Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000005';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.006.000' => 'Peralatan Gigi Diagnostik',
        '2.006.001' => 'Peralatan Gigi Prostetik',
        '2.006.002' => 'Peralatan Gigi Bedah',
        '2.006.003' => 'Peralatan Gigi Terapetik',
        '2.006.004' => 'Peralatan Gigi Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000006';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.007.000' => 'Peralatan THT Diagnostik',
        '2.007.001' => 'Peralatan THT Prostetik',
        '2.007.002' => 'Peralatan THT Bedah',
        '2.007.003' => 'Peralatan THT Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000007';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.008.000' => 'Peralatan GU Diagnostik',
        '2.008.001' => 'Peralatan GU Pemantauan',
        '2.008.002' => 'Peralatan GU Prostetik',
        '2.008.003' => 'Peralatan GU Bedah',
        '2.008.004' => 'Peralatan GU Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000008';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.009.000' => 'Peralatan RSU & P Pemantauan',
        '2.009.001' => 'Peralatan RSU & P Terapetik',
        '2.009.002' => 'Peralatan RSU & P Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000009';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.010.000' => 'Peralatan Neurologi Diagnostik',
        '2.010.001' => 'Peralatan Neurologi Bedah',
        '2.010.002' => 'Peralatan Neurologi Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000010';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.011.000' => 'Peralatan OG Diagnostik',
        '2.011.001' => 'Peralatan OG Pemantauan',
        '2.011.002' => 'Peralatan OG Prostetik',
        '2.011.003' => 'Peralatan OG Bedah',
        '2.011.004' => 'Peralatan OG Terapetik',
        '2.011.005' => 'Peralatan Bantu Reproduksi',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000011';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.012.000' => 'Peralatan Mata Diagnostik',
        '2.012.001' => 'Peralatan Mata Prostetik',
        '2.012.002' => 'Peralatan Mata Bedah',
        '2.012.003' => 'Peralatan Mata Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000012';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.013.000' => 'Peralatan Ortopedi Diagnostik',
        '2.013.001' => 'Peralatan Ortopedi Prostetik',
        '2.013.002' => 'Peralatan Ortopedi Bedah',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000013';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.014.000' => 'Peralatan Kesehatan Fisik Diagnostik',
        '2.014.001' => 'Peralatan Kesehatan Fisik Prostetik',
        '2.014.002' => 'Peralatan Kesehatan Fisik Terapetik',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000014';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.015.000' => 'Peralatan Radiologi Diagnostik',
        '2.015.001' => 'Peralatan Radiologi Terapetik',
        '2.015.002' => 'Peralatan Radiologi Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000015';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.016.000' => 'Peralatan Bedah Diagnostik',
        '2.016.001' => 'Peralatan Bedah Prostetik',
        '2.016.002' => 'Peralatan Bedah Terapetik',
        '2.016.003' => 'Peralatan Bedah',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000016';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.017.000' => 'Kapas Kecantikan',
        '2.017.001' => 'Facial Tissue',
        '2.017.002' => 'Toilet Tissue',
        '2.017.003' => 'Tissue Basah',
        '2.017.004' => 'Tissue Makan',
        '2.017.005' => 'Cotton Bud',
        '2.017.006' => 'Paper Towel',
        '2.017.007' => 'Tissue & Kapas Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000017';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.018.000' => 'Sabun Cuci',
        '2.018.001' => 'Deterjen',
        '2.018.002' => 'Pelembut Cucian',
        '2.018.003' => 'Pemutih',
        '2.018.004' => 'Enzim Pencuci',
        '2.018.005' => 'Pewangi Pakaian',
        '2.018.006' => 'Sabun Cuci Tangan',
        '2.018.007' => 'Sediaan Mencuci Lainya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000018';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.019.000' => 'Pembersih Peralatan Dapur',
        '2.019.001' => 'Pembersih Kaca',
        '2.019.002' => 'Pembersih Lantai',
        '2.019.003' => 'Pembersih Porselen',
        '2.019.004' => 'Pembersih Kloset',
        '2.019.005' => 'Pembersih Mebel',
        '2.019.006' => 'Pembersih Karpet',
        '2.019.007' => 'Pembersih Mobil',
        '2.019.008' => 'Pembersih Sepatu',
        '2.019.009' => 'Penjernih Air',
        '2.019.010' => 'Pembersih Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000019';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.020.000' => 'Dot & Sejenisnya',
        '2.020.001' => 'Popok Bayi',
        '2.020.002' => 'Botol Susu',
        '2.020.003' => 'Alat Perawatan Bayi Lainya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000020';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.021.000' => 'Antiseptika',
        '2.021.001' => 'Disinfektan',
        '2.021.002' => 'Antiseptika & Disinfektan Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000021';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.022.000' => 'Pewangi Ruangan',
        '2.022.001' => 'Pewangi Telepon',
        '2.022.002' => 'Pewangi Mobil',
        '2.022.003' => 'Pewangi Kulkas',
        '2.022.004' => 'Pewangi Lainnya',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000022';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $mainCategory = array(
        '2.023.000' => 'Pengendali Serangga',
        '2.023.001' => 'Pencegah Serangga',
        '2.023.002' => 'Pengendali Kutu Rambut',
        '2.023.003' => 'Pengendali Kutu Binatang Peliharaan (Non Ternak)',
        '2.023.004' => 'Pengendali Tikus Rumah',
        '2.023.005' => 'Pestisida Rumah Tangga Lainnya ',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '2000000023';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      // $mainCategory = array(
      //   '2.024.000' => '',
      //   '2.024.001' => '',
      //   '2.024.002' => '',
      //   '2.024.003' => '',
      //   '2.024.004' => '',
      //   '2.024.005' => '',
      //   '2.024.006' => '',
      //   '2.024.007' => '',
      //   '2.024.008' => '',
      //   '2.024.009' => '',
      //   '2.024.010' => '',
      //   '2.024.011' => '',
      //   '2.024.012' => '',
      //   '2.024.013' => '',
      // ); // end array mainCategory
      //
      // foreach ($mainCategory as $key => $value) {
      //   $category = new Category;
      //
      //   $category->id = str_replace('.', '', $key) ;
      //   $category->name =  $value;
      //   $category->id_parent = '2000000019';
      //   $category->status = 'Y';
      //
      //   $category->save();
      // } // end foreach
    }
}
