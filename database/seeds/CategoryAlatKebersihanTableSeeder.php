<?php

use Illuminate\Database\Seeder;
use \App\Model\Category;

class CategoryAlatKebersihanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $mainCategory = array(
        '3.000.000.000' => 'Tempat Sampah',
        '3.000.000.001' => 'Kantong Sampah',
        '3.000.000.002' => 'Sepeda Kebersihan',
      ); // end array mainCategory


      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = '3000000000000';
        $category->status = 'Y';

        $category->save();
      } // end foreach

      $category = new Category;
      $category->id = '3000000';$category->name =  ' Tempat Sampah';$category->id_parent = '3000000000';$category->status = 'Y';
      $category->save();
      $category = new Category;
      $category->id = '3000001';$category->name =  'Kantong Sampah';$category->id_parent = '3000000001';$category->status = 'Y';
      $category->save();
      $category = new Category;
      $category->id = '3000002';$category->name =  'Sepeda Kebersihan';$category->id_parent = '3000000002';$category->status = 'Y';
      $category->save();
    }
}
