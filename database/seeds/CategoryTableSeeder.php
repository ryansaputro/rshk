<?php

use Illuminate\Database\Seeder;
use \App\Model\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Category::truncate(); // kosongkan data

      $mainCategory = array(
        '1.000.000.000.000' => 'Obat',
        '2.000.000.000.000' => 'Alat Kesehatan',
        '3.000.000.000.000' => 'Alat Kebersihan',
        '4.000.000.000.000' => 'Online Shop - Perangkat Komputer',
      ); // end array mainCategory

      foreach ($mainCategory as $key => $value) {
        $category = new Category;

        $category->id = str_replace('.', '', $key) ;
        $category->name =  $value;
        $category->id_parent = 0;
        $category->status = 'Y';

        $category->save();
      } // end foreach

    } // end function

} // end class
