<?php

use Illuminate\Database\Seeder;
use \App\Model\Category;

class CategoryObatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $category = new Category;
      $category->id = '1000000000';$category->name =  'Obat';$category->id_parent = '1000000000000';$category->status = 'Y';
      $category->save();
      $category = new Category;
      $category->id = '1000000';$category->name =  'Obat';$category->id_parent = '1000000000';$category->status = 'Y';
      $category->save();
    }
}
