@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Form Ajuan Perubahan Data...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  @if(Session::has('message'))
     <div class="alert alert-success alert-dismissible">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>{{Session::get('message')}}</strong>
     </div>
   @endif
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Ajuan Perubahan Data ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="row">
            <form class="" action="{{URL::to('vms/users/perubahan_data')}}" method="post">
              {{ csrf_field() }}
              <div style="padding-bottom:30px;" class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <label for="destination" class="col-md-4 control-label">Perubahan Data Yg Diajukan</label>
                <div class="col-md-8">
                  <select class="form-control input-xs" name="data" required>
                    <option value="identitas perusahaan">Identitas Perusahaan</option>
                    <option value="akta perusahaan">Akta Perusahaan</option>
                    <option value="lisensi perusahaan">Lisensi Perusahaan</option>
                    <option value="klasifikasi perusahaan">Klasifikasi Perusahaan</option>
                    <option value="pemilik perusahaan">Pemilik Perusahaan</option>
                    <option value="pengurus perusahaan">Pengurus Perusahaan</option>
                    <option value="tenaga ahli perusahaan">Tenaga Ahli Perusahaan</option>
                  </select>
                </div>
              </div>
              <div style="padding-bottom:30px;" class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <label for="destination" class="col-md-4 control-label">Alasan</label>
                <div class="col-md-8">
                  <textarea name="reason" class="form-control input-xs" required></textarea>
                </div>
              </div>
              <div style="padding-top:20px;" class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <div class="col-md-offset-11">
                  <button type="submit" class="btn btn-md btn-primary btn-block">Ajukan</button>
                </div>
              </div>
            </form>
          </div>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
});

var name = "";
var id = "";
$('#item').on('change', function(){
  id = $('#item :selected').attr('data-id');
  id_item = $(this).val();
  name =$('#item :selected').text();
  $.ajax({
      url: '{{URL::to('vms/users/detailItem')}}',
      method: 'POST',
      data: {"_token": "{{ csrf_token() }}", "id": id, "id_item" : id_item},
      success: function (a) {
        $('#qty').attr('value', a.qty);
        if(a.qty > 0){
          $('#qty').attr('max', a.qty);
          $('#qty').attr('min', 1);
        }else{
          $('#qty').attr('max', 0);
          $('#qty').attr('min', 0);

        }
      }
  });
});
var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}

</script>
@endsection
