@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Permintaan Barang...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Lacak Pesanan...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="row btn-group btn-group-devided">
              <form>
                <div class="col-sm-offset-1 col-sm-4">
                  <select class="form-control input-sm filter" name="filter">
                    <option value="semua">Semua</option>
                    <option value="usulan">No Usulan</option>
                    <option value="po">No PO</option>
                    <option value="tanggal">Tanggal</option>
                    <option value="status">Status</option>
                  </select>
                </div>
                <div class="col-sm-5 search" style="text-align:right;">
                  <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
                </div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
                </div>
              </form>
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <table class="table table-bordered table-striped" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>No Usulan</th>
                <th>No PO</th>
                <th>Pengusul</th>
                <th>Status Pesanan</th>
                <th style="text-align:left;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($request_order as $k =>$v)
                @php
                  $id_proposer = md5($v->id);
                  $time = strtotime($v->datetime);
                  $function =  Fungsi::MonthIndonesia();
                  $no_po = md5($v->no_po);
                  $month = md5(date('n', $time));
                  $year = md5(date('Y', $time));
                  $id = md5($v->id_order);
                @endphp
                <tr>
                  <td>{{$k+1}}</td>
                  <td>
                    {{$v->datetime}}
                  </td>
                  <td>{{$v->no_prop}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</td>
                  <td>{{$v->no_po}}/PO/{{date('m', $time)}}/{{date('Y', $time)}}</td>
                  <td>{{$v->username}}</td>
                  <td>
                    @php
                      $status = $v->status_order;
                    @endphp
                    <span style="display: none;">{{ $status }}</span>
                    @if ($status == 0)
                      <span class="label bg-grey" style="display: block;">Menunggu Persetujuan</span>
                    @elseif ($status == 1)
                      <span class="label bg-yellow" style="display: block;">Telah disetujui Supervisi</span>
                    @elseif ($status == 2)
                      <span class="label bg-blue" style="display: block;">Telah disetujui Manager</span>
                    @elseif ($status == 3)
                      <span class="label bg-green" style="display: block;">Telah disetujui Direktur</span>
                    @elseif ($status == 4)
                      <span class="label bg-purple" style="display: block;">Barang Dikirim</span>
                    @elseif ($status == 5)
                      <span class="label bg-grey" style="display: block;">Barang Terkirim</span>
                    @elseif ($status == 6)
                      <span class="label bg-yellow" style="display: block;">Barang diterima oleh Gudang</span>
                    @else
                      <span class="label bg-red" style="display: block;">Barang Masalah</span>
                    @endif
                  </td>
                  <td style="text-align:left;">
                    @if ($status >= 3)
                      <a target="_blank" href="{{URL::to('vms/users/po/download/'.$no_po.'/'.$month.'/'.$year)}}" class="btn btn-success btn-xs">PO</a>
                      @php
                        $AllQty = 0;
                        $AllQty2 = 0;
                        $totalReject = 0;
                        $totQty = 0;
                      @endphp
                          @if(in_array($v->id_order, $delivery))
                            @foreach ($deliveryData as $k => $val)
                              @if ($val->id_order == $v->id_order)
                                @php
                                  $AllQty += $val->qty;
                                @endphp
                              @endif
                            @endforeach
                            @foreach ($order_detail as $k => $val2)
                              @if ($val2->id_order == $v->id_order)
                                @php
                                  $AllQty2 += $val2->qty;
                                @endphp
                              @endif
                            @endforeach
                            @php
                              $totQty = $AllQty2 - $AllQty;
                            @endphp
                            @if (in_array($v->id_order, $rejectIdItem))
                              @php
                              foreach ($rejectIdItem as $key => $value) {
                                $totalReject += $key;
                              }
                              $ItemReject = $AllQty-$totalReject;
                              @endphp
                            @endif

                              @if ($totQty > 0)
                                <a href="{{URL::to('vms/users/delivery_order/'.$id)}}" class="btn btn-primary btn-xs">Kirim Barang (sisa {{$totQty}})</a>
                              @endif
                            @elseif($totQty == 0)
                              @foreach ($order_detail as $k => $val2)
                                @if ($val2->id_order == $v->id_order)
                                  @php
                                    $AllQty2 += $val2->qty;
                                  @endphp
                                @endif
                              @endforeach
                              <a href="{{URL::to('vms/users/delivery_order/'.$id)}}" class="btn btn-primary btn-xs">Kirim Barang</a>
                            @else
                              -
                            @endif

                        {{-- @endif --}}
                    @else
                      -
                    @endif

                    @if ($status == 6)
                      <a href="{{URL::to('vms/users/invoice/create/'.$id)}}" class="btn btn-success btn-xs">Invoice</a>
                    @elseif ($status == 7)
                      <a href="{{URL::to('vms/users/delivery_order/resend/'.$id)}}" class="btn btn-danger btn-xs">Kirim Ulang Barang</a>

                    @endif

                      {{-- <a target="_blank" href="{{URL::to('vms/users/proposer/download/'.$id_proposer)}}" class="btn btn-primary btn-sm">Usulan</a> --}}
                  </td>

                </tr>
              @endforeach
            </tbody>
          </table>
          <!-- BEGIN PAGINATOR -->
          <div class="row">
            <div class="col-md-5 col-sm-5">
              <div>menampilkan {{ ($request_order->currentPage() - 1) * $request_order->perPage() + 1 }} sampai {{ $request_order->count() * $request_order->currentPage() }} dari {{ $request_order->total() }} data</div>
            </div>
              <div class="col-md-7 col-sm-7 block-paginate" style="text-align: right;">{{ $request_order->links() }}</div>
          </div>
          <!-- END PAGINATOR -->
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
$('.filter').on('change', function(){
  var filter = $(this).val();
  if(filter == 'tanggal'){
    $('div.search').html('<input type="date" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }else{
    $('div.search').html('<input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }
});
</script>
@endsection
