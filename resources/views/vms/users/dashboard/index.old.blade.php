@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection
@php
$id = (isset($id)) ? $id : 'index';
@endphp
@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">{{ str_replace('_', ' ', $id) }}...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">{{ str_replace('_', ' ', $id) }}...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
              {{-- <button type="button" class="btn btn-info btn-rounded ButtonEdit" onclick="Edit(this)" data-id="{{$id}}"><i class="fa fa-pencil"></i> Ubah</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        @php
          $jumlahPemilik = count($pemilik);
          $jumlah_tools = count($tools);
          $jumlah_experiences = count($experiences);
          $jumlah_list_tax = count($list_tax);
          $npwpScan = "";
        @endphp
        <div class="portlet-body">
          @if ($id == 'penyedia')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                    $arraySisi['l'] = array(
                      'Nama Perusahaan',
                      'Bentuk Usaha',
                      'Alamat ADP',
                      'Alamat Terbaru',
                      'Kode POS',
                      'Propinsi',
                      'Kabupaten/Kota',
                      'Kantor Cabang',
                    );
                    $arraySisi['lx'] = array(
                      $vendor->vendor_name,
                      'PT',
                      $vendor->address,
                      $vendor->address,
                      $vendor->post_code,
                      (array_key_exists($vendor->province,$list_propinsi)) ? $list_propinsi[$vendor->province] : '-',
                      $list_kota[$list_propinsi[$vendor->province]][$vendor->city],
                      ($vendor->branch_office == '1') ? 'Ya' : 'Tidak',
                    );
                    $arraySisi['r'] = array(
                      'User ID',
                      'NPWP',
                      'No Pengukuhan PKP',
                      'Email',
                      'Telepon',
                      'Fax',
                      'Telepon Seluler',
                      'Website',
                      'Perubahan Tanggal',
                    );
                    $arraySisi['rx'] = array(
                      $vendor->username,
                      $vendor->npwp,
                      $vendor->pkp,
                      $vendor->email,
                      $vendor->telephone,
                      $vendor->fax,
                      $vendor->mobile_phone,
                      $vendor->website,
                      $vendor->updated_at,
                    );
                    @endphp
                    @foreach ($arraySisi['l'] as $key => $value)
                      <div class="row">
                        <div class="col-md-3" style="text-align: right;">
                          {{ $value }}
                        </div>
                        <div class="col-md-3 form_input_left" id="form_input_left_{{ $key }}">
                          {{ $arraySisi['lx'][$key] }}
                        </div>
                        @isset($arraySisi['r'][$key])
                          <div class="col-md-3" style="text-align: right;">
                            {{ $arraySisi['r'][$key] }}
                          </div>
                          <div class="col-md-3 form_input" id="{{ $key }}" data-form="{{ $key }}" data-id="{{ $arraySisi['rx'][$key] }}">
                            {{ $arraySisi['rx'][$key] }}
                          </div>
                        @endisset
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="display: none;" id="row_branch">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12" id="branch_office_additional">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row" style="margin-top: 50px; margin-bottom: 20px;">
                      <div class="col-md-6" style="vertical-align: middle;">
                        <button class="btn btn-secondary btn-rounded" type="button" name="button">
                          Orang yang dapat dihubungi
                        </button>
                      </div>
                      <div class="col-md-6" style="text-align: right;">
                        {{-- <button class="btn btn-success btn-rounded AddBtn" type="button" name="button" onclick="Add(this)">
                          <span class="fa fa-plus"></span> Tambah
                        </button> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                    foreach ($pengurus as $key => $value) {
                      $arrayData[$key] = array(
                        $value->name_name_vendor_administrators,
                        $value->address_name_vendor_administrators,
                        $value->ktp_name_vendor_administrators,
                        $value->position_name_vendor_administrators,
                      );
                    }
                    @endphp
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td>Nama</td>
                          <td>Alamat</td>
                          <td>No Ktp</td>
                          <td>Jabatan</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($pengurus) > 0)
                        @foreach ($arrayData as $key => $value)
                          <tr>
                            <td>{{ $value[0] }}</td>
                            <td>{{ $value[1] }}</td>
                            <td>{{ $value[2] }}</td>
                            <td>{{ $value[3] }}</td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="4" rowspan="" headers="" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'izin_usaha')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                    foreach($surat as $k => $v){
                      $arrayData[$k] = array(
                        $v->id_license,
                        $v->name_vendor_business_license,
                        $v->reference_number,
                        $v->valid_until,
                        $v->giver_agency,
                        $v->qualification,
                        $v->classification,
                      );
                    }
                    @endphp
                    <table class="table table-bordered" id="LetterTable">
                      <thead>
                        <tr>
                          <td>Izin Usaha</td>
                          <td>No Surat</td>
                          <td>Berlaku Sampai</td>
                          <td>Instansi Pemberi</td>
                          <td>Kualifikasi</td>
                          <td>Klasifikasi</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($arrayData) > 0)
                        @foreach ($arrayData as $key => $value)
                          <tr class="rawTable" id="tr_{{ $value[0] }}" data-id="{{ $value[0] }}">
                            <td class="dataTable" style="width: 15%;" data-id="{{$value[1]}}" id="{{str_replace(' ', '_', $value[1])}}_{{ $value[0] }}">{{ $value[1] }}</td>
                            <td class="dataTable" data-id="{{$value[2]}}" id="{{str_replace(' ', '_', $value[2])}}_{{ $value[0] }}">{{ $value[2] }}</td>
                            <td class="dataTable" data-id="{{$value[3]}}" id="{{str_replace(' ', '_', $value[3])}}_{{ $value[0] }}" bgcolor="{{($value[3] < date('Y-m-d')) ? 'red' : 'white'}}">{{ $value[3] }}</td>
                            <td class="dataTable" data-id="{{$value[4]}}" id="{{str_replace(' ', '_', $value[4])}}_{{ $value[0] }}">{{ $value[4] }}</td>
                            <td class="dataTable" data-id="{{$value[5]}}" id="{{str_replace(' ', '_', $value[5])}}_{{ $value[0] }}">{{ $value[5] }}</td>
                            <td class="dataTable" data-id="{{$value[6]}}" id="{{str_replace(' ', '_', $value[6])}}_{{ $value[0] }}">{{ $value[6] }}</td>
                            <td style="text-align: center;" class="td_{{$value[0]}}">
                              <button type="button" class="btn btn-info btn-rounded btn-xs" onclick="EditLetter(this)" data-id="{{ $value[0] }}" data-one="{{$value[1]}}" data-two="{{$value[2]}}" data-three="{{$value[3]}}" data-four="{{$value[4]}}" data-five="{{$value[5]}}" data-six="{{$value[6]}}" id="edit_{{$value[0]}}"><i class="fa fa-pencil"></i> Ubah</button>
                             <button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteLetter(this)" data-id="{{ $value[0] }}" id="delete_{{$value[0]}}"><i class="fa fa-trash"></i> Hapus</button></td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="7" rowspan="" headers="">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                    <center class="menu_additional">
                      <button type="button" class="btn btn-info btn-rounded AddBtn" onclick="Add(this)" data-id=""><i class="fa fa-plus"></i> Tambah</button>
                    </center>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'akta')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <table class="table table-bordered" id="AktaTable">
                      <thead>
                        <tr>
                          <td>Tanggal Surat</td>
                          <td>No Akta</td>
                          <td>Notaris</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody class="pendirian">
                        @if(count($akta) > 0)
                        @foreach($akta as $k =>$v)
                         @php
                         $titik_notary_public =  str_replace('.', ' ', $v->notary_public);
                        @endphp
                        <tr class="id_deed" data-id="{{$v->id}}">
                          <td width="20%" class="{{str_replace(' ', '_', $v->date)}}">{{$v->date}}</td>
                          <td class="{{str_replace(' ', '_', $v->deed_number)}}">{{$v->deed_number}}</td>
                          <td class="{{str_replace(' ', '_', $titik_notary_public)}}">{{$v->notary_public}}</td>
                          <td style="text-align: center;" class="tombolAksiAkta">
                            {{-- <button type="button" class="btn btn-primary btn-rounded btn-xs" onclick="DokumenAkta(this)" data-id="{{$v->id}}" id="delete" data-date="{{$v->date}}" data-deed_number="{{$v->deed_number}}" data-notary_public="{{$v->notary_public}}"><i class="fa fa-file"></i> Dokumen</button> --}}

                            <button type="button" class="btn btn-info btn-rounded btn-xs" onclick="EditItemAkta(this)" data-id="{{$v->id}}" id="delete" data-date="{{$v->date}}" data-deed_number="{{$v->deed_number}}" data-notary_public="{{$v->notary_public}}"><i class="fa fa-pencil"></i> Edit</button>

                            <button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteAkta(this)" data-id="{{$v->id}}" id="delete"><i class="fa fa-trash"></i> Hapus</button>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tbody class="pembaruan">
                        @if(count($aktaBaru) > 0)
                          @foreach($aktaBaru as $k =>$v)
                          @php
                            $titik_notary_public =  str_replace('.', ' ', $v->notary_public);
                          @endphp
                          <tr  style="background-color: #f6f9fa; text-align: center;"><td colspan="4">Akta Pembaruan</td></tr>
                          <tr>
                            <td class="{{str_replace(' ', '_', $v->date)}}">{{$v->date}}</td>
                            <td class="{{str_replace(' ', '_', $v->deed_number)}}">{{$v->deed_number}}</td>
                            <td class="{{str_replace(' ', '_', $titik_notary_public)}}">{{$v->notary_public}}</td>
                            <td style="text-align: center;" class="tombolAksiAktaPembaruan">
                              <button type="button" class="btn btn-info btn-rounded btn-xs" onclick="EditItemAktaPembaruan(this)" data-id="{{$v->id}}" id="delete" data-date="{{$v->date}}" data-deed_number="{{$v->deed_number}}" data-notary_public="{{$v->notary_public}}"><i class="fa fa-pencil"></i> Edit</button>

                              <button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteItemAktaPembaruan(this)" data-id="{{$v->id}}" id="delete"><i class="fa fa-trash"></i> Hapus</button>
                            </td>
                          </tr>
                          @endforeach
                        @else
                          @if(count($akta) > 0)
                          <tr  style="background-color: #f6f9fa; text-align: center;" class="menu_additional_akta_pembaruan"><td colspan="4">Akta Pembaruan</td></tr>
                          <td colspan="4" rowspan="" headers="" style="text-align: center;"><button type="button" class="btn btn-success btn-rounded ButtonAdd" data-id="{{$id}}" onclick="AddAktaPembaruan(this)"><i class="fa fa-plus"></i> Tambah Pembaruan</button></td>
                          @endif
                        @endif
                      </tbody>
                    </table>
                    <center class="menu_additional_akta">
                    </center>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="display: none;" id="row_doc_akta">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12" id="doc_akta">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'pemilik')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <table class="table table-bordered" id="pemilik">
                      <thead>
                        <tr>
                          <td>Nama</td>
                          <td>KTP/No Paspor</td>
                          <td>Alamat</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if($jumlahPemilik > 0)
                        @php
                        foreach($pemilik as $k => $v){
                          $arrayData[$k] = array(
                            $v->name_vendor_owner,
                            $v->ktp_vendor_owner,
                            $v->address_vendor_owner,
                            $v->id,
                          );
                        }
                        @endphp
                        @foreach ($arrayData as $key => $value)
                          <tr>
                            <td class="nama_{{ $value[3] }}">{{ $value[0] }}</td>
                            <td class="ktp_{{ $value[3] }}">{{ $value[1] }}</td>
                            <td class="alamat_{{ $value[3] }}">{{ $value[2] }}</td>
                            <td style="text-align: center;" class="tombolAksiPemilik">

                              <button type="button" class="btn btn-info btn-rounded btn-xs" onclick="EditItemPemilik(this)" data-id="{{$value[3]}}" data-ktp="{{$value[1]}}" data-alamat="{{$value[2]}}" data-nama="{{$value[0]}}" id="editPemilik"><i class="fa fa-pencil"></i> Edit</button>

                              <button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeletePemilik(this)" data-id="{{$value[3]}}" id="delete"><i class="fa fa-trash"></i> Hapus</button>
                            </td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="5" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'pengurus')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                    foreach($pengurus as $k => $v){
                      $arrayData[$k] = array(
                        $v->name_name_vendor_administrators,
                        $v->ktp_name_vendor_administrators,
                        $v->address_name_vendor_administrators,
                        $v->position_name_vendor_administrators,
                        $v->id,
                      );
                    }
                    @endphp
                    <table class="table table-bordered" id="pengurus">
                      <thead>
                        <tr>
                          <td>Nama</td>
                          <td>KTP/No Paspor</td>
                          <td>Alamat</td>
                          <td>Jabatan</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($pengurus) > 0)
                        @foreach ($arrayData as $key => $value)
                          <tr class="tr_pengurus {{ $value[4] }}" data-id="{{$value[4]}}">
                            <td class="nama_{{ $value[4] }}">{{ $value[0] }}</td>
                            <td class="ktp_{{ $value[4] }}">{{ $value[1] }}</td>
                            <td class="alamat_{{ $value[4] }}">{{ $value[2] }}</td>
                            <td class="jabatan_{{ $value[4] }}">{{ $value[3] }}</td>
                            <td style="text-align: center;" class="tombolAksiPengurus">
                              <button type="button" class="btn btn-info btn-rounded btn-xs editPengurus edit_{{$value[4]}}" onclick="EditItemPengurus(this)" data-id="{{$value[4]}}" data-ktp="{{$value[1]}}" data-alamat="{{$value[2]}}" data-nama="{{$value[0]}}" data-jabatan="{{ $value[3] }}" ><i class="fa fa-pencil"></i> Edit</button>

                              <button type="button" class="btn btn-danger hide btn-rounded btn-xs deletePengurus delete_{{$value[4]}}" onclick="DeletePengurus(this)" data-id="{{$value[4]}}"><i class="fa fa-trash"></i> Hapus</button>
                            </td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="5" rowspan="" headers="" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'tenaga_ahli')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                    foreach($staff as $k => $v){
                      $arrayData[$k] = array(
                        $v->name_vendor_expert_staff,
                        $v->birth_vendor_expert_staff,
                        $v->gender_vendor_expert_staff,
                        $v->education_vendor_expert_staff,
                        $v->experience_vendor_expert_staff,
                        $v->expertise_vendor_expert_staff,
                        $v->address_vendor_expert_staff,
                        $v->nationallty_vendor_expert_staff,
                        $v->email_vendor_expert_staff,
                        $v->id,
                      );
                    }
                    @endphp
                    <table class="table table-bordered" id="tenaga_ahli">
                      <thead>
                        <tr>
                          <td>Nama</td>
                          <td>Tanggal Lahir</td>
                          <td>Jenis Kelamin(L/P)</td>
                          <td>Pendidikan Terakhir</td>
                          <td>Pengalaman Kerja(tahun)</td>
                          <td>Profesi Keahlian</td>
                          <td>Alamat</td>
                          <td>Warga Negara</td>
                          <td>Email</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($staff) > 0)
                        @foreach ($arrayData as $key => $value)
                          <tr class="tenaga_ahli_{{$value[9]}} tenaga_ahli" data-id="{{$value[9]}}">
                            <td class="nama_{{ $value[9] }}">{{ $value[0] }}</td>
                            <td class="ttl_{{ $value[9] }}">{{ $value[1] }}</td>
                            <td class="jk_{{ $value[9] }}">{{ $value[2] }}</td>
                            <td class="pendidikan_{{ $value[9] }}">{{ $value[3] }}</td>
                            <td class="pengalaman_{{ $value[9] }}">{{ $value[4] }}</td>
                            <td class="profesi_{{ $value[9] }}">{{ $value[5] }}</td>
                            <td class="alamat_{{ $value[9] }}">{{ $value[6] }}</td>
                            <td class="warga_{{ $value[9] }}">{{ $value[7] }}</td>
                            <td class="email_{{ $value[9] }}">{{ $value[8] }}</td>
                            <td style="text-align: center;" class="tombolAksiTenagaAhli">

                              <button type="button" class="btn btn-info btn-rounded btn-xs editTenagaAhli edit_{{$value[9]}}" onclick="EditItemTenagaAhli(this)" data-id="{{$value[9]}}" data-nama="{{$value[0]}}" data-ttl="{{$value[1]}}" data-jk="{{$value[2]}}" data-pendidikan="{{ $value[3] }}" data-pengalaman="{{ $value[4] }}" data-profesi="{{ $value[5] }}" data-alamat="{{ $value[6] }}" data-warga="{{ $value[7] }}" data-email="{{ $value[8] }}"><i class="fa fa-pencil"></i> Edit</button>

                              <button type="button" class="btn btn-danger hide btn-rounded btn-xs deleteTenagaAhli delete_{{$value[9]}}" onclick="DeleteTenagaAhli(this)" data-id="{{$value[9]}}"><i class="fa fa-trash"></i> Hapus</button>

                            </td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="10" rowspan="" headers="" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'peralatan')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <form method="post" id="formPeralatan" action="{{ URL::to('vms/users/create_tools') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <table class="table table-bordered" id="tools">
                      <thead>
                        <tr>
                          <td>Nama Alat</td>
                          <td>Jumlah</td>
                          <td>Kapasitas</td>
                          <td>Mark/Type</td>
                          <td>Kondisi</td>
                          <td>Tambahan Pembuatan</td>
                          <td>Lokasi Sekarang</td>
                          <td>Bukti Kepemilikan</td>
                          <td>Keterangan</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if($jumlah_tools > 0)
                        @foreach ($tools as $key => $value)
                          <tr class="tools tr_{{$value->id_vendor_tools}}" data-id="{{$value->id_vendor_tools}}">
                            <td class="alat_{{$value->id_vendor_tools}}">{{ $value->tool_name }}</td>
                            <td class="qty_{{$value->id_vendor_tools}}">{{ $value->qty }}</td>
                            <td class="kapasitas_{{$value->id_vendor_tools}}">{{ $value->capacity }}</td>
                            <td class="tipe_{{$value->id_vendor_tools}}">{{ $value->type }}</td>
                            <td class="kondisi_{{$value->id_vendor_tools}}">{{ $value->condition }}</td>
                            <td class="tambahan_{{$value->id_vendor_tools}}">{{ $value->additional_maker }}</td>
                            <td class="lokasi_{{$value->id_vendor_tools}}">{{ $value->location }}</td>
                            <td class="bukti_{{$value->id_vendor_tools}}"><a target="_blank" href="../../{{'assets/document/'.$value->id_vendor_detail.'/evidence_tools/'.$value->evidance}}" title="{{ $value->evidance }}">{{ $value->evidance }}</a></td>
                            <td class="keterangan_{{$value->id_vendor_tools}}">{{ $value->description }}</td>
                            <td style="text-align: center;" class="tombolAksiTools">
                              <button
                                  type="button"
                                  class="btn btn-info btn-rounded btn-xs editTools edit_{{$value->id}}"
                                  onclick="EditItemTools(this)"
                                  data-id="{{ $value->id }}"
                                  data-alat="{{ $value->tool_name }}"
                                  data-qty="{{ $value->qty }}"
                                  data-kapasitas="{{ $value->capacity }}"
                                  data-tipe="{{ $value->type }}"
                                  data-kondisi="{{ $value->condition }}"
                                  data-tambahan="{{ $value->additional_maker }}"
                                  data-lokasi="{{ $value->location }}"
                                  data-bukti="{{$value->evidance}}"
                                  data-keterangan="{{ $value->description }}"
                                  ><i class="fa fa-pencil"></i> Edit</button>

                              <button
                                  type="button"
                                  class="btn btn-danger hide btn-rounded btn-xs deleteTools delete_{{$value->id}}"
                                  onclick="DeleteTools(this)"
                                  data-id="{{$value->id}}">
                                  <i class="fa fa-trash"></i> Hapus</button>
                            </td>
                          </tr>
                        @endforeach
                        @else
                        <tr>
                          <td colspan="10" rowspan="" headers="" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'pengalaman')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <form method="post" id="formPengalaman" action="{{ URL::to('vms/users/create_experiences') }}">
                    {{ csrf_field() }}
                    <table class="table table-bordered" id="experiences">
                      <thead>
                        <tr>
                          <td>Pekerjaan</td>
                          <td>Lokasi</td>
                          <td>Pengguna</td>
                          <td>Alamat</td>
                          <td>Tanggal Kontrak</td>
                          <td>Selesai Kontrak</td>
                          <td>Nilai Kontrak (Rp)</td>
                          <td>Jenis</td>
                          <td>Sumber Data</td>
                          <td>Perubahan Tanggal</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if($jumlah_experiences > 0)
                        @foreach($experiences as $k => $v)
                        <tr class="experiences tr_{{$v->id}}" data-id="{{$v->id}}">
                            <td class="job_order_{{$v->id}}">{{$v->job_order}}</td>
                            <td class="location_{{$v->id}}">{{$v->location}}</td>
                            <td class="user_{{$v->id}}">{{$v->user}}</td>
                            <td class="address_{{$v->id}}">{{$v->address}}</td>
                            <td class="contract_date_{{$v->id}}">{{$v->contract_date}}</td>
                            <td class="contract_finish_{{$v->id}}">{{$v->contract_finish}}</td>
                            <td class="contract_value_{{$v->id}}">{{$v->contract_value}}</td>
                            <td class="type_{{$v->id}}">{{$v->type}}</td>
                            <td class="data_source_{{$v->id}}">{{$v->data_source}}</td>
                            <td class="updated_at_{{$v->id}}">{{$v->updated_at}}</td>
                            <td style="text-align: center;" class="tombolAksiExperiences">
                              <button
                                  type="button"
                                  class="btn btn-info btn-rounded btn-xs editExperiences edit_{{$v->id}}"
                                  onclick="EditItemExperiences(this)"
                                  data-id="{{ $v->id }}"
                                  data-job_order="{{ $v->job_order }}"
                                  data-location="{{ $v->location }}"
                                  data-user="{{ $v->user }}"
                                  data-address="{{ $v->address }}"
                                  data-contract_date="{{ $v->contract_date }}"
                                  data-contract_finish="{{ $v->contract_finish }}"
                                  data-contract_value="{{ $v->contract_value }}"
                                  data-type="{{ $v->type }}"
                                  data-data_source="{{$v->data_source}}"
                                  data-updated_at="{{ $v->updated_at }}"
                                  ><i class="fa fa-pencil"></i> Edit</button>
                              <button
                                  type="button"
                                  class="btn btn-danger hide btn-rounded btn-xs deleteExperiences delete_{{$v->id}}"
                                  onclick="DeleteExperiences(this)"
                                  data-id="{{$v->id}}">
                                  <i class="fa fa-trash"></i> Hapus</button>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="11" rowspan="" headers="" style="text-align: center;">Tidak Ada Data</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'pajak')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <form method="post" id="formPengalaman" action="{{ URL::to('vms/users/create_tax') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <h3><b>NPWP : {{$vendor->npwp}}</b></h3>
                    <table class="table table-bordered" id="tax">
                      <thead>
                        <tr>
                          <td>Bukti NPWP</td>
                          <td>Bukti Pajak</td>
                          <td>Bukti Pajak Badan</td>
                          <td style="text-align: center;">Aksi</td>
                        </tr>
                      </thead>
                      <tbody>
                        @if($jumlah_list_tax > 0)
                        @foreach($list_tax as $k => $v)
                        @php
                          $npwpScan = $v->npwp_scan;
                        @endphp
                        <tr class="tax tr_{{$v->id}}" data-id="{{$v->id}}">
                          <td>
                              <a
                                href="../../{{'assets/document/'.$v->id_vendor_detail.'/npwp_scan/'.$v->npwp_scan}}"
                                target="_blank"
                                title="{{$v->npwp_scan}}" class="btn btn-rounded btn-info btn-xs">Detail</a></td>
                          <td><a
                                href="../../{{'assets/document/'.$v->id_vendor_detail.'/evidence_tax/'.$v->evidence_tax}}"
                                target="_blank"
                                title="{{$v->evidence_tax}}" class="btn btn-rounded btn-info btn-xs">Detail</a></td>
                          <td><a
                                href="../../{{'assets/document/'.$v->id_vendor_detail.'/evidence_body_tax/'.$v->evidence_body_tax}}"
                                target="_blank"
                                title="{{$v->evidence_body_tax}}" class="btn btn-rounded btn-info btn-xs">Detail</a></td>
                            <td style="text-align: center;" class="tombolAksiTax">
                              {{-- <button
                                  type="button"
                                  class="btn btn-info btn-rounded btn-xs editExperiences edit_{{$v->id}}"
                                  onclick="EditItemExperiences(this)"
                                  data-id="{{ $v->id }}"
                                  data-npwp_scan="{{ $v->npwp_scan }}"
                                  data-evidence_tax="{{ $v->evidence_tax }}"
                                  data-evidence_body_tax="{{ $v->evidence_body_tax }}"
                                  ><i class="fa fa-pencil"></i> Edit</button> --}}
                              <button
                                  type="button"
                                  class="btn btn-danger hide btn-rounded btn-xs deleteTax delete_{{$v->id}}"
                                  onclick="DeleteTax(this)"
                                  data-id="{{$v->id}}">
                                  <i class="fa fa-trash"></i> Hapus</button>
                            </td>
                        </tr>
                        @endforeach
                        @else
                          <tr>
                            <td colspan="5" style="text-align: center;">Tidak Ada Data</td>
                          </tr>
                        @endif
                      </tbody>
                    </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'kotak_masuk')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>Telp</th>
                          <th>Tanggal Daftar</th>
                          <th>Dokumen</th>
                          <th>Status</th>
                        </thead>
                        <tbody>
                          @php
                          $jumlah_inbox = count($vendor_detail_lulus);
                          @endphp
                          @if($jumlah_inbox > 0)
                          @foreach($vendor_detail_lulus as $k => $v)
                          <tr class="user_{{$v->id_user_vendor}}" id="vendor">
                            <td>{{$k+1}}</td>
                            <td>{{$v->vendor_name}}</td>
                            <td>{{$v->address}}</td>
                            <td>{{$v->telephone}}</td>
                            <td>{{$v->created_at}}</td>
                            <td><a href="#" title="" data-id="{{$v->id_user_vendor}}" onclick="ListDoc(this)" class="btn btn-info btn-rounded btn-xs">Detail</a></td>
                            <td class="td_status" data-id="{{$v->status_vendor}}" id="status_{{$v->id_vendor}}"><button type="button" class="btn btn-xs btn-{{($v->last_status == 1) ? 'success' : 'danger'}} btn-rounded" data-id="{{$id}}">{{($v->last_status == 1) ? 'Lulus VMS' : 'Gagal VMS'}}</button>
                            </td>
                          </tr>
                          @endforeach
                          @else
                          <tr>
                            <td colspan="7" rowspan="" headers="" style="text-align: center;">Tidak Ada  Data</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="display: none;" id="detail">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <h3>Dokumen</h3>
                    <div class="row">
                      <div class="col-md-12" id="detail_body_dok">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'preferensi')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    @php
                      $arraySisi['l'] = array(
                        'Lokasi',
                        'Jenis Pekerjaan',
                        'Klarifikasi Pengadaan',
                        'Penyelenggara',
                        'HPS',
                        'Tanggal Dibuat',
                        'Perubahan Tanggal',
                      );
                      $arraySisi['r'] = array(
                        'Nasional',
                        '
                        <ul style="padding-left: 19px; margin: 0;">
                          <li>Barang</li>
                          <li>Jasa Konsultasi (Badan Usaha)</li>
                          <li>Kontruksi</li>
                          <li>Jasa Lainnya</li>
                        </ul>
                        ',
                        '-',
                        'Seluruh Penyelenggara',
                        'Bebas',
                        '22-02-2018 09:14:55',
                        '22-02-2018 09:14:55',
                      );
                    @endphp
                    @foreach ($arraySisi['l'] as $key => $value)
                      <div class="row">
                        <div class="col-md-6" style="text-align: right;">
                          {{ $value }}
                        </div>
                        <div class="col-md-6">
                          {!! $arraySisi['r'][$key] !!}
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'dokumen')
            <div class="row">
               <div class="col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <form  method="post" id="" action="{{ URL::to('vms/users/document_upload') }}" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <select name="menu"class="form-control full" required="">
                              <option disabled selected="">-Tipe Dokumen-</option>
                              @foreach($menu as $k => $v)
                              <option value="{{$k}}">{{$v}}</option>
                              @endforeach
                           </select>
                           <br><br>
                           <input type="file" placeholder="File" accept="application/pdf" class="form-control full" name="file" id="file" oninput="" required>
                           <br><br>
                           <div style="text-align: right;">
                              <button type="submit" class="btn btn-info btn-rounded"><i class="fa fa-check"></i> Selesai</button>
                           </div>
                        </form>
                        <br><br>
                        <div class="table-responsive">
                           <table class="table table-bordered">
                              <thead>
                                 <th>No
                                 <th>Tipe Dokumen
                                 <th>File
                                 <th>Aksi
                              </thead>
                              <tbody>
                                 <?php
                                    // $tanggal = substr($v->file, 0,10);
                                    // $tahun = substr($v->file, 0,4);
                                    // $bulan = substr($v->file, 4,2);
                                    // $hari = substr($v->file, 6,2);
                                    // $tgl = $tahun."-".$bulan."-".$hari;
                                    ?>
                                 {{-- <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{(array_key_exists($v->menu, $menu)) ? $menu[$v->menu] : ''}}</td>
                                    <td><a target="_blank" href="../../{{(array_key_exists($v->menu, $menu)) ? 'assets/document/'.$vendor->id.'/'.$menu[$v->menu].'/'.$tgl.'/'.$v->file : ''}}" title="{{$v->file}}">{{$v->file}}</a></td>
                                    <td><a onclick="DeleteDoc(this)" data-id="{{$v->id}}" data-menu="{{$v->menu}}" class="btn btn-danger hide btn-rounded"><i class="fa fa-trash"></i> Hapus</button></td>
                                 </tr> --}}
                                 {{-- @endforeach
                                 @else --}}
                                 {{-- <tr style="text-align: center; background-color: #edeff0;">
                                    <td colspan="4">Tidak ada data</td>
                                 </tr> --}}
                                 {{-- @endif --}}
                              </tbody>
                           </table>
                           <div class="col-md-5 col-sm-5">
                              {{-- <div>menampilkan {{ ($doc->currentPage() - 1) * $doc->perPage() + 1 }} sampai {{ $doc->count() * $doc->currentPage() }} dari {{ $doc->total() }} data</div> --}}
                           </div>
                           {{-- <div class="col-md-7 col-sm-7 block-paginate">{{ $doc->render() }}</div> --}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          @else
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    Ongoing...
                  </div>
                </div>
              </div>
            </div>
          @endif
          <div class="modal"></div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>

  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="margin-top:150px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Surat Ajuan Perubahan Data</h4>
          </div>
          <div class="modal-body">
            <div class="ceklis_list">
            </div>
                {{-- <br />
                <button class="btn btn-primary col-xs-12" id="get-checked-data">Simpan ?</button>
                <br><br> --}}
                <div class="div_aksi">
                  <form class="form-horizontal" method="POST" action="{{ URL::to('vms/users/data/changes') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div id="inputData"></div>
                  <div id="display-json" style="text-align:center; padding-top:10px;"></div>
                  <div class="id_vendor">
                  </div>
                  </form>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
//######################################################PAJAK################################################
function AddBtnTax() {
  var npwp = "{{$vendor->npwp}}";
  var vendor = "{{$vendor->id}}";
  var scan_npwp = "{{$tax}}";
  if(scan_npwp > 0){
    var npwp_scan = "{{$npwpScan}}";
      var table =
                  '<tr class="child">'+
                  '<td><input required type="hidden" name="npwp" value="'+npwp+'" id="job_order" class="form-control" readonly><a href="../../assets/document/'+vendor+'/npwp_scan/'+npwp_scan+'" target="_blank" title="'+npwp_scan+'" class="btn btn-rounded btn-info btn-xs">Detail</a></td>'+
                  '<td><input required type="file" name="evidence_tax" id="evidence_tax" accept="application/pdf"></td>'+
                  '<td><input required type="file" name="evidence_body_tax" id="evidence_body_tax" accept="application/pdf"></td>'+
                  '<td><button type="submit" class="btn btn-success btn-rounded btn-sm"><i class="fa fa-check"></i> Selesai</button>'+
                  '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowTax()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
  }else{
      var table =
                  '<tr class="child">'+
                  '<td><input required type="hidden" name="npwp" value="'+npwp+'" id="job_order" class="form-control" readonly><input required type="file" name="npwp_scan" id="npwp_scan" accept="application/pdf"></td>'+
                  '<td><input required type="file" name="evidence_tax" id="evidence_tax" accept="application/pdf"></td>'+
                  '<td><input required type="file" name="evidence_body_tax" id="evidence_body_tax" accept="application/pdf"></td>'+
                  '<td><button type="submit" class="btn btn-success btn-rounded btn-sm"><i class="fa fa-check"></i> Selesai</button>'+
                  '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowTax()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';

  }
    var jumlah = {{$jumlah_list_tax}};
    (jumlah > 0) ? $('#tax tbody').append(table):$('#tax tbody').html(table)

    $('.AddBtnTax').css('display','none');
    $.each($('tr.tax'), function(x,y){
        // $('.editTax').css('display','none');
        $('.deleteTax').css('display','none');
    })
}

function deleteRowTax() {
    $('#tax tr:last').remove();
    $('.AddBtnTax').css('display','');
    $.each($('tr.tax'), function(x,y){
        // $('.editTax').css('display','');
        $('.deleteTax').css('display','');
    })
}

function DeleteTax(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_tax')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}
//######################################################PENGALAMAN################################################
function AddBtnExperiences() {
    var table =
                '<tr class="child">'+
                '<td><input required type="text" name="job_order" id="job_order" class="form-control"></td>'+
                '<td><input required type="text" name="location" id="location" class="form-control"></td>'+
                '<td><input required type="text" name="user" id="user" class="form-control"></td>'+
                '<td><input required type="text" name="address" id="address" class="form-control"></td>'+
                '<td><input required type="date" name="contract_date" id="contract_date" class="form-control"></td>'+
                '<td><input required type="date" name="contract_finish" id="contract_finish" class="form-control"></td>'+
                '<td><input required type="number" name="contract_value" id="contract_value" class="form-control"></td>'+
                '<td><input required type="text" name="type" id="type" class="form-control"></td>'+
                '<td><input required type="text" name="data_source" id="data_source" class="form-control"></td>'+
                '<td>-</td>'+
                '<td><button type="submit" class="btn btn-success btn-rounded btn-sm"><i class="fa fa-check"></i> Selesai</button>'+
                '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowExperiences()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
    var jumlah = {{$jumlah_experiences}};
    (jumlah > 0) ? $('#experiences tbody').append(table):$('#experiences tbody').html(table)

    $('.AddBtnExperiences').css('display','none');
    $.each($('tr.experiences'), function(x,y){
        $('.editExperiences').css('display','none');
        $('.deleteExperiences').css('display','none');
    })
}

function deleteRowExperiences() {
    $('#experiences tr:last').remove();
    $('.AddBtnExperiences').css('display','');
    $.each($('tr.experiences'), function(x,y){
        $('.editExperiences').css('display','');
        $('.deleteExperiences').css('display','');
    })
}

function EditItemExperiences(a) {
  $("#formPengalaman").attr("action","{{ URL::to('vms/users/update_experiences') }}")
  var id = $(a).attr('data-id');
  var job_order = $(a).attr('data-job_order');
  var location = $(a).attr('data-location');
  var user = $(a).attr('data-user');
  var address = $(a).attr('data-address');
  var contract_date = $(a).attr('data-contract_date');
  var contract_finish = $(a).attr('data-contract_finish');
  var contract_value = $(a).attr('data-contract_value');
  var type = $(a).attr('data-type');
  var data_source = $(a).attr('data-data_source');

  var input_job_order = '<input required type="text" name="job_order" id="job_order" value="'+job_order+'" class="form-control">';
  var input_location = '<input required type="text" name="location" id="location" value="'+location+'" class="form-control">';
  var input_user = '<input required type="text" name="user" id="user" value="'+user+'" class="form-control">';
  var input_address = '<input required type="text" name="address" id="address" value="'+address+'" class="form-control">';
  var input_contract_date = '<input required type="date" name="contract_date" id="contract_date" value="'+contract_date+'" class="form-control">';
  var input_contract_finish = '<input required type="date" name="contract_finish" id="contract_finish" value="'+contract_finish+'" class="form-control">';
  var input_contract_value = '<input required type="number" name="contract_value" id="contract_value" value="'+contract_value+'" class="form-control">';
  var input_type = '<input required type="text" name="type" id="type" value="'+type+'" class="form-control">';
  var input_data_source = '<input required type="text" name="data_source" id="data_source" value="'+data_source+'" class="form-control"><input required type="hidden" name="id" id="id" value="'+id+'" class="form-control">';

  $(".job_order_"+id).html(input_job_order);
  $(".location_"+id).html(input_location);
  $(".user_"+id).html(input_user);
  $(".address_"+id).html(input_address);
  $(".contract_date_"+id).html(input_contract_date);
  $(".contract_finish_"+id).html(input_contract_finish);
  $(".contract_value_"+id).html(input_contract_value);
  $(".type_"+id).html(input_type);
  $(".data_source_"+id).html(input_data_source);

  var tombol = '<button type="submit" class="btn btn-success btn-rounded btn-sm" onclick="update_experiences(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
  $("tr.tr_"+id+" .tombolAksiExperiences").html(tombol);
  $(".AddBtnExperiences").css('display','none')

    $.each($('tr.experiences'), function(x,y){
      if($(y).attr('data-id') == id){
        $('.edit_'+id).css('display','none');
      }else{
        $('.edit_'+$(y).attr('data-id')).css('display','none');
        $('.delete_'+$(y).attr('data-id')).css('display','none');
      }
    });
}

function DeleteExperiences(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_experiences')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}
//######################################################PERALATAN#################################################
function AddBtnTools() {
    var table =
                '<tr class="child">'+
                '<td><input required type="text" name="tool_name" id="tool_name" class="form-control"></td>'+
                '<td><input required type="number" name="qty" id="qty" class="form-control"></td>'+
                '<td><input required type="number" name="capacity" id="capacity" class="form-control"></td>'+
                '<td><input required type="text" name="type" id="type" class="form-control"></td>'+
                '<td><input required type="text" name="condition" id="condition" class="form-control"></td>'+
                '<td><input required type="date" name="additional_maker" id="additional_maker" class="form-control"></td>'+
                '<td><input required type="text" name="location" id="location" class="form-control"></td>'+
                '<td><input required type="file" name="file" id="evidance" accept="application/pdf"></td>'+
                '<td><input required type="text" name="description" id="description" class="form-control"></td>'+
                '<td><button type="submit" class="btn btn-success btn-rounded btn-sm"><i class="fa fa-check"></i> Selesai</button>'+
                '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowTools()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
    var jumlah = {{$jumlah_tools}};
    (jumlah > 0) ? $('#tools tbody').append(table):$('#tools tbody').html(table)

    $('.AddBtnTools').css('display','none');
    $.each($('tr.tools'), function(x,y){
        $('.editTools').css('display','none');
        $('.deleteTools').css('display','none');
    })
}

function deleteRowTools() {
    $('#tools tr:last').remove();
    $('.AddBtnTools').css('display','');
    $.each($('tr.tools'), function(x,y){
        $('.editTools').css('display','');
        $('.deleteTools').css('display','');
    })
}

function EditItemTools(a) {
  $("#formPeralatan").attr("action","{{ URL::to('vms/users/update_tools') }}")
  var id = $(a).attr('data-id');
  var alat = $(a).attr('data-alat');
  var qty = $(a).attr('data-qty');
  var kapasitas = $(a).attr('data-kapasitas');
  var tipe = $(a).attr('data-tipe');
  var kondisi = $(a).attr('data-kondisi');
  var tambahan = $(a).attr('data-tambahan');
  var lokasi = $(a).attr('data-lokasi');
  var bukti = $(a).attr('data-bukti');
  var keterangan = $(a).attr('data-keterangan');

  var input_alat = '<input required type="text" name="tool_name" id="tool_name" value="'+alat+'" class="form-control">';
  var input_qty = '<input required type="number" name="qty" id="qty" value="'+qty+'" class="form-control">';
  var input_kapasitas = '<input required type="number" name="capacity" id="capacity" value="'+kapasitas+'" class="form-control">';
  var input_tipe = '<input required type="text" name="type" id="type" value="'+tipe+'" class="form-control">';
  var input_kondisi = '<input required type="text" name="condition" id="condition" value="'+kondisi+'" class="form-control">';
  var input_tambahan = '<input required type="date" name="additional_maker" id="additional_maker" value="'+tambahan+'" class="form-control">';
  var input_lokasi = '<input required type="text" name="location" id="location" value="'+lokasi+'" class="form-control">';
  var input_bukti = '<input required type="file" name="file" accept="application/pdf" id="file" value="'+bukti+'" class="form-control">';
  var input_keterangan = '<input required type="text" name="description" id="description" value="'+keterangan+'" class="form-control"><input required type="hidden" name="id" id="id" value="'+id+'" class="form-control">';

  $(".alat_"+id).html(input_alat);
  $(".qty_"+id).html(input_qty);
  $(".kapasitas_"+id).html(input_kapasitas);
  $(".tipe_"+id).html(input_tipe);
  $(".kondisi_"+id).html(input_kondisi);
  $(".tambahan_"+id).html(input_tambahan);
  $(".lokasi_"+id).html(input_lokasi);
  $(".bukti_"+id).html(input_bukti);
  $(".keterangan_"+id).html(input_keterangan);

  var tombol = '<button type="submit" class="btn btn-success btn-rounded btn-sm" onclick="UpdateTools(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
  $("tr.tr_"+id+" .tombolAksiTools").html(tombol);
  $(".AddBtnTools").css('display','none')

    $.each($('tr.tools'), function(x,y){
      if($(y).attr('data-id') == id){
        $('.edit_'+id).css('display','none');
      }else{
        $('.edit_'+$(y).attr('data-id')).css('display','none');
        $('.delete_'+$(y).attr('data-id')).css('display','none');
      }
    });

}

// function UpdateTools(a) {
//   var tool_name = $("#tool_name").val()
//   var qty = $("#qty").val()
//   var capacity = $("#capacity").val()
//   var type = $("#type").val()
//   var condition = $("#condition").val()
//   var additional_maker = $("#additional_maker").val()
//   var location = $("#location").val()
//   var file = $("#file").val()
//   var description = $("#description").val()
//   var id = $(".tools").attr('data-id')
//   var id_user = "{{Auth::user()->id}}";
//   var url = "{{URL::to('vms/users/update_tools')}}";
//     $.ajax({
//           type: "post",
//           url: url,
//         data: {
//             "_token": "{{ csrf_token() }}",
//             "tool_name": tool_name,
//             "qty": qty,
//             "capacity": capacity,
//             "type": type,
//             "condition": condition,
//             "additional_maker": additional_maker,
//             "location": location,
//             "file": file,
//             "description": description,
//             "id": id,
//             "id_user": id_user,
//             },
//           success: function (a) {
//             // location.reload();
//           }
//     });
// }

function DeleteTools(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_tools')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}

//######################################################TENAGA AHLI###############################################
function EditItemTenagaAhli(a) {
  var id = $(a).attr('data-id');
  var nama = $(a).attr('data-nama');
  var ttl = $(a).attr('data-ttl');
  var jk = $(a).attr('data-jk');
  var pendidikan = $(a).attr('data-pendidikan');
  var pengalaman = $(a).attr('data-pengalaman');
  var profesi = $(a).attr('data-profesi');
  var alamat = $(a).attr('data-alamat');
  var warga = $(a).attr('data-warga');
  var email = $(a).attr('data-email');

  var input_nama = '<input type="text" name="name_vendor_expert_staff[]" id="name_vendor_expert_staff" value="'+nama+'" class="form-control">';
  var input_ttl = '<input type="date" name="birth_vendor_expert_staff[]" id="birth_vendor_expert_staff" value="'+ttl+'" required="" class="form-control">';
  var input_alamat= '<textarea name="address_vendor_expert_staff[]" id="address_vendor_expert_staff" value="'+alamat+'" required="" class="form-control">'+alamat+'</textarea>';
  var input_jk = '<select name="gender_vendor_expert_staff[]" id="gender_vendor_expert_staff" required="" class="form-control">'+
                        '<option value="L">L</option>'+
                        '<option value="P">P</option>'+
                    '</select>';
  var input_pendidikan = '<input type="text" value="'+pendidikan+'" name="education_vendor_expert_staff[]" id="education_vendor_expert_staff" required="" class="form-control">';
  var input_warga = '<input type="text" value="'+warga+'" name="nationallty_vendor_expert_staff[]" id="nationallty_vendor_expert_staff" required="" class="form-control">';
  var input_pengalaman = '<input type="number" value="'+pengalaman+'" name="experience_vendor_expert_staff[]" id="experience_vendor_expert_staff" required="" class="form-control">';
  var input_email = '<input type="email" value="'+email+'" name="email_vendor_expert_staff[]" id="email_vendor_expert_staff" required="" class="form-control">';
  var input_profesi = '<input type="text" value="'+profesi+'" name="expertise_vendor_expert_staff[]" id="expertise_vendor_expert_staff" required="" class="form-control">';

  $("tr.tenaga_ahli_"+id+" td.nama_"+id).html(input_nama);
  $("tr.tenaga_ahli_"+id+" td.ttl_"+id).html(input_ttl);
  $("tr.tenaga_ahli_"+id+" td.jk_"+id).html(input_jk);
  $("tr.tenaga_ahli_"+id+" td.pendidikan_"+id).html(input_pendidikan);
  $("tr.tenaga_ahli_"+id+" td.pengalaman_"+id).html(input_pengalaman);
  $("tr.tenaga_ahli_"+id+" td.profesi_"+id).html(input_profesi);
  $("tr.tenaga_ahli_"+id+" td.alamat_"+id).html(input_alamat);
  $("tr.tenaga_ahli_"+id+" td.warga_"+id).html(input_warga);
  $("tr.tenaga_ahli_"+id+" td.email_"+id).html(input_email);

  var tombol = '<button type="button" class="btn btn-success btn-rounded btn-sm" onclick="UpdateTenagaAhli(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
  $("tr.tenaga_ahli_"+id+" .tombolAksiTenagaAhli").html(tombol);
  $(".AddBtnTenagaAhli").css('display','none')

    $.each($('.tenaga_ahli'), function(x,y){
      if($(y).attr('data-id') == id){
        $('.edit_'+id).css('display','none');
      }else{
        $('.edit_'+$(y).attr('data-id')).css('display','none');
        $('.delete_'+$(y).attr('data-id')).css('display','none');
      }
    })


}

function UpdateTenagaAhli(a) {
  var nama = $("#name_vendor_expert_staff").val();
  var ttl = $("#birth_vendor_expert_staff").val();
  var jk = $("#gender_vendor_expert_staff").val();
  var pendidikan = $("#education_vendor_expert_staff").val();
  var pengalaman = $("#experience_vendor_expert_staff").val();
  var profesi = $("#expertise_vendor_expert_staff").val();
  var alamat = $("#address_vendor_expert_staff").val();
  var warga = $("#nationallty_vendor_expert_staff").val();
  var email = $("#email_vendor_expert_staff").val();
  var id_tenaga = $("tr#tenaga_ahli").attr('data-id');
  var id_user = "{{Auth::user()->id}}";

  var url = "{{URL::to('vms/users/update_tenaga_ahli')}}";
  $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "nama": nama,
          "ttl": ttl,
          "jk": jk,
          "pendidikan": pendidikan,
          "pengalaman": pengalaman,
          "profesi": profesi,
          "alamat": alamat,
          "warga": warga,
          "email": email,
          "id_tenaga": id_tenaga,
          "id_user": id_user,
          },
        success: function (a) {
          location.reload();
        }
  });
}

function AddBtnTenagaAhli(){
    var table = '<tr class="child"><td><input type="text" name="name_vendor_expert_staff[]" id="name_vendor_expert_staff" class="form-control"></td>'+
                '<td><input type="date" name="birth_vendor_expert_staff[]" id="birth_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td>'+
                    '<select name="gender_vendor_expert_staff[]" id="gender_vendor_expert_staff" required="" class="form-control">'+
                        '<option value="L">L</option>'+
                        '<option value="P">P</option>'+
                    '</select>'+
                '</td>'+
                '<td><input type="text" name="education_vendor_expert_staff[]" id="education_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td><input type="number" name="experience_vendor_expert_staff[]" id="experience_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td><input type="text" name="expertise_vendor_expert_staff[]" id="expertise_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td><textarea name="address_vendor_expert_staff[]" id="address_vendor_expert_staff" required="" class="form-control"></textarea></td>'+
                '<td><input type="text" name="nationallty_vendor_expert_staff[]" id="nationallty_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td><input type="email" name="email_vendor_expert_staff[]" id="email_vendor_expert_staff" required="" class="form-control"></td>'+
                '<td><a href="#" class="btn btn-success btn-rounded btn-sm" onclick="SaveTenagaAhli(this)"><i class="fa fa-check"></i> Selesai</a>'+
                '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowTenagaAhli()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
    $('#tenaga_ahli tbody').append(table);
    $('.AddBtnTenagaAhli').css('display','none');
    $.each($('.tenaga_ahli'), function(x,y){
        $('.editTenagaAhli').css('display','none');
        $('.deleteTenagaAhli').css('display','none');
    })
}

function deleteRowTenagaAhli() {
    $('#tenaga_ahli tr:last').remove();
    $('.AddBtnTenagaAhli').css('display','');
    $.each($('.tenaga_ahli'), function(x,y){
        $('.editTenagaAhli').css('display','');
        $('.deleteTenagaAhli').css('display','');
    })
}

function SaveTenagaAhli(a) {
    var nama = $('#name_vendor_expert_staff').val();
    var ttl = $('#birth_vendor_expert_staff').val();
    var jk = $('#gender_vendor_expert_staff').val();
    var pendidikan = $('#education_vendor_expert_staff').val();
    var pengalaman = $('#experience_vendor_expert_staff').val();
    var profesi = $('#expertise_vendor_expert_staff').val();
    var alamat = $('#address_vendor_expert_staff').val();
    var warga = $('#nationallty_vendor_expert_staff').val();
    var email = $('#email_vendor_expert_staff').val();
    var id_user = "{{Auth::user()->id}}";

    if((nama == '') && (ttl  == '') && (jk  == '') && (pendidikan  == '') && (pengalaman  == '') && (profesi  == '') && (alamat  == '') && (warga  == '') && (email  == '')){
      alert("Mohon cek ulang isian Anda!")
    }else{
        var url = "{{URL::to('vms/users/create_tenaga_ahli')}}";
        $.ajax({
              type: "post",
              url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "nama": nama,
                "ttl": ttl,
                "jk": jk,
                "pendidikan": pendidikan,
                "pengalaman": pengalaman,
                "profesi": profesi,
                "alamat": alamat,
                "warga": warga,
                "email": email,
                "id_user": id_user,
                },
              success: function (a) {
                location.reload();
              }
        });
    }
}

function DeleteTenagaAhli(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_tenaga_ahli')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}
//######################################################DETAIL PERUSAHAN##########################################
var additional = [];
function Edit(a) {
  $(a).attr("data-toggle","modal");
  $(a).attr("data-target", "#myModal");
  var id_vendor = '<input required type="text" name="id_vendor" value="{{Auth::user()->id}}" id="id_vendor" class="form-control">';
  var data = '<input required type="text" name="data" value="Identitas Perusahaan" id="id_vendor" class="form-control">';
  $("div.id_vendor").html(id_vendor);
  $("div#inputData").html(data);
alert("diklik");
  // var menu = $(a).attr('data-id')
  //untuk kantor cabang
  // if((menu == 'penyedia') || (menu == 7)){
  //   $.each($('.form_input_left'), function(k, v){
  //     if(k == 7){
  //         input =
  //                 '<div class="custom-control custom-radio">'+
  //                   '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked1" {{($vendor->branch_office == 1) ? 'checked' : ''}} onclick="branch" name="branch_office" value="1">'+
  //                   '<label class="custom-control-label" for="defaultUnchecked1">Ya</label>'+
  //                 '</div>'+
  //                 '<div class="custom-control custom-radio">'+
  //                   '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked" {{($vendor->branch_office == 0) ? 'checked' : ''}} onclick="branch" name="branch_office" value="0">'+
  //                   '<label class="custom-control-label" for="defaultUnchecked">Tidak</label>'+
  //                 '</div>';
  //
  //         $('#form_input_left_'+k).html(input);
  //     }
  //
  //   })
    //data sebelah kanan
  //   $.each($('.form_input'), function(k, v){
  //     if(($(v).attr('data-form') != 0) && ($(v).attr('data-form') !=1)){
  //       if (typeof data[k] === "undefined") {
  //           var nilai = $(v).attr('data-id');
  //       }else{
  //           var nilai = data[k];
  //       }
  //       input = "<input type='text' class='form-control input-lg form_data' value='"+nilai+"' style='height:24px !important; min-height:0px;'>";
  //       $('#'+$(v).attr('data-form')).html(input);
  //     }
  //
  //   })
  //   var tombol = '<button type="button" class="btn btn-success btn-rounded" onclick="Update(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
  //   $('#tombol').html(tombol)
  //
  //   branch();
  //
  //   additional[0] = (typeof $('#addresscentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->address)) ? $branch_office_detail->address : ''; ?>" : $('#addresscentral').val();
  //
  //   additional[1] =  (typeof $('#telephonecentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->telephone)) ? $branch_office_detail->telephone : ''; ?>" : $('#telephonecentral').val();
  //   additional[2] = (typeof $('#faxcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->fax)) ? $branch_office_detail->fax : ''; ?>" : $('#faxcentral').val();
  //
  //   additional[3] = (typeof $('#hpcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->mobile_phone)) ? $branch_office_detail->mobile_phone : ''; ?>" : $('#hpcentral').val();
  // }else if(menu == 'izin_usaha'){
  //   console.log("loading ...")
  // }
}

var data = [];

function Update(a) {
  var radio = $('input[name=branch_office]:checked').val();
  if(radio == 1)$('#row_branch').css('display','none');

  var pkp = $($('.form_data')[0]).val();
  var email = $($('.form_data')[1]).val();
  var tlp = $($('.form_data')[2]).val();
  var fax = $($('.form_data')[3]).val();
  var hp = $($('.form_data')[4]).val();
  var web = $($('.form_data')[5]).val();
  var id_user = "{{Auth::user()->id}}";

  if(($('#form_input_left_7').text() != 'Tidak')){
    var addresscentral = $('#addresscentral').val();
    var telephonecentral = $('#telephonecentral').val();
    var faxcentral = $('#faxcentral').val();
    var hpcentral = $('#hpcentral').val();
    var branch_office = $('input[name=branch_office]:checked').val();
  }

  var url = "{{URL::to('vms/users/update')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "pkp": pkp,
          "email": email,
          "tlp": tlp,
          "fax": fax,
          "hp": hp,
          "web": web,
          "branch_office": branch_office,
          "addresscentral": addresscentral,
          "telephonecentral": telephonecentral,
          "faxcentral": faxcentral,
          "hpcentral": hpcentral,
          },
        success: function (a) {
          data[2] = a.pkp;
          data[3] = a.email;
          data[4] = a.telephone;
          data[5] = a.fax;
          data[6] = a.mobile_phone;
          data[7] = a.website;

          $.each($('.form_input'), function(k, v){
            if((k != 0) && (k != 1)){
              $('#'+k).html(data[k]);
              var tombol = '<button type="button" class="btn btn-info btn-rounded ButtonEdit" onclick="Edit(this)" data-id="'+k+'"><i class="fa fa-pencil"></i> Ubah</button>';
              $('#tombol').html(tombol);
              if(a.status == 1){
               var notif = '<div class="alert alert-success alert-dismissible">'+
                              '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                              '<strong>Sukses!</strong> Update Data Perusahaan!.'+
                            '</div>';
              }else{
               var notif = '<div class="alert alert-danger alert-dismissible">'+
                              '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                              '<strong>Gagal!</strong> Update Data Perusahaan!.'+
                            '</div>';
              }
                $('div.page-alerts').html(notif);

            }
          })
                var radiotext = (radio == 1) ? 'Ya' : 'Tidak';
                $('#form_input_left_7').html(radiotext);


        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(thrownError);
        }
   });
}

function branch(){
  $('.branch_office').on('change', function(){
      var data =
              '<h4>Kantor Pusat</h4><hr>'+
              '<div class="form-group{{ $errors->has('addresscentral') ? ' has-error' : '' }}">'+
                  '<label for="addresscentral" class="col-md-2 control-label">Alamat</label>'+
                  '<div class="col-md-12">'+
                    '<textarea name="addresscentral" class="form-control" id="addresscentral" rows="3" required="" cols="130">'+additional[0]+'</textarea>'+
                      '@if ($errors->has('addresscentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('addresscentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                 ' </div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('telephonecentral') ? ' has-error' : '' }}">'+
                  '<label for="telephonecentral" class="col-md-2 control-label">Telepon</label>'+
                  '<div class="col-md-12">'+
                      '<input id="telephonecentral" required="" type="text" class="form-control" name="telephonecentral" value="'+additional[1]+'" required autofocus>'+
                      '@if ($errors->has('telephonecentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('telephonecentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                  '</div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('faxcentral') ? ' has-error' : '' }}">'+
                  '<label for="faxcentral" class="col-md-2 control-label">Fax</label>'+
                  '<div class="col-md-12">'+
                      '<input id="faxcentral" type="text" required="" class="form-control" name="faxcentral" value="'+additional[2]+'" required autofocus>'+
                      '@if ($errors->has('faxcentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('faxcentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                  '</div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('hpcentral') ? ' has-error' : '' }}">'+
                  '<label for="hpcentral" class="col-md-2 control-label">Hp</label>'+
                  '<div class="col-md-12">'+
                      '<input id="hpcentral" type="text" required="" class="form-control" name="hpcentral" value="'+additional[3]+'" required autofocus>'+
                      '@if ($errors->has('hpcentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('hpcentral') }}</strong>'+
                         ' </span>'+
                      '@endif'+
                  '</div>'+
              '</div>';
      if($(this).val() == 1){
          $('#row_branch').css('display','block');
          $('#branch_office_additional').html(data);
      }else{
          $('#row_branch').css('display','none');
          $('#branch_office_additional').html('');
      }
  });
}

//######################################################DOCUMENT READY############################################
var menus ="";
$(document).ready(function(){
  menus = "{{$id}}";
  if(menus == 'izin_usaha'){
    $('.ButtonEdit').css('display','none');
  }else  if(menus == 'dokumen'){
    $('.ButtonEdit').css('display','none');
  }else if(menus == 'akta'){
      var jumlah = "{{count($akta)}}";
      if(jumlah >= 1){
        $('.ButtonEdit').css('display','none');
      }
      else{
        $('.ButtonEdit').css('display','none');
        var tombol = '<button type="button" onclick="AddAkta(this)" class="btn btn-success btn-rounded ButtonAdd" data-id="{{$id}}"><i class="fa fa-plus"></i> Tambah</button>';
        $('.menu_additional_akta').html(tombol)
      }
  }else if(menus == 'pengurus'){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnPengurus" onclick="AddPengurus()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);
  }else if(menus == 'inbox'){
      $('.ButtonEdit').css('display','none');
  }else if(menus == 'tenaga_ahli'){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnTenagaAhli" onclick="AddBtnTenagaAhli()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);
  }else if(menus == 'peralatan'){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnTools" onclick="AddBtnTools()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);
  }else if(menus == 'pengalaman'){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnExperiences" onclick="AddBtnExperiences()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);
  }else if(menus == 'pajak'){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnTax" onclick="AddBtnTax()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);
  }else if(menus == 'pemilik'){
    var pemilik = {{$jumlahPemilik}};
    if(pemilik == 0){
      buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtnPemilik" onclick="Add()" data-id=""><i class="fa fa-plus"></i> Tambah</button>';
      $('#tombol').html(buttonAdd);

      $('.AddBtnPemilik').click(function() {
          var table =
                  '<tr class="child"><td><input type="text" name="name_vendor_owner[]" id="name_vendor_owner" required="" class="form-control"></td>'+
                  '<td><input type="text" name="ktp_vendor_owner[]" id="ktp_vendor_owner" required="" class="form-control"></td>'+
                  '<td><textarea name="address_vendor_owner[]" id="address_vendor_owner" required="" class="form-control"></textarea></td>'+
                  '<td><a href="#" class="btn btn-success btn-rounded btn-sm" onclick="SavePemilik(this)"><i class="fa fa-check"></i> Selesai</a>'+
                  '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowPemilik()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
         $('#pemilik tbody').html(table);
      });
    }else{
      $('.ButtonEdit').css('display','none');
    }
  }else{
    $('.ButtonEdit').css('display','-webkit-inline-box');
  }
})

//#####################################################PENGURUS#################################################
function EditItemPengurus(a) {
  var id = $(a).attr('data-id');
  var nama = $(a).attr('data-nama');
  var ktp = $(a).attr('data-ktp');
  var alamat = $(a).attr('data-alamat');
  var jabatan = $(a).attr('data-jabatan');

  var input_nama = '<input type="text" name="name_vendor_administrators[]" id="name_vendor_administrators" value="'+nama+'" class="form-control">';
  var input_ktp = '<input type="text" name="ktp_vendor_administrators[]" id="ktp_vendor_administrators" value="'+ktp+'" class="form-control">';
  var input_alamat = '<input type="text" name="address_vendor_administrators[]" id="address_vendor_administrators" value="'+alamat+'" class="form-control">';
  var input_jabatan = '<input type="text" name="position_vendor_administrators[]" id="position_vendor_administrators" value="'+jabatan+'" class="form-control">';

  $("td.nama_"+id).html(input_nama);
  $("td.ktp_"+id).html(input_ktp);
  $("td.alamat_"+id).html(input_alamat);
  $("td.jabatan_"+id).html(input_jabatan);

  buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtn" style="display:none;" onclick="Add(this)" data-id=""><i class="fa fa-plus"></i> Tambah</button>'+
    '<button type="button" class="btn btn-success btn-rounded SaveBtn btn-sm" onclick="UpdatePengurus(this)" data-id=""><i class="fa fa-check"></i> Simpan</button>';
  $("td.tombolAksiPengurus").html(buttonAdd);
  $(".AddBtnPengurus").css("display","none");

    $.each($('.tr_pengurus'), function(x,y){
      if($(y).attr('data-id') == id){
        $('.edit_'+id).css('display','none');
      }else{
        $('.edit_'+$(y).attr('data-id')).css('display','none');
        $('.delete_'+$(y).attr('data-id')).css('display','none');
      }
    })

}

function UpdatePengurus(a) {
  var nama = $("#name_vendor_administrators").val();
  var ktp = $("#ktp_vendor_administrators").val();
  var alamat = $("#address_vendor_administrators").val();
  var jabatan = $("#position_vendor_administrators").val();
  var url = "{{URL::to('vms/users/update_pengurus')}}";
  var id_user = "{{Auth::user()->id}}";
  $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "nama": nama,
          "ktp": ktp,
          "alamat": alamat,
          "jabatan": jabatan,
          },
        success: function (a) {
          location.reload();

        }
  });
}

function AddPengurus() {
  var table =
          '<tr class="child"><td><input type="text" name="name_vendor_administrators[]" id="name_vendor_administrators" required="" class="form-control"></td>'+
          '<td><input type="text" name="ktp_vendor_administrators[]" id="ktp_vendor_administrators" required="" class="form-control"></td>'+
          '<td><textarea name="address_vendor_administrators[]" id="address_vendor_administrators" required="" class="form-control"></textarea></td>'+
          '<td><input type="text" name="position_vendor_administrators[]" id="position_vendor_administrators" required="" class="form-control"></td>'+
          '<td><a href="#" class="btn btn-success btn-rounded btn-sm" onclick="SavePengurus(this)"><i class="fa fa-check"></i> Selesai</a>'+
          '<a href="#" class="btn btn-danger hide btn-rounded btn-sm" onclick="deleteRowPengurus()"><i class="fa fa-trash"></i> Hapus</a></td></tr>';
  $('#pengurus tbody').append(table);
  $('.AddBtnPengurus').css('display','none');
  // $("td.tombolAksiPengurus").html("-");
    $.each($('.tr_pengurus'), function(x,y){
        $('.editPengurus').css('display','none');
        $('.deletePengurus').css('display','none');
    })
}

function deleteRowPengurus() {
    $('#pengurus tr:last').remove();
    $('.AddBtnPengurus').css('display','');
    $.each($('.tr_pengurus'), function(x,y){
        $('.editPengurus').css('display','');
        $('.deletePengurus').css('display','');
    })
}

function SavePengurus(a) {
  var nama = $("#name_vendor_administrators").val();
  var ktp = $("#ktp_vendor_administrators").val();
  var alamat = $("#address_vendor_administrators").val();
  var jabatan = $("#position_vendor_administrators").val();
  if((nama !== '') && (ktp !== '') && (alamat !== '') && (jabatan !== '')){
      var url = "{{URL::to('vms/users/create_pengurus')}}";
      var id_user = "{{Auth::user()->id}}";
      $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "nama": nama,
              "ktp": ktp,
              "alamat": alamat,
              "jabatan": jabatan,
              },
            success: function (a) {
              location.reload();

            }
      });
  }else{
    alert("mohon cek ulang isian anda!");
  }
}

function DeletePengurus(a) {
  var r = confirm("Apakah Anda Yakin Ingin Hapus?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_pengurus')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}

//#######################################################SURAT BISNIS##########################################
var dataId ="";
function EditLetter(a) {
  $('.AddBtn').css('display', 'none');
  dataId = $(a).attr('data-id');
  var dataOne = $(a).attr('data-one').split(' ').join('_');
  var dataTwo = $(a).attr('data-two').split(' ').join('_');
  var dataThree = $(a).attr('data-three').split(' ').join('_');
  var dataFour = $(a).attr('data-four').split(' ').join('_');
  var dataFive = $(a).attr('data-five').split(' ').join('_');
  var dataSix = $(a).attr('data-six').split(' ').join('_');

  $('#'+dataTwo+'_'+dataId).html('<input type="text" data-id="'+dataTwo+'_'+dataId+'" id="reference_number" required name="reference_number['+dataId+'][]" value="'+dataTwo.split('_').join('')+'" class="form-control input-sm" style="height:24px !important; min-height:0px; width:70%;">');
  $('#'+dataThree+'_'+dataId).html('<input type="date" data-id="'+dataThree+'_'+dataId+'" id="valid_until" required name="valid_until['+dataId+'][]" value="'+dataThree.split('_').join('')+'" class="form-control input-sm" style="height:24px !important; min-height:0px; width:80%;">');
  $('#'+dataFour+'_'+dataId).html('<input type="text" data-id="'+dataFour+'_'+dataId+'" id="giver_agency" required name="giver_agency['+dataId+'][]" value="'+dataFour.split('_').join('')+'" class="form-control input-sm" style="height:24px !important; min-height:0px; width:50%;">');
  $('#'+dataFive+'_'+dataId).html('<input type="text" data-id="'+dataFive+'_'+dataId+'" id="qualification" required name="qualification['+dataId+'][]" value="'+dataFive.split('_').join('')+'" class="form-control input-sm" style="height:24px !important; min-height:0px; width:50%;">');
  $('#'+dataSix+'_'+dataId).html('<input type="text" data-id="'+dataSix+'_'+dataId+'" id="classification" required name="classification['+dataId+'][]" value="'+dataSix.split('_').join('')+'" class="form-control input-sm" style="height:24px !important; min-height:0px; width:50%;">');

  $('td.dataTable').css('background','white');
  buttonAdd = '<button type="button" class="btn btn-info btn-rounded AddBtn" style="display:none;" onclick="Add(this)" data-id=""><i class="fa fa-plus"></i> Tambah</button>'+
    '<button type="button" class="btn btn-success btn-rounded SaveBtn" onclick="saveLetter(this)" data-id=""><i class="fa fa-check"></i> Simpan</button>';

  $('.menu_additional').html(buttonAdd);

  $.each($('.rawTable'), function(x,y){
      if($(y).attr('data-id') == dataId){
        $('#edit_'+dataId).css('display','none');
      }else{
        $('#edit_'+$(y).attr('data-id')).css('display','none');
        $('#delete_'+$(y).attr('data-id')).css('display','none');
        // $('.td_'+$(y).attr('data-id')).html('');
      }
  })
}

function saveLetter(a) {
  var reference_number = $('#reference_number').val();
  var valid_until = $('#valid_until').val();
  var giver_agency =  $('#giver_agency').val();
  var qualification =  $('#qualification').val();
  var classification = $('#classification').val();
  var id_bussines_license = dataId;

  if((reference_number !== '') && (valid_until !== '') &&(giver_agency !== '') && (qualification !== '') && (
classification !== '')){
      var url = "{{URL::to('vms/users/update_izin_usaha')}}";
      var id_user = "{{Auth::user()->id}}";
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "reference_number": reference_number,
              "valid_until": valid_until,
              "giver_agency": giver_agency,
              "qualification": qualification,
              "classification": classification,
              "id_bussines_license": id_bussines_license,
              },
            success: function (a) {
            $.each($('.rawTable'), function(x,y){
                  $('#edit_'+$(y).attr('data-id')).css('display','-webkit-inline-box');
                  $('#delete_'+$(y).attr('data-id')).css('display','-webkit-inline-box');
            })

            // $('#classification').attr('data-id');
            $('#'+$('#reference_number').attr('data-id')).html($('#reference_number').val())
            $('#'+$('#valid_until').attr('data-id')).html($('#valid_until').val())
            $('#'+$('#giver_agency').attr('data-id')).html($('#giver_agency').val())
            $('#'+$('#qualification').attr('data-id')).html($('#qualification').val())
            $('#'+$('#classification').attr('data-id')).html($('#classification').val())

            $('.SaveBtn').css('display','none');
            $('.AddBtn').css('display', '-webkit-inline-box');

            if(a.status == 1){
             var notif = '<div class="alert alert-success alert-dismissible">'+
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                            '<strong>Sukses!</strong> Update Izin Usaha Perusahaan!.'+
                          '</div>';
            }else{
             var notif = '<div class="alert alert-danger alert-dismissible">'+
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                            '<strong>Gagal!</strong> Update Izin Usaha Perusahaan!.'+
                          '</div>';
            }
              $('div.page-alerts').html(notif);


            }
        });
  }else{
    alert("mohon cek ulang isian anda!");
  }
}

function DeleteLetter(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id_user = "{{Auth::user()->id}}";
      var id_bussines_license = $(a).attr('data-id');
      var url = "{{URL::to('vms/users/delete_izin_usaha')}}";
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "id_bussines_license": id_bussines_license,
              },
            success: function (a) {
            $('#tr_'+a.id_bussines_license).remove();
            location.reload();

            if(a.status == 1){
             var notif = '<div class="alert alert-success alert-dismissible">'+
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                            '<strong>Sukses!</strong> Hapus Izin Usaha Perusahaan!.'+
                          '</div>';
            }else{
             var notif = '<div class="alert alert-danger alert-dismissible">'+
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                            '<strong>Gagal!</strong> Hapus Izin Usaha Perusahaan!.'+
                          '</div>';
            }
              $('div.page-alerts').html(notif);

            }
        });
    } else {
        location.reload();
    }
}

function Add(a) {
  var Table = '<tr>'+
                  '<td>'+
                      '<select class="form-control input-sm" id="id_vendor_business_license" onclick="LetterClick(this)" name="id_vendor_business_license" style="height:36px !important; min-height:0px; width:80%;">'+
                       '<option selected disabled>-Pilih-</option>'+
                        '@foreach($vendor_business_license as $k => $v)'+
                          '<option value="{{$v->id}}" {{(in_array($v->id, $Arraysurat)) ? 'disabled' : 'ra no'}}>{{$v->name_vendor_business_license}}</option>'+
                        '@endforeach'+
                        '</select></td>'+
                  '<td><input type="text" class="form-control input-sm reference_number_additional" style="height:36px !important; min-height:0px; width:80%;"></td>'+
                  '<td><input type="date" class="form-control input-sm valid_until_additional" style="height:36px !important; min-height:0px; width:80%;"></td>'+
                  '<td><input type="text" class="form-control input-sm giver_agency_additional" style="height:36px !important; min-height:0px; width:80%;"></td>'+
                  '<td><input type="text" class="form-control input-sm qualification_additional" style="height:36px !important; min-height:0px; width:80%;"></td>'+
                  '<td><input type="text" class="form-control input-sm classification_additional" style="height:36px !important; min-height:0px; width:80%;"></td>'+
                  '<td style="text-align:center;" class="BtnAction">'+
                  '</td>'+
              '</tr>';
  $('#LetterTable').append(Table);
}

function DeleteItem(a){
     $('#LetterTable tr:last').remove();
}

function DeleteItemAkta(a){
     $('#AktaTable tbody.pendirian tr:last').remove();
     $('.menu_additional_akta').css('display','block');
     var tombol = '<button type="button" class="btn btn-success btn-rounded ButtonAdd" data-id="{{$id}}" onclick="AddAkta(this)"><i class="fa fa-plus"></i> Tambah</button>';

      $('.menu_additional_akta').html(tombol)
      $('.pembaruan').css('display','');
}

var LincenseId="";
function LetterClick(a) {
  LincenseId = $(a).val();
  $('.reference_number_additional').attr('name','reference_number['+LincenseId+'][]');
  $('.valid_until_additional').attr('name','valid_until['+LincenseId+'][]');
  $('.giver_agency_additional').attr('name','giver_agency['+LincenseId+'][]');
  $('.qualification_additional').attr('name','qualification['+LincenseId+'][]');
  $('.classification_additional').attr('name','classification['+LincenseId+'][]');


  var Tmbl =
  '<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="saveLetterAdditional(this)" data-id=""><i class="fa fa-check"></i> Simpan</button>'+
  '<button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteItem(this)" data-id="" id="delete_"><i class="fa fa-trash"></i> Hapus</button>';
  $('.BtnAction').html(Tmbl)
}

function saveLetterAdditional(a) {
  var reference_number = $('.reference_number_additional').val();
  var valid_until = $('.valid_until_additional').val();
  var giver_agency = $('.giver_agency_additional').val();
  var qualification = $('.qualification_additional').val();
  var classification = $('.classification_additional').val();

  if((reference_number !== '') && (valid_until !== '') && (giver_agency !== '') && (qualification !== '') && (classification !== '')){
      var id_user = "{{Auth::user()->id}}";
      var url = "{{URL::to('vms/users/tambahan_izin_usaha')}}";
      var id_bussines_license = $(a).attr('data-id');
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "id_bussines_license": LincenseId,
              "reference_number": reference_number,
              "valid_until": valid_until,
              "giver_agency": giver_agency,
              "qualification": qualification,
              "classification": classification,
              },
            success: function (a) {
              location.reload();
            }
          })

  }else{
    alert("mohon cek ulang isian anda!");
  }
}


//##########################################################AKTA################################################
function DeleteAkta(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
        var id_deed = $(a).attr('data-id');
        var id_user = "{{Auth::user()->id}}";
        var url = "{{URL::to('vms/users/delete_akta')}}";
        var id_bussines_license = $(a).attr('data-id');
          $.ajax({
              type: "post",
              url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "id_user": id_user,
                "id_deed": id_deed
                },
              success: function (a) {
                $('#AktaTable tr:last').remove();
                var tombol = '<button type="button" class="btn btn-success btn-rounded ButtonAdd" data-id="{{$id}}" onclick="AddAkta(this)"><i class="fa fa-plus"></i> Tambah</button>';

                $('.menu_additional_akta').html(tombol)
                 if(a.status == 1){
                   var notif = '<div class="alert alert-success alert-dismissible">'+
                                  '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                  '<strong>Sukses!</strong> Hapus Akta Perusahaan!.'+
                                '</div>';
                  }else{
                   var notif = '<div class="alert alert-danger alert-dismissible">'+
                                  '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                  '<strong>Gagal!</strong> Hapus Akta Perusahaan!.'+
                                '</div>';
                  }
                $('div.page-alerts').html(notif);

              }
            });
    } else {
        location.reload();
    }
}

function DeleteItemAktaPembaruanNoAjax(a) {
    $('#AktaTable tbody.pembaruan tr:last').remove();
    $('.menu_additional_akta').css('display','block');
   var tombol = '<button type="button" class="btn btn-success btn-rounded ButtonAdd" data-id="{{$id}}" onclick="AddAktaPembaruan(this)"><i class="fa fa-plus"></i> Tambah Pembaruan</button>';

    $('.menu_additional_akta').html(tombol)
    $('.pendirian').css('display','');
}

function DeleteItemAktaPembaruan(a) {
var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
        var id_deed = $(a).attr('data-id');
        var url = "{{URL::to('vms/users/delete_akta_pembaruan')}}";
          $.ajax({
              type: "post",
              url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "id_deed": id_deed
                },
              success: function (a) {
                 location.reload();
              }
            });
    } else {
        location.reload();
    }
}

function AddAkta(a) {
    var AddTable = '<tr>'+
                      '<td>'+
                          '<input type="date" name="date" id="date" class="form-control">'+
                      '</td>'+
                      '<td>'+
                          '<input type="text" name="deed_number" id="deed_number" class="form-control">'+
                      '</td>'+
                      '<td>'+
                          '<input type="text" name="notary_public" id="notary_public" class="form-control">'+
                      '</td>'+
                      '<td>'+
                          '<button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteItemAkta(this)" data-id="{{$v->id}}" id="delete"><i class="fa fa-trash"></i> Hapus</button>'+
                      '</td>'+
                    '</tr>';
    var tombol = '<button type="button" class="btn btn-success btn-rounded" onclick="UpdateAkta(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
    $('.menu_additional_akta').html(tombol);
    $('#AktaTable tbody.pendirian').html(AddTable);

    $('.pembaruan').css('display','none');

}

function AddAktaPembaruan(a) {
    var AddTable = '<tr  style="background-color: #f6f9fa; text-align: center;"><td colspan="4">Akta Pembaruan</td></tr>'+
                    '<tr>'+
                      '<td>'+
                          '<input type="date" name="date" id="date" class="form-control">'+
                      '</td>'+
                      '<td>'+
                          '<input type="text" name="deed_number" id="deed_number" class="form-control">'+
                      '</td>'+
                      '<td>'+
                          '<input type="text" name="notary_public" id="notary_public" class="form-control">'+
                      '</td>'+
                      '<td style="text-align:center;">'+
                          '<button type="button" class="btn btn-danger hide btn-rounded btn-xs" onclick="DeleteItemAktaPembaruanNoAjax(this)" data-id="{{$v->id}}" id="delete"><i class="fa fa-trash"></i> Hapus</button>'+
                      '</td>'+
                    '</tr>';
    var tombol = '<button type="button" class="btn btn-success btn-rounded" onclick="UpdateAktaPembaruan(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
    $('.menu_additional_akta').html(tombol);
    $('#AktaTable tbody.pembaruan').html(AddTable);
}

function UpdateAkta(a) {
  var deed_number = $('#deed_number').val();
  var date = $('#date').val();
  var notary_public = $('#notary_public').val();
  var id_user = "{{Auth::user()->id}}";
  var url = "{{URL::to('vms/users/create_akta')}}";
  if((deed_number != '') && (date != '') && (notary_public != '')){
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "deed_number": deed_number,
              "date": date,
              "notary_public": notary_public
              },
            success: function (a) {
              location.reload();
              $('.pembaruan').css('display','none');
            }
    })
  }else{
    alert("mohon cek ulang isian anda!");
  }

}
function UpdateAktaPembaruan(a) {
  var deed_number = $('#deed_number').val();
  var date = $('#date').val();
  var notary_public = $('#notary_public').val();
  var id_deed = $('tr.id_deed').attr('data-id');
  var id_user = "{{Auth::user()->id}}";
  var url = "{{URL::to('vms/users/create_akta_pembaruan')}}";
  if((deed_number != '') && (date != '') && (notary_public != '')){
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "id_vendor_deed" : id_deed,
              "deed_number": deed_number,
              "date": date,
              "notary_public": notary_public
              },
            success: function (a) {
              location.reload();
              $('.pembaruan').css('display','none');
            }
    })
  }else{
    alert("mohon cek ulang isian anda!");
  }

}

function EditItemAkta(a) {
  var date = $(a).attr('data-date');
  var deed_number = $(a).attr('data-deed_number');
  var notary_public = $(a).attr('data-notary_public');
  var titik_notary_public = notary_public.split('.').join(' ');

  $('.'+date.split(' ').join('_')).html("<input type='date' name='date' id='date' value='"+date+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");
  $('.'+deed_number.split(' ').join('_')).html("<input type='text' name='deed_number' id='deed_number' value='"+deed_number+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");
  $('.'+titik_notary_public.split(' ').join('_')).html("<input type='text' name='notary_public' id='notary_public' value='"+notary_public+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");

  var tmblAksi = '<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="SaveUpdateAkta(this)" data-id="{{$v->id}}" id="simpanAkta"><i class="fa fa-check"></i> Simpan</button>';
  $('.tombolAksiAkta').html(tmblAksi);
}

function EditItemAktaPembaruan(a) {
  var date = $(a).attr('data-date');
  var deed_number = $(a).attr('data-deed_number');
  var notary_public = $(a).attr('data-notary_public');
  var titik_notary_public = notary_public.split('.').join(' ');

  $('.'+date.split(' ').join('_')).html("<input type='date' name='date' id='date' value='"+date+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");
  $('.'+deed_number.split(' ').join('_')).html("<input type='text' name='deed_number' id='deed_number' value='"+deed_number+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");
  $('.'+titik_notary_public.split(' ').join('_')).html("<input type='text' name='notary_public' id='notary_public' value='"+notary_public+"' class='form-control' style='height:24px !important; min-height:0px; width:100%;'>");

  var tmblAksi = '<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="SaveUpdateAktaPembaruan(this)" data-id="{{$v->id}}" id="simpanAkta"><i class="fa fa-check"></i> Simpan</button>';
  $('.tombolAksiAktaPembaruan').html(tmblAksi);
}

function SaveUpdateAkta(a) {
  var deed_number = $('#deed_number').val();
  var date = $('#date').val();
  var notary_public = $('#notary_public').val();
  var id_user = "{{Auth::user()->id}}";
  var url = "{{URL::to('vms/users/update_akta')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_user": id_user,
              "deed_number": deed_number,
              "date": date,
              "notary_public": notary_public
              },
            success: function (a) {
              location.reload();

            }
    })
}

function SaveUpdateAktaPembaruan(a) {
  var deed_number = $('#deed_number').val();
  var date = $('#date').val();
  var notary_public = $('#notary_public').val();
  var id_deed = $('tr.id_deed').attr('data-id');
  var url = "{{URL::to('vms/users/update_akta_pembaruan')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "deed_number": deed_number,
              "id_vendor_deed": id_deed,
              "date": date,
              "notary_public": notary_public
              },
            success: function (a) {
              location.reload();

            }
    })

}
//###########################################################DOKUMEN#############################################
// function DokumenAkta(a) {
//   $('#row_doc_akta').css('display','');
//   var upload = '<center><h3>Akta</h3></center>'+
//                 '<form  method="post" id="" action="{{ URL::to('vms/users/akta_upload') }}" enctype="multipart/form-data">'+
//                 '{{ csrf_field() }}'+
//                   '<input type="file" placeholder="File" class="form-control full" name="akta_pdf" id="akta_pdf" oninput="" required>'+
//                 '</form>'+
//                   '<br><br>'+
//                   '<div class="table-responsive">'+
//                     '<table class="table table-bordered">'+
//                       '<thead>'+
//                         '<th>No'+
//                         '<th>Dokumen'+
//                         '<th>Aksi'+
//                       '</thead>'+
//                       '<tbody>'+
//                       '@if(count($doc) >= 1)'+
//                       '@foreach($doc as $k => $v)'+
//                         '<tr>'+
//                           '<td>.$k+1.</td>'+
//                           '<td>.$v->file.</td>'+
//                           '<td><button class="btn btn-rounded btn-danger">Hapus</button></td>'+
//                         '</tr>'+
//                       '@endforeach'+
//                       '@else'+
//                         '<tr style="text-align: center;"><td colspan="3">Tidak ada data</td></tr>'+
//                       '@endif'+
//                       '</tbody>'+
//                     '</table>'
//                   '</div>'


//   $('#doc_akta').html(upload);
// }


function ListDoc(a) {
  $('#detail').css('display','-webkit-box');
  var id_user = $(a).attr('data-id');
  var page = "{{$vendor_detail_lulus->currentPage()}}";
   var url = "{{URL::to('vms/users/document_vendor')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "halaman": page,
          },
        success: function (a) {
          var jumlah = a.doc.length;
          var ul ="";
          ul += '<ul>';
              if(jumlah > 0){
                  $.each(a.doc, function(k, v){
                  tanggal = v.file.substr(0,8);
                  tahun = v.file.substr(0,4)
                  bulan = v.file.substr(4,2)
                  hari = v.file.substr(6,2)
                  full = tahun+"-"+bulan+"-"+hari;

                  menues = ['Penyedia',
                          'Izin Usaha',
                          'Akta',
                          'Pemilik',
                          'Pengurus',
                          'Tenaga Ahli',
                          'Peralatan',
                          'Pengalaman',
                          'Pajak',
                          'Dokumen',];

                  ul +='<b>'+menues[v.menu]+'</b>'
                    ul += '<li>';
                    ul += '<a href="/assets/document/'+a.vendor_id+'/'+menues[v.menu]+'/'+full+'/'+v.file+'" target="_blank">'+ v.file+'</a></li>';
                  })
                  ul += '</ul>';
                }else{
                  ul += '<li>Tidak ada Dokumen </li>'
                }
          $('#detail_body_dok').html(ul)
        }
    });
}


function DeleteDoc(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
        var id = $(a).attr('data-id');
        var menu = $(a).attr('data-menu');
        var url = "dokumen_delete/"+id+"/"+menu;
         $(a).attr("href",url)
    } else {
        location.reload();
    }
}
//####################################################PEMILIK##################################################
function deleteRowPemilik() {
   $('#pemilik tr:last').remove();
}

function SavePemilik() {
  var nama = $("#name_vendor_owner").val();
  var ktp = $("#ktp_vendor_owner").val();
  var alamat = $("#address_vendor_owner").val();
  var id_user = "{{Auth::user()->id}}";
  if((nama !== '') && (ktp !== '') && (alamat !== '')){
    var url = "{{URL::to('vms/users/create_pemilik')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "nama": nama,
          "ktp": ktp,
          "alamat": alamat,
          },
        success: function (a) {
          location.reload();
          $('.AddBtnPemilik').css('display','none');
        }
    });
  }else{
    alert("mohon cek ulang isian anda!")
  }
}

function EditItemPemilik(a) {
  var id = $(a).attr('data-id');
  var nama = $(a).attr('data-nama');
  var alamat = $(a).attr('data-alamat');
  var ktp = $(a).attr('data-ktp');

  var input_nama = '<input type="text" name="name_vendor_owner" id="name_vendor_owner" class="form-control" value="'+nama+'">';
  var input_alamat = '<input type="text" name="ktp_vendor_owner" id="ktp_vendor_owner" class="form-control" value="'+alamat+'">';
  var input_ktp = '<input type="text" name="address_vendor_owner" id="address_vendor_owner" class="form-control" value="'+ktp+'">';
  var tmblAksi = '<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="SaveUpdatePemilik(this)" data-id="'+id+'" data-nama="'+nama+'" data-alamat="'+alamat+'" data-ktp="'+ktp+'" id="simpanPemilik"><i class="fa fa-check"></i> Simpan</button>';

  $('td.nama_'+id).html(input_nama);
  $('td.ktp_'+id).html(input_ktp);
  $('td.alamat_'+id).html(input_alamat);
  $('td.tombolAksiPemilik').html(tmblAksi);
}

function SaveUpdatePemilik(a) {
  var id = $(a).attr('data-id');
  var nama = $("#name_vendor_owner").val();
  var alamat = $("#ktp_vendor_owner").val();
  var ktp = $("#address_vendor_owner").val();
  var id_user = "{{Auth::user()->id}}";

  var url = "{{URL::to('vms/users/update_pemilik')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id": id,
              "nama": nama,
              "alamat": alamat,
              "ktp": ktp,
              "id_user": id_user
              },
            success: function (a) {
              location.reload();
            }
    });
}

function DeletePemilik(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
        var id = $(a).attr('data-id');
        var url = "{{URL::to('vms/users/delete_pemilik')}}";
          $.ajax({
                type: "post",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                    },
                  success: function (a) {
                    location.reload();
                  }
          });
    } else {
        location.reload();
    }
}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});
</script>
@endsection
