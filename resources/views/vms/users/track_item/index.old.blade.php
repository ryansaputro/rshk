@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Lacak Barang...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Lacak Barang...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="row btn-group btn-group-devided">
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="row">
            <form>
              <div class="col-md-offset-3 col-sm-4 search" style="text-align:right;">
                <label for="id_po" class="col-sm-4">No Po</label>
                <div class="col-sm-8">
                  <input list="spj" type="text" name="id_po" value="{{($key != 'xxx') ? $key : ''}}" class="form-control input-sm" placeholder="pencarian ...">
                  <datalist id="spj">
                    @foreach ($do as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}">
                      @endforeach
                    </datalist>
                </div>
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form>
          </div>
        </div>
      </div>
        <div class="portlet light">
        @if ($deliveryDtl !== 'xxx')
          <div class="portlet-body">
            <div class="row">
              <div class="col-xs-12">
                  <a style="text-align:right;" href="#" class="btn btn-primary btn-sm" onclick='printDiv();' target="_blank">Print</a>
                <div class="portlet light" id='DivIdToPrint'>
                  <div class="portlet-title">
                    <div class="caption caption-md">
                      <span class="caption-helper uppercase">

                      </span>
                      <p style="color:#000;"><b>{{$VendorDetail->vendor_name}}</b></p>
                      {{$VendorDetail->address}}, {{$VendorDetail->city}} <br>
                      Tlp. {{$VendorDetail->telephone}} - Fax. {{$VendorDetail->fax}} <br>
                      {{$VendorDetail->province}} - {{$VendorDetail->post_code}}
                    </div>
                    <br>
                  </div><!--/.portlet-title--->
                  @php
                  $time = strtotime($delivery->datetime);
                  $function =  Fungsi::MonthIndonesia();
                  $functionDay =  Fungsi::DayIndonesia();
                  @endphp
                  <div class="actions" id="tombol">
                      <div class="col-xs-offset-8 col-xs-4" style="text-align:left;">
                        <u> <b>SURAT JALAN</b> </u>
                        <br>
                        <br>
                        <b>No. {{$delivery->no_do}}/SPJ/{{date('m', $time)}}/{{date('Y', $time)}}</b>
                      </div>
                  </div>
                  <div class="portlet-body">

                    <div class="col-xs-8" style="border:1px;">
                      <p>Kepada Yth.</p>
                      <b>{{$destination}}</b>
                      <br>
                      <p>{{$address}}</p>
                    </div>
                    <div class="col-xs-4" style="border:1px; text-align:left;">
                      <p><b>Tanggal Kirim : {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}</b></p>
                    </div>
                      <div class="col-xs-12">
                        Bersama dengan ini kami kirimkan sejumlah barang berikut ini:
                        <table class="table table-striped">
                          <tr>
                            <th>No</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                          </tr>
                          @if (count($deliveryDtl) > 0)
                            @foreach ($deliveryDtl as $k => $v)
                              <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$v->code}} <br> {{$v->name}} <br> {{$v->merk}} </td>
                                <td>{{$v->qty}}</td>
                                <td>{{$v->satuan}}</td>
                                <td>{{($v->description != '') ? $v->description : '-'}}</td>
                              </tr>
                            @endforeach
                          @else
                            <tr>
                              <td colspan="5">Tidak ada data</td>
                            </tr>
                          @endif
                        </table>
                        <div class="col-xs-6">
                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="no_po" class="col-xs-12 control-label">Sopir</label>
                            <br><br>
                            <br><br>
                            <br><br>
                            <label for="no_po" class="col-xs-12 control-label">{{$driver}}</label>
                          </div>
                        </div>
                        <div class="col-xs-6" style="text-align:right;">
                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="no_po" class="col-xs-12 control-label">{{$destination}}</label>
                            <br><br>
                            <br><br>
                            <br><br>
                            <label for="no_po" class="col-xs-12 control-label">(...............................)</label>
                          </div>
                        </div>
                      </div>
                    <!-- BEGIN PAGINATOR -->
                    <div class="row">
                        {{-- <div class="col-xs-7 col-xs-7 block-paginate">{{ $request_order->links() }}</div> --}}
                    </div>
                    <!-- END PAGINATOR -->
                  </div><!--/.portlet-body-->
                </div>
              </div>
            </div>
            <!-- BEGIN PAGINATOR -->
            <div class="row">
              <div class="col-md-5 col-sm-5">
                {{-- <div>menampilkan {{ ($request_order->currentPage() - 1) * $request_order->perPage() + 1 }} sampai {{ $request_order->count() * $request_order->currentPage() }} dari {{ $request_order->total() }} data</div> --}}
              </div>
              {{-- <div class="col-md-7 col-sm-7 block-paginate">{{ $request_order->links() }}</div> --}}
            </div>
            <!-- END PAGINATOR -->
          </div><!--/.portlet-body-->
        @else
          <h4>
            <center>Tidak Ada Data</center>
          </h4>
        @endif
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
