@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Lacak Barang...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Lacak Barang...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="row btn-group btn-group-devided">
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="row">
            <form>
              <div class="col-sm-11 search" style="text-align:right;">
                <label for="id_po" class="col-sm-1">No Po</label>
                <div class="col-sm-11">
                  <input list="spj" type="text" name="id_po" value="{{($key != 'xxx') ? $key : ''}}" class="form-control input-sm" placeholder="pencarian ...">
                  <datalist id="spj">
                    @foreach ($do as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}">
                      @endforeach
                    </datalist>
                </div>
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form>
          </div>
        </div>
      </div>
        <div class="portlet light">
        @if ($deliveryDtl !== 'xxx')
          <div class="portlet-body">
            <div class="row">
              <div class="">
                <table class="table table-striped">
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No SPJ</th>
                    <th>Driver</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                  @if (count($deliveryDtl) > 0)
                    @foreach ($deliveryDtl as $k => $v)
                      @if ($v->status == 1)
                        @php
                        $status = "sedang dikirim";
                        $statusClr = "primary";
                        @endphp
                      @else
                        @php
                        $status = "telah diterima";
                        $statusClr = "success";
                        @endphp
                      @endif
                      <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$v->datetime}}</td>
                        <td>{{$v->no_do}}/SPJ/{{date('m', $time)}}/{{date('Y', $time)}}</td>
                        <td>{{$v->driver}}/{{$v->car}}/{{$v->car_no}}</td>
                        <td><span class='btn btn-block btn-{{$statusClr}} btn-sm'>{{$status}}</span> </td>
                        <td> <a href="{{URL::to('vms/users/delivery_order/preview/'.md5($v->id))}}" class="btn btn-primary btn-sm">Detail</a> </td>
                      </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="6" style="text-align:center;">Tidak Ada Data</td>
                    </tr>
                  @endif

                </table>
              </div>
            </div>
            <!-- BEGIN PAGINATOR -->
            <div class="row">
              <div class="col-md-5 col-sm-5">
                {{-- <div>menampilkan {{ ($request_order->currentPage() - 1) * $request_order->perPage() + 1 }} sampai {{ $request_order->count() * $request_order->currentPage() }} dari {{ $request_order->total() }} data</div> --}}
              </div>
              {{-- <div class="col-md-7 col-sm-7 block-paginate">{{ $request_order->links() }}</div> --}}
            </div>
            <!-- END PAGINATOR -->
          </div><!--/.portlet-body-->
        @else
          <h4>
            <center>Tidak Ada Data</center>
          </h4>
        @endif
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
