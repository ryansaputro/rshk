@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Percakapan...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Percakapan...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
              {{-- <button type="button" class="btn btn-info btn-rounded" onclick="" data-id=""><i class="fa fa-pencil"></i> Ubah</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <table class="table table-bordered table-striped" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Pembeli</th>
                <th>Pesan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $key => $value)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$value->datetime}}</td>
                  <td>{{$value->username}}</td>
                  <td>{{$value->message}}<span class="badge">baru</span></td>
                  <td> <a href="{{URL::to('vms/users/percakapan_detail/'.$value->id_cart.'/'.$value->id_user.'/'.$value->id_item)}}" class="btn btn-sm btn-primary">Buka</a> </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">

</script>
@endsection
