@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              {{-- <button type="button" class="btn btn-success btn-rounded" onclick="" data-id=""><i class="fa fa-plus"></i> Tambah</button> --}}
              <a href="/su_vms/assets/users/template/template-product.xlsx" target="_blank" class="btn btn-warning btn-rounded"><i class="fa fa-download"></i> Template(xls)</a>
              <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button>
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <table class="table table-bordered table-striped" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Kategori</th>
                <th>Item</th>
                <th>Deskripsi</th>
                <th>Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @if (count($data) > 0)
              @foreach ($data as $k => $v)
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->created_at}}</td>
                  <td>{{$v->name_category}}</td>
                  <td>{{$v->code}} <br> {{$v->name}} <br> {{$v->merk}} </td>
                  <td style="width:30%;">{{$v->description}}</td>
                  <td><p style="color:{{($v->status == 'Y') ? 'green' : 'red' }}">{{($v->status == 'Y') ? 'Aktif': 'Non Aktif'}}</p></td>
                  <td class="text-center">
                    {{-- <a href="#" class="btn btn-sm btn-primary">Detail</a> --}}
  										<div class="btn-group btn-group-xs">
  											{{-- <a href="" data-id="{{$v->id}}" onclick="DeleteItem(this)" class="btn btn-danger tooltips delete_{{$v->id}}" data-toggle="tooltip" title="Delete" data-placement="bottom">
  												<i class="fa fa-trash"></i>
  											</a> --}}
  											<a href="{{ URL::to('/vms/users/product/edit/'.$v->id) }}" class="btn btn-warning tooltips" data-toggle="tooltip" title="Edit" data-placement="bottom">
  												<i class="fa fa-pencil"></i>
  											</a>
  											<a href="{{ URL::to('/vms/users/product/spec/'.$v->id) }}" class="btn btn-success tooltips" data-toggle="tooltip" title="Spesifikasi" data-placement="bottom">
  												<i class="fa fa-list"></i>
  											</a>
  											<a href="{{ URL::to('/catalog/guest/item/detail/'.$v->id) }}" class="btn btn-primary tooltips" data-toggle="tooltip" title="Lihat" data-placement="bottom">
  												<i class="fa fa-eye"></i>
  											</a>
  											<a href="{{ URL::to('/vms/users/product/image/'.$v->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Gambar" data-placement="bottom">
  												<i class="fa fa-picture-o"></i>
  											</a>
  											<a href="{{ URL::to('/vms/users/product/doc/'.$v->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Dokumen" data-placement="bottom">
  												<i class="fa fa-folder-open-o"></i>
  											</a>
  											<a href="{{ URL::to('/vms/users/product/price/'.$v->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Harga" data-placement="bottom">
  												<i class="fa fa-dollar"></i>
  											</a>
  										</div>
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="7" style="text-align: center;">Tidak ada data.</td>
              </tr>
            @endif
            </tbody>
          </table>
          <!-- BEGIN PAGINATOR -->
          <div class="row">
            <div class="col-md-5 col-sm-5">
              <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
            </div>
              <div class="col-md-7 col-sm-7 block-paginate" style="text-align: right;">{{ $data->links() }}</div>
          </div>
          <!-- END PAGINATOR -->
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>

  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form class="" action="{{URL::to('vms/users/product/add')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Unggah Produk</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
               <label class="col-sm-2 control-label">Kategori</label>
               <div class="col-sm-10">
                 <select class="form-control" name="category" id="select_new">
                    @foreach ($category as $k => $v)
                      @if($v->id_parent == 0)
                        <option value="" style="text-transform: uppercase; color:#000; font-weight: 800;" disabled><b>{{$v->name}}</b></option>
                          @foreach ($category as $kParent => $vParent)
                            @if($vParent->id_parent == $v->id)
                              <optgroup style="font-weight: 400 !important;" label="- {{$vParent->name}}">
                                @foreach ($category as $kChild => $vChild)
                                  @if($vChild->id_parent == $vParent->id)
                                    <option value="{{$vChild->id}}">  - {{$vChild->name}}</option>
                                  @endif
                                @endforeach
                              </optgroup>
                            @endif
                          @endforeach
                      @endif
                    @endforeach
                  </select>
               </div>
             </div>
             <div class="form-group">
                <label class="col-sm-2 control-label">file</label>
                <div class="col-sm-10">
                  <input type="file" name="import_file" class="form-control" accept=".xlsx,.xls" required >
                </div>
              </div>
          </div>
          <div class="modal-footer" style="border-top:0px;">
            <br>
            <br>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>

      </div>
    </div>
  @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    // $('#select_new').select2();
  });
</script>
@endsection
