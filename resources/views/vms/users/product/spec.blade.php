@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
                <a href="#modal_add" class="bt btn-info btn-sm button_add" onclick="AddSpec(this)"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
              {{-- <button type="button" class="btn btn-success btn-rounded" onclick="" data-id=""><i class="fa fa-plus"></i> Tambah</button> --}}
              {{-- <a href="/su_vms/assets/users/template/template-product.xlsx" target="_blank" class="btn btn-warning btn-rounded"><i class="fa fa-download"></i> Template(xls)</a> --}}
              {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
						<table class="table table-striped table-bordered" style="width: 100%">
                @if (sizeof($category_spec) == 0)
                  <div class="row">
                    <div class="col-md-6">
                      <h3>Spesifikasi Umum</h3>
                    </div>
                    <div class="col-md-6" style="text-align:right; padding-top:20px;">
                      <a href="#" class="btn btn-warning btn-sm general_spec" onclick="EditGeneralSpec(this)" data-weight="{{$general_spec->weight}}" data-height="{{$general_spec->height}}" data-volume="{{$general_spec->volume}}" data-minimal_buy="{{$general_spec->minimal_buy}}"> <i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                    </div>
                  </div>
                  <tr>
                    <td style="width:40%;">Berat(gr)</td>
                    <td style="width:30%;">{{$general_spec->weight}} gram</td>
                    {{-- <th colspan="2">Tidak ada Data </th> --}}
                  </tr>
                  <tr>
                    <td style="width:40%;">Tinggi</td>
                    <td style="width:30%;">{{$general_spec->height}}</td>
                    {{-- <th colspan="2">Tidak ada Data </th> --}}
                  </tr>
                  <tr>
                    <td style="width:40%;">Volume</td>
                    <td style="width:30%;">{{$general_spec->volume}}</td>
                    {{-- <th colspan="2">Tidak ada Data </th> --}}
                  </tr>
                  <tr>
                    <td style="width:40%;">Minimal Beli</td>
                    <td style="width:30%;">{{$general_spec->minimal_buy}}</td>
                    {{-- <th colspan="2">Tidak ada Data </th> --}}
                  </tr>
                @else
                  <h3>Spesifikasi Khusus</h3>
                  <thead>
                    <tr>
                      <th style="width:40%;">Spesifikasi</th>
                      <th style="width:30%;">Deskripsi</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
  								@foreach ($category_spec as $key => $value)
  									@php
  										$description = DB::table('spec_item')->where('id_category_spec', $value->id)->where('id_item', $id)->value('description');
  									@endphp
  									<tr>
  										<td>{{ $value->name }}</td>
  										<td>
                        {{$description == '' ? '-' : $description}}
  											{{-- <input type="text" name="description[{{$value->id}}]" value="{!! $description == '' ? '-' : $description !!}" class="form-control"> --}}
  											{{-- <input type="hidden" name="id_item" value="{{$id}}" class="form-control"> --}}
  										</td>
                      <td style="Color: {{($value->status == 'Y' ? 'green' : 'red')}}">{{($value->status == 'Y' ? 'Aktif' : 'Non Aktif')}}</td>
                      <td><a href="#" class="btn btn-warning btn-sm button_edit_khusus" data-status="{{$value->status}}" data-id_spec="{{$value->id}}" data-cat="{{$value->id_category}}" data-name="{{$value->name }}" date-desc="{{$description}}" onclick="EditSpec(this)"> <i class="fa fa-pencil" aria-hidden="true"></i> Edit </a></td>
  									</tr>
  								@endforeach
							</tbody>
						</table>
            <table class="table table-striped table-bordered" style="width: 100%">
              <div class="row">
                <div class="col-md-6">
                  <h3>Spesifikasi Umum</h3>
                </div>
                <div class="col-md-6" style="text-align:right; padding-top:20px;">
                  <a href="#" class="btn btn-warning btn-sm general_spec" onclick="EditGeneralSpec(this)" data-weight="{{$general_spec->weight}}" data-height="{{$general_spec->height}}" data-volume="{{$general_spec->volume}}" data-minimal_buy="{{$general_spec->minimal_buy}}"> <i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                </div>
              </div>
              <thead>
								<tr>
									<th style="width:40%;">Spesifikasi</th>
									<th style="width:30%;">Deskripsi</th>
								</tr>
							</thead>
              <tr>
                <td style="width:40%;">Berat(gr)</td>
                <td style="width:30%;">{{$general_spec->weight}} gram</td>
                {{-- <th colspan="2">Tidak ada Data </th> --}}
              </tr>
              <tr>
                <td style="width:40%;">Tinggi</td>
                <td style="width:30%;">{{$general_spec->height}}</td>
                {{-- <th colspan="2">Tidak ada Data </th> --}}
              </tr>
              <tr>
                <td style="width:40%;">Volume</td>
                <td style="width:30%;">{{$general_spec->volume}}</td>
                {{-- <th colspan="2">Tidak ada Data </th> --}}
              </tr>
              <tr>
                <td style="width:40%;">Minimal Beli</td>
                <td style="width:30%;">{{$general_spec->minimal_buy}}</td>
                {{-- <th colspan="2">Tidak ada Data </th> --}}
              </tr>
            @endif

            </table>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>

  <!-- Modal -->
		<div class="modal fade" id="modal_add" role="dialog">
			<div class="modal-dialog">
				<form class="ModalForm" method="post" action="{{URL::to('vms/users/product/spec/add')}}">
					{{ csrf_field() }}
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Kategori Spesifikasi Baru</h4>
					</div>
					<div class="modal-body">
						<div class="id" style="display:none;">

						</div>
						<div class="form-group" id="labelCat">
								<label for="exampleInputEmail1" class="bmd-label-floating">Kategori</label>
								<input id="id_category_spec" type="hidden" class="form-control" name="id_category_spec" placeholder="id_category_spec" value="{{$id_category}}" readonly>
								<input id="name_category" type="text" class="form-control" name="name_category" placeholder="name_category" value="{{$category_name}}" readonly>
								<span class="bmd-help"></span>
						</div>
						<div class="form-group" id="labelSub">
								<label for="name" class="bmd-label-floating" id="label">Nama Spesifikasi</label>
								<input id="name" type="text" required class="form-control" name="name" placeholder="name">
								<span class="bmd-help"></span>
						</div>
						<div class="form-group">
								<label>
									<input type="radio" required checked name="status" class="optionsRadios2 Y" value="Y"> Aktif
								</label>
								<label>
									<input type="radio" required name="status" class="optionsRadios2 N" value="N"> Non Aktif
								</label>
						 </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</form>
			</div>
		</div>

    <!-- Modal -->
      <div class="modal fade" id="modal_general" role="dialog">
        <div class="modal-dialog">
          <form class="ModalForm" method="post" action="{{URL::to('vms/users/product/spec/add')}}">
            {{ csrf_field() }}
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Kategori Spesifikasi Baru</h4>
            </div>
            <div class="modal-body">
              <div class="id" style="display:none;">

              </div>
              <div class="form-group" id="labelweight">
                  <label for="exampleInputEmail1" class="bmd-label-floating">Berat</label>
                  <input id="id_item" type="hidden" class="form-control" name="id_item" placeholder="id_item" value="{{$id}}" readonly>
                  <input id="weight" type="number" class="form-control" name="weight" required min="0" placeholder="weight" value="{{$general_spec->weight}}">
                  <span class="bmd-help"></span>
              </div>
              <div class="form-group" id="labelheight">
                  <label for="height" class="bmd-label-floating" id="label">Tinggi</label>
                  <input id="height" type="text" required class="form-control" required min="0" name="height" placeholder="height" value="{{$general_spec->height}}">
                  <span class="bmd-help"></span>
              </div>
              <div class="form-group" id="labelvolume">
                  <label for="volume" class="bmd-label-floating" id="label">Volume</label>
                  <input id="volume" type="text" required class="form-control" required min="0" name="volume" placeholder="volume" value="{{$general_spec->volume}}">
                  <span class="bmd-help"></span>
              </div>
              <div class="form-group" id="labelminimal_buy">
                  <label for="minimal_buy" class="bmd-label-floating" id="label">Minimal Beli</label>
                  <input id="minimal_buy" type="text" required class="form-control" required min="0" name="minimal_buy" placeholder="minimal_buy" value="{{$general_spec->minimal_buy}}">
                  <span class="bmd-help"></span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </form>
        </div>
      </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection
@section('js')
<script type="text/javascript">
  function AddSpec(a) {
    $('.button_add').attr('data-toggle', 'modal');
  }
  function EditSpec(a) {
  var name_value = $(a).attr('data-name');
  var cat_value = $(a).attr('data-cat');
  var desc_value = $(a).attr('date-desc');
  var status_value = $(a).attr('data-status');
  var spec_cat_value = $(a).attr('data-id_spec');
  if(desc_value === undefined){
    var desc = "-";
  }else{
    var desc = desc_value;
  }
  var name =  '<div class="form-group" id="labelCat">'+
                  '<label for="name" class="bmd-label-floating" id="label">Spesifikasi Kategori {{$category_name}}</label>'+
                  '<input id="name" type="text" required class="form-control" name="name" value="'+name_value+'" placeholder="name">'+
                  '<input id="id_category" type="hidden" required class="form-control" name="id_category" value="'+cat_value+'" placeholder="name">'+
                  '<input id="id_item" type="hidden" required class="form-control" name="id_item" value="{{$id}}" placeholder="name">'+
                  '<input id="id_category_spec" type="hidden" required class="form-control" name="id_category_spec" value="'+spec_cat_value+'" placeholder="name">'+
                  '<span class="bmd-help"></span>'+
              '</div>';

  var desc =  '<div class="form-group" id="labelDesc">'+
                  '<label for="name" class="bmd-label-floating" id="label">Deskripsi</label>'+
                  '<textarea name="description" required class="form-control" rows="8" cols="80">'+desc+'</textarea>'+
                  // '<input id="name" type="text" required class="form-control" name="name" value="'+name_value+'" placeholder="name">'+
                  '<span class="bmd-help"></span>'+
              '</div>'


    $('.button_edit_khusus').attr('data-toggle', 'modal');
    $('.button_edit_khusus').attr('data-target', '#modal_add');
    $('h4.modal-title').html("Kategori Spesifikasi  {{$category_name}}");
    $('div#labelCat').html(name);
    $('div#labelSub').html(desc);
    $('.ModalForm').attr('action', '{{URL::to('vms/users/product/spec/update')}}');

    // $.each($('.optionsRadios2'), function(x,y){
    //   if(status_value == $(y).val()){
    //     $('.Y').removeAttr('checked')
    //     $('div.radio span').removeClass('checked')
    //     $(y).attr('checked', 'checked');
    //     console.log($(y).val());
    //     $('div.radio span').addClass('checked')
    //   }
    // });
  }

  function EditGeneralSpec(a) {
    var weight = $(a).attr('data-weight');
    var height = $(a).attr('data-height');
    var volume = $(a).attr('data-volume');
    var minimal_buy = $(a).attr('data-minimal_buy');

    $('.general_spec').attr('data-toggle', 'modal');
    $('.general_spec').attr('data-target', '#modal_general');
    $('.ModalForm').attr('action', '{{URL::to('vms/users/product/spec/general/update/')}}');

  }
</script>
@endsection
