@extends('vms.guest.layouts.app')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Register...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar hide">
        <!-- BEGIN THEME PANEL -->
        <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div>
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light col-md-offset-3" style="width:50%; margin-top:40px;">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Registrasi</span>
            {{-- <span class="caption-helper">Registration...</span> --}}
          </div>
        </div>
        <form class="form-horizontal form-material" style="margin-top:20px;" id="loginform" action="{{URL::to('/vms/guest/register')}}" method="POST">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group m-t-40">
                <div class="col-xs-2">
                  <select class="form-control" name="jenis">
                    <option value="PT">PT.</option>
                    <option value="CV">CV.</option>
                  </select>
                </div>
                <div class="col-xs-10">
                  <input id="vendor_name" type="text" class="form-control" name="vendor_name" value="{{ old('vendor_name') }}" required autofocus placeholder="Nama Perusahaan">
                </div>
              </div>
              <div class="form-group m-t-40">
                <div class="col-xs-12">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail Perusahaan">
                </div>
              </div>
              <div class="form-group m-t-40">
                <div class="col-xs-6 {{ $errors->has('username') ? ' has-error' : '' }}">
                  <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus placeholder="Nama Perwakilan">
                </div>
                <div class="col-xs-6 {{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                  <input id="mobile_phone" type="text" class="form-control" name="mobile_phone" value="{{ old('contact_person') }}" required autofocus placeholder="No tlp.">
                </div>
              </div>
              <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                  <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Daftar</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
   {{-- <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
           <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>
   </div> --}}
   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
@endsection
