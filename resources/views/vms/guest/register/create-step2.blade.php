@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Akta Perusahaan</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step2/'.$id) }}">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                <div class="tab-pane active" id="portlet_tab1">
                  <div class="scroller" style="height: 325px;">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>Nomor Akta</th>
                              <th class="{{ $errors->has('deed_number') ? ' has-error' : '' }}" ><input type="text" name="deed_number" value="{{$vendorDeed->deed_number or ''}}" class="form-control" required=""></th>
                            </tr>
                            <tr>
                              <th>Tanggal</th>
                              <th class="{{ $errors->has('date') ? ' has-error' : '' }}" ><input type="date" required="" value="{{$vendorDeed->date or ''}}" name="date" class="form-control"></th>
                            </tr>
                            <tr>
                              <th>Notaris</th>
                              <th class="{{ $errors->has('notary_public') ? ' has-error' : '' }}" ><input type="text" required="" name="notary_public" value="{{$vendorDeed->notary_public or ''}}" class="form-control"></th>
                            </tr>
                          </table>
                        </div>
                      </div>
                           <a href="#" class="btn btn-block btn-primary hide" id="akta">Akta Perubahan Terakhir</a>
                           <div class="table-responsive AdditionalAkta">

                           </div>


                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('vms/users/registration/'.$id) }}" class="btn btn-danger"><< Identitas Perusahaan </a>
                        {{-- <a type="button" href="{{route('x.vms.register')}}" class="btn btn-warning"><< Identitas Perusahaan</a> --}}
                        {{-- <a type="button" href="/products/create-step2" class="btn btn-warning">Back to Step 2</a> --}}

                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                  </div>
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
   {{-- <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
           <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>
   </div> --}}
   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
  // function classification(a) {
  //   //  data-toggle="modal" data-target="#myModal"
  //   // alert("woy");
  //   var id = $(a).attr('data-id');
  //   var name = $(a).attr('data-name');
  //   $('h4.modal-title').text(name)
  //   $('.classification_'+id).attr('data-toggle', 'modal');
  //   $('.classification_'+id).attr('data-target', '#myModal');
  // }


    $('.branch_office').on('change', function(){
        var data =
                '<h4>Kantor Pusat</h4><hr>'+
                '<div class="form-group{{ $errors->has('addresscentral') ? ' has-error' : '' }}">'+
                    '<label for="addresscentral" class="col-md-2 control-label">Alamat</label>'+
                    '<div class="col-md-10">'+
                      '<textarea name="addresscentral" class="form-control" rows="3" required="" cols="135"></textarea>'+
                        '@if ($errors->has('addresscentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('addresscentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                   ' </div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('telephonecentral') ? ' has-error' : '' }}">'+
                    '<label for="telephonecentral" class="col-md-2 control-label">Telepon</label>'+
                    '<div class="col-md-10">'+
                        '<input id="telephonecentral" required="" type="text" class="form-control" name="telephonecentral" value="{{ old('telephonecentral') }}" required autofocus>'+
                        '@if ($errors->has('telephonecentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('telephonecentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                    '</div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('faxcentral') ? ' has-error' : '' }}">'+
                    '<label for="faxcentral" class="col-md-2 control-label">Fax</label>'+
                    '<div class="col-md-10">'+
                        '<input id="faxcentral" type="text" required="" class="form-control" name="faxcentral" value="{{ old('faxcentral') }}" required autofocus>'+
                        '@if ($errors->has('faxcentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('faxcentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                    '</div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('hpcentral') ? ' has-error' : '' }}">'+
                    '<label for="hpcentral" class="col-md-2 control-label">Hp</label>'+
                    '<div class="col-md-10">'+
                        '<input id="name" type="text" required="" class="form-control" name="hpcentral" value="{{ old('hpcentral') }}" required autofocus>'+
                        '@if ($errors->has('hpcentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('hpcentral') }}</strong>'+
                           ' </span>'+
                        '@endif'+
                    '</div>'+
                '</div>';
        if($(this).val() == 1){
            $('.additional').html(data);
        }else{
            $('.additional').html('');
        }
    })

$('#propinsi').on('change', function(){
    var id = $($(this).children(":selected")).text();
    $.ajax({
        url: '{{URL::to('/register/getKota')}}',
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", "id": id},
        success: function (a) {
            var option = "";
            $.each(a.kota, function(k, v){
                option += "<option value='"+v+"'>"+v+"</option>";
            })
                $('#kota').html(option)
        }
    });
})
// $('#kota').on('change'/su_vms/assets/global/plugins/chosen/, function(){
//     var id = $($(this).children(":selected")).text();
//     $.ajax({
//         url: '{{URL::to('/register/getPostCode')}}',
//         method: 'POST',
//         data: {"_token": "{{ csrf_token() }}", "id": id},
//         success: function (a) {
//             var option = "";
//             $.each(a.kota, function(k, v){
//                 option += "<option value='"+k+"'>"+v+"</option>";
//             })
//                 $('#post_code').html(option)
//         }
//     });
// })

$('#akta').on('click', function(){
    if($(this).hasClass('tampilkan')){
        $('.AdditionalAkta').html('');
        $('#akta').removeClass("tampilkan");
    }else{

        var table = '<br><table class="table table-bordered">'+
                            '<tr>'+
                                '<th>Nomor Akta</th>'+
                                '<th><input type="text" required="" name="deed_number_akta" class="form-control"></th>'+
                            '</tr>'+
                            '<tr>'+
                                '<th>Tanggal</th>'+
                                '<th><input type="date" name="date_akta" required="" class="form-control"></th>'+
                            '</tr>'+
                            '<tr>'+
                                '<th>Notaris</th>'+
                                '<th><input type="text" required="" name="notary_public_akta" class="form-control"></th>'+
                            '</tr>'+
                    '</table>';

        $('.AdditionalAkta').html(table);
        $('#akta').addClass("tampilkan");
    }
})

$('a#add').click(function() {
    var table =
            '<tr class="child"><td><input type="text" name="name_vendor_owner[]" required="" class="form-control"></td>'+
            '<td><input type="text" name="ktp_vendor_owner[]" required="" class="form-control"></td>'+
            '<td><textarea name="address_vendor_owner[]" required="" class="form-control"></textarea></td></tr>';
   $('#pemilik tbody').append(table);
});

$('a#delete').click(function() {
   $('#pemilik tr:last').remove();
});


$('a#addpengurus').click(function() {
    var table = '<tr class="child"><td><input type="text" name="name_vendor_administrators[]" class="form-control"></td>'+
                '<td><input type="text" name="ktp_vendor_administrators[]" required="" class="form-control"></td>'+
                '<td><textarea name="address_vendor_administrators[]" required="" class="form-control"></textarea></td>'+
                '<td><input type="text" name="position_vendor_administrators[]" required="" class="form-control"></td>'+
                '</tr>';
   $('#pengurus tbody').append(table);
});

$('a#deletepengurus').click(function() {
   $('#pengurus tr:last').remove();
});


$('a#addstafahli').click(function() {
    var table = '<tr class="child"><td><input type="text" name="name_vendor_expert_staff[]" class="form-control"></td>'+
                '<td><input type="date" name="birth_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><textarea name="address_vendor_expert_staff[]" required="" class="form-control"></textarea></td>'+
                '<td>'+
                    '<select name="gender_vendor_expert_staff[]" required="" class="form-control">'+
                        '<option value="L">L</option>'+
                        '<option value="P">P</option>'+
                    '</select>'+
                '</td>'+
                '<td><input type="text" name="education_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="text" name="nationallty_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="number" name="experience_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="email" name="email_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="text" name="expertise_vendor_expert_staff[]" required="" class="form-control"></td></tr>';
   $('#stafahli tbody').append(table);
});

$('a#deletestafahli').click(function() {
   $('#stafahli tr:last').remove();
});

$body = $("body");

$(document).on({
     ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
     // ready: function() {
     //   $('.qualification').select2();
     // }
});

</script>
@endsection
