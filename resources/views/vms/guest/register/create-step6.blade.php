@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">pengurus</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step6/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Nama Pengurus</label>
                          <div class="col-md-10">
                            <input type="text" name="name" value="" class="form-control name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Jabatan Pengurus</label>
                          <div class="col-md-10">
                            <input type="text" name="jabatan" value="" class="form-control jabatan">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Ktp Pengurus</label>
                          <div class="col-md-10">
                            <input type="number" name="ktp"  maxlength="16" value="" class="form-control ktp">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Alamat Pengurus</label>
                          <div class="col-md-10">
                            <input type="text" name="alamat" value="" class="form-control alamat">
                          </div>
                        </div>
                        <div class="col-md-offset-5 col-md-3">
                          <button type="button" class="btn btn-block btn-success" onclick="addPengurus()"  name="button">Tambah</button>
                        </div>
                        <br>
                        <br>
                        <br>
                        @php
                          // echo "<pre>";
                          // print_r(count($vendorClassification));
                          // echo "</pre>";
                        @endphp
                        <div class="table-responsive">
                          <table class="table table-striped" id="tableAdmin">
                            <thead>
                              <tr>
                                <th width="10%;">Hapus</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Ktp</th>
                                <th>Alamat</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if ($vendorAdministrators != null)
                                  @foreach ($vendorAdministrators as $k => $v)
                                    @php
                                    $idNya = $k;
                                    @endphp
                                    <tr class="{{$k}}">
                                      <td>
                                        <button type="button" name="button" onclick="Hapus(this)" data-id="{{$k}}" class="btn btn-block btn-danger btn-xs">Hapus</button>
                                      </td>
                                      <td><input type='text' required name='name_name_vendor_administrators[{{$k}}]' value='{{$v->name_name_vendor_administrators}}' class='form-control'></td>
                                      <td><input type='text' required name='ktp_name_vendor_administrators[{{$k}}]' value='{{$v->ktp_name_vendor_administrators}}' class='form-control'></td>
                                      <td><input type='text' required name='address_name_vendor_administrators[{{$k}}]' value='{{$v->address_name_vendor_administrators}}' class='form-control'></td>
                                      <td><input type='text' required name='position_name_vendor_administrators[{{$k}}]' value='{{$v->position_name_vendor_administrators}}' class='form-control'></td>
                                    </tr>
                                  @endforeach
                                @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                      @php
                      @endphp
                  </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step5/'.$id) }}" class="btn btn-danger"><< Pemilik </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
        </form>
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
  <script type="text/javascript">
  var arr = [];

    function Hapus(a) {
      $('#tableAdmin tbody tr.'+$(a).attr('data-id')).remove();
    }

      function addPengurus(){
      var nama = $('.name').val();
      var ktp = $('.ktp').val();
      var alamat = $('.alamat').val();
      var jabatan = $('.jabatan').val();
      var x = $('.x').val();
      if(x == ''){
        var nilai = 1;
      }else{
        var nilai = parseInt(x)+1;
      }
      var id_klasifikasi = $('.klasifikasi :selected').val();
      var table ="";

            var idx = $('.x').attr('value',nilai)
            var id = $('.x').val();
            table += "<tr class='"+nilai+"'>";
              table += "<td>";
                table += '<button type="button" name="button" onclick="Hapus(this)" data-id="'+nilai+'" class="btn btn-block btn-danger btn-xs">Hapus</button>';
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='name_name_vendor_administrators["+id+"]' value='"+nama+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='ktp_name_vendor_administrators["+id+"]' value='"+jabatan+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='address_name_vendor_administrators["+id+"]' value='"+ktp+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='position_name_vendor_administrators["+id+"]' value='"+alamat+"' class='form-control'>";
              table += "</td>";
            table += "</tr>";
              $('#tableAdmin tbody').append(table)
              $('.name').val('');
              $('.ktp').val('');
              $('.alamat').val('');
              $('.jabatan').val('');
              $('.name').focus();
    }
  </script>
@endsection
