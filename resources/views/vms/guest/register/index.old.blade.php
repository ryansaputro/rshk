@extends('vms.guest.layouts.app')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small>Register...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar hide">
        <!-- BEGIN THEME PANEL -->
        <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div>
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            <i class="icon-speech theme-font"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper">Registration...</span>
          </div>
          <ul class="nav nav-tabs" style="float: left;">
            <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">Identitas Perusahaan</a>
            </li>
            <li><a href="#portlet_tab2" data-toggle="tab">Akta</a></li>
            <li><a href="#portlet_tab3" data-toggle="tab">Ijin Usaha</a></li>
            <li><a href="#portlet_tab4" data-toggle="tab">Pemilik</a></li>
            <li><a href="#portlet_tab5" data-toggle="tab">Pengurus</a></li>
            <li><a href="#portlet_tab6" data-toggle="tab">Staf Ahli</a></li>
          </ul>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('register/save') }}" novalidate>
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                <div class="tab-pane active" id="portlet_tab1">
                  <div class="scroller" style="height: 325px;">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('vendor_name') ? ' has-error' : '' }}">
                            <label for="vendor_name" class="col-md-4 control-label">Nama Perusahaan</label>
                            <div class="col-md-8">
                                <input id="vendor_name" type="text" class="form-control" name="vendor_name" value="{{ old('vendor_name') }}" required autofocus>
                                @if ($errors->has('vendor_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vendor_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
                            <label for="npwp" class="col-md-4 control-label">NPWP</label>
                            <div class="col-md-8">
                                <input id="npwp" type="text" class="form-control" name="npwp" value="{{ old('npwp') }}" required autofocus>
                                @if ($errors->has('npwp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('npwp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Alamat</label>
                            <div class="col-md-8">
                              <textarea name="address" class="form-control" rows="3" cols="50"></textarea>

                                <!-- <input id="name" type="text" class="form-control" name="name" value="{{ old('address') }}" required autofocus> -->
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province" class="col-md-4 control-label">Propinsi</label>
                            <div class="col-md-8">
                                <select name="province" id="propinsi" required autofocus class="form-control">
                                    <option value="" selected="" disabled="">- pilih -</option>
                                    @foreach($propinsi as $k => $v)
                                        <option value="{{$v}}">{{$v}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Kota</label>
                            <div class="col-md-8">
                                <select name="city" id="kota" required autofocus class="form-control">
                                    <option value="" selected="" disabled="">- pilih -</option>
                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Kode Pos</label>
                            <div class="col-md-8">
                                <select name="post_code" id="post_code" required autofocus class="form-control">
                                    <option value="" selected="" disabled="">- pilih -</option>
                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="post_code" class="col-md-4 control-label">Kode Pos</label>
                            <div class="col-md-8">
                                <input id="post_code" type="text" class="form-control" name="post_code" value="{{ old('post_code') }}" required autofocus>
                                @if ($errors->has('post_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('post_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('branch_office') ? ' has-error' : '' }}">
                            <label for="branch_office" class="col-md-4 control-label">Kantor Cabang</label>
                            <div class="col-md-8">
                              <div class="input-group">
                                <div class="radio">
                                  <label><input type="radio" required="" name="branch_office" value="1" class="branch_office">Ya</label>
                                </div>
                                <div class="radio">
                                  <label><input type="radio" required="" name="branch_office" value="0" class="branch_office" checked="">Tidak</label>
                                </div>
                              </div>
                                @if ($errors->has('branch_office'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch_office') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div><!-- /div -->

                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Nama Pengguna</label>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pkp') ? ' has-error' : '' }}">
                            <label for="pkp" class="col-md-4 control-label">Nomor PKP*</label>
                            <div class="col-md-8">
                                <input id="pkp" type="text" class="form-control" name="pkp" value="{{ old('pkp') }}" required autofocus>
                                @if ($errors->has('pkp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pkp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                            <label for="telephone" class="col-md-4 control-label">Telepon</label>
                            <div class="col-md-8">
                                <input id="telephone" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}" required autofocus>
                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                            <label for="fax" class="col-md-4 control-label">Fax</label>
                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control" name="fax" value="{{ old('fax') }}" required autofocus>
                                @if ($errors->has('fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                            <label for="mobile_phone" class="col-md-4 control-label">Hp</label>
                            <div class="col-md-8">
                                <input id="mobile_phone" type="text" class="form-control" name="mobile_phone" value="{{ old('mobile_phone') }}" required autofocus>
                                @if ($errors->has('mobile_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>
                            <div class="col-md-8">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Website</label>
                            <div class="col-md-8">
                                <input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}" required autofocus>
                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      </div><!-- /div -->

                      <div class="col-md-12 additional">
                        <hr>
                      </div>
                    </div> <!-- col.md.12 -->
                  </div>
                </div><!--/.tab-pane-->
                <div class="tab-pane" id="portlet_tab2">
                   <div class="scroller" style="height: 325px;">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                    <tr>
                                        <th>Nomor Akta</th>
                                        <th><input type="text" name="deed_number" class="form-control" required=""></th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th><input type="date" required="" name="date" class="form-control"></th>
                                    </tr>
                                    <tr>
                                        <th>Notaris</th>
                                        <th><input type="text" required="" name="notary_public" class="form-control"></th>
                                    </tr>
                            </table>
                        </div>
                        <a href="#" class="btn btn-block btn-primary hide" id="akta">Akta Perubahan Terakhir</a>
                        <div class="table-responsive AdditionalAkta">

                        </div>
                   </div>
                </div><!--/.tab-pane-->
                <div class="tab-pane" id="portlet_tab3">
                  <div class="scroller" style="height: 325px;">
                    <div class="panel-group" id="accordion">
                      {{-- <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td style="vertical-align: bottom;">
                              <label for="">Kualifikasi</label>
                            </td>
                            <td>
                              <select  class="form-control qualification qualification" name="qualification[][]" required="">
                                <option value="kecil">Kecil</option>
                                <option value="non kecil">Non Kecil</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td style="vertical-align: bottom;">
                              <label for="">Klasifikasi</label>
                            </td>
                            <td>
                              <select data-placeholder="Klasifikasi Bidang Usaha..." class="chosen-select form-control" multiple tabindex="4">
                                <option value=""></option>
                                <option value="G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN">G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN</option>
                              </select>
                            </td>
                          </tr>
                        </tbody>
                      </table> --}}
                        @foreach($surat_perusahaan as $k => $v)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$v->id}}">{{$v->name_vendor_business_license}}</a>
                                </h4>
                            </div>
                            <div id="collapse{{$v->id}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered" id="table_{{$v->id}}">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor Surat</th>
                                                        <th>Instansi Pemberi</th>
                                                        <th>Berlaku Sampai</th>
                                                        <th class="">Kualifikasi</th>
                                                        <th class="">Klasifikasi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" name="reference_number[{{$v->id}}][]" required="" class="form-control"></td>
                                                        <td><input type="text" name="giver_agency[{{$v->id}}][]" required="" class="form-control"></td>
                                                        <td><input type="date" name="valid_until[{{$v->id}}][]" required="" class="form-control"></td>
                                                        <td class="">
                                                          <select  class="form-control qualification qualification_{{$v->id}}" name="qualification[{{$v->id}}][]" required="">
                                                            <option value="kecil">Kecil</option>
                                                            <option value="non kecil">Non Kecil</option>
                                                          </select>
                                                        </td>
                                                        <td class="">
                                                          {{-- <select  name="qualification[{{$v->id}}][]" required="" class="form-control" tabindex="-1" aria-hidden="true">
                                                            <option value="kecil">Kecil</option>
                                                            <option value="non kecil">Non Kecil</option>
                                                          </select> --}}
                                                          {{-- <button type="button" class="btn btn-info btn-sm classification_{{$v->id}}" data-id="{{$v->id}}" data-name="{{$v->name_vendor_business_license}}" onclick="classification(this)">Qulification</button> --}}
                                                          <select data-placeholder="Klasifikasi Bidang Usaha..." class="chosen-select form-control" multiple tabindex="4">
                                                            <option value=""></option>
                                                            <option value="G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN">G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN</option>
                                                          </select>
                                                          {{-- <input type="text" name="classification[{{$v->id}}][]" required="" class="form-control"> --}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                  </div>
                </div><!--/.tab-pane-->
                <div class="tab-pane" id="portlet_tab4">
                   <div class="scroller" style="height: 325px;">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="pemilik">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>No KTP</th>
                                        <th>Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="name_vendor_owner[]" required="" class="form-control"></td>
                                        <td><input type="text" name="ktp_vendor_owner[]" required="" class="form-control"></td>
                                        <td><textarea name="address_vendor_owner[]" required="" class="form-control"></textarea></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="text-align: right;">
                                <a href="#" class="btn btn-primary" id="add">Tambah</a>
                                <a href="#" class="btn btn-danger hide" id="delete">Hapus</a>
                            </div>
                        </div>
                   </div>
                </div><!--/.tab-pane-->
                <div class="tab-pane" id="portlet_tab5">
                    <div class="scroller" style="height: 325px;">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="pengurus">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>No KTP</th>
                                        <th>Alamat</th>
                                        <th>Jabatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="name_vendor_administrators[]" required="" class="form-control"></td>
                                        <td><input type="text" name="ktp_vendor_administrators[]" required="" class="form-control"></td>
                                        <td><textarea name="address_vendor_administrators[]" required="" class="form-control"></textarea></td>
                                        <td><input type="text" name="position_vendor_administrators[]" required="" class="form-control"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="text-align: right;">
                                <a href="#" class="btn btn-primary" id="addpengurus">Tambah</a>
                                <a href="#" class="btn btn-danger hide" id="deletepengurus">Hapus</a>
                            </div>
                        </div>
                    </div>
                </div><!--/.tab-pane-->
                <div class="tab-pane" id="portlet_tab6">
                    <div class="scroller" style="height: 325px;">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="stafahli">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Alamat</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Pendidikan</th>
                                        <th>Warga Negara</th>
                                        <th>Pengalaman(tahun)</th>
                                        <th>Email</th>
                                        <th>Keahlian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="name_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><input type="date" name="birth_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><textarea name="address_vendor_expert_staff[]" required="" class="form-control"></textarea></td>
                                        <td>
                                            <select name="gender_vendor_expert_staff[]" required="" required="" class="form-control">
                                                <option value="L">L</option>
                                                <option value="P">P</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="education_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><input type="text" name="nationallty_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><input type="number" name="experience_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><input type="email" name="email_vendor_expert_staff[]" required="" class="form-control"></td>
                                        <td><input type="text" name="expertise_vendor_expert_staff[]" required="" class="form-control"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="text-align: right;">
                                <a href="#" class="btn btn-primary" id="addstafahli">Tambah</a>
                                <a href="#" class="btn btn-danger hide" id="deletestafahli">Hapus</a>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <button class="btn btn-primary">Simpan</button>
                        <button class="btn btn-danger">Batal</button>
                    </div>
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
   {{-- <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
           <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>
   </div> --}}
   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
  // function classification(a) {
  //   //  data-toggle="modal" data-target="#myModal"
  //   // alert("woy");
  //   var id = $(a).attr('data-id');
  //   var name = $(a).attr('data-name');
  //   $('h4.modal-title').text(name)
  //   $('.classification_'+id).attr('data-toggle', 'modal');
  //   $('.classification_'+id).attr('data-target', '#myModal');
  // }


    $('.branch_office').on('change', function(){
        var data =
                '<h4>Kantor Pusat</h4><hr>'+
                '<div class="form-group{{ $errors->has('addresscentral') ? ' has-error' : '' }}">'+
                    '<label for="addresscentral" class="col-md-2 control-label">Alamat</label>'+
                    '<div class="col-md-10">'+
                      '<textarea name="addresscentral" class="form-control" rows="3" required="" cols="135"></textarea>'+
                        '@if ($errors->has('addresscentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('addresscentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                   ' </div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('telephonecentral') ? ' has-error' : '' }}">'+
                    '<label for="telephonecentral" class="col-md-2 control-label">Telepon</label>'+
                    '<div class="col-md-10">'+
                        '<input id="telephonecentral" required="" type="text" class="form-control" name="telephonecentral" value="{{ old('telephonecentral') }}" required autofocus>'+
                        '@if ($errors->has('telephonecentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('telephonecentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                    '</div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('faxcentral') ? ' has-error' : '' }}">'+
                    '<label for="faxcentral" class="col-md-2 control-label">Fax</label>'+
                    '<div class="col-md-10">'+
                        '<input id="faxcentral" type="text" required="" class="form-control" name="faxcentral" value="{{ old('faxcentral') }}" required autofocus>'+
                        '@if ($errors->has('faxcentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('faxcentral') }}</strong>'+
                            '</span>'+
                        '@endif'+
                    '</div>'+
                '</div>'+
                '<div class="form-group{{ $errors->has('hpcentral') ? ' has-error' : '' }}">'+
                    '<label for="hpcentral" class="col-md-2 control-label">Hp</label>'+
                    '<div class="col-md-10">'+
                        '<input id="name" type="text" required="" class="form-control" name="hpcentral" value="{{ old('hpcentral') }}" required autofocus>'+
                        '@if ($errors->has('hpcentral'))'+
                            '<span class="help-block">'+
                                '<strong>{{ $errors->first('hpcentral') }}</strong>'+
                           ' </span>'+
                        '@endif'+
                    '</div>'+
                '</div>';
        if($(this).val() == 1){
            $('.additional').html(data);
        }else{
            $('.additional').html('');
        }
    })

$('#propinsi').on('change', function(){
    var id = $($(this).children(":selected")).text();
    $.ajax({
        url: '{{URL::to('/register/getKota')}}',
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", "id": id},
        success: function (a) {
            var option = "";
            $.each(a.kota, function(k, v){
                option += "<option value='"+v+"'>"+v+"</option>";
            })
                $('#kota').html(option)
        }
    });
})
// $('#kota').on('change'/su_vms/assets/global/plugins/chosen/, function(){
//     var id = $($(this).children(":selected")).text();
//     $.ajax({
//         url: '{{URL::to('/register/getPostCode')}}',
//         method: 'POST',
//         data: {"_token": "{{ csrf_token() }}", "id": id},
//         success: function (a) {
//             var option = "";
//             $.each(a.kota, function(k, v){
//                 option += "<option value='"+k+"'>"+v+"</option>";
//             })
//                 $('#post_code').html(option)
//         }
//     });
// })

$('#akta').on('click', function(){
    if($(this).hasClass('tampilkan')){
        $('.AdditionalAkta').html('');
        $('#akta').removeClass("tampilkan");
    }else{

        var table = '<br><table class="table table-bordered">'+
                            '<tr>'+
                                '<th>Nomor Akta</th>'+
                                '<th><input type="text" required="" name="deed_number_akta" class="form-control"></th>'+
                            '</tr>'+
                            '<tr>'+
                                '<th>Tanggal</th>'+
                                '<th><input type="date" name="date_akta" required="" class="form-control"></th>'+
                            '</tr>'+
                            '<tr>'+
                                '<th>Notaris</th>'+
                                '<th><input type="text" required="" name="notary_public_akta" class="form-control"></th>'+
                            '</tr>'+
                    '</table>';

        $('.AdditionalAkta').html(table);
        $('#akta').addClass("tampilkan");
    }
})

$('a#add').click(function() {
    var table =
            '<tr class="child"><td><input type="text" name="name_vendor_owner[]" required="" class="form-control"></td>'+
            '<td><input type="text" name="ktp_vendor_owner[]" required="" class="form-control"></td>'+
            '<td><textarea name="address_vendor_owner[]" required="" class="form-control"></textarea></td></tr>';
   $('#pemilik tbody').append(table);
});

$('a#delete').click(function() {
   $('#pemilik tr:last').remove();
});


$('a#addpengurus').click(function() {
    var table = '<tr class="child"><td><input type="text" name="name_vendor_administrators[]" class="form-control"></td>'+
                '<td><input type="text" name="ktp_vendor_administrators[]" required="" class="form-control"></td>'+
                '<td><textarea name="address_vendor_administrators[]" required="" class="form-control"></textarea></td>'+
                '<td><input type="text" name="position_vendor_administrators[]" required="" class="form-control"></td>'+
                '</tr>';
   $('#pengurus tbody').append(table);
});

$('a#deletepengurus').click(function() {
   $('#pengurus tr:last').remove();
});


$('a#addstafahli').click(function() {
    var table = '<tr class="child"><td><input type="text" name="name_vendor_expert_staff[]" class="form-control"></td>'+
                '<td><input type="date" name="birth_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><textarea name="address_vendor_expert_staff[]" required="" class="form-control"></textarea></td>'+
                '<td>'+
                    '<select name="gender_vendor_expert_staff[]" required="" class="form-control">'+
                        '<option value="L">L</option>'+
                        '<option value="P">P</option>'+
                    '</select>'+
                '</td>'+
                '<td><input type="text" name="education_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="text" name="nationallty_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="number" name="experience_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="email" name="email_vendor_expert_staff[]" required="" class="form-control"></td>'+
                '<td><input type="text" name="expertise_vendor_expert_staff[]" required="" class="form-control"></td></tr>';
   $('#stafahli tbody').append(table);
});

$('a#deletestafahli').click(function() {
   $('#stafahli tr:last').remove();
});

$body = $("body");

$(document).on({
     ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
     // ready: function() {
     //   $('.qualification').select2();
     // }
});

</script>
@endsection
