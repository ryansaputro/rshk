@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Klasifikasi Bidang</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step4/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        {{-- <div class="form-group">
                          <label for="qualification" class="col-md-2">Kualifikasi Perusahaan</label>
                          <div class="col-md-10">
                            <select  class="form-control qualification qualification_" name="qualification[]" required="">
                              <option value="kecil">Kecil</option>
                              <option value="non kecil">Non Kecil</option>
                            </select>
                          </div>
                        </div> --}}
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Klasifikasi (Bidang)</label>
                          <div class="col-md-9">
                            {{-- <select data-placeholder="Klasifikasi Bidang Usaha..." class="chosen-select form-control" multiple tabindex="4"> --}}
                            <select data-placeholder="Klasifikasi Bidang Usaha..." class="form-control klasifikasi">
                              <option value="" disabled selected>-pilih-</option>
                              @foreach ($klasifikasi as $v)
                                <option value="{{$v->id}}">{{$v->code}} - {{$v->name}}</option>
                              @endforeach
                              {{-- <option value="">G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN</option> --}}
                            </select>
                          </div>
                          <div class="col-md-1">
                            <button type="button" class="btn btn-block btn-success" onclick="pilihKlasifikasi()"  name="button">Pilih</button>
                          </div>
                        </div>
                        <br>
                        @php
                          // echo "<pre>";
                          // print_r(count($vendorClassification));
                          // echo "</pre>";
                        @endphp
                        <div class="table-responsive">
                          <table class="table table-striped" id="tableClassification">
                            <thead>
                              <tr>
                                <th>Hapus</th>
                                <th>Deskripsi Bidang Usaha</th>
                                <th>Sub Bidang Usaha</th>
                                <th>Kualifikasi Bidang Usaha</th>
                                <th>No Surat</th>
                                <th>Tanggal kadaluarsa</th>
                              </tr>
                            </thead>
                            <tbody>
                              @php
                              $CodesArr = $klasifikasi->pluck('code','id')->toArray();
                              $DescArr = $klasifikasi->pluck('description','id')->toArray();
                              @endphp
                              @if ($vendorClassification != null)
                                @foreach ($vendorClassification as $k => $v)
                                  @foreach ($v->id_classification as $kv => $vk)
                                  @php
                                  $idNya = $kv;
                                  @endphp
                                  <tr class="{{$kv}}">
                                    <td>
                                      <button type="button" name="button" onclick="Hapus(this)" data-id="{{$kv}}" class="btn btn-block btn-danger btn-xs">Hapus</button>
                                    </td>
                                    <td>{{(array_key_exists($vk, $CodesArr)) ? $CodesArr[$vk] : ''}} - {{(array_key_exists($vk, $DescArr)) ? $DescArr[$vk] : ''}}</td>
                                    <td>
                                      <input type='text' required name='sub_classification[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->sub_classification[$kv]}}">
                                    </td>
                                    <td>
                                      <select class="form-control" name="qualification[{{$vk}}][{{$kv}}]">
                                        <option value="1" {{$v->qualification[$kv] == "1" ? 'selected' : ''}}>Kecil</option>
                                        <option value="2" {{$v->qualification[$kv] == "2" ? 'selected' : ''}}>Non Kecil</option>
                                      </select>
                                    </td>
                                    <td>
                                      <input type='text' required name='no_letter[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->no_letter[$kv]}}">
                                      <input type='hidden' required name='id_classification[{{$vk}}][{{$kv}}]' value='{{$vk}}' class='form-control'>
                                    </td>
                                    <td>
                                      <input type='date' required  name='expired_date[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->expired_date[$kv]}}">
                                    </td>
                                  </tr>
                                  @endforeach
                                @endforeach
                              @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                      @php
                      @endphp
                  </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step3/'.$id) }}" class="btn btn-danger"><< Lisensi </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
        </form>
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
  <script type="text/javascript">
  var arr = [];

    function Hapus(a) {
      $('#tableClassification tbody tr.'+$(a).attr('data-id')).remove();
    }

      function pilihKlasifikasi(){
      var x = $('.x').val();
      if(x == ''){
        var nilai = 1;
      }else{
        var nilai = parseInt(x)+1;
      }
      var id_klasifikasi = $('.klasifikasi :selected').val();
      // alert(id_klasifikasi);
      var table ="";

      $.ajax({
          url: '{{URL::to('/vms/guest/register/classification')}}',
          method: 'POST',
          data: {"_token": "{{ csrf_token() }}", "id": id_klasifikasi},
          success: function (a) {
            var idx = $('.x').attr('value',nilai)
            var id = $('.x').val();
            table += "<tr>";
              table += "<td>";
                table += '<button type="button" name="button" onclick="Hapus(this)" data-id="'+a.klasifikasi.id+'" class="btn btn-block btn-danger btn-xs">Hapus</button>';
              table += "</td>";
              table += "<td>";
                table += a.klasifikasi.code+" - "+a.klasifikasi.description;
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='sub_classification["+a.klasifikasi.id+"]["+id+"]' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<select name='qualification["+a.klasifikasi.id+"]["+id+"]' class='form-control' required>";
                  table += "<option value='1'>Kecil</option>"
                  table += "<option value='2'>Non Kecil</option>"
                table += "</select>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='no_letter["+a.klasifikasi.id+"]["+id+"]' class='form-control'>";
                table += "<input type='hidden' required name='id_classification["+a.klasifikasi.id+"]["+id+"]' value='"+id_klasifikasi+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='date' required  name='expired_date["+a.klasifikasi.id+"]["+id+"]' class='form-control'>";
              table += "</td>";
            table += "</tr>";
            // if((!arr[id_klasifikasi])){
              $('#tableClassification tbody').append(table)
            // }
            // console.log(!arr);
            // arr[id_klasifikasi] = true;
            // console.log(!arr[id_klasifikasi]);
            // arr[c] = true;
          }
      });
    }
  </script>
@endsection
