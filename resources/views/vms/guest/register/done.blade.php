@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            <span class="caption-subject theme-font bold uppercase">TERIMA KASIH</span>
          </div>
        </div>
        <div class="portlet-body">
            <div class="tab-content">
                    <div>
                      @if(session()->has('message'))
                          {{-- <div class="alert alert-success"> --}}
                            {{-- <button type="button" class="close" data-dismiss="alert">x</button> --}}
                              {{ session()->get('message') }}
                          {{-- </div> --}}
                      @endif

                    </div>
            </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  <script type="text/javascript">
  var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

  function printDiv()
  {

    var divToPrint=document.getElementById('DivIdToPrint');

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);

  }
  </script>
@endsection
