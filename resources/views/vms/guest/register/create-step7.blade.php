@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Tenaga Ahli</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step7/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Nama</label>
                          <div class="col-md-10">
                            <input type="text" name="name" value="" class="form-control name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Tanggal Lahir</label>
                          <div class="col-md-10">
                            <input type="date" name="ttl" value="" class="form-control ttl">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Alamat </label>
                          <div class="col-md-10">
                            <input type="text" name="alamat" value="" class="form-control alamat">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Jenis Kelamin </label>
                          <div class="col-md-10">
                            <select class="form-control jk" name="jk">
                              <option value="L">Laki-laki</option>
                              <option value="P">Perempuan</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Pendidikan </label>
                          <div class="col-md-10">
                            <input type="text" name="pendidikan" value="" class="form-control pendidikan">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Warga Negara </label>
                          <div class="col-md-10">
                            <input type="text" name="wn" value="" class="form-control wn">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Pengalaman (tahun)</label>
                          <div class="col-md-10">
                            <input type="text" name="pengalaman" value="" class="form-control pengalaman">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Email </label>
                          <div class="col-md-10">
                            <input type="email" name="email" value="" class="form-control email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Keahlian </label>
                          <div class="col-md-10">
                            <input type="text" name="keahlian" value="" class="form-control keahlian">
                          </div>
                        </div>
                        <div class="col-md-offset-5 col-md-3">
                          <button type="button" class="btn btn-block btn-success" onclick="addPengurus()"  name="button">Tambah</button>
                        </div>
                        <br>
                        <br>
                        <br>
                        @php
                          // echo "<pre>";
                          // print_r(count($vendorClassification));
                          // echo "</pre>";
                        @endphp
                        <div class="table-responsive">
                          <table class="table table-striped" id="tableStaffAhli">
                            <thead>
                              <tr>
                                <th width="10%;">Hapus</th>
                                <th>Nama</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>
                                <th>Jenis Kelamin</th>
                                <th>Pendidikan</th>
                                <th>Warga Negara</th>
                                <th>Pengalaman (tahun)</th>
                                <th>Email</th>
                                <th>Keahlian</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if ($vendorExpertStaff != null)
                                  @foreach ($vendorExpertStaff as $k => $v)
                                    @php
                                    $idNya = $k;
                                    @endphp
                                    <tr class="{{$k}}">
                                      <td>
                                        <button type="button" name="button" onclick="Hapus(this)" data-id="{{$k}}" class="btn btn-block btn-danger btn-xs">Hapus</button>
                                      </td>
                                      <td><input type='text' required name='name_vendor_expert_staff[{{$k}}]' value='{{$v->name_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='birth_vendor_expert_staff[{{$k}}]' value='{{$v->birth_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='address_vendor_expert_staff[{{$k}}]' value='{{$v->address_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='gender_vendor_expert_staff[{{$k}}]' value='{{$v->gender_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='education_vendor_expert_staff[{{$k}}]' value='{{$v->education_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='nationallty_vendor_expert_staff[{{$k}}]' value='{{$v->nationallty_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='experience_vendor_expert_staff[{{$k}}]' value='{{$v->experience_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='email_vendor_expert_staff[{{$k}}]' value='{{$v->email_vendor_expert_staff}}' class='form-control'></td>
                                      <td><input type='text' required name='expertise_vendor_expert_staff[{{$k}}]' value='{{$v->expertise_vendor_expert_staff}}' class='form-control'></td>
                                    </tr>
                                  @endforeach
                                @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                      @php
                      @endphp
                  </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step6/'.$id) }}" class="btn btn-danger"><< Pengurus </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
        </form>
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
  <script type="text/javascript">
  var arr = [];

    function Hapus(a) {
      $('#tableStaffAhli tbody tr.'+$(a).attr('data-id')).remove();
    }

      function addPengurus(){
      var nama = $('.name').val();
      var ttl = $('.ttl').val();
      var alamat = $('.alamat').val();
      var jk = $('.jk').val();
      var pendidikan = $('.pendidikan').val();
      var wn = $('.wn').val();
      var pengalaman = $('.pengalaman').val();
      var email = $('.email').val();
      var keahlian = $('.keahlian').val();
      var x = $('.x').val();
      if(x == ''){
        var nilai = 1;
      }else{
        var nilai = parseInt(x)+1;
      }
      var id_klasifikasi = $('.klasifikasi :selected').val();
      var table ="";

            var idx = $('.x').attr('value',nilai)
            var id = $('.x').val();
            table += "<tr class='"+nilai+"'>";
              table += "<td>";
                table += '<button type="button" name="button" onclick="Hapus(this)" data-id="'+nilai+'" class="btn btn-block btn-danger btn-xs">Hapus</button>';
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='name_vendor_expert_staff["+id+"]' value='"+nama+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='birth_vendor_expert_staff["+id+"]' value='"+ttl+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='address_vendor_expert_staff["+id+"]' value='"+alamat+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                // table += "<input type='text' required name='gender_vendor_expert_staff["+id+"]' value='"+jk+"' class='form-control'>";
                table += "<select name='gender_vendor_expert_staff["+id+"]' class='form-control'>";
                  table += (jk == 'L') ? "<option value='L'>Laki-laki</option>" : "<option value='L'>Laki-laki</option>";
                  table += (jk == 'P') ? "<option value='P'>Perempuan</option>" : "<option value='P'>Perempuan</option>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='education_vendor_expert_staff["+id+"]' value='"+pendidikan+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='nationallty_vendor_expert_staff["+id+"]' value='"+wn+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='experience_vendor_expert_staff["+id+"]' value='"+pengalaman+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='email_vendor_expert_staff["+id+"]' value='"+email+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                table += "<input type='text' required name='expertise_vendor_expert_staff["+id+"]' value='"+keahlian+"' class='form-control'>";
              table += "</td>";
            table += "</tr>";
              $('#tableStaffAhli tbody').append(table)
              $('.name').val('');
              $('.ttl').val('');
              $('.alamat').val('');
              $('.jk').val('');
              $('.pendidikan').val('');
              $('.wn').val('');
              $('.pengalaman').val('');
              $('.email').val('');
              $('.keahlian').val('');
              $('.name').focus();
    }
  </script>
@endsection
