@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Dokumen</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        @php
                          $date = date('Ymd');
                        @endphp
                        <div class="">
                          <table class="table table-striped" id="tableStaffAhli">
                            <thead>
                              <tr>
                                <th>DOKUMEN</th>
                                <th>UNGGAH</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Perusahaan</strong> </td>
                              </tr>
                              <tr>
                                  <td> <strong>LOGO</strong> </td>
                                  <td>
                                    @if ((empty($vendorDocLogo))||($vendorDocLogo->file == null))
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/logo/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <input type="file" name="logo" required class="form-control" value="" accept="image/jpeg" placeholder="max size 1mb">
                                            <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                                          </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                            </div>
                                        </div>
                                      </div>
                                      </td>
                                    </form>
                                  @else
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/logo/delete/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\logo/'.$date.'/'.$vendorDocLogo->file)}}">{{$vendorDocLogo->file}}</a>
                                          </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                            </div>
                                        </div>
                                      </div>
                                      </td>
                                    </form>
                                  @endif
                              </tr>
                              <tr>
                                <td> <strong>NPWP</strong> </td>
                                <td>
                                  @if ((empty($vendorDocNpwp)) || ($vendorDocNpwp->file == null))
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/npwp/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <input type="file" required name="npwp" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                            <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                          </div>
                                          <div class="col-xs-2">
                                            <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                          </div>
                                        </div>
                                      </div>
                                    </td>
                                  </form>
                                  @else
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/npwp/delete/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\npwp/'.$date.'/'.$vendorDocNpwp->file)}}">{{$vendorDocNpwp->file}}</a>
                                          </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                            </div>
                                        </div>
                                      </div>
                                      </td>
                                    </form>
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td> <strong>AKTA</strong> </td>
                                <td>
                                  @if ((empty($vendorDocAkta)) || ($vendorDocAkta->file == null))
                                  <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/akta/'.$id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                      <div class="col-xs-12">
                                        <div class="col-xs-10">
                                          <input type="file" required name="akta" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                          <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                        </div>
                                        <div class="col-xs-2">
                                          <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                        </div>
                                      </div>
                                    </div>
                                    </td>
                                  </form>
                                @else
                                  <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/akta/delete/'.$id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                      <div class="col-xs-12">
                                        <div class="col-xs-10">
                                          <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\akta/'.$date.'/'.$vendorDocAkta->file)}}">{{$vendorDocAkta->file}}</a>
                                        </div>
                                          <div class="col-xs-2">
                                            <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                          </div>
                                      </div>
                                    </div>
                                    </td>
                                  </form>
                                @endif
                                </td>
                              </tr>
                              @foreach ($surat_perusahaan as $k => $v)
                                <tr>
                                  <td><strong>{{$v->name_vendor_business_license}}</strong></td>
                                  <td>
                                    @if ((empty($vendorDocSurat[$v->id][0]['file'])) ||  (empty($vendorDocSurat[$v->id][0]['file'])) || ($vendorDocSurat[$v->id][0]['file'] == null))
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/surat/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <input type="hidden" name="id_surat" class="form-control" value="{{$v->id}}">
                                            <input type="file" required name="surat" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                            <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                          </div>
                                          <div class="col-xs-2">
                                            <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                          </div>
                                        </div>
                                      </div>
                                      </td>
                                    </form>
                                  @else
                                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/surat/delete/'.$id) }}" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="col-xs-10">
                                                <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\surat/'.$date.'/'.$vendorDocSurat[$v->id][0]['file'])}}">{{$vendorDocSurat[$v->id][0]['file']}}</a>
                                          </div>
                                            <div class="col-xs-2">
                                              <input type="hidden" name="id_index" value="{{$v->id}}">
                                              <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                            </div>
                                        </div>
                                      </div>
                                      </td>
                                    </form>
                                  @endif
                                  </td>
                                </tr>
                              @endforeach

                              @if ($vendorOwner != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Pemilik</strong> </td>
                                </tr>
                                @foreach ($vendorOwner as $k => $v)
                                  <tr>
                                    <td><strong>{{$v->name_vendor_owner}}</strong></td>
                                    <td>
                                      @if ((empty($vendorDocPemilik[$k][0]['file'])) || ($vendorDocPemilik[$k][0]['file'] == null))
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/pemilik/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                              <input type="hidden" name="id_pemilik" value="{{$k}}">
                                              <input type="file" required name="pemilik" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                              <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                            </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                            </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @else
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/pemilik/delete/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                                  <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\pemilik/'.$date.'/'.$vendorDocPemilik[$k][0]['file'])}}">{{$vendorDocPemilik[$k][0]['file']}}</a>
                                            </div>
                                              <div class="col-xs-2">
                                                <input type="hidden" name="id_index" value="{{$k}}">
                                                <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                              </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @endif
                                    </td>
                                  </tr>
                                @endforeach
                              @endif
                              @if ($vendorAdministrators != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Pengurus</strong> </td>
                                </tr>
                                @foreach ($vendorAdministrators as $k => $v)
                                  <tr>
                                    <td><strong>{{$v->name_name_vendor_administrators}}</strong></td>
                                    <td>
                                      @if ((empty($vendorDocPengurus[$k][0]['file'])) || ($vendorDocPengurus[$k][0]['file'] == null))
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/pengurus/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                              <input type="hidden" name="id_pengurus" value="{{$k}}">
                                              <input type="file" required name="pengurus" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                              <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                            </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                            </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @else
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/pengurus/delete/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                                  <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\pengurus/'.$date.'/'.$vendorDocPengurus[$k][0]['file'])}}">{{$vendorDocPengurus[$k][0]['file']}}</a>
                                            </div>
                                              <div class="col-xs-2">
                                                <input type="hidden" name="id_index" value="{{$k}}">
                                                <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                              </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @endif
                                    </td>
                                  </tr>
                                @endforeach
                              @endif
                              @if ($vendorExpertStaff != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Tenaga Ahli</strong> </td>
                                </tr>
                                @foreach ($vendorExpertStaff as $k => $v)
                                  <tr>
                                    <td><strong>{{$v->name_vendor_expert_staff}}</strong></td>
                                    <td>
                                      @if ((empty($vendorDocTa[$k][0]['file'])) || ($vendorDocTa[$k][0]['file'] == null))
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/tenaga_ahli/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                              <input type="hidden" name="id_tenaga_ahli" value="{{$k}}">
                                              <input type="file" required name="tenaga_ahli" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                              <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                            </div>
                                            <div class="col-xs-2">
                                              <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                            </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @else
                                      <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/tenaga_ahli/delete/'.$id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="col-xs-10">
                                                  <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\tenaga_ahli/'.$date.'/'.$vendorDocTa[$k][0]['file'])}}">{{$vendorDocTa[$k][0]['file']}}</a>
                                            </div>
                                              <div class="col-xs-2">
                                                <input type="hidden" name="id_index" value="{{$k}}">
                                                <button type="submit" name="button" class="btn btn-danger btn-block">Hapus</button>
                                              </div>
                                          </div>
                                        </div>
                                        </td>
                                      </form>
                                    @endif
                                @endforeach
                              </tr>
                              @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                      @php
                        $pemilik = count($vendorOwner);
                        $pengurus = count($vendorAdministrators);
                        $tenaga = count($vendorExpertStaff);
                        $surat = count($surat_perusahaan);
                        $total = 3+$pemilik+$pengurus+$tenaga+$surat;
                        $docpemilik = ($vendorDocPemilik == null) ? '0' : count($vendorDocPemilik);
                        $docpengurus = ($vendorDocPengurus == null) ? '0' : count($vendorDocPengurus);
                        $doctenaga = ($vendorDocTa == null) ? '0' : count($vendorDocTa);
                        $docsurat = ($vendorDocSurat == null) ? '0' : count($vendorDocSurat);
                        $totDoc = 3+$docpemilik+$docpengurus+$doctenaga+$docsurat;
                      @endphp
                    </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step7/'.$id) }}" class="btn btn-danger"><< Tenaga Ahli </a>
                        @if (($pemilik <= $docpemilik) && ($pengurus <= $docpengurus) && ($tenaga <= $doctenaga) && ($surat <= $docsurat))
                          <a href="{{ URL::to('/vms/guest/register/review/'.$id) }}" class="btn btn-primary">Selanjutnya >> </a>
                        @endif
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
@endsection
