@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">pemilik</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step5/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Nama Pemilik</label>
                          <div class="col-md-10">
                            <input type="text" name="name" value="" class="form-control name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Ktp Pemilik</label>
                          <div class="col-md-10">
                            <input type="number" name="ktp" maxlength="16"  value="" class="form-control ktp">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification" class="col-md-2">Alamat Pemilik</label>
                          <div class="col-md-10">
                            <input type="text" name="alamat" value="" class="form-control alamat">
                          </div>
                        </div>
                        <div class="col-md-offset-5 col-md-3">
                          <button type="button" class="btn btn-block btn-success" onclick="addPemilik()"  name="button">Tambah</button>
                        </div>
                        <br>
                        <br>
                        <br>
                        @php
                          // echo "<pre>";
                          // print_r(count($vendorClassification));
                          // echo "</pre>";
                        @endphp
                        <div class="table-responsive">
                          <table class="table table-striped" id="tableOwner">
                            <thead>
                              <tr>
                                <th width="10%;">Hapus</th>
                                <th>Nama</th>
                                <th>Ktp</th>
                                <th>Alamat</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if ($vendorOwner != null)
                                  @foreach ($vendorOwner as $k => $v)
                                    @php
                                    $idNya = $k;
                                    @endphp
                                    <tr class="{{$k}}">
                                      <td>
                                        <button type="button" name="button" onclick="Hapus(this)" data-id="{{$k}}" class="btn btn-block btn-danger btn-xs">Hapus</button>
                                      </td>
                                      <td><input type='text' required name='name_vendor_owner[{{$k}}]' value='{{$v->name_vendor_owner}}' class='form-control'></td>
                                      <td><input type='text' required name='ktp_vendor_owner[{{$k}}]' value='{{$v->ktp_vendor_owner}}' class='form-control'></td>
                                      <td><input type='text' required name='address_vendor_owner[{{$k}}]' value='{{$v->address_vendor_owner}}' class='form-control'></td>
                                    </tr>
                                  @endforeach
                                @endif
                              {{-- @php
                              $CodesArr = $klasifikasi->pluck('code','id')->toArray();
                              $DescArr = $klasifikasi->pluck('description','id')->toArray();
                              @endphp
                              @if ($vendorClassification != null)
                                @foreach ($vendorClassification as $k => $v)
                                  @foreach ($v->id_classification as $kv => $vk)
                                  @php
                                  $idNya = $kv;
                                  @endphp
                                  <tr class="{{$kv}}">
                                    <td>
                                      <button type="button" name="button" onclick="Hapus(this)" data-id="{{$kv}}" class="btn btn-block btn-danger btn-xs">Hapus</button>
                                    </td>
                                    <td>{{(array_key_exists($vk, $CodesArr)) ? $CodesArr[$vk] : ''}} - {{(array_key_exists($vk, $DescArr)) ? $DescArr[$vk] : ''}}</td>
                                    <td>
                                      <input type='text' required name='sub_classification[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->sub_classification[$kv]}}">
                                    </td>
                                    <td>
                                      <select class="form-control" name="qualification[{{$vk}}][{{$kv}}]">
                                        <option value="1" {{$v->qualification[$kv] == "1" ? 'selected' : ''}}>Kecil</option>
                                        <option value="2" {{$v->qualification[$kv] == "2" ? 'selected' : ''}}>Non Kecil</option>
                                      </select>
                                    </td>
                                    <td>
                                      <input type='text' required name='no_letter[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->no_letter[$kv]}}">
                                      <input type='hidden' required name='id_classification[{{$vk}}][{{$kv}}]' value='{{$vk}}' class='form-control'>
                                    </td>
                                    <td>
                                      <input type='date' required  name='expired_date[{{$vk}}][{{$kv}}]' class='form-control' value="{{$v->expired_date[$kv]}}">
                                    </td>
                                  </tr>
                                  @endforeach
                                @endforeach
                              @endif
                            </tbody> --}}
                          </table>
                        </div>
                      </div>
                      @php
                      @endphp
                  </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step4/'.$id) }}" class="btn btn-danger"><< Klasifikasi </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
        </form>
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
  <script type="text/javascript">
  var arr = [];

    function Hapus(a) {
      $('#tableOwner tbody tr.'+$(a).attr('data-id')).remove();
    }

      function addPemilik(){
      var nama = $('.name').val();
      var ktp = $('.ktp').val();
      var alamat = $('.alamat').val();
      var x = $('.x').val();
      if(x == ''){
        var nilai = 1;
      }else{
        var nilai = parseInt(x)+1;
      }
      var id_klasifikasi = $('.klasifikasi :selected').val();
      var table ="";

            var idx = $('.x').attr('value',nilai)
            var id = $('.x').val();
            table += "<tr class='"+nilai+"'>";
              table += "<td>";
                table += '<button type="button" name="button" onclick="Hapus(this)" data-id="'+nilai+'" class="btn btn-block btn-danger btn-xs">Hapus</button>';
              table += "</td>";
              table += "<td>";
                // table += nama;
                table += "<input type='text' required name='name_vendor_owner["+id+"]' value='"+nama+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                // table += ktp;
                table += "<input type='text' required name='ktp_vendor_owner["+id+"]' value='"+ktp+"' class='form-control'>";
              table += "</td>";
              table += "<td>";
                // table += alamat;
                table += "<input type='text' required name='address_vendor_owner["+id+"]' value='"+alamat+"' class='form-control'>";
              table += "</td>";
            table += "</tr>";
              $('#tableOwner tbody').append(table)
              $('.name').val('');
              $('.ktp').val('');
              $('.alamat').val('');
              $('.name').focus();
    }
  </script>
@endsection
