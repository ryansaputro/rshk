@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Lisensi Perusahaan</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step3/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                <div class="tab-pane active" id="portlet_tab1">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        @foreach($surat_perusahaan as $k => $v)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$v->id}}" aria-expanded="true"><strong>{{$v->name_vendor_business_license}}</strong></a>
                                </h4>
                            </div>
                            <div id="collapse{{$v->id}}" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped" id="table_{{$v->id}}">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor Surat</th>
                                                        <th>Instansi Pemberi</th>
                                                        <th>Tanggal Diterbitkan</th>
                                                        <th>Tanggal Berakhir</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="{{ $errors->has('reference_number') ? ' has-error' : '' }}">
                                                          <input type="text" value="{{$vendorLisensi[$k]->reference_number or ''}}" name="reference_number[{{$v->id}}]" required="" class="form-control">
                                                          @if ($errors->has('reference_number'))
                                                              <span class="help-block">
                                                                  <strong>{{ $errors->first('reference_number') }}</strong>
                                                              </span>
                                                          @endif
                                                        </td>
                                                        <td class="{{ $errors->has('giver_agency') ? ' has-error' : '' }}">
                                                          <input type="text" value="{{$vendorLisensi[$k]->giver_agency or ''}}" name="giver_agency[{{$v->id}}]" required="" class="form-control">
                                                          @if ($errors->has('giver_agency'))
                                                              <span class="help-block">
                                                                  <strong>{{ $errors->first('giver_agency') }}</strong>
                                                              </span>
                                                          @endif
                                                        </td>
                                                        <td class="{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                                          <input type="date" value="{{$vendorLisensi[$k]->start_date or ''}}" name="start_date[{{$v->id}}]" required="" class="form-control">
                                                          @if ($errors->has('start_date'))
                                                              <span class="help-block">
                                                                  <strong>{{ $errors->first('start_date') }}</strong>
                                                              </span>
                                                          @endif
                                                        </td>
                                                        <td class="{{ $errors->has('valid_until') ? ' has-error' : '' }}">
                                                          <input type="date" value="{{$vendorLisensi[$k]->valid_until or ''}}" name="valid_until[{{$v->id}}]" required="" class="form-control">
                                                          @if ($errors->has('valid_until'))
                                                              <span class="help-block">
                                                                  <strong>{{ $errors->first('valid_until') }}</strong>
                                                              </span>
                                                          @endif
                                                        </td>
                                                        {{-- <td class="">
                                                          <select  class="form-control qualification qualification_{{$v->id}}" name="qualification[{{$v->id}}][]" required="">
                                                            <option value="kecil">Kecil</option>
                                                            <option value="non kecil">Non Kecil</option>
                                                          </select>
                                                        </td>
                                                        <td class=""> --}}
                                                          {{-- <select  name="qualification[{{$v->id}}][]" required="" class="form-control" tabindex="-1" aria-hidden="true">
                                                            <option value="kecil">Kecil</option>
                                                            <option value="non kecil">Non Kecil</option>
                                                          </select> --}}
                                                          {{-- <button type="button" class="btn btn-info btn-sm classification_{{$v->id}}" data-id="{{$v->id}}" data-name="{{$v->name_vendor_business_license}}" onclick="classification(this)">Qulification</button> --}}
                                                          {{-- <select data-placeholder="Klasifikasi Bidang Usaha..." class="chosen-select form-control" multiple tabindex="4">
                                                            <option value=""></option>
                                                            <option value="G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN">G47726 - PERDAGANGAN ECERAN ALAT LABORATORIUM, FARMASI DAN KESEHATAN</option>
                                                          </select> --}}
                                                          {{-- <input type="text" name="classification[{{$v->id}}][]" required="" class="form-control"> --}}
                                                        {{-- </td> --}}
                                                        {{-- <td class="{{ $errors->has('lisensi_id') ? ' has-error' : '' }}"> <input type="file" name="lisensi_id[{{$v->id}}]" value="" required class="form-control" accept="application/pdf" > </td> --}}
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step2/'.$id) }}" class="btn btn-danger"><< Akta </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                  </div>
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
   {{-- <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
           <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>
   </div> --}}
   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
@endsection
