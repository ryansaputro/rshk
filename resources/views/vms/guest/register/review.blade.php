@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            <span class="caption-subject theme-font bold uppercase">ULASAN</span>
          </div>
        </div>
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/formRegister/store/'.$user->activation_key) }}">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-xs-12">
                        @php
                          // $data = (count($vendorDoc))-1;
                          $date = date('Ymd');
                        @endphp
                        <div id='DivIdToPrint'>
                          <table class="table table-striped" id="dataPerusahaan">
                            <tr>
                              <th colspan="2" style="background-color:#ddd; text-align:left;">DATA PERUSAHAAN</th>
                            </tr>
                            <tr>
                              <td>Nama Perusahaan</td>
                              <td>{{{ $user->name or '' }}}</td>
                            </tr>
                            <tr>
                              <td>NPWP</td>
                              <td>{{{ $identitas->npwp or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Alamat</td>
                              <td>{{{ $identitas->address or '' }}}, {{$list_kota[$list_propinsi[$identitas->province]][$identitas->city]}} - {{$list_propinsi[$identitas->province]}}</td>
                            </tr>
                            <tr>
                              <td>Kode Pos</td>
                              <td>{{{ $identitas->post_code or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Nama Pengguna</td>
                              <td>{{{  $user->username or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Nomor PKP</td>
                              <td>{{{ $identitas->pkp or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Telepon</td>
                              <td>{{{ $identitas->telephone or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Fax</td>
                              <td>{{{ $identitas->fax or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Hp</td>
                              <td>{{{ $identitas->mobile_phone or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Email</td>
                              <td>{{{ $identitas->email or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Bank</td>
                              <td>{{{ $identitas->bank or '' }}}</td>
                            </tr>
                            <tr>
                              <td>No Rekening</td>
                              <td>{{{ $identitas->no_rek or '' }}}</td>
                            </tr>
                            <tr>
                              <th colspan="2" style="background-color:#ddd; text-align:left;">AKTA PERUSAHAAN</th>
                            </tr>
                            <tr>
                              <td>No Akta</td>
                              <td>{{{ $vendorDeed->deed_number or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Tanggal</td>
                              <td>{{{ $vendorDeed->date or '' }}}</td>
                            </tr>
                            <tr>
                              <td>Notaris</td>
                              <td>{{{ $vendorDeed->notary_public or '' }}}</td>
                            </tr>
                            <tr>
                              <th colspan="2" style="background-color:#ddd; text-align:left;">LISENSI PERUSAHAAN</th>
                            </tr>
                            @foreach ($surat_perusahaan as $k => $v)
                              <tr>
                                <td colspan="2"><strong>{{$v->name_vendor_business_license}}</strong></td>
                              </tr>
                              <tr>
                                <td style="text-indent:5%;">Nomor</td>
                                <td>{{$vendorLisensi[$k]['id_vendor_business_license'] == $v->id ? $vendorLisensi[$k]['reference_number'] : ''}}</td>                              </tr>
                              </tr>
                              <tr>
                                <td style="text-indent:5%;">Instansi Pemberi</td>
                                <td>{{$vendorLisensi[$k]['id_vendor_business_license'] == $v->id ? $vendorLisensi[$k]['giver_agency'] : ''}}</td>                              </tr>
                              <tr>
                                <td style="text-indent:5%;">Tanggal Berlaku</td>
                                <td>{{$vendorLisensi[$k]['id_vendor_business_license'] == $v->id ? $vendorLisensi[$k]['valid_until'] : ''}}</td>                              </tr>
                              </tr>
                            @endforeach
                            <tr>
                              <th colspan="2" style="background-color:#ddd; text-align:left;">KLASIFIKASI PERUSAHAAN</th>
                            </tr>
                            {{-- @foreach ($klasifikasi as $k => $v)
                            <tr>
                              <td colspan="2"><strong>{{$v->code}} - {{$v->name}}</strong></td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Sub Bidang</td>
                              <td>{{$vendorClassification[0]['sub_classification'][$v->id]}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Kualifikasi Bidang</td>
                              <td>{{$vendorClassification[0]['qualification'][$v->id] == '1' ? 'Kecil' : 'Non Kecil'}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">No Surat Bidang</td>
                              <td>{{$vendorClassification[0]['no_letter'][$v->id]}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Tanggal Kadaluarsa</td>
                              <td>{{$vendorClassification[0]['expired_date'][$v->id]}}</td>
                            </tr>
                          @endforeach --}}
                          <tr>
                            <th colspan="2" style="background-color:#ddd; text-align:left;">PEMILIK PERUSAHAAN</th>
                          </tr>
                          @foreach ($vendorOwner as $k => $v)
                            <tr>
                              <td style="text-indent:5%;">Nama Pemilik</td>
                              <td>{{$v->name_vendor_owner}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">KTP Pemilik</td>
                              <td>{{$v->ktp_vendor_owner}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Alamat Pemilik</td>
                              <td>{{$v->address_vendor_owner}}</td>
                            </tr>
                          @endforeach
                          <tr>
                            <th colspan="2" style="background-color:#ddd; text-align:left;">PENGURUS PERUSAHAAN</th>
                          </tr>
                          @foreach ($vendorAdministrators as $k => $v)
                            <tr>
                              <td colspan="2"><strong>Pengurus {{$k+1}}</strong></td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Nama Pengurus</td>
                              <td>{{$v->name_name_vendor_administrators}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Jabatan Pengurus</td>
                              <td>{{$v->ktp_name_vendor_administrators}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">KTP Pengurus</td>
                              <td>{{$v->address_name_vendor_administrators}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Alamat Pengurus</td>
                              <td>{{$v->position_name_vendor_administrators}}</td>
                            </tr>
                          @endforeach
                          <tr>
                            <th colspan="2" style="background-color:#ddd; text-align:left;">TENAGA AHLI PERUSAHAAN</th>
                          </tr>
                          @foreach ($vendorExpertStaff as $k => $v)
                            <tr>
                              <td colspan="2"><strong>Tenaga Ahli {{$k+1}}</strong></td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Nama Tenaga Ahli</td>
                              <td>{{$v->name_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Tanggal Lahir Tenaga Ahli</td>
                              <td>{{$v->birth_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Alamat Tenaga Ahli</td>
                              <td>{{$v->address_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Kelamin Tenaga Ahli</td>
                              <td>{{$v->gender_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Pendidikan Tenaga Ahli</td>
                              <td>{{$v->education_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Kebangsaan Tenaga Ahli</td>
                              <td>{{$v->nationallty_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Pengalaman Tenaga Ahli (tahun)</td>
                              <td>{{$v->experience_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Email Tenaga Ahli</td>
                              <td>{{$v->email_vendor_expert_staff}}</td>
                            </tr>
                            <tr>
                              <td style="text-indent:5%;">Keahlian Tenaga Ahli</td>
                              <td>{{$v->expertise_vendor_expert_staff}}</td>
                            </tr>
                          @endforeach
                          </table>
                        </div>
                      </div>
                      @php
                        // echo "<pre>";
                        // print_r($vendorLisensi);
                        // echo "</pre>";
                      @endphp
                    </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step8/'.$id) }}" class="btn btn-danger"><< Dokumen </a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <input type='button' id='btn' value='Print' class="btn btn-success" onclick='printDiv();'>
                      </div>
                    </form>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  <script type="text/javascript">
  var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

  function printDiv()
  {

    var divToPrint=document.getElementById('DivIdToPrint');

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);

  }
  </script>
@endsection
