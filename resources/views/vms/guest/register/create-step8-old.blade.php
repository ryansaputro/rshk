@extends('vms.guest.layouts.app_new')

@section('title')
  VMS
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/style.css"> --}}
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/docsupport/prism.css">
  <link rel="stylesheet" href="/su_vms/assets/global/plugins/chosen/chosen.css">
  <style media="screen">
    .chosen-container {
      width: 100% !important;
    }
    .chosen-container-multi{
      width: 100% !important;
    }
  </style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  {{-- <div class="page-head">
    <div class="container-fluid"> --}}
      <!-- BEGIN PAGE TITLE -->
      {{-- <div class="page-title">
        <h1>VMS <small>Pendaftaran...</small></h1>
      </div> --}}
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      {{-- <div class="page-toolbar hide"> --}}
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      {{-- </div> --}}
      <!-- END PAGE TOOLBAR -->
    {{-- </div>
  </div> --}}

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <div class="caption">
            {{-- <i class="icon-speech theme-font"></i> --}}
            <span class="caption-subject theme-font bold uppercase">Dokumen</span>
            {{-- <span class="caption-helper">Pendaftaran</span> --}}
          </div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ URL::to('/vms/guest/register/create-step8/'.$id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet-body">
          <div class="tab-content">
                  <div>
                    <div class="row">
                      <div class="col-xs-12">
                        @php
                          $data = (count($vendorDoc))-1;
                          $date = date('Ymd');
                        @endphp
                        <div class="table-responsive">
                          <table class="table table-striped" id="tableStaffAhli">
                            <thead>
                              <tr>
                                {{-- <th width="10%;">Hapus</th> --}}
                                <th>Dokumen</th>
                                <th>Upload</th>
                                <th>File terupload</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Perusahaan</strong> </td>
                              </tr>
                              <tr>
                                <td> <strong>LOGO</strong> </td>
                                <td> <input type="file" name="logo" class="form-control" value="" accept="image/jpeg" placeholder="max size 1mb"> <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small></td>
                                <td>
                                  @if ($vendorDoc[$data]->file[3])
                                    <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/logo/'.$date.'/'.$vendorDoc[$data]->file[3])}}">{{$vendorDoc[$data]->file[3]}}</a>
                                  @else
                                    -
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td> <strong>NPWP</strong> </td>
                                <td>
                                  <input type="file" name="npwp" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                  <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                </td>
                                <td>
                                  @if ($vendorDoc[$data]->file[4])
                                    <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/npwp/'.$date.'/'.$vendorDoc[$data]->file[4])}}">{{$vendorDoc[$data]->file[4]}}</a>
                                  @else
                                    -
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td> <strong>AKTA</strong> </td>
                                <td>
                                  <input type="file" name="akta" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                  <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                </td>
                                <td>
                                  @if ($vendorDoc[$data]->file[5])
                                    <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/akta/'.$date.'/'.$vendorDoc[$data]->file[5])}}">{{$vendorDoc[$data]->file[5]}}</a>
                                  @else
                                    -
                                  @endif
                                </td>
                              </tr>
                              @foreach ($surat_perusahaan as $k => $v)
                                <tr>
                                  <td><strong>{{$v->name_vendor_business_license}}</strong></td>
                                  <td>
                                    <input type="file" name="surat[{{$v->id}}]" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                    <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                  </td>
                                  <td>
                                    @if ($vendorDoc[$data]->file[$k] != null)
                                      <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/surat/'.$date.'/'.$vendorDoc[$data]->file[$k])}}">{{$vendorDoc[$data]->file[$k]}}</a>
                                    @else
                                      -
                                    @endif
                                  </td>
                                </tr>
                              @endforeach
                              @if ($vendorOwner != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Pemilik</strong> </td>
                                </tr>
                                @foreach ($vendorOwner as $k => $v)
                                  @php
                                    $idx = $k+6;
                                  @endphp
                                  <tr>
                                    <td><strong>{{$v->name_vendor_owner}}</strong></td>
                                    <td>
                                      <input type="file" name="pemilik[]" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                      <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                    </td>
                                    <td>
                                      @if ($vendorDoc[$data]->file[$idx] != null)
                                        <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/pemilik/'.$date.'/'.$vendorDoc[$data]->file[$idx])}}">{{$vendorDoc[$data]->file[$idx]}}</a>
                                      @else
                                        -
                                      @endif
                                    </td>
                                  </tr>
                                  @php
                                    $idx++;
                                  @endphp
                                @endforeach
                              @endif
                              @if ($vendorAdministrators != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Pengurus</strong> </td>
                                </tr>
                                @foreach ($vendorAdministrators as $k => $v)
                                  @php
                                    $idx = $k+8;
                                  @endphp
                                  <tr>
                                    <td><strong>{{$v->name_name_vendor_administrators}}</strong></td>
                                    <td>
                                      <input type="file" name="pengurus[]" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                      <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                    </td>
                                    <td>
                                      @if ($vendorDoc[$data]->file[$idx] != null)
                                        <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/pengurus/'.$date.'/'.$vendorDoc[$data]->file[$idx])}}">{{$vendorDoc[$data]->file[$idx]}}</a>
                                      @else
                                        -
                                      @endif
                                    </td>
                                  </tr>
                                  @php
                                    $idx++;
                                  @endphp
                                @endforeach
                              @endif
                              @if ($vendorExpertStaff != null)
                                <tr>
                                  <td colspan="3" style="text-align:center; background-color:#ddd;"> <strong>Dokumen Tenaga Ahli</strong> </td>
                                </tr>
                                @foreach ($vendorExpertStaff as $k => $v)
                                  <tr>
                                    <td><strong>{{$v->name_vendor_expert_staff}}</strong></td>
                                    <td>
                                      <input type="file" name="tenaga_ahli[]" class="form-control" value="" accept="application/pdf" placeholder="max size 1mb">
                                      <small id="fileHelp" class="form-text text-muted">Please upload a valid pdf file. Size of pdf should not be more than 2MB.</small>
                                    </td>
                                    <td>
                                      @php
                                        $idx = $k+10;
                                      @endphp
                                      @if ($vendorDoc[$data]->file[$idx] != null)
                                        <a target="_blank" href="{{asset('assets/document/'.$idVendor.'\/tenaga_ahli/'.$date.'/'.$vendorDoc[$data]->file[$idx])}}">{{$vendorDoc[$data]->file[$idx]}}</a>
                                      @else
                                        -
                                      @endif
                                    </td>
                                  </tr>
                                  @php
                                    $idx++;
                                  @endphp

                                @endforeach
                              @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                      @php
                      @endphp
                  </div>
                      <div class="col-offset-9 col-xs-12" style="text-align:center;">
                        <a href="{{ URL::to('/vms/guest/register/create-step7/'.$id) }}" class="btn btn-danger"><< Tenaga Ahli </a>
                        <button type="submit" class="btn btn-primary">Selanjutnya >> </button>
                      </div>
                    </div> <!-- col.md.12 -->
                </div><!--/.tab-pane-->
          </div>
        </div>
                <div class="modal"></div>
                <input type="hidden" name="x" value="{{$idNya or ''}}" class="x">
        </form>
      </div>
    </div>
  </div>

   @endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
  {{-- <script type="text/javascript" src="/su_vms/assets/global/plugins/chosen/chosen.jquery.js"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script src="/su_vms/assets/global/plugins/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script> --}}
@endsection
