@extends('vms.guest.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small>Dashboard...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar hide">
        <!-- BEGIN THEME PANEL -->
        <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div>
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Page Content</span>
            <span class="caption-helper">weekly stats...</span>
          </div>
          <div class="actions hide">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
              <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
              <input type="radio" name="options" class="toggle" id="option1">Option 1</label>
              <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
              <input type="radio" name="options" class="toggle" id="option2">Option 2</label>
              <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
              <input type="radio" name="options" class="toggle" id="option2">Option 3</label>
            </div>
          </div>
        </div>
        <div class="portlet-body">
          Ongoing...
        </div>
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')

@endsection
