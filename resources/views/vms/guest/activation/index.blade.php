@extends('vms.guest.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small>Activation...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-offset-4 col-md-4" style="margin-top: 50px;">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper">Activation...</span>
          </div>
          <div class="actions">
          </div>
        </div>
        <div class="portlet-body">
          <form class="form-horizontal form-material" id="loginform" action="{{URL::to('activation/check')}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group m-t-40">
                <div class="col-xs-12">
                    <input id="activation_key" type="activation_key" class="form-control" name="activation_key" value="{{ old('activation_key') }}" required autofocus placeholder="Kode Aktivasi">
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Aktivasi</button>
                </div>
            </div>
          </form>
        </div><!--/.portlet-body--->
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')

@endsection
