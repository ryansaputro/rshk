Hello <b>{{ $name }}</b>,
<p>Demikian undangan untuk pengecekan berkas:</p>
  <center>
    <table style="width:100%;">
      <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td>{{$date}}</td>
      </tr>
      <tr>
        <td>Waktu</td>
        <td>:</td>
        <td>{{$time}}</td>
      </tr>
      <tr>
        <td>Tempat</td>
        <td>:</td>
        <td>{{$place}}</td>
      </tr>
    </table>
  </center>

<a href="{{$host}}">klik</a> link berikut untuk menyetujui kehadiran
Thank You,
<br/>
<i>Admin VMS</i>
