<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>Pusat Jantung Nasional RSHK - PT. Nusamart Aulia Mandiri</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="/assets/logo.png">
  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Pathway+Gothic+One|PT+Sans+Narrow:400+700|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->
  <!-- Global styles BEGIN -->
  <link href="/su_guest/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/su_guest/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/su_guest/assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
  <!-- Global styles END -->
  <!-- Page level plugin styles BEGIN -->
  <link href="/su_guest/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <!-- Page level plugin styles END -->
  <!-- Theme styles BEGIN -->
  <link href="/su_guest/assets/global/css/components.css" rel="stylesheet">
  <link href="/su_guest/assets/frontend/onepage/css/style.css" rel="stylesheet">
  <link href="/su_guest/assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
  <link href="/su_guest/assets/frontend/onepage/css/themes/blue.css" rel="stylesheet" id="style-color">
  <link href="/su_guest/assets/frontend/onepage/css/custom.css" rel="stylesheet">
  <!-- Theme styles END -->
</head>
<!--DOC: menu-always-on-top class to the body element to set menu on top -->
<body>
  <!-- BEGIN STYLE CUSTOMIZER -->
  <div class="color-panel hide">
    <div class="color-mode-icons icon-color"></div>
    <div class="color-mode-icons icon-color-close"></div>
    <div class="color-mode">
      <p>THEME COLOR</p>
      <ul class="inline">
        <li class="color-red current color-default" data-style="red"></li>
        <li class="color-blue" data-style="blue"></li>
        <li class="color-green" data-style="green"></li>
        <li class="color-orange" data-style="orange"></li>
        <li class="color-gray" data-style="gray"></li>
        <li class="color-turquoise" data-style="turquoise"></li>
      </ul>
      <p>MENU POSITION</p>
      <select class="form-control menu-pos">
        <option value="bottom">Bottom</option>
        <option value="top">Top</option>
      </select>
    </div>
  </div>
  <!-- END BEGIN STYLE CUSTOMIZER -->
  <!-- Header BEGIN -->
  <div class="header header-mobi-ext">
    <div class="container">
      <div class="row">
        <!-- Logo BEGIN -->
        <div class="col-md-2 col-sm-2">
          <a class="scroll site-logo" href="{!! route('x') !!}" style="padding-top: 21px;">
            {{-- <img src="/su_guest/assets/frontend/onepage/img/logo/blue.png" alt=""> --}}
            <h4 style="margin: 0px; font-size: 18px;">RSHK</h4>
          </a>
        </div>
        <!-- Logo END -->
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- Navigation BEGIN -->
        <div class="col-md-10 pull-right">
          <ul class="header-navigation">
            {{-- <li class="current"><a href="{!! route('x') !!}">HOME</a></li> --}}
            <li><a href="{!! route('x.auth') !!}">VMS</a></li>
            {{-- <li><a href="{!! route('x.vms.register') !!}">VMS REGISTER</a></li> --}}
            {{-- <li><a href="{!! route('x.vms.activation') !!}">VMS ACTIVATION</a></li> --}}
            {{-- <li><a href="{!! URL::to('vms/users/dashboard') !!}">VMS USER</a></li> --}}
            {{-- <li><a href="{!! URL::to('vms/admin/dashboard') !!}">VMS ADMIN</a></li> --}}
            <li><a href="{!! route('x.catalog.guest.item') !!}">CATALOG</a></li>
            <li><a href="{!! route('x.proposal') !!}">USULAN</a></li>
            {{-- <li><a href="{!! route('x.catalog.users') !!}">CATALOG USER</a></li> --}}
            {{-- <li><a href="{!! route('x.catalog.admin') !!}">CATALOG ADMIN</a></li> --}}
            {{-- <li><a href="{!! route('x.auth') !!}">LOGIN</a></li> --}}
          </ul>
        </div>
        <!-- Navigation END -->
      </div>
    </div>
  </div>
  <!-- Header END -->
  <!-- Promo block BEGIN -->
  <div class="promo-block" id="promo-block">
    <div class="tp-banner-container">
      <div class="tp-banner" >
        <ul>
          <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-1">
            <img src="/su_guest/assets/frontend/onepage/img/silder/slide1.png" alt="" data-bgfit="cover" style="opacity:0.4 !important;" data-bgposition="center center" data-bgrepeat="no-repeat">
            <div class="tp-caption large_text customin customout start"
              data-x="center"
              data-hoffset="0"
              data-y="center"
              data-voffset="60"
              data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="1000"
              data-start="500"
              data-easing="Back.easeInOut"
              data-endspeed="300">
              <div class="promo-like" style="background-color: white; padding: 25px;">
                {{-- <i class="fa fa-thumbs-up"></i> --}}
                <img src="/assets/logo.png" alt="" class="img-circle" style="height: 175px; border-radius: 75px;"/>
              </div>
              <div class="promo-like-text">
                <h2>Let's just do it</h2>
                <p>
                  PT. Nusamart Aulia Mandiri <br>
                  VMS - TENDER - INVENTORY - CATALOG - PAYMENT
                </p>
              </div>
            </div>
            <div class="tp-caption large_bold_white fade center"
              data-x="center"
              data-y="center"
              data-voffset="-110"
              data-speed="300"
              data-start="1700"
              data-easing="Power4.easeOut"
              data-endspeed="500"
              data-endeasing="Power1.easeIn"
              data-captionhidden="off"
              style="z-index: 6">
              Pusat Jantung Nasional <span>RSHK</span>
            </div>
          </li>
          <li data-transition="fadefromright" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-2">
            <img src="/su_guest/assets/frontend/onepage/img/silder/Slide2_bg.jpg" alt="slidebg2" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
            <div class="caption lft start"
              data-y="center"
              data-voffset="40"
              data-x="center"
              data-hoffset="-250"
              data-speed="600"
              data-start="500"
              data-easing="easeOutBack"><img src="/su_guest/assets/frontend/onepage/img/silder/Slide2_iphone_left.png" alt="">
            </div>
            <div class="caption lft start"
              data-y="center"
              data-voffset="130"
              data-x="center"
              data-hoffset="170"
              data-speed="600"
              data-start="1200"
              data-easing="easeOutBack"><img src="/su_guest/assets/frontend/onepage/img/silder/Slide2_iphone_right.png" alt="">
            </div>
            <div class="tp-caption large_bold_white fade"
              data-x="center"
              data-y="40"
              data-speed="300"
              data-start="1700"
              data-easing="Power4.easeOut"
              data-endspeed="500"
              data-endeasing="Power1.easeIn"
              data-captionhidden="off"
              style="z-index: 6">Extremely <span>Responsive</span> Design
            </div>
          </li>
          <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400" class="slider-item-3">
            <img src="https://www.hdwallpapers.in/download/2018_new_year_new_challenges-wide.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

            <div class="tp-caption tp-fade fadeout fullscreenvideo hide"
              data-x="0"
              data-y="0"
              data-speed="1000"
              data-start="1100"
              data-easing="Power4.easeOut"
              data-endspeed="1500"
              data-endeasing="Power4.easeIn"
              data-autoplay="true"
              data-autoplayonlyfirsttime="false"
              data-nextslideatend="true"
              data-forceCover="1"
              data-dottedoverlay="twoxtwo"
              data-aspectratio="16:9"
              data-forcerewind="on"
              style="z-index: 2">
              <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%">
                  {{-- <source src='/assets/me.mp4' type='video/mp4'> --}}
                  {{-- <source src='http://goodwebtheme.com/previewvideo/forest_edit.webm' type='video/webm'> --}}
                  {{-- <source src='http://goodwebtheme.com/previewvideo/forest_edit.ogv' type='video/ogg'> --}}
              </video>
            </div>
            <div class="tp-caption large_bold_white_25 customin customout tp-resizeme"
              data-x="center" data-hoffset="0"
              data-y="170"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="600"
              data-start="1400"
              data-easing="Power4.easeOut"
              data-endspeed="600"
              data-endeasing="Power0.easeIn"
              style="z-index: 3">
              EVERYTHNING WILL BE OKE <br> STAY POSITIVE
            </div>
            <div class="tp-caption medium_text_shadow customin customout tp-resizeme"
              data-x="center" data-hoffset="0"
              data-y="bottom" data-voffset="-140"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="600"
              data-start="1700"
              data-easing="Power4.easeOut"
              data-endspeed="600"
              data-endeasing="Power0.easeIn"
              style="z-index: 4">PT. Nusamart Aulia Mandiri
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Promo block END -->
  <a href="#promo-block" class="go2top scroll"><i class="fa fa-arrow-up"></i></a>
  <!--[if lt IE 9]>
  <script src="/su_guest/assets/global/plugins/respond.min.js"></script>
  <![endif]-->
  <!-- Load JavaScripts at the bottom, because it will reduce page load time -->
  <!-- Core plugins BEGIN (For ALL pages) -->
  <script src="/su_guest/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="/su_guest/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <script src="/su_guest/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <!-- Core plugins END (For ALL pages) -->
  <!-- BEGIN RevolutionSlider -->
  <script src="/su_guest/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <script src="/su_guest/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
  <script src="/su_guest/assets/frontend/onepage/scripts/revo-ini.js" type="text/javascript"></script>
  <!-- END RevolutionSlider -->
  <!-- Core plugins BEGIN (required only for current page) -->
  <script src="/su_guest/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
  <script src="/su_guest/assets/global/plugins/jquery.easing.js"></script>
  <script src="/su_guest/assets/global/plugins/jquery.parallax.js"></script>
  <script src="/su_guest/assets/global/plugins/jquery.scrollTo.min.js"></script>
  <script src="/su_guest/assets/frontend/onepage/scripts/jquery.nav.js"></script>
  <!-- Core plugins END (required only for current page) -->
  <!-- Global js BEGIN -->
  <script src="/su_guest/assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      Layout.init();
    });
  </script>
  <!-- Global js END -->
</body>
</html>
