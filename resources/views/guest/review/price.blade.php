@extends('guest.layouts.index')

@section('css')

@endsection

@section('xcss')

@endsection

@section('header')
  <h1>
    Price Review page
    <small>it all starts here</small>
  </h1>
  <ol class="hide breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Examples</a></li>
    <li class="active">Blank page</li>
  </ol>
@endsection

@section('content')
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List Proposal For Price Review</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped table-hover" style="width: 100%;">
        <thead>
          <tr>
            <th>Tools</th>
            <th>No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Total Volume</th>
            <th>Result Price</th>
            <th>Total</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          <td>1</td>
          <td>2</td>
          <td>3</td>
          <td>4</td>
          <td>5</td>
          <td>6</td>
          <td>7</td>
          <td>8</td>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer hide">
      <small>

      </small>
    </div>
    <!-- /.box-footer-->
  </div>
@endsection

@section('js')

@endsection

@section('xjs')

@endsection
