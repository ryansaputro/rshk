@extends('guest.layouts.index')

@section('css')

@endsection

@section('xcss')

@endsection

@section('header')
  <h1>
    Dashboard page
    <small>it all starts here</small>
  </h1>
  <ol class="hide breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Examples</a></li>
    <li class="active">Blank page</li>
  </ol>
@endsection

@section('content')
  
@endsection

@section('js')

@endsection

@section('xjs')

@endsection
