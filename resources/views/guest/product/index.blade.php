@extends('guest.layouts.index')

@section('css')

@endsection

@section('xcss')

@endsection

@section('header')
    <h1>
      Product page
      <small>it all starts here</small>
      <span class="pull-right">
        <button class="btn btn-xs bg-navy" type="button" name="button" data-toggle="modal" data-target="#modal_product_create">
          <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
        </button>
      </span>
    </h1>
    <ol class="hide breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">Blank page</li>
    </ol>
@endsection

@section('content')
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List Product</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover nowarp">
        <thead>
          <tr>
            <th class="hidden-xs">Tools</th>
            <th class="hidden-xs">No</th>
            <th class="hidden-xs">Code</th>
            <th>Name</th>
            <th class="">Description</th>
            <th>Specification</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="hidden-xs text-center">
              <div class="btn-group">
                <button class="btn btn-xs" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-navy" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-teal" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-purple" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-primary" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-success" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-info" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-danger" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-warning" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
              </div>
            </td>
            <td class="hidden-xs text-center">1</td>
            <td class="hidden-xs">42000000-AKS-000046003</td>
            <td>Baby Basket KA 13-01BSS</td>
            <td class=" text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_description">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
            <td class="text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_specification">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
          </tr>
          <tr>
            <td class="hidden-xs text-center">
              <div class="btn-group">
                <button class="btn btn-xs" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-navy" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-teal" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-purple" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-primary" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-success" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-info" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-danger" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-warning" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
              </div>
            </td>
            <td class="hidden-xs text-center">1</td>
            <td class="hidden-xs">42000000-AKS-000046003</td>
            <td>Baby Basket KA 13-01BSS</td>
            <td class=" text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_description">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
            <td class="text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_specification">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
          </tr>
          <tr>
            <td class="hidden-xs text-center">
              <div class="btn-group">
                <button class="btn btn-xs" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-navy" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-teal" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs bg-purple" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-primary" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-success" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-info" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-danger" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button class="btn btn-xs btn-warning" type="button" name="button" data-toggle="tooltip" title="x">
                  <i class="fa fa-circle-o"></i>
                </button>
              </div>
            </td>
            <td class="hidden-xs text-center">1</td>
            <td class="hidden-xs">42000000-AKS-000046003</td>
            <td>Baby Basket KA 13-01BSS</td>
            <td class=" text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_description">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
            <td class="text-center">
              <button class="btn btn-xs" type="button" name="button" data-toggle="modal" data-target="#modal_specification">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer hide">
      <small>
        
      </small>
    </div>
    <!-- /.box-footer-->
  </div>

  <div class="modal fade" id="modal_product_create">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Create Product</h4>
        </div>
        <div class="modal-body">
          <form class="" action="{!! route('x') !!}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="">Code</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Name</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Merk</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Weight</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Width</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Height</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Unit</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea class="form-control" name="name" rows="3" cols=""></textarea>
            </div>
            <div class="form-group text-center">
              <button class="btn btn-xs" type="submit" name="button" data-toggle="modal" data-target="#modal_description">
                <i class="fa fa-circle-o" data-toggle="tooltip" title="x"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="hide modal-footer" style="text-align: center;">
          <button type="button" class="hide btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="modal_description">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Description</h4>
        </div>
        <div class="modal-body">
          <blockquote>
            <small>Detail</small>
            <p style="border">
              10% dimensional tolerancs of the original Design and spesification could be change without prior noticee and without reduce dimension the function and performance
            </p>
          </blockquote>
        </div>
        <div class="modal-footer" style="text-align: center;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="modal_specification">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">List Specification</h4>
        </div>
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover nowarp">
              <thead>
                <th>Name</th>
                <th>Detail</th>
              </thead>
              <tbody>
                <tr>
                  <td>Merk</td>
                  <td>SARANDI KARYA NUGRAHA KARIXA</td>
                </tr>
                <tr>
                  <td>Weight</td>
                  <td>10 Kg</td>
                </tr>
                <tr>
                  <td>Width</td>
                  <td>100 cm x 500 cm</td>
                </tr>
                <tr>
                  <td>Height</td>
                  <td>1000 cm</td>
                </tr>
                <tr>
                  <td>Unit</td>
                  <td>Box</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer" style="text-align: center;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="hide btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('js')

@endsection

@section('xjs')

@endsection
