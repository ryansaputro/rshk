<link href="/su_catalog/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>
body{
  background-image:url('/assets/bg-login.jpg');
  background-repeat:no-repeat;
-webkit-background-size:cover;
-moz-background-size:cover;
-o-background-size:cover;
background-size:cover;
background-position:center;
}
.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <br><br>
            <center><h3 style="color:red;">Vendor Catalog System </h3></center>

            @if(session('message'))
                    <div class='alert alert-danger'>
                           {{session('message')}}
                    </div>
            @endif
            <div class="account-wall" style="box-shadow: 5px 10px #888888;">
                <img class="profile-img" src="/assets/logo.png"
                    alt="">

                <form class="form-signin" id="loginform" action="/auth" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group">
              			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
              			<label class="control-label visible-ie8 visible-ie9">Email</label>
              			<div class="input-icon">
              				<i class="fa fa-user"></i>
                              <input id="email" type="email" class="form-control placeholder-no-fix" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
              			</div>
              		</div>
              		<div class="form-group">
              			<label class="control-label visible-ie8 visible-ie9">Kata Sandi</label>
              			<div class="input-icon">
              				<i class="fa fa-lock"></i>
                        <input id="password" type="password" class="form-control placeholder-no-fix"  name="password" required placeholder="Password">
              			</div>
              		</div>

                  {{-- <div class="form-group">
                      <div class="col-md-6 col-md-offset-4 pull-left">
                          <div class="checkbox">
                            <label style="color:black">
                                <input  type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                          </div>
                     </div>
                 </div> --}}

                 <input type="hidden" name="vcs" value="vcs">

                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Masuk</button>
                {{-- <label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">
                    Remember me
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span> --}}
                </form>
            </div>

        </div>
    </div>
</div>
