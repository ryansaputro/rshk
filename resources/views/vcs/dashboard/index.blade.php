@extends('vms.activator.layouts.app')

@section('title')
  VMS
@endsection

@section('css')
<style type="text/css" media="screen">
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 )
                url('http://sampsonresume.com/labs/pIkfp.gif')
                50% 50%
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
.doc {
    -webkit-column-count: 4;
    -moz-column-count: 4;
    column-count: 4;
    list-style-type: none;
}

/* CSS REQUIRED */
.state-icon {
    left: -5px;
}
.list-group-item-primary {
    color: rgb(255, 255, 255);
    background-color: rgb(66, 139, 202);
}

/* DEMO ONLY - REMOVES UNWANTED MARGIN */
.well .list-group {
    margin-bottom: 0px;
}
</style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small>{{$id}}</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div>
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
   @if(Session::has('message'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{Session::get('message')}}</strong>
      </div>
    @endif
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">{{$id}}</span>
            {{-- <span class="caption-helper hide">weekly stats...</span> --}}
          </div>
          <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons" id="tombol">
                <form>
                  <input type="text" name="search" placeholder="pencarian..." class="form-control" value="{{($search != 'xxx') ? $search : ''}}">
               </form>
            </div>
          </div>
        </div>
          @if ($id == 'vendor_validasi')
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>Telp</th>
                          <th>Tanggal Daftar</th>
                          <th>Data</th>
                          <th>Status</th>
                          <th>Aksi</th>
                        </thead>
                        <tbody>
                          @foreach($vendor_detail_aktif as $k => $v)
                          <tr class="user_{{$v->id_user_vendor}}" id="vendor">
                            <td>{{$k+1}}</td>
                            <td>{{$v->vendor_name}}</td>
                            <td>{{$v->address}}</td>
                            <td>{{$v->telephone}}</td>
                            <td>{{$v->created_at}}</td>
                              <td><a href="#" title="" data-id="{{$v->id_user_vendor}}" onclick="ListDoc(this)" class="btn btn-info btn-rounded btn-xs" data-name="{{$v->vendor_name}}">Detail</a></td>
                            <td class="td_status" data-id="{{$v->status_vendor}}" id="status_{{$v->id_vendor}}"><button type="button" class="btn btn-xs btn-{{($v->last_status == 6) ? 'success' : 'danger'}} btn-rounded ButtonEdit" data-id="{{$id}}">{{($v->last_status == 6) ? 'Lulus VMS' : 'Gagal VMS'}}</button>
                            </td>
                            <td class="tombolAksiStatus" id="tombol_{{$v->id_vendor}}">
                              <button type="button" class="btn btn-{{($v->last_status !== 5) ? 'success' : 'danger'}} btn-rounded btn-xs buttonType " onclick="UpdateStatusVendor(this)" data-button_type="{{($v->last_status == 5) ? '6' : '5'}}" data-id="{{$v->id_vendor}}" data-status="{{$v->status_vendor}}"><i class="fa fa-{{($v->last_status == 6) ? 'check' : 'times'}}"></i>{{($v->last_status == 6) ? 'Lulus' : 'Gagal'}}</button>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      <div class="col-md-5 col-sm-5">
                        <div>menampilkan {{ ($vendor_detail_aktif->currentPage() - 1) * $vendor_detail_aktif->perPage() + 1 }} sampai {{ $vendor_detail_aktif->count() * $vendor_detail_aktif->currentPage() }} dari {{ $vendor_detail_aktif->total() }} data</div>
                      </div>
                        <div class="col-md-7 col-sm-7 block-paginate">{{ $vendor_detail_aktif->appends(['search' => $search])->links() }}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="display: none;" id="detail">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <h3>Detail Kategori</h3>
                    <div class="row">
                      <div class="col-md-12" id="div_detail_body_validasi">
                        <table id="identitas" class="table table-striped">
                          <thead>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @elseif ($id == 'lulus_uji_berkas')
            <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                     <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telp</th>
                            <th>Tanggal Daftar</th>
                            <th>Dokumen</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </thead>
                          <tbody>
                            @foreach($vendor_detail_lulus as $k => $v)
                            <tr class="user_{{$v->id_user_vendor}}" id="vendor">
                              <td>{{$k+1}}</td>
                              <td>{{$v->vendor_name}}</td>
                              <td>{{$v->address}}</td>
                               <td>{{$v->telephone}}</td>
                              <td>{{$v->created_at}}</td>
                              <td>
                                <a href="#" title="" data-id="{{$v->id_user_vendor}}" onclick="ListDoc(this)" class="btn btn-info btn-rounded btn-xs" data-name="{{$v->vendor_name}}">Detail</a>
                              </td>
                              <td class="td_status" data-id="{{$v->status_vendor}}" id="status_{{$v->id_vendor}}">
                                <button type="button" class="btn btn-xs btn-{{($v->last_status == 1) ? 'success' : 'danger'}} btn-rounded ButtonEdit" data-id="{{$id}}">{{($v->last_status == 1) ? 'Lulus VMS' : 'Gagal VMS'}}
                                </button>
                              </td>
                              <td class="tombolAksiStatus" id="tombol_{{$v->id_vendor}}">
                                <button type="button" class="btn btn-{{($v->last_status == 1) ? 'success' : 'danger'}} btn-rounded btn-xs buttonType " onclick="UpdateStatusVendor(this)" data-button_type="{{($v->last_status == 1) ? '1' : '0'}}" data-id="{{$v->id_vendor}}" data-status="{{$v->status_vendor}}">
                                  <i class="fa fa-{{($v->last_status == 1) ? 'check' : 'times'}}"></i>
                                  {{($v->last_status == 1) ? 'Lulus' : 'Gagal'}}
                                </button>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <div class="col-md-5 col-sm-5">
                          <div>menampilkan {{ ($vendor_detail_lulus->currentPage() - 1) * $vendor_detail_lulus->perPage() + 1 }} sampai {{ $vendor_detail_lulus->count() * $vendor_detail_lulus->currentPage() }} dari {{ $vendor_detail_lulus->total() }} data</div>
                        </div>
                        <div class="col-md-7 col-sm-7 block-paginate">{{ $vendor_detail_lulus->appends(['search' => $search])->links() }}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               <div class="row" style="display: none;" id="detail">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12" id="detail_body_dok" style="padding-top: 50px;">
                            <div class="container">
                              <div class="row">
                                    <!-- Nav tabs -->
                                  <div class="card">
                                    {{-- <ul class="nav nav-tabs" role="tablist">
                                      @foreach($menu_doc as $k => $v)
                                        <li role="presentation" class="{{($k == 0) ? 'active' : ''}}">
                                            <a href="#{{$k}}" aria-controls="{{$k}}" role="tab" data-toggle="tab">{{$v}}</a>
                                        </li>
                                      @endforeach
                                    </ul> --}}

                                    <!-- Tab panes -->
                                   {{--  <div class="tab-content">
                                      @foreach($menu_doc as $k => $v)
                                        <div role="tabpanel" class="tab-pane {{($k == 0) ? 'active' : ''}}" id="{{$k}}">
                                                  @php
                                                  $arraySisi['l'] = array(
                                                    'Nama Perusahaan',
                                                    'Bentuk Usaha',
                                                    'Alamat ADP',
                                                    'Alamat Terbaru',
                                                    'Kode POS',
                                                    'Propinsi',
                                                    'Kabupaten/Kota',
                                                    'Kantor Cabang',
                                                  );
                                                  $arraySisi['lx'] = array(
                                                    "vendor - vendor_name",
                                                    'PT',
                                                    "vendor - address",
                                                    "vendor - address",
                                                    "vendor - post_code",
                                                    "-",
                                                    "-",
                                                    "Tidak",
                                                  );
                                                  $arraySisi['r'] = array(
                                                    'User ID',
                                                    'NPWP',
                                                    'No Pengukuhan PKP',
                                                    'Email',
                                                    'Telepon',
                                                    'Fax',
                                                    'Telepon Seluler',
                                                    'Website',
                                                    'Perubahan Tanggal',
                                                  );
                                                  $arraySisi['rx'] = array(
                                                    "vendor - username",
                                                    "vendor - npwp",
                                                    "vendor - pkp",
                                                    "vendor - email",
                                                    "vendor - telephone",
                                                    "vendor - fax",
                                                    "vendor - mobile_phone",
                                                    "vendor - website",
                                                    "vendor - updated_at",
                                                  );
                                                  @endphp
                                                  <h3>Profil Vendor</h3>
                                            @foreach ($arraySisi['l'] as $key => $value)
                                                <div class="col-md-3" style="text-align: left; padding-left: 50px;">
                                                  {{ $value }}
                                                </div>
                                                <div class="col-md-3 form_input_left" id="form_input_left_{{ $key }}">
                                                  {{ $arraySisi['lx'][$key] }}
                                                </div>
                                                @isset($arraySisi['r'][$key])
                                                  <div class="col-md-3" style="text-align: left; padding-left: 50px;">
                                                    {{ $arraySisi['r'][$key] }}
                                                  </div>
                                                  <div class="col-md-3 form_input" id="{{ $key }}" data-form="{{ $key }}" data-id="{{ $arraySisi['rx'][$key] }}">
                                                    {{ $arraySisi['rx'][$key] }}
                                                  </div>
                                                @endisset
                                            @endforeach
                                        </div>
                                      @endforeach
                                    </div>
                                  </div>
                              </div>
                            </div> --}}
                      <h3 class="vendor_name" style="text-align: left; padding-top: 150px;">Dokumen</h3>
                              <ul class="doc" style="padding-top: 0px;">
                                @foreach($menu_doc as $k => $v)
                                  <b style="padding-left: -20px !important;" class="t_{{$v}}">{{$v}}</b>
                                  <li class="li_{{$k}}" style="margin-bottom: 5px; color: red;">Tidak Ada Data</li>
                                @endforeach
                              </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          @elseif ($id == 'vendor_hadir')
            <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                     <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Tempat</th>
                            {{-- <th>Aksi</th> --}}
                          </thead>
                          <tbody>
                            @foreach($vendor_detail_hadir as $k => $v)
                            <tr class="user_{{$v->id_user_vendor}}" id="vendor">
                              <td>{{$k+1}}</td>
                              <td>{{$v->vendor_name}}</td>
                              <td>{{$v->date}}</td>
                               <td>{{$v->time}}</td>
                              <td>{{$v->place}}</td>
                              {{-- <td>
                                <a href="#" title="" data-id="{{$v->id_user_vendor}}" onclick="ListDoc(this)" class="btn btn-info btn-rounded btn-xs" data-name="{{$v->vendor_name}}">Detail</a>
                              </td>
                              <td class="td_status" data-id="{{$v->status_vendor}}" id="status_{{$v->id_vendor}}">
                                <button type="button" class="btn btn-xs btn-{{($v->last_status == 1) ? 'success' : 'danger'}} btn-rounded ButtonEdit" data-id="{{$id}}">{{($v->last_status == 1) ? 'Lulus VMS' : 'Gagal VMS'}}
                                </button>
                              </td>
                              <td class="tombolAksiStatus" id="tombol_{{$v->id_vendor}}">
                                <button type="button" class="btn btn-{{($v->last_status == 1) ? 'success' : 'danger'}} btn-rounded btn-xs buttonType " onclick="UpdateStatusVendor(this)" data-button_type="{{($v->last_status == 1) ? '1' : '0'}}" data-id="{{$v->id_vendor}}" data-status="{{$v->status_vendor}}">
                                  <i class="fa fa-{{($v->last_status == 1) ? 'check' : 'uncheck'}}"></i>
                                  {{($v->last_status == 1) ? 'Lulus' : 'Batal'}}
                                </button>
                              </td> --}}
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <div class="col-md-5 col-sm-5">
                          <div>menampilkan {{ ($vendor_detail_lulus->currentPage() - 1) * $vendor_detail_lulus->perPage() + 1 }} sampai {{ $vendor_detail_lulus->count() * $vendor_detail_lulus->currentPage() }} dari {{ $vendor_detail_lulus->total() }} data</div>
                        </div>
                        <div class="col-md-7 col-sm-7 block-paginate">{{ $vendor_detail_lulus->appends(['search' => $search])->links() }}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               <div class="row" style="display: none;" id="detail">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12" id="detail_body_dok" style="padding-top: 50px;">
                            <div class="container">
                              <div class="row">
                                    <!-- Nav tabs -->
                                  <div class="card">
                                    {{-- <ul class="nav nav-tabs" role="tablist">
                                      @foreach($menu_doc as $k => $v)
                                        <li role="presentation" class="{{($k == 0) ? 'active' : ''}}">
                                            <a href="#{{$k}}" aria-controls="{{$k}}" role="tab" data-toggle="tab">{{$v}}</a>
                                        </li>
                                      @endforeach
                                    </ul> --}}

                                    <!-- Tab panes -->
                                   {{--  <div class="tab-content">
                                      @foreach($menu_doc as $k => $v)
                                        <div role="tabpanel" class="tab-pane {{($k == 0) ? 'active' : ''}}" id="{{$k}}">
                                                  @php
                                                  $arraySisi['l'] = array(
                                                    'Nama Perusahaan',
                                                    'Bentuk Usaha',
                                                    'Alamat ADP',
                                                    'Alamat Terbaru',
                                                    'Kode POS',
                                                    'Propinsi',
                                                    'Kabupaten/Kota',
                                                    'Kantor Cabang',
                                                  );
                                                  $arraySisi['lx'] = array(
                                                    "vendor - vendor_name",
                                                    'PT',
                                                    "vendor - address",
                                                    "vendor - address",
                                                    "vendor - post_code",
                                                    "-",
                                                    "-",
                                                    "Tidak",
                                                  );
                                                  $arraySisi['r'] = array(
                                                    'User ID',
                                                    'NPWP',
                                                    'No Pengukuhan PKP',
                                                    'Email',
                                                    'Telepon',
                                                    'Fax',
                                                    'Telepon Seluler',
                                                    'Website',
                                                    'Perubahan Tanggal',
                                                  );
                                                  $arraySisi['rx'] = array(
                                                    "vendor - username",
                                                    "vendor - npwp",
                                                    "vendor - pkp",
                                                    "vendor - email",
                                                    "vendor - telephone",
                                                    "vendor - fax",
                                                    "vendor - mobile_phone",
                                                    "vendor - website",
                                                    "vendor - updated_at",
                                                  );
                                                  @endphp
                                                  <h3>Profil Vendor</h3>
                                            @foreach ($arraySisi['l'] as $key => $value)
                                                <div class="col-md-3" style="text-align: left; padding-left: 50px;">
                                                  {{ $value }}
                                                </div>
                                                <div class="col-md-3 form_input_left" id="form_input_left_{{ $key }}">
                                                  {{ $arraySisi['lx'][$key] }}
                                                </div>
                                                @isset($arraySisi['r'][$key])
                                                  <div class="col-md-3" style="text-align: left; padding-left: 50px;">
                                                    {{ $arraySisi['r'][$key] }}
                                                  </div>
                                                  <div class="col-md-3 form_input" id="{{ $key }}" data-form="{{ $key }}" data-id="{{ $arraySisi['rx'][$key] }}">
                                                    {{ $arraySisi['rx'][$key] }}
                                                  </div>
                                                @endisset
                                            @endforeach
                                        </div>
                                      @endforeach
                                    </div>
                                  </div>
                              </div>
                            </div> --}}
                      <h3 class="vendor_name" style="text-align: left; padding-top: 150px;">Dokumen</h3>
                              <ul class="doc" style="padding-top: 0px;">
                                @foreach($menu_doc as $k => $v)
                                  <b style="padding-left: -20px !important;" class="t_{{$v}}">{{$v}}</b>
                                  <li class="li_{{$k}}" style="margin-bottom: 5px; color: red;">Tidak Ada Data</li>
                                @endforeach
                              </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
           @else
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <body onload=display_ct();>
                    Selemat Datang Admin VMS,
                        <span id='ct' ></span>
                    </body>
                  </div>
                </div>
              </div>
            </div>
          @endif
          <div class="modal"></div>
        </div><!--/.portlet-body--->
      </div>
    </div>
  </div>

  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="margin-top:150px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ceklis</h4>
          </div>
          <div class="modal-body">
            <div class="ceklis_list">
            </div>
                <br />
                <button class="btn btn-primary col-xs-12" id="get-checked-data">Simpan ?</button>
                <br><br>
                <div class="div_aksi">
                  <form class="form-horizontal" method="POST" action="{{ URL::to('vms/activator/checklist') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div id="inputData"></div>
                  <div id="display-json" style="text-align:center; padding-top:10px;"></div>
                  <div class="id_vendor">

                  </div>
                  </form>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
  <!-- IModal -->
    <div class="modal fade" id="IModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="margin-top:150px;">
          <form class="form-horizontal" method="POST" action="{{ URL::to('vms/activator/invitation') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Undang</h4>
          </div>
          <div class="modal-body">
            <div class="invitation_body">
            </div>
            <div class="div_vendor_id">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Undang</button>
          </div>
        </form>
        </div>
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">

var additional = [];
var statusChange ="";
var statusGlobal ="";
function ChangeStatus(a) {
  var id_user = $(a).attr('data-id')
  var status = $(a).attr('data-status')
  input = '<div class="input-group">'+
                    '<div class="custom-control custom-radio">'+
                      '<label style="margin-left:10px;"><input type="radio" '+((status == 1) ? "checked" : "")+' focus required="" name="status['+id_user+']" value="1" class="custom-control-input status" onclick="StatusChange(this)">Aktif</label>'+
                    '</div>'+
                    '<div class="custom-control custom-radio">'+
                      '<label style="margin-left:10px;"><input type="radio" required="" '+((status == 0) ? "checked" : "")+' name="status['+id_user+']" value="0" class="custom-control-input status" onclick="StatusChange(this)">Tidak Aktif</label>'+
                    '</div>'+
                  '</div>';
  var tombol = '<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="Update(this)" data-id="'+id_user+'" data-status="'+status+'"><i class="fa fa-check"></i> Selesai</button>';

  $('tr.user_'+id_user+' td.td_status').html(input);
  $('td#tombol_'+id_user).html(tombol);
  statusGlobal = status
}

function StatusChange(a) {
  statusChange = $(a).val();
}
function Update(a) {
  if(statusChange === ""){
    ab = statusGlobal;
  }else{
    ab = statusChange;
  }

  var id_user = $(a).attr('data-id');
  var status = ab;
  var page = "{{$vendor_detail->currentPage()}}";
  var url = "{{URL::to('vms/activator/update_status_user_vendor')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "status": ab,
          "halaman": page,
          },
        success: function (a) {
          location.reload();
        }
    });
}

function ListDoc(a) {
  $('#detail').css('display','-webkit-box');
  var id_user = $(a).attr('data-id');
  var page = "{{$vendor_detail->currentPage()}}";
  var url = "{{URL::to('vms/activator/detail_vendor')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "halaman": page,
          },
        success: function (a) {
          var jumlah = a.category.length;
          var table = "";
          var id_vendor = "";
          var ceklis = "";
            ceklis += '<ul id="check-list-box" class="list-group checked-list-box">';
              ceklis += '<li class="list-group-item LOGO" data-color="success">LOGO</li>';
              ceklis += '<li class="list-group-item NPWP" data-color="success">NPWP</li>';
              @foreach ($surat as $k => $v)
              ceklis += '<li class="list-group-item {{$v->name_vendor_business_license}}" data-color="success">{{$v->name_vendor_business_license}}</li>';
              @endforeach
            ceklis += '</ul>';
          $('div.ceklis_list').html(ceklis);
          var cekDok = [];
          $.each(a.vendorCek, function(x,y){
            if(a.id_vendor == y.id_vendor){
              cekDok[y.data] = y.data;
            }
          });
          $(function () {
              $('.list-group.checked-list-box .list-group-item').each(function () {
                  // Settings
                  var $widget = $(this),
                      $checkbox = $('<input type="checkbox" class="hidden '+$(this).text()+'" />'),
                      color = ($widget.data('color') ? $widget.data('color') : "primary"),
                      style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
                      settings = {
                          on: {
                              icon: 'glyphicon glyphicon-check'
                          },
                          off: {
                              icon: 'glyphicon glyphicon-unchecked'
                          }
                      };

                  $widget.css('cursor', 'pointer')
                  $widget.append($checkbox);

                  if(jQuery.inArray($(this).text(), cekDok)){
                    cekDok[$(this).text()];
                    if(cekDok[$(this).text()] !== undefined){
                      console.log(cekDok[$(this).text()]);
                      $("."+cekDok[$(this).text()]).prop('checked', !$("."+cekDok[$(this).text()]).is(':checked'));
                      $("."+cekDok[$(this).text()]).triggerHandler('change');
                      updateDisplay();
                      $("#display-json").html("");
                    }
                  }

                  // Event Handlers
                  $widget.on('click', function () {
                      $checkbox.prop('checked', !$checkbox.is(':checked'));
                      $checkbox.triggerHandler('change');
                      updateDisplay();
                      $("#display-json").html("");
                  });
                  $checkbox.on('change', function () {
                      updateDisplay();
                  });


                  // Actions
                  function updateDisplay() {
                      var isChecked = $checkbox.is(':checked');

                      // Set the button's state
                      $widget.data('state', (isChecked) ? "on" : "off");

                      // Set the button's icon
                      $widget.find('.state-icon')
                          .removeClass()
                          .addClass('state-icon ' + settings[$widget.data('state')].icon);

                      // Update the button's color
                      if (isChecked) {
                          $widget.addClass(style + color + ' active');
                      } else {
                          $widget.removeClass(style + color + ' active');
                      }
                  }

                  // Initialization
                  function init() {

                      if ($widget.data('checked') == true) {
                          $checkbox.prop('checked', !$checkbox.is(':checked'));
                      }

                      updateDisplay();

                      // Inject the icon if applicable
                      if ($widget.find('.state-icon').length == 0) {
                          $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
                      }
                  }
                  init();
              });

              $('#get-checked-data').on('click', function(event) {
                  $('#inputData').html('');
                  event.preventDefault();
                  var checkedItems = {}, counter = 0;
                  $("#check-list-box li.active").each(function(idx, li) {
                      checkedItems[counter] = $(li).text();
                      counter++;
                      $('#inputData').append('<input type="hidden" name="ceklis['+$(li).text()+']" value="1">');

                  });
                  $('#display-json').html('Apakah Anda Yakin ? <button type="submit" class="btn btn-sm btn-primary">Ya</button>');
              });
          });
          $.each(a.category, function(k,v){
            id_vendor = v.id_vendor_detail;
            table += '<tr>';
              table += '<td colspan="2" style="background-color:#ddd;"><strong>IDENTITAS PERUSAHAAN</strong>';
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Nama Perusahaan"
              table += '</td>';
              table += '<td>';
              table += v.vendor_name
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "NPWP"
              table += '</td>';
              table += '<td>';
              table += v.npwp
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Alamat"
              table += '</td>';
              table += '<td>';
              table += v.address
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Kode Pos"
              table += '</td>';
              table += '<td>';
              table += v.post_code
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Nama Pengguna"
              table += '</td>';
              table += '<td>';
              table += v.username
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Nomor PKP"
              table += '</td>';
              table += '<td>';
              table += v.pkp
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Telepon"
              table += '</td>';
              table += '<td>';
              table += v.telephone
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Fax"
              table += '</td>';
              table += '<td>';
              table += v.fax
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Hp"
              table += '</td>';
              table += '<td>';
              table += v.mobile_phone
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Email"
              table += '</td>';
              table += '<td>';
              table += v.email
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Bank"
              table += '</td>';
              table += '<td>';
              table += v.bank
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "No Rekening"
              table += '</td>';
              table += '<td>';
              table += v.no_rek
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Logo"
              table += '</td>';
              table += '<td>';
              $.each(a.dokumen, function(x,y){
                var date = y.created_at.split(' ');
                if(y.table == '-'){
                  table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/logo/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                }
              });
              table += '</td>';
            table += '</tr>';
            $('div.id_vendor').html("<input type='hidden' name='id_vendor' value='"+a.id_vendor+"'>");
          });

          $.each(a.akta, function(k,v){
            table += '<tr>';
              table += '<td colspan="2" style="background-color:#ddd;"><strong>AKTA PERUSAHAAN</strong>';
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "No Akta"
              table += '</td>';
              table += '<td>';
              table += v.deed_number
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Tanggal"
              table += '</td>';
              table += '<td>';
              table += v.date
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Notaris"
              table += '</td>';
              table += '<td>';
              table += v.notary_public
              table += '</td>';
            table += '</tr>';
            table += '<tr>';
              table += '<td>';
              table += "Dokumen"
              table += '</td>';
              table += '<td>';
              $.each(a.dokumen, function(x,y){
                var date = y.created_at.split(' ');
                if(y.table == 'vendor_deed'){
                  table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/akta/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                }
              });
              table += '</td>';
            table += '</tr>';
          });

            table += '<tr>';
            table += '<td colspan="2" style="background-color:#ddd;"><strong>LISENSI PERUSAHAAN</strong>';
            table += '</td>';
            table += '</tr>';
            $.each(a.suratDtl, function(k, v){
              table += '<tr>';
                table += '<td colspan="2"><strong>'+v.name_vendor_business_license+'</strong>';
                table += '</td>';
                table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Nomor"
                table += '</td>';
                table += '<td>';
                table += v.reference_number
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Instansi Pemberi"
                table += '</td>';
                table += '<td>';
                table += v.giver_agency
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Tanggal Kadaluarsa"
                table += '</td>';
                table += '<td>';
                table += v.valid_until
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Dokumen"
                table += '</td>';
                table += '<td>';
                $.each(a.dokumen, function(x,y){
                  var date = y.created_at.split(' ');
                  if(y.id_data == v.id_surat){
                    table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/surat/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                  }
                });
                table += '</td>';
              table += '</tr>';
            });

            table += '<tr>';
            table += '<td colspan="2" style="background-color:#ddd;"><strong>KLASIFIKASI PERUSAHAAN</strong>';
            table += '</td>';
            table += '</tr>';
            $.each(a.klasifikasi, function(k,v){
              table += '<tr>';
                table += '<td colspan="2"><strong>'+v.code+' - '+v.name+'</strong>';
                table += '</td>';
                table += '</tr>';
              table += '<tr>';
              table += '<tr>';
                table += '<td>';
                table += "Sub Bidang"
                table += '</td>';
                table += '<td>';
                table += v.sub_classification
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Kualifikasi Bidang"
                table += '</td>';
                table += '<td>';
                table += v.qualification = 1 ? 'Kecil' : 'Non Kecil'
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "No Surat Bidang"
                table += '</td>';
                table += '<td>';
                table += v.no_letter
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Tanggal Kadaluarsa"
                table += '</td>';
                table += '<td>';
                table += v.expired_date
                table += '</td>';
              table += '</tr>';
            });

            table += '<tr>';
              table += '<td colspan="2" style="background-color:#ddd;"><strong>PEMILIK PERUSAHAAN</strong>';
              table += '</td>';
            table += '</tr>';
            $.each(a.pemilik, function(k,v){
              table += '<tr>';
              table += '<td colspan="2"><strong>Pemilik '+parseInt(k+1)+'</strong>';
              table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Nama Pemilik"
                table += '</td>';
                table += '<td>';
                table += v.name_vendor_owner
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "KTP Pemilik"
                table += '</td>';
                table += '<td>';
                table += v.ktp_vendor_owner
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Alamat Pemilik"
                table += '</td>';
                table += '<td>';
                table += v.address_vendor_owner
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Dokumen"
                table += '</td>';
                table += '<td>';
                $.each(a.dokumen, function(x,y){
                  var date = y.created_at.split(' ');
                  if((y.table == 'vendor_owner') && (y.id_data == v.id)){
                    table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/pemilik/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                  }
                });
                table += '</td>';
              table += '</tr>';
            });

            table += '<tr>';
              table += '<td colspan="2" style="background-color:#ddd;"><strong>PENGURUS PERUSAHAAN</strong>';
              table += '</td>';
            table += '</tr>';
            $.each(a.pengurus, function(k,v){
              table += '<tr>';
              table += '<td colspan="2"><strong>Pengurus '+parseInt(k+1)+'</strong>';
              table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Nama Pengurus"
                table += '</td>';
                table += '<td>';
                table += v.name_name_vendor_administrators
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Jabatan Pengurus"
                table += '</td>';
                table += '<td>';
                table += v.position_name_vendor_administrators
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "KTP Pengurus"
                table += '</td>';
                table += '<td>';
                table += v.ktp_name_vendor_administrators
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Alamat Pengurus"
                table += '</td>';
                table += '<td>';
                table += v.name_name_vendor_administrators
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Dokumen"
                table += '</td>';
                table += '<td>';
                $.each(a.dokumen, function(x,y){
                  var date = y.created_at.split(' ');
                  if((y.table == 'vendor_administrators') && (y.id_data == v.id)){
                      table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/surat/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                  }
                });
                table += '</td>';
              table += '</tr>';
            });

            table += '<tr>';
              table += '<td colspan="2" style="background-color:#ddd;"><strong>TENAGA AHLI PERUSAHAAN</strong>';
              table += '</td>';
            table += '</tr>';
            $.each(a.ahli, function(k,v){
              table += '<tr>';
              table += '<td colspan="2"><strong>Tenaga Ahli '+parseInt(k+1)+'</strong>';
              table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Nama Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.name_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Tanggal Lahir Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.birth_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Alamat Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.address_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Kelamin Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.gender_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Pendidikan Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.education_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Kebangsaan Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.nationallty_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Pengalaman Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.experience_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Email Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.email_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Keahlian Tenaga Ahli"
                table += '</td>';
                table += '<td>';
                table += v.expertise_vendor_expert_staff
                table += '</td>';
              table += '</tr>';
              table += '<tr>';
                table += '<td>';
                table += "Dokumen"
                table += '</td>';
                table += '<td>';
                $.each(a.dokumen, function(x,y){
                  var date = y.created_at.split(' ');
                  if((y.table == 'vendor_expert_staff') && (y.id_data == v.id)){
                      table += '<a target="_blank" href="../../assets/document/'+a.id_vendor+'/tenaga_ahli/'+date[0].replace(/\-/g,'')+'/'+y.file+'">'+y.file+'</a>'
                  }
                });
                table += '</td>';
              table += '</tr>';
            });

          $('#div_detail_body_validasi table#identitas tbody').html(table)
          $("html, body").animate({ scrollTop: $("#div_detail_body_validasi table#identitas").prop("scrollHeight")}, 500)
        }
    });
}

// function ListDoc(x) {
//   $('#detail').css('display','-webkit-box');
//   var id_user = $(x).attr('data-id');
//   var page = "{{$vendor_detail->currentPage()}}";
//    var url = "{{URL::to('vms/activator/document_vendor')}}";
//     $.ajax({
//         type: "post",
//         url: url,
//       data: {
//           "_token": "{{ csrf_token() }}",
//           "id_user": id_user,
//           "halaman": page,
//           },
//         success: function (a) {
//           var data = [];
//               // console.log("VENDOR: "+a);
//           $.each(a.vendor_detail_x, function(k,v){
//             if(k != 1){
//               data[0] = v.vendor_name;
//               data[1] = v.address;
//               data[2] = v.address;
//               data[3] = v.post_code;
//               data[4] = v.province;
//               data[5] = v.city;
//               data[6] = v.username;
//               data[7] = v.npwp;
//               data[8] = v.pkp;
//               data[9] = v.email;
//               data[10] = v.telephone;
//               data[11] = v.fax;
//               data[12] = v.mobile_phone;
//               data[13] = v.website;
//               // $("#form_input_left_"+k).html(v.address)
//
//             }
//           });
//
//           $.each(data, function(k,v){
//               $("#form_input_left_"+k).html(v)
//               console.log("DATA: "+v);
//             });
//
//           var jumlah = a.doc.length;
//           var ul ="";
//               menues = ['Penyedia',
//                       'Izin Usaha',
//                       'Akta',
//                       'Pemilik',
//                       'Pengurus',
//                       'Tenaga Ahli',
//                       'Peralatan',
//                       'Pengalaman',
//                       'Pajak',
//                       'Dokumen'];
//
//               if(jumlah > 0){
//                   $.each(a.doc, function(k, v){
//                   tanggal = v.file.substr(0,8);
//                   tahun = v.file.substr(0,4)
//                   bulan = v.file.substr(4,2)
//                   hari = v.file.substr(6,2)
//                   full = tahun+"-"+bulan+"-"+hari;
//                   href = '/assets/document/'+a.vendor_id+'/'+menues[v.menu]+'/'+full+'/'+v.file
//                   $(".li_"+v.menu).html("<a href='"+href+"' target='_blank'>"+v.file+"</a>")
//                   })
//                 }else{
//                   $.each(menues, function(k,v){
//                   $(".li_"+k).html("<font style='color:red;'>Tidak Ada Data</font>")
//                   })
//                 }
//
//                 $('h3.vendor_name').html("Dokumen "+$(x).attr('data-name'));
//         }
//     });
// }

function UpdateStatusVendor(a) {
  var jenis_button = $(a).attr('data-button_type');
  if(jenis_button == 5){
    status = 6;
  }else{
    status = 5;
  }
  $body = $("body");
  var id_vendor = $(a).attr('data-id');
  var url = "{{URL::to('vms/activator/status_vendor')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "vendor": id_vendor,
          "status": status,
          },
        success: function (a) {

          if(jenis_button == 5){
             button = '<button type="button" class="btn btn-danger btn-rounded btn-xs buttonType " onclick="UpdateStatusVendor(this)" data-button_type="6" data-id="'+a.id_vendor+'" data-status=""><i class="fa fa-times"></i> Batal</button>';
             btn ='<button type="button" class="btn btn-xs btn-danger btn-rounded ButtonEdit" data-id="{{$id}}">Gagal VMS</button>';
          }else{
             button = '<button type="button" class="btn btn-success btn-rounded btn-xs buttonType " onclick="UpdateStatusVendor(this)" data-button_type="5" data-id="'+a.id_vendor+'" data-status=""><i class="fa fa-check"></i> Lulus</button>';
             btn ='<button type="button" class="btn btn-xs btn-success btn-rounded ButtonEdit" data-id="{{$id}}">Lulus VMS</button>';
          }


          $('#tombol_'+a.id_vendor).html(button)
          $('#status_'+a.id_vendor).html(btn)
        }
    });

}



$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});

function SendCode(a) {
  var id = $(a).attr('data-id');
  var nama = $(a).attr('data-name');
  var r = confirm("Apakah Anda Yakin ingin Kirim Code ke vendor "+nama+" ?");
    if (r == true) {
      var id = $(a).attr('data-id');
      var url = "{{URL::to('vms/activator/send_code_verifikasi')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id,
                  "name": nama,
                  },
                success: function (a) {
                  location.reload();
                }
        })

    } else {
        location.reload();
    }
}

function display_c(){
var refresh=1000; // Refresh rate in milli seconds
mytime=setTimeout('display_ct()',refresh)
}

function display_ct() {
var x = new Date()
var x1=x.getMonth() + 1+ "/" + x.getDate() + "/" + x.getYear();
x1 = x1 + " - " +  x.getHours( )+ ":" +  x.getMinutes() + ":" +  x.getSeconds();
document.getElementById('ct').innerHTML = x1;
display_c();
 }
</script>
@endsection
