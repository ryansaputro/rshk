@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Form Kontrak...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Kontrak ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('vcs/users/contract/create')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">

            <div class="col-md-12">
              <div class="col-md-12">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="no_contract" class="col-md-4 control-label">No Kontrak</label>
                  <div class="col-md-8">
                    <input style="margin-bottom:10px;" id="no_contract" type="text" class="form-control input-sm" name="no_contract" value="" required autofocus>
                    @if ($errors->has('no_contract'))
                      <span class="help-block">
                        <strong>{{ $errors->first('no_contract') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>


              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="start_date" class="col-md-4 control-label">Tanggal Berlaku</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="start_date" type="date" class="form-control input-sm" name="start_date" value="" required autofocus>
                  @if ($errors->has('start_date'))
                    <span class="help-block">
                      <strong>{{ $errors->first('start_date') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="end_date" class="col-md-4 control-label">Tanggal Berakhir</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="end_date" type="date" class="form-control input-sm" name="end_date" value="" required autofocus>
                  @if ($errors->has('end_date'))
                    <span class="help-block">
                      <strong>{{ $errors->first('end_date') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="description" class="col-md-4 control-label">Deskripsi</label>
                <div class="col-md-8">
                  <textarea name="description" style="margin-bottom:10px;" class="form-control input-sm"></textarea>
                  @if ($errors->has('description'))
                    <span class="help-block">
                      <strong>{{ $errors->first('description') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="data" class="col-md-4 control-label">Scan Kontrak</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="data" type="file" accept="application/pdf"  class="form-control input-sm" name="data" value="" required autofocus>
                  @if ($errors->has('data'))
                    <span class="help-block">
                      <strong>{{ $errors->first('data') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                  <button type="submit" name="button" class="btn btn-primary btn-sm">Simpan</button>
                  <a href="{{URL::to('vcs/users/contract')}}" class="btn btn-danger btn-sm">Kembali</a>
                </div>
              </div>

            </div>
          </div>
        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
   });


$('.pengirimanBy').on('change', function(){
  $('.pengirimanBy option').each(function(k,v){
    if($(v).is(':selected')){
      if($(this).val() == 0){
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Nama Kurir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No Resi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }else{
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Sopir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Mobil</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No. Polisi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }
    }
    $('.pengiriman').css('display','inline');
    $('.pengiriman').css('margin-top','30px');
    $('.pengiriman').html(data);
  });

});

$(document).ready(function(){
  // $('.pengiriman').hide();

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

});

var name = "";
var id = "";
$('#item').on('change', function(){
  id = $('#item :selected').attr('data-id');
  id_item = $(this).val();
  name =$('#item :selected').text();
  $.ajax({
      url: '{{URL::to('vcs/users/detailItem')}}',
      method: 'POST',
      data: {"_token": "{{ csrf_token() }}", "id": id, "id_item" : id_item},
      success: function (a) {
        $('#qty').attr('value', a.qty);
        if(a.qty > 0){
          $('#qty').attr('max', a.qty);
          $('#qty').attr('min', 1);
        }else{
          $('#qty').attr('max', 0);
          $('#qty').attr('min', 0);

        }
      }
  });
});
var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}

</script>
@endsection
