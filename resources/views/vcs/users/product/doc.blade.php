@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              <a href="#modal_add" class="btn btn-info btn-sm" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>

                {{-- <a href="#modal_add" class="bt btn-info btn-sm button_add" onclick="AddSpec(this)"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a> --}}
              {{-- <button type="button" class="btn btn-success btn-rounded" onclick="" data-id=""><i class="fa fa-plus"></i> Tambah</button> --}}
              {{-- <a href="/su_vms/assets/users/template/template-product.xlsx" target="_blank" class="btn btn-warning btn-rounded"><i class="fa fa-download"></i> Template(xls)</a> --}}
              {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <table class="table table-striped table-bordered" style="width: 100%">
						<thead>
							<tr>
								<th>#</th>
								{{-- <th>No</th> --}}
								<th>Name</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							@php
								$doc_item = DB::table('doc_item')->where('id_item', $id)->get();
							@endphp
							@foreach ($doc_item as $key => $value)
								<tr>
									<td style="vertical-align: middle; text-align: center;">
										<form class="" action="{{URL::to('vcs/users/product/doc/status/update')}}" method="post">
											{{ csrf_field() }}
											<a href="{{URL::asset('/assets/catalog/doc/'.$value->file)}}" target="_blank" class="btn btn-primary btn-xs">
												<span class="fa fa-download" style="color: white !important;"></span>
											</a>
											<a  onclick="DeleteImage(this)" data-name="{{$value->file}}" class="btn btn-danger btn-xs deleteFile">
												<span class="fa fa-trash" style="color: white !important;"></span> Hapus
											</a>
											<input type="hidden" name="id" value="{{ $value->id }}">
											@if ($value->status != 1)
												<input type="hidden" name="status" value="1">
												<button type="submit" class="btn btn-warning btn-xs">
													<span class="fa fa-eye"></span> Tampilkan
												</button>
											@else
												<input type="hidden" name="status" value="0">
												<button type="submit" class="btn btn-info btn-xs">
													<span class="fa fa-eye-slash"></span> Sembunyikan
												</button>
											@endif
										</form>
									</td>
									{{-- <td style="vertical-align: middle; text-align: center;">{{ $key+1 }}</td> --}}
									<td style="vertical-align: middle;">{{ $value->name }}</td>
									<td style="vertical-align: middle;">{{ $value->description == '' ? '-' : $value->description }}</td>
								</tr>
							@endforeach
							@if (sizeof($doc_item) == 0)
								<tr>
									<td colspan="4">Tidak ada Data </td>
								</tr>
							@endif
						</tbody>
					</table>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_add" role="dialog">
		<div class="modal-dialog modal-lg">
			<form class="ModalForm" method="post" action="{{URL::to('vcs/users/product/doc/save/'.$id)}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Dokumen Item Baru</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="">Dokumen <small style="Color:red;">(Ukuran maksimal 1Mb)</small> </label>
							<input type="file" name="file" value="" class="form-control" required accept="application/pdf,application/vnd.ms-excel" >
						</div>
						<div class="form-group">
							<label for="">Nama</label>
							<input type="text" name="name" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Deskripsi</label>
							<textarea name="description" rows="8" cols="80" class="form-control" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection
@section('js')
<script type="text/javascript">
function DeleteImage(a) {
  var r = confirm("Are You sure delete this Image?");
  var id = $(a).attr('data-name');
  if (r == true) {
    $('.deleteFile').attr('href', 'delete/'+id);
  }
}
</script>
@endsection
