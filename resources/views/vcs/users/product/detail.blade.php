@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection
@section('css')
@endsection
@section('xcss')
@endsection
@section('logo')
<a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
{{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
RSHK
</a>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-12">
      {{-- <div class="portlet light">
        <div class="portlet-body"> --}}
            <div class="chat_window" style="z-index:99999999999999;">
               <div class="top_menu">
                  {{-- <div class="buttons">
                     <div class="button close"></div>
                     <div class="button minimize"></div>
                     <div class="button maximize"></div>
                  </div> --}}
                  <div class="title">Chat</div>
               </div>
               <ul class="messages">
                  <li class="message left appeared pesanChat">
                  </li>
                  @foreach ($data as $key => $value)
                    <input type="hidden" name="id_vendor_detail" value="{{$value->id_vendor_detail}}" class="id_vendor_detail">
                    <input type="hidden" name="id_cart" value="{{$value->id_cart}}" class="id_cart">
                    <input type="hidden" name="id_user" value="{{$value->id_user}}" class="id_user">
                    <input type="hidden" name="id_item" value="{{$value->id_item}}" class="id_item">
                    @if($value->status == 0)
                    <li class="message left appeared pesanChat">
                      <div class="avatar">Penjual</div>
                      <div class="text_wrapper" style="margin-bottom:10px;">
                        <div class="text">{{$value->message}}<p style="font-size:12px; color:#000;">{{$value->datetime}}</p></div>
                      </div>
                    </li>
                  @else
                    <li class="message right appeared pesanChatRight">
                      <div class="avatar">{{$value->username}}</div>
                      <div class="text_wrapper" style="margin-bottom:10px;">
                        <div class="text">{{$value->message}}<p style="font-size:12px; color:#000;">{{$value->datetime}}</p></div>
                      </div>
                    </li>
                  @endif
                  @endforeach
               </ul>
               <div class="bottom_wrapper clearfix">
                  <div class="message_input_wrapper"><input class="message_input" placeholder="Type your message here..." /></div>
                  <div class="send_message">
                     <div class="icon"></div>
                     <div class="text">Send</div>
                  </div>
               </div>
            </div>
            <div class="message_template">
               <li class="message">
                  <div class="avatar"></div>
                  <div class="text_wrapper">
                     <div class="text"></div>
                  </div>
               </li>
            </div>
        </div>
      </div>
    {{-- </div>
  </div> --}}
@endsection
@section('note')
@endsection
@section('product-pop-up')
@endsection
@section('js')
<script type="text/javascript">
   // (function () {
       var Message;
       Message = function (arg) {
           this.text = arg.text, this.message_side = arg.message_side;
           this.draw = function (_this) {
               return function () {
                   var $message;
                   $message = $($('.message_template').clone().html());
                   $message.addClass(_this.message_side).find('.text').html(_this.text);
                   $('.messages').append($message);
                   return setTimeout(function () {
                       return $message.addClass('appeared');
                   }, 0);
               };
           }(this);
           return this;
       };
       $(function () {
           var getMessageText, message_side, sendMessage;
           message_side = 'right';
           getMessageText = function () {
               var $message_input;
               $message_input = $('.message_input');
               return $message_input.val();
           };
           sendMessage = function (text) {
               var $messages, message;
               if (text.trim() === '') {
                   return;
               }
               $('.message_input').val('');
               $messages = $('.messages');
               message_side = message_side === 'left' ? 'left' : 'right';
               message = new Message({
                   text: text,
                   message_side: message_side
               });
               message.draw();
               return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
           };


           $('.send_message').click(function (e) {
               $.ajax({
                   type: "post",
                   url: "{{URL::to('vcs/users/Addchat')}}",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "id_vendor_detail": $('.id_vendor_detail').val(),
                     "id_cart": $('.id_cart').val(),
                     "id_user": $('.id_user').val(),
                     "id_item": $('.id_item').val(),
                     "message": getMessageText(),
                     },
                   success: function (a) {
                     var txtx = "";
                     if(a.status == 0){
                       txtx  += '<div class="avatar">Penjual</div>';
                       txtx  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                       txtx  +=           '   <div class="text">'+getMessageText()+'<p style="font-size:12px; color:#000;">a few minute ago</p></div>';
                       txtx  +=           '</div>';
                       $('li.pesanChat').append(txtx);
                       $('.message_input').val('');

                     }else{
                       alert("Gagal Kirim Chat!")
                     }
                   }
                 });
           });
           $('.message_input').keyup(function (e) {
               if (e.which === 13) {
                   $('.send_message').trigger('click');
               }
           });
       });

   function chatOnline(a) {
     var vendor = $(a).attr('data-vendor');
     var id_item = $(a).attr('data-id_item');
     var id_cart = $(a).attr('data-id_cart');
     $('.chat_window').addClass('show')
     $.ajax({
         type: "post",
         url: "{{URL::to('catalog/users/chat')}}",
       data: {
           "_token": "{{ csrf_token() }}",
           "id_vendor_detail": vendor,
           "id_user": "{{Auth::user()->id}}",
           },
         success: function (a) {
           console.log(a)
           var txt = "";
           var txtx = "";
           jumlah = a.data.length;
           if(jumlah >= 1){
               $.each(a.data, function(k,v){
                 if(v.status == 0){
                   txt  += '<div class="avatar">'+a.name+'</div>';
                   txt  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                   txt  +=           '   <div class="text">'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                   txt  +=           '</div>';
                   $('li.pesanChat').html(txt);
                 }else if(v.status == 1){
                   txtx  += '<div class="avatar">Pembeli</div>';
                   txtx  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                   txtx  +=           '   <div class="text">'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                   txtx  +=           '</div>';
                   $('li.pesanChatRight').html(txtx);
                 }
             });
         }else{
           $('li.pesanChatRight').html("");
           $('li.pesanChat').html("");
         }

           $('.send_message').attr('data-id',vendor);
           $('.send_message').attr('data-id_item',id_item);
           $('.send_message').attr('data-id_cart',id_cart);

         }
       });
     // $(a).attr("data-toggle", "modal");
     // $(a).attr("data-target", "#myChat");
   }

   $('.close').on('click', function(){
     $('.chat_window').removeClass('show')
     $('.chat_window').addClass('hide')
   })

</script>
@endsection
