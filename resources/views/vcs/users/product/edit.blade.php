@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              {{-- <button type="button" class="btn btn-success btn-rounded" onclick="" data-id=""><i class="fa fa-plus"></i> Tambah</button> --}}
              {{-- <a href="/su_vms/assets/users/template/template-product.xlsx" target="_blank" class="btn btn-warning btn-rounded"><i class="fa fa-download"></i> Template(xls)</a> --}}
              {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="portlet-body">
  					<form class="" action="{{URL::to('vcs/users/product/update/'.$id)}}" method="post">
  						{{ csrf_field() }}
  						<div class="form-group hide">
  							<label for="">Kategori</label>
  							<input type="text" name="" value="{{ $category }}" class="form-control" style="text-transform: uppercase;" readonly>
  						</div>
  						<div class="form-group">
  							<label for="">Suplier</label>
  							<select class="form-control" name="id_supplier">
  								@foreach($supplier as $k => $v)
  									<option value="{{$v->id}}">{{$v->vendor_name}}</option>
  								@endforeach
  							</select>
  						</div>
  						<div class="form-group">
  							<label for="">Kode</label>
  							<input type="text" name="code" value="{{$data->code}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Asal Item</label>
  							{{-- <input type="text" name="item_from" value="{{$data->item_from}}" class="form-control" required> --}}
  							<select class="form-control" name="production_origin" required>
  								<option value="0" {{($data->production_origin == 0) ? 'selected' : ''}}>Local</option>
  								<option value="1" {{($data->production_origin == 1) ? 'selected' : ''}}>Import</option>
  							</select>
  						</div>
  						<div class="form-group">
  							<label for="">Nama</label>
  							<input type="text" name="name" value="{{$data->name}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Merk</label>
  							<input type="text" name="merk" value="{{$data->merk}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Harga Retail</label>
  							<input type="number" name="price_retail" value="{{$data->price_retail}}" class="form-control money retail" oninput="PriceRetail(this)" required>
  						</div>
  						<div class="form-group">
  							<label for="">Harga Pemerintah</label> <small style="Color:red;">(* harga retail - 10% PPN & 1.5% PPH)</small>
  							<input type="number" name="price_gov" value="{{$data->price_gov}}" class="form-control money goverment" required readonly>
  						</div>
  						<div class="form-group">
  							<label for="">Biaya Kirim</label>
  							<input type="number" name="price_shipment" value="{{$data->price_shipment}}" class="form-control money" required>
  						</div>
  						<div class="form-group">
  							<label for="">Tanggal</label>
  							<input type="date" name="price_date" value="{{$data->price_date}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Tanggal Terbit</label>
  							<input type="date" name="release_date" value="{{$data->release_date}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Tanggal Kadaluarsa</label>
  							<input type="date" name="expired_date" value="{{$data->expired_date}}" class="form-control" required>
  						</div>
  						<div class="form-group">
  							<label for="">Deskripsi</label>
  							<textarea name="description" rows="8" cols="80" class="form-control" required>{{$data->description}}
  							</textarea>
  						</div>
  						<div class="form-group text-center">
  							<button type="submit" name="button" class="btn btn-sm btn-success">
  								<i class="fa fa-pencil"></i>
  								Update
  							</button>
  						</div>
  					</form>
  				</div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>

@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
<script type="text/javascript">
// -10% - 1,5%

function PriceRetail(a) {
  var value = $(a).val();
  var values = value.replace('.,','');
  var Gov = parseInt(values)- (parseInt(values*0.1)+parseInt(values*0.15))
  console.log(values);
  console.log("Gov : "+Gov);
  $('.goverment').attr('value', Gov);
}

$(document).ready(function() {
  // $('.money').mask("000.000.000.000.000.000", {reverse: true});
});
</script>
@endsection
