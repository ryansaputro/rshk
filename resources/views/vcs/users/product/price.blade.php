@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="table-responsive">
						<table class="table table-striped table-bordered" style="width: 100%">
							<thead>
								<tr style="width: 50%;">
									<th rowspan="2">Tanggal</th>
									<th colspan="3" style="text-align:center;">Harga</th>
								</tr>
								<tr>
									<th>Retail</th>
									<th>Pemerintah</th>
									<th>Biaya Kirim</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($history as $k => $v)
                  @php
                  $time = strtotime($v->price_date);
                  $function =  Fungsi::MonthIndonesia();
                  $functionDay =  Fungsi::DayIndonesia();
                  @endphp
									<tr>
										<td style="vertical-align: middle;">{{$functionDay[date('l', $time)]}}, {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_gov, 2, ",", ".") }}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_retail, 2, ",", ".") }}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_shipment, 2, ",", ".") }}</td>
									</tr>
								@endforeach
								@if (sizeof($history) == 0)
									<tr>
										<td colspan="4">Tidak ada Data </td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection
@section('js')
@endsection
