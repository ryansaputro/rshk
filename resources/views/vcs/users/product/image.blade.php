@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')
  <link href="/sa_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
	<link href="/sa_catalog/assets/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Produk</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Produk...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              <a href="#modal_add" class="btn btn-info btn-sm" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                {{-- <a href="#modal_add" class="bt btn-info btn-sm button_add" onclick="AddSpec(this)"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a> --}}
              {{-- <button type="button" class="btn btn-success btn-rounded" onclick="" data-id=""><i class="fa fa-plus"></i> Tambah</button> --}}
              {{-- <a href="/su_vms/assets/users/template/template-product.xlsx" target="_blank" class="btn btn-warning btn-rounded"><i class="fa fa-download"></i> Template(xls)</a> --}}
              {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button> --}}
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <div class="row mix-grid">
            @php
              $image_item = DB::table('image_item')->where('id_item', $id)->get();
            @endphp
            @foreach ($image_item as $key => $value)
              <div class="col-md-3 col-sm-4 mix">
                <div class="mix-inner">
                  @php
                    $gambar = $value->file;
                    $imp_gmb = explode('/', $gambar);
                  @endphp

                  <img class="img-responsive" src="{{(($imp_gmb[0] == 'https:') || ($imp_gmb[0] == 'http:')) ? $gambar : '/assets/catalog/item/'.$value->file}}" alt="">

                  <div class="mix-details">
                    <h4>Aksi</h4>
                    <a style="background: red;" class="mix-link deleteFile" data-file="{{$value->file}}" onclick="DeleteImage(this)">
                      <i class="fa fa-trash"></i>
                    </a>
                    <a class="mix-preview fancybox-button" href="/assets/catalog/item/{{ $value->file }}" title="" data-rel="fancybox-button">
                      <i class="fa fa-eye"></i>
                    </a>
                  </div>
                </div>
              </div>
            @endforeach
            @if (sizeof($image_item) == 0)
              <div class="col-md-12">
                <p>
                  <strong>Data tidak tersedia...</strong>
                </p>
              </div>
            @endif
          </div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>


  	<div class="modal fade" id="modal_add" role="dialog">
  		<div class="modal-dialog">
  			<form class="ModalForm" method="post" action="{{URL::to('vcs/users/product/image/save/'.$id)}}" enctype="multipart/form-data">
  				{{ csrf_field() }}
  				<!-- Modal content-->
  				<div class="modal-content">
  					<div class="modal-header">
  						<button type="button" class="close" data-dismiss="modal">&times;</button>
  						<h4 class="modal-title">Gambar Item Baru</h4>
  					</div>
  					<div class="modal-body">
  						<label for="">Gambar 1 <small style="color:red;">(ukuran max gambar 1Mb)</small> </label>
  						<input type="file" name="file[]" value="" class="form-control" required accept="image/x-png,image/gif,image/jpeg">
  						@for($i = 2; $i<= 3; $i++)
  						<div class="form-group">
  							<label for="">Gambar {{$i}} <small style="color:red;">(ukuran max gambar 1Mb)</small> </label>
  							<input type="file" name="file[]" value="" class="form-control" accept="image/x-png,image/gif,image/jpeg">
  						</div>
  						@endfor
  					</div>
  					<div class="modal-footer">
  						<button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
  						<button type="submit" class="btn btn-primary">Simpan</button>
  					</div>
  				</div>
  			</form>
  		</div>
  	</div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection
@section('js')
  <script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="/sa_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="/sa_catalog/assets/admin/pages/scripts/portfolio.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
    Portfolio.init();
  });

  function DeleteImage(a) {
    var r = confirm("Are You sure delete this Image?");
    var id = $(a).attr('data-file');
    if (r == true) {
      $('.deleteFile').attr('href', 'delete/'+id);
    }
  }
</script>

@endsection
