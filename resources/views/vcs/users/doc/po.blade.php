@extends('catalog.users.layouts.app_doc')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
  <style media="screen">
  @media print {
      body {transform: scale(.4);width:65%; font-size: 10px;}
      /* table {page-break-inside: avoid;} */

  }
  </style>
@endsection


@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Catalog <small class="uppercase">Purchase Order Form...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        {{-- <div class="portlet-title">
          <div class="col-xs-6 caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Purchase Order ...
            </span>
          </div>
          <div class="col-xs-6" style="text-align:right;">
            <a  href="#" class="btn btn-primary btn-sm" onclick='printDiv();'>Print</a>
          </div>
          <br>
        </div><!--/.portlet-title---> --}}
        <div class="portlet-body" id='DivIdToPrint'>
          <div class="row" style="font-weight:100; color:#000;">
            <center>

            <img style="width:100px;" class="pull-left" src="/assets/kemenkes.jpg" alt="" style="width:200px;">
            <img style="width:100px;" class="pull-right" src="https://2.bp.blogspot.com/--flp8oUCSiY/Wnb9lKct-7I/AAAAAAAAI14/QFXnonW67vktZpovpdj3Fk6NIUmRlRU9wCLcBGAs/s1600/Penerimaan%2BBesar%2BBesaran%2BPegawai%2BRS%2BJantung%2B%2526%2BPembuluh%2BDarah%2BHarapan%2BKita.gif" alt="" style="width:200px;">

              <strong>KEMENTRIAN KESEHATAN</strong><br>
              <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
              <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
              Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
              Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
              Surat Elektronik : info@pjnhk.go.i<br>
              <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a><br>

            </center>

            <hr>
            @php
              $time = strtotime($data->datetime);
              $function =  Fungsi::MonthIndonesia();
              $functionDay =  Fungsi::DayIndonesia();
              $list_propinsi  =  Fungsi::propinsi();
              $list_kota  =  Fungsi::IndonesiaProvince();
              $times = $time + strtotime("+2 months");
            @endphp



           <center><b>SURAT PESANAN</b></center>
            <br>
            <div class="pesanan-head col-xs-12" style="font-size: 12px;border:1px solid #ddd; border-radius:5px !important; padding:10px; margin-bottom:30px; margin-left:10px;margin-right:10px;width:96%;">
              <h5>Kepada Yth. : </h5>
              <div class="col-xs-5">
                <div class="col-xs-3">
                  Nama
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{$vendorDetail->vendor_name}}<br>
                </div>

                <div class="col-xs-3">
                  Alamat
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{$vendorDetail->address}}, {{$list_kota[$list_propinsi[$vendorDetail->province]][$vendorDetail->city]}} - {{$vendorDetail->post_code}}
                  <br>
                </div>

                <div class="col-xs-3">
                  Telepon
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{$vendorDetail->telephone}}<br>
                </div>

                <div class="col-xs-3">
                  Fax
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{$vendorDetail->fax}}<br>
                </div>
              </div>

              <div class="col-xs-7">
                <div class="col-xs-4">
                  No Kontrak
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                    echo $contract->no_contract;
                   ?>
                  <br>
                </div>

                <div class="col-xs-4">
                  No. SP
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  <font style="text-transform:uppercase;">{{$data->no_medik != NULL ? $data->no_medik : $data->no_non_medik}}/{{date('m', $time)}}/  <?php
                    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                      echo $mainCat;
                     ?> E-catalog{{date('Y', $time)}}</font>
                  <br>
                </div>

                <div class="col-xs-4">
                  No Dokumen
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{$vendorDetail->no_prop}}<br>
                </div>

                <div class="col-xs-4">
                  Tanggal SP
                </div>
                <div class="col-xs-1">
                  :
                </div>
                <div class="col-xs-6">
                  {{date('d', $time)}}-{{date('m', $time)}}-{{date('Y', $time)}}<br>
                </div>
              </div>
            </div>
            <br><br>

              <div class="col-xs-12">
                <h5>KAMI MENEGASKAN BAHWA ORDER KAMI SEPERTI DIBAWAH INI :</h5>
              </div>
                <div class="col-xs-12">

                  <table summary="Shopping cart" id="detailCart" class="table table-bordered">
                    <thead>
                      <tr>
                        <td>No</td>
                        <td>Tanggal</td>
                        <td>Item</td>
                        <td>Qty</td>
                        <td>Harga</td>
                        <td>Biaya Kirim</td>
                        <td>Sub Total</td>
                      </tr>
                    </thead>
                    @php
                    $totQty=0;
                    $totPrice=0;
                    @endphp
                    @foreach($dataOrder as $k => $v)
                      @php
                      $totQty += $v->qty;
                      $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
                      $totPrice += $tot;
                      @endphp
                      <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$v->datetime}}</td>
                        <td>{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
                        <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($v->price_gov, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($v->price_shipment, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($tot, 0, ',', '.')}}</td>
                      </tr>
                    @endforeach
                    <tr>
                      <td colspan="3" style="text-align:right;"><strong>Total</strong></td>
                      <td style="text-align:right;">{{number_format($totQty, 0, ',', '.')}}</td>
                      <td style="text-align:right;" colspan="3"><strong>Rp.{{number_format($totPrice, 0, ',', '.')}}</strong></td>
                    </tr>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              <hr>
              <hr>
              @php
                if($totPrice >= 200000000){
                  $aritdate =  date('d m Y', strtotime($data->datetime. "+14 days"));
                  $NextDate = explode(' ', $aritdate);

                }else{
                  $aritdate =  date('d m Y', strtotime($data->datetime. "+7 days"));
                  $NextDate = explode(' ', $aritdate);
                }
              @endphp
              <div class="col-xs-12">
                <p>Terbilang : <label for="" style="text-transform: uppercase;">{{Terbilang::make($totPrice, ' rupiah', ' ')}}</label></p>
                <h5>Dengan Ketentuan Sebagai Berikut : </h5>
                <ol>
                  <li>Surat Pesanan Diterima oleh Distributor Tanggal :  {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}} </li>
                  <li>Barang barang tersebut dikirim dan diserahkan di gudang RS jantung dan Pembuluh Darah "Harapan Kita" pada hari kerja Jam 08.00 s/d 11.00 dan jam 13.00 s/d 15.00 paling lambat tanggal : {{$NextDate[0]}} {{$function[$NextDate[1]]}} {{$NextDate[2]}}</li>
                  <li>Pengiriman dapat dilakukan bertahap maxsimal 3 (tiga) kali dalam jangka waktu pengiriman barang yang tertera di atas (poin 1 dan point 2 )</b></li>
                  <li>Batas Waktu keterlambatan pengiriman adalah 1 (satu) bulan dari batas akhir jangka waktu pengiriman barang (point 2 ) bisa barang tidak dikirim maka distributor dikenakan sanksi sesuai ketentuan yang berlaku</li>
                  <li>Bila Terjadi Keterlambatan atau kelalaian  dalam pengiriman barang bukan karena terjadinya kahar (force major), maka distributor dikenakan denda sebesar 1 o/oo (satu permil) dari nilai barang yang tidak terpenuhi / tidak dikirim untuk setiap hari keterlambatan <br></li>
                  <li>Tagihan yang di ajukan setelah lewat tahun anggaran tidak akan dibayarkan</li>
                </ol>
              </div>
            <br>

          </div>

              <div class="row">
              	<div class="col-md-12">
              				<div class="table-responsive">
                        <table style="width:100%;">
                          <tr>
                            <td style="text-align:left;">Untuk dan atas nama<br><b>RS JANTUNG DAN P.D. HARAPAN KITA</b></td>
                            <td style="text-align:left;"><b></b> <br><b>{{$vendorDetail->vendor_name}}</b> </td>
                          </tr>
                          <tr>
                            <td style="text-align:left;">Petugas Pembuat Komitmen (PPK) <br><br><br> </td>
                            <td style="text-align:left;"><br><br><br></td>
                          </tr>
                          <tr>
                            <td style="text-align:left;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            <td style="text-align:left;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                          </tr>
                          <tr>
                            <td style="text-align:left;"> <b><u>{{$directur->name}}</u></b> </td>
                            <td style="text-align:left;"> <b><u>{{$vendorDirectur->name_name_vendor_administrators}}</u></b> </td>
                          </tr>
                          <tr>
                            <td style="text-align:left;"> {{$directur->nik}} </td>
                            <td style="text-align:left;"> {{$vendorDirectur->ktp_name_vendor_administrators}} </td>
                          </tr>
                        </table>
              				</div>
              	</div>
            </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('aa','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
