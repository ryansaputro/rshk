@extends('catalog.users.layouts.app_doc')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Purchase Order Form...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="col-xs-6 caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Purchase Order ...
            </span>
          </div>
          <div class="col-xs-6" style="text-align:right;">
            <a  href="#" class="btn btn-primary btn-sm" onclick='printDiv();'>Print</a>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body" id='DivIdToPrint'>
          <div class="row" style="font-weight:100; color:#000;">
            <center>
              <strong>KEMENTRIAN KESEHATAN</strong><br>
              <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
              <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
              Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
              Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
              Surat Elektronik : info@pjnhk.go.i<br>
              <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a><br>
            </center>
            <hr>
            @php
              $time = strtotime($data->datetime);
              $function =  Fungsi::MonthIndonesia();
              $functionDay =  Fungsi::DayIndonesia();
              $times = $time + strtotime("+2 months");
              $aritdate =  date('m Y', strtotime($data->datetime. "+60 days"));
              $NextDate = explode(' ', $aritdate);
            @endphp

            <center>
              <b><u>SURAT PERINTAH KERJA (SPK)</u><b>
              <h4><b>Nomor : {{$data->no_po}}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}</b></h4>
              <br>
              <b>PEKERJAAN :</b><br>
              <b>Pengadaan Belanja Modal Alat-alat Kesehatan</b><br>
              <b>RS JANTUNG DAN P.D. HARAPAN KITA</b><br>
            </center>
            <br>
            <div class="col-xs-12"  style="font-weight:100; color:#000;">
              <p>Pada hari ini <b>{{$functionDay[date('l', $time)]}}</b> Tanggal <b>{{date('d', $time)}}</b> bulan <b>{{$function[date('m', $time)]}}</b> tahun <b>{{date('Y', $time)}}</b> kami yang bertanda tangan dibawah ini :</p>
            </div>
            <ol  style="font-weight:100; color:#000;">
              <li>
                <table class="" style="width:100%;">
                  <tr>
                    <td style="width:20%;">Nama</td>
                    <td style="width:2%;">:</td>
                    <td>{{$directur->name}}</td>
                  </tr>
                  <tr>
                    <td>Pangkat/NIP</td>
                    <td>:</td>
                    <td>{{$directur->nik}}</td>
                  </tr>
                  <tr>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td>Pejabat Pembuat Komitmen (PPK) </td>
                  </tr>
                  <tr>
                    <td>Instansi</td>
                    <td>:</td>
                    <td>{{$directur->company}}</td>
                  </tr>
                  <tr>
                    <td>NPWP</td>
                    <td>:</td>
                    <td>{{$directur->npwp}}</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{$directur->address}}<br></td>
                  </tr>
                </table>
              </li>
              <li>
                <table class="" style="width:100%;">
                <tr>
                  <td style="width:20%;">Nama</td>
                  <td style="width:2%;">:</td>
                  <td>{{$vendorDirectur->name}}</td>
                </tr>
                <tr>
                  <td>Jabatan</td>
                  <td>:</td>
                  <td>Direktur <b>{{$vendorDetail->vendor_name}}</b> </td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>:</td>
                  <td>{{$vendorDetail->vendor_name}}</td>
                </tr>
                <tr>
                  <td>NPWP</td>
                  <td>:</td>
                  <td>{{$vendorDetail->npwp}}</td>
                </tr>
                <tr>
                  <td>No. Rek Bank</td>
                  <td>:</td>
                  <td>{{$vendorDetail->no_rek}} ({{$vendorDetail->bank}})</td>
                </tr>
                <tr>
                  <td>Alamat</td>
                  <td>:</td>
                  <td>{{$vendorDetail->address}}, {{$list_kota[$list_propinsi[$vendorDetail->province]][$vendorDetail->city]}} - {{$vendorDetail->post_code}}</td>
                </tr>
              </table>
            </li>
            </ol>
            <br>
            <div class="col-xs-12" style="font-weight:100; color:#000;">
              <span style="text-indent: 40px;">Bertindak untuk dan atas nama perusahaan <b>{{$vendorDetail->vendor_name}}</b> selanjutnya disebut <b><i>PIHAK KEDUA.</i></b></span>
              Berdasarkan :
              <br>
              <span style="text-indent: 40px;">a. Paket e Purchasing (Sistem Pengadaan secara Elektronik) dengan NOMOR PESANAN <b>{{$data->no_order}}, Tanggal {{date('d', $time)}} {{$function[date('m', $time)]}} {{(date('Y', $time))}}</b> maka <b>PIHAK PERTAMA</b> Menerbitkan Surat Perintah Kerja (SPK), dengan Ketentuan sebagai berikut : </span>
            </div>
            <ol>
              <li>Lingkup Pekerjaan :
                <div class="col-xs-12">
                  <table summary="Shopping cart" id="detailCart" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Biaya Kirim</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    @php
                    $totQty=0;
                    $totPrice=0;
                    @endphp
                    @foreach($dataOrder as $k => $v)
                      @php
                      $totQty += $v->qty;
                      $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
                      $totPrice += $tot;
                      @endphp
                      <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$v->datetime}}</td>
                        <td>{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
                        <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($v->price_gov, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($v->price_shipment, 0, ',', '.')}}</td>
                        <td style="text-align:right;">Rp. {{number_format($tot, 0, ',', '.')}}</td>
                      </tr>
                    @endforeach
                    <tr>
                      <td colspan="3" style="text-align:right;"><strong>Total</strong></td>
                      <td style="text-align:right;">{{number_format($totQty, 0, ',', '.')}}</td>
                      <td style="text-align:right;" colspan="3"><strong>Rp.{{number_format($totPrice, 0, ',', '.')}}</strong></td>
                    </tr>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </li>
              <li>Pembayaran dengan sumber Dana APBD Perubahan  bla bla.</li>
              <li>Jangka Waktu Pelaksanaan 60 (Enam puluh) hari kalender, tanggal mulai kerja yaitu mulai {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}} sampai dengan {{date('d', $time)}} {{$function[$NextDate[0]]}} {{$NextDate[1]}}.</li>
              <li>Instruksi kepada <b> <i>PIHAK KEDUA</i> :</b>
                <ul>
                  <li>Pembayarannya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini dan dibuktikan dengan Berita Acara Serah Terima Pekerjaan / barang.</li>
                  <li>Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan karena kesalahan/kelalaian penyedia, maka penyedia berkewajiban untuk membayar denda kepada Kas Daerah 1/1000 (seperseribu) dari nilai barang yang belum tersuplai sebelum PPN setiap hari kalender keterlambatan.</li>
                  <li>Selain tunduk kepada ketentuan dalam SPK ini, penyedia berkewajiban untuk mematuhi standar ketentuan dan syarat umum SPK ini.</li>
                  <li>SPK ini mulai berlaku efektif terhitung sejak tanggal diterbitkannya SP dan Penyelesaian keseluruhan pekerjaan sebagaimana diatur dalam SPK ini.</li>
                </ul>
              </li>
            </ol>
            <br>
            <div class="col-xs-12">
              <p>
                Demikianlah Surat Perintah Kerja ini dibuat dengan sebenernya pada hari ini, tanggal dan bulan tersebut diatas dalam rangkap 4 (empat),
                2 lembar dibubuhi materi yang berlaku dan mempunyai kekuatan hukum yang sama.
              </p>
            </div>
          </div>

              <div class="row">
              	<div class="col-md-12">
              				<div class="table-responsive">
                        <table style="width:100%;">
                          <tr>
                            <td style="text-align:center;">Untuk dan atas nama<br><b>RS JANTUNG DAN P.D. HARAPAN KITA</b></td>
                            <td style="text-align:center;">Untuk dan atas nama Penyedia <br><b>{{$vendorDetail->vendor_name}}</b> </td>
                          </tr>
                          <tr>
                            <td style="text-align:center;">Petugas Pembuat Komitmen (PPK) <br><br><br> </td>
                            <td style="text-align:center;"><br><br><br></td>
                          </tr>
                          <tr>
                            <td style="text-align:center;"> <img src="http://puslit.mercubuana.ac.id/wp-content/uploads/2015/05/ttd-anik-cap.jpg" alt="" style="width:200px;"> </td>
                            <td style="text-align:center;"> <img src="http://puslit.mercubuana.ac.id/wp-content/uploads/2015/05/ttd-anik-cap.jpg" alt="" style="width:200px;"> </td>
                          </tr>
                          <tr>
                            <td style="text-align:center;"> <b><u>{{$directur->name}}</u></b> </td>
                            <td style="text-align:center;"> <b><u>{{$vendorDirectur->name}}</u></b> </td>
                          </tr>
                          <tr>
                            <td style="text-align:center;"> {{$directur->nik}} </td>
                            <td style="text-align:center;"> {{$vendorDirectur->nik}} </td>
                          </tr>
                        </table>
              				</div>
              	</div>
            </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
