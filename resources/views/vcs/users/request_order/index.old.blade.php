@extends('vms.users.layouts.app')

@section('title')
  VMS
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>VMS <small class="uppercase">Permintaan Barang...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">List</span>
            <span class="caption-helper uppercase">Permintaan Barang...</span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
              <button type="button" class="btn btn-info btn-rounded" onclick="" data-id=""><i class="fa fa-pencil"></i> Ubah</button>
            </div>
          </div>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <table class="table table-bordered table-striped" style="width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th>Barang yang dipesan</th>
                <th>Jumlah yang dipesan</th>
                <th>Tracking status</th>
                <th>Stok barang</th>
                <th>Dokumen Pengusul</th>
              </tr>
            </thead>
            <tbody>
              @foreach($order as $k =>$v)
                @php
                  $id_proposer = md5($v->id_proposer);
                @endphp
                <tr>
                  <td>{{$k+1}}</td>
                  <td>
                    {{$v->code}}<br>
                    {{$v->name}}<br>
                    {{$v->merk}}
                  </td>
                  <td>{{$v->qty}}</td>
                  <td>
                    @php
                      $status = $v->status_order;
                    @endphp
                    <span style="display: none;">{{ $status }}</span>
                    @if ($status == 0)
                      <span class="label bg-grey" style="display: block;">waiting approval</span>
                    @elseif ($status == 1)
                      <span class="label bg-yellow" style="display: block;">approve suvervisor</span>
                    @elseif ($status == 2)
                      <span class="label bg-blue" style="display: block;">approve manager</span>
                    @elseif ($status == 3)
                      <span class="label bg-green" style="display: block;">approve directur</span>
                    @elseif ($status == 4)
                      <span class="label bg-purple" style="display: block;">approve finance</span>
                    @elseif ($status == 5)
                      <span class="label bg-purple" style="display: block;">success payment</span>
                    @else
                      <span class="label bg-red" style="display: block;">reject order</span>
                    @endif
                  </td>
                  <td>100 pcs</td>
                  <td> <a target="_blank" href="{{URL::to('vcs/users/proposer/download/'.$id_proposer)}}" class="btn btn-primary">Unduh</a> </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">

</script>
@endsection
