@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Form Kontrak...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Kontrak ...
            </span>
          </div>
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              {{-- <a href="{{URL::to('vcs/users/adendum/create')}}"  class="btn btn-primary btn-rounded"></i> Adendum Baru</a> --}}
            </div>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet light">
          <div class="portlet-body">
            <table class="table table-bordered table-striped" style="width: 100%;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Kontrak</th>
                  <th>Tanggal Berlaku</th>
                  <th>Tanggal Berakhir</th>
                  <th>Deskripsi</th>
                  <th>Data</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                @if (count($data) > 0)
                  @foreach ($data as $k => $v)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>
                        {{$v->no_adendum}}
                      </td>
                      <td>{{$v->start_date}}</td>
                      <td>{{$v->end_date}}</td>
                      <td>{{$v->description}}</td>
                      <td>{{$v->data}}</td>
                      <td><span class="label bg-{{$v->status == 1 ? 'green' : 'red'}}">{{$v->status == 1 ? 'berlaku' : 'kadaluarsa'}}</span></td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- BEGIN PAGINATOR -->
            {{-- <div class="row">
              <div class="col-md-5 col-sm-5">
                <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
              </div>
                <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
            </div> --}}
            <!-- END PAGINATOR -->
          </div><!--/.portlet-body-->
        </div>
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
@endsection
