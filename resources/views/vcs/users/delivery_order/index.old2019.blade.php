@extends('vcs.users.layouts.app')

@section('title')
  Katalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Katalog <small class="uppercase">Form Pengiriman Barang...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Kirim Barang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('vcs/users/submitDo')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="no_po" class="col-md-4 control-label">No SP</label>
                <div class="col-md-8">
                  @php
                  $time = strtotime($po->datetime);
                  @endphp
                  <input type="hidden" name="id_po" value="{{$po->id_order}}">
                  <input type="hidden" name="rs" value="Rumah Sakit Jantung Harapan Kita">
                  <input style="margin-bottom:10px; text-transform:uppercase;" id="no_po" readonly type="text" class="form-control input-sm" name="no_po" value="{{$po->no_medik != NULL ? $po->no_medik : $po->no_non_medik}}/{{date('m', $time)}}/{{$po->name_category}} E-CATALOG{{date('Y', $time)}}" required autofocus>
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <label for="destination" class="col-md-4 control-label">Tujuan</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="destination" type="text" class="form-control input-sm" name="rs" value="Rumah Sakit Jantung Harapan Kita" required>
                </div>
              </div>

              <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <textarea name="address" style="margin-bottom:10px; height:100px;" class="form-control input-sm"> Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420</textarea>
                </div>
              </div>
              </div>

            <div class="col-md-6">
              <div class="form-group{{ $errors->has('pengirimanBy') ? ' has-error' : '' }}">
                <label for="pengirimanBy" class="col-md-4 control-label">Pengiriman Oleh</label>
                <div class="col-md-8">
                  <select class="form-control input-sm pengirimanBy" required style="margin-bottom:10px;"  name="sent_by">
                    {{-- <option disabled selected>-pilih-</option> --}}
                    <option value="1">Pribadi</option>
                    {{-- <option value="0">Kurir</option> --}}
                  </select>
                  @if ($errors->has('pengirimanBy'))
                    <span class="help-block">
                      <strong>{{ $errors->first('pengirimanBy') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="pengiriman">
                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">Sopir</label>
                  <div class="col-md-8">
                    <input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">Mobil</label>
                  <div class="col-md-8">
                    <input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">No. Polisi</label>
                  <div class="col-md-8">
                    <input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemKirim">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Deskripsi</th>
                    <th>Jumlah SP</th>
                    <th>Jumlah Kirim</th>
                    <th>Harga Satuan</th>
                    <th>Satuan</th>
                    <th>Total</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($dataPO as $k => $v)
                    <tr id='tr_{{$v->id}}' data-id='{{$v->id}}'>
                      <td>{{$v->code}}</td>
                      <td>{{$v->name}}</td>
                      <td>{{number_format($v->qty, 0, ',', '.')}}</td>
                      <td>
                        @if (array_key_exists($v->id_item,$do))
                          <input type="number" min="0" class="form-control qty qty_{{$v->id_item}}" oninput="InputQty(this)" max="{{$v->qty-$do[$v->id_item]}}" name="qtyDO[]" value="{{$v->qty-$do[$v->id_item]}}" data-id="{{$v->id_item}}">
                        @else
                          <input type="number" min="0" class="form-control qty qty_{{$v->id_item}}" oninput="InputQty(this)" max="{{$v->qty}}" name="qtyDO[]" value="{{$v->qty}}" data-id="{{$v->id_item}}">
                        @endif
                          <input type="hidden" name="idDO[]" value="{{$v->id_item}}">
                          <input type="hidden" name="satuanDO[]" value="{{$v->id_satuan}}">
                      </td>
                      <td>
                        {{number_format($v->price_gov, 2, ',', '.')}}
                        <input type="hidden" name="price_pcs" value="{{$v->price_gov}}" class="price_pcs" data-id="{{$v->id_item}}">
                      </td>
                        <td>{{$v->unit_name}} </td>
                      <td class="total total_{{$v->id_item}}">
                          {{number_format($v->qty*$v->price_gov, 2, ',', '.')}}
                      </td>
                      <td>
                          <input type="text" name="keteranganDO[]" class="form-control" value="">
                      </td>
                      <td><button type="button" class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='{{$v->id}}'><i class='fa fa-times' aria-hidden='true'></i></button></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="col-md-offset-10 col-sm-2">
                <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button">Submit</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>

    <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
              <div style="overflow-x:auto;">
                <table class="table table-bordered">
                  <tr>
                    <th>Pilih</th>
                    <th>Item</th>
                    <th>Deskripsi</th>
                  </tr>
                  @foreach ($dataItem as $key => $value)
                    <tr>
                      <td> <input type="checkbox" class="form-control input-sm" name="pilih" value=""> </td>
                      <td> {{$value->name}} </td>
                      <td> {{$value->description}} </td>
                    </tr>
                    {{-- <option data-id="{{$value->id}}" value="{{$value->id_item}}">{{$value->name}}</option> --}}
                  @endforeach
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    @endsection
@section('note')
  2018 &copy; RSJPDHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  var vendorData = $(a).attr('data-vendor');
  var AllSubtot = 0;

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
            total = (nilai*price);
        $('td.total_'+$(a).attr('data-id')).html(addCommas(Math.ceil(total)));
      }
    });
}

var satuan_kirim = "";
var id_satuan_kirim = "";
function Convert(a) {
  $('input.sisa_hasil').val(0)
  $('input.sisa_satuan').val("PCS")
  $('input.hasil').val(0)
  $('input.satuan').val("-")
  var qty_per_pcs = $('#qty').val();
  var convert_pcs_to = $('#satuan').find(':selected').attr('data-convert');
  satuan_kirim = $('#satuan').find(':selected').text();
  id_satuan_kirim = $('#satuan').find(':selected').val();
  sisa = qty_per_pcs%convert_pcs_to;
  nilai = Math.floor(qty_per_pcs/convert_pcs_to);
  $('.after_convert').css('display', 'inline');
  if(sisa != 0){
    $('input.sisa_hasil').val(sisa)
    $('input.sisa_satuan').val("PCS")
  }
  $('input.hasil').val(nilai)
  $('input.satuan').val($('#satuan').find(':selected').text())
  $('#satuan option').prop('selected', function() {
       return this.defaultSelected;
   });

}


$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
      $( ".sisa_hasil" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
});


$('.pengirimanBy').on('change', function(){
  $('.pengirimanBy option').each(function(k,v){
    if($(v).is(':selected')){
      if($(this).val() == 0){
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Nama Kurir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No Resi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }else{
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Sopir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Mobil</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No. Polisi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }
    }
    $('.pengiriman').css('display','inline');
    $('.pengiriman').css('margin-top','30px');
    $('.pengiriman').html(data);
  });

});

$(document).ready(function(){
  // $('.pengiriman').hide();

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

  $(".choiceChosen, .productChosen").chosen({});

});

var name = "";
var id = "";
$('#item').on('change', function(){
  id = $('#item :selected').attr('data-id');
  id_item = $(this).val();
  name =$('#item :selected').text();
  $.ajax({
      url: '{{URL::to('vcs/users/detailItem')}}',
      method: 'POST',
      data: {"_token": "{{ csrf_token() }}", "id": id, "id_item" : id_item},
      success: function (a) {
        $('#qty').attr('value', a.qty);
        if(a.qty > 0){
          $('#qty').attr('max', a.qty);
          $('#qty').attr('min', 1);
        }else{
          $('#qty').attr('max', 0);
          $('#qty').attr('min', 0);

        }

        $.each($('#satuan option'), function(k,v){
          if (a.satuan.satuan == $(v).val()) {
            $(this).attr('selected','selected');
          }
        });

      }
  });
});
var arr = [];
function AddItem(a) {
  if(name !== ''){
    if(satuan_kirim == ''){
      satuan_kirim = $('#satuan').find(':selected').text();
      id_satuan_kirim = $('#satuan').find(':selected').val();
    }
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    // "<td>"+$('#satuan :selected').text()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan :selected').val()+"'></td>"+
    "<td>"+satuan_kirim+"<input type='hidden' name='satuanDO[]' value='"+id_satuan_kirim+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

  // $("#qty").val(0)
  $(".hasil").val(0)
  $(".satuan").val("")
  $(".sisa_hasil").val(0)
  $("#keterangan").val("")
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
</script>
@endsection
