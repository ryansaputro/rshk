<?php
use Fungsi as Fungsi;
use App\Model\VendorStatus;
use App\Model\VendorDetail;
use Illuminate\Support\Facades\Auth;

$vendor = DB::table('vendor_status')->select('vendor_status.last_status')->join('vendor_detail', 'vendor_status.id_vendor_detail', '=', 'vendor_detail.id')->where('vendor_detail.id_user', Auth::user()->id)->value('last_status');
$menus = Fungsi::menu();
$arraySubMenu[0] = $menus;
?>
<ul class="nav navbar-nav">
    <li><a href="{!! URL::to('vcs/users/dashboard') !!}"><i class="fa fa-tachometer" aria-hidden="true"></i>
            Beranda</a></li>
    <li><a href="{{URL::to('vcs/users/percakapan')}}"><i class="fa fa-comments" aria-hidden="true"></i></i> Percakapan</a></li>
    <li><a href="{{URL::to('vcs/users/request_order')}}"><i class="fa fa-line-chart" aria-hidden="true"></i> Lacak Pesanan</a></li>
    @if ($vendor != 0)
    <li>
        <a href="{{URL::to('vcs/users/product')}}"><i class="fa fa-archive" aria-hidden="true"></i> Produk</a>
    </li>
    @endif
    <li><a href="{{URL::to('vcs/users/track_item')}}"><i class="fa fa-truck" aria-hidden="true"></i> Lacak Barang</a></li>
    <li><a href="{{URL::to('vcs/users/contract')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> Kontrak</a></li>
</ul>
