<?php
use Fungsi as Fungsi;
$menus = Fungsi::ActivatorMenu();
?>
<ul class="nav navbar-nav">
    <li>
      <a href="{!! URL::to('vms/activator/dashboard') !!}">Dashboard</a>
    </li>
    @php
    $arrayMenu = array(
                          'Vendor',
                          // 'Lulus Uji Berkas'
                        );
    $arraySubMenu[0] = $menus;
    @endphp

  @foreach ($arrayMenu as $key => $value)
  <li class="menu-dropdown classic-menu-dropdown ">
    @isset($arraySubMenu[$key])
      <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="{{ str_replace(' ', '_', strtolower($value)) }}" aria-expanded="false">
    {{ $value }}  <i class="fa fa-angle-down"></i>
    @else
      <a class="has-arrow waves-effect waves-dark" href="{{ str_replace(' ', '_', strtolower($value)) }}" aria-expanded="false"> {{ $value }}
    @endisset
    </a>
    @isset($arraySubMenu[$key])
      <ul class="dropdown-menu pull-left">
        @foreach ($arraySubMenu[$key] as $key1 => $value1)
          <li><a href="{{ str_replace(' ', '_', strtolower($value1)) }}">{{ $value1 }}</a></li>
        @endforeach
      </ul>
    @endisset
  </li>
@endforeach  {{-- ex multi level dd --}}
  {{-- <li class="menu-dropdown classic-menu-dropdown ">
    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="#">
      Extra <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
      <li class=" dropdown-submenu">
        <a href="#">
          <i class="icon-briefcase"></i> Data Tables
        </a>
        <ul class="dropdown-menu">
          <li class="">
            <a href="table_basic.html">
              Basic Datatables
            </a>
          </li>
          <li class="">
            <a href="table_tree.html">
              Tree Datatables
            </a>
          </li>
          <li class="">
            <a href="table_responsive.html">
              Responsive Datatables
            </a>
          </li>
          <li class="">
            <a href="table_managed.html">
              Managed Datatables
            </a>
          </li>
          <li class="">
            <a href="table_editable.html">
              Editable Datatables
            </a>
          </li>
          <li class="">
            <a href="table_advanced.html">
              Advanced Datatables
            </a>
          </li>
          <li class="">
            <a href="table_ajax.html">
              Ajax Datatables
            </a>
          </li>
        </ul>
      </li>
      <li class=" dropdown-submenu">
        <a href="#">
          <i class="icon-wallet"></i> Portlets
        </a>
        <ul class="dropdown-menu">
          <li class="">
            <a href="portlet_general.html">
              General Portlets
            </a>
          </li>
          <li class="">
            <a href="portlet_general2.html">
              New Portlets #1 <span class="badge badge-roundless badge-danger">new</span>
            </a>
          </li>
          <li class="">
            <a href="portlet_general3.html">
              New Portlets #2 <span class="badge badge-roundless badge-danger">new</span>
            </a>
          </li>
          <li class="">
            <a href="portlet_ajax.html">
              Ajax Portlets
            </a>
          </li>
          <li class="">
            <a href="portlet_draggable.html">
              Draggable Portlets
            </a>
          </li>
        </ul>
      </li>
      <li class=" dropdown-submenu">
        <a href="#">
          <i class="icon-bar-chart"></i> Charts </a>
        <ul class="dropdown-menu">
          <li class="">
            <a href="charts_amcharts.html">
              amChart
            </a>
          </li>
          <li class="">
            <a href="charts_flotcharts.html">
              Flotchart
            </a>
          </li>
        </ul>
      </li>
      <li class=" dropdown-submenu">
        <a href="#">
          <i class="icon-pointer"></i> Maps
        </a>
        <ul class="dropdown-menu">
          <li class="">
            <a href="maps_google.html">
              Google Maps </a>
          </li>
          <li class="">
            <a href="maps_vector.html">
              Vector Maps
            </a>
          </li>
        </ul>
      </li>
      <li class=" dropdown-submenu">
        <a href="#">
          <i class="icon-puzzle"></i> Multi Level
        </a>
        <ul class="dropdown-menu">
          <li class="">
            <a href="#">
              <i class="icon-settings"></i> Item 1
            </a>
          </li>
          <li class="">
            <a href="#">
              <i class="icon-user"></i> Item 2
            </a>
          </li>
          <li class="">
            <a href="#">
              <i class="icon-globe"></i> Item 3
            </a>
          </li>
          <li class=" dropdown-submenu">
            <a href="#">
              <i class="icon-folder"></i> Sub Items
            </a>
            <ul class="dropdown-menu">
              <li class="">
                <a href="#">
                  Item 1
                </a>
              </li>
              <li class="">
                <a href="#">
                  Item 2
                </a>
              </li>
              <li class="">
                <a href="#">
                  Item 3
                </a>
              </li>
              <li class="">
                <a href="#">
                  Item 4
                </a>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="#">
              <i class="icon-share"></i> Item 4
            </a>
          </li>
          <li class="">
            <a href="#">
              <i class="icon-bar-chart"></i> Item 5
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </li> --}}
</ul>
