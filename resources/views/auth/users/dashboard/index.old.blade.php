@extends('auth.users.layouts.app')

@section('title')
  Login
@endsection

@section('css')

@endsection

@section('logo')
  <!-- <a href="{!! route('x') !!}">
	  <img src="/assets/logo.png" alt="" id="logo_company" class="img-thumbnail" style="height: 150px; border-radius: 75px;"/>
    {{-- <span class="fa fa-home fa-5x"></span> --}}
	</a> -->
@endsection

@section('content')




  <!-- BEGIN LOGIN FORM -->
    <form class="form-horizontal form-material" id="loginform" action="/auth" method="POST">

      @if(session('message'))
              <div class='alert alert-danger'>
                     {{session('message')}}
              </div>
      @endif
      {{ csrf_field() }}
  		<!-- <h3 class="form-title">
        Form <small>Login...</small>
      </h3> -->
      <center><img src="/assets/logo.png" style="width:150px;"></center>

  		<div class="alert alert-danger display-hide">
  			<button class="close" data-close="alert"></button>
  			<span>
  			Enter any username and password. </span>
  		</div>
  		<div class="form-group">
  			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  			<label class="control-label visible-ie8 visible-ie9">Username</label>
  			<div class="input-icon">
  				<i class="fa fa-user"></i>
                  <input id="email" type="email" class="form-control placeholder-no-fix" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
  			</div>
  		</div>
  		<div class="form-group">
  			<label class="control-label visible-ie8 visible-ie9">Password</label>
  			<div class="input-icon">
  				<i class="fa fa-lock"></i>
            <input id="password" type="password" class="form-control placeholder-no-fix"  name="password" required placeholder="Password">
  			</div>
  		</div>

                        {{-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 pull-left">
                                <div class="checkbox">
                                    <label style="color:black">
                                        <input  type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> --}}

      <div class="form-group text-center">
  			<button type="submit" class="btn btn-block btn-primary tooltips" data-placement="bottom" data-original-title="Login" style="width: 100%;">
  			  Login
  			</button>
  		</div>
  	</form>


    {{-- <div class="row text-center">
      <div class="col-md-6">
        <a href="{!! route('x.vms.register') !!}" class="btn btn-lg btn-info" data-placement="bottom" data-original-title="Register" style="width: 100%;">
          Register
        </a>
      </div>
      <div class="col-md-6">
        <a href="{!! route('x.vms.activation') !!}" class="btn btn-lg purple" data-placement="bottom" data-original-title="Activation" style="width: 100%;">
          Activation
        </a>
      </div>
    </div> --}}
	<!-- END LOGIN FORM -->
@endsection

@section('note')
  2018 &copy; RSHK.
@endsection

@section('js')
<script type="text/javascript">
  $('#email').on('input', function(){
    var character = $(this).val().length;
    var url = "{{URL::to('login/image')}}";
    if(character >= 10){
      $.ajax({
        type: "post",
        url: url,
        data: {
            "_token": "{{ csrf_token() }}",
            "email": $(this).val(),
            },
          success: function (a) {
            if(a.image !== 'xxx'){
              $('#logo_company').attr('src','/su_vms/assets/users/company_image/'+a.image);
            }else{
              $('#logo_company').attr('src','/assets/logo.png');
            }
          }
      });
    }
  });
</script>
@endsection
