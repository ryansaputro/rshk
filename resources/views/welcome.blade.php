<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PT. Nusamart Aulia Mandiri</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #ffffff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    Pusat Jantung Nasional RSHK
                </div>
                <div class="title m-b-md">
                    PT. Nusamart Aulia Mandiri
                </div>

                <div class="links">
                    <a href="{!! route('x.auth') !!}">LOGIN</a>
                    <a href="{!! route('x.vms.register') !!}">VMS REGISTER</a>
                    <a href="{!! route('x.vms.activation') !!}">VMS ACTIVATION</a>
                    <a href="{!! URL::to('vms/users/dashboard') !!}">VMS USER</a>
                    <a href="{!! URL::to('vms/admin/dashboard') !!}">VMS ADMIN</a>
                    <a href="{!! route('x.catalog.guest') !!}">CATALOG GUEST</a>
                    <a href="{!! route('x.catalog.users') !!}">CATALOG USER</a>
                    <a href="{!! route('x.catalog.admin') !!}">CATALOG ADMIN</a>
                </div>
                <div class="links" style="margin-top: 50px;">
                  {{(Auth::check()) ? Auth::user()->id : 'Information : You Are Not Login...'}}
                </div>
            </div>
        </div>
    </body>
</html>
