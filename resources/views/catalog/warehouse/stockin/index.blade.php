@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Stok Masuk</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
            <div class="col-sm-6 col-sm-6" style="float:left;">
              {{-- <button type="button" class="btn btn-info btn-rounded btn-sm" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button>
              <a href="/su_catalog/assets/users/template/template-inventory.xlsx" target="_blank" class="btn btn-warning btn-rounded btn-sm"><i class="fa fa-download"></i> Template(xls)</a> --}}
            </div>
            {{-- <form>
              <div class="col-sm-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                  <option value="status">Status</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form> --}}
          </div>
          <br>
          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Masuk</th>
                  <th>Gudang</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Petugas</th>
                </tr>
              </thead>
              @if (count($data) > 0)
                @foreach ($data as $k => $v)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->datetime}}</td>
                    <td>{{$v->nama_instalasi}}</td>
                    <td>{{$v->code}} - {{$v->name}}</td>
                    <td>{{$v->id_qty}} {{$v->unit_name}}</td>
                    <td>{{$v->username}}</td>
                  </tr>
                  @endforeach
              @else
                <tr>
                  <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
                </tr>
              @endif
          </table>
        </div>
      </div>
      <!-- BEGIN PAGINATOR -->
      {{-- <div class="row">
        <div class="col-md-5 col-sm-5">
          <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
        </div>
          <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
      </div> --}}
      <!-- END PAGINATOR -->
    </div>
    <div class="modal"></div>
  </div>

@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready( function () {
    $('#mainCart').DataTable();
  } );
  </script>
@endsection
