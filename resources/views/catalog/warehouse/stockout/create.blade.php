@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('content')
  @php
    $romanFormat = Fungsi::MonthRoman();
  @endphp
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Pengeluaran Barang Gudang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('/catalog/warehouse/stockout/create')}}" novalidate method="post">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="id_order" class="col-md-2 control-label">No Permintaan</label>
                <div class="col-md-7">
                  <select class="form-control input-sm" name="id_order" onchange="changeRequest(this)" style="margin-bottom:10px;">
                    @foreach ($dataQuery as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->id_order}}" data-id="{{$v->id}}" {{isset($id_order) == $v->id_order ? 'selected' : '' }}>{{date('y', $time)}}{{date('m', $time)}}{{date('d', $time)}}-ORD{{$v->no_request}}-{{$v->kode_instalasi}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('id_order'))
                    <span class="help-block">
                      <strong>{{ $errors->first('id_order') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="col-md-3">
                  <button type="submit" name="button" class="btn btn-sm btn-primary btn-block">Cek</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <br><br>
        <hr>
      {{-- </div><!--/.portlet-body-->
    </div><!--/.portlet-light--> --}}


{{-- <div class="row">
  <div class="col-md-12">
    <div class="portlet-light">
      <div class="portlet-body"> --}}
        <div class="row">
          <div class="col-md-12">
            <form class="" action="{{URL::to('/catalog/warehouse/stockout/store')}}" novalidate method="post">
              {{ csrf_field() }}
              @if (isset($dataOrder))

                <div class="row" style="margin-top:20px; font-weight:100; color:#000;">
                  <center>
                    <img style="width:12%;" class="pull-left" src="/assets/kemenkes.jpg" alt="" style="width:200px;">
                    <img style="width:12%;" class="pull-right" src="https://2.bp.blogspot.com/--flp8oUCSiY/Wnb9lKct-7I/AAAAAAAAI14/QFXnonW67vktZpovpdj3Fk6NIUmRlRU9wCLcBGAs/s1600/Penerimaan%2BBesar%2BBesaran%2BPegawai%2BRS%2BJantung%2B%2526%2BPembuluh%2BDarah%2BHarapan%2BKita.gif" alt="" style="width:200px;">

                    <strong>KEMENTRIAN KESEHATAN</strong><br>
                    <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
                    <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
                    Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
                    Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
                    Surat Elektronik : info@pjnhk.go.id <br>
                    <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a><br>

                  </center>

                  <hr>
                  @php
                  $time = strtotime($data->datetime);
                  $function =  Fungsi::MonthIndonesia();
                  $functionDay =  Fungsi::DayIndonesia();
                  $list_propinsi  =  Fungsi::propinsi();
                  $list_kota  =  Fungsi::IndonesiaProvince();
                  $times = $time + strtotime("+2 months");
                  $aritdate =  date('d m Y', strtotime($data->datetime. "+12 days"));
                  $NextDate = explode(' ', $aritdate);
                  @endphp



                  <center><b>SURAT PERMINTAAN</b></center>
                  <br>
                  <div class="col-md-12">
                    <table class="table table-bordered">
                      <tr>
                        <td>Nama </td>
                        <td width="1px">:</td>
                        <td>{{$vendorDetail->vendor_name}}</td>

                        <td>NO Kontrak </td>
                        <td>:</td>
                        <td> :{{$contract->no_contract}}</td>
                      </tr>
                      <tr>
                        <td rowspan="2">Alamat </td>
                        <td width="1px"  rowspan="2">:</td>
                        <td rowspan="2"> {{$vendorDetail->address}}, {{$list_kota[$list_propinsi[$vendorDetail->province]][$vendorDetail->city]}} - {{$vendorDetail->post_code}} </td>

                        <td>NO SP </td>
                        <td width="1px">:</td>
                        <td>{{$data->no_po}}/{{date('m', $time)}}/{{$mainCat}}/Ecatalog/{{date('Y', $time)}}</td>
                      </tr>
                      <tr>
                        <td>NO Dokumen </td>
                        <td width="1px">:</td>
                        <td> - </td> <!--004/11/Obat/Ecatalog2018-->


                      </tr>
                      <tr>
                        <td>Telepon </td>
                        <td width="1px">:</td>
                        <td>{{$vendorDetail->telephone}} </td>


                        <td>Tanggal SP  </td>
                        <td width="1px">:</td>
                        <td>{{date('d', $time)}}-{{date('m', $time)}}-{{date('Y', $time)}}</td>
                      </tr>
                      <tr>
                        <td>Tipe Permintaan </td>
                        <td width="1px">:</td>
                        <td>
                          {{$userRequests->request_type == '1' ? 'Keluar (BHP/Inventaris)' : '-'}}
                          {{-- <select class="form-control" name="request_type">
                          <option value="1">Keluar (BHP/Inventaris)</option>
                        </select> --}}
                      </td>
                      <td> Gudang Pemberi </td>
                      <td width="1px">:</td>
                      <td>
                        {{$categoryWarehouse->nama_instalasi}}
                        <input type="hidden" name="id_gudang" value="{{$categoryWarehouse->id}}">
                      </td>
                    </tr>
                  </table>
                </div>

                <div class="col-xs-12">
                    <table summary="Shopping cart" id="detailCart" class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tanggal</th>
                          <th>Item</th>
                          <th>Qty Permintaan</th>
                          <th>Qty Diberi</th>
                          <th>Satuan</th>
                          <th>Harga Satuan</th>
                          <th>Total Harga</th>
                        </tr>
                      </thead>
                      @php
                        $totQty=0;
                        $totPrice=0;
                      @endphp

                      @foreach($dataOrder as $k => $v)
                        @php
                          $totQty += $v->qty;
                          $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
                          $totPrice += $tot;
                        @endphp
                        <tr>
                          <td>{{$k+1}}</td>
                          <td>{{$v->datetime}}</td>
                          <td><input type="hidden" name="id_item[]" value="{{$v->id_item}}">{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
                          <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
                          <td> <input type="number" oninput="ChangeQty(this)" data-item="{{$v->id_item}}" name="qty[]" class="form-control input-sm" value="{{$v->qty}}" required max="{{$v->qty}}" min="0" style="width:100%;"> </td>
                          <td>
                              <select class="form-control" name="satuan[]">
                                @foreach ($satuan as $k => $s)
                                  @if(array_key_exists($v->id_item, $detailUserSatuan))
                                    <option value="{{$s->id}}" {{$detailUserSatuan[$v->id_item] == $s->unit_name ? 'selected' : ''}}>{{$s->unit_name}}</option>
                                  @endif
                                @endforeach
                              </select>
                          </td>
                          <td style="text-align:right;" class="price_{{$v->id_item}}" data-price="{{$v->price_gov}}"> <input type="hidden" name="price[]" value="{{$v->price_gov}}"> {{number_format($v->price_gov, 0, ',', '.')}}</td>
                          <td style="text-align:right;" class="tot_{{$v->id_item}}">{{number_format(($v->qty*$v->price_gov), 0, ',', '.')}}</td>
                        </tr>
                      @endforeach
                      <tbody>
                      </tbody>
                      <input type="hidden" name="id_user" value="{{$peminta->id_user}}">
                      <input type="hidden" name="id_user_request" value="{{$userRequests->id}}">
                    </table>
                </div>
                <div class="row hide">
                  <div class="col-md-2">
                        <div class="table-responsive">
                          <table style="width:100%;">
                            <tr>
                              <td style="text-align:right;"><b style="text-transform:uppercase;">{{$peminta->nama_instalasi}}</b> </td>
                            </tr>
                            <tr>
                              <td style="text-align:right;"><br><br><br></td>
                            </tr>
                            <tr>
                              <td style="text-align:right;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                              <td style="text-align:right;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            </tr>
                            <tr>
                              {{-- <td style="text-align:center;"> <b><u>{{$supervisi->name}}</u></b> </td> --}}
                              <td style="text-align:right;"> <b><u>{{$peminta->username_catalog}}</u></b> </td>
                            </tr>
                            <tr>
                              {{-- <td style="text-align:center;"> {{$supervisi->nip}} </td> --}}
                              <td style="text-align:right;"> {{$peminta->nip}} </td>
                            </tr>
                          </table>
                        </div>
                  </div>
            </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-sm-10">
                    <font>
                      <b style="text-transform:uppercase;">peminta : </b>
                    </font>
                     <br>
                     <font style="text-transform:uppercase;">{{$peminta->nama_instalasi}}</font>-
                    {{$peminta->username_catalog}} ({{$peminta->nip}})
                  </div>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-block" name="button">Setujui</button>
                  </div>

                </div>
              </div>
              </div>
            @else
              <center>
                <h4>Tidak ada Data</h4>
              </center>
            @endif

          {{-- </div><!--/.col-md-12-->
        </div><!--/.row-->
      </div><!--/.portlet-body-->
    </div><!--/.portlet-light--> --}}
        </form>
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">
function ChangeQty(a) {
  var input = $(a).val();
  var maxInput = $(a).attr('max');
  var idItem = $(a).attr('data-item');
  var price = $('.price_'+idItem).attr('data-price');
  var total = input*price;
  $('td.tot_'+idItem).html(addCommas(total))
}

function changeRequest(a) {
  var id_request = $(a).find(':selected').attr('data-id');
}

var name = "";
var id = "";

function poNumber(a) {
  id_do = $(a).val();
  $.ajax({
    url: '{{URL::to('catalog/pphp/bast/receive/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_do" : id_do},
    success: function (a) {
      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);

      var table = "";
      $.each(a.data, function(k,v){
        var total = parseInt(v.qty_kirim)*parseInt(v.price_gov);
        table += '<tr>'+
                    '<td>'+v.code+'</td>'+
                    '<td>'+v.name+'</td>'+
                    '<td>'+addCommas(v.qty)+'</td>'+
                    '<td>';
                      if(v.id_item in a.dataReceive){
                      table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+a.dataReceive[v.id_item]+'" data-id="'+v.id+'">'
                      // table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+(parseInt(a.dataReceive[v.id_item])+parseInt(a.dataReject[v.id_item]))+'" data-id="'+v.id+'">'
                      table +=  '<input type="hidden" name="receive_satuan[]" value="'+a.dataSatuan[v.id_item]+'">'
                      }else{
                          table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" disabled oninput="InputQty(this)" max="0" name="receive_qty_item[]" value="0" data-id="'+v.id+'">'
                      }
                      table += '<input type="hidden" name="receive_id_item[]" value="'+v.id+'">'+
                      '<input type="hidden" name="id_receive_item" value="'+v.id_receive_item+'">'+
                    '</td>'+
                    '<td>'+
                        addCommas(v.price_gov)+
                      '<input type="hidden" name="price_pcs" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id+'">'+
                      {{-- <input type="hidden" name="price_shipment" value="{{$v->price_shipment}}" class="price_shipment" data-id="{{$v->id}}"> --}}
                    '</td>'+
                    '<td>';
                    if(v.id_item in a.dataSatuan){
                      table += a.dataSatuanName[v.id_item];
                    }else{
                      table += a.ReceiveItemSatuanAll[v.id_item]
                    }
                    table += '</td>';
                    table += '<td class="total total_'+v.id+'">';
                        if(v.id_item in a.dataReceive){
                          table += addCommas(a.dataReceive[v.id_item]*v.price_gov);
                        }else{
                          table += addCommas(0*v.price_gov);
                        }
                    table += '</td>';
                    table += '<td>';
                    if(v.id_item in a.dataDesc){
                      table += '<input type="text" name="receive_keterangan[]" class="form-control" value="'+a.dataDesc[v.id_item]+'">';
                    }else{
                      table += '<input type="text" name="receive_keterangan[]" readonly class="form-control" value="'+a.ReceiveItemDescAll[v.id_item]+'">';
                    }

                    table += '</td>'+
                    '<td><button class="btn btn-danger btn-sm btn-round" onclick="remove(this)" data-id="'+v.id_order+'"><i class="fa fa-times" aria-hidden="true"></i></button></td>'+
                  '</tr>';
      });



      $('table#itemKirim tbody').html(table);

    }
  });
}

var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  $(document).ready(function(){
    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
</script>
@endsection
