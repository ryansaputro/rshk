@extends('catalog.users.layouts.app_doc')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection


@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Catalog <small class="uppercase">Purchase Order Form...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        {{-- <div class="portlet-title">
          <div class="col-xs-6 caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Purchase Order ...
            </span>
          </div>
          <div class="col-xs-6" style="text-align:right;">
            <a  href="#" class="btn btn-primary btn-sm" onclick='printDiv();'>Print</a>
          </div>
          <br>
        </div><!--/.portlet-title\---> --}}
        <div class="portlet-body" id='DivIdToPrint'>
          <div class="row" style="font-weight:100; color:#000;">
            <center>
              <img style="width:12%;" class="pull-left" src="/assets/kemenkes.jpg" alt="" style="width:200px;">
              <img style="width:12%;" class="pull-right" src="https://2.bp.blogspot.com/--flp8oUCSiY/Wnb9lKct-7I/AAAAAAAAI14/QFXnonW67vktZpovpdj3Fk6NIUmRlRU9wCLcBGAs/s1600/Penerimaan%2BBesar%2BBesaran%2BPegawai%2BRS%2BJantung%2B%2526%2BPembuluh%2BDarah%2BHarapan%2BKita.gif" alt="" style="width:200px;">

              <strong>KEMENTRIAN KESEHATAN</strong><br>
              <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
              <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
              Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
              Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
              Surat Elektronik : info@pjnhk.go.id <br>
              <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a><br>

            </center>

            <hr>
            @php
              $time = strtotime($stockOut->datetime);
              $times = strtotime($userRequest->datetime);
              $function =  Fungsi::MonthIndonesia();
              $functionDay =  Fungsi::DayIndonesia();
              $list_propinsi  =  Fungsi::propinsi();
              $list_kota  =  Fungsi::IndonesiaProvince();
            @endphp



           <center><b>PENGELUARAN BARANG GUDANG</b></center>
            <br>
            <div class="col-md-12">
              <table class="table table-bordered">
                <tr>
                  <td>No Pengeluaran </td>
                  <td width="1px">:</td>
                  <td>{{date('y', $time)}}{{date('m', $time)}}{{date('d', $time)}}-OTR{{$stockOut->no_stock_out}}-{{$userRequest->kode_instalasi}}</td>
                  <td>Departemen Peminta </td>
                  <td width="1px">:</td>
                  <td>{{$userRequest->instalasi_user}}</td>
                </tr>
                <tr>
                  <td>No Permintaan </td>
                  <td width="1px">:</td>
                  <td>
                    @if (isset($userRequest->no_request))
                      {{date('y', $times)}}{{date('m', $times)}}{{date('d', $times)}}-ORD{{$userRequest->no_request}}-{{$userRequest->kode_user}}
                    @endif
                  </td>
                  <td>Gudang Pemberi </td>
                  <td width="1px">:</td>
                  <td>{{$userRequest->nama_instalasi}}</td>
                </tr>
                <tr>
                  <td>Tgl Pengeluaran </td>
                  <td width="1px">:</td>
                  <td>{{date('d', $time)}}-{{date('M', $time)}}-{{date('Y', $time)}}</td>
                  <td>Tipe Permintaan </td>
                  <td width="1px">:</td>
                  <td>
                    @if (isset($userRequest->request_type))
                      {{$userRequest->request_type == '1' ? 'Keluar (BHP/Inventaris)' : '-'}}
                    @endif
                  </td>
                </tr>
                <tr>
                  <td>Tgl Permintaan </td>
                  <td width="1px">:</td>
                  <td>{{date('d', $times)}}-{{date('M', $times)}}-{{date('Y', $times)}}</td>
                  <td>User Peminta </td>
                  <td width="1px">:</td>
                  <td>{{$userRequest->user_peminta}}</td>
                </tr>
                </tr>
              </table>
            </div>

                <div class="col-xs-12">
                  <table summary="Shopping cart" id="detailCart" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Spesifikasi</th>
                        <th>Jumlah Diminta</th>
                        <th>Jumlah Diberi</th>
                        <th>Satuan</th>
                        <th>Harga Satuan</th>
                        <th>Total Harga</th>
                      </tr>
                    </thead>
                    <tbody>
                    @php
                    $totQty=0;
                    $allTot=0;
                    @endphp
                    @foreach($stockOutDetail as $k => $v)
                      @php
                        $total = $v->qty*$v->price_gov;
                        $allTot += $total;
                        $jmlAsk = (array_key_exists($v->id_item, $userRequestDetail))  ? $userRequestDetail[$v->id_item] : '-';
                      @endphp
                      <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$v->code}}</td>
                        <td>{{$v->name}}</td>
                        <td>{{$v->description}}</td>
                        <td style="text-align:right;">
                          {{number_format($jmlAsk, 0, ',', '.')}}
                        </td>
                        <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
                        <td>{{array_key_exists($v->id_item, $userRequestDetailSatuan)  ? $userRequestDetailSatuan[$v->id_item] : '-'}}</td>
                        <td style="text-align:right;">{{number_format($v->price_gov, 0, ',', '.')}}</td>
                        <td style="text-align:right;">{{number_format($total, 0, ',', '.')}}</td>
                      </tr>
                    @endforeach
                    <tr>
                      <th colspan="8">Total (Rp.)</th>
                      <th style="text-align:right;">{{number_format($allTot, 0, ',', '.')}}</th>
                    </tr>
                    </tbody>
                  </table>
                  <p>Terbilang : <label for="" style="text-transform: uppercase;">{{Terbilang::make($allTot, ' rupiah', ' ')}}</label></p>
                  <b>Barang - barang tersebut telah diserahkan dalam keadaan baik.</b><br><br><br>
                  <div class="row">
                    <div class="col-xs-2">
                      <b>Catatan :</b><br>
                      <b>Tembusan :</b><br>
                      <ol>
                        <li><b>Unit Peminta (User)</b></li>
                        <li><b>Bag. Keuangan</b></li>
                      </ol>
                    </div>
                    <div class="col-xs-10">
                      <div class="col-xs-4">
                        Mengetahui/Menyetujui <br>
                        Kaur Pergudangan SNM
                      </div>
                      <div class="col-xs-4">
                        Yang Menyerahkan Barang <br>
                        Penanggung Jawab Persediaan Barang <br>
                        <br><br><br><br>
                        <u>
                        {{$stockOut->username}}
                        </u><br>
                      </div>
                      <div class="col-xs-4">
                        Jakarta, {{date('d', $time)}}-{{date('M', $time)}}-{{date('Y', $time)}} <br>
                        Yang Menerima Barang
                        <br><br><br><br><br>
                        <u>
                          {{$userRequest->user_peminta}}
                        </u><br>
                        {{$userRequest->nip_peminta}}
                      </div>
                    </div>
                  </div>
                </div>
          </div>


        </div><!--/.portlet-body-->
      </div><!--/.portlet-light-->
    </div><!--/.col-md-12-->
  </div><!--/.row-->
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('aa','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
