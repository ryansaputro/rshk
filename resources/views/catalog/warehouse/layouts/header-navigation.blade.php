<ul>
  {{-- <li><a href="{!! route('x.catalog.guest') !!}" >Dashboard</a></li> --}}
  <li><a href="{!! route('x.catalog.guest.item') !!}">PRODUK</a></li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Barang
      <i class="fa fa-angle-down"></i>
    </a>

    <!-- BEGIN DROPDOWN MENU -->
    <ul class="dropdown-menu">
      {{-- <li class="dropdown-submenu">
        <a href="shop-product-list.html">Hi Tops <i class="fa fa-angle-right"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li class="dropdown-submenu">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
              Second Level Link
              <i class="fa fa-angle-right"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
            </ul>
          </li>
        </ul>
      </li> --}}
      <li><a href="{{URL::to('catalog/warehouse/index')}}">Stok Barang</a></li>
      <li><a href="{{URL::to('catalog/warehouse/stockin/index')}}">Barang Masuk</a></li>
      <li><a href="{{URL::to('catalog/warehouse/stockout/index')}}">Barang Keluar</a></li>
      @if (Auth::user()->id_gudang == '3')
        <li><a href="{{URL::to('catalog/warehouse/distributed/index')}}">Distribusi ke Depo</a></li>
      @endif
    </ul>
    <!-- END DROPDOWN MENU -->
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Laporan
      <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu">
      <li><a href="{{URL::to('catalog/warehouse/report/stock')}}">Laporan Stok</a></li>
      <li><a href="{{URL::to('catalog/warehouse/report/stockin')}}">Barang Masuk</a></li>
      <li><a href="{{URL::to('catalog/warehouse/report/stockout')}}">Barang Keluar</a></li>
    </ul>
  </li>
  <!-- BEGIN TOP SEARCH -->
  {{-- <li class="menu-search">
    <span class="sep"></span>
    <i class="fa fa-search search-btn"></i>
    <div class="search-box">
      <form>
        <div class="input-group">
          <input type="text" name="search" placeholder="Search" class="form-control">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Search</button>
          </span>
        </div>
      </form>
    </div>
  </li> --}}
  <!-- END TOP SEARCH -->
</ul>
