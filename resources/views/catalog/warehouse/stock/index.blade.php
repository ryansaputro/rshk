@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Stok Barang</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
            <div class="col-sm-6 col-sm-6" style="float:left;">
              <button type="button" class="btn btn-info btn-rounded btn-sm" data-toggle="modal" data-target="#myModal" data-id=""><i class="fa fa-upload"></i> Import(xls)</button>
              <a href="/su_catalog/assets/users/template/template-inventory.xlsx" target="_blank" class="btn btn-warning btn-rounded btn-sm"><i class="fa fa-download"></i> Template(xls)</a>
            </div>
            {{-- <form>
              <div class="col-sm-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                  <option value="status">Status</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form> --}}
          </div>
          <br>
          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Masuk</th>
                  <th>Gudang</th>
                  <th>Item</th>
                  <th>Stok</th>
                  <th>Tanggal Keluar</th>
                </tr>
              </thead>
              @if (count($data) > 0)
                @foreach ($data as $k => $v)
                  @php
                  // $time = strtotime($v->timePO);
                  // $times = strtotime($v->timeDO);
                  @endphp
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->last_date_in}}</td>
                    <td>{{$v->nama_instalasi}}</td>
                    <td>{{$v->code}} - {{$v->name}}</td>
                    <td>{{$v->qty}} {{$v->unit_name}}</td>
                    <td>{{$v->last_date_out}}</td>
                    {{-- <td><span class="label label-{{$v->status == '1' ? 'danger' : 'success'}}">{{$v->status == '1' ? 'menunggu diperiksa' : 'telah diperiksa'}}</span></td>
                    <td>
                      <a href="{{URL::to('catalog/pphp/bast/preview/'.md5($v->id_order).'/'.md5($v->id_do))}}">detail</a>
                    </td> --}}
                  </tr>

                @endforeach
              @else
                <tr>
                  <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
                </tr>
              @endif
          </table>
        </div>
        <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
          <h1>  <center>Detail Pesanan</center></h1>
          <div class="row">
            <div class="col-xs-6">
              <address>
              <strong>Pembeli: </strong><br>
                <p id="name_buyer">Nama : xx</p>
                <p id="telephone_buyer">Telp: xx</p>
                <p id="email_buyer">Email: xx</p>
              </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                <strong>Order:</strong><br>
                <p id="no_order">No Order : yy</p>
                <p id="date_order">Tanggal : yy</p>
              </address>
            </div>

          </div>
          <table summary="Shopping cart" id="detailCart" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Biaya Kirim</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          {{-- <div class="shopping-total Modal" style="width: 300px;">
            <ul>
              <li>
                <em>Total</em>
                <input type="hidden" name="subtot" value="" id="input_subtot">
                <strong class="price">
                  Rp
                  <span id="subtot"></span>
                </strong>
              </li> --}}
              {{-- <li>
                <em>Shipping Cost <small>PPN 10%</small></em>

                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
              {{-- <li class="shopping-total-price">
                <em>Total</em>
                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
            {{-- </ul>
          </div> --}}
        </div>
      </div>
      <!-- BEGIN PAGINATOR -->
      {{-- <div class="row">
        <div class="col-md-5 col-sm-5">
          <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
        </div>
          <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
      </div> --}}
      <!-- END PAGINATOR -->
    </div>
    <div class="modal"></div>
  </div>


    <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
          <form class="" action="{{URL::to('catalog/warehouse/import/data')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Unggah Produk</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                 <label class="col-sm-2 control-label">Kategori</label>
                 <div class="col-sm-10">
                   <select class="form-control" name="category" id="select_new">
                      @foreach ($category as $k => $v)
                        @if ($v->id_parent == 0)
                          <option value="" style="text-transform: uppercase; color:#000; font-weight: 800;" disabled><b>{{$v->name}}</b></option>
                            @foreach ($category as $kParent => $vParent)
                              @if($vParent->id_parent == $v->id)
                                  <option value="{{$vParent->id}}" style="text-transform: uppercase; color:#000; font-weight: 400;" disabled>&nbsp;&nbsp;&nbsp;-{{$vParent->name}}</option>
                                  @foreach ($category as $kParent => $vParent1)
                                    @if($vParent->id == $vParent1->id_parent)
                                        <option value="{{$vParent1->id}}" style="text-transform: uppercase; color:#000;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-{{$vParent1->name}}</option>
                                      @endif
                                  @endforeach
                              @endif
                            @endforeach
                          @endif
                      @endforeach
                    </select>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label">file</label>
                  <div class="col-sm-10">
                    <input type="file" name="import_file" class="form-control" accept=".xlsx,.xls" required >
                  </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top:0px;">
              <br>
              <br>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>

        </div>
      </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
{{-- <script src="js/jquery.js" type="text/javascript"></script> --}}
<script type="text/javascript">
$(document).ready( function () {
  $('#mainCart').DataTable();
} );
</script>
@endsection
