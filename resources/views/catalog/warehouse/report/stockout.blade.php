@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Laporan Stok keluar</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
          <form>
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('item') ? ' has-error' : '' }}  col-md-12">
                <label for="item" class="col-md-4 control-label">Barang</label>
                <div class="col-md-8">
                  <select class="choiceChosen form-control" name="id_item">
                    <option value="xxx">Semua</option>
                    @foreach ($item as $k => $v)
                      {{$v->id_item}}
                      <option value="{{$v->id}}" {{$id_item == $v->id ? 'selected' : ''}}>{{$v->name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('item'))
                    <span class="help-block">
                      <strong>{{ $errors->first('item') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                <label for="user" class="col-md-4 control-label">Satuan</label>
                <div class="col-md-8">
                  <select class="choiceChosen form-control" name="id_satuan">
                    <option value="xxx">Semua</option>
                    @foreach ($satuan as $k => $v)
                      <option value="{{$v->id}}" {{$id_satuan == $v->id ? 'selected' : ''}}>{{$v->unit_name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('user'))
                    <span class="help-block">
                      <strong>{{ $errors->first('user') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('last_date_out') ? ' has-error' : '' }} col-md-12">
                <label for="last_date_out" class="col-md-4 control-label">Tanggal Keluar</label>
                <div class="col-md-8">
                  <input type="text" name="last_date_out" class="form-control input-sm" value="{{$param_last_out == 'xxx' ? '' : $param_last_out }}" />
                  @if ($errors->has('last_date_out'))
                    <span class="help-block">
                      <strong>{{ $errors->first('last_date_out') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }} col-md-12">
                <label for="stock" class="col-md-4 control-label">Stok</label>
                <div class="col-md-8">
                  <input type="number" name="stock" class="form-control input-sm" value="{{$stock !== 'xxx' ? $stock : ''}}" />
                  @if ($errors->has('stock'))
                    <span class="help-block">
                      <strong>{{ $errors->first('stock') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}  col-md-12">
                <label for="date" class="col-md-4 control-label"></label>
                <div class="col-md-8 pull-right">
                  <button type="submit" name="button" style="margin-right:0px;" class="btn btn-sm btn-primary btn-block">Cek</button>
                </div>
              </div>
            </div>
          </form>
          </div>
          <hr>
          <br>
          <br>
          <table summary="Shopping cart" id="mainCart" class="table table-striped display nowrap">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Keluar</th>
                  <th>Peminta</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Petugas</th>
                </tr>
              </thead>
              <tbody>
              @if (($last_date_out !== 'xxx') || ($id_item !== 'xxx') || ($id_satuan !== 'xxx') || ($stock !== 'xxx'))
                @foreach ($dataMasterGudang as $k =>$v)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->datetime}}</td>
                    <td>{{array_key_exists($v->id_user_requestor, $user) ? $user[$v->id_user_requestor] : ''}} {{$v->nama_depo !== null ? $v->nama_depo : ''}}</td>
                    <td>{{$v->code}} - {{$v->name}}</td>
                    <td>{{$v->qty}} {{$v->unit_name}}</td>
                    <td>{{$v->username}}</td>
                  </tr>
                @endforeach
              @endif
              </tbody>
          </table>

        </div>
      </div>
      <!-- BEGIN PAGINATOR -->
      {{-- <div class="row">
        <div class="col-md-5 col-sm-5">
          <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
        </div>
          <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
      </div> --}}
      <!-- END PAGINATOR -->
    </div>
    <div class="modal"></div>
  </div>

@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function() {
      $('#mainCart').DataTable( {
          dom: 'Bfrtip',
          "paging":   false,
          buttons: [
              'copy', 'csv', 'excel','print'
          ]
      } );
  } );

  $(function() {
    $('input[name="last_date_in"]').daterangepicker({
      opens: 'right',
      autoUpdateInput: false,
      locale: {
      format: 'YYYY/MM/DD',
      cancelLabel: 'Clear'
    },

    }, function(start, end, label) {
    });

      $('input[name="last_date_in"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
      });

      $('input[name="last_date_in"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
      });

    $('input[name="last_date_out"]').daterangepicker({
      opens: 'left',
      autoUpdateInput: false,
      locale: {
      format: 'YYYY/MM/DD',
      cancelLabel: 'Clear'
      },
    }, function(start, end, label) {
    });

      $('input[name="last_date_out"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
      });

      $('input[name="last_date_out"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
      });

  });

  $(document).ready(function(){
      //Chosen
    $(".choiceChosen, .productChosen").chosen({});
    //Logic
    // $(".choiceChosen").change(function(){
    //   if($(".choiceChosen option:selected").val()=="no"){
    //     $(".productChosen option[value='2']").attr('disabled',true).trigger("chosen:updated");
    //     $(".productChosen option[value='1']").removeAttr('disabled',true).trigger("chosen:updated");
    //   } else {
    //     $(".productChosen option[value='1']").attr('disabled',true).trigger("chosen:updated");
    //     $(".productChosen option[value='2']").removeAttr('disabled',true).trigger("chosen:updated");
    //   }
    // })
  })
</script>
@endsection
