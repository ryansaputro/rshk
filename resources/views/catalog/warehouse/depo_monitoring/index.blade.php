@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Monitoring Depo</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Keluar</th>
                  <th>Gudang</th>
                  <th>Dibawah Stok Minimum</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($depoUnderStock as $k => $v)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->last_date_out}}</td>
                    <td>{{$v->nama_depo}}</td>
                    <td>{{$v->stock}} barang</td>
                    {{-- <td> <a href="{{URL::to('catalog/warehouse/distributed/depo/'.$v->id_depo)}}" class="btn btn-xs btn-info" style="color:#fff;">Kirim Barang</a> </td> --}}
                    <td>
                      <a href="{{URL::to('catalog/warehouse/distributed/depo/'.md5($v->id_depo))}}" class="btn btn-xs btn-info" style="color:#fff;">Kirim Barang</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
      </div>
      <!-- BEGIN PAGINATOR -->
      {{-- <div class="row">
        <div class="col-md-5 col-sm-5">
          <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
        </div>
          <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
      </div> --}}
      <!-- END PAGINATOR -->
    </div>
    <div class="modal"></div>
  </div>

@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready( function () {
    $('#mainCart').DataTable();
  } );
  </script>
@endsection
