@extends('catalog.warehouse.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('content')
  @php
    $romanFormat = Fungsi::MonthRoman();
  @endphp
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Pengeluaran Barang Gudang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
        <div class="row">
          <div class="col-md-12">

                <div class="row" style="margin-top:20px; font-weight:100; color:#000;">
                  <form class="" action="{{URL::to('catalog/warehouse/distributed/depo/'.md5($headData->id_depo))}}" method="post">
                    {{ csrf_field() }}
                  <center>
                    <img style="width:12%;" class="pull-left" src="/assets/kemenkes.jpg" alt="" style="width:200px;">
                    <img style="width:12%;" class="pull-right" src="https://2.bp.blogspot.com/--flp8oUCSiY/Wnb9lKct-7I/AAAAAAAAI14/QFXnonW67vktZpovpdj3Fk6NIUmRlRU9wCLcBGAs/s1600/Penerimaan%2BBesar%2BBesaran%2BPegawai%2BRS%2BJantung%2B%2526%2BPembuluh%2BDarah%2BHarapan%2BKita.gif" alt="" style="width:200px;">

                    <strong>KEMENTRIAN KESEHATAN</strong><br>
                    <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
                    <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
                    Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
                    Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
                    Surat Elektronik : info@pjnhk.go.id <br>
                    <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a><br>

                  </center>

                  <hr>

                  <center><b>SURAT DISTRIBUSI BARANG</b></center>
                  <br>
                  <div class="col-md-12">
                    <table class="table table-bordered">
                      <tr>
                        <td>Nama Unit</td>
                        <td width="1px">:</td>
                        <td>{{$headData->nama_instalasi}}</td>

                        <td>Nama Depo </td>
                        <td>:</td>
                        <td>{{$headData->nama_depo}}</td>

                      </tr>
                    </tr>
                  </table>
                </div>

                <div class="col-xs-12">
                    <table summary="Shopping cart" id="detailCart" class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tanggal Keluar</th>
                          <th>Gudang</th>
                          <th>Item</th>
                          <th>Stok</th>
                          <th>Satuan</th>
                          <th>Harga</th>
                          <th>Tanggal Masuk</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listData as $k => $v)
                          <tr>
                            <td>{{$k+1}}</td>
                            <td>{{$v->last_date_out}}</td>
                            <td>{{$v->nama_depo}}</td>
                            <td>
                              {{$v->code}} - {{$v->name}}
                              <input type="hidden" name="id_item[]" value="{{$v->id_item}}">
                            </td>
                            <td> <input type="number" min="0" max="{{array_key_exists($v->id_item, $masterGudang) ? $masterGudang[$v->id_item] : $v->qty}}" name="qty[]" value="{{$v->qty}}" class="form-control input-sm"></td>
                            <td>
                                <input type="hidden" name="satuan[]" value="{{$v->id_satuan}}" class="form-control input-sm">
                                {{$v->unit_name}}
                            </td>
                            <td>
                                {{number_format(($v->price_gov), 0, ',', '.')}}
                              <input type="hidden" name="price[]" value="{{$v->price_gov}}" class="form-control input-sm">
                            </td>
                            <td>{{$v->last_date_in}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
                <div class="col-md-2 pull-right">
                  <button type="submit" class="btn btn-sm btn-info btn-block" style="color:#fff;" name="button">Simpan</button>
                </div>
              </form>
              </div>

          {{-- </div><!--/.col-md-12-->
        </div><!--/.row-->
      </div><!--/.portlet-body-->
    </div><!--/.portlet-light--> --}}
        </form>
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">
function ChangeQty(a) {
  var input = $(a).val();
  var maxInput = $(a).attr('max');
  var idItem = $(a).attr('data-item');
  var price = $('.price_'+idItem).attr('data-price');
  var total = input*price;
  $('td.tot_'+idItem).html(addCommas(total))
}

function changeRequest(a) {
  var id_request = $(a).find(':selected').attr('data-id');
}

var name = "";
var id = "";

function poNumber(a) {
  id_do = $(a).val();
  $.ajax({
    url: '{{URL::to('catalog/pphp/bast/receive/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_do" : id_do},
    success: function (a) {
      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);

      var table = "";
      $.each(a.data, function(k,v){
        var total = parseInt(v.qty_kirim)*parseInt(v.price_gov);
        table += '<tr>'+
                    '<td>'+v.code+'</td>'+
                    '<td>'+v.name+'</td>'+
                    '<td>'+addCommas(v.qty)+'</td>'+
                    '<td>';
                      if(v.id_item in a.dataReceive){
                      table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+a.dataReceive[v.id_item]+'" data-id="'+v.id+'">'
                      // table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+(parseInt(a.dataReceive[v.id_item])+parseInt(a.dataReject[v.id_item]))+'" data-id="'+v.id+'">'
                      table +=  '<input type="hidden" name="receive_satuan[]" value="'+a.dataSatuan[v.id_item]+'">'
                      }else{
                          table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" disabled oninput="InputQty(this)" max="0" name="receive_qty_item[]" value="0" data-id="'+v.id+'">'
                      }
                      table += '<input type="hidden" name="receive_id_item[]" value="'+v.id+'">'+
                      '<input type="hidden" name="id_receive_item" value="'+v.id_receive_item+'">'+
                    '</td>'+
                    '<td>'+
                        addCommas(v.price_gov)+
                      '<input type="hidden" name="price_pcs" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id+'">'+
                      {{-- <input type="hidden" name="price_shipment" value="{{$v->price_shipment}}" class="price_shipment" data-id="{{$v->id}}"> --}}
                    '</td>'+
                    '<td>';
                    if(v.id_item in a.dataSatuan){
                      table += a.dataSatuanName[v.id_item];
                    }else{
                      table += a.ReceiveItemSatuanAll[v.id_item]
                    }
                    table += '</td>';
                    table += '<td class="total total_'+v.id+'">';
                        if(v.id_item in a.dataReceive){
                          table += addCommas(a.dataReceive[v.id_item]*v.price_gov);
                        }else{
                          table += addCommas(0*v.price_gov);
                        }
                    table += '</td>';
                    table += '<td>';
                    if(v.id_item in a.dataDesc){
                      table += '<input type="text" name="receive_keterangan[]" class="form-control" value="'+a.dataDesc[v.id_item]+'">';
                    }else{
                      table += '<input type="text" name="receive_keterangan[]" readonly class="form-control" value="'+a.ReceiveItemDescAll[v.id_item]+'">';
                    }

                    table += '</td>'+
                    '<td><button class="btn btn-danger btn-sm btn-round" onclick="remove(this)" data-id="'+v.id_order+'"><i class="fa fa-times" aria-hidden="true"></i></button></td>'+
                  '</tr>';
      });



      $('table#itemKirim tbody').html(table);

    }
  });
}

var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  $(document).ready(function(){
    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
</script>
@endsection
