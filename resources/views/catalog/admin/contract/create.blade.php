@extends('catalog.admin.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Kontrak
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Kontrak ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/admin/contract/create')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">

            <div class="col-md-12">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="id_vendor" class="col-md-2 control-label">Penyedia</label>
                  <div class="col-md-10">
                    <select class="form-control input-sm select2" name="id_vendor_detail" style="margin-bottom:10px;" required autofocus>
                      @foreach ($vendor as $k => $v)
                          <option value="{{$v->id_vendor}}">{{$v->vendor_name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('id_vendor'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_vendor') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="kontrak" style="margin-top:70px;">
                  <table class="table table-striped" id="contract">
                    <thead>
                      <tr>
                        <th>No Kontrak</th>
                        <th>Tanggal Berlaku</th>
                        <th>Tanggal Berakhir</th>
                        <th>Deskripsi</th>
                        <th>Scan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr data="0">
                        <td>
                          <input style="margin-bottom:10px;" id="no_contract" type="text" class="form-control input-sm" name="no_contract[]" value="" required autofocus>
                        </td>
                        <td>
                          <input style="margin-bottom:10px;" id="start_date" type="date" class="form-control input-sm" name="start_date[]" value="" required autofocus>
                        </td>
                        <td>
                          <input style="margin-bottom:10px;" id="end_date" type="date" class="form-control input-sm" name="end_date[]" value="" required autofocus>
                        </td>
                        <td>
                          <textarea name="description[]" style="margin-bottom:10px;" class="form-control input-sm"></textarea>
                        </td>
                        <td>
                          <input style="margin-bottom:10px;" id="data" type="file" accept="application/pdf"  class="form-control input-sm" name="data[]" value="" required autofocus>
                        </td>
                        <td>-</td>
                      </tr>
                    </tbody>
                  </table>
                </div>




              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                  <a href="#" class="btn btn-success btn-sm" onclick="addContract()">Tambah Kontrak</a>
                  <a href="{{URL::to('catalog/admin/contract')}}" class="btn btn-danger btn-sm">Kembali</a>
                  <button type="submit" name="button" class="btn btn-primary btn-sm">Simpan</button>
                </div>
              </div>

          </div>
        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
function addContract() {
  var kode = parseInt($("#contract").find("tr").last().attr("data"))+parseInt(1);
  var contract = '<tr data="'+kode+'" class="tr_'+kode+'">'+
                    '<td>'+
                      '<input style="margin-bottom:10px;" id="no_contract" type="text" class="form-control input-sm" name="no_contract[]" value="" required autofocus>'+
                    '</td>'+
                    '<td>'+
                      '<input style="margin-bottom:10px;" id="start_date" type="date" class="form-control input-sm" name="start_date[]" value="" required autofocus>'+
                    '</td>'+
                    '<td>'+
                      '<input style="margin-bottom:10px;" id="end_date" type="date" class="form-control input-sm" name="end_date[]" value="" required autofocus>'+
                  '  </td>'+
                    '<td>'+
                      '<textarea name="description[]" style="margin-bottom:10px;" class="form-control input-sm"></textarea>'+
                    '</td>'+
                    '<td>'+
                      '<input style="margin-bottom:10px;" id="data" type="file" accept="application/pdf"  class="form-control input-sm" name="data[]" value="" required autofocus>'+
                    '</td>'+
                    '<td>'+
                      '<a href="#" onclick="hapus(this)" data-id="'+kode+'" class="btn btn-danger btn-sm">Hapus</a>'+
                    '</td>'+
                  '</tr>';
    $('#contract tbody').append(contract);


}

function hapus(a){
  var tr = $(a).attr('data-id');
  $('#contract tbody .tr_'+tr).remove();
}


$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
   });


$('.pengirimanBy').on('change', function(){
  $('.pengirimanBy option').each(function(k,v){
    if($(v).is(':selected')){
      if($(this).val() == 0){
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Nama Kurir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No Resi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }else{
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Sopir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Mobil</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No. Polisi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }
    }
    $('.pengiriman').css('display','inline');
    $('.pengiriman').css('margin-top','30px');
    $('.pengiriman').html(data);
  });

});

$(document).ready(function(){
  // $('.pengiriman').hide();

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

});

var name = "";
var id = "";
$('#item').on('change', function(){
  id = $('#item :selected').attr('data-id');
  id_item = $(this).val();
  name =$('#item :selected').text();
  $.ajax({
      url: '{{URL::to('vcs/users/detailItem')}}',
      method: 'POST',
      data: {"_token": "{{ csrf_token() }}", "id": id, "id_item" : id_item},
      success: function (a) {
        $('#qty').attr('value', a.qty);
        if(a.qty > 0){
          $('#qty').attr('max', a.qty);
          $('#qty').attr('min', 1);
        }else{
          $('#qty').attr('max', 0);
          $('#qty').attr('min', 0);

        }
      }
  });
});
var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}

</script>
@endsection
