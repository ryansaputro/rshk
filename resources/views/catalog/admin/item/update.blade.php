@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Update
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Update...</span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-info btn-sm hide">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<form class="" action="" method="post">
						{{ csrf_field() }}
						<div class="form-group hide">
							<label for="">Category</label>
							{{-- <input type="text" name="" value="{{ \App\Model\Category::find($id)->name }}" class="form-control" style="text-transform: uppercase;" readonly> --}}
						</div>
						<div class="form-group">
							<label for="">Supplier</label>
							<select class="form-control" name="id_supplier">
								@foreach($supplier as $k => $v)
									<option value="{{$v->id}}" {{($v->id == $supplierSelected) ? 'selected' : ''}}>{{$v->vendor_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="">Code</label>
							<input type="text" name="code" value="{{$data->code}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Item From</label>
							{{-- <input type="text" name="item_from" value="{{$data->item_from}}" class="form-control" required> --}}
							<select class="form-control" name="production_origin" required>
								<option value="0" {{($data->production_origin == 0) ? 'selected' : ''}}>Local</option>
								<option value="1" {{($data->production_origin == 1) ? 'selected' : ''}}>Import</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Nama</label>
							<input type="text" name="name" value="{{$data->name}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Merk</label>
							<input type="text" name="merk" value="{{$data->merk}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Price Retail</label>
							<input type="text" name="price_retail" value="{{$data->price_retail}}" class="form-control money" required>
						</div>
						<div class="form-group">
							<label for="">Price Goverment</label>
							<input type="text" name="price_gov" value="{{$data->price_gov}}" class="form-control money" required>
						</div>
						<div class="form-group">
							<label for="">Price Shipment</label>
							<input type="text" name="price_shipment" value="{{$data->price_shipment}}" class="form-control money" required>
						</div>
						<div class="form-group">
							<label for="">Date Price</label>
							<input type="date" name="price_date" value="{{$data->price_date}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Date Release</label>
							<input type="date" name="release_date" value="{{$data->release_date}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Date Expired</label>
							<input type="date" name="expired_date" value="{{$data->expired_date}}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea name="description" rows="8" cols="80" class="form-control" required>{{$data->description}}
							</textarea>
						</div>
						<div class="form-group text-center">
							<button type="submit" name="button" class="btn btn-lg btn-success">
								{{-- <i class="fa fa-pencil"></i> --}}
								Update
							</button>
						</div>
					</form>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
@endsection

@section('xjs')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.money').mask("000.000.000.000.000.000", {reverse: true});
		});
	</script>
@endsection
