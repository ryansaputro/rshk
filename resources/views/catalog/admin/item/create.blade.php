@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Create
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Create...</span>
						<span class="caption-subject bold uppercase">Category</span>
						<span class="caption-helper" style="text-transform: capitalize;">{{ \App\Model\Category::find($id)->name }}...</span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-info btn-sm hide">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<form class="" action="{{URL::to('catalog/admin/item/store')}}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="id_category" value="{{ $id }}">
						<div class="form-group hide">
							<label for="">Category</label>
							<input type="text" name="" value="{{ \App\Model\Category::find($id)->name }}" class="form-control" style="text-transform: uppercase;" readonly>
						</div>
						<div class="form-group">
							<label for="">Supplier</label>
							<select class="form-control" name="id_supplier">
								<option value="" selected disabled>Choice...</option>
								@foreach($supplier as $k => $v)
									<option value="{{$v->id}}">{{$v->vendor_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="">Code</label>
							<input type="text" name="code" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Item From</label>
							{{-- <input type="text" name="item_from" value="" class="form-control" required> --}}
							<select class="form-control" name="production_origin" required>
								<option value="" selected disabled>Choice...</option>
								<option value="0">Local</option>
								<option value="1">Import</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Nama</label>
							<input type="text" name="name" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Merk</label>
							<input type="text" name="merk" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Price Retail</label>
							<input type="text" name="price_retail" value="" class="form-control money" required id="price_retail" oninput="givePrice(this.value)" onchange="newMask()">
						</div>
						<div class="form-group">
							<label for="">Price Goverment</label>
							<input type="text" name="price_gov" value="" class="form-control money" required id="price_gov" readonly>
						</div>
						<div class="form-group">
							<label>
								Pengurangan
								PPN 10% <span id="ppn"></span>
								&
								PPH 1.5% <span id="pph"></span>
								dari Harga Retail
							</label>
						</div>
						<div class="form-group">
							<label for="">Price Shipment</label>
							<input type="text" name="price_shipment" value="" class="form-control money" required>
						</div>
						<div class="form-group">
							<label for="">Price Date</label>
							<input type="date" name="price_date" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Date Release</label>
							<input type="date" name="release_date" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Date Expired</label>
							<input type="date" name="expired_date" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea name="description" rows="8" cols="80" class="form-control" required>
							</textarea>
						</div>
						<div class="form-group text-center">
							<button type="submit" name="button" class="btn btn-lg btn-success">
								{{-- <i class="fa fa-plus"></i> --}}
								Save
							</button>
						</div>
					</form>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
@endsection

@section('xjs')
	<script type="text/javascript">
		$(document).ready(function() {
			// $('.money').mask("000.000.000.000.000.000", {reverse: true});
		});
		function log(a) {
			console.log($(a).val());
		}

		function givePrice(valPrice) {
			$('#price_retail').unmask();
			$('#price_gov').unmask();
			console.log(valPrice);
			var ppn = valPrice*10/100;
			var pph = valPrice*1.5/100;
			var realPrice = valPrice-ppn-pph;
			$('#ppn').html(': '+ ppn);
			$('#pph').html(': '+ pph);
			$('#price_gov').val(realPrice);

			// $('#ppn').mask("000.000.000.000.000.000", {reverse: true});
			// $('#pph').mask("000.000.000.000.000.000", {reverse: true});
		}
		function newMask() {
			console.log('test');
			// $('#price_retail').unmask();
			// $('#price_gov').unmask();
		}
	</script>
@endsection
