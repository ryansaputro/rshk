@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')
	<style media="screen">
		.treeview {
			font-size: 12px;
			color: black;
			list-style-type: none;
			padding: 0;
			/* text-transform: uppercase; */
			vertical-align: -webkit-baseline-middle;
		}
		.treeview ul {
			list-style-type: none;
			padding-left: 10px;
		}
		.treeview li {
			margin-bottom: 0px;
			/* border-top-style: dotted;
	    border-right-style: solid;
	    border-bottom-style: dotted;
	    border-left-style: solid; */
			padding: 10px;
		}
		.treeview > ul > li {
			margin-left: 15px;
		}
		.treeview > ul > ul > li {
			margin-left: 45px;
		}
		.treeview li a {
			text-decoration: none;
			color: black;
		}

		/** SPINNER CREATION **/

		.loader {
		  position: relative;
		  text-align: center;
		  margin: 15px auto 35px auto;
		  z-index: 9999;
		  display: block;
		  width: 80px;
		  height: 80px;
		  border: 10px solid rgba(0, 0, 0, .3);
		  border-radius: 50% !important;
		  border-top-color: #000;
		  animation: spin 1s ease-in-out infinite;
		  -webkit-animation: spin 1s ease-in-out infinite;
			display: none;
		}

		@keyframes spin {
		  to {
		    -webkit-transform: rotate(360deg);
		  }
		}

		@-webkit-keyframes spin {
		  to {
		    -webkit-transform: rotate(360deg);
		  }
		}


		/** MODAL STYLING **/

		.modal-content {
		  border-radius: 0px;
		  box-shadow: 0 0 20px 8px rgba(0, 0, 0, 0.7);
		}

		.modal-backdrop.show {
		  opacity: 0.75;
		}

		.loader-txt {
			display:none;
		  p {
		    font-size: 13px;
		    color: #666;
		    small {
		      font-size: 11.5px;
		      color: #999;
		    }
		  }
		}

		#output {
		  padding: 25px 15px;
		  background: #222;
		  border: 1px solid #222;
		  max-width: 350px;
		  margin: 35px auto;
		  font-family: 'Roboto', sans-serif !important;
		  p.subtle {
		    color: #555;
		    font-style: italic;
		    font-family: 'Roboto', sans-serif !important;
		  }
		  h4 {
		    font-weight: 300 !important;
		    font-size: 1.1em;
		    font-family: 'Roboto', sans-serif !important;
		  }
		  p {
		    font-family: 'Roboto', sans-serif !important;
		    font-size: 0.9em;
		    b {
		      text-transform: uppercase;
		      text-decoration: underline;
		    }
		  }
		}

	</style>
@endsection

@section('page-title')
	RSJPDHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET-->
			<div class="portlet light">
				@if(session()->has('message'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">x</button>
						{{ session()->get('message') }}
					</div>
				@endif

				<div class="buttons">
					<a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm">Unggah Item</a>
	        {{-- <button class="grid btn btn-sm btn-primary">Grid View</button>
	        <button class="list btn btn-sm btn-primary">List View</button> --}}
		    </div>

				<div class="portlet-body">
					@php
						$category = \App\Model\Category::where('id_parent', 0)->get();
					@endphp
					<ul class="treeview list">
						@foreach ($category as $key => $value)
							<li class="row">
								<a href="#id_{{ $value->id }}" data-toggle="collapse">
									<i class="fa fa-folder-open-o" aria-hidden="true"></i>
									{{ $value->name }}
								</a>
							</li>
							<ul id="id_{{ $value->id }}" class="collapse">
								@php
									$category1 = \App\Model\Category::where('id_parent', $value->id)->get();
								@endphp
								@foreach ($category1 as $key1 => $value1)
									@php
										if ($value->status == 'N') {
											$value1->status = 'N';
										}
									@endphp
									<li class="row">
										<a href="#id_{{ $value1->id }}" data-toggle="collapse">
											<i class="fa fa-folder-open-o" aria-hidden="true"></i>
											{{ $value1->name }}
										</a>
									</li>
									<ul id="id_{{ $value1->id }}" class="collapse">
										@php
											$category2 = \App\Model\Category::where('id_parent', $value1->id)->get();
										@endphp
										@foreach ($category2 as $key2 => $value2)
											@php
												if ($value1->status == 'N') {
													$value2->status = 'N';
												}
											@endphp
											<li class="row">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												{{ $value2->name }}
												<span style="float: right;">
													<a href="{{ url('/catalog/admin/item/create/'.$value2->id) }}" class="btn btn-sm btn-primary btn-circle">
														<i class="fa fa-plus"></i> Tambah Item
													</a>
													<a href="{{ url('/catalog/admin/item/list/'.$value2->id) }}" class="btn btn-sm btn-success btn-circle">
														<i class="fa fa-eye"></i> List Item
													</a>
												</span>
											</li>
										@endforeach
									</ul>
								@endforeach
							</ul>
						@endforeach
					</ul>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog  modal-full">

	    <!-- Modal content-->
			<form class="" action="{{URL::to('catalog/admin/item/upload')}}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
	    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Unggah Item</h4>
		      </div>
		      <div class="modal-body">
						<div class="loader"></div>
						 <div class="loader-txt text-center">
							 <p>Mohon tunggu .....</p>
						 </div>
						<div class="row input_text">
							<div class="form-group">
								<label for="id_vendor" class="col-md-2">Penyedia</label>
								<div class="col-md-10">
									<select class="form-control input-sm select2"  style="margin-bottom:5px !important;" name="id_vendor_detail" onclick="penyedia(this)">
										<option value="" disabled selected>-Pilih Penyedia-</option>
										@foreach ($vendor as $k => $v)
											<option value="{{$v->id_vendor}}">{{$v->vendor_name}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_vendor" class="col-md-2">Kontrak</label>
								<div class="col-md-10">
									<select class="form-control input-sm select2"  style="margin-bottom:5px !important;" id="contract" name="id_contract">
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_vendor" class="col-md-2">Adendum</label>
								<div class="col-md-10">
									<select class="form-control input-sm select2"  style="margin-bottom:5px !important;" id="adendum" name="id_adendum">
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_vendor" class="col-md-2">Kategori</label>
								<div class="col-md-10">
									<select class="form-control input-sm select2"  style="margin-bottom:5px !important;" name="category" id="select_new">
										@foreach ($categorys as $k => $v)
											@if($v->id_parent == 0)
												<option value="" style="text-transform: uppercase; color:#000; font-weight: 800;" disabled><b>{{$v->name}}</b></option>
												@foreach ($categorys as $kParent => $vParent)
													@if($vParent->id_parent == $v->id)
														<optgroup style="font-weight: 400 !important;" label="- {{$vParent->name}}">
															@foreach ($categorys as $kChild => $vChild)
																@if($vChild->id_parent == $vParent->id)
																	<option value="{{$vChild->id}}">  - {{$vChild->name}}</option>
																@endif
															@endforeach
														</optgroup>
													@endif
												@endforeach
											@endif
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="id_vendor" class="col-md-2">File</label>
								<div class="col-md-10">
									<input type="file" name="import_file"  style="margin-bottom:5px !important;" class="form-control input-sm" accept=".xlsx,.xls" required >
								</div>
							</div>

						</div>
		      </div>
		      <div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Keluar</button>
						<button type="submit" class="btn btn-primary btn-sm unggah" name="button">Unggah</button>
		      </div>

	    </div>
		</form>

	  </div>
	</div>
@endsection

@section('js')

@endsection

@section('xjs')
	<script type="text/javascript">
		$(document).ready(function() {

		});

		$(".unggah").on("click", function(e) {
			$('.loader-txt').css('display', 'block');
			$('.loader').css('display', 'block');
			$('div.input_text').css('display', 'none');
			$('.unggah').css('display', 'none');
		});

		function penyedia(a) {
			var vendor = $(a).val();
			$.ajax({
					url: '{{URL::to('catalog/admin/vendor/contract')}}',
					method: 'POST',
					data: {"_token": "{{ csrf_token() }}", "id_vendor": vendor},
					success: function (x) {
						var option = "";
						option += '<option disabled selected>-Pilih Kontrak-</option>'
						$.each(x.contract, function(k, v){
							option += '<option value="'+v.id+'">'+v.no_contract+'</option>'
						});
						$('#contract').html(option)
					}
			});

		}

		// function contract(a) {
		$("#contract").on('click', function(){
			var contract = $(this).val();
			$.ajax({
					url: '{{URL::to('catalog/admin/vendor/adendum')}}',
					method: 'POST',
					data: {"_token": "{{ csrf_token() }}", "id_contract": contract},
					success: function (x) {
						var option = "";
						option += '<option disabled selected>-Pilih Adendum-</option>'
						$.each(x.adendum, function(k, v){
							option += '<option value="'+v.id+'">'+v.no_adendum+'</option>'
						});
						$('#adendum').html(option)
					}
			});
		});

	</script>
@endsection
