@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')
@endsection

@section('page-title')
	RSJPDHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			{{-- <div class="alert alert-success" id="notif">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<strong id="status"></strong><p id="kata_status"></p>
			</div> --}}
			<!-- BEGIN PORTLET-->
			<div class="portlet light">
				@if(session()->has('message'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">x</button>
						{{ session()->get('message') }}
					</div>
				@endif
				{{-- <div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Category...</span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-info btn-sm hide">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div> --}}
				<div class="">
					<a href="{{URL::to('catalog/admin/item')}}" class="btn btn-danger btn-sm">Kembali</a>
				</div>

				<div class="portlet-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
								<tr>
									<th>No</th>
									<th>Kode</th>
									<th>Nama</th>
									<th>Satuan</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($item as $k => $v)
									<tr>
										<td>{{$k+1}}</td>
										<td>{{$v->code}}</td>
										<td>{{$v->name}}</td>
										{{-- <td>{{in_array($v->satuan, $satuan) ? $satuan[$v->satuan] : 'satuan tidak diketahui'}}</td> --}}
										<td>{{$v->unit_name}}</td>
										<td>Sukses</td>
									</tr>
								@endforeach
								@foreach ($item_reject as $key => $ve)
									<tr style="background-color:red;">
										<td style="color:white;">{{$k+($key+2)}}</td>
										<td style="color:white;">{{$ve->code}}</td>
										<td style="color:white;">{{$ve->name}}</td>
										<td style="color:white;">
											@if (!empty($ve->satuan))
												@if (array_key_exists($ve->satuan, $satuan))
													{{$satuan[$ve->satuan]}}
												@endif
											@else
												satuan tidak diketahui ({{$ve->satuan_name}})
											@endif
										</td>
										{{-- <td style="color:white;">{{$ve->unit_name}}</td> --}}
										<td style="color:white;">Gagal</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

@endsection

@section('js')

@endsection

@section('xjs')
@endsection
