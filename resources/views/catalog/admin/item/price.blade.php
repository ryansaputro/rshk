@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Price History
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Price History...</span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-info btn-sm hide">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered" style="width: 100%">
							<thead>
								<tr style="width: 50%;">
									<th rowspan="2">Date</th>
									<th colspan="3">Price</th>
								</tr>
								<tr>
									<th>Retail</th>
									<th>Goverment</th>
									<th>Shipment</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($history as $k => $v)
									<tr>
										<td style="vertical-align: middle;">{{ date('l, d F Y', strtotime($v->price_date)) }}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_gov, 2, ",", ".") }}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_retail, 2, ",", ".") }}</td>
										<td style="vertical-align: middle;">{{ $v->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($v->price_shipment, 2, ",", ".") }}</td>
									</tr>
								@endforeach
								@if (sizeof($history) == 0)
									<tr>
										<td colspan="4">Tidak ada Data </td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>
@endsection

@section('js')

@endsection

@section('xjs')
	<script type="text/javascript">

	</script>
@endsection
