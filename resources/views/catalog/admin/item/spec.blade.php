@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Specification
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(session()->has('message'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					{{ session()->get('message') }}
				</div>
			@endif
			<!-- BEGIN PORTLET-->
			<form class="" action="{{URL::to('catalog/admin/item/spec/save')}}" method="post">
				{{ csrf_field() }}
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-purple-plum">
							<i class="icon-speech font-purple-plum"></i>
							<span class="caption-subject bold uppercase">Item</span>
							<span class="caption-helper">Specification...</span>
						</div>
						<div class="actions">
							<button  type="submit" class="btn btn-circle btn-info btn-sm">
								<i class="fa fa-pencil"></i> Save
							</button>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered" style="width: 100%">
							<thead>
								<tr>
									<th>Specification</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($category_spec as $key => $value)
									@php
										$description = DB::table('spec_item')->where('id_category_spec', $value->id)->where('id_item', $id)->value('description');
									@endphp
									<tr>
										<td>{{ $value->name }}</td>
										<td>
											<input type="text" name="description[{{$value->id}}]" value="{!! $description == '' ? '-' : $description !!}" class="form-control">
											<input type="hidden" name="id_item" value="{{$id}}" class="form-control">
										</td>
									</tr>
								@endforeach
								@if (sizeof($category_spec) == 0)
									<tr>
										<th colspan="2">Tidak ada Data </th>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</form>
			<!-- END PORTLET-->
		</div>
	</div>
@endsection

@section('js')

@endsection

@section('xjs')
	<script type="text/javascript">

	</script>
@endsection
