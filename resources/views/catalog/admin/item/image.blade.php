@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('css')
	<link href="/sa_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
	<link href="/sa_catalog/assets/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Image
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(session()->has('message'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					{{ session()->get('message') }}
				</div>
			@endif
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Image...</span>
					</div>
					<div class="actions">
						<a href="#modal_add" class="btn btn-circle btn-info btn-sm" data-toggle="modal">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="boxless">
						<ul class="nav nav-tabs hide">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
								4 Columns </a>
							</li>
							<li>
								<a href="#tab_2" data-toggle="tab">
								3 Columns </a>
							</li>
							<li>
								<a href="#tab_3" data-toggle="tab">
								2 Columns </a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<!-- BEGIN FILTER -->
								<div class="margin-top-10">
									<ul class="mix-filter hide">
										<li class="filter" data-filter="all">
											 All
										</li>
										<li class="filter" data-filter="category_1">
											 UI Design
										</li>
										<li class="filter" data-filter="category_2">
											 Web Development
										</li>
										<li class="filter" data-filter="category_3">
											 Photography
										</li>
										<li class="filter" data-filter="category_3 category_1">
											 Wordpress and Logo
										</li>
									</ul>
									<div class="row mix-grid">
										@php
											$image_item = DB::table('image_item')->where('id_item', $id)->get();
										@endphp
										@foreach ($image_item as $key => $value)
											<div class="col-md-3 col-sm-4 mix">
												<div class="mix-inner">
													<img class="img-responsive" src="/assets/catalog/item/{{ $value->file }}" alt="">
													<div class="mix-details">
														<h4>Action</h4>
														<a style="background: red;" class="mix-link deleteFile" data-file="{{$value->file}}" onclick="DeleteImage(this)">
															<i class="fa fa-trash"></i>
														</a>
														<a class="mix-preview fancybox-button" href="/assets/catalog/item/{{ $value->file }}" title="" data-rel="fancybox-button">
															<i class="fa fa-eye"></i>
														</a>
													</div>
												</div>
											</div>
										@endforeach
										@if (sizeof($image_item) == 0)
											<div class="col-md-12">
												<p>
													<strong>No Data Available...</strong>
												</p>
											</div>
										@endif
									</div>
								</div>
								<!-- END FILTER -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<div class="modal fade" id="modal_add" role="dialog">
		<div class="modal-dialog">
			<form class="ModalForm" method="post" action="{{URL::to('catalog/admin/item/image/'.$id)}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">New Image Item</h4>
					</div>
					<div class="modal-body">
						<label for="">Image 1 <small>(max image size 1Mb)</small> </label>
						<input type="file" name="file[]" value="" class="form-control" required accept="image/x-png,image/gif,image/jpeg">
						@for($i = 2; $i<= 3; $i++)
						<div class="form-group">
							<label for="">Image {{$i}} <small>(max image size 1Mb)</small> </label>
							<input type="file" name="file[]" value="" class="form-control" accept="image/x-png,image/gif,image/jpeg">
						</div>
						@endfor
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="/sa_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
@endsection

@section('xjs')
	<script src="/sa_catalog/assets/admin/pages/scripts/portfolio.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Portfolio.init();
		});

		function DeleteImage(a) {
			var r = confirm("Are You sure delete this Image?");
			var id = $(a).attr('data-file');
			if (r == true) {
				$('.deleteFile').attr('href', 'delete/'+id);
			}
		}
	</script>
@endsection
