@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')
	<style media="screen">
		.table .btn {
			margin-right: 0;
		}
	</style>
@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			List
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(session()->has('message'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					{{ session()->get('message') }}
				</div>
			@endif
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">List...</span>
						<span class="caption-subject bold uppercase">Category</span>
						<span class="caption-helper" style="text-transform: capitalize;">{{ \App\Model\Category::find($id)->name }}...</span>
					</div>
					<div class="actions">
						<a href="{{ url('/catalog/admin/item/create/'.$id) }}" class="btn btn-sm btn-info btn-circle">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-bordered table-striped" id="sample_2" style="width: 100%;" >
						<thead>
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Harga</th>
								<th>Ongkos Kirim</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@php
								$item_category = DB::table('category_item')
								->join('item', 'category_item.id_item', '=', 'item.id')
								->where('category_item.id_category', $id)
								->get();
							@endphp
							{{-- {{ dd($item_category) }} --}}
							@foreach ($item_category as $key => $value)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{ $value->code }}</td>
									<td>{{ $value->name }}</td>
									{{-- <td>{{ $value->merk }}</td> --}}
									<td>
										<span>
											{{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
										</span>
										<span class="pull-right">
											@if ($value->price_country == 0)
												{{ number_format($value->price_retail, 2, ',', '.') }}
											@else
												{{ number_format($value->price_retail, 2, '.', ',') }}
											@endif
										</span>
									</td>
									<td>
										<span>
											{{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
										</span>
										<span class="pull-right">
											@if ($value->price_country == 0)
												{{ number_format($value->price_shipment, 2, ',', '.') }}
											@else
												{{ number_format($value->price_shipment, 2, '.', ',') }}
											@endif
										</span>
									</td>
									<td>{{ substr($value->price_date, 0, 7) }}</td>
									<td class="text-center">
										<div class="btn-group btn-group-xs">
											<a href="" data-id="{{$value->id}}" onclick="DeleteItem(this)" class="btn btn-danger tooltips delete_{{$value->id}}" data-toggle="tooltip" title="Delete" data-placement="bottom">
												<i class="fa fa-trash"></i>
											</a>
											<a href="{{ URL::to('/catalog/admin/item/update/'.$value->id) }}" class="btn btn-warning tooltips" data-toggle="tooltip" title="Update" data-placement="bottom">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ URL::to('/catalog/admin/item/spec/'.$value->id) }}" class="btn btn-success tooltips" data-toggle="tooltip" title="Specification" data-placement="bottom">
												<i class="fa fa-list"></i>
											</a>
											<a href="{{ URL::to('/catalog/guest/item/detail/'.$value->id) }}" class="btn btn-primary tooltips" data-toggle="tooltip" title="View" data-placement="bottom">
												<i class="fa fa-eye"></i>
											</a>
											<a href="{{ URL::to('/catalog/admin/item/image/'.$value->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Images" data-placement="bottom">
												<i class="fa fa-picture-o"></i>
											</a>
											<a href="{{ URL::to('/catalog/admin/item/doc/'.$value->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Documents" data-placement="bottom">
												<i class="fa fa-folder-open-o"></i>
											</a>
											<a href="{{ URL::to('/catalog/admin/item/price/'.$value->id) }}" class="btn btn-info tooltips" data-toggle="tooltip" title="Price" data-placement="bottom">
												<i class="fa fa-history"></i>
											</a>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
function DeleteItem(a) {
	var url = "/catalog/admin/item/delete/"+$(a).attr('data-id');
	var r = confirm("Are sure delete this data ?");
		if (r == true) {
				$('.delete_'+$(a).attr('data-id')).attr('href', url);
		}
}
</script>
@endsection

@section('xjs')
	<script type="text/javascript">

	</script>
@endsection
