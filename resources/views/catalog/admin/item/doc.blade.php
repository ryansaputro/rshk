@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')

@endsection

@section('page-title')
	RSHK <small>Item...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Item
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Document
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(session()->has('message'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">x</button>
					{{ session()->get('message') }}
				</div>
			@endif
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Item</span>
						<span class="caption-helper">Document...</span>
					</div>
					<div class="actions">
						<a href="#modal_add" class="btn btn-circle btn-info btn-sm" data-toggle="modal">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered" style="width: 100%">
						<thead>
							<tr>
								<th>#</th>
								{{-- <th>No</th> --}}
								<th>Name</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							@php
								$doc_item = DB::table('doc_item')->where('id_item', $id)->get();
							@endphp
							@foreach ($doc_item as $key => $value)
								<tr>
									<td style="vertical-align: middle; text-align: center;">
										<form class="" action="{!! route('x.catalog.admin.item.status') !!}" method="post">
											{{ csrf_field() }}
											<a href="{{URL::asset('/assets/catalog/doc/'.$value->file)}}" target="_blank" class="btn btn-primary btn-xs">
												<span class="fa fa-download" style="color: white !important;"></span>
											</a>
											<a  onclick="DeleteImage(this)" data-name="{{$value->file}}" class="btn btn-danger btn-xs deleteFile">
												<span class="fa fa-trash" style="color: white !important;"></span> Delete
											</a>
											<input type="hidden" name="id" value="{{ $value->id }}">
											@if ($value->status != 1)
												<input type="hidden" name="status" value="1">
												<button type="submit" class="btn btn-warning btn-xs">
													<span class="fa fa-eye"></span> Tampilkan
												</button>
											@else
												<input type="hidden" name="status" value="0">
												<button type="submit" class="btn btn-info btn-xs">
													<span class="fa fa-eye-slash"></span> Sembunyikan
												</button>
											@endif
										</form>
									</td>
									{{-- <td style="vertical-align: middle; text-align: center;">{{ $key+1 }}</td> --}}
									<td style="vertical-align: middle;">{{ $value->name }}</td>
									<td style="vertical-align: middle;">{{ $value->description == '' ? '-' : $value->description }}</td>
								</tr>
							@endforeach
							@if (sizeof($doc_item) == 0)
								<tr>
									<td colspan="4">Tidak ada Data </td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<div class="modal fade" id="modal_add" role="dialog">
		<div class="modal-dialog modal-lg">
			<form class="ModalForm" method="post" action="{{URL::to('catalog/admin/item/doc/'.$id)}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">New Document Item</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="">Document <small>(Max size 1Mb)</small> </label>
							<input type="file" name="file" value="" class="form-control" required accept="application/pdf,application/vnd.ms-excel" >
						</div>
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" name="name" value="" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea name="description" rows="8" cols="80" class="form-control" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')

@endsection

@section('xjs')
	<script type="text/javascript">
	function DeleteImage(a) {
		var r = confirm("Are You sure delete this Image?");
		var id = $(a).attr('data-name');
		if (r == true) {
			$('.deleteFile').attr('href', 'delete/'+id);
		}
	}
	</script>
@endsection
