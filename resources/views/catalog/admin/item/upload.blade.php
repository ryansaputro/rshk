@extends('catalog.admin.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Kontrak
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-title">
          {{-- <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Kontrak ...
            </span>
          </div> --}}
          <div class="actions" id="tombol">
            <div class="btn-group btn-group-devided">
              <a href="{{URL::to('catalog/admin/contract/create')}}"  class="btn btn-primary btn-rounded"></i> Kontrak Baru</a>
            </div>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet light">
          <div class="portlet-body">
            <table class="table table-bordered table-striped" style="width: 100%;" id="sample_6">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Penyedia</th>
                  <th>Jumlah Kontrak</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($contract as $k => $v)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->vendor_name}}</td>
                    <td>
                      <a href="{{URL::to('catalog/admin/contract/detail/'.md5($v->id_vendor))}}" class="btn btn-xs btn-success">{{$v->jumlah_kontrak}} Kontrak</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div><!--/.portlet-body-->
        </div>
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
@endsection
