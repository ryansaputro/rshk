@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			PPK
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
					     <div class="col-lg-12">
			                            @if(session('message'))
			                                    <div class='alert alert-success'>
			                                           {{session('message')}}
			                                    </div>
			                            @endif
                           </div>

					<div class="portlet-body table-wrapper-responsive">
						<a href="{!! route('x.catalog.admin.users.ppk_tambah') !!}" class="btn btn-primary btn-sm pull-right" data-toggle="modal">
							<i class="fa fa-plus"></i>Tambah</a>


							<table class="table table-striped table-bordered table-hover" id="sample_6">
							<thead>
							<tr>
								<th>
									No
								</th>
								<th>
									Golongan
								</th>
								<th>
									Username
								</th>
								<th>
									 Kode Instalasi
								</th>
								<th>Email </th>
								<th class="hidden-xs">
									PPK
								</th>

								<th> Nama PPK</th>
								<th> NIK</th>
								<th> No tlpn </th>
								<th class="hidden-xs">

								</th>
							</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
									// echo "<pre>";
									// print_r($data);
									// echo "</pre>";
								?>
								@foreach($data as $u)

										<tr>
								<td>
									 <?php echo $no++?>
								</td>
								<td>{{$u->jenis_gudang}}</td>

								<td>
									{{$u->username}}
								</td>
								<td>
									 {{$u->kode_instalasi}}
								</td>
								<td>

										{{$u->email}}
								</td>
								<td>
									{{$u->nama_instalasi}}
								</td>
								<td>
									 {{$u->nama_ppk}}
								</td>
								<td>
									 {{$u->nik}}
								</td>
								<td>
									 {{$u->mobile_phone}}
								</td>

								 <td>

                            <form action="{{route('x.catalog.admin.users.ppk_delete',$u->id_user)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{route('x.catalog.admin.users.ppk_edit',$u->id)}}" class=" btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
							</tr>
								@endforeach


							</tbody>
							</table>
						</div>




@endsection
