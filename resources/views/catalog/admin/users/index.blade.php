@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		<span class="fa fa-plus"></span> Tambah <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			@php
			 $levelUsers = \App\Role::where('display_name', 'LIKE','%catalog%')->orderBy('display_name', 'asc')->get();
			@endphp
			@foreach ($levelUsers as $key => $value)
				<li>
					<a class="" data-toggle="modal" href="#modal_{{ $value->id }}">
						<span class="fa fa-plus"></span> {{ $value->description }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endsection

@section('content')

	

	@php
	 $levelUsers = \App\Role::where('display_name', 'LIKE','%catalog%')->orderBy('display_name', 'asc')->get();
	@endphp
	@foreach ($levelUsers as $key => $value)
		<div class="modal fade" id="modal_{{ $value->id }}_update" tabindex="-1" role="modal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">
							<span class="fa fa-pencil"></span> {{ $value->description }}
						</h4>
					</div>
					<div class="modal-body">
						<form class="" action="{!! route('x.catalog.admin.users.update') !!}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="" id="data_id_{{ $value->id }}">
							<div class="form-group">
								<label for="">Nama</label>
								<input type="text" name="nama" value="" id="data_nama_{{ $value->id }}" class="form-control">
							</div>
							<div class="form-group">
								<label for="">No. Tlp</label>
								<input type="text" name="mobile" value="" id="data_mobile_{{ $value->id }}" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Username / E-Mail</label>
								<input type="email" name="username" value="" id="data_username_{{ $value->id }}" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Password</label>
								<input type="password" name="password" value="" class="form-control">
							</div>
							<input type="hidden" name="role_id" value="{{ $value->id }}">

							@if ($value->id == 8)
								{{-- directur --}}
								<input type="hidden" name="atasan" value="1">
							@elseif ($value->id == 6)
								{{-- manager --}}
								<input type="hidden" name="atasan" value="1">
							@elseif ($value->id == 2)
								{{-- finance --}}
								<div class="form-group">
									<label for="">Manager</label>
									<select class="form-control" name="atasan" id="data_atasan_{{ $value->id }}">
										<option value="" selected disabled>Select Manager</option>
										@php
										$managerUser = DB::table('role_user')
										->rightJoin('users', 'role_user.user_id', '=', 'users.id')
										->where('role_id', 6)->get();
										@endphp
										@foreach ($managerUser as $key_managerUser => $value_managerUser)
											<option value="{{ $value_managerUser->id }}">{{ $value_managerUser->name }} - {{ $value_managerUser->email }}</option>
										@endforeach
									</select>
								</div>
							@elseif ($value->id == 7)
								{{-- supervisi --}}
								<div class="form-group">
									<label for="">Manager</label>
									<select class="form-control" name="atasan" id="data_atasan_{{ $value->id }}">
										<option value="" selected disabled>Select Manager</option>
										@php
											$managerUser = DB::table('role_user')
											->rightJoin('users', 'role_user.user_id', '=', 'users.id')
											->where('role_id', 6)->get();
										@endphp
										@foreach ($managerUser as $key_managerUser => $value_managerUser)
											<option value="{{ $value_managerUser->id }}">{{ $value_managerUser->name }} - {{ $value_managerUser->email }}</option>
										@endforeach
									</select>
								</div>
							@elseif ($value->id == 3)
								{{-- staff --}}
								<div class="form-group">
									<label for="">Supervisi</label>
									<select class="form-control" name="atasan" id="data_atasan_{{ $value->id }}">
										<option value="" selected disabled>Select Supervisi</option>
										@php
											$supervisiUser = DB::table('role_user')
											->rightJoin('users', 'role_user.user_id', '=', 'users.id')
											->where('role_id', 7)->get();
										@endphp
										@foreach ($supervisiUser as $key_supervisiUser => $value_supervisiUser)
											<option value="{{ $value_supervisiUser->id }}">{{ $value_supervisiUser->name }} - {{ $value_supervisiUser->email }}</option>
										@endforeach
									</select>
								</div>
							@endif
							<div class="form-group text-center">
								<button type="submit" name="" class="btn btn-info">
									<span class="fa fa-pencil"></span> Simpan
								</button>
							</div>
						</form>

					</div>
					<div class="modal-footer hide">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="button" class="btn blue">Save changes</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	@endforeach

	@php
	 $levelUsers = \App\Role::where('display_name', 'LIKE','%catalog%')->orderBy('display_name', 'asc')->get();
	@endphp
	@foreach ($levelUsers as $key => $value)
		<div class="modal fade" id="modal_{{ $value->id }}_category" tabindex="-1" role="modal" aria-hidden="true">
			<div class="modal-dialog modal-full">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">
							<span class="fa fa-list"></span> Category {{ $value->description }}
						</h4>
					</div>
					<div class="modal-body">
						<form class="" action="{!! route('x.catalog.admin.users.category') !!}" method="post">
						  {{ csrf_field() }}
							<input type="hidden" name="user_id" id="user_id_{{ $value->id }}"  value="">
						  @php
						    $category = \App\Model\Category::where('id_parent', 0)->get();
						  @endphp
							@if ($value->id == 8)
								<ul class="treeview">
						    @foreach ($category as $key => $valuex)
						      <li class="row">
						        <a href="#id_{{ $valuex->id }}_{{ $value->id }}" data-toggle="collapse">
						          <i class="icon-control-play" style="color: blue; position: static;"></i>
						          {{ $valuex->name }}
						        </a>
						        <span style="float: right;">
											<input type="checkbox" name="category[{{ $valuex->id }}]" id="category_{{ $valuex->id }}_{{ $value->id }}" class="form-contorl">
						        </span>
						        <span style="float: right;" class="hide">
						          <button type="button" name="button" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{$valuex->id}}" data-id="{{ $valuex->id }}" onclick="AddCategory(this)" >
						            <i class="fa fa-plus"></i>
						          </button>
						          <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex->id }}" data-parent="{{$valuex->id_parent}}" data-id="{{ $valuex->id }}" data-name="{{ $valuex->name }}" onclick="DeleteCategory(this)">
						            <i class="fa fa-trash"></i>
						          </button>
						          @if ($valuex->status == 'Y')
						            <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{ $valuex->id_parent}}" data-id="{{ $valuex->id }}" onclick="EditCategory(this)" >
						              <i class="fa fa-eye"></i>
						            </button>
						          @else
						            <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{ $valuex->id_parent}}" data-id="{{ $valuex->id }}" onclick="EditCategory(this)" >
						              <i class="fa fa-eye-slash"></i>
						            </button>
						          @endif
						        </span>
						      </li>
						      <ul id="id_{{ $valuex->id }}_{{ $value->id }}" class="collapse">
						        @php
						          $category1 = \App\Model\Category::where('id_parent', $valuex->id)->get();
						        @endphp
						        @foreach ($category1 as $key1 => $valuex1)
						          @php
						            if ($valuex->status == 'N') {
						              $valuex1->status = 'N';
						            }
						          @endphp
						          <li class="row">
						            <a href="#id_{{ $valuex1->id }}_{{ $value->id }}" data-toggle="collapse">
						              <i class="icon-control-play" style="color: blue; position: static;"></i>
						              {{ $valuex1->name }}
						            </a>
						            <span style="float: right;" class="hide">
						              <button type="button" name="button" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="AddCategory(this)" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id}}">
						                <i class="fa fa-plus"></i>
						              </button>
						              <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id_parent}}" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="DeleteCategory(this)">
						                <i class="fa fa-trash"></i>
						              </button>
						              @if ($valuex1->status == 'Y')
						                <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id_parent}}" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="EditCategory(this)">
						                  <i class="fa fa-eye"></i>
						                </button>
						              @else
						                <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" data-parent="{{ $valuex1->id_parent}}" data-id="{{ $valuex1->id }}" onclick="EditCategory(this)" >
						                  <i class="fa fa-eye-slash"></i>
						                </button>
						              @endif
						            </span>
						          </li>
						          <ul id="id_{{ $valuex1->id }}_{{ $value->id }}" class="collapse">
						            @php
						              $category2 = \App\Model\Category::where('id_parent', $valuex1->id)->get();
						            @endphp
						            @foreach ($category2 as $key2 => $valuex2)
						              @php
						                if ($valuex1->status == 'N') {
						                  $valuex2->status = 'N';
						                }
						              @endphp
						              <li class="row">
						                <i class="icon-grid" style="color: blue; position: static;"></i>
						                {{ $valuex2->name }}
						                <span style="float: right;" class="hide">
						                  <a href="{{ URL::to('/catalog/admin/category/spec/'.$valuex2->id) }}" class="btn btn-sm btn-info btn-circle" target="_blank" data-toggle="tooltip" title="Specification" data-placement="bottom">
						                    <i class="fa fa-list"></i>
						                  </a>
						                  <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex2->id }}" data-parent="{{$valuex2->id_parent}}" data-id="{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" onclick="DeleteCategory(this)">
						                    <i class="fa fa-trash"></i>
						                  </button>
						                  @if ($valuex2->status == 'Y')
						                    <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex2->id }}" data-parent="{{$valuex2->id_parent}}" data-id="{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" onclick="EditCategory(this)">
						                      <i class="fa fa-eye"></i>
						                    </button>
						                  @else
						                    <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" data-parent="{{ $valuex2->id_parent}}" data-id="{{ $valuex2->id }}" onclick="EditCategory(this)" >
						                      <i class="fa fa-eye-slash"></i>
						                    </button>
						                  @endif
						                </span>
						              </li>
						            @endforeach
						          </ul>
						        @endforeach
						      </ul>
						    @endforeach
						  </ul>
							@elseif ($value->id == 6)
								<ul class="treeview">
						    @foreach ($category as $key => $valuex)
						      <li class="row">
						        <a href="#id_{{ $valuex->id }}_{{ $value->id }}" data-toggle="collapse">
						          <i class="icon-control-play" style="color: blue; position: static;"></i>
						          {{ $valuex->name }}
						        </a>
						        <span style="float: right;">
											<input type="checkbox" name="category[{{ $valuex->id }}]" class="form-contorl">
						        </span>
						        <span style="float: right;" class="hide">
						          <button type="button" name="button" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{$valuex->id}}" data-id="{{ $valuex->id }}" onclick="AddCategory(this)" >
						            <i class="fa fa-plus"></i>
						          </button>
						          <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex->id }}" data-parent="{{$valuex->id_parent}}" data-id="{{ $valuex->id }}" data-name="{{ $valuex->name }}" onclick="DeleteCategory(this)">
						            <i class="fa fa-trash"></i>
						          </button>
						          @if ($valuex->status == 'Y')
						            <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{ $valuex->id_parent}}" data-id="{{ $valuex->id }}" onclick="EditCategory(this)" >
						              <i class="fa fa-eye"></i>
						            </button>
						          @else
						            <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex->id }}" data-name="{{ $valuex->name }}" data-parent="{{ $valuex->id_parent}}" data-id="{{ $valuex->id }}" onclick="EditCategory(this)" >
						              <i class="fa fa-eye-slash"></i>
						            </button>
						          @endif
						        </span>
						      </li>
						      <ul id="id_{{ $valuex->id }}_{{ $value->id }}" class="collapse">
						        @php
						          $category1 = \App\Model\Category::where('id_parent', $valuex->id)->get();
						        @endphp
						        @foreach ($category1 as $key1 => $valuex1)
						          @php
						            if ($valuex->status == 'N') {
						              $valuex1->status = 'N';
						            }
						          @endphp
						          <li class="row">
						            <a href="#id_{{ $valuex1->id }}_{{ $value->id }}" data-toggle="collapse">
						              <i class="icon-control-play" style="color: blue; position: static;"></i>
						              {{ $valuex1->name }}
						            </a>
						            <span style="float: right;" class="hide">
						              <button type="button" name="button" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="AddCategory(this)" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id}}">
						                <i class="fa fa-plus"></i>
						              </button>
						              <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id_parent}}" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="DeleteCategory(this)">
						                <i class="fa fa-trash"></i>
						              </button>
						              @if ($valuex1->status == 'Y')
						                <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex1->id }}" data-parent="{{$valuex1->id_parent}}" data-id="{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" onclick="EditCategory(this)">
						                  <i class="fa fa-eye"></i>
						                </button>
						              @else
						                <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex1->id }}" data-name="{{ $valuex1->name }}" data-parent="{{ $valuex1->id_parent}}" data-id="{{ $valuex1->id }}" onclick="EditCategory(this)" >
						                  <i class="fa fa-eye-slash"></i>
						                </button>
						              @endif
						            </span>
						          </li>
						          <ul id="id_{{ $valuex1->id }}_{{ $value->id }}" class="collapse">
						            @php
						              $category2 = \App\Model\Category::where('id_parent', $valuex1->id)->get();
						            @endphp
						            @foreach ($category2 as $key2 => $valuex2)
						              @php
						                if ($valuex1->status == 'N') {
						                  $valuex2->status = 'N';
						                }
						              @endphp
						              <li class="row">
						                <i class="icon-grid" style="color: blue; position: static;"></i>
						                {{ $valuex2->name }}
						                <span style="float: right;" class="hide">
						                  <a href="{{ URL::to('/catalog/admin/category/spec/'.$valuex2->id) }}" class="btn btn-sm btn-info btn-circle" target="_blank" data-toggle="tooltip" title="Specification" data-placement="bottom">
						                    <i class="fa fa-list"></i>
						                  </a>
						                  <button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $valuex2->id }}" data-parent="{{$valuex2->id_parent}}" data-id="{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" onclick="DeleteCategory(this)">
						                    <i class="fa fa-trash"></i>
						                  </button>
						                  @if ($valuex2->status == 'Y')
						                    <button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $valuex2->id }}" data-parent="{{$valuex2->id_parent}}" data-id="{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" onclick="EditCategory(this)">
						                      <i class="fa fa-eye"></i>
						                    </button>
						                  @else
						                    <button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $valuex2->id }}" data-name="{{ $valuex2->name }}" data-parent="{{ $valuex2->id_parent}}" data-id="{{ $valuex2->id }}" onclick="EditCategory(this)" >
						                      <i class="fa fa-eye-slash"></i>
						                    </button>
						                  @endif
						                </span>
						              </li>
						            @endforeach
						          </ul>
						        @endforeach
						      </ul>
						    @endforeach
						  </ul>
							@endif
						  <div class="form-group text-center">
						    <button type="submit" name="" class="btn btn-info">
						      <span class="fa fa-pencil"></span> Simpan
						    </button>
						  </div>
						</form>
					</div>
					<div class="modal-footer hide">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="button" class="btn blue">Save changes</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	@endforeach
@endsection

@section('xjs')
	<script type="text/javascript">
		function getData(id, role) {
			$.ajax({
				url: '{!! route('x.catalog.admin.users.getdata') !!}',
				type: 'get',
				dataType: 'json',
				data: {
					_token: '{{ csrf_field() }}',
					id: id,
					role: role,
				},
				success: function(res) {
					// console.log(res);
					$('#data_id_'+res.role).val(res.user.id);
					$('#data_nama_'+res.role).val(res.user.name);
					$('#data_mobile_'+res.role).val(res.mobile);
					$('#data_username_'+res.role).val(res.user.email);
					$('#data_atasan_'+res.role).val(res.atasan);
				}
			});
		}
		function changeIdRole(id_role, id_user) {
			$('#user_id_'+id_role).val(id_user);
			// console.log($('#user_id_'+id_role).val());
			$('')

			$.ajax({
				url: '{!! route('x.catalog.admin.users.category') !!}',
				type: 'get',
				dataType: 'json',
				data: {
					_token: '{{ csrf_field() }}',
					id: id_user,
				},
				success: function(res) {
					// console.log(res);
					// $('#data_id_'+res.role).val(res.user.id);
					res.category.forEach(function(index, el) {
						console.log(res.category[el]['category_id']);
						console.log(id_role);
						$('#category_'+res.category[el]['category_id']+'_'+id_role).prop( "checked", true );
					});
					$.uniform.update();
				}
			});
		}
	</script>
@endsection
