@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Direktorat Penunjang
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
					     <div class="col-lg-12">
			                            @if(session('message'))
			                                    <div class='alert alert-success'>
			                                           {{session('message')}}
			                                    </div>
			                            @endif
                           </div>

					<div class="portlet-body table-wrapper-responsive">
						<a href="{!! route('x.catalog.admin.users.direktur_penunjang_tambah') !!}" class="btn btn-primary btn-sm pull-right" data-toggle="modal">
							<i class="fa fa-plus"></i>Tambah</a>


						<table class="table table-striped table-bordered table-hover" id="sample_6">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Nama Unit</th>
                  <th>Email</th>
                  <th>Kategory</th>
                  <th></th>

                </tr>
              </thead>
              <tbody>

                <?php
                  $no=1;
                  // echo "<pre>";
                  // print_r($data);
                  echo "</pre>";
                ?>
                    @foreach($data as $d)

                      <tr>
                          <td>{{$no++}}</td>
                          <td>{{$d->username}}</td>
                          <td>{{$d->nama_instalasi}}</td>
                          <td>{{$d->email}}</td>
                          <td><button type="button"
                            class="btn btn-primary id"
                            data-toggle="modal"
                            data-id="{{$d->kategory}}"
                            data-target="#exampleModalLong">
                                   Kategory
                                </button>
                           <td>




						   <form action="{{URL::to('catalog/admin/users/direktur_penunjang_delete',$d->id)}}" method="post">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<a href="{{URL::to('catalog/admin/users/direktur_penunjang_edit',$d->id)}}" class=" btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
											<button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash"></i></button>
										</form>
                        </td>
                      </tr>

                    @endforeach()


              </tbody>
          </table>
						</div>


<script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript">

  </script>
<script type="text/javascript">


 $(".id").click(function(){
		 var a = $(this).attr('data-id');

		  var url = "{{URL::to('catalog/admin/users/direktur_main_category')}}";

			  $.ajax({
			 type: "post",
			  url: url,
			  data: {
				"_token": "{{ csrf_token() }}",
				"id_kategory": a,
				},
			  success:function(data){
				// alert(data);
			   $("#id_aktivity").html(data);


			  }

			});


	});



</script>

					<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLongTitle">Kategory</h5>
							  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
							</div>
							<div class="modal-body">

										<div id="id_aktivity"></div>

							</div>
							<div class="modal-footer">

							</div>
						  </div>
						</div>
					  </div>
@endsection
