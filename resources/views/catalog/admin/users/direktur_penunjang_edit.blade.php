@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Edit Kepala Instalasi
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{{route('x.catalog.admin.users.direktur_penunjang_edit_simpan',$data1->id)}}" method="post">
									  {{ csrf_field() }}
                            		 {{ method_field('PUT') }}
								<div class="form-body">

									<div class="form-group">
										<label>PPK</label>
											<select name="id_ppk" class="form-control">
												@foreach($data2 as $d)

												<?php
														if($d->id==$data1->id_ppk){

															$selected="selected";
														}else{
															$selected=" ";
														}
													?>


														<option value="{{$d->id}}"<?php echo $selected?>>{{$d->nama_instalasi}}</option>
												@endforeach()
											</select>

									</div>


									<div class="form-group">
										<label>Kategory</label>
										<select class="form-control select2" required="" id="id_kategory" name="id_kategory[]" multiple="multiple">
																		@foreach($data as $d)
																			<option value="{{$d->id}}">{{$d->name}}</option>
																		@endforeach()
										</select>
										<input type="checkbox" id="allkategory" >All
									</div>

									<?php

										// echo "<pre>";
										// print_r($data1);
										// echo "</pre>";
									?>

									<div class="form-group">
										<label>Kode Instalasi</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="kode" value="{{$data1->kode_instalasi}}" class="form-control" placeholder="Kode Instalasi">
										</div>
									</div>
									<div class="form-group">
										<label>Nama Instalasi</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="nama_instalasi" value="{{$data1->nama_instalasi}}" class="form-control input-circle-right" placeholder="Nama Instalasi">
										</div>
									</div>

									<div class="form-group">
										<label>Email</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="email" name="email" value="{{$data1->email}}" class="form-control" placeholder="Email">
										</div>
									</div>

									<div class="form-group">
										<label>Username</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="username" value="{{$data1->username}}" class="form-control input-circle-right" placeholder="Username">
										</div>
									</div>

									<div class="form-group">
										<label>Password</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
										</div>
									</div>


								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>

					<script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
				  <script type="text/javascript">
				    $(function () {


				              $("#id_kategory").select2();
				              // $("#divisi_id").select2();
				              // $("#activity_id").select2();
				              // $("#market_type_id").select2();
				              $("#id_kategory").trigger("change");
				              // $("#divisi_id").trigger("change");


				             $("#allkategory").click(function(){
				                if($("#allkategory").is(':checked') ){
				                    $("#id_kategory").find('option').prop("selected",true);
				                    $("#id_kategory").trigger("change");
				                    var id_kategory = $("#id_kategory").val();

				                }else{
				                    $("#id_kategory").val('').trigger("change");
				                     var id_kategory = $("#id_kategory").val();
				                     //alert(id_kategory);
				                 }
				            });


				              $("#id_kategory").on('change', function() {
				              var id_kategory  = $("#id_kategory").val();
				              //alert(id_kategory);



				        });



				          });

				  </script>


@endsection
