@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Finance
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
					     <div class="col-lg-12">
			                            @if(session('message'))
			                                    <div class='alert alert-success'>
			                                           {{session('message')}}
			                                    </div>
			                            @endif
                           </div>

					<div class="portlet-body table-wrapper-responsive">
						<a href="{!! route('x.catalog.admin.users.finance_tambah') !!}" class="btn btn-primary btn-sm pull-right" data-toggle="modal">
							<i class="fa fa-plus"></i>Tambah</a>


							<table class="table table-striped table-bordered table-hover" id="sample_6">
							<thead>
							<tr>
								<th>
									No
								</th>
								<th>
									Username
								</th>
								<th>
									 Kode Instalasi
								</th>
								<th>Nama Instalasi</th>
								<th>Email </th>




								<th class="hidden-xs">

								</th>
							</tr>
							</thead>
							<tbody>
									<?php
										$no=1;
									?>
									@foreach($data as $d)

										<tr>
											<td>{{$no++}}</td>
											<td>{{$d->username}}</td>
											<td>{{$d->kode_instalasi}}</td>
											<td>{{$d->nama_instalasi}}</td>
											<td>{{$d->email}}</td>




											<td>
													 <form action="{{route('x.catalog.admin.users.finance_delete',$d->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{route('x.catalog.admin.users.finance_edit',$d->id)}}" class=" btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash"></i></button>
                            </form>
											</td>
										</tr>

									@endforeach()


							</tbody>
							</table>
						</div>




@endsection
