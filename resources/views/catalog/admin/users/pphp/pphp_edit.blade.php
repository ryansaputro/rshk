@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Edit PPHP
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
					

		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{!! route('x.catalog.admin.users.pphp_edit_simpan',$data1->id) !!}" method="post">
									{{ csrf_field() }}
                            		 {{ method_field('PUT') }}
								<div class="form-body">


									<div class="form-group">
										<label>Kode Instalasi</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="kode" value="{{$data1->kode_instalasi}}" required=""  class="form-control" placeholder="Kode Instalasi">
										</div>
									</div>
									<div class="form-group">
										<label>Nama Instalasi</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" value="{{$data1->nama_instalasi}}" required="" name="nama_instalasi" class="form-control input-circle-right" placeholder="Nama Instalasi">
										</div>
									</div>

									<div class="form-group">
										<label>Jenis PPHP</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" value="{{$data1->jenis}}"  required="" name="jenis" class="form-control input-circle-right" placeholder="Jenis">
										</div>
									</div>


									<div class="form-group">
										<label>Nama</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" value="{{$data1->nama}}"  required="" name="nama" class="form-control input-circle-right" placeholder="Nama">
										</div>
									</div>



									<div class="form-group">
										<label>Posisi</label>
										<div class="input-group">
											<select name="posisi" class="form-control">
												 <option value="Ketua" <?php if($data1->posisi=="Ketua"){echo "selected";}?>>Ketua</option>
												 <option value="Anggota" <?php if($data1->posisi=="Anggota"){echo "selected";}?>>Anggota</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label>Email</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="email" required="" value="{{$data1->email}}"  name="email" class="form-control" placeholder="Email">
										</div>
									</div>

									<div class="form-group">
										<label>Username</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="username" value="{{$data1->username}}"  required="" class="form-control input-circle-right" placeholder="Username">
										</div>
									</div>

									<div class="form-group">
										<label>Password</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="password"  name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
										</div>
									</div>
									
																		
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection



