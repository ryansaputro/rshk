<style>
body {
  font-family: "Helvetica Neue", Helvetica, Arial, san-serif;
  text-align: center;
  margin-top:2em;
}

h1 {
  font-weight: bold;
  font-size: 250%;
  margin-bottom: 1em;
}

p {
  max-width: 75%;
  background: rgba(255, 255, 255, 0.85);
  border: 1px solid black;
  margin: 0 auto 3em;
  padding: 20px;
  line-height: 1.5em;
  text-align: left;
}

#canvas {
  overflow: hidden;
  position: absolute;
  top: 0;
  left: -25%;
  right: 0;
  bottom: 0;
  width: 125%;
  height: 100%;
}

.square {
  position: absolute;
  z-index: -1;
  opacity: 0.30;
  transform: skew(-15deg);
}
</style>

<div id="canvas"></div>
<h1>Silahkan Anda Untuk Melengkapi Data dibawah ini</h1>





<script>

$(document).ready(function() {
      const SQ_NUM = 8; // the number of squares
      for (var i = 0; i < SQ_NUM; i ++) { // create the squares
        $('#canvas').append('<div class="square" id="square' + i + '"> </div>');
      }

      function get_random_color() {
        var colors = [
          '#E6C262',
          '#C43019',
          '#159B99'
        ];
        return colors[Math.floor(Math.random()*colors.length)];
      }

        for (var i = 0; i < SQ_NUM; i ++) {
          var obj = $('#square' + i);
          obj.css(get_random_color());
          // fulfill random color
          obj.css("background", get_random_color());
          var width = $(document).width();
          var height = $(document).height();
          var left = Math.floor(Math.random() * width);
          var top = Math.floor(Math.random() * height);
          // move randomly
          obj.css("left", left);
          obj.css("top", top);

          var minw = 120;
          var maxw = 480;
          var minh = 5;
          var maxh = 320;
          console.log(maxw);
          var sw = Math.floor(Math.random() * (maxw - minw)+minw);
          var sh = Math.floor(Math.random() * ( maxh - minh)+minh);
          // set the size
          obj.css("width", sw);
          obj.css("height", sh);
        }
    });
</script>
