@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Edit Vendor
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
              <form role="form" action="{!! route('x.catalog.admin.users.master_vendor_edit_simpan',$data->id) !!}" method="post">
									{{ csrf_field() }}
                  {{ method_field('PUT') }}
								<div class="form-body">

									<div class="form-group">
										<label>Kode Vendor</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="kode" required=""  class="form-control" placeholder="Kode Instalasi">
										</div>
									</div>

									<div class="form-group">
										<label>Nama Vendor</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" required="" name="nama_vendor" class="form-control input-circle-right" placeholder="Nama Instalasi">
										</div>
									</div>

                  <div class="form-group">
										<label>Email</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" required="" name="email" class="form-control input-circle-right" placeholder="Email">
										</div>
									</div>

                  <div class="form-group">
										<label>No Hp</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="number" required="" name="no_hp" class="form-control input-circle-right" placeholder="No Hp">
										</div>
									</div>

                  <div class="form-group">
										<label>No Surat Kontrak</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" required="" name="no_surat_kontrak" class="form-control input-circle-right" placeholder="No Surat Kontrak">
										</div>
									</div>


                  <div class="form-group">
										<label>Surat Kontrak</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="file" required="" name="file" class="form-control input-circle-right" placeholder="Nama Instalasi">
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
