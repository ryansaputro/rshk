@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')
	<style media="screen">
		.treeview {
			font-size: 18px;
			color: black;
			list-style-type: none;
			padding: 0;
			text-transform: uppercase;
			vertical-align: -webkit-baseline-middle;
		}
		.treeview ul {
			list-style-type: none;
			padding-left: 10px;
		}
		.treeview li {
			margin-bottom: 10px;
			border-top-style: dotted;
	    border-right-style: solid;
	    border-bottom-style: dotted;
	    border-left-style: solid;
			padding: 10px;
		}
		.treeview > ul > li {
			margin-left: 15px;
		}
		.treeview > ul > ul > li {
			margin-left: 45px;
		}
		.treeview li a {
			text-decoration: none;
			color: black;
		}
	</style>
@endsection

@section('page-title')
	RSHK <small>Category...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Category
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="alert alert-success" id="notif">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong id="status"></strong><p id="kata_status"></p>
	</div>
	@if(session()->has('message'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">x</button>
					{{ session()->get('message') }}
			</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Catagory</span>
						<span class="caption-helper">List...</span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-info btn-sm ParentAdd" onclick="AddCategory(this)" data-parent="0">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					@php
						$category = \App\Model\Category::where('id_parent', 0)->get();
					@endphp
					<ul class="treeview">
						@foreach ($category as $key => $value)
							<li class="row">
								<a href="#id_{{ $value->id }}" data-toggle="collapse">
									<i class="icon-control-play" style="color: blue; position: static;"></i>
									{{ $value->name }}
								</a>
								<span style="float: right;">
									<button type="button" name="button" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $value->id }}" data-name="{{ $value->name }}" data-parent="{{$value->id}}" data-id="{{ $value->id }}" onclick="AddCategory(this)" >
										<i class="fa fa-plus"></i>
									</button>
									<button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $value->id }}" data-parent="{{$value->id_parent}}" data-id="{{ $value->id }}" data-name="{{ $value->name }}" onclick="DeleteCategory(this)">
										<i class="fa fa-trash"></i>
									</button>
									@if ($value->status == 'Y')
										<button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $value->id }}" data-name="{{ $value->name }}" data-parent="{{ $value->id_parent}}" data-id="{{ $value->id }}" onclick="EditCategory(this)" >
											<i class="fa fa-eye"></i>
										</button>
									@else
										<button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $value->id }}" data-name="{{ $value->name }}" data-parent="{{ $value->id_parent}}" data-id="{{ $value->id }}" onclick="EditCategory(this)" >
											<i class="fa fa-eye-slash"></i>
										</button>
									@endif
								</span>
							</li>
							<ul id="id_{{ $value->id }}" class="collapse">
								@php
									$category1 = \App\Model\Category::where('id_parent', $value->id)->get();
								@endphp
								@foreach ($category1 as $key1 => $value1)
									@php
										if ($value->status == 'N') {
											$value1->status = 'N';
										}
									@endphp
									<li class="row">
										<a href="#id_{{ $value1->id }}" data-toggle="collapse">
											<i class="icon-control-play" style="color: blue; position: static;"></i>
											{{ $value1->name }}
										</a>
										<span style="float: right;">
											<button type="button" name="button" data-id="{{ $value1->id }}" data-name="{{ $value1->name }}" onclick="AddCategory(this)" class="btn btn-sm btn-primary btn-circle childCategory childCategory_{{ $value1->id }}" data-parent="{{$value1->id}}">
												<i class="fa fa-plus"></i>
											</button>
											<button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $value1->id }}" data-parent="{{$value1->id_parent}}" data-id="{{ $value1->id }}" data-name="{{ $value1->name }}" onclick="DeleteCategory(this)">
												<i class="fa fa-trash"></i>
											</button>
											@if ($value1->status == 'Y')
												<button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $value1->id }}" data-parent="{{$value1->id_parent}}" data-id="{{ $value1->id }}" data-name="{{ $value1->name }}" onclick="EditCategory(this)">
													<i class="fa fa-eye"></i>
												</button>
											@else
												<button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $value1->id }}" data-name="{{ $value1->name }}" data-parent="{{ $value1->id_parent}}" data-id="{{ $value1->id }}" onclick="EditCategory(this)" >
													<i class="fa fa-eye-slash"></i>
												</button>
											@endif
										</span>
									</li>
									<ul id="id_{{ $value1->id }}" class="collapse">
										@php
											$category2 = \App\Model\Category::where('id_parent', $value1->id)->get();
										@endphp
										@foreach ($category2 as $key2 => $value2)
											@php
												if ($value1->status == 'N') {
													$value2->status = 'N';
												}
											@endphp
											<li class="row">
												<i class="icon-grid" style="color: blue; position: static;"></i>
												{{ $value2->name }}
												<span style="float: right;">
													<a href="{{ URL::to('/catalog/admin/category/spec/'.$value2->id) }}" class="btn btn-sm btn-info btn-circle" target="_blank" data-toggle="tooltip" title="Specification" data-placement="bottom">
														<i class="fa fa-list"></i>
													</a>
													<button type="button" name="button" class="btn btn-sm btn-danger btn-circle childCategory childCategory_{{ $value2->id }}" data-parent="{{$value2->id_parent}}" data-id="{{ $value2->id }}" data-name="{{ $value2->name }}" onclick="DeleteCategory(this)">
														<i class="fa fa-trash"></i>
													</button>
													@if ($value2->status == 'Y')
														<button type="button" name="button" class="btn btn-sm btn-success btn-circle childCategory childCategory_{{ $value2->id }}" data-parent="{{$value2->id_parent}}" data-id="{{ $value2->id }}" data-name="{{ $value2->name }}" onclick="EditCategory(this)">
															<i class="fa fa-eye"></i>
														</button>
													@else
														<button type="button" name="button" class="btn btn-sm btn-warning btn-circle childCategory childCategory_{{ $value2->id }}" data-name="{{ $value2->name }}" data-parent="{{ $value2->id_parent}}" data-id="{{ $value2->id }}" onclick="EditCategory(this)" >
															<i class="fa fa-eye-slash"></i>
														</button>
													@endif
												</span>
											</li>
										@endforeach
									</ul>
								@endforeach
							</ul>
						@endforeach
					</ul>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<!-- Modal -->
	  <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog">
				<form class="ModalForm" method="post">
					{{ csrf_field() }}
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">New Category</h4>
	        </div>
	        <div class="modal-body">
						<div class="id" style="display:none;">

						</div>
						<div class="form-group" id="labelCat">
						    <label for="exampleInputEmail1" class="bmd-label-floating">Category</label>
								<input id="id_parent" type="hidden" class="form-control" name="id_parent" placeholder="id_parent" readonly>
						    <input id="parent" type="text" class="form-control" name="parent" placeholder="parent" readonly>
						    <span class="bmd-help"></span>
						</div>
						<div class="form-group" id="labelSub">
						    <label for="name" class="bmd-label-floating" id="label">SubCategory</label>
						    <input id="name" type="text" required class="form-control" name="name" placeholder="name">
						    <span class="bmd-help"></span>
						</div>
						<div class="form-group">
						    <label>
						      <input type="radio" required checked name="status" class="optionsRadios2" value="Y"> Active
						    </label>
						    <label>
						      <input type="radio" required name="status" class="optionsRadios2" value="N"> Non Active
						    </label>
						 </div>
					</div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	          <button type="submit" class="btn btn-primary">Save</button>
	        </div>
	      </div>
			</form>

	    </div>
	  </div>@endsection

@section('js')
<script type="text/javascript">
	function AddCategory(a) {
		var parent = $(a).attr('data-parent');
		$('.ModalForm').attr('action',"{{URL::to('catalog/admin/category/add')}}");
		if(parent != 0){
			$('#id_parent').attr('value', $(a).attr('data-parent'))
			$('#parent').attr('value', $(a).attr('data-name'))
			$('.childCategory').attr('data-toggle','modal');
			$('.childCategory').attr('data-target','#myModal');
		}else{
			$('#id_parent').attr('value', $(a).attr('data-parent'))
			$("#labelCat").css("display","none");
			$("#label").text("Category");
			$('.ParentAdd').attr('data-toggle','modal');
			$('.ParentAdd').attr('data-target','#myModal');
		}
	}

	function EditCategory(a) {
		var parent = $(a).attr('data-parent');
		var id = $(a).attr('data-id');
		var name = $(a).attr('data-name')
		$('.ModalForm').attr('action',"{{URL::to('catalog/admin/category/update')}}");
		$('#id_parent').attr('value', $(a).attr('data-parent'))
		$("#labelCat").css("display","none");
		$("#label").text("Category");
		$("#name").attr("value", name);
		$('div.id').html('<input type="hidden" name="id" value="'+id+'">');
		$('.childCategory').attr('data-toggle','modal');
		$('.childCategory').attr('data-target','#myModal');
	}

	function DeleteCategory(a) {
		var parent = $(a).attr('data-parent');
		var id = $(a).attr('data-id');
		var name = $(a).attr('data-name')
		var url = "{{URL::to('catalog/admin/category/delete')}}"
		if(parent == 0)	var konfirmasi = "Are you sure delete  "+name+" ? Sub Category will automatically deleted"
		else var konfirmasi = "Are you sure delete  "+name+" ?"

		var r = confirm(konfirmasi);
			if (r == true) {
					$.ajax({
							type: "post",
							url: url,
						data: {
								"_token": "{{ csrf_token() }}",
								"id": id,
								"parent": parent,
								},
							success: function (a) {
								if(a.status ==1){
									$("div#notif").removeClass();
									$("div#notif").addClass("alert alert-success");
									$("#status").html("delete successfuly!");
									$("#notif").fadeTo(2000, 500).slideUp(500, function(){
										$("#notif").slideUp(500);
										location.reload();
									});
								}else{
									$("div#notif").removeClass();
									$("div#notif").addClass("alert alert-danger");
									$("#status").html("delete fail!");
									$("#notif").fadeTo(2000, 500).slideUp(500, function(){
										$("#notif").slideUp(500);
										location.reload();
									});
								}
							}
					});
			}
	}

	$(document).ready(function(){
		$("div#notif").hide();
	})
</script>
@endsection

@section('xjs')
	<script type="text/javascript">

	</script>
@endsection
