@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('xcss')
	<style media="screen">
		.up {
			text-transform: uppercase;
		}
	</style>
@endsection

@section('page-title')
	RSHK <small>Category...</small>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Admin
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Category
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Specification
		</li>
	</ul>
@endsection

@section('toolbar')
	<div class="btn-group pull-right hide">
		<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
		Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>
				<a href="#">Action</a>
			</li>
			<li>
				<a href="#">Another action</a>
			</li>
			<li>
				<a href="#">Something else here</a>
			</li>
			<li class="divider">
			</li>
			<li>
				<a href="#">Separated link</a>
			</li>
		</ul>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success" id="notif">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<strong id="status"></strong><p id="kata_status"></p>
			</div>
			@if(session()->has('message'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">x</button>
							{{ session()->get('message') }}
					</div>
			@endif
			<!-- BEGIN PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase">Category</span>
						<span class="caption-helper" style="text-transform: capitalize;">{{ \App\Model\Category::find($id)->name }}...</span>
						<span class="caption-subject bold uppercase">Specification</span>
						<span class="caption-helper">List...</span>
					</div>
					<div class="actions">
						<a href="#modal_add" class="btn btn-circle btn-info btn-sm" data-toggle="modal">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered" style="width: 100%">
						<thead>
							<tr>
								<th style="width: 25%">#</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($category_spec as $key => $value)
								<tr>
									<td class="text-center">
										<button type="button" name="button" class="btn btn-xs btn-danger" data-name="{{ $value->name }}" data-id="{{$value->id}}" data-category="{{$id}}" onclick="DeleteSpec(this)">
											<i class="fa fa-trash"></i>
										</button>
										<button type="button" name="button" class="btn btn-xs btn-info edit" data-name="{{ $value->name }}" data-id="{{$value->id}}" data-category="{{$id}}" onclick="EditSpec(this)">
											<i class="fa fa-pencil"></i>
										</button>
									</td>
									<td class="up">{{ $value->name }}</td>
								</tr>
							@endforeach
							@if (sizeof($category_spec) == 0)
								<tr>
									<td colspan="2" style="text-align:center;">No Data Available...</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>

	<!-- Modal -->
		<div class="modal fade" id="modal_add" role="dialog">
			<div class="modal-dialog">
				<form class="ModalForm" method="post" action="{{URL::to('catalog/admin/category/spec/add')}}">
					{{ csrf_field() }}
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">New Category Spesification</h4>
					</div>
					<div class="modal-body">
						<div class="id" style="display:none;">

						</div>
						<div class="form-group" id="labelCat">
								<label for="exampleInputEmail1" class="bmd-label-floating">Category</label>
								<input id="id_category_spec" type="hidden" class="form-control" name="id_category_spec" placeholder="id_category_spec" value="{{$id}}" readonly>
								<input id="name_cateory" type="text" class="form-control" name="name_cateory" placeholder="name_cateory" value="{{$name}}" readonly>
								<span class="bmd-help"></span>
						</div>
						<div class="form-group" id="labelSub">
								<label for="name" class="bmd-label-floating" id="label">Spesification Name</label>
								<input id="name" type="text" required class="form-control" name="name" placeholder="name">
								<span class="bmd-help"></span>
						</div>
						<div class="form-group">
								<label>
									<input type="radio" required checked name="status" class="optionsRadios2" value="Y"> Active
								</label>
								<label>
									<input type="radio" required name="status" class="optionsRadios2" value="N"> Non Active
								</label>
						 </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</form>

			</div>
		</div>
@endsection

@section('js')
<script type="text/javascript">
function EditSpec(a) {
	$('.ModalForm').attr('action',"{{URL::to('catalog/admin/category/spec/update')}}");
	$('.edit').attr('data-toggle','modal');
	$('.edit').attr('data-target','#modal_add');
	$('#name').attr("value", $(a).attr('data-name'));
	$('#id_category_spec').attr("value", $(a).attr('data-id'));
}

function DeleteSpec(a) {
	var name = $(a).attr('data-name');
	var id = $(a).attr('data-id');
	var url = "{{URL::to('catalog/admin/category/spec/delete')}}";
	var r = confirm("Are you sure delete "+name+" ?");
		if (r == true) {
			$.ajax({
					type: "post",
					url: url,
					data: {
						"_token": "{{ csrf_token() }}",
						"id": id,
						},
					success: function (a) {
						if(a.status ==1){
							$("div#notif").removeClass();
							$("div#notif").addClass("alert alert-success");
							$("#status").html("delete successfuly!");
							$("#notif").fadeTo(2000, 500).slideUp(500, function(){
								$("#notif").slideUp(500);
								location.reload();
							});
						}else{
							$("div#notif").removeClass();
							$("div#notif").addClass("alert alert-danger");
							$("#status").html("delete fail!");
							$("#notif").fadeTo(2000, 500).slideUp(500, function(){
								$("#notif").slideUp(500);
								location.reload();
							});
						}
					}
			});
		}
}

$(document).ready(function(){
	$("div#notif").hide();
})
</script>
@endsection

@section('xjs')
	<script type="text/javascript">

	</script>
@endsection
