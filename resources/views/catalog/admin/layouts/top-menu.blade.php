<ul class="nav navbar-nav pull-right">
  <!-- BEGIN USER LOGIN DROPDOWN -->
  <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
  <li class="dropdown dropdown-user">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
    <img alt="" class="img-circle" src="/sa_catalog/assets/admin/layout/img/avatar.png"/>
    <span class="username username-hide-on-mobile">
      {{ Auth::user()->roles()->first()->name }}
    </span>
    <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-menu-default">
      <li>
        <a href="{{URL::to('catalog/admin/reset')}}">
              <i class="icon-key"></i>Ganti Password
        </a>

      </li>


      <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="icon-key"></i>Log Out
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>
  <!-- END USER LOGIN DROPDOWN -->
  <!-- BEGIN QUICK SIDEBAR TOGGLER -->
</ul>
