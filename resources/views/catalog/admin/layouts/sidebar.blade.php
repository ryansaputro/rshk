<li class="start ">
  <a href="{!! route('x.catalog.admin') !!}">
  <i class="icon-home"></i>
  <span class="title">Dashboard</span>
  </a>
</li>
<li class="heading">
  <h3 class="uppercase">Features</h3>
</li>
<li class="">
  <a href="{!! route('x.catalog.admin.item') !!}">
  <i class="icon-grid"></i>
  <span class="title">Item</span>
  </a>
</li>
<li class="">
  <a href="{{URL::to('catalog/admin/users/vendor')}}">
  <i class="icon-user"></i>
  <span class="title">Penyedia</span>
  </a>
</li>
<li class="">
  <a href="{{URL::to('catalog/admin/contract')}}">
  <i class="icon-doc"></i>
  <span class="title">Kontrak</span>
  </a>
</li>
<li class="">
  <a href="{!! route('x.catalog.admin.category') !!}">
  <i class="icon-list"></i>
  <span class="title">Kategori</span>
  </a>
</li>
<li class="heading">
  <h3 class="uppercase">Pengaturan</h3>
</li>
<li class="last ">
  <a href="javascript:;">
  <i class="icon-users"></i>
  <span class="title">Pengguna</span>
  <span class="arrow "></span>
  </a>
  <ul class="sub-menu">


     <li>
      <a href="{!! route('x.catalog.admin.users.ppk') !!}">
        <i class="icon-user"></i>
        <span class="title">PPK</span>
      </a>
    </li>


    <li>
      <a href="{!! route('x.catalog.admin.users.direktur_penunjang') !!}">
        <i class="icon-user"></i>
        <span class="title">Kepala Instalasi</span>
      </a>
    </li>


    <li>
      <a href="{!! route('x.catalog.admin.users.pphp') !!}">
        <i class="icon-user"></i>
        <span class="title">PPHP</span>
      </a>
    </li>

    <li>
      <a href="{!! route('x.catalog.admin.users.gudang') !!}">
        <i class="icon-user"></i>
        <span class="title">Gudang</span>
      </a>
    </li>

    <li>
      <a href="{!! route('x.catalog.admin.users.finance') !!}">
        <i class="icon-user"></i>
        <span class="title">Keuangan</span>
      </a>
    </li>

    {{-- <li>
      <a href="{!! route('x.catalog.admin.users.users_depo') !!}">
        <i class="icon-user"></i>
        <span class="title">Depo</span>
      </a>
    </li> --}}


     <!-- <li>
      <a href="{!! route('x.catalog.admin.users.user_staff') !!}">
        <i class="icon-user"></i>
        <span class="title">User Staff</span>
      </a>
    </li> -->



  </ul>
</li>


<li class="last ">
  <a href="javascript:;">
  <i class="icon-list"></i>
  <span class="title">Master Gudang</span>
  <span class="arrow "></span>
  </a>
  <ul class="sub-menu">


     <li class="">
          <a href="{!! route('x.catalog.admin.users.master_gudang') !!}">
          <i class="icon-list"></i>
          <span class="title">Gudang</span>
          </a>
  </li>


    <li class="">
        <a href="{!! route('x.catalog.admin.users.jenis_gudang') !!}">
        <i class="icon-list"></i>
        <span class="title">Jenis Gudang</span>
        </a>
    </li>

    <li class="">
        <a href="{!! route('x.catalog.admin.users.inisial_gudang') !!}">
        <i class="icon-list"></i>
        <span class="title">Satuan Gudang</span>
        </a>
    </li>

    <li class="">
        <a href="{!! route('x.catalog.admin.users.depo') !!}">
        <i class="icon-list"></i>
        <span class="title">Depo Gudang</span>
        </a>
    </li>

  </ul>
</li>

<li class="">
  <a href="{!! route('x.catalog.admin.users.satuan') !!}">
  <i class="icon-list"></i>
  <span class="title">Master Satuan</span>
  </a>
</li>

<li class="">
  <a href="{!! route('x.catalog.admin.users.konversi') !!}">
  <i class="icon-list"></i>
  <span class="title">Master Penyedia</span>
  </a>
</li>





<!-- <li class="">
  <a href="{!! route('x.catalog.admin.category') !!}">
  <i class="icon-list"></i>
  <span class="title">Konversi Satuan</span>
  </a>
</li> -->

<!-- <li class="">
  <a href="{!! route('x.catalog.admin.category') !!}">
  <i class="icon-list"></i>
  <span class="title">Format Penomoran</span>
  </a>
</li>


<li class="">
  <a href="{!! route('x.catalog.admin.users.master_vendor') !!}">
  <i class="icon-list"></i>
  <span class="title">Master Vendor</span>
  </a>
</li> -->
