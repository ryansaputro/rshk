@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Ganti Password
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')
					               <div class="col-lg-12">
			                            @if(session('message'))
			                                    <div class='alert alert-success'>
			                                           {{session('message')}}
			                                    </div>
			                            @endif
                           </div>

                           <div class="container">
                               <div class="row">
                                   <div class="col-md-10">
                                     <div class="panel panel-default">
                                       <div class="panel-heading">Change password</div>
                                         <div class="panel-body">
                                             @if (session('error'))
                                             <div class="alert alert-danger">
                                             {{ session('error') }}
                                             </div>
                                             @endif
                                                 @if (session('success'))
                                                 <div class="alert alert-success">
                                                 {{ session('success') }}
                                                 </div>
                                                 @endif


                                   <form id="add" class="form-horizontal" method="POST" action="{{URL::to('catalog/admin/action_ganti_password')}}">
                                       {{ csrf_field() }}
                                       <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                       <label for="new-password" class="col-md-4 control-label">Current Password</label>
                                       <div class="col-md-6">
                                       <input id="current-password" type="password" class="form-control" name=" current_password" required>
                                       @if ($errors->has('current-password'))
                                       <span class="help-block">
                                       <strong>{{ $errors->first('current-password') }}</strong>
                                       </span>
                                       @endif
                                       </div>
                                       </div>
                                       <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                       <label for="new-password" class="col-md-4 control-label">New Password</label>
                                       <div class="col-md-6">
                                       <input id="new-password" type="password" class="form-control" name="new_password" required>
                                       @if ($errors->has('new-password'))
                                       <span class="help-block">
                                       <strong>{{ $errors->first('new-password') }}</strong>
                                       </span>
                                       @endif
                                       </div>
                                       </div>
                                       <div class="form-group">
                                       <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password</label>
                                       <div class="col-md-6">
                                       <input id="new-password-confirm" type="password" class="form-control" name="new_password_confirmation" required>
                                       </div>
                                       </div>
                                       <div class="form-group">
                                       <div class="col-md-6 col-md-offset-4">
                                       <button type="submit" id="target"  class="btn btn-primary">
                                            Ubah Password
                                       </button>
                                       </div>
                                       </div>
                                 </form>
                               </div>
                              </div>
                             </div>
                           </div>
                      </div>




@endsection
