@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Tambah Penyedia
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')

		<?php
				// echo "<pre>";
				// print_r($data);
				// echo "</pre>";
		 ?>

		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{{route('x.catalog.admin.users.vendor_edit_simpan',$data->id_vendor)}}" method="post">
								{{ csrf_field() }}
								{{ method_field('PUT') }}

								<?php
										$propinsi = Fungsi::propinsi();

										$a = Fungsi::IndonesiaProvince();
										$kota = $a[$propinsi[$data->province]];
										// if (array_key_exists($v->province,$a)){
												//	$kota = $a[$propinsi[$v->province]];
													// echo "<pre>";
													// print_r($kota);
													//
													// echo "<pre>";
										// print_r($data->province);
										// echo "</pre>";

										//echo $propinsi[$data->province];
								?>
								<div class="form-body">


                  <!-- <div class="form-group">
                    <label>Kode Penyedia</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="code"   class="form-control" placeholder="Kode Penyedia">
                    </div>
                  </div> -->


									<input type="hidden" name="id_user" value="{{$data->id_user}}" >

									<div class="form-group">
										<label>Nama Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="vendor_name" required="" value="{{$data->vendor_name}}"  class="form-control" placeholder="Nama Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Pimpinan Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="vendor_directur" required="" value="{{$data->name}}"  class="form-control" placeholder="Pimpinan Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Alamat Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<textarea name="address" class="form-control"  rows="3" required cols="50">{{$data->address}}</textarea>
										</div>
									</div>

									<div class="form-group">
										<label>Provinsi Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
												<select name="province" id="propinsi" required autofocus class="form-control">
														<option value=""  disabled="">- pilih -</option>
														@foreach($propinsi as $k => $v)
																<?php
																		//echo $propinsi[$data->province];
																		if($propinsi[$data->province]==$v){
																			$selected = "selected";

																		}else{
																			$selected = " ";
																		}
																 ?>

																<option value="{{$k}}"{{$selected}}>{{$v}}</option>
														@endforeach
													</select>
										</div>
									</div>

									<div class="form-group">
										<label>Kota Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<select name="city" id="kota" required autofocus class="form-control">
												<option value=""  disabled="">- pilih -</option>
												@foreach($kota as $k => $v)
														<?php
																//echo $propinsi[$data->province];
																// if($propinsi[$data->province]==$v){
																// 	$selected = "selected";
																//
																// }else{
																// 	$selected = " ";
																// }
														 ?>

														<option value="{{$k}}">{{$v}}</option>
												@endforeach


											</select>
										</div>
									</div>


								<div class="form-group">
										<label>No Whatshap</label>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-envelope"></i>
												</span>
														<input type="text" name="mobile_phone" required="" value="{{$data->mobile_phone}}"   class="form-control" placeholder="Telepon Penyedia">
									    </div>
								</div>



									<div class="form-group">
										<label>Telepon Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="telephone" required="" value="{{$data->telephone}}"   class="form-control" placeholder="Telepon Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Nama Bank</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="bank" class="form-control" value="{{$data->bank}}" placeholder="Bank Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>No Rekening</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="no_rek" class="form-control" value="{{$data->no_rek}}" placeholder="Nomor Rekening">
										</div>
									</div>


									<div class="form-group">
										<label>Atas Nama</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="atas_nama" value="{{$data->atas_nama}}" class="form-control" placeholder="Atas Nama">
										</div>
									</div>

									<div class="form-group">
										<label>Email Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="email" name="email" value="{{$data->email}}" class="form-control" placeholder="Email Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Password Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="password" name="password" class="form-control" placeholder="Password Penyedia" id="myInput">
											<input type="checkbox" onclick="myFunction()">Show Password
										</div>
									</div>


                  <div class="form-group">
                    <label>Status Penyedia</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                           <select class="form-control" name="status">

                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                           </select>
                    </div>
                  </div>

								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
@section('js')
<script type="text/javascript">
$('#propinsi').on('change', function(){
    var id = $($(this).children(":selected")).text();
    $.ajax({
        url: '{{URL::to('/register/getKota')}}',
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", "id": id},
        success: function (a) {
            var option = "";
            $.each(a.kota, function(k, v){
                option += "<option value='"+k+"'>"+v+"</option>";
            })
                $('#kota').html(option)
        }
    });
})

function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endsection
