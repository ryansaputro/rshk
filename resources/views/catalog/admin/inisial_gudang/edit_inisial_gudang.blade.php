@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Edit Satuan Gudang
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
						<form role="form" action="{!! route('x.catalog.admin.users.inisial_gudang_edit_simpan',$data->id) !!}" method="post">
								{{ csrf_field() }}
								{{ method_field('PUT') }}
								<div class="form-body">



									<div class="form-group">
										<label>Category</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<?php
													// echo "<pre>";
													// print_r($data);
													// echo "</pre>";
											 ?>

											<select name="id_category" required class="form-control">
                            <option value="">--Pilih Category -- </option>
                            @foreach($category as $c)
														<?php
																if($data->id_category==$c->id){
																	$selected="selected";
																}else{
																	$selected = " ";
																}
														 ?>
                                <option value="{{$c->id}}"<?php echo $selected; ?>>{{$c->name}}</option>
                            @endforeach
											</select>
										</div>
									</div>


                  <div class="form-group">
                    <label>Gudang</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>

                      <select name="id_gudang" required class="form-control">
                        <option value="">--Pilih Gudang -- </option>
                        @foreach($gudang as $g)
												<?php
														if($data->id_gudang==$g->id){
															$selected="selected";
														}else{
															$selected = " ";
														}
												 ?>
                            <option value="{{$g->id}}" {{$selected}}>{{$g->nama_instalasi}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>


									<div class="form-group">
										<label>Deskripsi</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<textarea type="text" required="" name="deskripsi" class="form-control input-circle-right" placeholder="Deskripsi">{{$data->description}}</textarea>
										</div>
									</div>


                  <div class="form-group">
                    <label>Status</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>

                      <select name="status" required class="form-control">


                            <option value="">--Pilih Status-- </option>
                            <option value="1"
														<?php
														if($data->status==1){
															echo "selected";
														}
														?>
														 >Aktif</option>
                            <option value="0"
														<?php
														if($data->status==0){
															echo "selected";
														}
														?>
														>Tidak Aktif</option>


                      </select>
                    </div>
                  </div>



								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
