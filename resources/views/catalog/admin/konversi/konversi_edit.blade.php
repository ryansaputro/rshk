@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Edit Konversi
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
              <form role="form" action="{!! route('x.catalog.admin.users.konversi_edit_simpan',$edit->id) !!}" method="post">
									{{ csrf_field() }}
                  {{ method_field('PUT') }}
								<div class="form-body">






                  <div class="form-group">
										<label>Kategory</label>
										<select class="form-control select2" required="" id="id_kategory" name="id_kategory[]" multiple="multiple">
                      <?php
                      foreach($data as $d){
                        $data1 = DB::table('category')
                              ->where('id_parent',$d->id)
                              ->select('*')
                              ->get();
                      ?>

                          @foreach($data1 as $b)
                              <?php
                                    $data2 = DB::table('category')
                                    ->where('id_parent',$b->id)
                                    ->select('*')
                                    ->get();
                              ?>
                            <optgroup label="{{$b->name}}">
                                <?php
                                    foreach($data2 as $h)
                                    {
                                ?>
                                      <option value="{{$h->id}}">{{$h->name}}</option>
                                <?php
                                    }
                                ?>
                            </optgroup>
                          @endforeach()

                      <?php
                          }

                       ?>



										</select>
										<input type="checkbox" id="allkategory" >All
									</div>


                  <div class="form-group">
										<label>Gudang</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
                      <select name="id_gudang" class="form-control">

                              @foreach($gudang as $g)
                                  <option value="{{$g->id}}">{{$g->nama_instalasi}}</option>
                              @endforeach()
                              <optgroup label="Farmasi">
                                    <?php
                                    $data2 = DB::table('master_depo')
                                    ->select('*')
                                    ->get();
                                    ?>

                                    @foreach($data2 as $d)
                                            <option value="D{{$d->id}}">{{$d->nama_depo}}</option>
                                    @endforeach
                              </optgroup>





                      </select>
										</div>
									</div>


									<div class="form-group">
										<label>Satuan</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
                              <select name="satuan" class="form-control">
                                @foreach($satuan as $d)
                                        <option value="{{$d->id}}">{{$d->nama_satuan}}</option>
                                @endforeach
                              </select>



										</div>
									</div>

                  <div class="form-group">
										<label>Status</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
										             <select name="status" class="form-control">
                                      <option value="1">Aktif</option>
                                      <option value="0">Tidak Aktif<option>
                                 </select>
										</div>
									</div>



								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>

								</div>
							</form>
						</div>
					</div>


          <script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
          <script type="text/javascript">
            $(function () {


                      $("#id_kategory").select2();
                      // $("#divisi_id").select2();
                      // $("#activity_id").select2();
                      // $("#market_type_id").select2();
                      $("#id_kategory").trigger("change");
                      // $("#divisi_id").trigger("change");


                     $("#allkategory").click(function(){
                        if($("#allkategory").is(':checked') ){
                            $("#id_kategory").find('option').prop("selected",true);
                            $("#id_kategory").trigger("change");
                            var id_kategory = $("#id_kategory").val();

                        }else{
                            $("#id_kategory").val('').trigger("change");
                             var id_kategory = $("#id_kategory").val();
                             //alert(id_kategory);
                         }
                    });


                      $("#id_kategory").on('change', function() {
                      var id_kategory  = $("#id_kategory").val();
                      //alert(id_kategory);



                });



                  });

          </script>


@endsection
