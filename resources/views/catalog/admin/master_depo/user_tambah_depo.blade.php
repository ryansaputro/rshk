@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Tambah Users Depo
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{!! route('x.catalog.admin.users.users_depo_simpan') !!}" method="post">
									{{ csrf_field() }}
								<div class="form-body">





                  <div class="form-group">
										<label>Nama Depo</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
										              <select name="id_depo" class="form-control">

                                        @foreach($data as $d)
                                            <option value="{{$d->id}}">{{$d->nama_depo}}</option>
                                        @endforeach

                                  </select>
										</div>
									</div>


                  <div class="form-group">
										<label>Nama</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" required="" name="nama" class="form-control input-circle-right" placeholder="nama">
										</div>
									</div>

									<div class="form-group">
										<label>NIK</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" required="" name="nik" class="form-control input-circle-right" placeholder="NIK">
										</div>
									</div>


									<div class="form-group">
										<label>No Tlpn</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="number" required="" name="no_tlpn" class="form-control input-circle-right" placeholder="No Tlpn">
										</div>
									</div>




									<div class="form-group">
										<label>Email</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="email" required="" name="email" class="form-control" placeholder="Email">
										</div>
									</div>

									<div class="form-group">
										<label>Username</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="username" required="" class="form-control input-circle-right" placeholder="Username">
										</div>
									</div>

									<div class="form-group">
										<label>Password</label>
										<div class="input-group">
											<span class="input-group-addon input-circle-left">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="password" required="" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
										</div>
									</div>


								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
