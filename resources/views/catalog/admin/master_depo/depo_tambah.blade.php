@extends('catalog.admin.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Tambah Finance
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{!! route('x.catalog.admin.users.depo_simpan') !!}" method="post">
									{{ csrf_field() }}
								<div class="form-body">



                  <div class="form-group">
										<label>Nama Gudang</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											     <select class="form-control" name="id_gudang">

                                @foreach($data as $d)

                                    <option value="{{$d->id}}">{{$d->nama_instalasi}}</option>

                                @endforeach
                           </select>
										</div>
									</div>



                  <div class="form-group">
                    <label>Kode Depo</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="kode_depo" required=""  class="form-control" placeholder="Kode Depo">
                    </div>
                  </div>


									<div class="form-group">
										<label>Nama Depo</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="nama_depo" required=""  class="form-control" placeholder="Nama Depo">
										</div>
									</div>

                  <div class="form-group">
                    <label>Status Depo</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                           <select class="form-control" name="status">

                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                           </select>
                    </div>
                  </div>

								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
