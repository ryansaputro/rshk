<ul>
  {{-- <li><a href="{!! route('x.catalog.guest') !!}" >Dashboard</a></li> --}}
  <li><a href="{!! route('x') !!}">PRODUK</a></li>
  {{-- <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Woman
    </a>

    <!-- BEGIN DROPDOWN MENU -->
    <ul class="dropdown-menu">
      <li class="dropdown-submenu">
        <a href="shop-product-list.html">Hi Tops <i class="fa fa-angle-right"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li class="dropdown-submenu">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
              Second Level Link
              <i class="fa fa-angle-right"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li><a href="shop-product-list.html">Running Shoes</a></li>
      <li><a href="shop-product-list.html">Jackets and Coats</a></li>
    </ul>
    <!-- END DROPDOWN MENU -->
  </li> --}}
  <!-- BEGIN TOP SEARCH -->
  <li class="menu-search">
    <span class="sep"></span>
    <i class="fa fa-search search-btn"></i>
    <div class="search-box">
      <form>
        <div class="input-group">
          <input type="text" name="search" placeholder="Search" class="form-control">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Search</button>
          </span>
        </div>
      </form>
    </div>
  </li>
  <!-- END TOP SEARCH -->
</ul>
