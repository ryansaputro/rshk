<div class="sidebar col-md-4 col-sm-12">
  <h2>Kategori</h2>
  <form>
  <ul class="list-group margin-bottom-25 sidebar-menu">
    @php
      $category = App\Model\Category::where('id_parent', 0)->where('status', 'Y')->get();
    @endphp
    @foreach ($category as $key => $value)
      <li class="list-group-item clearfix dropdown active">
        <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value->name }}</a>
        <ul class="dropdown-menu" style="display:block;">
          @php
            $category1 = App\Model\Category::where('id_parent', $value->id)->where('status', 'Y')->get();
          @endphp
          @foreach ($category1 as $key1 => $value1)
            <li class="list-group-item dropdown clearfix ">
              <a href="javascript:void(0);" class=""><i class="fa fa-angle-right"></i> {{ $value1->name }}</a>
              <ul class="dropdown-menu" >
                @php
                $category2 = App\Model\Category::where('id_parent', $value1->id)->where('status', 'Y')->get();
                @endphp
                @foreach ($category2 as $key2 => $value2)
                  <li class="list-group-item clearfix">
                    <a href="javascript:void(0);"  onclick="SearchCategory(this)" data-search="{{$value2->id}}"><i class="fa fa-archive"></i> {{ $value2->name }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach
  </ul>
  <input type="hidden" name="category" value="" id="category">
  <button type="submit" style="display:none;" id="searchByCategory">oke</button>
  </form>
</div>
