@extends('catalog.guest.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
<style>
  .glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 50px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #31d8c8;
    border:1;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}

div.caption {
  /* overflow: hidden; padding:10px; margin-top:10px; height:auto; background-color:#f9f9f9;  */
  /* min-height: 300px !important;
  overflow: hidden; */
  /* margin-bottom: 90px; */
  /* white-space: nowrap; */
  overflow-wrap: break-word;
}

</style>
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
    RSHK
  </a>
@endsection

@section('sidebar')
  @include('catalog.guest.layouts.sidebar')
@endsection

@section('content')



 <div class="col-md-8 col-sm-12">
     @if(session('message'))
       <div class='alert alert-success'>
         {{session('message')}}
       </div>
     @endif
<div class="row product-list">
    <div class="well well-sm" style="margin-top:32px;">
        <strong>Display</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    <div id="products" class="row list-group">

    @if(count($item) > 0)
      @foreach ($item as $key => $value)
        <div class="item col-lg-4">
            <div class="thumbnail" style="border:3px solid #ddd;">
                @php
                $image_count = DB::table('image_item')
                               ->where('id_item', $value->id)
                               ->groupBy('id_item','id','name','file','created_at','updated_at')
                               ->get();


                $image_item = DB::table('image_item')
                              ->where('id_item', $value->id)
                              ->groupBy('id_item','id','name','file','created_at','updated_at')
                              ->value('file');
                @endphp
              @if (sizeof($image_count) == 0)
                <a href="/assets/catalog/item/lost.png" class="group list-group-image btn btn-default fancybox-button">
                  <img src="/assets/catalog/item/lost.png"  class="img-responsive group list-group-image" alt="">
                </a>
                <div>
                </div>
              @else
                @php
                $gambar = $image_item;
                $img_oke = explode('|', $gambar);
                $imp_gmb = explode('/', $img_oke[0]);
                @endphp
                <a
                  href="{{(($imp_gmb[0] == 'https:') || ($imp_gmb[0] == 'http:')) ? $img_oke[0] : '/assets/catalog/item/'.$image_item}}"
                  class="btn btn-default fancybox-button" style="width:100%;">
                  <img
                  src="{{(($imp_gmb[0] == 'https:') || ($imp_gmb[0] == 'http:')) ? $img_oke[0] : '/assets/catalog/item/'.$image_item}}"
                  class="img-responsive group list-group-image" style="width:150px; object-fit: cover; height:150px;"  alt="">
                </a>
              @endif
                <div>
                  @php
                    $sub_category = DB::table('category_item')->where('id_item', $value->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('id_parent');
                    $category = DB::table('category')->where('id', $sub_category)->value('id_parent');
                  @endphp
                      <div class="" style="padding-left:10px; padding-right:10px; padding-top:10px; clear: both; min-height:70px;color: #c33320;">
                        <span class="fa fa-bookmark"></span>
                        {{ DB::table('category')->where('id', $category)->value('name') }}
                        <span class="fa fa-caret-right"></span>
                        {{ DB::table('category')->where('id', $sub_category)->value('name') }}
                        <span class="fa fa-caret-right"></span>
                        {{ DB::table('category_item')->where('id_item', $value->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('name') }}
                      </div>
                </div>


                <div class="caption">
                  <div class="" style='clear: both; min-height:60px;'>
                    <h4 class="group inner list-group-item-heading">
                      <a href="{{ URL::to('/catalog/guest/item/detail/'.$value->id) }}" style="text-decoration: none; font-size: 14px;">
                        <b>
                          {{ $value->code }}
                          {{ $value->name }}
                          {{ $value->merk }}
                        </b>
                      </a>
                    </h4>
                  </div>
                  @php
                  $time = strtotime($value->price_date);
                  $time_release = strtotime($value->release_date);
                  $time_expired_date = strtotime($value->expired_date);
                  $functionDay =  Fungsi::DayIndonesia();
                  $function =  Fungsi::MonthIndonesia();
                  @endphp
              <div class="pull" style="color: black; min-height:30px;">
                <small>Harga Rumah Sakit : </small>
                {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                @if ($value->price_country == 0)
                  {{ number_format($value->price_gov, 0, ',', '.') }}
                @else
                  {{ number_format($value->price_gov, 0, '.', ',') }}
                @endif
                <small>Harga : </small>
                {{$functionDay[date('l', $time)]}}, {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}
                <br>
                <div class="" style="color: {{ $value->stock == 0 ? 'red' : 'green'}};">
                  <small>
                    {{ $value->stock == 0 ? 'Stok : Tidak Tersedia' : 'Stok : '.$value->stock.' '.$value->unit_name.' Tersedia'}}
                  </small>
                </div>

              </div>
              <div class="pull-right" style="color: black;">
              </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12" style="min-height:65px;">
                            <br>
                            <b>{{$value->vendor_name}}</b></p>
                            <p class="lead">
                        </div>
                        <div class="col-xs-12 col-md-12">
                              <a href="{{ URL::to('/catalog/guest/item/detail/'.$value->id) }}" style="margin-bottom:1px;" class="btn btn-success btn-sm btn-round tooltips btn-block" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}">
                                <span class="fa fa-info"></span> Detail
                              </a>
                        <?php
                        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                        ?>
                        @if($id_role == '3')
                            @if($value->stock != 0)
                              <button type="button" style="border-radius: 0px !important;" name="button" class="btn btn-primary btn-sm btn-round tooltips BtnCart btn-block" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}" onclick="AddToCart(this)">
                                <span class="fa fa-shopping-cart"></span> Keranjang
                              </button>
                            @endif
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
        @endif



</div>




          <!-- BEGIN PAGINATOR -->
          <div class="row">
      <div class="col-md-5 col-sm-5">
        <div>menampilkan {{ ($item->currentPage() - 1) * $item->perPage() + 1 }} sampai {{ $item->count() * $item->currentPage() }} dari {{ $item->total() }} data</div>
      </div>
        <div class="col-md-7 col-sm-7 block-paginate">{{$item->appends(['search' => $search, 'category' => $filterByCategory])->links() }}</div>
    </div>
    <!-- END PAGINATOR -->

</div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
  <div id="product-pop-up" style="display: none; width: 700px;">
    <div class="product-page product-pop-up">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-3">
          <div class="product-main-image">
            <img src="/su_catalog/assets/frontend/pages/img/products/model7.jpg" alt="Cool green dress with red bell" class="img-responsive">
          </div>
          <div class="product-other-images">
            <a href="javascript:;" class="active"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model3.jpg"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model4.jpg"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model5.jpg"></a>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-9">
          <h2>Cool green dress with red bell</h2>
          <div class="price-availability-block clearfix">
            <div class="price">
              <strong><span>$</span>47.00</strong>
              <em>$<span>62.00</span></em>
            </div>
            <div class="availability">
              Availability: <strong>In Stock</strong>
            </div>
          </div>
          <div class="description">
            <p>
              Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat Nostrud duis molestie at dolore.
            </p>
          </div>
          <div class="product-page-options">
            <div class="pull-left">
              <label class="control-label">Size:</label>
              <select class="form-control input-sm">
                <option>L</option>
                <option>M</option>
                <option>XL</option>
              </select>
            </div>
            <div class="pull-left">
              <label class="control-label">Color:</label>
              <select class="form-control input-sm">
                <option>Red</option>
                <option>Blue</option>
                <option>Black</option>
              </select>
            </div>
          </div>
          <div class="product-page-cart">
            <div class="product-quantity">
              <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
            </div>
            <button class="btn btn-primary" type="submit">Add to cart</button>
            <a href="shop-item.html" class="btn btn-default">More details</a>
          </div>
        </div>

        <div class="sticker sticker-sale"></div>
      </div>
    </div>
  </div>



@endsection

@section('js')
<script>
$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
});
</script>
<script type="text/javascript">
  function SearchCategory(a) {
    $('#category').attr('value', $(a).attr('data-search'));
    $('#searchByCategory').trigger("click");
  }
</script>
@endsection
