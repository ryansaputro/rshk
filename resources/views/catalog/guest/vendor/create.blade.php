@extends('catalog.guest.layouts.app')

@section('title')
	Catalog
@endsection

@section('page-title')
	RSHK <small>Users Config...</small>
@endsection

@section('xcss')
	<style media="screen">
		.treeview{font-size:18px;color:#000;list-style-type:none;padding:33px;text-transform:uppercase;vertical-align:-webkit-baseline-middle}.treeview ul{list-style-type:none;padding-left:10px}.treeview li{margin-bottom:10px;padding:10px;border-style:dotted solid}.treeview>ul>li{margin-left:15px}.treeview>ul>ul>li{margin-left:45px}.treeview li a{text-decoration:none;color:#000}
	</style>
@endsection

@section('breadcrumb')
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Catalog
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			Tambah Penyedia
			<i class="fa fa-angle-right"></i>
		</li>
		<!-- <li>
			Users Config
		</li> -->
	</ul>
@endsection

@section('content')


		<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="{{URL::to('vendor/register')}}" method="post">
									{{ csrf_field() }}
								<div class="form-body">


                  <div class="form-group">
                    <label>No Kontrak</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="no_contract" required=""  class="form-control" placeholder="Nomor Kontrak">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Tanggal Berlaku</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="date" name="start_date" required=""  class="form-control" placeholder="Tanggal Mulai Kontrak">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Tanggal Berakhir</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="date" name="end_date" required=""  class="form-control" placeholder="Tanggal Kontrak Berakhir">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Deskripsi</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
											<textarea name="description" required=""  class="form-control" placeholder="Deskripsi Kontrak"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kode Penyedia</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="code" required=""  class="form-control" placeholder="Kode Penyedia">
                    </div>
                  </div>

									<div class="form-group">
										<label>Nama Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="vendor_name" required=""  class="form-control" placeholder="Nama Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Pimpinan Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="vendor_directur" required=""  class="form-control" placeholder="Pimpinan Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>No Whatsapp</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="mobile_phone" required=""  class="form-control" placeholder="No  Whatsapp Perusahaan">
										</div>
									</div>

									<div class="form-group">
										<label>Alamat Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<textarea name="address" class="form-control" rows="3" required cols="50"></textarea>
										</div>
									</div>

									<div class="form-group">
										<label>Provinsi Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
												<select name="province" id="propinsi" required autofocus class="form-control">
														<option value="" selected="" disabled="">- pilih -</option>
														@foreach($propinsi as $k => $v)
																<option value="{{$k}}">{{$v}}</option>
														@endforeach
													</select>
										</div>
									</div>

									<div class="form-group">
										<label>Kota Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<select name="city" id="kota" required autofocus class="form-control">
											</select>
										</div>
									</div>

									<div class="form-group">
										<label>Telepon Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="telephone" required=""  class="form-control" placeholder="Telepon Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Nama Bank</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="bank" class="form-control" placeholder="Bank Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>No Rekening</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="no_rek" class="form-control" placeholder="Nomor Rekening">
										</div>
									</div>

									<div class="form-group">
										<label>Atas Nama</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" name="on_behalft" class="form-control" placeholder="Atas Nama">
										</div>
									</div>

									<div class="form-group">
										<label>Email Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="email" name="email" class="form-control" placeholder="Email Penyedia">
										</div>
									</div>

									<div class="form-group">
										<label>Password Penyedia</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="password" name="password" class="form-control" placeholder="Password Penyedia" id="myInput">
											<input type="checkbox" onclick="myFunction()">Show Password
										</div>
									</div>


                  {{-- <div class="form-group">
                    <label>Status Penyedia</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                           <select class="form-control" name="status">

                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                           </select>
                    </div>
                  </div> --}}

								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</form>
						</div>
					</div>


@endsection
@section('js')
<script type="text/javascript">
$('#propinsi').on('change', function(){
    var id = $($(this).children(":selected")).text();
    $.ajax({
        url: '{{URL::to('/register/getKota')}}',
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", "id": id},
        success: function (a) {
            var option = "";
            $.each(a.kota, function(k, v){
                option += "<option value='"+k+"'>"+v+"</option>";
            })
                $('#kota').html(option)
        }
    });
})

function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endsection
