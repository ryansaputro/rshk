@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/frontend/pages/css/loading.css" rel="stylesheet" type="text/css">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
    RSHK
  </a>
@endsection

@section('sidebar')
  @include('catalog.users.layouts.sidebar')
@endsection

@section('content')
  <div class="col-md-8 col-sm-12">
    <div class="alert alert-success" id="notif" style="display:none;">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h2>List Produk </h2>
      </div>
    </div>
    <div class="row product-list">
    @if($cate!=null)
      @if(count($item) > 0)
      @foreach ($item as $key => $value)
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="product-item row">
            <div class="pi-img-wrapper col-md-2 col-sm-2 col-xs-2">
              <br>
              @php
                $image_count = DB::table('image_item')
                               ->where('id_item', $value->id)
                               ->groupBy('id_item','id','name','file','created_at','updated_at')
                               ->get();
                $image_item = DB::table('image_item')
                              ->where('id_item', $value->id)
                              ->groupBy('id_item','id','name','file','created_at','updated_at')
                              ->value('file');
              @endphp
              @if (sizeof($image_count) == 0)
                <img src="/assets/catalog/item/lost.png" class="img-responsive" alt="">
                <div>
                  <a href="/assets/catalog/item/lost.png" class="btn btn-default fancybox-button">Zoom</a>
                  {{-- <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a> --}}
                </div>
              @else
                @php
                  $gambar = $image_item;
                  $imp_gmb = explode('/', $gambar);
                @endphp
                <img src="{{(($imp_gmb[0] == 'https:') || ($imp_gmb[0] == 'http:')) ? $gambar : '/assets/catalog/item/'.$image_item}}" class="img-responsive" alt="">

                <div>
                  <a href="/assets/catalog/item/{{ $image_item }}" class="btn btn-default fancybox-button">Zoom</a>
                  {{-- <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a> --}}
                </div>
              @endif
            </div>
            @if ($value->production_origin == 0)
              <div class="sticker sticker-local"></div>
            @else
              <div class="sticker sticker-import"></div>
            @endif
            <div class="col-md-5 col-sm-5 col-xs-5">
              @php
              $sub_category = DB::table('category_item')->where('id_item', $value->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('id_parent');
              $category = DB::table('category')->where('id', $sub_category)->value('id_parent');
              @endphp
              <span class="fa fa-bookmark"></span>
              {{ DB::table('category')->where('id', $category)->value('name') }}
              <span class="fa fa-caret-right"></span>
              {{ DB::table('category')->where('id', $sub_category)->value('name') }}
              <span class="fa fa-caret-right"></span>
              {{ DB::table('category_item')->where('id_item', $value->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('name') }}
              <br>
              <br>
              <br>
              <a href="{{ URL::to('/catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none; font-size: 14px;">
                <b>{{ $value->code }}
                <br>
                {{ $value->name }}
                <br>
                {{ $value->merk }}</b>
              </a>
              <br>
              <br>
              <br>
              <a href="#" style="text-decoration: none; color: black;">
                {{-- {{ DB::table('supplier_item')->where('id_item', $value->id)->join('supplier', 'supplier.id', '=', 'supplier_item.id_supplier')->value('name') }} --}}
                {{-- <b> PENYEDIA : {{ DB::table('supplier_item')->where('id_item', $value->id)->join('vendor_detail', 'vendor_detail.id', '=', 'supplier_item.id_supplier')->value('vendor_name') }} </b> --}}
                <b>PENYEDIA : {{$value->vendor_name}}</b>
              </a>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-5">
              <div class="" style="color: black;">
                <b>Detail Harga :</b><br><br>
                <small>Harga Retail : </small>
                {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                @if ($value->price_country == 0)
                  {{ number_format($value->price_retail, 0, ',', '.') }}
                @else
                  {{ number_format($value->price_retail, 0, '.', ',') }}
                @endif
              </div>
              <div class="" style="color: black;">
                <small>Harga Rumah Sakit : </small>
                {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                @if ($value->price_country == 0)
                  {{ number_format($value->price_gov, 0, ',', '.') }}
                @else
                  {{ number_format($value->price_gov, 0, '.', ',') }}
                @endif
              </div>
              @php
              $time = strtotime($value->price_date);
              $time_release = strtotime($value->release_date);
              $time_expired_date = strtotime($value->expired_date);
              $functionDay =  Fungsi::DayIndonesia();
              $function =  Fungsi::MonthIndonesia();
              @endphp

              <small>Harga : </small>
              {{$functionDay[date('l', $time)]}}, {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}
              <br>
              <small>Terbit : </small>
              {{$functionDay[date('l', $time_release)]}}, {{date('d', $time_release)}} {{$function[date('m', $time_release)]}} {{date('Y', $time_release)}}
              <br>
              <small>Kadaluarsa : </small>
              {{$functionDay[date('l', $time_expired_date)]}}, {{date('d', $time_expired_date)}} {{$function[date('m', $time_expired_date)]}} {{date('Y', $time_expired_date)}}

              <div class="" style="color: {{ $value->stock == 0 ? 'red' : 'green'}};">
                <small>
                  Stok : {{ $value->stock == 0 ? 'Tidak Tersedia' : 'Tersedia'}}
                </small>
              </div>
            {{-- </div> --}}
            {{-- <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 0;"> --}}
              <br>
              @php
                $roles = Auth::user()->roles;
                $id_role = $roles->first()->id;
              @endphp
              <a href="{{ URL::to('/catalog/users/item/detail/'.$value->id) }}" style="margin-bottom:1px;" class="btn btn-success btn-sm btn-round tooltips btn-block" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}">
                <span class="fa fa-info"></span> Detail
              </a>

              @if($id_role == '3')
                @if($value->stock != 0)
                  <button type="button" name="button" style="margin-left:0px;" class="btn btn-primary tooltips BtnCart btn-block btn-sm" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}" onclick="AddToCart(this)">
                    <span class="fa fa-shopping-cart"> </span>
                    tambah
                  </button>
                @endif
              @endif
            </div>
            {{-- <div class="row">
              <div class="col-md-12">
                <div class="col-md-7" style="margin-top:10px;">
                  <a href="{{ URL::to('/catalog/users/item/detail/'.$value->id) }}" style="border-radius: 0px !important;" class="btn btn-success btn-sm btn-round tooltips btn-block" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}">
                    <span class="fa fa-info"></span> Detail
                  </a>
                </div>
                <div class="col-md-5" style="margin-top:10px;">
                  @if($id_role == '3')
                    @if($value->stock != 0)
                      <button type="button" style="border-radius: 0px !important;" name="button" class="btn btn-primary btn-sm btn-round tooltips BtnCart btn-block" data-placement="bottom" data-original-title="cart" data-id="{{$value->id}}" onclick="AddToCart(this)">
                        <span class="fa fa-shopping-cart"></span> Keranjang
                      </button>
                    @endif
                  @endif
                </div>
              </div>
            </div> --}}
            {{-- <div class="pi-price">$29.00</div> --}}
            {{-- <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a> --}}
          </div>
        </div>
      @endforeach
      <div class="col-md-3 col-sm-6 col-xs-12 hide">
          <div class="product-item">
            <div class="pi-img-wrapper">
              @php
              $image_item = DB::table('image_item')
                          ->where('id_item', $value->id)
                          ->groupBy('id_item','id','name','file','created_at','updated_at')
                          ->value('file');
              @endphp
              <img src="/assets/catalog/item/{{ $image_item }}" class="img-responsive" alt="">
              <div>
                <a href="/assets/catalog/item/{{ $image_item }}" class="btn btn-default fancybox-button">Zoom</a>
                {{-- <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a> --}}
              </div>
            </div>
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12 text-center">
                <a href="#" style="text-decoration: none;">
                  {{ $value->name }}
                  <br>
                  {{ $value->code }}
                </a>
              </div>
            </div>
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12">
                <small>Price Retail</small>
                {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                @if ($value->price_country == 0)
                  {{ number_format($value->price_retail, 2, ',', '.') }}
                @else
                  {{ number_format($value->price_retail, 2, '.', ',') }}
                @endif
              </div>
              <div class="col-md-12">
                <small>Price Goverment</small>
                {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                @if ($value->price_country == 0)
                  {{ number_format($value->price_gov, 2, ',', '.') }}
                @else
                  {{ number_format($value->price_gov, 2, '.', ',') }}
                @endif
              </div>
            </div>
            <div class="" style="text-align: center;">
              <a href="javascript:;" class="btn btn-default add2cart" style="float: none !important;">Add to cart</a>
            </div>
            {{-- <div class="pi-price">$29.00</div> --}}
            {{-- <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a> --}}
          </div>
        </div>
    </div>
  @else
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="product-item row"> Tidak Ada Data
    </div>
  </div>
  @endif

  @else

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="product-item row"> Silahkan Pilih Kategory
    </div>
  </div>


  @endif



    <!-- END PRODUCT LIST -->
    <!-- BEGIN PAGINATOR -->

@if($cate!=null)
    <div class="row">
      <div class="col-md-5 col-sm-5">
        <div>menampilkan {{ ($item->currentPage() - 1) * $item->perPage() + 1 }} sampai {{ $item->count() * $item->currentPage() }} dari {{ $item->total() }} data</div>
      </div>
      <div class="col-md-7 col-sm-7 block-paginate">{{$item->appends(['search' => $search, 'category' => $filterByCategory])->links() }}</div>
        {{-- <div class="col-md-7 col-sm-7 block-paginate">{{ $item->links() }}</div> --}}
    </div>
@endif

    <!-- END PAGINATOR -->
  </div>
  <div class="modal"></div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
  <div id="product-pop-up" style="display: none; width: 700px;">
    <div class="product-page product-pop-up">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-3">
          <div class="product-main-image">
            <img src="/su_catalog/assets/frontend/pages/img/products/model7.jpg" alt="Cool green dress with red bell" class="img-responsive">
          </div>
          <div class="product-other-images">
            <a href="javascript:;" class="active"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model3.jpg"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model4.jpg"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="/su_catalog/assets/frontend/pages/img/products/model5.jpg"></a>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-9">
          <h2>Cool green dress with red bell</h2>
          <div class="price-availability-block clearfix">
            <div class="price">
              <strong><span>$</span>47.00</strong>
              <em>$<span>62.00</span></em>
            </div>
            <div class="availability">
              Availability: <strong>In Stock</strong>
            </div>
          </div>
          <div class="description">
            <p>
              Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat Nostrud duis molestie at dolore.
            </p>
          </div>
          <div class="product-page-options">
            <div class="pull-left">
              <label class="control-label">Size:</label>
              <select class="form-control input-sm">
                <option>L</option>
                <option>M</option>
                <option>XL</option>
              </select>
            </div>
            <div class="pull-left">
              <label class="control-label">Color:</label>
              <select class="form-control input-sm">
                <option>Red</option>
                <option>Blue</option>
                <option>Black</option>
              </select>
            </div>
          </div>
          <div class="product-page-cart">
            <div class="product-quantity">
              <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
            </div>
            <button class="btn btn-primary" type="submit">Add to cart</button>
            <a href="shop-item.html" class="btn btn-default">More details</a>
          </div>
        </div>

        <div class="sticker sticker-sale"></div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript">
  function SearchCategory(a) {
    $('#category').attr('value', $(a).attr('data-search'));
    $('#searchByCategory').trigger("click");
  }
  function AddToCart(a) {
    var id = $(a).attr('data-id');
    //alert(id);
    var url = "{{URL::to('catalog/users/addcart')}}";
      $.ajax({
        type: "post",
        url: url,
        data: {
            "_token": "{{ csrf_token() }}",
            "id": id,
            },
          success: function (a) {
            $("#cartItem").html(a.cart+" Items");
            if(a.status == 1){
              $("div#notif").css('display', 'display');
              $("div#notif").removeClass();
              $("div#notif").addClass("alert alert-success");
              $("#notif").fadeTo(2000, 500).slideUp(500, function(){
               $("#notif").slideUp(500);
                });
              $("#status").html("Sukses!");
              $("p#kata_status").html("Item "+a.item_name+" ditambahkan ke keranjang.");
              $('html, body').animate({scrollTop:0}, 'slow');
            }else{
              $("div#notif").removeClass();
              $("div#notif").addClass("alert alert-danger");
              $("#notif").fadeTo(2000, 500).slideUp(500, function(){
               $("#notif").slideUp(500);
                });
              $("#status").html("Gagal!");
              $("p#kata_status").html("Item "+a.item_name+" ditambahkan ke keranjang.");
              $('html, body').animate({scrollTop:0}, 'slow');
            }
          }
      });
  }

  $(document).ready(function(){
    $("div#notif").hide();
    // $("div#notif").css('display', 'none');
  })

  $body = $("body");

  $(document).on({
      ajaxStart: function() { $body.addClass("loading");    },
       ajaxStop: function() { $body.removeClass("loading"); }
  });

  $(document).ready(function(){
    $("#dashboard_nav").removeAttr('href');
    $("#dashboard_nav").attr('href','{{URL::to('catalog/users/')}}');
    $("#cartItem").removeAttr('href');
    $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
    // $("#cart").css('display','none');
    $("#history").removeAttr('href');
    $("#history").attr('href','{{URL::to('catalog/users/history')}}');
    $("#approval").removeAttr('href');
    $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
    $("#tracking").removeAttr('href');
    $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
    $("li#cart").attr("class","active");

  });
</script>
@endsection
