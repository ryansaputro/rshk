@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
@endsection


@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
    RSHK
  </a>
@endsection

@section('content')
  <div class="row">
      <h1>Menu Chatting</h1>
      <div class="col-md-12">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <h3>
        <button type="button" name="button" class="btn btn-primary btn-sm">
          Vendor
        </button>
      </h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <p style="background-color: white; padding: 10px; border: 1px solid grey;">bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <h3 class="pull-right">
        <button type="button" name="button" class="btn btn-primary btn-sm">
          Pembeli
        </button>
      </h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <p style="background-color: white; padding: 10px; border: 1px solid grey; text-align: right;">bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-10">
      <input type="text" name="" value="" class="form-control">
    </div>
    <div class="col-md-2">
      <button type="button" name="button" class="btn btn-primary" style="width: 100%;">
        <i class="fa fa-comments"></i> Send
      </button>
    </div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')

@endsection
