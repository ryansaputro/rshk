<div class="sidebar col-md-4 col-sm-12">
  <h2>Kategori </h2>
<?php
    //jika dia didalem divisi
    if(Auth::user()->roles()->first()->name){

      $id_user = Auth::user()->id;

      $staff = DB::table('user_catalog')
            ->select('*')
            ->where('id_user','=',$id_user)
            ->first();

      $id_dir = $staff->id_user_catalog_supervisi;

      $user_staff = DB::table('user_catalog as m')
            ->join('user_catalog_supervisi as d', 'd.id', '=', 'm.id_user_catalog_supervisi')
            ->select('d.kategory')
            ->where('m.id_user_catalog_supervisi','=',$id_dir)
            ->first();

      $id_cat = $staff->id_kategory;
      $staff->id_kategory;
      $str = $staff->id_kategory;
      $str1 = (explode(",",$str));

      $main = $user_staff->kategory;
      $main1 = (explode(",",$main));

?>

  <form>

  <ul class="list-group margin-bottom-25 sidebar-menu">
    @php
      $category = App\Model\Category::where('id_parent', 0)
      ->where('status', 'Y')
      ->Wherein('id',$main1)
      ->get();

    @endphp

    @foreach ($category as $key => $value)

      <li class="list-group-item clearfix dropdown active">
        <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value->name }}</a>
        <ul class="dropdown-menu" style="display:block;">
          @php
          $id_cat = [];
             $category1 = DB::table('category')
                ->where('id_parent','=',$value->id)
                ->Wherein('id',$str1)
                ->select('*')
                ->get();

          @endphp

          @foreach ($category1 as $key1 => $value1)
            <li class="list-group-item dropdown clearfix ">
              <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value1->name }}</a>
              <ul class="dropdown-menu" style="display:block;" >
                @php
                $category2 = App\Model\Category::where('id_parent', $value1->id)->where('status', 'Y')->get();
                @endphp
                @foreach ($category2 as $key2 => $value2)
                  <li class="list-group-item clearfix">
                    <a href="javascript:void(0);" onclick="SearchCategory(this)" data-search="{{$value2->id}}"><i class="fa fa-archive"></i> {{ $value2->name }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach
  </ul>
  <input type="hidden" name="category" value="" id="category">
  <button type="submit" style="display:none;" id="searchByCategory">oke</button>
  </form>


<?php
  }else{
?>
  <form>

  <ul class="list-group margin-bottom-25 sidebar-menu">
    @php
      $category = App\Model\Category::where('id_parent', 0)->where('status', 'Y')->get();
    @endphp
    @foreach ($category as $key => $value)
      <li class="list-group-item clearfix dropdown active">
        <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value->name }}</a>
        <ul class="dropdown-menu" style="display:block;">
          @php
            $category1 = App\Model\Category::where('id_parent', $value->id)->where('status', 'Y')->get();
          @endphp
          @foreach ($category1 as $key1 => $value1)
            <li class="list-group-item dropdown clearfix ">
              <a href="javascript:void(0);" class=""><i class="fa fa-angle-right"></i> {{ $value1->name }}</a>
              <ul class="dropdown-menu" >
                @php
                $category2 = App\Model\Category::where('id_parent', $value1->id)->where('status', 'Y')->get();
                @endphp
                @foreach ($category2 as $key2 => $value2)
                  <li class="list-group-item clearfix">
                    <a href="javascript:void(0);" onclick="SearchCategory(this)" data-search="{{$value2->id}}"><i class="fa fa-archive"></i> {{ $value2->name }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach
  </ul>
  <input type="hidden" name="category" value="" id="category">
  <button type="submit" style="display:none;" id="searchByCategory">oke</button>
  </form>


  <?php
    }
  ?>
</div>
