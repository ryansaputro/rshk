@php
use App\Model\Manager;
use App\Model\Cart;

@endphp




@if(Auth::check())

@if(Auth::user()->roles()->first()->name == 'divisi')
<div class="top-cart-info keranjang">
  @php
    $data = New Manager;
      $id_user = Auth::user()->id;
      $id_catalog_company = Auth::user()->id_user_catalog_company;
      $roles = Auth::user()->roles->first()->id;
      if($roles == 3){
        $filter = 'id_user';
        $id_user = Auth::user()->id;

        $dataList = DB::table('cart')
              ->where('id_user',$id_user)
              ->where('cart.status',1)
              ->groupBy('cart.id_item','cart.qty','cart.id_user','cart.id','cart.id_user_manager','cart.id_user_supervisi','cart.id_user_finance','cart.id_vendor_detail','cart.status')
              ->select('cart.id_item','cart.qty','cart.id_user','cart.id','cart.id_user_manager','cart.id_user_supervisi','cart.id_user_finance','cart.id_vendor_detail','cart.status')
              ->get();


        $value = Auth::user()->id;
      }else if($roles == 2){
        $filter = 'id_user_finance';
        $id_finance = DB::table('users')->select('user_catalog_finance.id')->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')->where('users.id', $id_user)->value('id');
        $dataList = $data->HirarkiFinance($id_finance)->where('cart.status','1')->get();
      }else if($roles == 7){
        $filter = 'id_user_supervisi';
        $id_supervisi = DB::table('users')->select('user_catalog_supervisi.id')->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')->where('users.id', $id_user)->value('id');
        $dataList = $data->HirarkiSupervisi($id_supervisi)->where('cart.id_user',$id_user)->where('cart.status','1')->groupBy('cart.id_item')->get();
      }else if($roles == 6){
        $filter = 'id_user_manager';
        $id_manager = DB::table('users')->select('user_catalog_manager.id')->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')->where('users.id', $id_user)->value('id');
        $dataList = $data->Hirarki($id_manager)->where('cart.status','1')->groupBy('cart.id_item')->get();
      }else if($roles == 8){
        $filter = 'id_user_directur';
        $id_manager = DB::table('user_catalog_manager')
                        ->select('user_catalog_manager.id')
                        ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                        ->where('user_catalog_directur.id_user', $id_user)
                        ->pluck('id')
                        ->toArray();
        $dataList = $data->HirarkiDirectur($id_manager)->where('cart.status','1')->groupBy('cart.id_item')->get();
      }

      $item = $dataList;
    // $item = App\Model\Cart::where($filter,Auth::user()->id)->groupBy('id_item')->get();
  @endphp
  <a href="{!! route('x.catalog.users.cart') !!}" id="cartItem" data-cart="{{ sizeof($item) }}" class="top-cart-info-count" style="text-decoration: none;">
    {{ sizeof($item) }} Items
  </a>
    <i class="fa fa-shopping-cart"></i>
  {{-- <a href="javascript:void(0);" class="top-cart-info-value">$1260</a> --}}
</div>
@endif
@endif

{{-- <div class="top-cart-content-wrapper">
  <div class="top-cart-content">
    <ul class="scroller" style="height: 250px;">
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
      <li>
        <a href="shop-item.html"><img src="/su_catalog/assets/frontend/pages/img/cart-img.jpg" alt="Rolex Classic Watch" width="37" height="34"></a>
        <span class="cart-content-count">x 1</span>
        <strong><a href="shop-item.html">Rolex Classic Watch</a></strong>
        <em>$1230</em>
        <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
      </li>
    </ul>
    <div class="text-right">
      <a href="shop-shopping-cart.html" class="btn btn-default">View Cart</a>
      <a href="shop-checkout.html" class="btn btn-primary">Checkout</a>
    </div>
  </div>
</div> --}}
