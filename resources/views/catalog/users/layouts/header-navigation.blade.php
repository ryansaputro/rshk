<ul>
  <li id="dashboard_nav"><a id="dashboard_nav" href="{!! route('x.catalog.users') !!}" >Beranda</a></li>
  <li id="cart"><a id="cart" href="{!! route('x.catalog.users.item') !!}" >Produk</a></li>
  <li id="history"><a id="history" href="{!! route('x.catalog.users.history') !!}" >Riwayat</a></li>
  {{-- <li id="approval"><a id="approval" href="{{URL::to('catalog/users/order_approved')}}" >Pesanan Tersetujui</a></li> --}}
  <li id="tracking"><a id="tracking" href="{!! route('x.catalog.users.track') !!}" >Lacak</a></li>
    <!-- <li id="user123"><a id="user123" href="{{URL::to('catalog/users/user')}}" >User</a></li> -->
  {{-- <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Woman
    </a>

    <!-- BEGIN DROPDOWN MENU -->
    <ul class="dropdown-menu">
      <li class="dropdown-submenu">
        <a href="shop-product-list.html">Hi Tops <i class="fa fa-angle-right"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li><a href="shop-product-list.html">Second Level Link</a></li>
          <li class="dropdown-submenu">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
              Second Level Link
              <i class="fa fa-angle-right"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
              <li><a href="shop-product-list.html">Third Level Link</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li><a href="shop-product-list.html">Running Shoes</a></li>
      <li><a href="shop-product-list.html">Jackets and Coats</a></li>
    </ul>
    <!-- END DROPDOWN MENU -->
  </li> --}}
  <!-- BEGIN TOP SEARCH -->
  <li class="menu-search">
    <span class="sep"></span>
    <i class="fa fa-search search-btn"></i>
    <div class="search-box">
      <form>
        <div class="input-group">
          <input type="text" name="search" placeholder="Search" class="form-control">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Pencarian</button>
          </span>
        </div>
      </form>
    </div>
  </li>
  <!-- END TOP SEARCH -->
</ul>
