<div class="sidebar col-md-4 col-sm-12">
  <h2>Kategori {{Auth::user()->roles()->first()->name}}</h2>
<?php
    //jika dia didalem divisi
    if(Auth::user()->roles()->first()->name){

      $id_user = Auth::user()->id;

      if(Auth::user()->roles()->first()->id == '3'){
        $userCatalog = DB::table('user_catalog')->select('user_catalog.id', 'user_catalog.id_kategory')->first();
      }
      // $user = DB::table('users')
      //           ->select('id','name','email','id_direktur_penunjang','id_staff')
      //           ->where('users.id', Auth::user()->id)
      //           ->first();
      //
      //  $id_s = $user->id_staff;
      //
      //   $staff = DB::table('master_staff')
      //           ->select('*')
      //           ->where('id_staff','=',$id_s)
      //           ->first();
      //
      //      $id_dir = $staff->id_direktur_penunjang;
      //
      //    $user_staff = DB::table('master_staff as m')
      //           ->join('master_direktorat_penunjang as d', 'd.id', '=', 'm.id_direktur_penunjang')
      //           ->select('d.kategory')
      //           ->where('m.id_direktur_penunjang','=',$id_dir)
      //           ->first();



             // echo "<pre>";
             // print_r($staff);
             // echo "</pre>";



?>

  <form>

  <ul class="list-group margin-bottom-25 sidebar-menu">
    @php
      $category = App\Model\Category::where('id_parent', 0)
      ->where('status', 'Y')
      ->whereIn('id',[$userCatalog->id_kategory])
      ->get();

      dd($userCatalog->id_kategory);
    @endphp

    @foreach ($category as $key => $value)
      <li class="list-group-item clearfix dropdown active">
        <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value->name }}</a>
        <ul class="dropdown-menu" style="display:block;">
          @php


             $category1 = DB::table('category')
                ->select('*')
                ->whereIn('id',[$userCatalog->id_kategory])
                // ->WhereIn('id',[2000000001,2000000002,2000000004])
                ->get();
                dd($category1);
          @endphp


          <?php
            // echo "<pre>";
            // print_r($category1);
            // echo "</pre>";
          ?>
          @foreach ($category1 as $key1 => $value1)
            <li class="list-group-item dropdown clearfix ">
              <a href="javascript:void(0);" class=""><i class="fa fa-angle-right"></i> {{ $value1->name }}</a>
              <ul class="dropdown-menu" >
                @php
                $category2 = App\Model\Category::where('id_parent', $value1->id)->where('status', 'Y')->get();
                @endphp
                @foreach ($category2 as $key2 => $value2)
                  <li class="list-group-item clearfix">
                    <a href="javascript:void(0);" onclick="SearchCategory(this)" data-search="{{$value2->id}}"><i class="fa fa-archive"></i> {{ $value2->name }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach
  </ul>
  <input type="hidden" name="category" value="" id="category">
  <button type="submit" style="display:none;" id="searchByCategory">oke</button>
  </form>


<?php
  }else{
?>
  <form>

  <ul class="list-group margin-bottom-25 sidebar-menu">
    @php
      $category = App\Model\Category::where('id_parent', 0)->where('status', 'Y')->get();
    @endphp
    @foreach ($category as $key => $value)
      <li class="list-group-item clearfix dropdown active">
        <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $value->name }}</a>
        <ul class="dropdown-menu" style="display:block;">
          @php
            $category1 = App\Model\Category::where('id_parent', $value->id)->where('status', 'Y')->get();
          @endphp
          @foreach ($category1 as $key1 => $value1)
            <li class="list-group-item dropdown clearfix ">
              <a href="javascript:void(0);" class=""><i class="fa fa-angle-right"></i> {{ $value1->name }}</a>
              <ul class="dropdown-menu" >
                @php
                $category2 = App\Model\Category::where('id_parent', $value1->id)->where('status', 'Y')->get();
                @endphp
                @foreach ($category2 as $key2 => $value2)
                  <li class="list-group-item clearfix">
                    <a href="javascript:void(0);" onclick="SearchCategory(this)" data-search="{{$value2->id}}"><i class="fa fa-archive"></i> {{ $value2->name }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach
  </ul>
  <input type="hidden" name="category" value="" id="category">
  <button type="submit" style="display:none;" id="searchByCategory">oke</button>
  </form>


  <?php
    }
  ?>
</div>
