<div class="row">
    <!-- BEGIN TOP BAR LEFT PART -->
    <div class="col-md-6 col-sm-6 additional-shop-info">
        <ul class="list-unstyled list-inline">
            <li><i class="fa fa-phone"></i><span>+62 8521 9378 505</span></li>
            <!-- BEGIN CURRENCIES -->
            <li class="shop-currencies">
                <a href="javascript:void(0);" class="current">Rp</a>
                <a href="javascript:void(0);" class="current">Indonesia</a>
                {{-- <a href="javascript:void(0);">$</a> --}}
                {{-- <a href="javascript:void(0);">€</a> --}}
                {{-- <a href="javascript:void(0);">£</a> --}}
            </li>
            <!-- END CURRENCIES -->
            <!-- BEGIN LANGS -->
            {{-- <li class="langs-block">
                <a href="javascript:void(0);" class="current">Indonesia</a>
                <div class="langs-block-others-wrapper"><div class="langs-block-others">
                  <a href="javascript:void(0);">English</a> --}}
                  {{-- <a href="javascript:void(0);">French</a> --}}
                  {{-- <a href="javascript:void(0);">Germany</a> --}}
                  {{-- <a href="javascript:void(0);">Turkish</a> --}}
                {{-- </div></div>
            </li> --}}
            <!-- END LANGS -->
        </ul>
    </div>
    <!-- END TOP BAR LEFT PART -->
    <!-- BEGIN TOP BAR MENU -->
    <div class="col-md-6 col-sm-6 additional-nav">
        <ul class="list-unstyled list-inline pull-right">
          @if(Auth::check())
            <li>
              <a href="#" style="text-transform: uppercase;" style="">
                {{-- {{ Auth::user()->roles()->first()->name }} --}}
                @if(Auth::user()->roles()->first()->name == 'divisi')
                  Bagian
                @elseif(Auth::user()->roles()->first()->name == 'supervisi')
                  Kepala Instalasi
                @elseif(Auth::user()->roles()->first()->name == 'managercatalog')
                  Direksi
                @elseif(Auth::user()->roles()->first()->name == 'directurcatalog')
                  PPK
                @elseif(Auth::user()->roles()->first()->name == 'finance')
                  Keuangan
                @endif
              </a>
            </li>
            {{-- <li><a href="#">My Wishlist</a></li> --}}
            {{-- <li><a href="#">Checkout</a></li> --}}
            <li class="langs-block" style="text-transform: uppercase;cursor: pointer;">
                {{-- <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> --}}
                {{-- <img alt="" class="img-circle" style="width: 10%;" src="{{asset((Auth::user()->image == '') ? '/su_vms/assets/admin/layout3/img/no_avatar.jpg' : '/su_vms/assets/admin/layout3/img/'.Auth::user()->image)}}"> --}}
                {{ Auth::user()->name }}
                {{-- </a> --}}
                <div class="langs-block-others-wrapper"><div class="langs-block-others">





                  <a href="
                  @if(Auth::user()->roles()->first()->name == 'divisi')
                      {{URL::to('catalog/users/reset')}}
                  @elseif(Auth::user()->roles()->first()->name == 'supervisi')
                      {{URL::to('catalog/users/supervisi/reset')}}
                  @elseif(Auth::user()->roles()->first()->name == 'managercatalog')
                      {{URL::to('/auth')}}
                  @elseif(Auth::user()->roles()->first()->name == 'directurcatalog')
                      {{URL::to('catalog/users/directur/reset')}}
                  @elseif(Auth::user()->roles()->first()->name == 'finance')
                      {{URL::to('catalog/users/finance/reset')}}
                  @endif

                  ">
                  <i class="icon-key"></i>Ganti Password
                  </a>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="icon-key"></i>Keluar
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        </form>

                </div></div>
            </li>
          @else
            <li>
              <a href="{{URL::to('/auth')}}">Login</a>
            </li>
          @endif
            {{-- <li><a href="{!! route('x.catalog.guest.item') !!}">Log Out</a></li> --}}
        </ul>
    </div>
    <!-- END TOP BAR MENU -->
</div>
