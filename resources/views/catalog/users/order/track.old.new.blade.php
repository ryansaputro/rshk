@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/css/tracking.css"/>
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('sidebar')
  {{-- @include('catalog.users.layouts.sidebar') --}}
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Lacak Pesanan</h1>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light">
          <div class="row">
            <form>
              <div class="col-sm-offset-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                  <option value="status">Status</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form>
          </div>
          <div class="portlet-body">
           <table class="table table-striped">
             <thead>
               <tr>
                 <th>No</th>
                 <th>No Usulan</th>
                 <th>No PO</th>
                 <th>No Pesanan</th>
                 <th>Tanggal</th>
                 <th>Nominal</th>
                 <th>Status</th>
                 <th>Aksi</th>
               </tr>
             </thead>
             <tbody>
               @if(count($order) > 0)
               @foreach($order as $k => $v)
                 @php
                 $time = strtotime($v->datetime);
                 $id_proposer = md5($v->id_proposer);
                 $no_po = md5($v->no_po);
                 $month = md5(date('n', $time));
                 $year = md5(date('Y', $time));
                 @endphp
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>
                       <a target="_blank" href="{{URL::to('catalog/users/proposer/download/'.$id_proposer)}}">{{ $v->no_prop }}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</a>
                   </td>
                   <td>
                     @if($v->status >= '3')
                       <a target="_blank" href="{{URL::to('catalog/users/po/download/'.$no_po.'/'.$month.'/'.$year)}}">{{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}<a/>
                     @else
                       {{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}
                     @endif
                   </td>
                   <td>#{{ $v->no_order }}</td>
                   <td>
                     <span style="display: none;">

                     </span>
                     {{$v->datetime}}
                   </td>
                   <td>
                     Rp. {{ number_format($v->total, 0, ',', '.') }}
                   </td>
                   <td>
                     @php
                       $status = $v->status;
                     @endphp
                     <span style="display: none;">{{ $status }}</span>
                     @if ($status == 0)
                       <span class="label bg-grey" style="display: block;">Menunggu Persetujuan</span>
                     @elseif ($status == 1)
                       <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                     @elseif ($status == 2)
                       <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                     @elseif ($status == 3)
                       <span class="label bg-green" style="display: block;">Telah disetujui PPK</span>
                     @elseif ($status == 4)
                       <span class="label bg-purple" style="display: block;">Barang Dikirim</span>
                     @elseif ($status == 5)
                       <span class="label bg-grey" style="display: block;">Barang Terkirim</span>
                     @elseif ($status == 6)
                       <span class="label bg-yellow" style="display: block;">Barang diterima oleh Gudang</span>
                   @elseif ($status == 7)
                       <span class="label bg-danger" style="display: block;">Barang Bermasalah</span>
                     @endif
                   </td>
                   <td>
                     <a style="float:left !important; color:white;" href="#" onclick="DetailCart(this)" class='btn btn-primary btn-xs' data-id="{{$v->id}}">Detail</a>
                     <a style="float:left !important; color:white;" href="#" onclick="TrackCart(this)" class='btn btn-success btn-xs' data-id="{{$v->id}}">Lacak</a>
                   </td>
                 </tr>
               @endforeach
               @else
                 <tr>
                   <td colspan="8" style="text-align:center;">Tidak Ada Data</td>
                 </tr>
               @endif
             </tbody>
           </table>
           <!-- BEGIN PAGINATOR -->
           <div class="row">
             <div class="col-md-5 col-sm-5">
               <div>menampilkan {{ ($order->currentPage() - 1) * $order->perPage() + 1 }} sampai {{ $order->count() * $order->currentPage() }} dari {{ $order->total() }} data</div>
             </div>
               <div class="col-md-7 col-sm-7 block-paginate">{{ $order->links() }}</div>
           </div>
           <!-- END PAGINATOR -->

            <div class="card">
             <div class="card-body">
                <div class="table-wrapper-responsive hide" id="divDetailCart" style="padding-top:15px;">
                   <h1><center>Detail Pesanan</center></h1>
                   <div class="row">
                     <div class="col-xs-6">
                       <address>
                       <strong>Pembeli: </strong><br>
                         <span id="name_buyer">Nama : xx</span><br>
                         <span id="telephone_buyer">Telp: xx</span><br>
                         <span id="email_buyer">Email: xx</span><br>
                       </address>
                     </div>
                     <div class="col-xs-6 text-right">
                       <address>
                         <strong>Pesanan:</strong><br>
                         <span id="no_order">No Pesanan : yy</span><br>
                         <span id="date_order">Tanggal : yy</span><br>
                       </address>
                     </div>
                   </div>
                   <div class="table-wrapper-responsive show">
                     <table summary="Shopping cart" id="detailCart" class="table table-bordered table striped">
                       <thead>
                         <tr>
                           <th>No</th>
                           <th>Item</th>
                           <th>Qty</th>
                           <th>Harga</th>
                           <th>Biaya Kirim</th>
                           <th>Total</th>
                         </tr>
                       </thead>
                       <tbody>
                       </tbody>
                     </table>
                   </div>
                   {{-- <a href="{{URL::to('catalog/users/proposer/download/'.$)}}" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Unduh Usulan</a> --}}
                </div>
              </div>
            </div>

            <div class="card">
             <div class="card-body">
                <div class="table-wrapper-responsive hide" id="divTrackCart" style="margin-top:60px;">
                    <h1><center>Lacak Pesanan</center></h1>
                    {{-- <div class="stepwizard" style="margin-bottom:20px;">
                      <div class="stepwizard-row setup-panel">
                        @for($i=0; $i<=4; $i++)
                        @if ($i == 0)
                        @php $status = "Menunggu Persetujuan"; @endphp
                        @elseif ($i == 1)
                        @php $status = "Telah disetujui Kepala Instalasi"; @endphp
                        @elseif ($i == 2)
                        @php $status = "Telah disetujui Direksi"; @endphp
                        @elseif ($i == 3)
                        @php $status = "Telah disetujui PPK"; @endphp
                        @elseif ($i == 4)
                        @php $status = "Telah disetujui Finance"; @endphp
                        @endif
                        <div class="stepwizard-step">
                        <a href="#" type="button" class="btn btn-danger btn-circle status_{{$i}} status"><i class="fa fa-close" aria-hidden="true"></i></a>
                        <p>{{$status}}</p>
                        </div>
                        @endfor
                      </div>
                    </div> --}}
                    {{-- <div class="table-wrapper-responsive show">
                      <table border="1px" id="detailStatus" style="width:100%;">
                        <thead>
                          <tr>
                            <th rowspan="2">No</th>
                            <th style="text-align:center;" colspan="2">Bagian</th>
                            <th style="text-align:center;" colspan="2">Kepala Instalasi</th>
                            <th style="text-align:center;" colspan="2">Direksi</th>
                            <th style="text-align:center;" colspan="2">PPK</th>
                            <th style="text-align:center;" colspan="2">Finance</th>
                          </tr>
                          <tr>
                            @for($i=1; $i<=5; $i++)
                              <th>Detail</th>
                              <th>Transaksi</th>
                            @endfor
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div> --}}
                    <div class="container">
                        <div class="row">
                                <div class="board">
                                  <div class="board-inner">
                                    <ul class="nav nav-tabs" id="myTab">
                                      <div class=""></div>
                                      <div class="liner"></div>
                                      <div class="linerA"></div>
                                         <li id="li_satu">
                                           <a href="#0" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Pengusul" class="menu pengusul" data-id="0">
                                            <span class="round-tabs one" id="0">
                                                    <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                                    <div style="size:12px; margin-top:30px !important;">Pengusul</div>
                                            </span>
                                            </a>
                                        </li>

                                          <li><a href="#1" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Kepala Bagian" class="menu kepala_bagian" data-id="1">
                                             <span class="round-tabs four" id="1">
                                               <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Kepala Bagian</div>
                                             </span>
                                            </a>
                                         </li>

                                          {{-- <li><a href="#2" data-toggle="tab" title="Direktur" class="menu" data-id="2">
                                             <span class="round-tabs four" id="2">
                                               <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                               <div style="size:12px;">Direktur</div>
                                             </span>
                                            </a>
                                         </li> --}}

                                          <li><a href="#3" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="PPK" class="menu ppk" data-id="3">
                                             <span class="round-tabs four" id="3">
                                               <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">PPK</div>
                                             </span>
                                            </a>
                                         </li>

                                          <li><a href="#4" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Dikirim" class="menu dikirim" data-id="4">
                                             <span class="round-tabs four" id="4">
                                               <i class="fa fa-truck fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Dikirim</div>
                                             </span>
                                            </a>
                                         </li>

                                          <li><a href="#5" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Diterima" class="menu diterima" data-id="5">
                                             <span class="round-tabs four" id="5">
                                               <i class="fa fa-gift fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Diterima</div>
                                             </span>
                                            </a>
                                         </li>

                                          <li><a href="#5" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Diterima" class="menu gudang" data-id="6">
                                             <span class="round-tabs four" id="6">
                                               <i class="fa fa-home fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px;  margin-top:30px !important;">Gudang</div>
                                             </span>
                                            </a>
                                         </li>

                                          <li><a href="#6" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Finance" class="menu" data-id="6">
                                             <span class="round-tabs four" id="6">
                                               <i class="fa fa-money fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Menunggu Pembayaran</div>
                                             </span>
                                            </a>
                                         </li>

                                          <li><a href="#7" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Selesai" class="menu" data-id="7">
                                             <span class="round-tabs four" id="7">
                                               <i class="fa fa-dollar fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Pembayaran Sukses</div>
                                             </span>
                                            </a>
                                         </li>
                                          <li><a href="#8" style="background:#64aed900 !important;" data-toggle="modal" data-target="#myModal" title="Selesai" class="menu" data-id="7">
                                             <span class="round-tabs four" id="8">
                                               <i class="fa fa-check fa-3x" aria-hidden="true" style="padding-top:15px;"></i>
                                               <div style="size:12px; margin-top:30px !important;">Selesai</div>
                                             </span>
                                            </a>
                                         </li>
                                       </ul>
                                     </div>

                                     <!-- Modal -->
                                      <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                     <!-- Modal content-->
                                           <div class="modal-content">
                                             <div class="modal-header">
                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               <h4 class="modal-title">Rincian Persetujuan</h4>
                                             </div>
                                             <div class="modal-body">
                                               <div class="tab-content" id="isi">
                                                 @for ($i = 0; $i < 8; $i++)
                                                   <div class="tab-pane fade in {{($i == '0') ? 'active' : ''}}" id="{{$i}}">
                                                     <h3 class="head text-center">Rincian Persetujuan</h3>
                                                     <div class="narrow text-center" id="deskripsi">
                                                       <table class="table table-striped" id="user">
                                                         <thead>
                                                           <tr>
                                                             <th width="50%" style="text-align:center;">Nama</th>
                                                             <th width="50%" style="text-align:center;">Tanggal Transaksi</th>
                                                           </tr>
                                                         </thead>
                                                         <tbody>
                                                           <tr>
                                                             <td id="name"></td>
                                                             <td id="date"></td>
                                                           </tr>
                                                         </tbody>
                                                       </table>
                                                     </div>
                                                  </div>
                                                 @endfor
                                              </div>
                                             </div>
                                             <div class="modal-footer">
                                               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                           </div>

                                         </div>
                                       </div>

                                </div>
                              </div>
                            </div>

                </div>
              </div>
            </div>
          </div> <!--.\portlet-body-->
      </div><!--.\col-md-12-->
    </div><!--.\row shop-tracking-status-->
  </div><!--.\col-md-12 col-sm-12-->
  <div class="modal"></div>
@endsection

@section('note')

@endsection

@section('js')
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@endsection

@section('xjs')
  <script type="text/javascript">
  $('.filter').on('change', function(){
    var filter = $(this).val();
    var select = '<select class="form-control input-sm" name="search" class="filter">'+
                      '<option value="5">Semua</option>'+
                      '<option value="0">Menunggu Persetujuan</option>'+
                      '<option value="1">Telah disetujui Kepala Instalasi</option>'+
                      '<option value="2">Telah disetujui Direksi</option>'+
                      '<option value="3">Telah disetujui PPK</option>'+
                      '<option value="4">Telah disetujui Finance</option>'+
                  '</select>'

    if(filter == 'status'){
      $('div.search').html(select)
    }else if(filter == 'tanggal'){
      $('div.search').html('<input type="date" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
    }else{
      $('div.search').html('<input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
    }
  });
    // $(function() {
    //   $('table').dataTable({
    //     "order": [[ 3, "asc" ]]
    //   });
    // });
    function TrackCart(a) {
      for (var i = 1; i < 8 ; i++) {
        $("span#"+i).removeClass('one');
        $("span#"+i).addClass('four');
      }
      $("ul#myTab li").removeClass('active')
      // $("ul#myTab li#li_satu").attr('class', 'active')
      $("#divTrackCart").removeClass('hide');
      $("#divDetailCart").removeClass('show');
      $("#divDetailCart").addClass('hide');
      $("#divTrackCart").addClass('show');
      var id_order = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/track_order')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_order": id_order,
                  },
                success: function (a) {

                  // dt = "";
                  // $('.pengusul').on('click', function(){
                    // dt +=
                    // // '<table class="table table-striped" id="user">'+
                    //         '<thead>'+
                    //           '<tr>'+
                    //             '<th width="50%" style="text-align:center;">Nama</th>'+
                    //             '<th width="50%" style="text-align:center;">Tanggal Transaksi</th>'+
                    //           '</tr>'+
                    //         '</thead>';
                  //         //   '<tbody>'+
                  //         //     '<tr>'+
                  //         //       '<td id="name"></td>'+
                  //         //       '<td id="date"></td>'+
                  //         //     '</tr>'+
                  //         //   '</tbody>'+
                  //         // '</table>';
                  //
                  //     $('#user thead').html(dt)
                  // })

                  if(a.status == 1){
                    var numberStatus = 0;
                    $.each(a.data, function(k,v){
                      html =
                      // '<table class="table table-striped" id="user">'+
                              '<thead>'+
                                '<tr>'+
                                  '<th width="50%" style="text-align:center;">Nama</th>'+
                                  '<th width="50%" style="text-align:center;">Tanggal Transaksi</th>'+
                                '</tr>'+
                              '</thead>';
                      $('#user thead').html(html)
                      console.log(html);
                      numberStatus = v.status_pesan;
                      if(v.username_catalog == null){
                         ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                         b = v.created_at;
                       }else{
                         b = v.created_at;
                         ab = v.username_catalog+"<br>"+v.mobile_user+"<br>"+v.email_user;
                       }
                      $("td#name").html(ab)
                      $("td#date").html(b)
                    })

                    for (var i = 0; i <= numberStatus ; i++) {
                      $("span#"+i).removeClass('four');
                      $("span#"+i).addClass('one');
                    }

                    $('a.menu').on('click', function(){
                      statusdata = $(this).attr('data-id');
                      $.each(a.data, function(k,v){
                        mng_phone = (v.mobile_phone_manager != null) ? v.mobile_phone_manager : '-';
                        mng_email = (v.email_manager != null) ? v.email_manager : '-';

                        fnc_email = (v.email_finance != null) ? v.email_finance : '-';
                        fnc_phone = (v.mobile_phone_finance != null) ? v.mobile_phone_finance : '-';

                        if(statusdata == 0){
                          // if(v.username_catalog == null){
                          //    ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                          //    b = v.created_at;
                          //  }else{
                          //    b = v.created_at;
                          //    ab = v.username_catalog+"<br>"+v.mobile_user+"<br>"+v.email_user;
                          //  }
                          $("td#name").html(ab)
                          $("td#date").html(b)

                        }
                        else if(statusdata == 1){
                            if(v.datetime_supervisi == '0000-00-00 00:00:00'){
                              b = "-";
                              ab = "-";
                            }else{
                              b = (v.datetime_supervisi != '0000-00-00 00:00:00') ? v.datetime_supervisi : '-';
                              ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                            }
                          alert("baca ini")
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 2){
                          ab = (v.name_manager != null) ? v.name_manager : '-<br>'+mng_email+"<br>"+mng_phone;
                          b = (v.datetime_manager != '0000-00-00 00:00:00') ? v.datetime_manager : '-'

                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 3){
                          ab = (v.name_directur != null) ? v.name_directur : '-';
                          b = (v.datetime_directur != '0000-00-00 00:00:00') ? v.datetime_directur : '-';
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 4){
                          ab = (v.driver != null) ? v.driver+"<br>"+v.car+"<br>"+v.car_no : '-<br>';
                          b = (v.date_do != '0000-00-00 00:00:00') ? v.date_do : '-';
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 5){
                          if(v.date_receive == null){
                            b = "-";
                            ab = "-";
                          }else{
                            b = (v.date_receive != null) ? v.date_receive : '-';
                            ab = (a.users.name != null) ? a.users.name : '-';
                          }


                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else{
                          // dt += '<table class="table table-striped" id="user">'+
                          //         '<thead>'+
                          //           '<tr>'+
                          //             '<th width="50%" style="text-align:center;">Nama</th>'+
                          //             '<th width="50%" style="text-align:center;">Tanggal Transaksi</th>'+
                          //           '</tr>'+
                          //         '</thead>'+
                          //         '<tbody>'+
                          //           '<tr>'+
                          //             '<td id="name"></td>'+
                          //             '<td id="date"></td>'+
                          //           '</tr>'+
                          //         '</tbody>'+
                          //       '</table>';
                          //
                          // $('#deskripsi').html(dt)
                          $("td#name").html('-')
                          $("td#date").html('-')

                        }

                      })
                    })

                    $('.gudang').on('click', function(){
                      // $('#deskripsi').html("")
                      dt += '<table class="table table-striped" id="gudang">'+
                        '<thead>'+
                          '<tr>'+
                            '<th style="text-align:left;">No</th>'+
                            '<th style="text-align:left;">Nama</th>'+
                            '<th style="text-align:left;">Qty</th>'+
                            '<th style="text-align:left;">Satuan</th>'+
                            '<th style="text-align:left;">Tanggal Transaksi</th>'+
                            '<th style="text-align:left;">Petugas</th>'+
                          '</tr>'+
                        '</thead>';

                      dt += '<tbody>';
                        $.each(a.gudang, function(a, b){
                          dt += '<tr>';
                          dt += '<td id="name">'+parseInt(a+1)+'</td>';
                          dt += '<td id="name">'+b.code+'<br>'+b.merk+'<br>'+b.name+'</td>';
                          dt += '<td id="name">'+b.qty_kirim+'</td>';
                          dt += '<td id="name">'+b.unit_name+'</td>';
                          dt += '<td id="name">'+b.datetime+'</td>';
                          dt += '<td id="name">'+b.username+'</td>';
                          dt += '<td id="date"></td>';
                          dt += '</tr>';
                          console.log(b.code);
                        });
                      dt += '</tbody>'+
                          '</table>';
                      $('#user').html(dt)
                    });

                  }else{
                    alert("Data Error!")
                  }
                }

          });
      $("html, body").animate({ scrollTop: $('#divTrackCart').prop("scrollHeight")}, 500)
    }

    function DetailCart(a) {
      $("#divTrackCart").removeClass('show');
      $("#divTrackCart").addClass('hide');
      var id_order = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/detail_order')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_order": id_order,
                  },
                success: function (a) {
                  $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
                  $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
                  $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
                  $("#no_order").html("<b>No Pesanan :</b> #"+a.dataItem.no_order);
                  $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
                  var table = ""
                  var subtot = 0;
                  var totQty = 0;
                  var totAll = 0;
                  $.each(a.data, function(k, v){
                    totQty += v.qty;
                    totAll += ((v.price_gov*v.qty)+parseInt(v.price_shipment));
                    table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                    table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                    table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'">'+v.name+'</td>';
                    table += '<td style="text-align:right;">';
                        table += '<strong>';
                        table += '<span>'+addCommas(v.qty);
                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td style="text-align:right;">';
                        table += '<strong>';
                        table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                        table += '<span>Rp. '+addCommas(v.price_gov);
                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td style="text-align:right;">  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_shipment+'" class="total total_'+v.id_item+'">';
                          table += '<strong>';
                          table += '<span data-value="'+v.price_shipment+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas(v.price_shipment)+'</span>';
                          table += '</strong>';
                    table += '</td>';
                    table += '<td style="text-align:right;">  <input type="hidden" name="total['+v.id_item+']" value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total total_'+v.id_item+'">';
                          table += '<strong>';
                          table += '<span data-value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas(((v.price_gov*v.qty)+parseInt(v.price_shipment)))+'</span>';
                          table += '</strong>';
                    table += '</td>';
                    table += "</tr>";
                    sub = v.price_gov*v.qty;
                    subtot += sub;
                  });
                    table += '<td colspan="2" style="text-align:right;">';
                      table += "<strong>Total</strong>";
                    table += '</td>';
                    table += '<td style="text-align:right;">';
                      table += "<strong>"+addCommas(totQty)+"</strong>";
                    table += '</td>';
                    table += '<td colspan="3" style="text-align:right;">';
                    table += "<strong>Rp. "+addCommas(totAll)+"</strong>";
                    table += '</td>';

                  $("#detailCart tbody").html(table);
                  $(".Modal span#subtot").html(addCommas(subtot));
                  $("#divDetailCart").addClass('show');
                  $("html, body").animate({ scrollTop: $("#divDetailCart").prop("scrollHeight")}, 500)
                }
        })
      $("#divDetailCart").css('display','');
    }


    $(document).ready(function(){
      $("#dashboard_nav").removeAttr('href');
      $("#dashboard_nav").attr('href','{{URL::to('catalog/users')}}');
      $("#cartItem").removeAttr('href');
      $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
      // $("#cart").css('display','none');
      $("#cart").css('display','block');
      $("#history").removeAttr('href');
      $("#history").attr('href','{{URL::to('catalog/users/history')}}');
      $("#approval").removeAttr('href');
      $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
      $("#tracking").removeAttr('href');
      $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
      $("li#tracking").attr("class","active");
    });


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }</script>
@endsection
