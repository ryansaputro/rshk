@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
    RSHK
  </a>
@endsection

@section('sidebar')
  {{-- @include('catalog.users.layouts.sidebar') --}}
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption caption-md">
              <i class="icon-bar-chart theme-font hide"></i>
              <span class="caption-subject theme-font bold uppercase">RSHK</span>
              <span class="caption-helper">Order History...</span>
            </div>
            <div class="actions">
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-bordered table striped">
              <thead>
                <tr>
                  <th>No Order</th>
                  <th>Date</th>
                  <th>Nominal</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @for ($i=1; $i <= 10; $i++)
                  <tr>
                    <td>ORDERCODE{{ rand(1, 100) }}</td>
                    <td>
                      @php
                        $timestamp = mt_rand(1, time());
                        $realdate = date('Ymd', $timestamp);
                        $date = date('l, d F Y', $timestamp);
                      @endphp
                      <span style="display: none;">
                        {{ $realdate }}
                      </span>
                      {{ $date }}
                    </td>
                    <td>
                      Rp. {{ number_format(rand(10000000, 10000000000), 2, ',', '.') }}
                    </td>
                    <td>
                      @php
                        $status = rand(0, 5);
                      @endphp
                      <span style="display: none;">{{ $status }}</span>
                      @if ($status == 0)
                        <span class="label bg-grey" style="display: block;">no status</span>
                      @elseif ($status == 1)
                        <span class="label bg-blue" style="display: block;">approve Kepala Bagian</span>
                      @elseif ($status == 2)
                        <span class="label bg-blue" style="display: block;">approve Kepala Bagian</span>
                      @elseif ($status == 3)
                        <span class="label bg-green" style="display: block;">approve PPK</span>
                      @elseif ($status == 4)
                        <span class="label bg-purple" style="display: block;">success payment</span>
                      @else
                        <span class="label bg-red" style="display: block;">reject order</span>
                      @endif
                    </td>
                    <td>
                      <button type="button" name="button" class="btn btn-xs btn-primary" style="width: 100%;" >
                        <span class="fa fa-eye"></span>
                      </button>
                    </td>
                  </tr>
                @endfor
              </tbody>
            </table>
          </div><!--/.portlet-body--->
        </div>
      </div>
    </div>
  </div>
@endsection

@section('note')

@endsection

@section('js')
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@endsection

@section('xjs')
  <script type="text/javascript">
    $(function() {
      $('table').dataTable({
        "order": [[ 3, "asc" ]]
      });
    });

    $(document).ready(function(){
      $("#dashboard_nav").removeAttr('href');
      $("#dashboard_nav").attr('href','{{URL::to('catalog/users/')}}');
      $("#cartItem").removeAttr('href');
      $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
      // $("#cart").css('display','none');
      $("#history").removeAttr('href');
      $("#history").attr('href','{{URL::to('catalog/users/history')}}');
      $("#approval").removeAttr('href');
      $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
      $("#tracking").removeAttr('href');
      $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
    });
    </script>
@endsection
