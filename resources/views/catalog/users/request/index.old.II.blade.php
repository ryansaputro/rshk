@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Permintaan Barang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('vms/users/submitDo')}}" method="post">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="no_po" class="col-md-4 control-label">No Po</label>
                <div class="col-md-8">
                  @php
                  $time = strtotime($po->datetime);
                  @endphp
                  <input type="hidden" name="id_po" value="{{$po->id_order}}">
                  <input type="hidden" name="rs" value="Rumah Sakit Jantung Harapan Kita">
                  <input style="margin-bottom:10px;" id="no_po"  readonly type="text" class="form-control input-sm" name="no_po" value="{{$po->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}" required autofocus>
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <label for="destination" class="col-md-4 control-label">Tujuan</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="destination" type="text" class="form-control input-sm" name="rs" value="Rumah Sakit Jantung Harapan Kita" disabled required>
                </div>
              </div>

              <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <textarea name="address" style="margin-bottom:10px; height:100px;" class="form-control input-sm" readonly> Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420</textarea>
                </div>
              </div>

              <div class="form-group{{ $errors->has('pengirimanBy') ? ' has-error' : '' }}">
                <label for="pengirimanBy" class="col-md-4 control-label">Pengiriman Oleh</label>
                <div class="col-md-8">
                  <select class="form-control input-sm pengirimanBy" required style="margin-bottom:10px;"  name="sent_by">
                    {{-- <option disabled selected>-pilih-</option> --}}
                    <option value="1">Pribadi</option>
                    <option value="0">Kurir</option>
                  </select>
                  @if ($errors->has('pengirimanBy'))
                    <span class="help-block">
                      <strong>{{ $errors->first('pengirimanBy') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="pengiriman">
                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">Sopir</label>
                  <div class="col-md-8">
                    <input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">Mobil</label>
                  <div class="col-md-8">
                    <input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ?  'has-error' : '' }}">
                  <label for="post_code" class="col-md-4 control-label">No. Polisi</label>
                  <div class="col-md-8">
                    <input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                    @if ($errors->has('post_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('post_code') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>              </div>

              </div>

            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="item" class="col-md-4 control-label">Nama Item</label>
                <div class="col-md-8">
                  {{-- <input style="margin-bottom:10px;" list="item" oninput="item(this)" class="form-control input-sm"> --}}
                  {{-- <button type="button" class="btn btn-info btn-sm btn-block" style="margin-bottom:10px;" data-toggle="modal" data-target="#myModal">Pilih Item</button> --}}
                  <select class="form-control input-sm" style="margin-bottom:10px;" id="item">
                    <option disabled selected> - pilih -</option>
                    @foreach ($dataItem as $key => $value)
                      <option data-id="{{$value->id}}" value="{{$value->id_item}}">{{$value->name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('item'))
                    <span class="help-block">
                      <strong>{{ $errors->first('item') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="qty" class="col-md-4 control-label">Jumlah Item</label>
                <div class="col-md-6">
                  <input style="margin-bottom:10px;" id="qty"  type="number" class="form-control  input-sm" name="qty" value="{{ old('qty') }}" required autofocus>
                  @if ($errors->has('qty'))
                    <span class="help-block">
                      <strong>{{ $errors->first('qty') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="col-md-2">
                  <select class="form-control input-sm" style="margin-bottom:10px;" id="satuan" name="satuan">
                    @foreach ($satuan as $k => $v)
                      <option value="{{$v->id}}" {{$v->id == '2' ? 'selected' : ''}} data-convert="{{$v->convert_pcs}}">{{$v->unit_name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label"></label>
                <div class="col-md-8">
                  <button type="button" name="button" onclick="Convert(this)" class="btn btn-primary btn-block btn-xs" style="margin-bottom:10px;">Konversi</button>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} after_convert " style="display:'';">
                <label for="post_code" class="col-md-4 control-label">Hasil</label>
                <div class="col-md-2 hasil">
                  <input type="text" name="" value="" class="form-control input-sm hasil" style="margin-bottom:10px;">
                </div>
                <div class="col-md-2 satuan">
                  <input type="text" name="" value="BOX" class="form-control input-sm satuan" style="margin-bottom:10px;">
                </div>
                <div class="col-md-2 sisa_hasil">
                  <input type="text" name="" value="" class="form-control input-sm sisa_hasil" style="margin-bottom:10px;">
                </div>
                <div class="col-md-2 sisa_satuan">
                  <input type="text" name="" value="PCS" class="form-control input-sm sisa_satuan" style="margin-bottom:10px;">
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="keterangan"  type="text" class="form-control  input-sm" name="keterangan" value="{{ old('post_code') }}" autofocus>
                  @if ($errors->has('keterangan'))
                    <span class="help-block">
                      <strong>{{ $errors->first('keterangan') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-offset-10 col-sm-2">
                  <button type="button" class="btn btn-sm btn-primary btn-block" onclick="AddItem(this)" name="button">Oke</button>
                </div>
              </div>
            </div>

          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemKirim">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <div class="col-md-offset-10 col-sm-2">
                <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button">Submit</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>

    <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
              <div style="overflow-x:auto;">
                <table class="table table-bordered">
                  <tr>
                    <th>Pilih</th>
                    <th>Item</th>
                    <th>Deskripsi</th>
                  </tr>
                  @foreach ($dataItem as $key => $value)
                    <tr>
                      <td> <input type="checkbox" class="form-control input-sm" name="pilih" value=""> </td>
                      <td> {{$value->name}} </td>
                      <td> {{$value->description}} </td>
                    </tr>
                    {{-- <option data-id="{{$value->id}}" value="{{$value->id_item}}">{{$value->name}}</option> --}}
                  @endforeach
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    @endsection
@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var satuan_kirim = "";
var id_satuan_kirim = "";
function Convert(a) {
  $('input.sisa_hasil').val(0)
  $('input.sisa_satuan').val("PCS")
  $('input.hasil').val(0)
  $('input.satuan').val("-")
  var qty_per_pcs = $('#qty').val();
  var convert_pcs_to = $('#satuan').find(':selected').attr('data-convert');
  satuan_kirim = $('#satuan').find(':selected').text();
  id_satuan_kirim = $('#satuan').find(':selected').val();
  sisa = qty_per_pcs%convert_pcs_to;
  nilai = Math.floor(qty_per_pcs/convert_pcs_to);
  $('.after_convert').css('display', 'inline');
  if(sisa != 0){
    $('input.sisa_hasil').val(sisa)
    $('input.sisa_satuan').val("PCS")
  }
  $('input.hasil').val(nilai)
  $('input.satuan').val($('#satuan').find(':selected').text())
  $('#satuan option').prop('selected', function() {
       return this.defaultSelected;
   });

}


$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
      $( ".sisa_hasil" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
});


$('.pengirimanBy').on('change', function(){
  $('.pengirimanBy option').each(function(k,v){
    if($(v).is(':selected')){
      if($(this).val() == 0){
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Nama Kurir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No Resi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }else{
        var data = '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Sopir</label>'+
          '<div class="col-md-8">'+
            '<input type="text" style="margin-bottom:10px;" name="driver" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">Mobil</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>'+

        '<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">'+
          '<label for="post_code" class="col-md-4 control-label">No. Polisi</label>'+
          '<div class="col-md-8">'+
            '<input type="text" name="car_no" style="margin-bottom:10px;" class="form-control input-sm" value="" required>'+
            '@if ($errors->has('post_code'))'+
              '<span class="help-block">'+
                '<strong>{{ $errors->first('post_code') }}</strong>'+
              '</span>'+
            '@endif'+
          '</div>'+
        '</div>';
      }
    }
    $('.pengiriman').css('display','inline');
    $('.pengiriman').css('margin-top','30px');
    $('.pengiriman').html(data);
  });

});

$(document).ready(function(){
  // $('.pengiriman').hide();

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

});

var name = "";
var id = "";
$('#item').on('change', function(){
  id = $('#item :selected').attr('data-id');
  id_item = $(this).val();
  name =$('#item :selected').text();
  $.ajax({
      url: '{{URL::to('vms/users/detailItem')}}',
      method: 'POST',
      data: {"_token": "{{ csrf_token() }}", "id": id, "id_item" : id_item},
      success: function (a) {
        $('#qty').attr('value', a.qty);
        if(a.qty > 0){
          $('#qty').attr('max', a.qty);
          $('#qty').attr('min', 1);
        }else{
          $('#qty').attr('max', 0);
          $('#qty').attr('min', 0);

        }
      }
  });
});
var arr = [];
function AddItem(a) {
  if(name !== ''){
    if(satuan_kirim == ''){
      satuan_kirim = $('#satuan').find(':selected').text();
      id_satuan_kirim = $('#satuan').find(':selected').val();
    }
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"PCS<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    // "<td>"+$('#satuan :selected').text()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan :selected').val()+"'></td>"+
    "<td>"+satuan_kirim+"<input type='hidden' name='satuanDO[]' value='"+id_satuan_kirim+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }

  // $("#qty").val(0)
  $(".hasil").val(0)
  $(".satuan").val("")
  $(".sisa_hasil").val(0)
  $("#keterangan").val("")
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}

</script>
@endsection
