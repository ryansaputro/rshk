@extends('catalog.users.layouts.app_doc')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection


@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
  <div class="page-head">
    <div class="container-fluid">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Catalog <small class="uppercase">Purchase Order Form...</small></h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <!-- BEGIN THEME PANEL -->
        {{-- <div class="btn-group btn-theme-panel">
          <a href="javascript:;" class="btn">
            <i class="icon-plus"></i>
          </a>
        </div> --}}
        <!-- END THEME PANEL -->
      </div>
      <!-- END PAGE TOOLBAR -->
    </div>
  </div>

@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        {{-- <div class="portlet-title">
          <div class="col-xs-6 caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Purchase Order ...
            </span>
          </div>
          <div class="col-xs-6" style="text-align:right;">
            <a  href="#" class="btn btn-primary btn-sm" onclick='printDiv();'>Print</a>
          </div>
          <br>
        </div><!--/.portlet-title---> --}}
        <div class="portlet-body" id='DivIdToPrint'>
          <div class="row" style="font-weight:100; color:#000;">
            <form class="" action="{{URL::to('catalog/users/request/'.md5($data->id))}}" method="post">
              {{ csrf_field() }}

            <center>
              <img style="width:15%;" class="pull-left" src="/assets/kemenkes.jpg" alt="" style="width:200px;">
              <img style="width:15%;" class="pull-right" src="https://2.bp.blogspot.com/--flp8oUCSiY/Wnb9lKct-7I/AAAAAAAAI14/QFXnonW67vktZpovpdj3Fk6NIUmRlRU9wCLcBGAs/s1600/Penerimaan%2BBesar%2BBesaran%2BPegawai%2BRS%2BJantung%2B%2526%2BPembuluh%2BDarah%2BHarapan%2BKita.gif" alt="" style="width:200px;">

              <strong>KEMENTRIAN KESEHATAN</strong><br>
              <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong><br>
              <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong><br>
              Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420 <br>
              Telepon 021.5684085 - 093, 5681111, Faksimile 5684230 <br>
              Surat Elektronik : info@pjnhk.go.id <br>
              <a href="http://www.pjnhk.go.id" style="padding-right:175px;">http://www.pjnhk.go.id</a><br>

            </center>

            <hr>
            @php
              $time = strtotime($data->datetime);
              $function =  Fungsi::MonthIndonesia();
              $functionDay =  Fungsi::DayIndonesia();
              $list_propinsi  =  Fungsi::propinsi();
              $list_kota  =  Fungsi::IndonesiaProvince();
              $times = $time + strtotime("+2 months");
              $aritdate =  date('d m Y', strtotime($data->datetime. "+12 days"));
              $NextDate = explode(' ', $aritdate);
            @endphp



           <center><b>SURAT PERMINTAAN</b></center>
            <br>
            <div class="col-md-12">
              <table class="table table-bordered">
                <tr>
                  <td>Nama </td>
                  <td width="1px">:</td>
                  <td>{{$vendorDetail->vendor_name}}</td>

                  <td>NO Kontrak </td>
                  <td>:</td>
                  <td> :{{$contract->no_contract}}</td>
                </tr>
                <tr>
                  <td rowspan="2">Alamat </td>
                  <td width="1px"  rowspan="2">:</td>
                  <td rowspan="2"> {{$vendorDetail->address}}, {{$list_kota[$list_propinsi[$vendorDetail->province]][$vendorDetail->city]}} - {{$vendorDetail->post_code}} </td>

                  <td>NO SP </td>
                  <td width="1px">:</td>
                  <td>{{$data->no_po}}/{{date('m', $time)}}/{{$mainCat}}/Ecatalog/{{date('Y', $time)}}</td>
                </tr>
                <tr>
                  <td>NO Dokumen </td>
                  <td width="1px">:</td>
                  <td> - </td> <!--004/11/Obat/Ecatalog2018-->


                </tr>
                <tr>
                  <td>Telepon </td>
                  <td width="1px">:</td>
                  <td>{{$vendorDetail->telephone}} </td>


                  <td>Tanggal SP  </td>
                  <td width="1px">:</td>
                  <td>{{date('d', $time)}}-{{date('m', $time)}}-{{date('Y', $time)}}</td>
                </tr>
                <tr>
                  <td>Tipe Permintaan </td>
                  <td width="1px">:</td>
                  <td>
                    <select class="form-control" name="request_type">
                      <option value="1">Keluar (BHP/Inventaris)</option>
                    </select>
                  </td>


                  <td> Gudang Pemberi </td>
                  <td width="1px">:</td>
                  <td>
                    {{$categoryWarehouse->nama_instalasi}}
                    <input type="hidden" name="id_gudang" value="{{$categoryWarehouse->id}}">
                  </td>
                </tr>
              </table>
            </div>

                <div class="col-xs-12">
                  <table summary="Shopping cart" id="detailCart" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Item</th>
                        <th>Qty Pesanan</th>
                        <th>Qty Permintaan</th>
                        <th>Satuan</th>
                      </tr>
                    </thead>
                    @php
                    $totQty=0;
                    $totPrice=0;
                    @endphp
                    @foreach($dataOrder as $k => $v)
                      @php
                      $totQty += $v->qty;
                      $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
                      $totPrice += $tot;
                      @endphp
                      <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$v->datetime}}</td>
                        <td><input type="hidden" name="id_item[]" value="{{$v->id_item}}">{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
                        <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
                        <td> <input type="number" name="qty[]" class="form-control input-sm" value="{{$v->qty}}" required max="{{$v->qty}}" min="0" style="width:100%;"> </td>
                        <td>
                            <select class="form-control" name="satuan[]">
                              @foreach ($satuan as $k => $v)
                                <option value="{{$v->id}}" {{$v->id == '2' ? 'selected' : ''}}>{{$v->unit_name}}</option>
                              @endforeach
                            </select>
                        </td>
                      </tr>
                    @endforeach
                    <tbody>
                    </tbody>
                  </table>
                  <button type="submit" class="btn btn-primary pull-right" name="button">Simpan</button>
                </div>
              </form>
          </div>

              {{-- <div class="row">
              	<div class="col-md-12">
              				<div class="table-responsive">
                        <table style="width:100%;">
                          <tr>
                            <td style="text-align:right;"><b style="text-transform:uppercase;">{{$peminta->nama_instalasi}}</b> </td>
                          </tr>
                          <tr>
                            <td style="text-align:right;"><br><br><br></td>
                          </tr>
                          <tr>
                            <td style="text-align:right;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            <td style="text-align:right;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                          </tr>
                          <tr> --}}
                            {{-- <td style="text-align:center;"> <b><u>{{$supervisi->name}}</u></b> </td> --}}
                            {{-- <td style="text-align:right;"> <b><u>{{$peminta->username_catalog}}</u></b> </td>
                          </tr>
                          <tr> --}}
                            {{-- <td style="text-align:center;"> {{$supervisi->nip}} </td> --}}
                            {{-- <td style="text-align:right;"> {{$peminta->nip}} </td>
                          </tr>
                        </table>
              				</div>
              	</div>
          </div> --}}
        </div><!--/.portlet-body-->
      </div><!--/.portlet-light-->
    </div><!--/.col-md-12-->
  </div><!--/.row-->
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('aa','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
