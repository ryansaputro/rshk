@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')

  <style media="screen">
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal {
      display: block;
  }
  </style>


@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')

  <div class="col-md-12 col-sm-12">
    <h1>Tambah User Unit</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">

          </div>
          <br>
              <div class="row">
        <div class="col-md-12 ">
          <!-- BEGIN SAMPLE FORM PORTLET-->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption">

              </div>

            </div>
            <?php
            $id_user = Auth::user()->id;



                $user = DB::table('user_catalog_supervisi')
                ->select('id')
                ->where('user_catalog_supervisi.id_user', Auth::user()->id)
                ->first();

               $id_dir = $user->id;

                // $user1 = DB::table('users as u')
                // ->join('master_direktorat_penunjang as m', 'm.id', '=', 'u.id_direktur_penunjang')
                // ->select('m.kategory')
                // ->where('m.id',$id_dir)
                // ->first();

                $user1 = DB::table('user_catalog_supervisi as u')
                // ->join('master_direktorat_penunjang as m', 'm.id', '=', 'u.id_direktur_penunjang')
                ->select('kategory')
                ->where('id_user',$id_user)
                ->first();

              //  echo "<pre>";
              //  print_r($user1);
              //  echo "</pre>";

               $str = $user1->kategory;

               $str1 = (explode(",",$str));

                // echo "<pre>";
                // print_r($str1);
                // echo "</pre>";

                $category = DB::table('category')
                ->Wherein('id_parent',$str1)
                ->select('*')
                ->get();





            ?>
            <div class="portlet-body form">
              <form role="form" action="{{URL::to('catalog/users/supervisi/user_simpan')}}" method="post">
                  {{ csrf_field() }}
                <div class="form-body">
                  <div class="form-group">
                    <label>Kategory</label>
                     <select class="form-control select2" required="" id="id_kategory" name="id_kategory[]" multiple="multiple">

                            @foreach($category as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>

                           @endforeach()
                      </select>

                       <input type="checkbox" id="allkategory" >All
                  </div>



                  <div class="form-group">
                    <label>Kode Instalasi</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="kode" required=""  class="form-control" placeholder="Kode Instalasi">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Nama Unit Instalasi</label>
                    <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" required="" name="nama_instalasi" class="form-control input-circle-right" placeholder="Nama Instalasi">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Email</label>
                    <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="email" required="" name="email" class="form-control" placeholder="Email">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Username</label>
                    <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" name="username" required="" class="form-control input-circle-right" placeholder="Username">
                    </div>
                  </div>

                  <input type="hidden" value="{{$id_dir}}" name="id_direktur_penunjang">

                  <div class="form-group">
                    <label>Password</label>
                    <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input type="password" required="" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                  </div>


                </div>
                <div class="form-actions">
                  <button type="submit" class="btn blue">Submit</button>
                  <button type="button" class="btn default">Cancel</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

        </div>


      </div>
    </div>

  </div>




  <script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(function () {


              $("#id_kategory").select2();
              // $("#divisi_id").select2();
              // $("#activity_id").select2();
              // $("#market_type_id").select2();
              $("#id_kategory").trigger("change");
              // $("#divisi_id").trigger("change");


             $("#allkategory").click(function(){
                if($("#allkategory").is(':checked') ){
                    $("#id_kategory").find('option').prop("selected",true);
                    $("#id_kategory").trigger("change");
                    var id_kategory = $("#id_kategory").val();

                }else{
                    $("#id_kategory").val('').trigger("change");
                     var id_kategory = $("#id_kategory").val();
                     //alert(id_kategory);
                 }
            });


              $("#id_kategory").on('change', function() {
              var id_kategory  = $("#id_kategory").val();
              //alert(id_kategory);



        });



          });

  </script>

@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script>
  $(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
  $("li#history").attr("class","active");
});
</script>
 @endsection
