@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')

<div class="col-md-12 col-sm-12">
    <h1>Budget</h1>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="portlet-body">
					<form class="" action="{{URL::to('catalog/users/supervisi/budget/save')}}" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="">Periode</label>
							<input type="month" name="periode" value="" min="{{date('Y-m')}}" required class="form-control" style="text-transform: uppercase;">
						</div>
						<div class="form-group">
							<label for="">Disetujui Oleh</label>
							<select class="form-control" required name="approved_by">
                @foreach ($manager as $k => $v)
                  <option value="{{$v->id}}" >{{$v->name}}</option>
                @endforeach
							</select>
						</div>
            <label for="">Budget</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input id="msg" type="text" required class="form-control money" name="budget">
            </div>

						<div class="input-group" style="text-align:right;">
              <br>
							<a href="{{URL::to('catalog/users/supervisi/budget')}}" type="button" name="button" class="btn btn-sm btn-danger">
                <i class="fa fa-arrow-left"></i>
                kembali
              </a>
							<button type="submit" class="btn btn-sm btn-success">
								<i class="fa fa-check"></i>
								simpan
							</button>
						</div>
					</form>
				</div>
      </div>
    </div>

    {{-- <a href="{{URL::to('catalog/users/supervisi/dashboard')}}" class="btn btn-primary" style="float: left;">Dashboard <i class="fa fa-home"></i></a> --}}
    {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="confirm">Checkout <i class="fa fa-check"></i></button> --}}
    {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#myModalUsulan" id="confirm">Usulan <i class="fa fa-check"></i></button> --}}
    </div>


@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("div#notif").hide();
  $('.money').mask("000.000.000.000.000.000", {reverse: true});
});
  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});



$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
});
</script>
@endsection
