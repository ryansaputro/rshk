@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{$company->company}}
  </a>
@endsection

@section('content')
  @php
  $id = "";
  $totalAll =0;
  $jumlahData = count($dataCart);
  @endphp
<div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="col-md-6">
        <h1>Keranjang Belanja</h1>
      </div>
      <div class="col-md-6">
        <h2 style="float: right;">Budget <small>Rp.</small> {{number_format($budgetYear, 0, ',', '.')}}</h2>
      </div>
    </div>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    @foreach ($dataCart as $k => $v)
      @if($id !== $v->id_vendor_detail)
    <div class="goods-page">
       <div class="goods-data clearfix">
          <div class="table-wrapper-responsive">
             <table summary="Shopping cart" class="mainCart">
               <tr>
                 <th colspan="2">
                   <strong>
                     <span>
                       Penyedia : {{ DB::table('supplier_item')->where('id_item', $v->id_item)->join('vendor_detail', 'vendor_detail.id', '=', 'supplier_item.id_supplier')->value('vendor_name') }}
                   </span>
                   </strong>
                 </th>
                 <th colspan="6"  style="text-align:left;">
                   {{-- <a href="#" onclick="chatOnline(this)" data-id_item="{{$v->id_item}}" data-id_cart="{{$v->id}}" data-vendor="{{$v->id_vendor_detail}}"  class="btn btn-md btn-success pull-right" style="text-decoration: none; color: white;">
                     <i class="fa fa-comments"></i>
                     Negosiasi
                   </a> --}}
                 </th>
               </tr>
                <tr>
                  <th class="goods-page-image">Gambar</th>
                  <th class="goods-page-description">Deskripsi</th>
                  <th class="goods-page-ref-no">Ref Kode</th>
                  <th class="goods-page-quantity">Qty</th>
                  <th class="goods-page-price">Harga</th>
                  <th class="goods-page-price">Biaya Kirim</th>
                  <th class="goods-page-total" style="text-align:center;">Total</th>
                  <th class="goods-page-action actions"></th>
                </tr>
                @php
                $subtot=0;
                @endphp
                <tbody>
                   @if(count($dataCart) > 0)
                   @foreach ($dataCart as $key => $value)
                   @if($value->id_vendor_detail == $v->id_vendor_detail)
                     <input type="hidden" name="id_vendor_detail[{{$v->id}}]" value="{{$v->id_vendor_detail}}">
                   <tr class="dataTr tr_{{$value->id}}" data-id="{{$value->id}}">
                      <td class="goods-page-image">
                         <a href="javascript:;"><img class="imageProduct" src="/assets/catalog/item/{{ $value->file }}" alt=""></a>
                      </td>
                      <td class="goods-page-description">
                         <h3>
                            <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                            {{ $value->name }}
                            </a>
                         </h3>
                         <p><strong>Merk</strong> - {{ $value->merk }}</p>
                         <em>
                         <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                         Lainnya!
                         </a>
                         {{-- <a href="#" onclick="chatOnline(this)" data-id_item="{{$value->id_item}}" data-id_cart="{{$value->id}}" data-vendor="{{$value->id_vendor_detail}}"  class="btn btn-xs btn-success pull-right" style="text-decoration: none; color: white;">
                         <i class="fa fa-comments"></i>
                         Negosiasi
                         </a> --}}
                         </em>
                      </td>
                      <td class="goods-page-ref-no">
                         <h3>
                            <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                            {{ $value->code }}
                            <input type="hidden" name="id_item[{{$v->id}}][{{$value->id_item}}]" value="{{$value->id_item}}">
                            </a>
                         </h3>
                         <p>
                            <strong>Penyedia</strong> -
                            {{ DB::table('supplier_item')->where('id_item', $v->id_item)->join('vendor_detail', 'vendor_detail.id', '=', 'supplier_item.id_supplier')->value('vendor_name') }}
                         </p>
                         <p><strong>Harga</strong> -
                           @php
                           $time = strtotime($value->price_date);
                           $functionDay =  Fungsi::DayIndonesia();
                           $function =  Fungsi::MonthIndonesia();
                           @endphp
                           {{$functionDay[date('l', $time)]}}, {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}</p>
                      </td>
                      <td class="goods-page-quantity">
                         <div class="product-quantity">
                            <input id="product-quantity" style="width:50px;" name="qty[{{$v->id}}][{{$value->id_item}}]" oninput="InputQty(this)" type="number" value="{{$value->qty}}" data-vendor="{{$v->id}}" data-id="{{$value->id}}" readonly class="form-control input-sm qty qty_{{$value->id}}">
                         </div>
                      </td>
                      <td class="goods-page-price" style="text-align:right;">
                         @php
                         if ($value->price_country == 0)
                         $price =$value->price_gov;
                         else
                         $price = $value->price_gov;
                         @endphp
                         <strong>
                         <input type="hidden" name="price_pcs[{{$value->id}}][]" value="{{$price}}" class="price_pcs" data-id="{{$value->id}}">
                         <span>
                         {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                         {{number_format($price, 0, ',', '.')}}
                         </span>
                         </strong>
                      </td>
                      <td class="goods-page-price" style="text-align:right;">
                         @php
                         if ($value->price_country == 0)
                         $price =$value->price_gov;
                         else
                         $price = $value->price_gov;
                         @endphp
                         <strong>
                         <input type="hidden" name="price_shipment[{{$value->id}}][]" value="{{$value->price_shipment}}" class="price_shipment" data-id="{{$value->id}}">
                         {{-- <span><a href="{{ URL::to('catalog/users/item/chat/'.$value->id) }}" class="btn btn-xs btn-info pull-right hide" style="text-decoration: none; color: white;"> --}}
                         {{-- Chat In Here --}}
                         {{-- </a> --}}
                         {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                         {{number_format($value->price_shipment, 0, ',', '.')}}
                         </span>
                         </strong>
                      </td>
                      <td class="goods-page-total" style="text-align:right;">
                         <input type="hidden" name="total[{{$v->id}}][{{$value->id_item}}]" data-id="{{$v->id}}" value="{{($price*$value->qty)+$value->price_shipment}}" class="total total_{{$value->id}}">
                         <strong>
                         {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                         <span data-value="{{($price*$value->qty)+$value->price_shipment}}" data-id="{{$v->id}}" class="total_tag total_tag_{{$value->id}}">
                         {{number_format(($price*$value->qty)+$value->price_shipment, 0, ',', '.')}}
                         </span>
                         </strong>
                      </td>
                      <td class="del-goods-col">
                         <a class="del-goods" href="javascript:;" onclick="DeleteCart(this)" data-id="{{$value->id}}">&nbsp;</a>
                      </td>
                   </tr>
                   @php
                   $tot = (($price*$value->qty)+$value->price_shipment);
                   $subtot += $tot;
                   @endphp
                   @endif
                   @endforeach
                   @else
                   <tr>
                      <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
                   </tr>
                   @endif
                </tbody>
             </table>
             <div class="shopping-total" style="width: 400px;">
                <ul>
                   <li>
                      <em>Sub total</em>
                      <input type="hidden" name="subtot[{{$v->id}}]" data-id="{{$v->id}}" value="{{$subtot}}" class="input_subtot input_subtot_{{$v->id}}">
                      <strong class="price" style="margin-right:60px;">
                      Rp
                      <span class="subtot subtot_{{$v->id}}">{{number_format($subtot, 0, ',', '.')}}</span>
                      </strong>
                   </li>
                </ul>
             </div>
       @php
       $id = $v->id_vendor_detail;
       @endphp
         </div>
       </div>
     </div>
     @php
     $totalAll += $subtot;
     @endphp
    @endif

    @endforeach
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="shopping-total" style="width: 400px;">
          <ul>
            <li>
              <em>Total Harga ({{$jumlahData}} Item)</em>
              <input type="hidden" name="totalAll" value="{{$totalAll}}" class="totalAll">
              <strong class="price" style="margin-right:60px;">
                Rp
                <span class="totalAll">{{number_format($totalAll, 0, ',', '.')}}</span>
              </strong>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <a href="{{URL::to('catalog/users/supervisi/dashboard')}}" class="btn btn-primary" style="float: left;">Dashboard <i class="fa fa-home"></i></a>
    {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="confirm">Checkout <i class="fa fa-check"></i></button> --}}
    @if (count($usulan) > 0)
      <button class="btn btn-primary" data-toggle="modal" data-target="#myModalUsulan" id="confirm">Usulan <i class="fa fa-check"></i></button>
    @endif
    </div>

    <!-- Modal -->
      <div class="modal fade" id="myModalUsulan" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Usulan</h4>
            </div>
            <div class="modal-body">
              <select class="form-control usulan" name="usulan">
                @foreach ($usulan as $key => $v)
                  @php
                    $time = strtotime($v->datetime);
                  @endphp
                  <option value="{{$v->id}}">{{ $v->no_prop }}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</option>
                @endforeach
              </select>
            </div>
            <div class="modal-footer">
              {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
              {{-- <button type="button" class="btn btn-primary" data-dismiss="modal">checkout  <i class="fa fa-check"></i></button> --}}
              <button class="btn btn-primary" onclick="CheckOut(this)" id="confirm">Checkout <i class="fa fa-check"></i></button>
            </div>
          </div>

        </div>
      </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog" style="z-index:100000000000 !important;">
       <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
             <form class="" action="{{URL::to('catalog/users/supervisi/order')}}" method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <h4 class="modal-title header_usulan">Checkout Usulan#</h4>
                </div>
                <div class="modal-body" id="DivIdToPrint">
                   <div class="row">
                      <div class="col-md-12">
                         <table class="table table-striped" style="width: 100%">
                            <thead>
                               <tr>
                                  <th colspan="2" style="text-transform: uppercase; width: 50%;">Pembeli</th>
                                  <th colspan="2" style="text-transform: uppercase;">Supervisi</th>
                               </tr>
                            </thead>
                            <tbody>
                               @if(count($dataCart) > 0)
                               <tr>
                                  <td>Nama</td>
                                  <td style="text-transform: capitalize;">: {{$pembeli->pembeli}}</td>
                                  <td>Nama</td>
                                  <td style="text-transform: capitalize;">: {{$supervisi->name}}</td>
                               </tr>
                               <tr>
                                  <td>No Tlp</td>
                                  <td>: {{$pembeli->mobile_phone}}</td>
                                  <td>No Tlp</td>
                                  <td>: {{$supervisi->mobile_phone}}</td>
                               </tr>
                               <tr>
                                  <td>E-Mail</td>
                                  <td>: {{$pembeli->email}}</td>
                                  <td>E-Mail</td>
                                  <td>: {{$supervisi->email}}</td>
                               </tr>
                               <tr>
                                  <th>Budget</th>
                                  <th>: Rp. 100.000.000.000</th>
                                  <th>Sisa Budget</th>
                                  <th>: Rp. 99.000.000.000</th>
                               </tr>
                               @endif
                            </tbody>
                         </table>
                      </div>
                   </div>
                   {{--
                   <div class="row hide">
                      <div class="col-xs-6">
                         <address>
                            <strong>Pembeli</strong><br>
                            @if(count($dataCart) > 0)
                            {{$pembeli->username_catalog}}<br>
                            {{$pembeli->telephone}}<br>
                            {{$pembeli->email}}<br>
                            @endif
                         </address>
                      </div>
                      <div class="col-xs-6 text-right">
                         <address>
                            <strong>Supervisi</strong><br>
                            @if(count($dataCart) > 0)
                            {{$supervisi->name}}<br>
                            {{$supervisi->mobile_phone}}<br>
                            {{$supervisi->email}}<br>
                            @endif
                         </address>
                      </div>
                   </div>
                   --}}
                   <div class="row">
                      <div class="col-md-12">
                         <div class="table-wrapper-responsive">
                            <table class="table table-bordered" id="cartConfirm" border="1px;">
                               <thead>
                                  <tr>
                                     <th class="goods-page-image">Image</th>
                                     <th class="goods-page-description">Description</th>
                                     <th class="goods-page-ref-no">Ref Kode</th>
                                     <th class="goods-page-quantity" style="width:20%;">Quantity</th>
                                     <th class="goods-page-price">Unit price</th>
                                     <th class="goods-page-total">Total</th>
                                  </tr>
                               </thead>
                               <tbody>
                               </tbody>
                            </table>
                          <div class="subtot">
                          </div>
                         </div>
                      </div>
                   </div>
                </div>
                <span id="usulan"></span>
                <div class="modal-footer">
                   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                   <button type="button" class="btn btn-primary" id='btn' value='Print' onclick='printDiv();' target="_blank">Print</button>
                   <button type="submit" class="btn btn-success">Konfirmasi</button>
                </div>
             </form>
          </div>
       </div>
    </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript">

function CheckOut(a){
  $(a).attr('data-toggle','modal');
  $(a).attr('data-target','#myModal');
  var nilai = $('select[name=usulan] :selected').val();
  var input = '<input type="hidden" name="usulan" value="'+nilai+'">';
  $('.header_usulan').html("No Usulan "+$('select[name=usulan] :selected').text())
  $('#usulan').html(input);

  // alert(nilai)
}

$(document).ready(function() {
  $('.keranjang').attr('style','display:none !important;');
  $("div#notif").hide();
  var data = "{{$jumlahData}}";
  if(data == 0)
    $("#confirm").css("display","none");
  else
  $("#confirm").css("display","display");
});

function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  var vendorData = $(a).attr('data-vendor');
  var AllSubtot = 0;

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        $.each($('.price_shipment'), function(x,y){
          var ShipId = $(y).attr('data-id');
          if(ShipId == nilai_tr){
            var ship = parseInt($(y).val());
            total = ((nilai*price)+ship);
          }
        });

        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          idVendor = $(v).attr('data-id');
          if(vendorData == idVendor){
            subtot += Number(sub);
          }

          AllSubtot += Number(sub);
        })

          $('.input_subtot_'+vendorData).html(subtot);
          $('.input_subtot_'+vendorData).attr("value",subtot);
          $('.subtot_'+vendorData).html(addCommas(subtot));
          $('input.totalAll').attr("value", AllSubtot);
          $('span.totalAll').html(addCommas(AllSubtot));
          $(".Modal span#subtot").html(addCommas(subtot));
          $(".Modal input#input_subtot").attr("value",subtot);

      }
    });

}

$("#confirm").on('click', function(){
  var data="";
  $.each($('.mainCart'), function(x, y){
    data += $(y).html();
  });

  var html = "";
  $.each($('div.shopping-total'), function(x,y){
    html += $(y).html();
  })
  $('div.subtot').html(html)

  $("#cartConfirm").html(data);
  $("#cartConfirm th.goods-page-action").css('display','none');

  $("#cartConfirm a.del-goods").removeAttr("onclick");
  $("#cartConfirm a.del-goods").html("-");
  $("#cartConfirm a.del-goods").removeClass();
  $("#cartConfirm td.del-goods-col").css('display','none');
  $("#cartConfirm .imageProduct").css('width','100%');
});

function DeleteCart(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id_cart = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/supervisi/delete_cart')}}";
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_cart": id_cart,
              },
            success: function (a) {
              $('.tr_'+a.id_cart).remove();
              if(a.status == 1){
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-success");
                $("#status").html("Sukses!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                 location.reload();
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }else{
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-danger");
                $("#status").html("Gagal!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                 location.reload();
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }
            }
        });
    } else {
        location.reload();
    }
}

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});



$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
});
</script>
@endsection
