@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')
  <link href="/su_catalog/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
  <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
  <link href="/su_catalog/assets/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{$company->company}}
  </a>
@endsection

@section('content')
  @php
    $item = App\Model\Item::find($id);
  @endphp
  <div class="col-md-12 col-sm-12">
    <div class="product-page" style="padding: 10px;">
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <div class="product-main-image">
            @php
              $image_item = DB::table('image_item')->where('id_item', $item->id)->groupBy('id_item')->value('file');
            @endphp
            <img
            class="img-responsive"
            src="/assets/catalog/item/{{ $image_item }}"
            alt="{{ $item->name }}"
            data-BigImgsrc="/assets/catalog/item/{{ $image_item }}"
            >
          </div>
          <div class="product-other-images">
            @php
              $image_item = DB::table('image_item')->where('id_item', $item->id)->get();
            @endphp
            @foreach ($image_item as $key => $value)
              <a
              href="/assets/catalog/item/{{ $value->file }}"
              class="fancybox-button"
              rel="photos-lib">
              <img
              alt="{{ $item->name }}"
              src="/assets/catalog/item/{{ $value->file }}"
              >
            </a>
            @endforeach
          </div>
        </div>
        <div class="col-md-8 col-sm-8">
          <div class="product-page-content" style="padding: 10px;">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#Detail" data-toggle="tab">Detail</a></li>
              <li class=""><a href="#Description" data-toggle="tab">Description</a></li>
              <li><a href="#Specification" data-toggle="tab">Specification</a></li>
              <li><a href="#PriceHistory" data-toggle="tab">Price History</a></li>
              <li><a href="#Documents" data-toggle="tab">Documents</a></li>
              <li><a href="#Report" data-toggle="tab">Report</a></li>
              <li class="hide"><a href="#Reviews" data-toggle="tab">Reviews (2)</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade in active" id="Detail">
                <div class="uppercase" style="margin-bottom: 10px;">
                  @php
                  $sub_category = DB::table('category_item')->where('id_item', $item->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('id_parent');
                  $category = DB::table('category')->where('id', $sub_category)->value('id_parent');
                  @endphp
                  <span class="fa fa-bookmark"></span>
                  {{ DB::table('category')->where('id', $category)->value('name') }}
                  <br>
                  &nbsp;
                  <span class="fa fa-caret-right"></span>
                  {{ DB::table('category')->where('id', $sub_category)->value('name') }}
                  &nbsp;
                  <span class="fa fa-caret-right"></span>
                  {{ DB::table('category_item')->where('id_item', $item->id)->join('category', 'category.id', '=', 'category_item.id_category')->value('name') }}
                </div>
                <h4 style="font-size: 15px;">
                  Kode
                  <small>
                    {{ $item->code }}
                  </small>
                </h4>
                <h4 style="font-size: 15px;">
                  Nama
                  <small>
                    {{ $item->name }}
                  </small>
                </h4>
                <h4 style="font-size: 15px;">
                  Merk
                  <small>
                    {{ $item->merk }}
                  </small>
                </h4>
                <h4 style="font-size: 15px;">
                  Penyedia
                  <small>
                    {{ DB::table('supplier_item')->where('id_item', $item->id)->join('supplier', 'supplier.id', '=', 'supplier_item.id_supplier')->value('name') }}
                  </small>
                </h4>
                <div class="" style="color: red;">
                  <small>
                    {{ $item->stock == 0 ? 'Contact Supplier To Know Stock Available' : "Stock : ".$value->stock." Unit" }}
                  </small>
                </div>
                <h4 style="font-size: 15px;">
                  <small>Price</small>
                  {{ date('l, d F Y', strtotime($item->price_date)) }}
                  &nbsp;&nbsp;
                  <small>Release</small>
                  {{ date('l, d F Y', strtotime($item->release_date)) }}
                  &nbsp;&nbsp;
                  <small>Expired</small>
                  {{ date('l, d F Y', strtotime($item->expired_date)) }}
                </h4>
                <div class="price-availability-block clearfix">
                  <div class="price" style="margin-right: 10px;">
                    <em>Retail</em>
                    <strong>
                      <span>
                        {{ $item->price_country == 0 ? 'Rp ' : '$ ' }}
                      </span>
                      @if ($item->price_country == 0)
                        {{ number_format($item->price_retail, 2, ',', '.') }}
                      @else
                        {{ number_format($item->price_retail, 2, '.', ',') }}
                      @endif
                    </strong>
                  </div>
                  <div class="price">
                    <em>Goverment</em>
                    <strong>
                      <span>
                        {{ $item->price_country == 0 ? 'Rp ' : '$ ' }}
                      </span>
                      @if ($item->price_country == 0)
                        {{ number_format($item->price_gov, 2, ',', '.') }}
                      @else
                        {{ number_format($item->price_gov, 2, '.', ',') }}
                      @endif
                    </strong>
                  </div>
                  <div class="availability hide">
                    Availability: <strong>In Stock</strong>
                  </div>
                </div>
                <div class="product-page-cart">
                  <div class="product-quantity">
                      <input id="product-quantity" type="text" value="1" readonly class="form-control input-sm">
                  </div>
                  <button class="btn btn-primary" type="submit">Add to cart</button>
                </div>
                <div class="description hide">
                  <p>
                    {!! $item->description !!}
                  </p>
                </div>
                <div class="product-page-options hide">
                  <div class="pull-left">
                    <label class="control-label">Size:</label>
                    <select class="form-control input-sm">
                      <option>L</option>
                      <option>M</option>
                      <option>XL</option>
                    </select>
                  </div>
                  <div class="pull-left">
                    <label class="control-label">Color:</label>
                    <select class="form-control input-sm">
                      <option>Red</option>
                      <option>Blue</option>
                      <option>Black</option>
                    </select>
                  </div>
                </div>
                <div class="review hide">
                  <input type="range" value="4" step="0.25" id="backing4">
                  <div class="rateit" data-rateit-backingfld="#backing4" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
                  </div>
                  <a href="javascript:;">7 reviews</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:;">Write a review</a>
                </div>
                <ul class="social-icons hide">
                  <li><a class="facebook" data-original-title="facebook" href="javascript:;"></a></li>
                  <li><a class="twitter" data-original-title="twitter" href="javascript:;"></a></li>
                  <li><a class="googleplus" data-original-title="googleplus" href="javascript:;"></a></li>
                  <li><a class="evernote" data-original-title="evernote" href="javascript:;"></a></li>
                  <li><a class="tumblr" data-original-title="tumblr" href="javascript:;"></a></li>
                </ul>
              </div>
              <div class="tab-pane fade" id="Description">
                <p>
                  {!! $item->description !!}
                </p>
              </div>
              <div class="tab-pane fade" id="Specification">
                <table class="table table-bordered table-hover table-striped">
										<thead>
											<tr>
												<th style="width: 30%;">Nama</th>
												<th>Deskripsi</th>
											</tr>
										</thead>
										<tbody>
											@php
												$id_category = DB::table('category_item')->where('id_item', $item->id)->value('id_category');
												$id_parent = DB::table('category')->where('id', $id_category)->value('id_parent');
												// ->orderBy('name', 'ASC')
												$category_spec = DB::table('category_spec')->where('id_category', $id_parent)->get();
											@endphp
											@foreach ($category_spec as $key => $value)
												@php
													$description = DB::table('spec_item')->where('id_category_spec', $value->id)->where('id_item', $item->id)->value('description');
												@endphp
												<tr>
													<td>{{ $value->name }}</td>
													<td>
                            <p style="text-align: justify;">
                              {!! $description == '' ? '-' : $description !!}
                            </p>
													</td>
												</tr>
											@endforeach
											@if (sizeof($category_spec) == 0)
												<tr>
													<th colspan="2">Tidak ada Data </th>
												</tr>
											@endif
										</tbody>
									</table>
              </div>
              <div class="tab-pane fade" id="PriceHistory">
                @php
                $price_item = DB::table('price_item')->where('id_item', $item->id)->orderBy('price_date', 'desc')->get();
                @endphp
                <table class="table table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th rowspan="2" style="vertical-align: middle;">Tanggal</th>
											<th colspan="2">Harga</th>
										</tr>
										<tr>
											<th style="width: 25%;">Retail</th>
											<th style="width: 25%;">Pemerintah</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($price_item as $key => $value)
											<tr>
												<td style="vertical-align: middle;">{{ date('l, d F Y', strtotime($value->price_date)) }}</td>
												<td style="vertical-align: middle;">{{ $item->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($value->price_retail, 2, ",", ".") }}</td>
												<td style="vertical-align: middle;">{{ $item->price_country == 0 ? 'Rp.' : '$' }} {{ number_format($value->price_gov, 2, ",", ".") }}</td>
											</tr>
										@endforeach
										@if (sizeof($price_item) == 0)
											<tr>
												<td colspan="3">Tidak ada Data </td>
											</tr>
										@endif
									</tbody>
								</table>
              </div>
              <div class="tab-pane fade" id="Documents">
                @php
                  $doc_item = DB::table('doc_item')->where('id_item', $item->id)->get();
                @endphp
                <table class="table table-bordered table-hover table-striped">
										<thead>
											<tr>
												<th style="width: 5%;">No</th>
												<th>Deskripsi</th>
												{{-- <th>Deskripsi</th> --}}
												<th style="width: 10%;">Unduh</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($doc_item as $key => $value)
												<tr>
													<td style="vertical-align: middle; text-align: center;">{{ $key+1 }}</td>
													<td style="vertical-align: middle;">{{ $value->name }}</td>
													{{-- <td style="vertical-align: middle;">{{ $value->description == '' ? '-' : $value->description }}</td> --}}
													<td style="vertical-align: middle; text-align: center;">
														<a href="{{URL::asset('/assets/catalog/doc/'.$value->file)}}" target="_blank" class="btn btn-primary btn-sm">
															<span class="fa fa-download" style="color: white !important;"></span> Unduh
														</a>
													</td>
												</tr>
											@endforeach
											@if (sizeof($doc_item) == 0)
												<tr>
													<td colspan="4">Tidak ada Data </td>
												</tr>
											@endif
										</tbody>
									</table>
              </div>
              <div class="tab-pane fade" id="Report">
                <span style="font-size: 21px;">*</span>
  							<span style="font-size: 12px; color: gray;">Request To Fill</span>
  							<form class="form" action="report" method="post" style="margin: 5px 15px;">
  								{{ csrf_field() }}
  								<input type="hidden" name="id_user" value="0">
  								<input type="hidden" name="id_item" value="{{ $item->id }}">
  								<div class="form-group row">
  									<div class="input-group">
  										<div class="input-group-addon">* Nama</div>
  										<input type="text" name="nama" value="" class="form-control" required>
  									</div>
  								</div>
  								<div class="form-group row">
  									<div class="input-group">
  										<div class="input-group-addon">* NIK</div>
  										<input type="number" min="1" name="nik" value="" class="form-control" required>
  									</div>
  								</div>
  								<div class="form-group row">
  									<div class="input-group">
  										<div class="input-group-addon">* E-Mail</div>
  										<input type="email" name="email" value="" class="form-control" required>
  									</div>
  								</div>
  								<div class="form-group row">
                    <div class="input-group-addon">* Alasan</div>
  								</div>
  								<div class="form-group row">
                    <textarea name="alasan" rows="3" class="form-control"></textarea>
  								</div>
  								<div class="form-group row">
  									<button type="submit" class="btn btn-lg btn-primary" style="width: 100%; color: white; cursor: pointer;">
  										<span class="fa fa-paper-plane"></span> Report
  									</button>
  								</div>
  							</form>
              </div>
              <div class="tab-pane fade hide" id="Reviews">
                <!--<p>There are no reviews for this product.</p>-->
                <div class="review-item clearfix">
                  <div class="review-item-submitted">
                    <strong>Bob</strong>
                    <em>30/12/2013 - 07:37</em>
                    <div class="rateit" data-rateit-value="5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                  </div>
                  <div class="review-item-content">
                      <p>Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem.</p>
                  </div>
                </div>
                <div class="review-item clearfix">
                  <div class="review-item-submitted">
                    <strong>Mary</strong>
                    <em>13/12/2013 - 17:49</em>
                    <div class="rateit" data-rateit-value="2.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                  </div>
                  <div class="review-item-content">
                      <p>Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem.</p>
                  </div>
                </div>

                <!-- BEGIN FORM-->
                <form action="#" class="reviews-form" role="form">
                  <h2>Write a review</h2>
                  <div class="form-group">
                    <label for="name">Name <span class="require">*</span></label>
                    <input type="text" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email">
                  </div>
                  <div class="form-group">
                    <label for="review">Review <span class="require">*</span></label>
                    <textarea class="form-control" rows="8" id="review"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="email">Rating</label>
                    <input type="range" value="4" step="0.25" id="backing5">
                    <div class="rateit" data-rateit-backingfld="#backing5" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
                    </div>
                  </div>
                  <div class="padding-top-20">
                    <button type="submit" class="btn btn-primary">Send</button>
                  </div>
                </form>
                <!-- END FORM-->
              </div>
            </div>
          </div>
        </div>
        @if ($item->price_country == 0)
          <div class="sticker sticker-local"></div>
        @else
          <div class="sticker sticker-import"></div>
        @endif
      </div>
    </div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
  <script src="/su_catalog/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
  <script src="/su_catalog/assets/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
@endsection
