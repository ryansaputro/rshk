@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{$company->company}}
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Shopping cart</h1>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
        <table summary="Shopping cart" id="mainCart">
          <tr>
            <th class="goods-page-image">User Name</th>
            <th class="goods-page-image">Image</th>
            <th class="goods-page-description">Description</th>
            <th class="goods-page-ref-no">Ref Code</th>
            <th class="goods-page-quantity">Qty</th>
            <th class="goods-page-price">Unit price</th>
            <th class="goods-page-total" colspan="2">Total</th>
          </tr>
          @php
            $subtot=0;
            $jumlahData = count($dataCart);
          @endphp
          <tbody>
          @if(count($dataCart) > 0)
          @foreach ($dataCart as $key => $value)
            <tr class="dataTr tr_{{$value->id}}" data-id="{{$value->id}}">
              <td class="goods-page-image">{{$value->pembeli}}<br>{{$value->email}}<br>{{$value->telephone}}</td>
              <td class="goods-page-image">
                <a href="javascript:;"><img src="/assets/catalog/item/{{ $value->file }}" alt=""></a>
              </td>
              <td class="goods-page-description">
                <h3>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    {{ $value->name }}
                  </a>
                </h3>
                <p><strong>Merk</strong> - {{ $value->merk }}</p>
                <em>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    More info is here!
                  </a>
                </em>
              </td>
              <td class="goods-page-ref-no">
                <h3>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    {{ $value->code }}
                    <input type="hidden" name="id_item[{{$value->id}}]" value="{{$value->id_item}}">
                  </a>
                </h3>
                <p>
                  <strong>Penyedia</strong> - {{ DB::table('supplier_item')->where('id_item', $value->id)->join('supplier', 'supplier.id', '=', 'supplier_item.id_supplier')->value('name') }}
                </p>
                <p><strong>Price</strong> - {{ date('l, d F Y', strtotime($value->price_date)) }}</p>
              </td>
              <td class="goods-page-price">
                {{-- <div class="product-quantity"> --}}
                    <input id="product-quantity" name="qty[{{$value->id}}]" onchange="InputQty(this)" type="hidden" value="{{$value->qty}}" data-id="{{$value->id}}" readonly class="form-control input-sm qty qty_{{$value->id}}">
                    <strong>
                      <span>
                        {{$value->qty}}
                      </span>
                    </strong>
                  {{-- </div> --}}
              </td>
              <td class="goods-page-price">
                @php
                if ($value->price_country == 0)
                  $price =$value->price_gov;
                else
                  $price = $value->price_gov;
                @endphp
                <strong>
                  <input type="hidden" name="price_pcs[{{$value->id}}][]" value="{{$price}}" class="price_pcs" data-id="{{$value->id}}">
                  <span>
                    {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                    {{number_format($price, 0, ',', '.')}}
                  </span>
                </strong>
              </td>
              <td class="goods-page-total">
                <input type="hidden" name="total[{{$value->id}}][]" value="{{$price*$value->qty}}" class="total total_{{$value->id}}">
                <strong>
                  {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                  <span data-value="{{$price*$value->qty}}" class="total_tag total_tag_{{$value->id}}">
                    {{number_format($price*$value->qty, 0, ',', '.')}}
                  </span>
                </strong>
              </td>
            </tr>
            @php
              $tot = $price*$value->qty;
              $subtot += $tot;
            @endphp
          @endforeach
        @else
          <tr>
            <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
          </tr>
        @endif
        </tbody>
        </table>
        </div>
        <div class="shopping-total" style="width: 300px;">
          <ul>
            <li>
              <em>Total</em>
              <input type="hidden" name="subtot" value="" id="input_subtot">
              <strong class="price">
                Rp
                <span id="subtot">{{number_format($subtot, 0, ',', '.')}}</span>
              </strong>
            </li>
            {{-- <li>
              <em>Shipping Cost <small>PPN 10%</small></em>

              <strong class="price"><span>Rp </span>xxx</strong>
            </li> --}}
            {{-- <li class="shopping-total-price">
              <em>Total</em>
              <strong class="price"><span>Rp </span>xxx</strong>
            </li> --}}
          </ul>
        </div>
      </div>
      <a href="{{URL::to('catalog/users/supervisi/dashboard')}}" class="btn btn-primary" style="float: left;">Dashboard <i class="fa fa-home"></i></a>
      {{-- <button class="btn btn-primary" type="submit" style="float: left;">Continue shopping <i class="fa fa-shopping-cart"></i></button> --}}
    </div>
  </div>

@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript">

$(document).ready(function() {
  $('.keranjang').attr('style','display:none !important;');
  $("div#notif").hide();
  var data = "{{$jumlahData}}";
  if(data == 0)
    $("#confirm").css("display","none");
  else
  $("#confirm").css("display","display");
});

function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        total = nilai*price
        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          subtot += Number(sub);
        })
          $('#subtot').html(addCommas(subtot));
          $('#input_subtot').html(subtot);
          $(".Modal span#subtot").html(addCommas(subtot));
      }
    });

}


$("#confirm").on('click', function(){
  $("#cartConfirm").html($('#mainCart').html());
  $("#cartConfirm a.del-goods").removeAttr("onclick");
  $("#cartConfirm a.del-goods").html("-");
  $("#cartConfirm a.del-goods").removeClass();
});

function DeleteCart(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id_cart = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/delete_cart')}}";
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_cart": id_cart,
              },
            success: function (a) {
              $('.tr_'+a.id_cart).remove();
              if(a.status == 1){
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-success");
                $("#status").html("Sukses!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }else{
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-danger");
                $("#status").html("Gagal!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }
            }
        });
    } else {
        location.reload();
    }
}

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
});
</script>
@endsection
