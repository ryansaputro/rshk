@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')

<div class="col-md-12 col-sm-12">
  <div class="row">
    <div class="col-md-6">
      <h1>Budget Terpakai</h1>
    </div>
    <div class="col-md-6">
      <h2 style="float: right;"> <small>Rp.</small> {{number_format($budgetUsed, 0, ',', '.')}}</h2>
    </div>
  </div>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="portlet-body">
          <div class="row">
            <form>
              <div class="col-sm-offset-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form>
          </div>
          <br>
          <table class="table table-striped">
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Dipakai Oleh</th>
              <th>No Usulan</th>
              <th>No PO</th>
              <th style="text-align:right;">Nominal</th>
            </tr>
            @php
            $budget_used = 0;
            @endphp
          @if (count($DataDetail) > 0)
          @foreach ($DataDetail as $k => $v)
            @php
            $time = strtotime($v->datetime);
            $time_po = strtotime($v->datetime);
            $id_proposer = md5($v->id_proposer);
            $no_po = md5($v->no_po);
            $month_po = md5(date('n', $time_po));
            $year_po = md5(date('Y', $time_po));
            $month = md5(date('n', $time));
            $year = md5(date('Y', $time));
            $budget_used += $v->budget_used;
            @endphp
            <tr>
              <td>{{$k+1}}</td>
              <td>{{$v->datetime}}</td>
              <td>{{($v->username_catalog != null) ? $v->username_catalog : $v->name}}</td>
              <td>
                  <a target="_blank" href="{{URL::to('catalog/users/supervisi/proposer/download/'.$id_proposer)}}">{{ $v->no_prop }}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</a>
              </td>
              <td>
                @if($v->status == '4')
                  <a target="_blank" href="{{URL::to('catalog/users/supervisi/po/download/'.$no_po.'/'.$month_po.'/'.$year_po)}}">{{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}<a/>
                @else
                  {{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}
                @endif
              </td>
              <td style="text-align:right;">Rp. {{ number_format($v->budget_used, 0, ',', '.') }}</td>
            </tr>
          @endforeach
        @else
          <tr>
            <td colspan="6" style="text-align:center;">Tida Ada Data</td>
          </tr>
        @endif
          <tr>
            <td style="text-align:right;" colspan="5"><b>Total</b></td>
            <td style="text-align:right;"><b>Rp. {{ number_format($budget_used, 0, ',', '.') }}</b></td>
          </tr>
          </table>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
<script type="text/javascript">
$('.filter').on('change', function(){
  var filter = $(this).val();
  if(filter == 'tanggal'){
    $('div.search').html('<input type="date" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }else{
    $('div.search').html('<input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }
});

$(document).ready(function() {
  $("div#notif").hide();
  $('.money').mask("000.000.000.000.000.000", {reverse: true});
});
  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});



$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
});
</script>
@endsection
