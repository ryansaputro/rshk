@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
  </a>
@endsection

@section('content')
<img src="https://www.pjnhk.go.id/storage/uploads/rujukan-nasional/x6rrcMFv420ruwcl4VsK8jEPYxmGyF0O5rr4u6kq.jpeg" alt="" style="width: 100% !important;height:300px;">

 <div class="top-right">
   {{-- Manager : {{$company->name_manager}}<br>
   Finance : {{$company->name_finance}}<br>
   Supervisi : {{$company->name_supervisi}}<br>
   Login : {{$company->username_catalog}}<br> --}}
 </div>

<!--buttoon-->
  <div class="btn-group btn-group-justified">
      {{-- <div class="btn-group">
          <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/cart')}}'">
              <span class="glyphicon glyphicon-shopping-cart"></span>
              <p>Keranjang</p>
          </button>
      </div> --}}
      <div class="btn-group">
      <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/order_waiting')}}'">
              <span class="glyphicon glyphicon-cd"></span>
        <p>
          Pesanan Menunggu
          @if ($order > 0)
            <span class="badge badge-success">{{$order}}</span></a>
          @endif
        </p>

      </button>
      </div>
      <div class="btn-group">
      <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/order_approved')}}'">
              <span class="glyphicon glyphicon-thumbs-up"></span>
        <p>Pesanan Tersetujui</p>
      </button>
      </div>
      {{-- <div class="btn-group">
      <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/budget')}}'">
              <span class="glyphicon glyphicon-briefcase"></span>
        <p>Budget</p>
      </button>
      </div> --}}
      {{-- <div class="btn-group">
          <button type="button" class="btn btn-nav">
              <span class="glyphicon glyphicon-plane"></span>
        <p>Order Deliver</p>
          </button>
      </div>
      <div class="btn-group">
          <button type="button" class="btn btn-nav">
              <span class="glyphicon glyphicon-ok"></span>
        <p>Order Success</p>
          </button>
      </div> --}}
      <div class="btn-group">
          <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/track')}}'">
              <span class="glyphicon glyphicon-bell"></span>
        <p>Lacak</p>
          </button>
      </div>
      <div class="btn-group">
          <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/history')}}'">
              <span class="glyphicon glyphicon-list-alt"></span>
        <p>Riwayat</p>
          </button>
      </div>


       <div class="btn-group">

          <button type="button"  class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/supervisi/user')}}'">
              <span class="glyphicon glyphicon-user"></span>
        <p>User </p>
          </button>
      </div>
  </div>
<!--./buttoon-->
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script type="text/javascript">
var activeEl = '';
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});
$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');

  $("#user").removeAttr('href');
  $("a#user").attr('href','{{URL::to('catalog/users/supervisi/user')}}');

  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
  $("li#dashboard_nav").attr("class","active");
  $("li.menu-search").hide();

  $("#user123").removeAttr('href');
  $("a#user123").attr('href','{{URL::to('catalog/users/user')}}');
  $("li#dashboard_nav").attr("class","active");
  $("li.menu-search").hide();


});
</script>
@endsection
