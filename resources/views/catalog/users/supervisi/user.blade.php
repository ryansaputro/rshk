@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <style media="screen">
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal {
      display: block;
  }
  </style>
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>User Unit</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">

          </div>
          <br>
         <!--  <button href="" class="btn btn-primary btn-sm pull-right">
              <i class="fa fa-plus"></i>Tambah</button> -->

              <a href="{{URL::to('catalog/users/supervisi/user_tambah')}}"  class="btn btn-default pull-right"> <i class="fa fa-plus"></i>Tambah</a>

          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Nama Unit</th>
                  <th>Email</th>
                  <th>Kategory</th>
                  <th></th>

                </tr>
              </thead>
              <tbody>

                <?php
                  $no=1;
                  // echo "<pre>";
                  // print_r($data);
                  echo "</pre>";
                ?>
                    @foreach($data as $d)

                      <tr>
                          <td>{{$no++}}</td>
                          <td>{{$d->username}}</td>
                          <td>{{$d->nama_instalasi}}</td>
                          <td>{{$d->email}}</td>
                          <td><button type="button"
                            class="btn btn-primary id"
                            data-toggle="modal"
                            data-id="{{$d->id_kategory}}"
                            data-target="#exampleModalLong">
                                   Kategory
                                </button>
                           <td>




                            <form action="{{URL::to('catalog/users/supervisi/user_delete',$d->id)}}"" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button class="btn btn-sm btn-danger pull-right" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash"></i></button>
                               <a href="{{URL::to('catalog/users/supervisi/user_edit',$d->id)}}"  class="btn btn-default  btn-sm pull-right"> <i class="fa fa-pencil"></i></a>
                               </td>
                            </form>
                        </td>
                      </tr>

                    @endforeach()


              </tbody>
          </table>
        </div>


      </div>
    </div>

  </div>

<script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript">

</script>
 <script type="text/javascript">


     $(".id").click(function(){
             var a = $(this).attr('data-id');

              var url = "{{URL::to('catalog/users/user_kategory')}}";

                  $.ajax({
                 type: "post",
                  url: url,
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id_kategory": a,
                    },
                  success:function(data){
                    // alert(data);
                   $("#id_aktivity").html(data);


                  }

                });


        });



    </script>

   <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLongTitle">Kategory</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                            <div id="id_aktivity"></div>

                                </div>
                                <div class="modal-footer">

                                </div>
                              </div>
                            </div>
                          </div>




@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script>
  $(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/supervisi/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/supervisi/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/supervisi/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/supervisi/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/supervisi/track')}}');
  $("li#history").attr("class","active");
});
</script>
 @endsection
