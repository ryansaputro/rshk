@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Riwayat Pesanan</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
            {{-- <form>
              <div class="col-sm-offset-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                  <option value="status">Status</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form> --}}
          </div>
          <br>
          <table id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Usulan</th>
                  <th>No SP</th>
                  <th>No Pesanan</th>
                  <th>Tanggal</th>
                  <th>Pembeli</th>
                  <th>Status</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                @if(count($order) > 0)
                @foreach($order as $k => $v)
                  @php
                    $time = strtotime($v->datetime);
                    $id_proposer = md5($v->id_proposer);
                    $no_po = md5($v->no_po);
                    $month = md5(date('n', $time));
                    $year = md5(date('Y', $time));
                  @endphp
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>
                        <a target="_blank" href="{{URL::to('catalog/users/proposer/download/'.$id_proposer)}}">{{ $v->no_prop }}.{{$v->kode_instalasi}}.{{date('m', $time)}}.{{date('Y', $time)}}</a>
                    </td>
                    <td>
                      @if($v->status >= '3')
                        <a target="_blank" style="text-transform:uppercase;" href="{{URL::to('catalog/users/po/download/'.md5($v->id))}}">
                          {{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('m', $time)}}/<?php error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); echo $v->name_category ?>/E-catalog{{date('Y', $time)}}<a/>
                      @else
                        <font style="text-transform:uppercase;">
                          {{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('m', $time)}}/<?php error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); echo $v->name_category ?>/E-catalog{{date('Y', $time)}}
                        </font>
                      @endif
                    </td>
                    <td>#{{$v->no_order}}</td>
                    <td>{{$v->datetime}}</td>
                    <td>{{($v->username_catalog == null) ? $v->name : $v->username_catalog}}</td>
                    <td>
                      @php
                        $status = $v->status;
                      @endphp
                      <span style="display: none;">{{ $status }}</span>
                      @if ($status == 0)
                        <span class="label bg-grey" style="display: block;">Menunggu Persetujuan</span>
                      @elseif ($status == 1)
                        <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                      @elseif ($status == 2)
                        <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                      @elseif ($status == 3)
                        <span class="label bg-green" style="display: block;">Telah disetujui PPK</span>
                      @elseif ($status == 4)
                        <span class="label bg-purple" style="display: block;">Barang Dikirim</span>
                      @elseif ($status == 5)
                        <span class="label bg-grey" style="display: block;">Barang Terkirim</span>
                      @elseif ($status == 6)
                        <span class="label bg-yellow" style="display: block;">Barang diterima oleh Gudang</span>
                      @endif
                    </td>
                    <td> <a style="float:left !important; color:white;" href="#" onclick="DetailCart(this)" class='btn btn-primary btn-xs' data-id="{{$v->id}}">Detail</a></td>
                  </tr>
                @endforeach
              @else
              <tr>
                <td ></td>
                <td ></td>
                <td ></td>
                <td >Tidak Ada</td>
                <td ></td>
                <td ></td>
                <td ></td>
                <td ></td>
              </tr>
              @endif
              </tbody>
          </table>
        </div>
        {{-- <div class="row">
          <div class="col-md-5 col-sm-5">
            <div>menampilkan {{ ($order->currentPage() - 1) * $order->perPage() + 1 }} sampai {{ $order->count() * $order->currentPage() }} dari {{ $order->total() }} data</div>
          </div>
            <div class="col-md-7 col-sm-7 block-paginate">{{ $order->links() }}</div>
        </div> --}}

        <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
          <form class="" action="{{URL::to('catalog/users/approvement')}}" method="post">
            {{ csrf_field() }}
          <h1>  <center>Detail Pesanan</center></h1>
          <div class="row">
            <div class="col-xs-6">
              <address>
              <strong>Pembeli: </strong><br>
                <p id="name_buyer">Nama : xx</p>
                <p id="telephone_buyer">Telp: xx</p>
                <p id="email_buyer">Email: xx</p>
              </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                <strong>Order:</strong><br>
                <p id="no_order">No Order : yy</p>
                <p id="date_order">Tanggal : yy</p>
              </address>
            </div>

          </div>
          <table summary="Shopping cart" id="detailCart" class="table table-bordered table striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Total</th>
                  <th>Status</th>
                  <th>Keterangan</th>
                  <th>Eksekutor</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          {{-- <div class="shopping-total Modal" style="width: 300px;">
            <ul>
              <li style="margin-left:-195px;">
                <em>Total</em>
                <input type="hidden" name="subtot" value="" id="input_subtot">
                <strong class="price">
                  Rp
                  <span id="subtot"></span>
                </strong>
              </li> --}}
              {{-- <li>
                <em>Shipping Cost <small>PPN 10%</small></em>

                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
              {{-- <li class="shopping-total-price">
                <em>Total</em>
                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
            {{-- </ul>
          </div> --}}
        </form>
        </div>
      </div>
    </div>
    <div class="modal"></div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script type="text/javascript">
$(document).ready( function () {
  $('#mainCart').DataTable();
} );


$('.filter').on('change', function(){
  var filter = $(this).val();
  var select = '<select class="form-control input-sm" name="search" class="filter">'+
                    '<option value="5">Semua</option>'+
                    '<option value="0">Menunggu Persetujuan</option>'+
                    '<option value="1">Telah disetujui Kepala Instalasi</option>'+
                    '<option value="2">Telah disetujui Direksi</option>'+
                    '<option value="3">Telah disetujui PPK</option>'+
                    '<option value="4">Telah disetujui Finance</option>'+
                '</select>'

  if(filter == 'status'){
    $('div.search').html(select)
  }else if(filter == 'tanggal'){
    $('div.search').html('<input type="date" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }else{
    $('div.search').html('<input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
  }
});
var activeEl = 2;
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});

function DetailCart(a) {
  var id_order = $(a).attr('data-id');
  var url = "{{URL::to('catalog/users/detail_history')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_order": id_order,
              },
            success: function (a) {
              $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
              $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
              $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
              $("#no_order").html("<b>No Order :</b> #"+a.dataItem.no_order);
              $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
              var table = ""
              var subtot = 0;
              $.each(a.data, function(k, v){
                if(v.username_catalog !== null){
                  executor = v.username_catalog;
                }else if(v.name_supervisi !== null){
                  executor = v.name_supervisi;
                }else if(v.name_manager !== null){
                  executor = v.name_manager;
                }else if(v.name_directur !== null){
                  executor = v.name_directur;
                }

                if(v.event == 'NEW'){
                  statusOrder = "<span class='btn-info btn-block btn-sm'>Baru <i class='fa fa-dot-circle-o' aria-hidden='true'></i></span>"
                }else if(v.event == 'REDUCE'){
                  statusOrder = "<span class='btn-warning btn-block btn-sm'> Dikurangi <i class='fa fa-arrow-down' aria-hidden='true'></i></span>"
                }else if(v.event == 'ADD'){
                  statusOrder = "<span class='btn-success btn-block btn-sm'> Ditambah <i class='fa fa-arrow-up' aria-hidden='true'></i> </span>"
                }else if(v.event == 'REJECT'){
                  statusOrder = "<span class='btn-danger btn-block btn-sm'>Ditolak <i class='fa fa-times' aria-hidden='true'></i></span>"
                }


                table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                table += '<td>'+v.datetime_history+'</td>';
                table += '<td>'+v.name+'</td>';
                table += '<td>';
                    table += '<strong>';
                    table += '<span>'+addCommas(v.qty);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td>';
                    table += '<strong>';
                    table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                    table += '<span>Rp. '+addCommas(v.price_gov);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td>  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_gov*v.qty+'" class="total total_'+v.id_item+'">';
                      table += '<strong>';
                      table += '<span data-value="'+v.price_gov*v.qty+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas(v.price_gov*v.qty)+'</span>';
                      table += '</strong>';
                table += '</td>';
                table += '<td style="text-align:center;">'+statusOrder+'</td>';
                table += '<td>'+v.description+'</td>';
                table += '<td>'+executor+'</td>';
                table += "</tr>";
                sub = v.price_gov*v.qty;
                subtot += sub;
              });

              $("#detailCart tbody").html(table);
              $(".Modal span#subtot").html(addCommas(subtot));
              $("html, body").animate({ scrollTop: $("#detailCart").prop("scrollHeight")}, 500)

            }
    })
  $("#divDetailCart").css('display','');
}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});

function InputQty(a) {

  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        total = nilai*price;

        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          subtot += Number(sub);
        })
          $('#subtot').html(addCommas(subtot));
          $('#input_subtot').html(subtot);
          $(".Modal span#subtot").html(addCommas(subtot));
      }
    });

}

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}


$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("#dashboard_nav").attr('href','{{URL::to('catalog/users')}}');
  $("#cartItem").removeAttr('href');
  $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
  $("#cart").css('display','block');
  $("#history").removeAttr('href');
  $("#history").attr('href','{{URL::to('catalog/users/history')}}');
  $("#approval").removeAttr('href');
  $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
  $("li#history").attr("class","active");

});
</script>
@endsection
