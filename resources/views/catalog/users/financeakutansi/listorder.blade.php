@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/css/tracking.css"/>
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    
  </a>
@endsection

@section('sidebar')
  {{-- @include('catalog.users.layouts.sidebar') --}}
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Lacak Pesanan</h1>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light">
          <div class="row">
            <form>
              <div class="col-sm-offset-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="usulan">No Usulan</option>
                  <option value="po">No PO</option>
                  <option value="tanggal">Tanggal</option>
                  <option value="status">Status</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form>
          </div>
          <div class="portlet-body">
           <table class="table table-striped">
             <thead>
               <tr>
                 <th>No</th>
                 <th>No Usulan</th>
                 <th>No PO</th>
                 <th>No Pesanan</th>
                 <th>Tanggal</th>
                 <th>Nominal</th>
                 <th>Status</th>
                 <th>Aksi</th>
               </tr>
             </thead>
             <tbody>
               @if(count($order) > 0)
               @foreach($order as $k => $v)
                 @php
                 $time = strtotime($v->datetime);
                 $id_proposer = md5($v->id_proposer);
                 $no_po = md5($v->no_po);
                 $month = md5(date('n', $time));
                 $year = md5(date('Y', $time));
                 @endphp
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>
                       <a target="_blank" href="{{URL::to('catalog/users/finance/proposer/download/'.$id_proposer)}}">{{ $v->no_prop }}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</a>
                   </td>
                   <td>
                     @if($v->status >= '3')
                       <a target="_blank" href="{{URL::to('catalog/users/finance/po/download/'.$no_po.'/'.$month.'/'.$year)}}">{{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}<a/>
                     @else
                       {{ $v->no_po }}/P.O/{{date('m', $time)}}/{{date('Y', $time)}}
                     @endif
                   </td>
                   <td>#{{ $v->no_order }}</td>
                   <td>
                     <span style="display: none;">

                     </span>
                     {{$v->datetime}}
                   </td>
                   <td>
                     Rp. {{ number_format($v->total, 0, ',', '.') }}
                   </td>
                   <td>
                     @php
                       $status = $v->status;
                     @endphp
                     <span style="display: none;">{{ $status }}</span>
                     @if ($status == 0)
                       <span class="label bg-grey" style="display: block;">Menunggu Persetujuan</span>
                       @elseif ($status == 1)
                         <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                       @elseif ($status == 2)
                         <span class="label bg-blue" style="display: block;">Telah disetujui Kepala Bagian</span>
                     @elseif ($status == 3)
                       <span class="label bg-green" style="display: block;">Telah disetujui PPK</span>
                     @elseif ($status == 4)
                       <span class="label bg-purple" style="display: block;">Barang Dikirim</span>
                     @elseif ($status == 5)
                       <span class="label bg-grey" style="display: block;">Barang Terkirim</span>
                     @elseif ($status == 6)
                       <span class="label bg-yellow" style="display: block;">Barang diterima oleh Gudang</span>
                     @endif
                   </td>
                   <td>
                     <a style="float:left !important; color:white;" href="#" onclick="DetailCart(this)" class='btn btn-primary btn-xs' data-id="{{$v->id}}">Detail</a>
                     <a style="float:left !important; color:white;" href="#" onclick="TrackCart(this)" class='btn btn-success btn-xs' data-id="{{$v->id}}">Lacak</a>
                   </td>
                 </tr>
               @endforeach
               @else
                 <tr>
                   <td colspan="8" style="text-align:center;">Tidak Ada Data</td>
                 </tr>
               @endif
             </tbody>
           </table>
           <!-- BEGIN PAGINATOR -->
           <div class="row">
             <div class="col-md-5 col-sm-5">
               <div>menampilkan {{ ($order->currentPage() - 1) * $order->perPage() + 1 }} sampai {{ $order->count() * $order->currentPage() }} dari {{ $order->total() }} data</div>
             </div>
               <div class="col-md-7 col-sm-7 block-paginate">{{ $order->links() }}</div>
           </div>
           <!-- END PAGINATOR -->

            <div class="card">
             <div class="card-body">
                <div class="table-wrapper-responsive hide" id="divDetailCart" style="padding-top:15px;">
                   <h1><center>Detail Pesanan</center></h1>
                   <div class="row">
                     <div class="col-xs-6">
                       <address>
                       <strong>Pembeli: </strong><br>
                         <span id="name_buyer">Nama : xx</span><br>
                         <span id="telephone_buyer">Telp: xx</span><br>
                         <span id="email_buyer">Email: xx</span><br>
                       </address>
                     </div>
                     <div class="col-xs-6 text-right">
                       <address>
                         <strong>Pesanan:</strong><br>
                         <span id="no_order">No Pesanan : yy</span><br>
                         <span id="date_order">Tanggal : yy</span><br>
                       </address>
                     </div>
                   </div>
                   <table summary="Shopping cart" id="detailCart" class="table table-bordered table striped">
                       <thead>
                         <tr>
                           <th>No</th>
                           <th>Item</th>
                           <th>Qty</th>
                           <th>Harga</th>
                           <th>Ongkos Kirim</th>
                           <th>Total</th>
                         </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
                   {{-- <a href="{{URL::to('catalog/users/proposer/download/'.$)}}" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Unduh Usulan</a> --}}
                </div>
              </div>
            </div>

            <div class="card">
             <div class="card-body">
                <div class="table-wrapper-responsive hide" id="divTrackCart" style="padding-top:15px;">
                    <h1><center>Lacak Pesanan</center></h1>
                    {{-- <div class="stepwizard" style="margin-bottom:20px;">
                      <div class="stepwizard-row setup-panel">
                        @for($i=0; $i<=4; $i++)
                        @if ($i == 0)
                        @php $status = "Menunggu Persetujuan"; @endphp
                        @elseif ($i == 1)
                        @php $status = "Telah disetujui Kepala Instalasi"; @endphp
                        @elseif ($i == 2)
                        @php $status = "Telah disetujui Direksi"; @endphp
                        @elseif ($i == 3)
                        @php $status = "Telah disetujui PPK"; @endphp
                        @elseif ($i == 4)
                        @php $status = "Telah disetujui Finance"; @endphp
                        @endif
                        <div class="stepwizard-step">
                        <a href="#" type="button" class="btn btn-danger btn-circle status_{{$i}} status"><i class="fa fa-close" aria-hidden="true"></i></a>
                        <p>{{$status}}</p>
                        </div>
                        @endfor
                      </div>
                    </div> --}}
                <div class="container">
                    <div class="row">
                            <div class="board">
                              <div class="board-inner">
                                <ul class="nav nav-tabs" id="myTab">
                                  <div class=""></div>
                                  {{-- <div class="liner"></div> --}}
                                     <li class="active" id="li_satu">
                                       <a href="#0" data-toggle="tab" title="Pengusul" class="menu" data-id="0">
                                        <span class="round-tabs one" id="0">
                                                <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                                <div style="size:12px;">Pengusul</div>
                                        </span>
                                        </a>
                                    </li>

                                      <li><a href="#1" data-toggle="tab" title="Kepala Instalasi" class="menu" data-id="1">
                                         <span class="round-tabs four" id="1">
                                           <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">Kepala Instalasi</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#2" data-toggle="tab" title="Direktur" class="menu" data-id="2">
                                         <span class="round-tabs four" id="2">
                                           <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">Direktur</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#3" data-toggle="tab" title="PPK" class="menu" data-id="3">
                                         <span class="round-tabs four" id="3">
                                           <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">PPK</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#4" data-toggle="tab" title="Dikirim" class="menu" data-id="4">
                                         <span class="round-tabs four" id="4">
                                           <i class="fa fa-truck fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">Dikirim</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#5" data-toggle="tab" title="Diterima" class="menu" data-id="5">
                                         <span class="round-tabs four" id="5">
                                           <i class="fa fa-gift fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">Diterima</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#6" data-toggle="tab" title="Finance" class="menu" data-id="6">
                                         <span class="round-tabs four" id="6">
                                           <i class="fa fa-money fa-3x" aria-hidden="true" style="padding-top:5px;"></i>
                                           <div style="size:12px;">Menunggu Pembayaran</div>
                                         </span>
                                        </a>
                                     </li>

                                      <li><a href="#7" data-toggle="tab" title="Selesai" class="menu" data-id="7">
                                         <span class="round-tabs four" id="7">
                                           <i class="fa fa-dollar fa-3x" aria-hidden="true" style="padding-top:5px;"></i>
                                           <div style="size:12px;">Pembayaran Sukses</div>
                                         </span>
                                        </a>
                                     </li>
                                      <li><a href="#8" data-toggle="tab" title="Selesai" class="menu" data-id="7">
                                         <span class="round-tabs four" id="8">
                                           <i class="fa fa-check fa-3x" aria-hidden="true" style="padding-top:10px;"></i>
                                           <div style="size:12px;">Selesai</div>
                                         </span>
                                        </a>
                                     </li>
                                   </ul>
                                 </div>

                                 <div class="tab-content" id="isi">
                                   @for ($i = 0; $i < 8; $i++)
                                     <div class="tab-pane fade in {{($i == '0') ? 'active' : ''}}" id="{{$i}}">
                                       <h3 class="head text-center">Rincian Persetujuan</h3>
                                       <div class="narrow text-center" id="deskripsi">
                                         <table class="table table-bordered" id="user">
                                           <thead>
                                             <tr>
                                               <th width="50%" style="text-align:center;">Nama</th>
                                               <th width="50%" style="text-align:center;">Tanggal Transaksi</th>
                                             </tr>
                                           </thead>
                                           <tbody>
                                             <tr>
                                               <td id="name"></td>
                                               <td id="date"></td>
                                             </tr>
                                           </tbody>
                                         </table>
                                       </div>
                                    </div>
                                   @endfor
                                </div>

                            </div>
                          </div>
                        </div>

                      {{-- <table class="table table-bordered" id="detailStatus">
                      <thead>
                        <tr>
                          <th rowspan="2">No</th>
                          <th style="text-align:center;" colspan="2">Bagian</th>
                          <th style="text-align:center;" colspan="2">Kepala Instalasi</th>
                          <th style="text-align:center;" colspan="2">Direksi</th>
                          <th style="text-align:center;" colspan="2">PPK</th>
                          <th style="text-align:center;" colspan="2">Finance</th>
                        </tr>
                        <tr>
                          @for($i=1; $i<=5; $i++)
                            <th>Detail</th>
                            <th>Transaksi</th>
                          @endfor
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table> --}}
                </div>
              </div>
            </div>
          </div> <!--.\portlet-body-->
      </div><!--.\col-md-12-->
    </div><!--.\row shop-tracking-status-->
  </div><!--.\col-md-12 col-sm-12-->
  <div class="modal"></div>
@endsection

@section('note')

@endsection

@section('js')
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@endsection

@section('xjs')
  <script type="text/javascript">
  $(function(){
$('a[title]').tooltip();
});
  $('.filter').on('change', function(){
    var filter = $(this).val();
    var select = '<select class="form-control input-sm" name="search" class="filter">'+
                      '<option value="5">Semua</option>'+
                      '<option value="0">Menunggu Persetujuan</option>'+
                      '<option value="1">Telah disetujui Kepala Instalasi</option>'+
                      '<option value="2">Telah disetujui Direksi</option>'+
                      '<option value="3">Telah disetujui PPK</option>'+
                      '<option value="4">Telah disetujui Finance</option>'+
                  '</select>'

    if(filter == 'status'){
      $('div.search').html(select)
    }else if(filter == 'tanggal'){
      $('div.search').html('<input type="date" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
    }else{
      $('div.search').html('<input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">')
    }
  });
    // $(function() {
    //   $('table').dataTable({
    //     "order": [[ 3, "asc" ]]
    //   });
    // });

    function DetailCart(a) {
      $("#divTrackCart").removeClass('show');
      $("#divTrackCart").addClass('hide');
      var id_order = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/finance/detail_order')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_order": id_order,
                  },
                success: function (a) {
                  $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
                  $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
                  $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
                  $("#no_order").html("<b>No Pesanan :</b> #"+a.dataItem.no_order);
                  $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
                  var table = ""
                  var subtot = 0;
                  var totQty = 0;
                  var totAll = 0;
                  $.each(a.data, function(k, v){
                    totQty += v.qty;
                    totAll += ((v.price_gov*v.qty)+parseInt(v.price_shipment));
                    table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                    table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                    table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'">'+v.name+'</td>';
                    table += '<td style="text-align:right;">';
                        table += '<strong>';
                        table += '<span>'+addCommas(v.qty);
                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td style="text-align:right;">';
                        table += '<strong>';
                        table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                        table += '<span>Rp. '+addCommas(v.price_gov);
                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td style="text-align:right;">  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_shipment+'" class="total total_'+v.id_item+'">';
                          table += '<strong>';
                          table += '<span data-value="'+v.price_shipment+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas(v.price_shipment)+'</span>';
                          table += '</strong>';
                    table += '</td>';
                    table += '<td style="text-align:right;">  <input type="hidden" name="total['+v.id_item+']" value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total total_'+v.id_item+'">';
                          table += '<strong>';
                          table += '<span data-value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas(((v.price_gov*v.qty)+parseInt(v.price_shipment)))+'</span>';
                          table += '</strong>';
                    table += '</td>';
                    table += "</tr>";
                    sub = v.price_gov*v.qty;
                    subtot += sub;
                  });
                    table += '<td colspan="2" style="text-align:right;">';
                      table += "<strong>Total</strong>";
                    table += '</td>';
                    table += '<td style="text-align:right;">';
                      table += "<strong>"+addCommas(totQty)+"</strong>";
                    table += '</td>';
                    table += '<td colspan="3" style="text-align:right;">';
                    table += "<strong>Rp. "+addCommas(totAll)+"</strong>";
                    table += '</td>';

                  $("#detailCart tbody").html(table);
                  $(".Modal span#subtot").html(addCommas(subtot));
                  $("#divDetailCart").addClass('show');
                  $("html, body").animate({ scrollTop: $("#divDetailCart").prop("scrollHeight")}, 500)
                }
        })
      $("#divDetailCart").css('display','');
    }
    function TrackCart(a) {
      for (var i = 1; i < 8 ; i++) {
        $("span#"+i).removeClass('one');
        $("span#"+i).addClass('four');
      }
      $("ul#myTab li").removeClass('active')
      $("ul#myTab li#li_satu").attr('class', 'active')

      $("#divTrackCart").removeClass('hide');
      $("#divDetailCart").removeClass('show');
      $("#divDetailCart").addClass('hide');
      $("#divTrackCart").addClass('show');
      var id_order = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/finance/track_order')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_order": id_order,
                  },
                success: function (a) {
                  if(a.status == 1){
                    var numberStatus = 0;
                    $.each(a.data, function(k,v){
                      numberStatus = v.status_pesan;
                      if(v.username_catalog == null){
                         ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                         b = v.created_at;
                       }else{
                         b = v.created_at;
                         ab = v.username_catalog+"<br>"+v.mobile_user+"<br>"+v.email_user;
                       }
                      $("td#name").html(ab)
                      $("td#date").html(b)
                    })

                    for (var i = 0; i <= numberStatus ; i++) {
                      $("span#"+i).removeClass('four');
                      $("span#"+i).addClass('one');
                      console.log(i);
                    }

                    $('a.menu').on('click', function(){
                      statusdata = $(this).attr('data-id');
                      $.each(a.data, function(k,v){
                        mng_phone = (v.mobile_phone_manager != null) ? v.mobile_phone_manager : '-';
                        mng_email = (v.email_manager != null) ? v.email_manager : '-';

                        fnc_email = (v.email_finance != null) ? v.email_finance : '-';
                        fnc_phone = (v.mobile_phone_finance != null) ? v.mobile_phone_finance : '-';


                        console.log(mng_phone);
                        if(statusdata == 0){
                          if(v.username_catalog == null){
                             ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                             b = v.created_at;
                           }else{
                             b = v.created_at;
                             ab = v.username_catalog+"<br>"+v.mobile_user+"<br>"+v.email_user;
                           }
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 1){
                            if(v.datetime_supervisi == '0000-00-00 00:00:00'){
                              b = "-";
                              ab = "-";
                            }else{
                              b = (v.datetime_supervisi != '0000-00-00 00:00:00') ? v.datetime_supervisi : '-';
                              ab = v.name+"<br>"+v.mobile_phone+"<br>"+v.email;
                            }

                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 2){
                          ab = (v.name_manager != null) ? v.name_manager : '-<br>'+mng_email+"<br>"+mng_phone;
                          b = (v.datetime_manager != '0000-00-00 00:00:00') ? v.datetime_manager : '-'

                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 3){
                          ab = (v.name_directur != null) ? v.name_directur : '-';
                          b = (v.datetime_directur != '0000-00-00 00:00:00') ? v.datetime_directur : '-';
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 4){
                          ab = (v.driver != null) ? v.driver+"<br>"+v.car+"<br>"+v.car_no : '-<br>';
                          b = (v.date_do != '0000-00-00 00:00:00') ? v.date_do : '-';
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 5){
                          if(v.date_receive == null){
                            b = "-";
                            ab = "-";
                          }else{
                            b = (v.date_receive != null) ? v.date_receive : '-';
                            ab = (a.users.name != null) ? a.users.name : '-';
                          }


                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else if(statusdata == 6){
                          ab = (v.name_finance != null) ? v.name_finance : '-<br>'+fnc_email+"<br>"+fnc_phone
                          b = (v.datetime_finace != '0000-00-00 00:00:00') ? v.datetime_finace : '-'
                          $("td#name").html(ab)
                          $("td#date").html(b)
                        }
                        else{
                          $("td#name").html('-')
                          $("td#date").html('-')

                        }

                      })
                    })

                  }else{
                    alert("Data Error!")
                  }
                }
          });
      $("html, body").animate({ scrollTop: $('#divTrackCart').prop("scrollHeight")}, 500)
    }

    $(document).ready(function(){
      $("#dashboard_nav").removeAttr('href');
      $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/finance/dashboard')}}');
      $("#cartItem").removeAttr('href');
      $("a#cartItem").attr('href','{{URL::to('catalog/users/finance/cart')}}');
      $("#cart").css('display','none');
      $("#history").removeAttr('href');
      $("a#history").attr('href','{{URL::to('catalog/users/finance/history')}}');
      $("#approval").removeAttr('href');
      $("a#approval").attr('href','{{URL::to('catalog/users/finance/payment_approved')}}');
      $("a#approval").attr('text', 'Pembayaran');
      $("#tracking").removeAttr('href');
      $("a#tracking").attr('href','{{URL::to('catalog/users/finance/track')}}');
    });


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }</script>
@endsection
