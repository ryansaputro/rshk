@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <style media="screen">
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal {
      display: block;
  }
  </style>
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Ganti Password</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">

          </div>
          <br>
         <!--  <button href="" class="btn btn-primary btn-sm pull-right">
              <i class="fa fa-plus"></i>Tambah</button> -->



  <div class="container">
      <div class="row">
          <div class="col-md-10">
            <div class="panel panel-default">
              <div class="panel-heading">Change password</div>
                <div class="panel-body">
                    @if (session('error'))
                    <div class="alert alert-danger">
                    {{ session('error') }}
                    </div>
                    @endif
                        @if (session('success'))
                        <div class="alert alert-success">
                        {{ session('success') }}
                        </div>
                        @endif


          <form id="add" class="form-horizontal" method="POST" action="{{URL::to('catalog/users/finance/action_ganti_password')}}">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
              <label for="new-password" class="col-md-4 control-label">Current Password</label>
              <div class="col-md-6">
              <input id="current-password" type="password" class="form-control" name=" current_password" required>
              @if ($errors->has('current-password'))
              <span class="help-block">
              <strong>{{ $errors->first('current-password') }}</strong>
              </span>
              @endif
              </div>
              </div>
              <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
              <label for="new-password" class="col-md-4 control-label">New Password</label>
              <div class="col-md-6">
              <input id="new-password" type="password" class="form-control" name="new_password" required>
              @if ($errors->has('new-password'))
              <span class="help-block">
              <strong>{{ $errors->first('new-password') }}</strong>
              </span>
              @endif
              </div>
              </div>
              <div class="form-group">
              <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password</label>
              <div class="col-md-6">
              <input id="new-password-confirm" type="password" class="form-control" name="new_password_confirmation" required>
              </div>
              </div>
              <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
              <button type="submit" id="target"  class="btn btn-primary">
                   Ubah Password
              </button>
              </div>
              </div>
        </form>
      </div>
     </div>
    </div>
  </div>








        </div>
      </div>
    </div>

  </div>

<script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript">

</script>






@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script>
  $(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/finance/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/finance/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/finance/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/finance/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/finance/track')}}');
  $("li#history").attr("class","active");
});
</script>
 @endsection
