@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')
<img src="/assets/finance.jpg" alt="" style="width: 100% !important;height:300px;">
 <div class="centered"><h1 style="color: #64aed9; font-size: 45px;"><b></b></h1></div>

 <!--buttoon-->
   <div class="btn-group btn-group-justified">
       {{-- <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/cart')}}'">
               <span class="glyphicon glyphicon-shopping-cart"></span>
               <p>Cart</p>
           </button>
       </div> --}}
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/bast_waiting')}}'">
               <span class="glyphicon glyphicon-list-alt"></span>
         <p>BAST Menunggu</p>
       </button>
       </div>
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/payment_waiting')}}'">
               <span class="glyphicon glyphicon-cd"></span>
         <p>Pesanan Menunggu</p>
       </button>
       </div>
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/payment_approved')}}'">
               <span class="glyphicon glyphicon-thumbs-up"></span>
         <p>Pesanan Tersetujui</p>
       </button>
       </div>
       <div class="btn-group">
         <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/order_approved')}}'">
           {{-- <span class="glyphicon glyphicon-briefcase"></span> --}}
           <img src="{!! asset('su_vms/assets/users/company_image/midtrans.png') !!}" width="48px;" alt="">
           <p>Link Pembayaran</p>
         </button>
       </div>
       {{-- <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/order_approved')}}'">
               <span class="glyphicon glyphicon-briefcase"></span>
         <p>Budget</p>
       </button>
       </div> --}}
       <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/track')}}'">
               <span class="glyphicon glyphicon-bell"></span>
         <p>Lacak</p>
           </button>
       </div>
       <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/finance/history')}}'">
               <span class="glyphicon glyphicon-list-alt"></span>
         <p>Riwayat</p>
           </button>
       </div>
   </div>
<!--./buttoon-->
@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript">
var activeEl = '';
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});

$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/finance/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/finance/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/finance/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/finance/payment_approved')}}');
  $("a#approval").attr('text', 'Pembayaran');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/finance/track')}}');
});

</script>
@endsection
