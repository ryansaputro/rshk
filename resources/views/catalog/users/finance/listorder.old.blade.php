@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{$company->company}}
  </a>
@endsection

@section('sidebar')
  {{-- @include('catalog.users.layouts.sidebar') --}}
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="col-md-12">
        <h1>Lacak Pesanan</h1>
        <div class="portlet light">
          <div class="portlet-body">
            <table class="table table-bordered table striped">
              <thead>
                <tr>
                  <th>No Order</th>
                  <th>Date</th>
                  <th>Nominal</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(count($order) > 0)
                @foreach($order as $k => $v)
                  <tr>
                    <td>#{{ $v->no_order }}</td>
                    <td>
                      <span style="display: none;">

                      </span>
                      {{$v->datetime}}
                    </td>
                    <td>
                      Rp. {{ number_format($v->total, 0, ',', '.') }}
                    </td>
                    <td>
                      @php
                        $status = $v->status;
                      @endphp
                      <span style="display: none;">{{ $status }}</span>
                      @if ($status == 0)
                        <span class="label bg-grey" style="display: block;">waiting approval</span>
                      @elseif ($status == 1)
                        <span class="label bg-yellow" style="display: block;">approve suvervisor</span>
                      @elseif ($status == 2)
                        <span class="label bg-blue" style="display: block;">approve manager</span>
                      @elseif ($status == 3)
                        <span class="label bg-green" style="display: block;">approve directur</span>
                      @elseif ($status == 4)
                        <span class="label bg-purple" style="display: block;">approve finance</span>
                      @elseif ($status == 5)
                        <span class="label bg-purple" style="display: block;">success payment</span>
                      @else
                        <span class="label bg-red" style="display: block;">reject order</span>
                      @endif
                    </td>
                    <td>
                      <a style="float:left !important; color:white;" href="#" onclick="DetailCart(this)" class='btn btn-primary btn-xs' data-id="{{$v->id}}">Detail</a>
                    </td>
                  </tr>
                @endforeach
                @else
                  <tr>
                    <td colspan="5" style="text-align:center;">Tidak Ada Data</td>
                  </tr>
                @endif
              </tbody>
            </table>
            <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
              <form class="" action="{{URL::to('catalog/users/supervisi/approvement')}}" method="post">
                {{ csrf_field() }}
              <h1>  <center>Detail Pesanan</center></h1>
              <div class="row">
                <div class="col-xs-6">
                  <address>
                  <strong>Pembeli: </strong><br>
                    <p id="name_buyer">Nama : xx</p>
                    <p id="telephone_buyer">Telp: xx</p>
                    <p id="email_buyer">Email: xx</p>
                  </address>
                </div>
                <div class="col-xs-6 text-right">
                  <address>
                    <strong>Order:</strong><br>
                    <p id="no_order">No Order : yy</p>
                    <p id="date_order">Tanggal : yy</p>
                  </address>
                </div>

              </div>
              <table summary="Shopping cart" id="detailCart" class="table table-bordered table striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Item</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
              <div class="shopping-total Modal" style="width: 300px;">
                <ul>
                  <li>
                    <em>Total</em>
                    <input type="hidden" name="subtot" value="" id="input_subtot">
                    <strong class="price">
                      Rp
                      <span id="subtot"></span>
                    </strong>
                  </li>
                  {{-- <li>
                    <em>Shipping Cost <small>PPN 10%</small></em>

                    <strong class="price"><span>Rp </span>xxx</strong>
                  </li> --}}
                  {{-- <li class="shopping-total-price">
                    <em>Total</em>
                    <strong class="price"><span>Rp </span>xxx</strong>
                  </li> --}}
                </ul>
              </div>
            </form>
            </div>
          </div><!--/.portlet-body--->
        </div>
      </div>
    </div>
  </div>
  <div class="modal"></div>
@endsection

@section('note')

@endsection

@section('js')
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="/su_catalog/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@endsection

@section('xjs')
  <script type="text/javascript">
    // $(function() {
    //   $('table').dataTable({
    //     "order": [[ 3, "asc" ]]
    //   });
    // });

    function DetailCart(a) {
      var id_order = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/finance/detail_order')}}";
        $.ajax({
              type: "post",
              url: url,
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_order": id_order,
                  },
                success: function (a) {
                  $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
                  $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
                  $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
                  $("#no_order").html("<b>No Order :</b> #"+a.dataItem.no_order);
                  $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
                  var table = ""
                  var subtot = 0;
                  $.each(a.data, function(k, v){
                    table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                    table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                    table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'">'+v.name+'</td>';
                    table += '<td>';
                        table += '<strong>';
                        table += '<span>'+addCommas(v.qty);

                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td>';
                        table += '<strong>';
                        table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                        table += '<span>'+addCommas(v.price_gov);

                        table += '</span>';
                        table += '</strong>'
                    table += '</td>';
                    table += '<td>  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_gov*v.qty+'" class="total total_'+v.id_item+'">';
                          table += '<strong>';
                          table += '<span data-value="'+v.price_gov*v.qty+'" class="total_tag total_tag_'+v.id_item+'">'+addCommas(v.price_gov*v.qty)+'</span>';
                          table += '</strong>';
                    table += '</td>';
                    // table += '<td><div class="">';
                    //       table += '<label><input type="radio" class="radio-inline form-control input-sm" name="status['+v.id_item+']" checked value="1"><span class="outside"><span class="inside"></span></span>Setuju</label>';
                    //       table += '<label><input type="radio"class="radio-inline form-control input-sm" name="status['+v.id_item+']"  value="0"><span class="outside"><span class="inside"></span></span>Tolak</label>';
                    // table += '</div></td>';
                    // table += "<td><a href='#' style='float:left !important; color:white;' class='btn btn-danger btn-xs'>Tolak</a></td>";
                    table += "</tr>";
                    sub = v.price_gov*v.qty;
                    subtot += sub;
                  });

                  $("#detailCart tbody").html(table);
                  $(".Modal span#subtot").html(addCommas(subtot));
                  $("#detailCart").animate({ scrollTop: $('#detailCart').prop("scrollHeight")}, 1000);
                }
        })
      $("#divDetailCart").css('display','');
    }

    $(document).ready(function(){
      $("#dashboard_nav").removeAttr('href');
      $("#dashboard_nav").attr('href','{{URL::to('catalog/users/finance/dashboard')}}');
      $("#cartItem").removeAttr('href');
      $("#cartItem").attr('href','{{URL::to('catalog/users/finance/cart')}}');
      $("#cart").css('display','none');
      $("#history").removeAttr('href');
      $("#history").attr('href','{{URL::to('catalog/users/finance/history')}}');
      $("#approval").removeAttr('href');
      $("#approval").attr('href','{{URL::to('catalog/users/finance/payment_approved')}}');
      $("#approval").html('PEMBAYARAN APPROVE');
      $("#tracking").removeAttr('href');
      $("#tracking").attr('href','{{URL::to('catalog/users/finance/track')}}');
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }</script>
@endsection
