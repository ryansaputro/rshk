<style type="text/css">
/*!
 * Bootstrap v3.3.2 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*! normalize.css v3.0.2 | MIT License | git.io/normalize */
html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
}
body {
  margin: 0;
}
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
menu,
nav,
section,
summary {
  display: block;
}
audio,
canvas,
progress,
video {
  display: inline-block;
  vertical-align: baseline;
}
audio:not([controls]) {
  display: none;
  height: 0;
}
[hidden],
template {
  display: none;
}
a {
  background-color: transparent;
}
a:active,
a:hover {
  outline: 0;
}
abbr[title] {
  border-bottom: 1px dotted;
}
b,
strong {
  font-weight: bold;
}
dfn {
  font-style: italic;
}
h1 {
  margin: .67em 0;
  font-size: 2em;
}
mark {
  color: #000;
  background: #ff0;
}
small {
  font-size: 80%;
}
sub,
sup {
  position: relative;
  font-size: 75%;
  line-height: 0;
  vertical-align: baseline;
}
sup {
  top: -.5em;
}
sub {
  bottom: -.25em;
}
img {
  border: 0;
}
svg:not(:root) {
  overflow: hidden;
}
figure {
  margin: 1em 40px;
}
hr {
  height: 0;
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
}
pre {
  overflow: auto;
}
code,
kbd,
pre,
samp {
  font-family: monospace, monospace;
  font-size: 1em;
}
button,
input,
optgroup,
select,
textarea {
  margin: 0;
  font: inherit;
  color: inherit;
}
button {
  overflow: visible;
}
button,
select {
  text-transform: none;
}
button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  cursor: pointer;
}
button[disabled],
html input[disabled] {
  cursor: default;
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0;
}
input {
  line-height: normal;
}
input[type="checkbox"],
input[type="radio"] {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 0;
}
input[type="number"]::-webkit-inner-spin-button,
input[type="number"]::-webkit-outer-spin-button {
  height: auto;
}
input[type="search"] {
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
  -webkit-appearance: textfield;
}
input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}
fieldset {
  padding: .35em .625em .75em;
  margin: 0 2px;
  border: 1px solid #c0c0c0;
}
legend {
  padding: 0;
  border: 0;
}
textarea {
  overflow: auto;
}
optgroup {
  font-weight: bold;
}
table {
  border-spacing: 0;
  border-collapse: collapse;
}
td,
th {
  padding: 0;
}
/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
@media print {
  *,
  *:before,
  *:after {
    color: #000 !important;
    text-shadow: none !important;
    background: transparent !important;
    -webkit-box-shadow: none !important;
            box-shadow: none !important;
  }
  a,
  a:visited {
    text-decoration: underline;
  }
  a[href]:after {
    content: " (" attr(href) ")";
  }
  abbr[title]:after {
    content: " (" attr(title) ")";
  }
  a[href^="#"]:after,
  a[href^="javascript:"]:after {
    content: "";
  }
  pre,
  blockquote {
    border: 1px solid #999;

    page-break-inside: avoid;
  }
  thead {
    display: table-header-group;
  }
  tr,
  img {
    page-break-inside: avoid;
  }
  img {
    max-width: 100% !important;
  }
  p,
  h2,
  h3 {
    orphans: 3;
    widows: 3;
  }
  h2,
  h3 {
    page-break-after: avoid;
  }
  select {
    background: #ffffff !important;
  }
  .navbar {
    display: none;
  }
  .btn > .caret,
  .dropup > .btn > .caret {
    border-top-color: #000 !important;
  }
  .label {
    border: 1px solid #000;
  }
  .table {
    border-collapse: collapse !important;
  }
  .table td,
  .table th {
    background-color: #ffffff !important;
  }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ddd !important;
  }
}
@font-face {
  font-family: 'Glyphicons Halflings';

  src: url('../fonts/glyphicons-halflings-regular.eot');
  src: url('../fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('../fonts/glyphicons-halflings-regular.woff') format('woff'), url('../fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
}
html {
  font-size: 10px;

  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
  background-color: #ffffff;
}
input,
button,
select,
textarea {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
}
a {
  color: #337ab7;
  text-decoration: none;
}
a:hover,
a:focus {
  color: #23527c;
  text-decoration: underline;
}
a:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
figure {
  margin: 0;
}
img {
  vertical-align: middle;
}
.img-responsive,
.thumbnail > img,
.thumbnail a > img,
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  display: block;
  max-width: 100%;
  height: auto;
}
.img-rounded {
  border-radius: 6px;
}
.img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #ffffff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
       -o-transition: all .2s ease-in-out;
          transition: all .2s ease-in-out;
}
.img-circle {
  border-radius: 50%;
}
hr {
  margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #eee;
}
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0, 0, 0, 0);
  border: 0;
}
.sr-only-focusable:active,
.sr-only-focusable:focus {
  position: static;
  width: auto;
  height: auto;
  margin: 0;
  overflow: visible;
  clip: auto;
}
h1,
h2,
h3,
h4,
h5,
h6,
.h1,
.h2,
.h3,
.h4,
.h5,
.h6 {
  font-family: inherit;
  font-weight: 500;
  /* line-height: 1.1; */
  color: inherit;
}
h1 small,
h2 small,
h3 small,
h4 small,
h5 small,
h6 small,
.h1 small,
.h2 small,
.h3 small,
.h4 small,
.h5 small,
.h6 small,
h1 .small,
h2 .small,
h3 .small,
h4 .small,
h5 .small,
h6 .small,
.h1 .small,
.h2 .small,
.h3 .small,
.h4 .small,
.h5 .small,
.h6 .small {
  font-weight: normal;
  line-height: 1;
  color: #777;
}
h1,
.h1,
h2,
.h2,
h3,
.h3 {
  margin-top: 0px;
  margin-bottom: 0px;
}
h1 small,
.h1 small,
h2 small,
.h2 small,
h3 small,
.h3 small,
h1 .small,
.h1 .small,
h2 .small,
.h2 .small,
h3 .small,
.h3 .small {
  font-size: 65%;
}
h4,
.h4,
h5,
.h5,
h6,
.h6 {
  margin-top: 10px;
  margin-bottom: 10px;
}
h4 small,
.h4 small,
h5 small,
.h5 small,
h6 small,
.h6 small,
h4 .small,
.h4 .small,
h5 .small,
.h5 .small,
h6 .small,
.h6 .small {
  font-size: 75%;
}
h1,
.h1 {
  font-size: 36px;
}
h2,
.h2 {
  font-size: 30px;
}
h3,
.h3 {
  font-size: 24px;
}
h4,
.h4 {
  font-size: 18px;
}
h5,
.h5 {
  font-size: 14px;
}
h6,
.h6 {
  font-size: 12px;
}
p {
  margin: 0 0 10px;
}

@media (min-width: 768px) {
  .lead {
    font-size: 21px;
  }
}
small,
.small {
  font-size: 85%;
}

ul,
ol {
  margin-top: 0;
  margin-bottom: 10px;
}
ul ul,
ol ul,
ul ol,
ol ol {
  margin-bottom: 0;
}
.list-unstyled {
  padding-left: 0;
  list-style: none;
}
.list-inline {
  padding-left: 0;
  margin-left: -5px;
  list-style: none;
}
.list-inline > li {
  display: inline-block;
  padding-right: 5px;
  padding-left: 5px;
}
dl {
  margin-top: 0;
  margin-bottom: 20px;
}
dt,
dd {
  line-height: 1.42857143;
}
dt {
  font-weight: bold;
}
dd {
  margin-left: 0;
}

.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
@media (min-width: 768px) {
  .container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .container {
    width: 1170px;
  }
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.row {
  margin-right: -15px;
  margin-left: -15px;
}

  .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
    float: left;
  }
  .col-sm-12 {
    width: 100%;
  }
  .col-sm-11 {
    width: 91.66666667%;
  }
  .col-sm-10 {
    width: 83.33333333%;
  }
  .col-sm-9 {
    width: 75%;
  }
  .col-sm-8 {
    width: 66.66666667%;
  }
  .col-sm-7 {
    width: 58.33333333%;
  }
  .col-sm-6 {
    width: 50%;
  }
  .col-sm-5 {
    width: 41.66666667%;
  }
  .col-sm-4 {
    width: 33.33333333%;
  }
  .col-sm-3 {
    width: 25%;
  }
  .col-sm-2 {
    width: 16.66666667%;
  }
  .col-sm-1 {
    width: 8.33333333%;
  }

table {
  background-color: transparent;
}
caption {
  padding-top: 8px;
  padding-bottom: 8px;
  color: #777;
  text-align: left;
}
th {
  text-align: left;
}
.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 20px;
}
.table > thead > tr > th,
.table > tbody > tr > th,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > tbody > tr > td,
.table > tfoot > tr > td {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}
.table > thead > tr > th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd;
}
.table > caption + thead > tr:first-child > th,
.table > colgroup + thead > tr:first-child > th,
.table > thead:first-child > tr:first-child > th,
.table > caption + thead > tr:first-child > td,
.table > colgroup + thead > tr:first-child > td,
.table > thead:first-child > tr:first-child > td {
  border-top: 0;
}
.table > tbody + tbody {
  border-top: 2px solid #ddd;
}
.table .table {
  background-color: #ffffff;
}
.table-condensed > thead > tr > th,
.table-condensed > tbody > tr > th,
.table-condensed > tfoot > tr > th,
.table-condensed > thead > tr > td,
.table-condensed > tbody > tr > td,
.table-condensed > tfoot > tr > td {
  padding: 5px;
}
.table-bordered {
  border: 1px solid #ddd;
}
.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td {
  border: 1px solid #ddd;
}
.table-bordered > thead > tr > th,
.table-bordered > thead > tr > td {
  border-bottom-width: 2px;
}
.table-striped > tbody > tr:nth-of-type(odd) {
  background-color: #f9f9f9;
}
.table-hover > tbody > tr:hover {
  background-color: #f5f5f5;
}
table col[class*="col-"] {
  position: static;
  display: table-column;
  float: none;
}
table td[class*="col-"],
table th[class*="col-"] {
  position: static;
  display: table-cell;
  float: none;
}
.table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
.form-control-static {
  padding-top: 7px;
  padding-bottom: 7px;
  margin-bottom: 0;
}
.form-control-static.input-lg,
.form-control-static.input-sm {
  padding-right: 0;
  padding-left: 0;
}
.input-sm {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.input-sm {
  height: 30px;
  line-height: 30px;
}
textarea.input-sm,
select[multiple].input-sm {
  height: auto;
}
.form-group-sm .form-control {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.form-group-sm .form-control {
  height: 30px;
  line-height: 30px;
}
textarea.form-group-sm .form-control,
select[multiple].form-group-sm .form-control {
  height: auto;
}
.form-group-sm .form-control-static {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.input-lg {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.input-lg {
  height: 46px;
  line-height: 46px;
}
textarea.input-lg,
select[multiple].input-lg {
  height: auto;
}
.form-group-lg .form-control {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.form-group-lg .form-control {
  height: 46px;
  line-height: 46px;
}
textarea.form-group-lg .form-control,
select[multiple].form-group-lg .form-control {
  height: auto;
}
.form-group-lg .form-control-static {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}

@media (min-width: 768px) {
  .form-inline .form-group {
    display: inline-block;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .form-inline .form-control {
    display: inline-block;
    width: auto;
    vertical-align: middle;
  }
  .form-inline .form-control-static {
    display: inline-block;
  }
  .form-inline .input-group {
    display: inline-table;
    vertical-align: middle;
  }
  .form-inline .input-group .input-group-addon,
  .form-inline .input-group .input-group-btn,
  .form-inline .input-group .form-control {
    width: auto;
  }
  .form-inline .input-group > .form-control {
    width: 100%;
  }
  .form-inline .control-label {
    margin-bottom: 0;
    vertical-align: middle;
  }
  .form-inline .radio,
  .form-inline .checkbox {
    display: inline-block;
    margin-top: 0;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .form-inline .radio label,
  .form-inline .checkbox label {
    padding-left: 0;
  }
  .form-inline .radio input[type="radio"],
  .form-inline .checkbox input[type="checkbox"] {
    position: relative;
    margin-left: 0;
  }
  .form-inline .has-feedback .form-control-feedback {
    top: 0;
  }
}
.form-horizontal .radio,
.form-horizontal .checkbox,
.form-horizontal .radio-inline,
.form-horizontal .checkbox-inline {
  padding-top: 7px;
  margin-top: 0;
  margin-bottom: 0;
}
.form-horizontal .radio,
.form-horizontal .checkbox {
  min-height: 27px;
}
.form-horizontal .form-group {
  margin-right: -15px;
  margin-left: -15px;
}
@media (min-width: 768px) {
  .form-horizontal .control-label {
    padding-top: 7px;
    margin-bottom: 0;
    text-align: right;
  }
}
.form-horizontal .has-feedback .form-control-feedback {
  right: 15px;
}
.input-group {
  position: relative;
  display: table;
  border-collapse: separate;
}
.input-group[class*="col-"] {
  float: none;
  padding-right: 0;
  padding-left: 0;
}
.input-group .form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
}
.input-group-lg > .form-control,
.input-group-lg > .input-group-addon,
.input-group-lg > .input-group-btn > .btn {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.input-group-lg > .form-control,
select.input-group-lg > .input-group-addon,
select.input-group-lg > .input-group-btn > .btn {
  height: 46px;
  line-height: 46px;
}
textarea.input-group-lg > .form-control,
textarea.input-group-lg > .input-group-addon,
textarea.input-group-lg > .input-group-btn > .btn,
select[multiple].input-group-lg > .form-control,
select[multiple].input-group-lg > .input-group-addon,
select[multiple].input-group-lg > .input-group-btn > .btn {
  height: auto;
}
.input-group-sm > .form-control,
.input-group-sm > .input-group-addon,
.input-group-sm > .input-group-btn > .btn {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.input-group-sm > .form-control,
select.input-group-sm > .input-group-addon,
select.input-group-sm > .input-group-btn > .btn {
  height: 30px;
  line-height: 30px;
}
textarea.input-group-sm > .form-control,
textarea.input-group-sm > .input-group-addon,
textarea.input-group-sm > .input-group-btn > .btn,
select[multiple].input-group-sm > .form-control,
select[multiple].input-group-sm > .input-group-addon,
select[multiple].input-group-sm > .input-group-btn > .btn {
  height: auto;
}
.input-group-addon,
.input-group-btn,
.input-group .form-control {
  display: table-cell;
}
.input-group-addon:not(:first-child):not(:last-child),
.input-group-btn:not(:first-child):not(:last-child),
.input-group .form-control:not(:first-child):not(:last-child) {
  border-radius: 0;
}
.input-group-addon,
.input-group-btn {
  width: 1%;
  white-space: nowrap;
  vertical-align: middle;
}
.input-group-addon {
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1;
  color: #000000;
  text-align: center;
  background-color: #eee;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.input-group-addon.input-sm {
  padding: 5px 10px;
  font-size: 12px;
  border-radius: 3px;
}
.input-group-addon.input-lg {
  padding: 10px 16px;
  font-size: 18px;
  border-radius: 6px;
}
.input-group-addon input[type="radio"],
.input-group-addon input[type="checkbox"] {
  margin-top: 0;
}
.input-group .form-control:first-child,
.input-group-addon:first-child,
.input-group-btn:first-child > .btn,
.input-group-btn:first-child > .btn-group > .btn,
.input-group-btn:first-child > .dropdown-toggle,
.input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle),
.input-group-btn:last-child > .btn-group:not(:last-child) > .btn {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.input-group-addon:first-child {
  border-right: 0;
}
.input-group .form-control:last-child,
.input-group-addon:last-child,
.input-group-btn:last-child > .btn,
.input-group-btn:last-child > .btn-group > .btn,
.input-group-btn:last-child > .dropdown-toggle,
.input-group-btn:first-child > .btn:not(:first-child),
.input-group-btn:first-child > .btn-group:not(:first-child) > .btn {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.input-group-addon:last-child {
  border-left: 0;
}
.input-group-btn {
  position: relative;
  font-size: 0;
  white-space: nowrap;
}
.input-group-btn > .btn {
  position: relative;
}
.input-group-btn > .btn + .btn {
  margin-left: -1px;
}
.input-group-btn > .btn:hover,
.input-group-btn > .btn:focus,
.input-group-btn > .btn:active {
  z-index: 2;
}
.input-group-btn:first-child > .btn,
.input-group-btn:first-child > .btn-group {
  margin-right: -1px;
}
.input-group-btn:last-child > .btn,
.input-group-btn:last-child > .btn-group {
  margin-left: -1px;
}


/*# sourceMappingURL=bootstrap.css.map */
h1, h2, h3, h4, h5, h6{
  line-height: 10px !important;
}
p{
  line-height: 5px;
}
hr{
  line-height: 0px;
  border-top: 3px double #8c8b8b;
}
.page-break {
    page-break-after: always;
}
</style>

<center>
  <h3> <strong>KEMENTRIAN KESEHATAN</strong> </h3>
  <h4> <strong>DIREKTORAT JENDERAL PELAYANAN KESEHATAN </strong></h4>
  <h4> <strong>RS JANTUNG DAN P.D. HARAPAN KITA </strong></h4>
  <p> <strong>Jalan Let. Jend. S. Parman Kav. 87 Slipi Jakarta 112420</strong> </p>
  <p> <strong>Telepon 021.5684085 - 093, 5681111, Faksimile 5684230</strong> </p>
  <p> Surat Elektronik : info@pjnhk.go.id</p>
  <p> <a href="http://www.pjnhk.go.id">http://www.pjnhk.go.id</a> </p>
</center>
<hr>
@php
  $time = strtotime($data->datetime);
  $function =  Fungsi::MonthIndonesia();
  $functionDay =  Fungsi::DayIndonesia();
  $times = $time + strtotime("+2 months");
  $aritdate =  date('m Y', strtotime($data->datetime. "+60 days"));
  $NextDate = explode(' ', $aritdate);
@endphp

<center>
  <h4><b><u>SURAT PERINTAH KERJA (SPK)</u><b></h4>
  <h4><b>Nomor : {{$data->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</b></h4>
  <br>
  <h5><b>PEKERJAAN :</b></h5>
  <h5><b>Pengadaan Belanja Modal Alat-alat Kesehatan</b></h5>
  <h5><b>RS JANTUNG DAN P.D. HARAPAN KITA</b></h5>
</center>
<br>
  <p>Pada hari ini <b>{{$functionDay[date('l', $time)]}}</b> Tanggal <b>{{date('d', $time)}}</b> bulan <b>{{$function[date('m', $time)]}}</b> tahun <b>{{date('Y', $time)}}</b> kami yang bertanda tangan dibawah ini :</p>
<ol>
  <li>
    <table class="" style="width:100%;">
      <tr>
        <td style="width:20%;">Nama</td>
        <td style="width:2%;">:</td>
        <td>{{$directur->name}}</td>
      </tr>
      <tr>
        <td>Pangkat/NIP</td>
        <td>:</td>
        <td>{{$directur->nik}}</td>
      </tr>
      <tr>
        <td>Jabatan</td>
        <td>:</td>
        <td>Pejabat Pembuat Komitmen (PPK) </td>
      </tr>
      <tr>
        <td>Instansi</td>
        <td>:</td>
        <td>{{$directur->company}}</td>
      </tr>
      <tr>
        <td>NPWP</td>
        <td>:</td>
        <td>{{$directur->npwp}}</td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>{{$directur->address}}</td>
      </tr>
    </table>
  </li>
  <li>
    <table class="" style="width:100%;">
    <tr>
      <td style="width:20%;">Nama</td>
      <td style="width:2%;">:</td>
      <td>{{$vendorDirectur->name}}</td>
    </tr>
    <tr>
      <td>Jabatan</td>
      <td>:</td>
      <td>Direktur <b>{{$vendorDetail->vendor_name}}</b> </td>
    </tr>
    <tr>
      <td>Nama Perusahaan</td>
      <td>:</td>
      <td>{{$vendorDetail->vendor_name}}</td>
    </tr>
    <tr>
      <td>NPWP</td>
      <td>:</td>
      <td>{{$vendorDetail->npwp}}</td>
    </tr>
    <tr>
      <td>No. Rek Bank</td>
      <td>:</td>
      <td>{{$vendorDetail->no_rek}} ({{$vendorDetail->bank}})</td>
    </tr>
    <tr>
      <td>Alamat</td>
      <td>:</td>
      <td>{{$vendorDetail->address}}</td>
    </tr>
  </table>
</li>
</ol>
<br>
<p style="text-indent: 40px;">Bertindak untuk dan atas nama perusahaan <b>{{$vendorDetail->vendor_name}}</b> selanjutnya disebut <b><i>PIHAK KEDUA.</i></b></p>
<br>
<p>Berdasarkan :</p>
<p style="text-indent: 40px;">a. Paket e Purchasing (Sistem Pengadaan secara Elektronik) dengan NOMOR PESANAN <b>{{$data->no_order}},  <br><br><br><br> Tanggal {{date('d', $time)}} {{$function[date('m', $time)]}} {{(date('Y', $time))}}</b> maka <b>PIHAK PERTAMA</b> Menerbitkan Surat Perintah Kerja (SPK), dengan Ketentuan  <br><br><br><br>sebagai berikut : </p>
<ol>
  <li>Lingkup Pekerjaan :
    <table summary="Shopping cart" id="detailCart" class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>Item</th>
          <th>Qty</th>
          <th>Harga</th>
          <th>Biaya Kirim</th>
          <th>Sub Total</th>
        </tr>
      </thead>
      @php
      $totQty=0;
      $totPrice=0;
      @endphp
      @foreach($dataOrder as $k => $v)
        @php
        $totQty += $v->qty;
        $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
        $totPrice += $tot;
        @endphp
        <tr>
          <td>{{$k+1}}</td>
          <td>{{$v->datetime}}</td>
          <td>{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
          <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}}</td>
          <td style="text-align:right;">Rp. {{number_format($v->price_gov, 0, ',', '.')}}</td>
          <td style="text-align:right;">Rp. {{number_format($v->price_shipment, 0, ',', '.')}}</td>
          <td style="text-align:right;">Rp. {{number_format($tot, 0, ',', '.')}}</td>
        </tr>
      @endforeach
      <tr>
        <td colspan="3" style="text-align:right;"><strong>Total</strong></td>
        <td style="text-align:right;">{{number_format($totQty, 0, ',', '.')}}</td>
        <td style="text-align:right;" colspan="3"><strong>Rp.{{number_format($totPrice, 0, ',', '.')}}</strong></td>
      </tr>
      <tbody>
      </tbody>
    </table>
  </li>
  <li>Pembayaran dengan sumber Dana APBD Perubahan  bla bla.</li>
  <li>Jangka Waktu Pelaksanaan 60 (Enam puluh) hari kalender, tanggal mulai kerja yaitu mulai {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}} sampai dengan {{date('d', $time)}} {{$function[$NextDate[0]]}} {{$NextDate[1]}}.</li>
  <li>Instruksi kepada <b> <i>PIHAK KEDUA</i> :</b>
    <ul>
      <li>Pembayarannya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini dan dibuktikan dengan Berita Acara Serah Terima Pekerjaan / barang.</li>
      <li>Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan karena kesalahan/kelalaian penyedia, maka penyedia berkewajiban untuk membayar denda kepada Kas Daerah 1/1000 (seperseribu) dari nilai barang yang belum tersuplai sebelum PPN setiap hari kalender keterlambatan.</li>
      <li>Selain tunduk kepada ketentuan dalam SPK ini, penyedia berkewajiban untuk mematuhi standar ketentuan dan syarat umum SPK ini.</li>
      <li>SPK ini mulai berlaku efektif terhitung sejak tanggal diterbitkannya SP dan Penyelesaian keseluruhan pekerjaan sebagaimana diatur dalam SPK ini.</li>
    </ul>
  </li>
</ol>
<br>
<p>
  Demikianlah Surat Perintah Kerja ini dibuat dengan sebenernya pada hari ini, tanggal dan bulan tersebut diatas <br><br><br><br> dalam rangkap 4 (empat),
  2 lembar dibubuhi materi yang berlaku dan mempunyai kekuatan hukum yang sama.
</p>

<br><br><br>
<table style="width:100%;">
  <tr>
    <td style="text-align:center;">Untuk dan atas nama<br><b>RS JANTUNG DAN P.D. HARAPAN KITA</b></td>
    <td style="text-align:center;">Untuk dan atas nama Penyedia <br><b>{{$vendorDetail->vendor_name}}</b> </td>
  </tr>
  <tr>
    <td style="text-align:center;">Petugas Pembuat Komitmen (PPK) <br><br><br> </td>
    <td style="text-align:center;"><br><br><br></td>
  </tr>
  <tr>
    <td style="text-align:center;"> <img src="http://puslit.mercubuana.ac.id/wp-content/uploads/2015/05/ttd-anik-cap.jpg" alt="" style="width:200px;"> </td>
    <td style="text-align:center;"> <img src="http://puslit.mercubuana.ac.id/wp-content/uploads/2015/05/ttd-anik-cap.jpg" alt="" style="width:200px;"> </td>
  </tr>
  <tr>
    <td style="text-align:center;"> <b><u>{{$directur->name}}</u></b> </td>
    <td style="text-align:center;"> <b><u>{{$vendorDirectur->name}}</u></b> </td>
  </tr>
  <tr>
    <td style="text-align:center;"> {{$directur->nik}} </td>
    <td style="text-align:center;"> {{$vendorDirectur->nik}} </td>
  </tr>
</table>
