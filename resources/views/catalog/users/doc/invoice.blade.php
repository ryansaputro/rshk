@extends('catalog.users.layouts.app_doc')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-body" id='DivIdToPrint'>
          <div class="row">
            <div class="col-xs-2">
              <img src="/assets/logo.png" class="logoRshk" style="width:75%;" alt="">
            </div>
            <div class="col-xs-8">
              <center>
                <h4>
                  <b>INVOICE</b> <br>
                </h4>
                  R.S. JANTUNG DAN PEMBULUH DARAH HARAPAN KITA <br>
                  Jln.Let.Jen S. Parman Kav 87 Slipi Jakarta Barat,<br> Telpon 5684086 - 093 Faksimile 5684230 <br>
                  <h4>
                    <br>
                  </h4>
               </center>
            </div>

            <div class="col-xs-12">
              <hr style="border-top: 3px double #8c8b8b;">
            </div>

          </div><!--/.row-->
          <div class="row">
                  <div class="col-xs-12">
              		<div class="invoice-title">
              			<h2>Invoice</h2><h3 class="pull-right">Pesanan # {{$data->no_order}}</h3>
              		</div>
              		<hr>
              		<div class="row">
              			<div class="col-xs-6">
              				<address>
              				<strong>Ditagihkan Kepada:</strong><br>
              					Rumah Sakit Harapan Kita<br>
              					Jl. Bla Bla
              				</address>
              			</div>
              			<div class="col-xs-6 text-right">
              				<address>
                  			<strong>Dikirim Oleh:</strong><br>
              				  {{$vendor->vendor_name}}<br>
              					{{$vendor->address}}<br>
              					{{$list_kota[$list_propinsi[$vendor->province]][$vendor->city]}} - {{$vendor->post_code}}<br>
              				</address>
              			</div>
              		</div>
              		<div class="row">
              			<div class="col-xs-6">
                      <address>
              					<strong>No SP:</strong><br>
                        @php
                          $time = strtotime($data->datetime);
                          $function =  Fungsi::MonthIndonesia();
                        @endphp
                        <font style="text-transform:uppercase;">
                          {{$data->no_medik != NULL ? $data->no_medik : $data->no_non_medik}}/{{date('m', $time)}}/{{$data->name_category}} E-catalog{{date('Y', $time)}} <br>
                        </font>
              				</address>
                      <address>
              					<strong>Tanggal Pesanan:</strong><br>
                        @php
                          $time = strtotime($data->datetime);
                          $function =  Fungsi::MonthIndonesia();
                        @endphp
                        {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}
              					<br><br>
              				</address>
              			</div>
              			<div class="col-xs-6 text-right">
                      <address>
              					<strong>Bank:</strong><br>
              					{{$vendor->bank}}<br>
              					{{$vendor->no_rek}}
              				</address>
              			</div>
              		</div>
              	</div>
              </div>

              <div class="row">
              	<div class="col-md-12">
              		<div class="panel panel-default">
              			<div class="panel-heading">
              				<h3 class="panel-title"><strong>Ringkasan Pesanan</strong></h3>
              			</div>
              			<div class="panel-body">
              				<div class="table-responsive">
              					<table class="table table-condensed">
              						<thead>
                                <tr>
                                <td>No</td>
                  							<td><strong>Item</strong></td>
                  							<td class="text-center"><strong>Qty</strong></td>
                  							<td class="text-right"><strong>Harga</strong></td>
                  							<td class="text-right"><strong>Biaya Kirim</strong></td>
                  							<td class="text-right"><strong>Sub Total</strong></td>
                                </tr>
              						</thead>
              						<tbody>
                            @php
                              $totQty=0;
                              $totPrice=0;
                            @endphp
                            @foreach($dataItem as $k => $v)
                              @php
                                $totQty += $v->qty;
                                $tot = ($v->price_gov*$v->qty)+$v->price_shipment;
                                $totPrice += $tot;
                              @endphp
                              <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$v->code}}<br>{{$v->name}}<br>{{$v->merk}}</td>
                                <td style="text-align:right;">{{number_format($v->qty, 0, ',', '.')}} {{$v->unit_name}}</td>
                                <td style="text-align:right;">Rp. {{number_format($v->price_gov, 0, ',', '.')}}</td>
                                <td style="text-align:right;">Rp. {{number_format($v->price_shipment, 0, ',', '.')}}</td>
                                <td style="text-align:right;">Rp. {{number_format($tot, 0, ',', '.')}}</td>
                              </tr>
                            @endforeach
                            <tr>
                              <td colspan="2" style="text-align:right;"><strong>Total</strong></td>
                              <td style="text-align:right;">{{number_format($totQty, 0, ',', '.')}}</td>
                              <td style="text-align:right;" colspan="3"><strong>Rp.{{number_format($totPrice, 0, ',', '.')}}</strong></td>
                            </tr>

              						</tbody>
              					</table>
                        <p>Terbilang : <label for="" style="text-transform: uppercase;">{{Terbilang::make($totPrice, ' rupiah', ' ')}}</label></p>

              				</div>
              			</div>
              		</div>
              	</div>
            </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved.
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
