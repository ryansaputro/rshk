@extends('catalog.users.layouts.app_doc')

@section('title')
  Catalog
@endsection

@section('css')

@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  @php
  $functionDay =  Fungsi::DayIndonesia();
  $times = strtotime($Bap->datetime);
  $time = strtotime($Bap->datetime_order);
  $function =  Fungsi::MonthIndonesia();
  $functionDay =  Fungsi::DayIndonesia();
  $RomanMonth =  Fungsi::MonthRoman();
  $bulanWord =  Fungsi::MonthIndonesia();

  if($data->total >= 200000000){
    $aritdate =  date('Y-m-d', strtotime($data->datetime. "+14 days"));
    $NextDate = explode(' ', $aritdate);

  }else{
    $aritdate =  date('Y-m-d', strtotime($data->datetime. "+7 days"));
    $NextDate = explode(' ', $aritdate);
  }

  $time = strtotime($aritdate);
  $newformat = date('Y-m-d',$time);
  @endphp
  <div class="row" id='DivIdToPrint'>
    <div class="col-md-12">
      <div class="portlet light">
        <div class="row">
          <div class="col-xs-2">
            <img src="/assets/logo.png" class="logoRshk" style="width:75%;" alt="">
          </div>
          <div class="col-xs-8">
            <center>
              <br>
              <h4>
                <b>BERITA ACARA PEMBAYARAN</b> <br>
              </h4>
                R.S. JANTUNG DAN PEMBULUH DARAH HARAPAN KITA <br>
                Jln.Let.Jen S. Parman Kav 87 Slipi Jakarta Barat,<br> Telpon 5684086 - 093 Faksimile 5684230 <br>
                Nomor : {{$Bap->no_bap}}
             </center>
          </div>

          <div class="col-xs-12">
            <hr style="border-top: 3px double #8c8b8b;">
          </div>

        </div><!--/.row-->
        <div class="portlet-body">
          <div class="row">

                <div class="col-xs-12">
                  Pada Hari ini, {{$functionDay[date('l', $times)]}} Tanggal
                  <font style="text-transform:capitalize;">
                    {{Terbilang::make(date('d', $times), '', ' ')}} {{$bulanWord[date('m', $times)]}}
                    {{Terbilang::make(date('Y', $times))}}
                  </font>, kami yang bertanda tangan di bawah ini menyatakan bahwa Pengadaan Barang/Jasa di bawah ini dapat dibayarkan berdasarkan pada rincian berikut :
                  <br>
                  <br>
                </div>

                <div class="col-xs-3">Pengadaan</div>
                <div class="col-xs-1">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;">
                  {{$Bap->procurement}}
                </div>

                <div class="col-xs-3">Pelaksana</div>
                <div class="col-xs-1">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;">
                  {{$Bap->vendor_name}}
                </div>

                <div class="col-xs-3">Nomor Kontrak</div>
                <div class="col-xs-1">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;">
                  {{$Bap->no_contract}}
                </div>

                <div class="col-xs-3" style="margin-top:30px;">Nomor SP</div>
                <div class="col-xs-1"  style="margin-top:30px;">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;margin-top:30px;">
                  <font style="text-transform:uppercase;">{{$Bap->no_medik != NULL ? $Bap->no_medik : $Bap->no_non_medik}}/{{date('m', $time)}}/{{$Bap->name_category}} E-catalog{{date('Y', $time)}}</font>
                </div>

                <div class="col-xs-3">Nilai SP</div>
                <div class="col-xs-1">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;">
                  <font style="text-transform:uppercase;"> Rp.{{number_format($data->total, 0, ',','.')}}</font>
                </div>

                <div class="col-xs-3">Nomor BAST</div>
                <div class="col-xs-1">:</div>
                <div class="col-xs-8" style="text-transform:uppercase;">
                  @foreach ($bast as $k => $v)
                    @php
                    $timesDO = strtotime($v->datetime_kirim);
                    $times = strtotime($v->datetime);
                    $new_date = date('Y-m-d', $timesDO);
                    if($new_date > $newformat){
                      $date1 = new DateTime($new_date);
                      $date2 = new DateTime($newformat);
                      $telat =  "(".$date2->diff($date1)->format('%R%a hari').")";
                    }else{
                      $telat =  "";
                    }
                    @endphp
                    <font style="margin-bottom:10px; display: block;margin: 0px 0;">
                      <a style="color: {{$new_date > $newformat ? 'red' : ''}} !important;" target="_blank" href="{{URL::to('catalog/users/directur/bast/preview/'.md5($v->id))}}">{{$v->no_bast}}/BAST/{{$v->name_category}}/{{$userCode->kode_instalasi}}/RSJPDHK/{{$RomanMonth[date('m', $times)]}}/{{date('Y', $timesDO)}}
                        {{$telat}}</a>
                    </font>
                  @endforeach
                </div>

                  <div class="col-xs-12">
                    <strong style="text-transform:uppercase; ">
                      uraian pemabayaran :
                    </strong>
                  </div>
                  <div class="col-xs-12">

                    <div class="col-xs-8">
                      1. Harga berdasarkan SP sebesar
                    </div>
                    <div class="col-xs-1">
                      :
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                      Rp.{{number_format($data->total, 0, ',','.')}}
                    </div>

                    <div class="col-xs-8">
                      2. Harga Barang tidak terkirim
                    </div>
                    <div class="col-xs-1">
                      :
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                      Rp.{{number_format($Bap->item_not_send, 0, ',','.')}}
                    </div>

                    <div class="col-xs-8">
                      3. Selisih Pembayaran
                    </div>
                    <div class="col-xs-1">
                      :
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                      Rp.{{number_format($data->total-$Bap->must_pay, 0, ',','.')}}
                    </div>

                    <div class="col-xs-8" style="padding-left:50px;">
                      a. Nilai Kwitansi
                    </div>
                    <div class="col-xs-1" style="padding-left:15px;">
                      :
                    </div>
                    <div class="col-xs-3" style="padding-left:15px; text-align:right;">
                      Rp.{{number_format($Bap->must_pay, 0, ',','.')}}
                    </div>
                    <div class="col-xs-8" style="padding-left:50px;">
                      b. Denda Keterlambatan 1‰ (satu per seribu)
                    </div>
                    <div class="col-xs-1" style="padding-left:15px;">
                      :
                    </div>
                    <div class="col-xs-3" style="padding-left:15px; text-align:right;">
                      Rp.{{number_format($data->total-$Bap->must_pay, 0, ',','.')}}
                    </div>
                    <div class="col-xs-8" style="padding-left:50px;">
                      c. Denda Barang Tidak Terkirim (5%)
                    </div>
                    <div class="col-xs-1" style="padding-left:15px;">
                      :
                    </div>
                    <div class="col-xs-3" style="padding-left:15px; text-align:right;">
                      Rp.{{number_format($Bap->item_not_send, 0, ',','.')}}
                    </div>
                    <div class="col-xs-8" style="padding-left:50px;">
                      d. Total yang dapat dibayarkan sebesar
                    </div>
                    <div class="col-xs-1" style="padding-left:15px;">
                      :
                    </div>
                    <div class="col-xs-3" style="padding-left:15px; text-align:right;">
                      Rp.{{number_format($Bap->must_pay, 0, ',','.')}}
                    </div>

                  </div>
              <div class="col-xs-12" style="margin-top:20px; margin-bottom:20px;">
                Demikian Berita Acara Pembayaran ini dibuat dan ditandatangani pada tanggal tersebut diatas untuk dipergunakan seperlunya.
              </div>
              <div class="col-xs-6">
                <p for="no_po" class="col-xs-12">Menyetujui :</p>
              </div>
              <div class="col-xs-6">
                <p for="no_po" class="col-xs-12">Jakarta, {{date('d', $times)}} {{$bulanWord[date('m', $times)]}} {{date('Y', $times)}}</p>
              </div>
              <div class="col-xs-12">
                <div class="col-xs-6">
                    <p for="no_po" class="col-xs-12">PPK Pengadaan Barang / Jasa Medis</p>
                    <br><br><br><br><br>
                    <p for="no_po" class="col-xs-12">{{$directur->name}}</p>
                    <p for="no_po" class="col-xs-12">{{$directur->nik}}</p>
                    <br><br>
                </div>
                <div class="col-xs-6 pull-right">
                    <p class="col-xs-12">YANG MENERIMA :</p>
                    <p class="col-xs-12">PENERIMA HASIL PEKERJAAN BARANG / JASA MEDIS</p>
                    <p class="col-xs-12">RUMAH SAKIT JANTUNG DAN PEMBULUH DARAH HARAPAN KITA</p>
                    <br><br><br>
                    <br><br><br>
                    <ol>
                      @foreach ($pphp as $k => $v)
                        <li>{{$v->username}} &nbsp;&nbsp;&nbsp;.........................</li>
                      @endforeach
                    </ol>
                </div>

        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
@endsection

@section('js')
  <script type="text/javascript">
  var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

  jQuery(document).bind("keyup keydown", function(e){
      if(e.ctrlKey && e.keyCode == 80){

          var divToPrint=document.getElementById('DivIdToPrint');

          var newWin=window.open('','Print-Window');

          newWin.document.open();

          newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

          newWin.document.close();

          setTimeout(function(){newWin.close();},10);

      }
  });
  </script>
@endsection
