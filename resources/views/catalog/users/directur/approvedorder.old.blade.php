@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
     <br>
    <p style="text-align:left; font-size:12px !important;">{{Auth::user()->name}}</p>

  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Pesanan Terkonfirmasi</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <table summary="Shopping cart" id="mainCart">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>No Order</th>
                  <th>Pembeli</th>
                  <th>Di Setujui Oleh <br>(Supervisi)</th>
                  <th>Di Setujui Oleh <br>(Manager)</th>
                  <th>Detail</th>
                  <th>Revisi</th>
                </tr>
              </thead>
              <tbody>
                @if(count($order) > 0)
                @foreach($order as $k => $v)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->datetime}}</td>
                    <td style="color:blue;">#{{$v->no_order}}</td>
                    <td>{{$v->username_catalog}}</td>
                    <td>{{$v->name}}<br>{{$v->email}}<br>{{$v->mobile_phone}}</td>
                    <td>{{$v->name_manager}}<br>{{$v->email_manager}}<br>{{$v->mobile_phone_manager}}</td>
                    <td> <a style="float:left !important; color:white;" href="#" onclick="DetailCart(this)" class='btn btn-primary btn-xs' data-id="{{$v->id}}">Detail</a></td>
                    <td> <a style="float:left !important; color:white;" href="#" onclick="RevisiCart(this)" class='btn btn-success btn-xs' data-id="{{$v->id}}">Revisi</a></td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" style="text-align:center;">Tidak Ada Data</td>
                </tr>
              @endif
              </tbody>
          </table>
        </div>
        <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
          <form class="" action="{{URL::to('catalog/users/directur/revisi')}}" method="post">
            {{ csrf_field() }}
          <h1>  <center>Detail Pesanan</center></h1>
          <div class="row">
            <div class="col-xs-6">
              <address>
              <strong>Pembeli: </strong><br>
                <p id="name_buyer">Nama : xx</p>
                <p id="telephone_buyer">Telp: xx</p>
                <p id="email_buyer">Email: xx</p>
              </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                <strong>Order:</strong><br>
                <p id="no_order">No Order : yy</p>
                <p id="date_order">Tanggal : yy</p>
              </address>
            </div>

          </div>
          <table summary="Shopping cart" id="detailCart">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Total</th>
                  <th id="status">Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          <div class="shopping-total Modal" style="width: 300px;">
            <ul>
              <li>
                <em>Total</em>
                <input type="hidden" name="subtot" value="" id="input_subtot">
                <strong class="price">
                  Rp
                  <span id="subtot"></span>
                </strong>
              </li>
              {{-- <li>
                <em>Shipping Cost <small>PPN 10%</small></em>

                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
              {{-- <li class="shopping-total-price">
                <em>Total</em>
                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
            </ul>
            <button class="btn btn-primary" type="submit" id="revisi">Revisi <i class="fa fa-check"></i></button>
          </div>
        </form>
        </div>
      </div>
    </div>
    <div class="modal"></div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script type="text/javascript">
var activeEl = 2;
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});

function DetailCart(a) {
  $("#status").css('display','none');
  $("#revisi").css('display','none');
  var id_order = $(a).attr('data-id');
  var url = "{{URL::to('catalog/users/directur/detail_order')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_order": id_order,
              },
            success: function (a) {
              $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
              $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
              $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
              $("#no_order").html("<b>No Order :</b> #"+a.dataItem.no_order);
              $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
              var table = ""
              var subtot = 0;
              $.each(a.data, function(k, v){
                table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'">'+v.name+'</td>';
                table += '<td>';
                    table += '<strong>';
                    table += '<span>'+addCommas(v.qty);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td>';
                    table += '<strong>';
                    table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                    table += '<span>'+addCommas(v.price_gov);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td>  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_gov*v.qty+'" class="total total_'+v.id_item+'">';
                      table += '<strong>';
                      table += '<span data-value="'+v.price_gov*v.qty+'" class="total_tag total_tag_'+v.id_item+'">'+addCommas(v.price_gov*v.qty)+'</span>';
                      table += '</strong>';
                table += '</td>';
                // table += '<td><div class="">';
                //       table += '<label><input type="radio" class="radio-inline form-control input-sm" name="status['+v.id_item+']" checked value="1"><span class="outside"><span class="inside"></span></span>Setuju</label>';
                //       table += '<label><input type="radio"class="radio-inline form-control input-sm" name="status['+v.id_item+']"  value="0"><span class="outside"><span class="inside"></span></span>Tolak</label>';
                // table += '</div></td>';
                // table += "<td><a href='#' style='float:left !important; color:white;' class='btn btn-danger btn-xs'>Tolak</a></td>";
                table += "</tr>";
                sub = v.price_gov*v.qty;
                subtot += sub;
              });

              $("#detailCart tbody").html(table);
              $(".Modal span#subtot").html(addCommas(subtot));
              $(".Modal input#input_subtot").attr("value",subtot);
              $("#detailCart").animate({ scrollTop: $('#detailCart').prop("scrollHeight")}, 1000);
            }
    })
  $("#divDetailCart").css('display','');
}

function RevisiCart(a) {
  $("#status").css('display','');
  $("#revisi").css('display','');
  var id_order = $(a).attr('data-id');
  var url = "{{URL::to('catalog/users/directur/detail_order')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_order": id_order,
              },
            success: function (a) {
              $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
              $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
              $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
              $("#no_order").html("<b>No Order :</b> #"+a.dataItem.no_order);
              $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
              var table = ""
              var subtot = 0;
              $.each(a.data, function(k, v){
                table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                table += '<td><input type="hidden" name="id_order" value="'+v.id_order+'">'+(parseInt(k)+1)+'</td>';
                table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'">'+v.name+'</td>';
                table += '<td class="goods-page-quantity"><div class="product-quantity"><input id="product-quantity" name="qty['+v.id_item+']" onchange="InputQty(this)" type="number" value="'+v.qty+'" data-id="'+v.id_item+'" class="form-control input-sm qty qty_'+v.id_item+'"></div></td>';
                // table += '<td>';
                //     table += '<strong>';
                //     table += '<span>'+addCommas(v.qty);
                //
                //     table += '</span>';
                //     table += '</strong>'
                // table += '</td>';
                table += '<td>';
                    table += '<strong>';
                    table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                    table += '<span>'+addCommas(v.price_gov);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td>  <input type="hidden" name="total['+v.id_item+']" value="'+v.price_gov*v.qty+'" class="total total_'+v.id_item+'">';
                      table += '<strong>';
                      table += '<span data-value="'+v.price_gov*v.qty+'" class="total_tag total_tag_'+v.id_item+'">'+addCommas(v.price_gov*v.qty)+'</span>';
                      table += '</strong>';
                table += '</td>';
                table += '<td><div class="">';
                      table += '<label><input type="radio" class="radio-inline form-control input-sm" name="status['+v.id_item+']" checked value="1"><span class="outside"><span class="inside"></span></span>Setuju</label>';
                      table += '<label><input type="radio"class="radio-inline form-control input-sm" name="status['+v.id_item+']"  value="0"><span class="outside"><span class="inside"></span></span>Tolak</label>';
                table += '</div></td>';
                table += "</tr>";
                sub = v.price_gov*v.qty;
                subtot += sub;
              });

              $("#detailCart tbody").html(table);
              $(".Modal span#subtot").html(addCommas(subtot));
              $(".Modal input#input_subtot").attr("value",subtot);
              $("#detailCart").animate({ scrollTop: $('#detailCart').prop("scrollHeight")}, 1000);
            }
    })
  $("#divDetailCart").css('display','');
}


$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});

function InputQty(a) {

  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        total = nilai*price;

        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          subtot += Number(sub);
        })
          $('#subtot').html(addCommas(subtot));
          $('#input_subtot').attr("value",subtot);
          $(".Modal span#subtot").html(addCommas(subtot));
      }
    });

}

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("#dashboard_nav").attr('href','{{URL::to('catalog/users/directur/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("#cartItem").attr('href','{{URL::to('catalog/users/directur/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("#history").attr('href','{{URL::to('catalog/users/directur/history')}}');
  $("#approval").removeAttr('href');
  $("#approval").attr('href','{{URL::to('catalog/users/directur/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("#tracking").attr('href','{{URL::to('catalog/users/directur/track')}}');
});


</script>
@endsection
