@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
<style>
.informasi {
  display: inline-block;
  width: 100% !important;
  margin-right: .5em;
  padding-top: 0.0em;
}
</style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/users/directur/bap/create/'.$id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">

              <div class="col-md-12">

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                <label for="id_contract" class="col-md-3 control-label">Pengadaan</label>
                <div class="col-md-8">
                  <input type="text" name="procurement" value="{{$data->name_category}}" class="form-control input-sm">
                  @if ($errors->has('id_contract'))
                    <span class="help-block">
                      <strong>{{ $errors->first('id_contract') }}</strong>
                    </span>
                  @endif
                </div>
              </div>


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="id_vendor_detail" class="col-md-3 control-label">Pelaksana</label>
                  <div class="col-md-8">
                    <select name="id_vendor_detail" class="form-control input-sm">
                        <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                    </select>
                    @if ($errors->has('id_vendor_detail'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_vendor_detail') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="no_contract" class="col-md-3 control-label">Nomor Kontrak</label>
                  <div class="col-md-8">
                    <select name="id_contract" class="form-control input-sm">
                        <option value="{{$contract->id}}">{{$contract->no_contract}}</option>
                    </select>
                    <strong>

                      {{--  <input id="no_contract" type="hidden" class="form-control input-sm" name="no_contract" value="{{$contract->id}}" readonly required autofocus>  --}}
                    </strong>
                    @if ($errors->has('no_contract'))
                      <span class="help-block">
                        <strong>{{ $errors->first('no_contract') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="no_adendum" class="col-md-3 control-label">Nomor Adendum</label>
                  <div class="col-md-8">
                    {{--  <strong>  --}}
                      {{--  <ol style="padding-left:15px;">  --}}

                        @foreach ($adendum as $k => $v)
                          <div class="col-md-12" style="margin-bottom:5px !important; margin-left:-15px; width:104%;">
                            <input id="no_adendum" type="text" readonly class="form-control input-sm" name="no_adendum" value="{{$v->no_adendum}}" readonly required autofocus>
                          </div>
                        @endforeach
                      {{--  </ol>  --}}
                    {{--  </strong>  --}}
                    @if ($errors->has('no_adendum'))
                      <span class="help-block">
                        <strong>{{ $errors->first('no_adendum') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="id_order" class="col-md-3 control-label">Nomor SP</label>
                  <div class="col-md-8">
                    <div class="col-md-11">
                      <input style="margin-left:-15px; text-transform: uppercase;" id="no_order" type="text" readonly class="form-control input-sm" name="id_order" value="{{$data->no_medik != NULL ? $data->no_medik : $data->no_non_medik}}/{{date('n',strtotime($data->datetime))}}/{{$data->name_category}}/E-catalog{{date('Y', strtotime($data->datetime))}}" required autofocus>
                    </div>
                    <div class="col-md-1">
                      <a target="_blank" style="text-transform:uppercase; margin-left:-15px;" href="{{URL::to('catalog/users/directur/po/download/'. md5($data->id))}}"  class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> cek</a>
                    </div>
                    <input id="id_order" type="hidden" readonly class="form-control input-sm" name="id_order" value="{{$data->id}}" required autofocus>
                    @if ($errors->has('id_order'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_order') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="total" class="col-md-3 control-label">Nilai SP</label>
                  <div class="col-md-8">
                    <strong>
                      Rp.{{number_format($data->total, 0, ',','.')}}
                    </strong>
                    <input style="margin-bottom:10px;" id="total" type="hidden" readonly class="form-control input-sm" name="total" value="{{$data->total}}" required autofocus>
                    @if ($errors->has('total'))
                      <span class="help-block">
                        <strong>{{ $errors->first('total') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                @php
                $function =  Fungsi::MonthIndonesia();
                $functionDay =  Fungsi::DayIndonesia();
                $RomanMonth =  Fungsi::MonthRoman();
                if($data->total >= 200000000){
                  $aritdate =  date('Y-m-d', strtotime($data->datetime. "+14 days"));
                  $NextDate = explode(' ', $aritdate);

                }else{
                  $aritdate =  date('Y-m-d', strtotime($data->datetime. "+7 days"));
                  $NextDate = explode(' ', $aritdate);
                }

                $time = strtotime($aritdate);
                $newformat = date('Y-m-d',$time);
                $totBAST = 0;
                @endphp
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="total" class="col-md-3 control-label">Nomor BAST</label>
                  <div class="col-md-8">
                      @foreach ($bast as $k => $v)
                        @php
                        $timesDO = strtotime($v->datetime_kirim);
                        $times = strtotime($v->datetime);
                        $new_date = date('Y-m-d', $timesDO);
                        if($new_date > $newformat){
                          $date1 = new DateTime($new_date);
                          $date2 = new DateTime($newformat);
                          $telat =  "(".$date2->diff($date1)->format('%R%a hari').")";
                        }else{
                          $telat =  "";
                        }
                        @endphp

                        <div class="col-md-11" style="margin-bottom:-15px !important;">
                          <input name="no_bast" id="" class="form-control input-sm" type="text" readonly style="text-transform: uppercase;margin-left:-15px; background-color:{{$new_date > $newformat ? '#ed555d' : '#51b6af'}}; color:#fff;" value="{{$v->no_bast}}/BAST/{{$v->name_category}}/{{$userCode->kode_instalasi}}/RSJPDHK/{{$RomanMonth[date('m', $times)]}}/{{date('Y', $timesDO)}}"> <br>
                        </div>
                        <div class="col-md-1">
                          <a target="_blank" style="text-transform:uppercase; margin-left:-15px;" href="{{URL::to('catalog/users/directur/bast/preview/'.md5($v->id))}}"  class="btn btn-{{$new_date > $newformat ? 'danger' : 'success'}} btn-sm"><i class="fa fa-search" aria-hidden="true"></i> cek</a>
                        </div>
                        <input name="id_bast" id="" class="form-control input-sm" type="hidden" style="text-transform: uppercase;" value="{{$v->id}}">
                        @php
                        $detail = DB::table('bast_detail')->select(DB::raw('SUM(bast_detail.qty*item.price_gov)  AS total'))->join('item', 'bast_detail.id_item', '=', 'item.id')->where('bast_detail.id_bast', $v->id)->first();
                        $subtot = $detail->total;
                        $totBAST += $subtot;
                        @endphp
                      @endforeach
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="item_not_send" class="col-md-3 control-label">Barang Tidak Terkirim</label>
                  <div class="col-md-8">
                    <input style="margin-bottom:10px;" id="item_not_send" type="number" min="0" max="0" class="form-control input-sm" name="item_not_send" value="0" required autofocus>
                    @if ($errors->has('item_not_send'))
                      <span class="help-block">
                        <strong>{{ $errors->first('item_not_send') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="fine" class="col-md-3 control-label">Denda Keterlambatan 1‰ (satu per seribu)</label>
                  <div class="col-md-8">
                      @foreach ($bast as $k => $v)
                        @php
                        $timesDO = strtotime($v->datetime_kirim);
                        $times = strtotime($v->datetime);
                        $new_date = date('Y-m-d', $timesDO);
                        if($new_date > $newformat){
                          $date1 = new DateTime($new_date);
                          $date2 = new DateTime($newformat);
                          $telat =  "(".$date2->diff($date1)->format('%a hari').")";
                          $telat1 =  $date2->diff($date1)->format('%a');
                        }else{
                          $telat =  "";
                          $telat1 =  "";
                        }

                        $jumTot = 0;
                        @endphp

                        @if($new_date > $newformat)
                            @foreach ($CodeBast as $ke => $va)
                              @if($v->id == $va)
                                @if(array_key_exists($ke, $ItemBast))
                                  @php
                                  $qty = $QtyBast[$ke];
                                  $harga = $PriceBast[$ke];
                                  $tot = $qty*$harga;
                                  $jumTot += $tot;
                                @endphp
                              @else
                                @php
                                $qty =  0;
                                $harga =  0;
                                $tot =  0;
                                $jumTot += 0;
                                @endphp
                                @endif
                              @endif
                            @endforeach
                            @php

                            $datas[] = $jumTot;
                            $late[] = $telat1;
                            $informasi[] = $v->no_bast.'/BAST/'.$v->name_category.'/'.$userCode->kode_instalasi.'/RSJPDHK/'.$RomanMonth[date("m", $times)].'/'.date("Y", $timesDO);
                            @endphp
                        @endif
                        @php
                        @endphp
                      @endforeach
                      @php
                        $jumkes = 0;
                      @endphp
                      @if (isset($informasi))
                          @foreach ($informasi as $x => $y)
                            @php
                              $jumlah = ceil($datas[$x]*1/1000*$late[$x]);
                              $jumkes += $jumlah;
                            @endphp
                            <label class="informasi" style="text-transform:uppercase;">{{$y}} (1/1000 x {{number_format($datas[$x], 0, ',','.')}} x {{$late[$x]}})<input name="member1" id="member1" class="form-control input-sm" width="100%" name="item_not_send" value="{{number_format(ceil($datas[$x]*1/1000*$late[$x]), 2, ',','.')}}" max="0.001" min="0.001" readonly required /></label>
                            <input type="hidden" name="bast_denda[]" value="{{$jumlah}}">
                          @endforeach
                      @endif
                      <label class="informasi" style="text-transform:uppercase;">Total : <input name="member1" id="member1" class="form-control input-sm" width="100%" name="item_not_send" value="{{number_format($jumkes, 2, ',','.')}}" max="0.001" min="0.001" readonly required /></label>
                      <input type="hidden" name="bast_denda_total" value="{{$jumkes}}">

                    @if ($errors->has('fine'))
                      <span class="help-block">
                        <strong>{{ $errors->first('fine') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="mustpay" class="col-md-3 control-label">Total yang Dibayarkan</label>
                  <div class="col-md-8">
                    <input id="member1" class="form-control input-sm" width="100%" name="mustpay" value="{{number_format($totBAST-$jumkes, 2, ',','.')}}" max="{{$totBAST-$jumkes}}" min="{{$data->total-$jumkes}}" readonly required />
                    <input type="hidden" name="must_pay" value="{{$totBAST-$jumkes}}">
                    @if ($errors->has('mustpay'))
                      <span class="help-block">
                        <strong>{{ $errors->first('mustpay') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

              <div class="form-group col-md-12">
                <br><br>
                <div class="col-md-9"></div>
                <div class="col-md-2">
                  <a href="{{URL::to('catalog/users/directur/ppk_review')}}" class="btn btn-danger btn-sm">Kembali</a>
                  <button type="submit" name="button" class="btn btn-primary btn-sm">Simpan</button>
                </div>
              </div>

            </div>
        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function(){
    $("#dashboard_nav").removeAttr('href');
    $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/directur/dashboard')}}');
    $("#cartItem").removeAttr('href');
    $("a#cartItem").attr('href','{{URL::to('catalog/users/directur/cart')}}');
    $("#cart").css('display','none');
    $("#history").removeAttr('href');
    $("a#history").attr('href','{{URL::to('catalog/users/directur/history')}}');
    $("#approval").removeAttr('href');
    $("a#approval").attr('href','{{URL::to('catalog/users/directur/payment_approved')}}');
    $("a#approval").attr('text', 'Pembayaran');
    $("#tracking").removeAttr('href');
    $("a#tracking").attr('href','{{URL::to('catalog/users/directur/track')}}');
  });
  </script>
@endsection
