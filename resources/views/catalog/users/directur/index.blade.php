@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
  <link href="/su_catalog/assets/global/css/page-approval.css" rel="stylesheet">
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    <p style="text-align:left; font-size:12px !important;">{{Auth::user()->name}}</p>
  </a>
@endsection

@section('content')
<img src="https://media.glassdoor.com/eep/11958/femaledoctorchildpatientd.compress.gd.jpg" alt="" style="width: 100% !important;">
 <div class="centered">
   {{-- <h1 style="color: #64aed9; font-size: 45px;"> <b></b></h1> --}}
 </div>


 <!--buttoon-->
   <div class="btn-group btn-group-justified">
       {{-- <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/cart')}}'">
               <span class="glyphicon glyphicon-shopping-cart"></span>
               <p>Cart</p>
           </button>
       </div> --}}
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/ppk_review')}}'">
               <span class="glyphicon glyphicon-list-alt"></span>
         <p>Review</p>
       </button>
       </div>
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/order_waiting')}}'">
               <span class="glyphicon glyphicon-cd"></span>
               <p>
                 Pesanan Menunggu
                 @if ($order > 0)
                   <span class="badge badge-success">{{$order}}</span></a>
                 @endif

               </p>
       </button>
       </div>
       <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/order_approved')}}'">
               <span class="glyphicon glyphicon-thumbs-up"></span>
               <p>Pesanan Tersetujui</p>
       </button>
       </div>
       {{-- <div class="btn-group">
       <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/order_approved')}}'">
               <span class="glyphicon glyphicon-briefcase"></span>
               <p>Budget</p>
       </button>
       </div> --}}
       {{-- <div class="btn-group">
           <button type="button" class="btn btn-nav">
               <span class="glyphicon glyphicon-plane"></span>
         <p>Order Deliver</p>
           </button>
       </div>
       <div class="btn-group">
           <button type="button" class="btn btn-nav">
               <span class="glyphicon glyphicon-ok"></span>
         <p>Order Success</p>
           </button>
       </div> --}}
       <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/track')}}'">
               <span class="glyphicon glyphicon-bell"></span>
               <p>Lacak</p>
           </button>
       </div>
       <div class="btn-group">
           <button type="button" class="btn btn-nav" onclick="window.location.href='{{URL::to('catalog/users/directur/history')}}'">
               <span class="glyphicon glyphicon-list-alt"></span>
               <p>Riwayat</p>
           </button>
       </div>
   </div>
 @endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript">
var activeEl = '';
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});

$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/directur/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/directur/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/directur/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/directur/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/directur/track')}}');
  $("li#dashboard_nav").attr("class","active");
});
</script>
@endsection
