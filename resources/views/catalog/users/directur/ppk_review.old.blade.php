@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{$company->company}}
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Review</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
          </div>
          <br>
          <table summary="Shopping cart" id="mainCart">
              <thead>
                  <tr>
                    <th>No</th>
                    <th>No Usulan</th>
                    <th>No SP</th>
                    <th>No Pesanan</th>
                    <th>Tanggal</th>
                    <th>Pembeli</th>
                    <th>Total</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Dokumen</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($order as $k => $v)
                  @php
                  $time = strtotime($v->datetime);
                  $id_proposer = md5($v->id_proposer);
                  $no_po = md5($v->no_po);
                  $month = md5(date('n', $time));
                  $year = md5(date('Y', $time));
                  $id = md5($v->id);
                  @endphp

                  <tr>
                    <td>{{$k+1}}</td>
                    <td>
                        <a target="_blank" rel="noopener noreferrer" href="{{URL::to('catalog/users/directur/proposer/download/'.$id_proposer)}}">{{ $v->no_prop }}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</a>
                    </td>
                    <td>
                      @if($v->status >= '3')
                        <a target="_blank" style="text-transform:uppercase;" href="{{URL::to('catalog/users/directur/po/download/'.$no_po.'/'.$month.'/'.$year)}}">
                          {{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('n', $time)}}/{{$v->name_category}}/E-catalog{{date('Y', $time)}}<a/>
                      @else
                        <font style="text-transform:uppercase;">
                          {{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('n', $time)}}/{{$v->name_category}}/E-catalog{{date('Y', $time)}}
                        </font>
                      @endif
                    </td>
                      <td>#{{$v->no_order}}</td>
                    <td>{{$v->datetime}}</td>
                    <td>{{($v->username_catalog == null) ? $v->name : $v->username_catalog}}</td>
                    <td>Rp.{{number_format($v->total, 0, ',','.')}}</td>
                    <td>
                      @php
                        $status = $v->status;
                      @endphp
                      <span style="display: none;">{{ $status }}</span>
                      @if ($status == 0)
                        <span class="label bg-grey" style="display: block;">Menunggu Persetujuan</span>
                      @elseif ($status == 1)
                        <span class="label bg-yellow" style="display: block;">Telah disetujui Supervisi</span>
                      @elseif ($status == 2)
                        <span class="label bg-blue" style="display: block;">Telah disetujui Manager</span>
                      @elseif ($status == 3)
                        <span class="label bg-green" style="display: block;">Telah disetujui Direktur</span>
                      @elseif ($status == 4)
                        <span class="label bg-purple" style="display: block;">Barang Dikirim</span>
                      @elseif ($status == 5)
                        <span class="label bg-grey" style="display: block;">Barang Terkirim</span>
                      @elseif ($status == 6)
                        <span class="label bg-yellow" style="display: block;">Barang diterima oleh Gudang</span>
                      @endif
                    </td>
                    <td>
                      <div class="">
                        <a style="float:left !important; color:white; margin-left:5px;" href="{{URL::to('catalog/users/directur/proposer/download/'.$id_proposer)}}" class="btn btn-info btn-xs btn-block">Usulan</a>
                        @if($v->status >= '3')
                          <a style="float:left !important; color:white;"  href="{{URL::to('catalog/users/directur/po/download/'.$no_po.'/'.$month.'/'.$year)}}" class="btn btn-warning btn-xs btn-block">Purchase Order</a>
                        @endif
                        @if (in_array($v->id, $bapbID))
                          @php
                            $jmlId = array_count_values($bapbID);
                            $no =1;
                          @endphp

                            @foreach ($bapbIDS as $q => $p)
                              @if ($v->id == $p)
                                @if ($bapbStatus[$q] =='2')
                                  <a style="float:left !important;  color:white;" href="{{URL::to('catalog/users/directur/bapb/preview/'.md5($q))}}" target="_blank"  class="btn btn-success btn-xs btn-block">BAPB - {{$no}}</a>
                                @else
                                  <a style="float:left !important; color:white;" data-id="{{$v->id}}" data-bapb="{{$q}}" data-mbapb="{{md5($q)}}"  onclick="DetailCart(this)" class="btn btn-danger btn-xs btn-block">BAPB - {{$no}}</a>
                                @endif
                                  @php
                                  $no++;
                                  @endphp
                              @endif
                            @endforeach

                        @endif
                        @if (in_array($v->id, $bastID))
                          @php
                            $jmlId = array_count_values($bastID);
                          @endphp
                              @if(array_key_exists($v->id,$jmlId))
                                  @if (in_array($v->id, $bastIDS))
                                    @php
                                    $no =1;
                                    @endphp
                                    @foreach ($bastIDS as $xy => $xz)
                                      @if ($v->id == $xz)
                                          <a style="float:left !important;  color:white;" href="{{URL::to('catalog/users/directur/bast/preview/'.md5($xy))}}" target="_blank"  class="btn btn-default btn-xs btn-block">BAST - {{$no}}</a>
                                        @php
                                        $no++;
                                        @endphp
                                      @endif
                                    @endforeach
                                  @endif
                              @endif
                              @if(($v->status >= '6'))
                                <a style="float:left !important; color:white;" target="_blank" href="{{URL::to('catalog/users/directur/invoice/create/'.$id)}}" class="btn btn-success btn-xs btn-block">Invoice</a>
                                @if (array_key_exists($v->id, $bap))
                                  <a style="float:left !important;  color:white;" href="{{URL::to('catalog/users/directur/bap/preview/'.md5($bap[$v->id]))}}" target="_blank"  class="btn btn-default btn-xs btn-block">BAP</a>
                                @else
                                  <a style="float:left !important; color:white;" href="{{URL::to('catalog/users/directur/bap/create/'.$id)}}" class="btn btn-danger btn-xs btn-block">Buat BAP</a>
                                @endif
                              @endif
                        @endif
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
          <form class="form_checked"  method="post">
            {{ csrf_field() }}
          <h1>  <center>Detail Pesanan</center></h1>
          <div class="row">
            <div class="col-xs-6">
              <address>
              <strong>Pembeli: </strong><br>
                <p id="name_buyer">Nama : xx</p>
                <p id="telephone_buyer">Telp: xx</p>
                <p id="email_buyer">Email: xx</p>
              </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                <strong>Order:</strong><br>
                <p id="no_order">No Order : yy</p>
                <p id="date_order">Tanggal : yy</p>
              </address>
            </div>

          </div>
          <table summary="Shopping cart" id="detailCart">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th style="text-align:left;">Qty</th>
                  <th style="text-align:right;">Harga</th>
                  <th style="text-align:right;">Biaya Kirim</th>
                  <th style="text-align:right;">Total</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          <div class="shopping-total Modal" style="width: 300px;">
            <ul>
              <li style="margin-left:-130px;">
                <em style="float:left !important;">Total</em>
                <input type="hidden" name="subtot" value="" id="input_subtot">
                <strong class="price">
                  Rp
                  <span id="subtot"></span>
                </strong>
              </li>
              {{-- <li>
                <em>Shipping Cost <small>PPN 10%</small></em>

                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
              {{-- <li class="shopping-total-price">
                <em>Total</em>
                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
            </ul>
            <button class="btn btn-primary" type="submit">Konfirmasi <i class="fa fa-check"></i></button>
          </div>
        </form>
        </div>
      </div>
    </div>
    <div class="modal"></div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script type="text/javascript">
$(document).ready( function () {
  $('table#mainCart').DataTable();
} );

$('.konfirmasi').on('click', function(){
  $body = $("body");
  $body.addClass("loading");
})

var activeEl = 2;
$(function() {
    var items = $('.btn-nav');
    $( items[activeEl] ).addClass('active');
    $( ".btn-nav" ).click(function() {
        $( items[activeEl] ).removeClass('active');
        $( this ).addClass('active');
        activeEl = $( ".btn-nav" ).index( this );
    });
});

function DetailCart(a) {
  var id_order = $(a).attr('data-id');
  var bapb = $(a).attr('data-bapb');
  var md5bapb = $(a).attr('data-mbapb');
  var url = "{{URL::to('catalog/users/directur/detail_bapb')}}";
    $.ajax({
          type: "post",
          url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_order": id_order, "id_bapb" : bapb
              },
            success: function (a) {
              $("#name_buyer").html("<b>Nama :</b> "+a.user_catalog.username_catalog);
              $("#telephone_buyer").html("<b>Telp :</b> "+a.user_catalog.telephone);
              $("#email_buyer").html("<b>Email :</b> "+a.user_catalog.email);
              $("#no_order").html("<b>No Order :</b> #"+a.dataItem.no_order);
              $("#date_order").html("<b>Tanggal :</b> "+a.dataItem.datetime);
              var table = ""
              var subtot = 0;
              //
              $.each(a.data, function(k, v){
                table += '<tr class="dataTr tr_'+v.id_item+'" data-id="'+v.id_item+'">';
                table += '<td><input type="hidden" name="id_bapb" value="'+v.id_bapb+'">'+(parseInt(k)+1)+'</td>';
                table += '<td><input type="hidden" name="id_item['+v.id_item+']" value="'+v.id_item+'"><input type="hidden" name="id_user" value="'+a.user_catalog.id+'"><a target="_blank" href="/catalog/users/item/detail/'+v.id_item+'" style="text-decoration: none; font-size: 14px;">'+v.name+'</a></td>';
                table += '<td class="goods-page-quantity"><div class=""><input style="width: 70px;" id="product-quantity" name="qty['+v.id_item+']" readonly max="'+v.stock+'" oninput="InputQty(this)" type="hidden" value="'+v.qty+'" data-id="'+v.id_item+'" class="form-control input-sm qty qty_'+v.id_item+'"><input style="width: 70px;" id="product-quantity" name="satuan['+v.id_item+']" readonly type="hidden" value="'+v.id_satuan+'" data-id="'+v.id_item+'" class="form-control input-sm qty satuan_'+v.id_item+'">'+v.qty+' '+v.unit_name+'</div></td>';
                table += '<td style="text-align:right;">';
                    table += '<strong>';
                    table += '<input type="hidden" name="price_pcs['+v.id_item+']" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id_item+'">';
                    table += '<span>Rp. '+addCommas(v.price_gov);

                    table += '</span>';
                    table += '</strong>'
                table += '</td>';
                table += '<td style="text-align:right;">  <input type="hidden" name="price_shipment['+v.id_item+']" data-id="'+v.id_item+'" value="'+v.price_shipment+'" class="price_shipment price_shipment_'+v.id_item+'">';
                      table += '<strong>';
                      table += '<span data-value="'+v.price_shipment+'" class="price_shipment price_shipment_'+v.id_item+'">Rp. '+addCommas(v.price_shipment)+'</span>';
                      table += '</strong>';
                table += '</td>';
                table += '<td style="text-align:right;">  <input type="hidden" name="total['+v.id_item+']" value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total total_'+v.id_item+'">';
                      table += '<strong>';
                      table += '<span data-value="'+((v.price_gov*v.qty)+parseInt(v.price_shipment))+'" class="total_tag total_tag_'+v.id_item+'">Rp. '+addCommas((v.price_gov*v.qty)+parseInt(v.price_shipment))+'</span>';
                      table += '</strong>';
                table += '</td>';
                // table += '<td><div class="" style="text-align:right;">';
                //       table += '<label style="padding-right:20px; font-size:12px;"><input type="radio" onclick="SetujuOrder(this)" data-id="'+v.id_item+'" class="setuju radio-inline form-control input-sm" name="status['+v.id_item+']" checked value="1"><span class="outside"><span class="inside"></span></span>Setuju</label>';
                //       table += '<label style="padding-right:20px; font-size:12px;"><input type="radio" onclick="TolakOrder(this)" data-id="'+v.id_item+'" class="tolak radio-inline form-control input-sm" name="status['+v.id_item+']"  value="0"><span class="outside"><span class="inside"></span></span>Tolak</label>';
                // table += '</div></td>';
                // table += '<td style="text-align:right;">';
                //       table += '<span class="rejectInput rejectInput_'+v.id_item+'"></span>';
                // table += '</td>';
                // table += "<td><a href='#' style='float:left !important; color:white;' class='btn btn-danger btn-xs'>Tolak</a></td>";
                table += "</tr>";
                sub = ((v.price_gov*v.qty)+parseInt(v.price_shipment));
                subtot += sub;
              });


              $("#detailCart tbody").html(table);
              $(".Modal span#subtot").html(addCommas(subtot));
              $('#input_subtot').attr("value",subtot);
              $("html, body").animate({ scrollTop: $("#detailCart tbody").prop("scrollHeight")}, 500)
            }
    })

  var url = {!! json_encode(url('/')) !!}
  console.log(md5bapb);
  $("#divDetailCart").css('display','');
  $('.form_checked').attr("action", url+"/catalog/users/directur/bapb/checked/"+md5bapb);
}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});

function InputQty(a) {

  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        total = nilai*price;

        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          subtot += Number(sub);
        })
          $('#subtot').html(addCommas(subtot));
          $('#input_subtot').html(subtot);
          $(".Modal span#subtot").html(addCommas(subtot));
      }
    });

}

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/directur/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/directur/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/directur/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/directur/payment_approved')}}');
  $("a#approval").attr('text', 'Pembayaran');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/directur/track')}}');
});
</script>
@endsection
