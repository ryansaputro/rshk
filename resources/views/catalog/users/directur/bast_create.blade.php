@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
<style>
.informasi {
  display: inline-block;
  width: 100% !important;
  margin-right: .5em;
  padding-top: 0.0em;
}
</style>
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
              {{ session()->get('message') }}
          </div>
      @endif
      <div class="portlet light">
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/users/directur/bast/create/'.$id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">

              <div class="col-md-12">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="id_vendor_detail" class="col-md-3 control-label">Pelaksana</label>
                  <div class="col-md-8">
                    <select name="id_vendor_detail" class="form-control input-sm">
                        <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                    </select>
                    @if ($errors->has('id_vendor_detail'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_vendor_detail') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="no_contract" class="col-md-3 control-label">Nomor Kontrak</label>
                  <div class="col-md-8">
                    <select name="id_contract" class="form-control input-sm">
                        <option value="{{$contract->id}}">{{$contract->no_contract}}</option>
                    </select>
                    @if ($errors->has('no_contract'))
                      <span class="help-block">
                        <strong>{{ $errors->first('no_contract') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="no_adendum" class="col-md-3 control-label">Nomor Adendum</label>
                  <div class="col-md-8">
                        @foreach ($adendum as $k => $v)
                          <div class="col-md-12" style="margin-bottom:5px !important; margin-left:-15px; width:104%;">
                            <input id="no_adendum" type="text" readonly class="form-control input-sm" name="no_adendum" value="{{$v->no_adendum}}" readonly required autofocus>
                          </div>
                        @endforeach
                    @if ($errors->has('no_adendum'))
                      <span class="help-block">
                        <strong>{{ $errors->first('no_adendum') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12">
                  <label for="id_order" class="col-md-3 control-label">Nomor SP</label>
                  <div class="col-md-8">
                    <div class="col-md-11">
                      <input style="margin-left:-15px; text-transform: uppercase;" id="no_order" type="text" readonly class="form-control input-sm" name="id_order" value="{{$data->no_medik != NULL ? $data->no_medik : $data->no_non_medik}}/{{date('n',strtotime($data->datetime))}}/{{$data->name_category}}/E-catalog{{date('Y', strtotime($data->datetime))}}" required autofocus>
                    </div>
                    <div class="col-md-1">
                      <a target="_blank" style="text-transform:uppercase; margin-left:-15px;" href="{{URL::to('catalog/users/directur/po/download/'. md5($data->id))}}"  class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> cek</a>
                    </div>
                    <input id="id_order" type="hidden" readonly class="form-control input-sm" name="id_order" value="{{$data->id}}" required autofocus>
                    @if ($errors->has('id_order'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_order') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                @php
                $function =  Fungsi::MonthIndonesia();
                $functionDay =  Fungsi::DayIndonesia();
                $RomanMonth =  Fungsi::MonthRoman();
                if($data->total >= 200000000){
                  $aritdate =  date('Y-m-d', strtotime($data->datetime. "+14 days"));
                  $NextDate = explode(' ', $aritdate);

                }else{
                  $aritdate =  date('Y-m-d', strtotime($data->datetime. "+7 days"));
                  $NextDate = explode(' ', $aritdate);
                }

                $time = strtotime($aritdate);
                $newformat = date('Y-m-d',$time);
                @endphp

                <div class="table-response col-md-11">
                  <table class="table table-striped">
                    <tr>
                      <th>Kode</th>
                      <th>Deskripsi</th>
                      <th>Jumlah Terima</th>
                      <th>Harga Satuan</th>
                      <th>Hasil</th>
                      <th>Satuan</th>
                      <th>Total</th>
                    </tr>
                    @php
                    $tot = 0;
                    @endphp
                    @foreach ($bapb as $k => $v)
                      @php
                        $subtot = $v->qty*$v->price_gov;
                        $tot += $subtot;
                      @endphp
                      <tr>
                        <td>
                          {{$v->code}}
                          <input type="hidden" name="id_item[]" value="{{$v->id_item}}">
                          <input type="hidden" name="qty[]" value="{{$v->qty}}" class="qty_{{$v->id_item}}">
                          <input type="hidden" name="satuan[]" value="{{$v->id_satuan}}">
                          <input type="hidden" name="description[]" value="-">
                        </td>
                        <td>{{$v->name}}</td>
                        <td>{{number_format($v->qty, 0, ',','.')}} <font class="satuan_awal_{{$v->id_item}}" data-id="{{$v->id_satuan}}" data-convert="{{$v->convert_pcs}}">{{$v->unit_name}}</font></td>
                        <td>{{number_format($v->price_gov, 0, ',','.')}}</td>
                        <td>
                          <font class="qty_label_{{$v->id_item}}">
                            {{number_format($v->qty, 0, ',','.')}}
                          </font>
                          <input type="hidden" name="hasil[]" class="form-control hasil_{{$v->id_item}}" value="{{$v->qty}}">
                        </td>
                        <td>
                          <select name="satuanDO[]" onchange="Convert(this)"  data-qty="{{$v->qty}}"  data-id_detail="{{$v->id_item}}" class="satuan">
                            @foreach ($allSatuan as $q => $n)
                              <option value="{{$n->id}}" data-convert="{{$n->convert_pcs}}"  {{$n->id == $v->id_satuan ? 'selected' : ''}}>{{$n->unit_name}}</option>
                            @endforeach
                          </select>
                        </td>
                        <td>{{number_format($subtot, 0, ',','.')}}</td>
                      </tr>
                    @endforeach
                    <tr>
                      <td colspan="3"></td>
                      <td colspan="3">
                        <strong> Total </strong>
                      </td>
                      <td>
                        {{number_format($tot, 0, ',','.')}}
                      </td>
                    </tr>
                  </table>
                </div>

              <div class="form-group col-md-12">
                <br><br>
                <div class="col-md-9"></div>
                <div class="col-md-2">
                  <a href="{{URL::to('catalog/users/directur/ppk_review')}}" class="btn btn-danger btn-sm">Kembali</a>
                  <button type="submit" name="button" class="btn btn-primary btn-sm">Simpan</button>
                </div>
              </div>

            </div>
        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function(){
    $("#dashboard_nav").removeAttr('href');
    $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/directur/dashboard')}}');
    $("#cartItem").removeAttr('href');
    $("a#cartItem").attr('href','{{URL::to('catalog/users/directur/cart')}}');
    $("#cart").css('display','none');
    $("#history").removeAttr('href');
    $("a#history").attr('href','{{URL::to('catalog/users/directur/history')}}');
    $("#approval").removeAttr('href');
    $("a#approval").attr('href','{{URL::to('catalog/users/directur/payment_approved')}}');
    $("a#approval").attr('text', 'Pembayaran');
    $("#tracking").removeAttr('href');
    $("a#tracking").attr('href','{{URL::to('catalog/users/directur/track')}}');
  });

  function Convert(a) {

    $('.keterangan').css('display', 'block');
    $('.oke').css('display', 'block');
    $('input.sisa_hasil').val(0)
    $('input.hasil').val(0)
    $('input.satuan').val("-")

    var id_detail = $(a).attr('data-id_detail');
    var qty_per_pcs = $('.qty_'+id_detail).val();
    var convert_pcs_to = $(a).find(':selected').attr('data-convert');
    console.log("hai: "+convert_pcs_to);
    var convert_pcs_id = $(a).find(':selected').val();
    var convert_pcs_awal_id = $('.satuan_awal_'+id_detail).attr('data-id');
    var convert_pcs_awal = $('.satuan_awal_'+id_detail).attr('data-convert');

    console.log(id_detail);
    console.log(convert_pcs_id);

    if(convert_pcs_id != convert_pcs_awal_id){
      sisa = qty_per_pcs%convert_pcs_to;
        if(convert_pcs_id == 2){
          nilai = Math.floor(qty_per_pcs*convert_pcs_awal);
          console.log("qty_per_pcs"+qty_per_pcs);
          console.log("convert_pcs_awal"+convert_pcs_awal);
        }else{
          nilai = Math.floor(qty_per_pcs/convert_pcs_to);
          console.log("qty_per_pcs"+qty_per_pcs);
          console.log("convert_pcs_to"+convert_pcs_to);
        }
      }else{
        if(convert_pcs_id == convert_pcs_awal_id){
          nilai = qty_per_pcs;
        }else {
          nilai = Math.floor(qty_per_pcs/convert_pcs_to);
        }
      }

    $('input.hasil_'+id_detail).val(nilai)
    $('.qty_label_'+id_detail).html(nilai)
  }

  </script>
@endsection
