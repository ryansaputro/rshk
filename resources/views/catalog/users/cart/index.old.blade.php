@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  <style media="screen">
  * {
    box-sizing: border-box;
  }

  .chat_window {
    position: absolute;
    width: calc(100% - 20px);
    max-width: 800px;
    height: 500px;
    border-radius: 10px;
    background-color: #fff;
    left: 50%;
    top: 50%;
    transform: translateX(-50%) translateY(-50%);
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.15);
    background-color: #f8f8f8;
    overflow: hidden;
  }

  .top_menu {
    background-color: #fff;
    width: 100%;
    padding: 20px 0 15px;
    box-shadow: 0 1px 30px rgba(0, 0, 0, 0.1);
  }
  .top_menu .buttons {
    margin: 3px 0 0 20px;
    position: absolute;
  }
  .top_menu .buttons .button {
    width: 16px;
    height: 16px;
    border-radius: 50%;
    display: inline-block;
    margin-right: 10px;
    position: relative;
  }
  .top_menu .buttons .button.close {
    background-color: #f5886e;
  }
  .top_menu .buttons .button.minimize {
    background-color: #fdbf68;
  }
  .top_menu .buttons .button.maximize {
    background-color: #a3d063;
  }
  .top_menu .title {
    text-align: center;
    color: #bcbdc0;
    font-size: 20px;
  }

  .messages {
    position: relative;
    list-style: none;
    padding: 20px 10px 0 10px;
    margin: 0;
    height: 347px;
    overflow: scroll;
  }
  .messages .message {
    clear: both;
    overflow: hidden;
    margin-bottom: 20px;
    transition: all 0.5s linear;
    opacity: 0;
  }
  .messages .message.left .avatar {
    background-color: #f5886e;
    float: left;
  }
  .messages .message.left .text_wrapper {
    background-color: #ffe6cb;
    margin-left: 20px;
  }
  .messages .message.left .text_wrapper::after, .messages .message.left .text_wrapper::before {
    right: 100%;
    border-right-color: #ffe6cb;
  }
  .messages .message.left .text {
    color: #c48843;
  }
  .messages .message.right .avatar {
    background-color: #fdbf68;
    float: right;
  }
  .messages .message.right .text_wrapper {
    background-color: #c7eafc;
    margin-right: 20px;
    float: right;
  }
  .messages .message.right .text_wrapper::after, .messages .message.right .text_wrapper::before {
    left: 100%;
    border-left-color: #c7eafc;
  }
  .messages .message.right .text {
    color: #45829b;
  }
  .messages .message.appeared {
    opacity: 1;
  }
  .messages .message .avatar {
    width: 60px;
    height: 60px;
    border-radius: 50%;
    display: inline-block;
  }
  .messages .message .text_wrapper {
    display: inline-block;
    padding: 20px;
    border-radius: 6px;
    width: calc(100% - 85px);
    min-width: 100px;
    position: relative;
  }
  .messages .message .text_wrapper::after, .messages .message .text_wrapper:before {
    top: 18px;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }
  .messages .message .text_wrapper::after {
    border-width: 13px;
    margin-top: 0px;
  }
  .messages .message .text_wrapper::before {
    border-width: 15px;
    margin-top: -2px;
  }
  .messages .message .text_wrapper .text {
    font-size: 18px;
    font-weight: 300;
  }

  .bottom_wrapper {
    position: relative;
    width: 100%;
    background-color: #fff;
    padding: 20px 20px;
    position: absolute;
    bottom: 0;
  }
  .bottom_wrapper .message_input_wrapper {
    display: inline-block;
    height: 50px;
    border-radius: 25px;
    border: 1px solid #bcbdc0;
    width: calc(100% - 160px);
    position: relative;
    padding: 0 20px;
  }
  .bottom_wrapper .message_input_wrapper .message_input {
    border: none;
    height: 100%;
    box-sizing: border-box;
    width: calc(100% - 40px);
    position: absolute;
    outline-width: 0;
    color: gray;
  }
  .bottom_wrapper .send_message {
    width: 140px;
    height: 50px;
    display: inline-block;
    border-radius: 50px;
    background-color: #a3d063;
    border: 2px solid #a3d063;
    color: #fff;
    cursor: pointer;
    transition: all 0.2s linear;
    text-align: center;
    float: right;
  }
  .bottom_wrapper .send_message:hover {
    color: #a3d063;
    background-color: #fff;
  }
  .bottom_wrapper .send_message .text {
    font-size: 18px;
    font-weight: 300;
    display: inline-block;
    line-height: 48px;
  }

  .message_template {
    display: none;
  }

  .close {
    float: right;
    font-size: 17px;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #ffffff;
    filter: alpha(opacity=20);
    /* opacity: .2; */
    padding-left: 3px;
}

  </style>
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    {{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Shopping cart</h1>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
            @php
              $id = "";
            @endphp
            @foreach ($dataCart as $key => $value)
              {{$id}} == {{$value->id_vendor_detail}}<br>
            @if($id !== $value->id_vendor_detail)
              <table class="table table-bordered">
            <tr>
              <td>no {{$value->id_vendor_detail}}</td>
              <td>tes</td>
            </tr>
          </table>
            @endif
            @php
            $id = $value->id_vendor_detail;
            @endphp
          @endforeach
        @foreach ($dataCart as $key => $value)
          @if($id !== $value->id_vendor_detail)
            <hr>
        <table summary="Shopping cart" id="mainCart">
          <tr>
            <th class="goods-page-image">Image</th>
            <th class="goods-page-description">Description</th>
            <th class="goods-page-ref-no">Ref Kode</th>
            <th class="goods-page-quantity">Quantity</th>
            <th class="goods-page-price">Unit price</th>
            <th class="goods-page-price">Shipment</th>
            <th class="goods-page-total" colspan="2">Total</th>
          </tr>
          @php
            $subtot=0;
            $jumlahData = count($dataCart);
          @endphp
          <tbody>
          @if(count($dataCart) > 0)
          @foreach ($dataCart as $key => $value)
            <tr class="dataTr tr_{{$value->id}}" data-id="{{$value->id}}">
              <td class="goods-page-image">
                <a href="javascript:;"><img class="imageProduct" src="/assets/catalog/item/{{ $value->file }}" alt=""></a>
              </td>
              <td class="goods-page-description">
                <h3>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    {{ $value->name }}
                  </a>
                </h3>
                <p><strong>Merk</strong> - {{ $value->merk }}</p>
                <em>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    More info is here!
                  </a>
                  <a href="#" onclick="chatOnline(this)" data-id_item="{{$value->id_item}}" data-id_cart="{{$value->id}}" data-vendor="{{$value->id_vendor_detail}}"  class="btn btn-xs btn-success pull-right" style="text-decoration: none; color: white;">
                    <i class="fa fa-comments"></i>
                    Chat Supplier
                  </a>
                </em>
              </td>
              <td class="goods-page-ref-no">
                <h3>
                  <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                    {{ $value->code }}
                    <input type="hidden" name="id_item[{{$value->id}}]" value="{{$value->id_item}}">
                  </a>
                </h3>
                <p>
                  <strong>Penyedia</strong> -
                  {{ DB::table('supplier_item')->where('id_item', $value->id_item)->join('vendor_detail', 'vendor_detail.id', '=', 'supplier_item.id_supplier')->value('vendor_name') }}
                </p>
                <p><strong>Price</strong> - {{ date('l, d F Y', strtotime($value->price_date)) }}</p>
              </td>
              <td class="goods-page-quantity">
                <div class="product-quantity">
                    <input id="product-quantity" style="width:50px;" name="qty[{{$value->id}}]" onchange="InputQty(this)" type="text" value="{{$value->qty}}" data-id="{{$value->id}}" readonly class="form-control input-sm qty qty_{{$value->id}}">
                </div>
              </td>
              <td class="goods-page-price">
                @php
                if ($value->price_country == 0)
                  $price =$value->price_gov;
                else
                  $price = $value->price_gov;
                @endphp
                <strong>
                  <input type="hidden" name="price_pcs[{{$value->id}}][]" value="{{$price}}" class="price_pcs" data-id="{{$value->id}}">
                  <span>
                    {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                    {{number_format($price, 0, ',', '.')}}
                  </span>
                </strong>
              </td>
              <td class="goods-page-price">
                @php
                if ($value->price_country == 0)
                  $price =$value->price_gov;
                else
                  $price = $value->price_gov;
                @endphp
                <strong>
                  <input type="hidden" name="price_shipment[{{$value->id}}][]" value="{{$value->price_shipment}}" class="price_shipment" data-id="{{$value->id}}">
                  <span><a href="{{ URL::to('catalog/users/item/chat/'.$value->id) }}" class="btn btn-xs btn-info pull-right hide" style="text-decoration: none; color: white;">
                    Chat In Here
                  </a>
                    {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                    {{number_format($value->price_shipment, 0, ',', '.')}}
                  </span>
                </strong>
              </td>
              <td class="goods-page-total">
                <input type="hidden" name="total[{{$value->id}}][]" value="{{$price*$value->qty}}" class="total total_{{$value->id}}">
                <strong>
                  {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                  <span data-value="{{$price*$value->qty}}" class="total_tag total_tag_{{$value->id}}">
                    {{number_format(($price*$value->qty)+$value->price_shipment, 0, ',', '.')}}
                  </span>
                </strong>
              </td>
              <td class="del-goods-col">
                <a class="del-goods" href="javascript:;" onclick="DeleteCart(this)" data-id="{{$value->id}}">&nbsp;</a>
              </td>
            </tr>
            @php
              $tot = (($price*$value->qty)+$value->price_shipment);
              $subtot += $tot;
            @endphp
          @endforeach
        @else
          <tr>
            <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
          </tr>
        @endif
        </tbody>
        </table>
      @endif
      @php
      $id = $value->id_vendor_detail;
      @endphp
    @endforeach
        </div>
        <div class="shopping-total" style="width: 300px;">
          <ul>
            <li>
              <em>Total</em>
              <input type="hidden" name="subtot" value="{{$subtot}}" id="input_subtot">
              <strong class="price">
                Rp
                <span id="subtot">{{number_format($subtot, 0, ',', '.')}}</span>
              </strong>
            </li>
            {{-- <li>
              <em>Shipping Cost <small>PPN 10%</small></em>

              <strong class="price"><span>Rp </span>xxx</strong>
            </li> --}}
            {{-- <li class="shopping-total-price">
              <em>Total</em>
              <strong class="price"><span>Rp </span>xxx</strong>
            </li> --}}
          </ul>
        </div>
      </div>
      <a href="{!! route('x.catalog.users.item') !!}" class="btn btn-primary" style="float: left;">Continue shopping <i class="fa fa-shopping-cart"></i></a>
      {{-- <a href="{!! route('x.catalog.users.chat') !!}" class="btn btn-primary" style="float: left;">Chat <i class="fa fa-comments"></i></a> --}}
      {{-- <button class="btn btn-primary" type="submit" style="float: left;">Continue shopping <i class="fa fa-shopping-cart"></i></button> --}}
      <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="confirm">Checkout <i class="fa fa-check"></i></button>
    </div>
  </div>

  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog" style="z-index:100000000000 !important;">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <form class="" action="{{URL::to('catalog/users/order')}}" method="post">
            {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Checkout</h4>
          </div>
          <div class="modal-body" id="DivIdToPrint">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped" style="width: 100%">
                  <thead>
                    <tr>
                      <th colspan="2" style="text-transform: uppercase; width: 50%;">Pembeli</th>
                      <th colspan="2" style="text-transform: uppercase;">Supervisi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($dataCart) > 0)
                    <tr>
                      <td>Nama</td>
                      <td style="text-transform: capitalize;">: {{$pembeli->username_catalog}}</td>
                      <td>Nama</td>
                      <td style="text-transform: capitalize;">: {{$supervisi->name}}</td>
                    </tr>
                    <tr>
                      <td>No Tlp</td>
                      <td>: {{$pembeli->telephone}}</td>
                      <td>No Tlp</td>
                      <td>: {{$supervisi->mobile_phone}}</td>
                    </tr>
                    <tr>
                      <td>E-Mail</td>
                      <td>: {{$pembeli->email}}</td>
                      <td>E-Mail</td>
                      <td>: {{$supervisi->email}}</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
            {{-- <div class="row hide">
              <div class="col-xs-6">
                <address>
                  <strong>Pembeli</strong><br>
                  @if(count($dataCart) > 0)
                  {{$pembeli->username_catalog}}<br>
                  {{$pembeli->telephone}}<br>
                  {{$pembeli->email}}<br>
                  @endif
                </address>
              </div>
              <div class="col-xs-6 text-right">
                <address>
                  <strong>Supervisi</strong><br>
                  @if(count($dataCart) > 0)
                  {{$supervisi->name}}<br>
                  {{$supervisi->mobile_phone}}<br>
                  {{$supervisi->email}}<br>
                  @endif
                </address>
              </div>
            </div> --}}
            <div class="row">
              <div class="col-md-12">
                <div class="table-wrapper-responsive">
                  <table class="table table-bordered" id="cartConfirm" border="1px;">
                    <thead>
                      <tr>
                        <th class="goods-page-image">Image</th>
                        <th class="goods-page-description">Description</th>
                        <th class="goods-page-ref-no">Ref Kode</th>
                        <th class="goods-page-quantity" style="width:20%;">Quantity</th>
                        <th class="goods-page-price">Unit price</th>
                        <th class="goods-page-total">Total</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                  <div class="shopping-total Modal" style="width: 300px;">
                    <ul>
                      <li>
                        <em>Total</em>
                        <input type="hidden" name="subtot" value="{{$subtot}}" id="input_subtot">
                        <strong class="price">
                          Rp
                          <span id="subtot">{{number_format($subtot, 0, ',', '.')}}</span>
                        </strong>
                      </li>
                      {{-- <li>
                        <em>Shipping Cost <small>PPN 10%</small></em>

                        <strong class="price"><span>Rp </span>xxx</strong>
                      </li> --}}
                      {{-- <li class="shopping-total-price">
                        <em>Total</em>
                        <strong class="price"><span>Rp </span>xxx</strong>
                      </li> --}}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id='btn' value='Print' onclick='printDiv();' target="_blank">Print</button>
            <button type="submit" class="btn btn-success">Konfirmasi</button>
          </div>
        </form>
        </div>

      </div>
    </div>

    <div class="chat_window hide" style="z-index:99999999999999;">
       <div class="top_menu">
          <div class="buttons">
             <div class="button close"></div>
             <div class="button minimize"></div>
             <div class="button maximize"></div>
          </div>
          <div class="title">Chat</div>
       </div>
       <ul class="messages">
         <li class="message left appeared pesanChat">
          </li>
         <li class="message right appeared pesanChatRight">
          </li>
      </ul>
       <div class="bottom_wrapper clearfix">
          <div class="message_input_wrapper"><input class="message_input" placeholder="Type your message here..." /></div>
          <div class="send_message">
             <div class="icon"></div>
             <div class="text">Send</div>
          </div>
       </div>
    </div>
    <div class="message_template">
       <li class="message">
          <div class="avatar"></div>
          <div class="text_wrapper">
             <div class="text"></div>
          </div>
       </li>
    </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript">
// (function () {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };
        sendMessage = function (text) {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            message_side = message_side === 'left' ? 'left' : 'right';
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };


        $('.send_message').click(function (e) {
            $.ajax({
                type: "post",
                url: "{{URL::to('catalog/users/Addchat')}}",
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id_vendor_detail": $(this).attr('data-id'),
                  "id_cart": $(this).attr('data-id_cart'),
                  "id_item": $(this).attr('data-id_item'),
                  "message": getMessageText(),
                  "id_user": "{{Auth::user()->id}}",
                  },
                success: function (a) {
                  var txtx = "";
                  if(a.status == 1){
                    txtx  += '<div class="avatar">Pembeli</div>';
                    txtx  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                    txtx  +=           '   <div class="text">'+getMessageText()+'<p style="font-size:12px; color:#000;">a few minute ago</p></div>';
                    txtx  +=           '</div>';
                    $('li.pesanChatRight').append(txtx);
                    $('.message_input').val('');

                  }else{
                    alert("Gagal Kirim Chat!")
                  }
                }
              });
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                $('.send_message').trigger('click');
            }
        });
    });

function chatOnline(a) {
  var vendor = $(a).attr('data-vendor');
  var id_item = $(a).attr('data-id_item');
  var id_cart = $(a).attr('data-id_cart');
  $('.chat_window').addClass('show')
  $.ajax({
      type: "post",
      url: "{{URL::to('catalog/users/chat')}}",
    data: {
        "_token": "{{ csrf_token() }}",
        "id_vendor_detail": vendor,
        "id_user": "{{Auth::user()->id}}",
        },
      success: function (a) {
        console.log(a)
        var txt = "";
        var txtx = "";
        jumlah = a.data.length;
        if(jumlah >= 1){
            $.each(a.data, function(k,v){
              if(v.status == 0){
                txt  += '<div class="avatar">'+a.name+'</div>';
                txt  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                txt  +=           '   <div class="text">'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                txt  +=           '</div>';
                $('li.pesanChat').html(txt);
              }else if(v.status == 1){
                txtx  += '<div class="avatar">Pembeli</div>';
                txtx  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                txtx  +=           '   <div class="text">'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                txtx  +=           '</div>';
                $('li.pesanChatRight').html(txtx);
              }
          });
      }else{
        $('li.pesanChatRight').html("");
        $('li.pesanChat').html("");
      }

        $('.send_message').attr('data-id',vendor);
        $('.send_message').attr('data-id_item',id_item);
        $('.send_message').attr('data-id_cart',id_cart);

      }
    });
  // $(a).attr("data-toggle", "modal");
  // $(a).attr("data-target", "#myChat");
}

$('.close').on('click', function(){
  $('.chat_window').removeClass('show')
  $('.chat_window').addClass('hide')
})

$(document).ready(function() {
  $('.keranjang').attr('style','display:none !important;');
  $("div#notif").hide();
  var data = "{{$jumlahData}}";
  if(data == 0)
    $("#confirm").css("display","none");
  else
  $("#confirm").css("display","display");
});

function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
        $.each($('.price_shipment'), function(x,y){
          var ShipId = $(y).attr('data-id');
          if(ShipId == nilai_tr){
            var ship = parseInt($(y).val());
            total = ((nilai*price)+ship);
          }
        });
        $('.total_'+priceId).attr('value', Math.ceil(total))
        $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

        $.each($('.total'), function(k,v){
          sub = $(v).val();
          subtot += Number(sub);
        })
          $('#subtot').html(addCommas(subtot));
          $('#input_subtot').html(subtot);
          $('#input_subtot').attr("value",subtot);
          $(".Modal span#subtot").html(addCommas(subtot));
          $(".Modal input#input_subtot").attr("value",subtot);
      }
    });

}


$("#confirm").on('click', function(){
  $("#cartConfirm").html($('#mainCart').html());
  $("#cartConfirm a.del-goods").removeAttr("onclick");
  $("#cartConfirm a.del-goods").html("-");
  $("#cartConfirm a.del-goods").removeClass();
  $("#cartConfirm td.del-goods-col").css('display','none');
  $("#cartConfirm .imageProduct").css('width','100%');
});

function DeleteCart(a) {
  var r = confirm("Apakah Anda Yakin ingin Hapus?");
    if (r == true) {
      var id_cart = $(a).attr('data-id');
      var url = "{{URL::to('catalog/users/delete_cart')}}";
        $.ajax({
            type: "post",
            url: url,
          data: {
              "_token": "{{ csrf_token() }}",
              "id_cart": id_cart,
              },
            success: function (a) {
              $('.tr_'+a.id_cart).remove();
              if(a.status == 1){
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-success");
                $("#status").html("Sukses!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }else{
                $("div#notif").removeClass();
                $("div#notif").addClass("alert alert-danger");
                $("#status").html("Gagal!");
                $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                 $("#notif").slideUp(500);
                  });
                $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                $('html, body').animate({scrollTop:0}, 'slow');
              }
            }
        });
    } else {
        location.reload();
    }
}

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}


$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("#dashboard_nav").attr('href','{{URL::to('catalog/users/')}}');
  $("#cartItem").removeAttr('href');
  $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
  // $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("#history").attr('href','{{URL::to('catalog/users/history')}}');
  $("#approval").removeAttr('href');
  $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
});
</script>
@endsection
