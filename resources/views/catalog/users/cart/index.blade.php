@extends('catalog.users.layouts.app')
@section('title')
Catalog
@endsection
@section('plugins')
@endsection
@section('css')

<link href="/su_catalog/assets/frontend/pages/css/chat.css" rel="stylesheet" type="text/css">
{{-- <link href="/su_vms/assets/users/css/chatting_custom.css" rel="stylesheet" type="text/css"> --}}
<link href="/su_catalog/assets/frontend/pages/css/loading.css" rel="stylesheet" type="text/css">

@endsection
@section('xcss')
@endsection
@section('logo')
<a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
{{-- <img src="/su_catalog/assets/frontend/layout/img/logos/logo-shop-blue.png" alt="Metronic Shop UI"> --}}
RSHK
</a>
@endsection
@section('content')
  @php
  $id = "";
  $id_contract = "";
  $id_vendor = "";
  $rowspan = 0;
  $totalAll =0;
  $jumlahData = count($dataCart);
  use App\Model\ItemImage;
  @endphp
<div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="col-md-6">
        <h1>Keranjang</h1>
      </div>
    </div>
   <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong>
      <p id="kata_status"></p>
   </div>
   @if(session()->has('message'))
   <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">x</button>
      {{ session()->get('message') }}
   </div>
   @endif
@if (count($dataCart) > 0)
   @php
    foreach ($dataCart as $key => $value) {
          $kode_kontrak[] = $value->id_contract;
          $kode_penyedia[$value->id_contract] = $value->id_vendor_detail;
          $nama_penyedia[$value->id_contract] = $value->vendor_name;
          $id_contract = $value->id_contract;
    }
    $data = array_count_values($kode_kontrak);
   @endphp


         <div class="goods-page">
            <div class="goods-data clearfix">
               <div class="table-wrapper-responsive">
                 <div class="row">
                 </div>
                        @foreach ($data as $k => $v)
                          @php
                          $subtot=0;
                          @endphp
                      <table class="mainCart table table-stripped">
                      <tbody>
                        <tr>
                          <td colspan="9">
                            {{-- id_cart == id kontrak --}}
                            <a href="#" class="percakapan_{{$k}}" onclick="chatOnline(this)" data-id_cart="{{$k}}" data-vendor="{{$kode_penyedia[$k]}}"  style="text-decoration: none; color: #68aed8;">
                              <i class="fa fa-comments fa-2x"></i>
                              {{$nama_penyedia[$k]}}
                            </a>
                            <input type="hidden" name="id_vendor_detail[{{$k}}]" value="{{ DB::table('contract')->select('vendor_detail.id')->join('vendor_detail', 'vendor_detail.id', '=', 'contract.id_vendor_detail')->where('contract.id', $k)->value('id') }}">
                          </td>
                        </tr>
                          <tr>
                             <th class="goods-page-image">Gambar</th>
                             <th class="goods-page-description" style="width:20%;">Deskripsi</th>
                             <th class="goods-page-ref-no">Ref Kode</th>
                             <th class="goods-page-quantity">Qty</th>
                             <th class="goods-page-satuan">Satuan</th>
                             <th class="goods-page-price">Harga</th>
                             <th class="goods-page-price">Biaya Kirim</th>
                             <th class="goods-page-total" style="text-align:center;">Total</th>
                             <th class="goods-page-action actions"></th>
                          </tr>

                        @if(count($dataCart) > 0)
                          @foreach ($dataCart as $key => $value)
                            @php
                            $gambar = ItemImage::where('id_item', $value->id)->value('file');
                            @endphp
                            @if($value->id_contract == $k)
                                <tr class="dataTr tr_{{$value->id}}" data-id="{{$value->id}}">
                                </td>
                                <td class="goods-page-image">
                                  @if ($gambar == null)
                                    <img src="/assets/catalog/item/lost.png" class="img-responsive" alt="">
                                  @else
                                    @php
                                      // $gambar = $value->file;
                                      $imp_gmb = explode('/', $gambar);
                                    @endphp
                                      {{-- <img src="{{(($imp_gmb[0] == 'https:') || ($imp_gmb[0] == 'http:')) ? $gambar : '/assets/catalog/item/'.$value->file}}" class="img-responsive" alt=""> --}}
                                  @endif
                                </td>
                                <td class="goods-page-description">
                                   <h3>
                                      <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                                      <b>{{ $value->name }}</b>
                                      </a>
                                   </h3>
                                   <p><strong>Merk</strong> - {{ $value->merk }}</p>
                                   <em>
                                   <a href="{{ URL::to('catalog/users/item/detail/'.$value->id_item) }}" style="text-decoration: none;">
                                   Lainnya!
                                   </a>
                                   </em>
                                </td>
                                <td class="goods-page-ref-no">
                                   <h3>
                                      <a href="{{ URL::to('catalog/users/item/detail/'.$value->id) }}" style="text-decoration: none;">
                                      <b>{{ $value->code }}</b>
                                      <input type="hidden" name="id_item[{{$k}}][{{$value->id_item}}]" value="{{$value->id_item}}">
                                      </a>
                                   </h3>
                                   <p>
                                      <strong>Penyedia</strong> -
                                      {{ DB::table('supplier_item')->where('id_item', $value->id_item)->join('vendor_detail', 'vendor_detail.id', '=', 'supplier_item.id_supplier')->value('vendor_name') }}
                                   </p>
                                   <p><strong>Harga</strong> -
                                     @php
                                     $time = strtotime($value->price_date);
                                     $functionDay =  Fungsi::DayIndonesia();
                                     $function =  Fungsi::MonthIndonesia();
                                   @endphp
                                     {{$functionDay[date('l', $time)]}}, {{date('d', $time)}} {{$function[date('m', $time)]}} {{date('Y', $time)}}</p>
                                </td>
                                <td class="goods-page-quantity">
                                   <div class="">
                                      <input id="" max="{{$value->stock}}" min="0" style="width:100px;" name="qty[{{$k}}][{{$value->id_item}}]" oninput="InputQty(this)" type="number" value="{{$value->qty}}" data-vendor="{{$k}}" data-id="{{$value->id}}" class="form-control qty qty_{{$value->id}}">
                                   </div>
                                </td>
                                <td>{{$value->unit_name}}</td>
                                <td class="goods-page-price" style="text-align:right;">
                                   @php
                                   if ($value->price_country == 0)
                                   $price =$value->price_gov;
                                   else
                                   $price = $value->price_gov;
                                   @endphp
                                   <strong>
                                   <input type="hidden" name="price_pcs[{{$k}}][]" value="{{$price}}" class="price_pcs" data-id="{{$value->id}}" data-id_contract="{{$k}}">
                                   <span>
                                   {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                                   {{number_format($price, 0, ',', '.')}}
                                   </span>
                                   </strong>
                                </td>
                                <td class="goods-page-price" style="text-align:right;">
                                   @php
                                   if ($value->price_country == 0)
                                   $price =$value->price_gov;
                                   else
                                   $price = $value->price_gov;
                                   @endphp
                                   <strong>
                                   <input type="hidden" name="price_shipment[{{$k}}][]" value="{{$value->price_shipment}}" class="price_shipment" data-id="{{$value->id}}">
                                   {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                                   {{number_format($value->price_shipment, 0, ',', '.')}}
                                   </span>
                                   </strong>
                                </td>
                                <td class="goods-page-total" style="text-align:right;">
                                   <input type="hidden" name="total[{{$k}}][{{$value->id_item}}]" data-id="{{$value->id}}" data-id_contract="{{$k}}" value="{{($price*$value->qty)+$value->price_shipment}}" class="total total_{{$value->id}}">
                                   <strong>
                                   {{ $value->price_country == 0 ? 'Rp ' : '$ ' }}
                                   <span data-value="{{($price*$value->qty)+$value->price_shipment}}" data-id="{{$value->id}}" class="total_tag total_tag_{{$value->id}}">
                                   {{number_format(($price*$value->qty)+$value->price_shipment, 0, ',', '.')}}
                                   </span>
                                   </strong>
                                </td>
                                <td class="del-goods-col">
                                   <a class="del-goods" href="javascript:;" onclick="DeleteCart(this)" data-id="{{$value->id}}">&nbsp;</a>
                                </td>

                              </tr>
                              @php
                              $tot = (($value->price_gov*$value->qty)+$value->price_shipment);
                              $subtot += $tot;
                              @endphp

                            @endif
                            @php
                            $id_contract = $value->id_contract;
                            $id_vendor = $value->id_vendor_detail;
                            @endphp
                          @endforeach

                          </tbody>
                          @else
                            <tr>
                              <td colspan="7" style="text-align:center;">Tidak Ada Data</td>
                            </tr>
                          @endif
                        </table>
                        <div class="shopping-total" style="width: 400px;">
                           <ul>
                              <li>
                                 <em>
                                  <font style="padding-right:75px; font-size:12px;">
                                    <b>
                                      Sub total &nbsp
                                    </b>
                                  </font>
                                   <input type="hidden" name="subtot[{{$k}}]" data-id="{{$k}}" value="{{$subtot}}" class="input_subtot input_subtot_{{$k}}">
                                   <strong class="price" style="margin-right:60px;">
                                   Rp
                                   <span class="subtot subtot_{{$k}}">{{number_format($subtot, 0, ',', '.')}}</span>
                                   </strong>
                                 </em>
                              </li>
                           </ul>
                        </div>
                        @php
                        $totalAll += $subtot;
                        @endphp

                @endforeach
              </div>
            </div>
          </div>
    @endif

  <div class="goods-page">
    <div class="goods-data clearfix">
      <div class="shopping-total allTotal" style="width: 400px;">
        <ul>
          <li>
            <em>
              <font style="padding-right:75px; font-size:12px;">
                <b>
                  Total ({{$jumlahData}} Item) &nbsp
                </b>
              </font>

              <input type="hidden" name="totalAll" value="{{$totalAll}}" class="totalAll">
              <strong class="price" style="margin-right:60px;">
                Rp
                <span class="totalAll">{{number_format($totalAll, 0, ',', '.')}}</span>
              </strong>
            </em>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
    <a href="{!! route('x.catalog.users.item') !!}" class="btn btn-primary btn-sm" style="float: left;"><i class="fa fa-shopping-cart"></i> Belanja</a>
    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" id="confirm">Lanjutkan <i class="fa fa-check"></i></button>
  </div>
  </div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" style="z-index:100000000000 !important;">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <form class="" action="{{URL::to('catalog/users/order')}}" method="post">
            {{ csrf_field() }}
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Ringkasan</h4>
            </div>
            <div class="modal-body" id="DivIdToPrint">
               {{-- <div class="row">
                  <div class="col-md-12">
                     <table class="table table-striped" style="width: 100%">
                        <thead>
                           <tr>
                              <th colspan="2" style="text-transform: uppercase; width: 50%;">Pembeli</th>
                              <th colspan="2" style="text-transform: uppercase;">Supervisi</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(count($dataCart) > 0)
                           <tr>
                              <td>Nama</td>
                              <td style="text-transform: capitalize;">: {{$pembeli->username_catalog}}</td>
                              <td>Nama</td>
                              <td style="text-transform: capitalize;">: {{$supervisi->name}}</td>
                           </tr>
                           <tr>
                              <td>No Tlp</td>
                              <td>: {{$pembeli->telephone}}</td>
                              <td>No Tlp</td>
                              <td>: {{$supervisi->mobile_phone}}</td>
                           </tr>
                           <tr>
                              <td>E-Mail</td>
                              <td>: {{$pembeli->email}}</td>
                              <td>E-Mail</td>
                              <td>: {{$supervisi->email}}</td>
                           </tr>
                           <tr>
                              <th>Budget</th>
                              <th>: Rp. {{number_format($budgetYear, 0, ',', '.')}}</th>
                              <th>Sisa Budget</th>
                              <th>: Rp. {{number_format($budgetYear-$budgetUsed, 0, ',', '.')}}</th>
                           </tr>
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div> --}}

               <div class="row">
                  <div class="col-xs-6">
                     <address>
                        <strong>Pembeli</strong><br>
                        @if(count($dataCart) > 0)
                        {{$pembeli->username_catalog}}<br>
                        {{$pembeli->telephone == '' ? '-' : $pembeli->telephone}}<br>
                        {{$pembeli->email}}<br>
                        @endif
                     </address>
                  </div>
                  <div class="col-xs-6 text-right">
                     <address>
                        <strong>Supervisi</strong><br>
                        @if(count($dataCart) > 0)
                        {{$supervisi->name}}<br>
                        {{$supervisi->mobile_phone}}<br>
                        {{$supervisi->email}}<br>
                        @endif
                     </address>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-12">
                     <div class="table-wrapper-responsive">
                        <table class="table table-bordered" id="cartConfirm" border="1px;" style="width:100%;">
                           <thead>
                              <tr>
                                 <th class="goods-page-image">Image</th>
                                 <th class="goods-page-description">Description</th>
                                 <th class="goods-page-ref-no">Ref Kode</th>
                                 <th class="goods-page-quantity" style="width:20%;">Quantity</th>
                                 <th class="goods-page-price">Unit price</th>
                                 <th class="goods-page-total">Total</th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                      <div class="subtot">
                      </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary" id='btn' value='Print' onclick='printDiv();' target="_blank">Print</button>
               <button type="submit" class="btn btn-success konfirmasi">Konfirmasi</button>
            </div>
         </form>
      </div>
   </div>
</div>


<div class="chat_window hide" style="z-index:99999999999999;">
   <div class="top_menu">
      <div class="buttons">
         <div class="button close"></div>
         {{-- <div class="button minimize"></div>
         <div class="button maximize"></div> --}}
      </div>
      <div class="title">Chat</div>
   </div>
   <div class="pesanan" style="overflow-y: scroll;height: 100px !important;">
     <div class="table-responsive">
       <table class="table table-striped psn">
         <thead>
           <tr>
             <td>No</td>
             <td>Nama Item</td>
           </tr>
         </thead>
         <tbody>
         </tbody>
       </table>
     </div>

   </div>
   <ul class="messages">
      {{-- <li class="message left appeared pesanChat">
      </li>
      <li class="message right appeared pesanChatRight">
      </li> --}}
   </ul>
   <div class="bottom_wrapper clearfix">
      <div class="message_input_wrapper"><input class="message_input" placeholder="Type your message here..." /></div>
      <div class="send_message">
         <div class="icon"></div>
         <div class="text">Send</div>
      </div>
   </div>
</div>

<div class="message_template">
   <li class="message">
      <div class="avatar"></div>
      <div class="text_wrapper">
         <div class="text"></div>
      </div>
   </li>
</div>

<div class="modal"></div>
@endsection
@section('note')
@endsection
@section('product-pop-up')
@endsection
@section('js')

<script type="text/javascript">
$('.konfirmasi').on('click', function(){
  $(this).addClass('buttonload');
  $(this).html('<i class="fa fa-spinner fa-spin"></i>Loading');
  // $(this).attr('disabled', 'disabled');
})


   // (function () {
       var Message;
       Message = function (arg) {
           this.text = arg.text, this.message_side = arg.message_side;
           this.draw = function (_this) {
               return function () {
                   var $message;
                   $message = $($('.message_template').clone().html());
                   $message.addClass(_this.message_side).find('.text').html(_this.text);
                   $('.messages').append($message);
                   return setTimeout(function () {
                       return $message.addClass('appeared');
                   }, 0);
               };
           }(this);
           return this;
       };

       $(function () {
           var getMessageText, message_side, sendMessage;
           message_side = 'right';
           getMessageText = function () {
               var $message_input;
               $message_input = $('.message_input');
               return $message_input.val();
           };
           sendMessage = function (text) {
               var $messages, message;
               if (text.trim() === '') {
                   return;
               }
               $('.message_input').val('');
               $messages = $('.messages');
               message_side = message_side === 'left' ? 'left' : 'right';
               message = new Message({
                   text: text,
                   message_side: message_side
               });
               message.draw();
               $('.chat_window').removeClass('show')
               $('.chat_window').addClass('hide')

               return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);

           };


           $('.send_message').click(function (e) {
             var contract = $(this).attr('data-id_contract');
             var vendor = $(this).attr('data-id');

               $.ajax({
                 type: "post",
                 url: "{{URL::to('catalog/users/Addchat')}}",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "id_vendor_detail": $(this).attr('data-id'),
                     "id_contract": $(this).attr('data-id_contract'),
                     "message": getMessageText(),
                     "id_user": "{{Auth::user()->id}}",
                     },
                   success: function (a) {
                     var txtx = "";
                     if(a.status == 1){
                       $('div.close').trigger('click');
                       $('.percakapan_'+contract).trigger('click');
                       $('.message_input').val('');
                       $('.chat_window').animate({ scrollTop: $('.chat_window').scrollTop() }, 1000);

                     }else{
                       alert("Gagal Kirim Chat!")
                     }
                   }
                 });
           });

           $('.message_input').keyup(function (e) {
               if (e.which === 13) {
                   $('.send_message').trigger('click');
               }
           });
       });

   function chatOnline(a) {
     $('.chat_window').animate({ scrollTop: $('.chat_window').scrollTop() }, 1000);
     var vendor = $(a).attr('data-vendor');
     // var id_item = $(a).attr('data-id_item');
     var id_kontrak = $(a).attr('data-id_cart');
     $('.chat_window').addClass('show');
     $.ajax({
         type: "post",
         url: "{{URL::to('catalog/users/chat')}}",
       data: {
           "_token": "{{ csrf_token() }}",
           "id_vendor_detail": vendor,
           "id_contract": id_kontrak,
           "id_user": "{{Auth::user()->id}}",
           },
         success: function (a) {
           var txt = "";
           var txtx = "";
           jumlah = a.data.length;
           if(jumlah >= 1){
               $.each(a.data, function(k,v){

                 if(v.status == 0){
                   txtx  += '<li class="message left appeared pesanChat">';
                   txtx  += '<div class="avatar"><img class="logo" src="/assets/vendor.png" style="height: 65px;padding-right: 30px;" alt="Metronic Shop UI"></div>';
                   txtx  +=           '<div class="text_wrapper" style="margin-bottom:10px;">';
                   txtx  +=           '   <div class="text"><font style="color:red; font-size:12px;">'+a.name+'</font><br>'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                   txtx  +=           '</div>';
                   txtx  += '</li>';
                 }else if(v.status == 1){
                   txtx  += '<li class="message right appeared pesanChatRight">';
                   txtx  +=   '<div class="avatar"><img class="logo" src="/assets/logo.png" style="height: 75px;padding-right: 30px;" alt="Metronic Shop UI"></div>';
                   txtx  +=      '<div class="text_wrapper" style="margin-bottom:10px;">';
                   txtx  +=      '<div class="text"><font style="color:red; font-size:12px;">RSJPDHK</font><br>'+v.message+'<p style="font-size:12px; color:#000;">'+v.datetime+'</p></div>';
                   txtx  +=    '</div>';
                   txtx  += '</li>';

                 }

             });

             $('ul.messages').html(txtx)
         }else{
             $('li.pesanChatRight').html("");
             $('li.pesanChat').html("");
         }
           psn = ""
           $.each(a.dataItem, function(k,v){
             psn += "<tr><td>"+(parseInt(k)+1)+"</td><td>"+v.name+"</td></tr>";
           });

           $('table.psn tbody').html(psn)
           $('.send_message').attr('data-id',vendor);
           // $('.send_message').attr('data-id_item',id_item);
           $('.send_message').attr('data-id_contract',id_kontrak);

         }
       });
     // $(a).attr("data-toggle", "modal");
     // $(a).attr("data-target", "#myChat");
   }

   $('.close').on('click', function(){
     $('.chat_window').removeClass('show')
     $('.chat_window').addClass('hide')
   })

   $(document).ready(function() {
     // var budget = "{{$budgetYear}}";
     // var totalAll = $('.totalAll').val();
     // if(parseInt(totalAll) > parseInt(budget)){
     //   $('#confirm').css('display', 'none');
     // }else{
       // $('#confirm').css('display', 'display');
     // }
     // $('.keranjang').attr('style','display:none !important;');

     $("div#notif").hide();
     var data = "{{$jumlahData}}";
     if(data == 0)
       $("#confirm").css("display","none");
     else
     $("#confirm").css("display","display");
   });

   // var budget = "{{$budgetYear}}";

   function InputQty(a) {
     if(parseInt($(a).val()) > parseInt($(a).attr('max'))){
       $(a).val($(a).attr('max'))
     }else if($(a).val() == ''){
       $(a).val(1)
     }
     var subtot = 0;
     var nilai = $(a).val();
     var nilai_tr = $(a).attr('data-id');
     var vendorData = $(a).attr('data-vendor');
     var AllSubtot = 0;

     $(".qty_"+nilai_tr).removeAttr();
     $(".qty_"+nilai_tr).attr("value",nilai)

       $.each($('.price_pcs'), function(k,v){
         var priceId = $(v).attr('data-id');
         var kodeKontrak = $(v).attr('data-id_contract');
         if(priceId == nilai_tr){
           var price = $(v).val();
           $.each($('.price_shipment'), function(x,y){
             var ShipId = $(y).attr('data-id');
             if(ShipId == nilai_tr){
               var ship = parseInt($(y).val());
               total = ((nilai*price)+ship);
             }
           });

           $('.total_'+priceId).attr('value', Math.ceil(total))
           console.log("kode harga : "+priceId);
           $('.total_tag_'+priceId).html(addCommas(Math.ceil(total)))

           $.each($('.total'), function(k,v){
             sub = $(v).val();
             idVendor = $(v).attr('data-id_contract');
             if(vendorData == idVendor){
               subtot += Number(sub);
             }
             console.log("SUBTOT: "+sub);
             console.log("vendorData: "+vendorData);
             console.log("idVendor: "+idVendor);

             AllSubtot += Number(sub);
           })

             $('.input_subtot_'+vendorData).html(subtot);
             $('.input_subtot_'+vendorData).attr("value",subtot);
             $('.subtot_'+vendorData).html(addCommas(subtot));
             $('input.totalAll').attr("value", AllSubtot);
             $('span.totalAll').html(addCommas(AllSubtot));
             $(".Modal span#subtot").html(addCommas(subtot));
             $(".Modal input#input_subtot").attr("value",subtot);

         }
       });

       var totalAll = $('input.totalAll').val();
       // if(parseInt(totalAll) <= parseInt(budget)){
       //   $('#confirm').css('display', '');
       // }else{
       //   alert("Maaf Pembelian Anda melebihi budget")
       //   $('#confirm').css('display', 'none');
       // }
   }


   $("#confirm").on('click', function(){
     var data="";
     $.each($('.mainCart'), function(x, y){
       data += $(y).html();
     });

     var html = "";
     $.each($('div.shopping-total'), function(x,y){
       html += $(y).html();
     })
     $('div.subtot').html(html)

     $("#cartConfirm").html(data);
     $("#cartConfirm th.goods-page-action").css('display','none');

     $("#cartConfirm a.del-goods").removeAttr("onclick");
     $("#cartConfirm td.goods-page-description").css("font-size","12px");
     $("#cartConfirm td.goods-page-ref-no").css("font-size","12px");
     $("#cartConfirm td.goods-page-description").css("font-weight","1");
     $("#cartConfirm td.goods-page-ref-no").css("font-weight","1");
     $("#cartConfirm a.del-goods").html("-");
     $("#cartConfirm a.del-goods").removeClass();
     $("#cartConfirm td.del-goods-col").css('display','none');
     $("#cartConfirm .imageProduct").css('width','100%');
     $("#cartConfirm .qty").attr("readonly","readonly");
   });

   function DeleteCart(a) {
     var r = confirm("Apakah Anda Yakin ingin Hapus?");
       if (r == true) {
         var id_cart = $(a).attr('data-id');
         var url = "{{URL::to('catalog/users/delete_cart')}}";
           $.ajax({
               type: "post",
               url: url,
             data: {
                 "_token": "{{ csrf_token() }}",
                 "id_cart": id_cart,
                 },
               success: function (a) {
                 $('.tr_'+a.id_cart).remove();
                 if(a.status == 1){
                   $("div#notif").removeClass();
                   $("div#notif").addClass("alert alert-success");
                   $("#status").html("Sukses!");
                   $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                    $("#notif").slideUp(500);
                    location.reload();
                     });
                   $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                   $('html, body').animate({scrollTop:0}, 'slow');
                 }else{
                   $("div#notif").removeClass();
                   $("div#notif").addClass("alert alert-danger");
                   $("#status").html("Gagal!");
                   $("#notif").fadeTo(2000, 500).slideUp(500, function(){
                    $("#notif").slideUp(500);
                    location.reload();
                     });
                   $("p#kata_status").html("Menghapus Data "+a.item+" dari keranjang.");
                   $('html, body').animate({scrollTop:0}, 'slow');
                 }
               }
           });
       } else {
           location.reload();
       }
   }

     function addCommas(nStr)
   {
       nStr += '';
       x = nStr.split('.');
       x1 = x[0];
       x2 = x.length > 1 ? '.' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1)) {
           x1 = x1.replace(rgx, '$1' + '.' + '$2');
       }
       return x1 + x2;
   }

   function printDiv()
   {

     var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

     var divToPrint=document.getElementById('DivIdToPrint');

     var newWin=window.open('','Print-Window');

     newWin.document.open();

     newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

     newWin.document.close();

     setTimeout(function(){newWin.close();},10);

   }

   $body = $("body");

   $(document).on({
       ajaxStart: function() { $body.addClass("loading");    },
        ajaxStop: function() { $body.removeClass("loading"); }
   });


   $(document).ready(function(){
     $("#dashboard_nav").removeAttr('href');
     $("#dashboard_nav").attr('href','{{URL::to('catalog/users/')}}');
     $("#cartItem").removeAttr('href');
     $("#cartItem").attr('href','{{URL::to('catalog/users/cart')}}');
     // $("#cart").css('display','none');
     $("#history").removeAttr('href');
     $("#history").attr('href','{{URL::to('catalog/users/history')}}');
     $("#approval").removeAttr('href');
     $("#approval").attr('href','{{URL::to('catalog/users/order_approved')}}');
     $("#tracking").removeAttr('href');
     $("#tracking").attr('href','{{URL::to('catalog/users/track')}}');
   });
</script>
@endsection
