@extends('catalog.users.layouts.app')

@section('title')
  Catalog
@endsection

@section('plugins')

@endsection

@section('css')
  {{-- <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css"> --}}
@endsection

@section('xcss')

@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">

  </a>
@endsection

@section('content')

<div class="col-md-12 col-sm-12">
    <h1>Budget</h1>
    <div class="alert alert-success" id="notif">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong id="status"></strong><p id="kata_status"></p>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="portlet-body">
					<table class="table table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Periode</th>
                <th>Diajukan Oleh</th>
                <th>Disetujui Oleh</th>
                <th style="text-align:right;">Budget</th>
              </tr>
            </thead>
            <tbody>
              @php
                $budgets = 0;
              @endphp
              @if (count($budget) > 0)
              @foreach ($budget as $k => $v)
                @php
                  $budgets += $v->budget;
                @endphp
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->periode}}</td>
                  <td>{{$v->name_supervisi}}</td>
                  <td>{{$v->name}}</td>
                  <td style="text-align:right;">Rp. {{ number_format($v->budget, 0, ',', '.') }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5" style="text-align:center;">Budget tidak tersedia</td>
              </tr>
            @endif
              <tr>
                <tr>
                  <td style="text-align:right" colspan="4"><b>Total</b></td>
                  <td style="text-align:right"><b>Rp. {{ number_format($budgets, 0, ',', '.') }}</b></td>
                </tr>
              </tr>
            </tbody>
          </table>
				</div>
      </div>
    </div>
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="portlet-body">
          <h1>detail</h1>
          <table class="table table-striped">
            <tr>
              <th>Budget Total</th>
              <th>Rp. {{ number_format($budgets, 0, ',', '.') }}</th>
            </tr>
            <tr>
              <th>Budget terpakai</th>
              <th><a href="{{URL::to('catalog/users/manager/budget/detail')}}">Rp. {{ number_format($budgetUsed, 0, ',', '.') }}</a></th>
            </tr>
            <tr>
              <th>Budget sisa</th>
              <th>Rp. {{ number_format(($budgets-$budgetUsed), 0, ',', '.') }}</th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('note')

@endsection

@section('product-pop-up')

@endsection

@section('js')
<script type="text/javascript" src="/sa_catalog/assets/global/plugins/jquery-mask/dist/jquery.mask.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("div#notif").hide();
  $('.money').mask("000.000.000.000.000.000", {reverse: true});
});
  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }
});



$(document).ready(function(){
  $("#dashboard_nav").removeAttr('href');
  $("a#dashboard_nav").attr('href','{{URL::to('catalog/users/manager/dashboard')}}');
  $("#cartItem").removeAttr('href');
  $("a#cartItem").attr('href','{{URL::to('catalog/users/manager/cart')}}');
  $("#cart").css('display','none');
  $("#history").removeAttr('href');
  $("a#history").attr('href','{{URL::to('catalog/users/manager/history')}}');
  $("#approval").removeAttr('href');
  $("a#approval").attr('href','{{URL::to('catalog/users/manager/order_approved')}}');
  $("#tracking").removeAttr('href');
  $("a#tracking").attr('href','{{URL::to('catalog/users/manager/track')}}');
});
</script>
@endsection
