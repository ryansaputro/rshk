@extends('catalog.pphp.layouts.app')
@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')
  <div class="row">

    <div class="col-xs-offset-11">
      <a style="float:right; margin-bottom:20px;" href="#" class="btn btn-info" onclick='printDiv();' >Print</a>
    </div>

  </div>

  <div class="row">

    <div class="col-xs-12 portlet light" style="border:2px solid black;">

      <div class="" id='DivIdToPrint'>

        <div class="portlet-title">

          <div class="row">

            <div class="col-xs-8">

              <div class="col-xs-8">
                <p style="color:#000;"><b>{{$destination}}</b></p>
                {{$address}}  <br>
                Tlp. {{$telp}}
              </div>

            </div>

            <div class="col-xs-4">

              Kepada : <br><br>
              <b>{{$VendorDetail->vendor_name}}</b> <br>
                 {{$VendorDetail->address}}, {{$VendorDetail->city}}<br>
                 {{$VendorDetail->telephone}} - Fax. {{$VendorDetail->fax}}<br>
                 {{$list_kota[$list_propinsi[$VendorDetail->province]][$VendorDetail->city]}} - {{$VendorDetail->post_code}}
            </div>

          </div><!--/.row-->

          <br>

        </div><!--/.portlet-title--->
        @php
        $time = strtotime($receive_item->datetime);
        $times = strtotime($order->datetime);
        $function =  Fungsi::MonthIndonesia();
        $functionDay =  Fungsi::DayIndonesia();
        @endphp
        <div class="actions" id="tombol">

          <div class="row">

            <div class="col-xs-8">

              <div class="col-xs-8" style="text-align:left;">
                Tanggal : {{date('d', $time)}} {{$bulanWord[date('m', $time)]}} {{date('Y', $time)}}
              </div>

            </div>

            <div class="col-xs-4" style="text-align:left;">
              No. Polisi : {{$car_no}}<br>
              Sopir : {{$driver}} <br>
              No PO : {{$order->no_po}}/PO/{{date('m', $times)}}/{{date('Y', $times)}}
            </div>

          </div>
        </div>
        <div class="portlet-body">

          <div class="col-xs-4" style="border:1px; text-align:left;">
          </div>
            <div class="col-xs-12">
              <br>
              <center> <h3> <u>BUKTI TANDA TERIMA BARANG</u> </h3> </center>
              <br>
              <p style="text-indent:30px;">
                Dengan ini kami menyatakan bahwa kami telah menerima barang dengan kondisi barang baik serta dengan jumlah dan deskripsi sebagai berikut :
              </p>
              <table class="table table-striped">
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Satuan</th>
                  <th>Keterangan</th>
                </tr>
                @if (count($deliveryDtl) > 0)
                  @foreach ($deliveryDtl as $k => $v)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$v->code}} <br> {{$v->name}} <br> {{$v->merk}} </td>
                      <td>{{$v->qty}}</td>
                      <td>{{$v->satuan}}</td>
                      <td>{{($v->description != '') ? $v->description : '-'}}</td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="5">Tidak ada data</td>
                  </tr>
                @endif
              </table>
              <div class="col-xs-12" style="border:1px solid black;">
                <div class="col-xs-8">
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="no_po" class="col-xs-12 control-label">Dikirim Oleh :</label>
                    <br><br>
                    Nama : {{$destination}}
                    <br><br>
                    <br><br>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="no_po" class="col-xs-12 control-label">Diterima Oleh :</label>
                    <br><br>
                    <br><br>
                    <br><br>
                    <label for="no_po" class="col-xs-12 control-label">(...............................)</label>
                  </div>
                </div>

              </div>
            </div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
