@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Serah Barang ke Gudang</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
            <div class="col-sm-6 col-sm-6" style="float:left;">
              {{-- <a href="{{URL::to('catalog/pphp/to_warehouse/create/')}}" class="btn btn-info btn-sm" style="color:#fff;">Serah Barang</a> --}}
            </div>
            {{-- <form>
              <div class="col-sm-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="po">No PO</option>
                  <option value="spj">No SPJ</option>
                  <option value="tanggal">Tanggal</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form> --}}
          </div>
          <br>
          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Masuk</th>
                  <th>No SPJ</th>
                  <th>Gudang</th>
                  <th>Item</th>
                  <th>Qty</th>
                </tr>
              </thead>
              <tbody>
                @if (count($data) > 0)
                  @foreach ($data as $k => $v)
                    @php
                    $times = strtotime($v->datetime);
                    @endphp
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$v->datetime}}</td>
                      <td>{{$v->no_receive_item}}/SPJ/{{$v->code_vendor}}/{{date('m', $times)}}/{{date('Y', $times)}}</td>
                      <td>{{$v->nama_instalasi}}</td>
                      <td>{{$v->code}} - {{$v->name}}</td>
                      <td>{{$v->id_qty}} {{$v->unit_name}}</td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="6" style="text-align:center;">tidak ada data</td>
                  </tr>
                @endif
              </tbody>
          </table>
        </div>
        <div class="table-wrapper-responsive" id="divDetailCart" style="display:none; padding-top:15px;">
          <h1>  <center>Detail Pesanan</center></h1>
          <div class="row">
            <div class="col-xs-6">
              <address>
              <strong>Pembeli: </strong><br>
                <p id="name_buyer">Nama : xx</p>
                <p id="telephone_buyer">Telp: xx</p>
                <p id="email_buyer">Email: xx</p>
              </address>
            </div>
            <div class="col-xs-6 text-right">
              <address>
                <strong>Order:</strong><br>
                <p id="no_order">No Order : yy</p>
                <p id="date_order">Tanggal : yy</p>
              </address>
            </div>

          </div>
          <table summary="Shopping cart" id="detailCart" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Biaya Kirim</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          {{-- <div class="shopping-total Modal" style="width: 300px;">
            <ul>
              <li>
                <em>Total</em>
                <input type="hidden" name="subtot" value="" id="input_subtot">
                <strong class="price">
                  Rp
                  <span id="subtot"></span>
                </strong>
              </li> --}}
              {{-- <li>
                <em>Shipping Cost <small>PPN 10%</small></em>

                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
              {{-- <li class="shopping-total-price">
                <em>Total</em>
                <strong class="price"><span>Rp </span>xxx</strong>
              </li> --}}
            {{-- </ul>
          </div> --}}
        </div>
      </div>
      <!-- BEGIN PAGINATOR -->
      <div class="row">
        <div class="col-md-5 col-sm-5">
          <div>menampilkan {{ ($data->currentPage() - 1) * $data->perPage() + 1 }} sampai {{ $data->count() * $data->currentPage() }} dari {{ $data->total() }} data</div>
        </div>
          <div class="col-md-7 col-sm-7 block-paginate">{{ $data->links() }}</div>
      </div>
      <!-- END PAGINATOR -->
    </div>
    <div class="modal"></div>
  </div>
@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')

@endsection
