@extends('catalog.pphp.layouts.app')
@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a href="/" style="text-decoration: none;">
    {{-- <img src="/su_vms/assets/admin/layout3/img/logo-blue-steel.png" alt="logo" class="logo-default"> --}}
    <h1>RSHK</h1>
  </a>
@endsection

@section('page-head')
@endsection

@section('content')

  @php
  $time = strtotime($bast->datetime);
  $times = strtotime($receive_item->datetime);
  $timesDO = strtotime($delivery->datetime);
  $timesPO = strtotime($order->datetime);
  $function =  Fungsi::MonthIndonesia();
  $functionDay =  Fungsi::DayIndonesia();
  $RomanMonth =  Fungsi::MonthRoman();
  $total = 0;
  // $pphp = array("ketua"=>"Ryan Raputro S. KOM", "anggota 1"=>"Sisca Ningtyas S. PD", "anggota 2"=>"Siti Fatimah Ningtyas S. PD");
  $functionDay =  Fungsi::DayIndonesia();
  $aritdate =  date('d m Y', strtotime($receive_item->datetime. "+12 days"));
  $NextDate = explode(' ', $aritdate);
  @endphp


  <div class="row">

    <div class="col-xs-offset-11">
      <a style="float:right; margin-bottom:20px;" href="#" class="btn btn-info" onclick='printDiv();' >Print</a>
    </div>

  </div>

  <div class="row" id='DivIdToPrint'>

    <div class="col-xs-12 portlet light">

      <div class="" >

        <div class="portlet-title">

          <div class="row">
            <div class="col-xs-2">
              <img src="/assets/logo.png" class="logoRshk" style="width:75%;" alt="">
            </div>
            <div class="col-xs-8">
              <center>
                <h4>
                  <b>PENERIMA HASIL PEKERJAAN BARANG / JASA MEDIS</b> <br>
                </h4>
                  R.S. JANTUNG DAN PEMBULUH DARAH HARAPAN KITA <br>
                  Jln.Let.Jen S. Parman Kav 87 Slipi Jakarta Barat,<br> Telpon 5684086 - 093 Faksimile 5684230 <br>
                  <h4>
                    <b>BERITA ACARA SERAH TERIMA BARANG / JASA MEDIS</b> <br>
                  </h4>
                  <font style="text-transform:uppercase;">
                     Nomor : {{$bast->no_bast}}/BAST/{{$mainCategory->name}}/{{$userCode->kode_instalasi}}/RSJPDHK/{{$RomanMonth[date('m', $timesDO)]}}/{{date('Y', $timesDO)}}<br>
                   </font>
               </center>
            </div>

            <div class="col-xs-12">
              <hr style="border-top: 3px double #8c8b8b;">
            </div>

          </div><!--/.row-->

          <br>

        </div><!--/.portlet-title--->

        <div class="actions" id="tombol">

          <div class="row">

            <div class="col-xs-12">

                <p>
                  Pada Hari ini, {{$functionDay[date('l', $times)]}} Tanggal
                  <font style="text-transform:capitalize;">
                    {{Terbilang::make(date('d', $times), '', ' ')}} {{$bulanWord[date('m', $times)]}}
                  {{Terbilang::make(date('Y', $times))}}
                </font>, kami yang bertanda tangan di bawah ini bertindak selaku panitia Penerima barang / Jasa Medis Tahun Anggaran {{date('Y', $times)}} yang ditunjuk
                berdasarkan Surat Keputusan Direktur Utama Badan Layanan Umum Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita, dengan Nomor Keputusan : <i>no keputusan ambil dr DB</i>
                </p>

                <p>Telah memeriksa dan menerima dengan teliti pekerjaan {{$userCode->nama_instalasi}} yang tertera dalam lampiran Berita Acara Pemeriksaan dan Penerimaan Barang / Jasa yang diserahkan oleh
                  <font style="text-transform:uppercase;">{{$VendorDetail->vendor_name}}</font> kepada Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita Jakarta berdasarkan :
                </p>

                <div class="col-xs-2">
                  No. Kontrak <br>
                  No. Adendum <br>
                  No. SP <br>
                  Tanggal <br>
                </div>
                <div class="col-xs-1">
                  : <br>
                  : <br>
                  : <br>
                  : <br>
                </div>
                <div class="col-xs-6">
                  <font style="text-transform:uppercase;">
                  {{$contract->no_contract}} <br>
                  - <br>
                  {{$order->no_medik != NULL ? $order->no_medik : $order->no_non_medik}}/{{date('m', $timesPO)}}/{{$mainCategory->name}} E-catalog{{date('Y', $timesPO)}} <br>
                  {{date('d', $times)}}-{{$bulanWord[date('m', $times)]}}-{{date('Y', $times)}}
                </font>
                </div>


            </div>

          </div>
          <p>
            <br>
            Bahwa, kami telah mengadakan pemeriksaan Barang / Jasa sesuai pesanan dengan keadaan baik, maka barang tersebut dapat diterima dengan batas akhir waktu penyerahan
            {{$NextDate[0]}} {{$function[$NextDate[1]]}} {{$NextDate[2]}}.
          </p>
        </div>
              <div class="col-xs-offset-5">
                <p class="col-xs-12" style="text-indent:20px;">
                  Jakarta, {{date('d', $times)}} {{$bulanWord[date('m', $times)]}} {{date('Y', $times)}}
                </p>
              </div>
              <div class="col-xs-6">
                <p for="no_po" class="col-xs-12">Menyetujui :</p>
              </div>
              <div class="col-xs-12">
                <div class="col-xs-5">
                    <p for="no_po" class="col-xs-12">PPK Pengadaan Barang / Jasa Medis</p>
                    <br><br><br><br><br>
                    <p for="no_po" class="col-xs-12">{{$directur->name}}</p>
                    <br><br>
                </div>
                <div class="col-xs-7">
                    <p class="col-xs-12">YANG MENERIMA :</p>
                    <p class="col-xs-12">PENERIMA HASIL PEKERJAAN BARANG / JASA MEDIS</p>
                    <p class="col-xs-12">RUMAH SAKIT JANTUNG DAN PEMBULUH DARAH HARAPAN KITA</p>
                    <br><br><br>
                    <br><br><br>
                    <ol>
                      @foreach ($pphp as $k => $v)
                        <li>{{$v->username}} &nbsp;&nbsp;&nbsp;.........................</li>
                      @endforeach
                    </ol>
                </div>

              </div>
      </div>
    </div>

    <div class="page-break">

    <div class="col-xs-12 portlet light">

      <div class="" id='DivIdToPrint'>

        <div class="portlet-title">

          <div class="row">

            <div class="col-xs-12">
              <center>
                <h4>
                  <b>BERITA ACARA SERAH TERIMA BARANG / JASA</b> <br>
                </h4>
                <font style="text-transform:uppercase;">
                   Nomor : {{$bast->no_bast}}/BAST/{{$mainCategory->name}}/{{$userCode->kode_instalasi}}/RSJPDHK/{{$RomanMonth[date('m', $timesDO)]}}/{{date('Y', $timesDO)}}<br>
                 </font>
               </center>
               <hr style="border-top: 3px double #8c8b8b;">
            </div>

          </div><!--/.row-->

          <br>

        </div><!--/.portlet-title--->

        <div class="actions" id="tombol">

          <div class="row">

            <div class="col-xs-12">

                <b>
                  Tanggal Diterima: {{date('d', $times)}} {{$bulanWord[date('m', $times)]}} {{date('Y', $times)}}
                </b>

            </div>

          </div>
        </div>
        <div class="portlet-body">
          <hr style="border-top: 3px double #8c8b8b;">
              <table class="table table-striped">
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Deskripsi</th>
                  <th>Jumlah SP</th>
                  <th>Jumlah Kirim</th>
                  <th>Harga Satuan</th>
                  <th>Satuan</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                </tr>
                @if (count($deliveryDtl) > 0)
                  @php
                    $total = 0;
                  @endphp
                  @foreach ($deliveryDtl as $k => $v)
                    @php
                      $sub = $v->price_gov*$v->qty_kirim;
                      $total += $sub;
                    @endphp
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$v->code}}</td>
                      <td>{{$v->name}}</td>
                      <td>{{number_format($v->qty_po, 0, ',', '.')}}</td>
                      <td>{{number_format($v->qty_kirim+$v->qty_reject, 0, ',', '.')}}</td>
                      <td>{{number_format($v->price_gov, 2, ',', '.')}}</td>
                      <td>{{$v->unit_name}}</td>
                      <td>{{number_format($v->price_gov*$v->qty_kirim, 2, ',', '.')}}</td>
                      <td>{{($v->description != '') ? $v->description : '-'}}</td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="9" style="text-align:center;">Tidak Ada Data</td>
                  </tr>
                @endif
                <tr>
                  <th colspan="2" style="text-align:right;">Total</th>
                  <th colspan="5"></th>
                  <th colspan="5">{{number_format($total, 2, ',', '.')}}</th>
                </tr>
              </table>
              <hr style="border-top: 3px double #8c8b8b;">
              <p>Terbilang : <label for="" style="text-transform: uppercase;">{{Terbilang::make($total, ' rupiah', ' ')}}</label></p>
              <div class="col-xs-offset-5">
                <p class="col-xs-12" style="text-indent:20px;">
                  <b>Jakarta, {{date('d', $times)}} {{$bulanWord[date('m', $times)]}} {{date('Y', $times)}}</b>
                </p>
              </div>
              <div class="col-xs-6">
                <p for="no_po" class="col-xs-12">Menyetujui :</p>
              </div>
              <div class="col-xs-12">
                <div class="col-xs-5">
                    <p for="no_po" class="col-xs-12">PPK Pengadaan Barang / Jasa Medis</p>
                    <br><br><br><br><br>
                    <p for="no_po" class="col-xs-12">{{$directur->name}} </p>
                    <br><br>
                </div>
                <div class="col-xs-7">
                    <p class="col-xs-12">YANG MENERIMA :</p>
                    <p class="col-xs-12">PENERIMA HASIL PEKERJAAN BARANG / JASA MEDIS</p>
                    <p class="col-xs-12">RUMAH SAKIT JANTUNG DAN PEMBULUH DARAH HARAPAN KITA</p>
                    <br><br><br>
                    <br><br><br>
                    <ol>
                      @foreach ($pphp as $k => $v)
                        <li>{{$v->username}} &nbsp;&nbsp;&nbsp;.........................</li>
                      @endforeach
                    </ol>
                </div>

              </div>
        </div><!--/.portlet-body-->
      </div>
    </div>
  </div><!--/.page-break-->
  </div>
@endsection

@section('js')
<script type="text/javascript">
var myStyle = '<link rel="stylesheet" href="/su_vms/assets/global/plugins/bootstrap/css/bootstrap.css" />';

function printDiv()
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write(myStyle+'<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  // newWin.focus(); // necessary for IE >= 10*/

  // newWin.print();
  // newWin.close();
  setTimeout(function(){newWin.close();},10);

}
</script>
@endsection
