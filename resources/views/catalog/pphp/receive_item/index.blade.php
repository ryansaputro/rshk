@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="col-md-12 col-sm-12">
    <h1>Penerimaan Barang</h1>
    @if(session()->has('message'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="goods-page">
      <div class="goods-data clearfix">
        <div class="table-wrapper-responsive">
          <div class="row">
            <div class="col-sm-6 col-sm-6" style="float:left;">
              <a href="{{URL::to('catalog/pphp/create')}}" class="btn btn-info btn-sm" style="color:#fff;">Terima Barang</a>
            </div>
            {{-- <form>
              <div class="col-sm-6 col-sm-2">
                <select class="form-control input-sm filter" name="filter">
                  <option value="semua">Semua</option>
                  <option value="po">No PO</option>
                  <option value="spj">No SPJ</option>
                  <option value="tanggal">Tanggal</option>
                </select>
              </div>
              <div class="col-sm-3 search" style="text-align:right;">
                <input type="text" name="search" value="" class="form-control input-sm" placeholder="pencarian ...">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> cari</button>
              </div>
            </form> --}}
          </div>
          <br>
          <table summary="Shopping cart" id="mainCart" class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>No SP</th>
                  <th>No SPJ</th>
                  <th>Penyedia</th>
                  <th>Status</th>
                  <th>BAPB</th>
                  <th style="text-align:center;">SPJ</th>
                </tr>
              </thead>
              <tbody>
              @if (count($data) > 0)
                @foreach ($data as $k => $v)

                  @php
                  $time = strtotime($v->timePO);
                  $times = strtotime($v->timeDO);

                  @endphp
                  <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  $timestamp = strtotime($v->uploaded_at);
                  $upload =  date('Ymd', $timestamp);
                  ?>
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$v->datetime}}</td>
                    <td style="text-transform:uppercase;">
                      <a href="{{URL::to('catalog/pphp/po/download/'.md5($v->id_order))}}" target="_blank">
                        {{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('n', $time)}}/{{$v->name_category}}/E-catalog{{date('Y', $time)}}<a/>
                      </a>
                    </td>
                    <td>
                      <a href="{{URL::to('catalog/pphp/preview/'.md5($v->id_order).'/'.md5($v->id))}}" target="_blank">
                        {{$v->no_do}}/SPJ/{{$v->code}}/{{date('m', $times)}}/{{date('Y', $times)}}
                      </a>
                    </td>
                    <td>{{$v->vendor_name}}</td>
                    <td><span class="btn btn-{{$v->status == '1' ? 'success' : 'danger'}} btn-xs">{{$v->status == '1' ? 'Diterima' : 'Ditolak'}}</span></td>
                    <td>
                      @if ($v->id_bapb != null)
                        <a target="_blank" href="{{URL::to('catalog/pphp/bapb/preview/'.md5($v->id_order).'/'.md5($v->id))}}" style="color:white;" class="btn btn-info btn-xs">BAPB</a>
                        {{-- <a target="_blank" href="{{URL::to('catalog/pphp/bapb/preview/'.$v->id_order.'/'.$v->id)}}" style="color:white;" class="btn btn-info btn-xs">BAPB</a> --}}
                      @endif
                    </td>
                    <td> <a class="btn btn-xs btn-warning"  style="color:white;" href="{{URL::to('catalog/pphp/preview/'.md5($v->id_order).'/'.md5($v->id))}}">SPJ Awal</a>
                      @if (($v->data_spj != null) || ($v->data_spj != ''))
                        <a href="{{asset('/assets/document/'.$v->id_vendor_detail.'/spj/'.$upload.'/'.$v->data_spj)}}" class="btn btn-xs btn-info"  style="color:white;">SPJ Akhir</a>
                      @else
                          <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_{{$v->id}}" style="color:white;">Upload SPJ</button>
                      @endif
                    </td>
                  </tr>

                  <!-- Modal -->
                  <div class="modal fade" id="exampleModal_{{$v->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Unggah SPJ Cap Basah</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form class="form-horizontal spj_form" method="POST" action="{{ URL::to('/catalog/pphp/upload/spj/'.md5($v->id)) }}" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            <div class="row">
                              <div class="col-xs-12">
                                <div class="col-xs-10">
                                  <input type="file" name="data_spj" required class="form-control" value="" accept="image/jpeg" placeholder="max size 1mb">
                                  <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                                </div>
                                  <div class="col-xs-2">
                                    <button type="submit" name="button" class="btn btn-primary btn-block">Unggah</button>
                                  </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              @else
                <tr>
                  <td colspan="8" style="text-align:center;">Tidak Ada Data</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal"></div>
  </div>


@endsection

@section('note')

@endsection

@section('product-pop-up')
@endsection

@section('js')
<script type="text/javascript">
$(document).ready( function () {
  $('#mainCart').DataTable();
} );
</script>
@endsection
