@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Penerimaan Barang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/pphp/receiveSave')}}" method="post">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="no_po" class="col-md-4 control-label">No SP</label>
                <div class="col-md-8">
                  <select class="form-control choiceChosen input sm" name="no_po"  id="no_po" onchange="poNumber(this)">
                    <option selected disabled>-pilih-</option>
                    @foreach ($po as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->id}}" style="text-transform:uppercase;">{{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('m', $time)}}/{{$v->name_category}}/E-CATALOG{{date('Y', $time)}}</option>
                    @endforeach
                  </select>
                  {{-- <input style="margin-bottom:10px;" id="no_po"  readonly type="text" class="form-control input-sm" name="no_po" value="{{$po->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}" required autofocus> --}}
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="post_code" class="col-md-4 control-label">Penyedia</label>
                <div class="col-md-8">
                  <input type="text" readonly name="vendor" class="form-control input-sm" id="vendor" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="post_code" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <input type="text" name="address" readonly id="address" class="form-control input-sm" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="post_code" class="col-md-4 control-label">Telepon</label>
                <div class="col-md-8">
                  <input type="text" name="telephone" readonly id="telephone" class="form-control input-sm" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="post_code" class="col-md-4 control-label">No SPJ</label>
                <div class="col-md-8" id="divSpj">
                  <select class="form-control choiceChosen input-sm" name="spj" id="SPJ">
                  </select>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-md-6 itemDiv" style="display:none;">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="item" class="col-md-4 control-label">Nama Item</label>
                <div class="col-md-8" id="divItem">
                  <select class="form-control choiceChosen input-sm" id="item">

                  </select>

                  @if ($errors->has('item'))
                    <span class="help-block">
                      <strong>{{ $errors->first('item') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="qty" class="col-md-4 control-label">Jumlah Item<br> (SP)</label>
                <div class="col-md-6">
                  <input id="qty"  type="number" class="form-control  input-sm" name="qty" value="{{ old('qty') }}" required autofocus>
                  @if ($errors->has('qty'))
                    <span class="help-block">
                      <strong>{{ $errors->first('qty') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="col-md-2">
                  <select class="form-control input-sm" id="satuan" name="satuan">
                    @foreach ($unit as $k => $v)
                      <option value="{{$v->id}}" data-convert="{{$v->convert_pcs}}">{{$v->unit_name}}</option>
                      {{-- <option value="{{$v->id}}" {{$v->id == '2' ? 'selected' : ''}} data-convert="{{$v->convert_pcs}}">{{$v->unit_name}}</option> --}}
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="post_code" class="col-md-4 control-label"></label>
                <div class="col-md-8">
                  <button type="button" name="button" onclick="Convert(this)" class="btn btn-primary btn-block btn-xs">Konversi</button>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} after_convert  col-xs-12" style="display:'';">
                <label for="post_code" class="col-md-4 control-label">Hasil</label>
                <div class="col-md-2 hasil">
                  <input type="text" readonly name="" value="" class="form-control input-sm hasil">
                </div>
                <div class="col-md-2 satuan">
                  <input type="text" readonly name="" value="BOX" class="form-control input-sm satuan">
                </div>
                <div class="col-md-2 sisa_hasil">
                  <input type="text" readonly name="" value="" class="form-control input-sm sisa_hasil">
                </div>
                <div class="col-md-2 sisa_satuan">
                  <input type="text" readonly name="" value="PCS" class="form-control input-sm sisa_satuan">
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} keterangan  col-xs-12" style="display:none;">
                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                <div class="col-md-8">
                  <input id="keterangan"  type="text" class="form-control  input-sm" name="keterangan" value="{{ old('post_code') }}" autofocus>
                  @if ($errors->has('keterangan'))
                    <span class="help-block">
                      <strong>{{ $errors->first('keterangan') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <div class="col-md-offset-10 col-sm-2">
                  <button type="button" class="btn btn-sm btn-primary btn-block oke" style="display:none;" onclick="AddItem(this)" name="button">Oke</button>
                </div>
              </div>
            </div>

          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemKirim">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              {{-- <div class="col-md-6">
                <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button" value="save">Simpan</button>
              </div> --}}
              <div class="col-md-3 pull-right">
                <button  class="btn btn-sm btn-success btn-block submitBtnBast" name="button" value="bast">Simpan & Generate BAPB</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">

var satuan_kirim = "";
var id_satuan_kirim = "";

function Convert(a) {
  $('.keterangan').css('display', 'block');
  $('.oke').css('display', 'block');
  $('input.sisa_hasil').val(0)
  $('input.hasil').val(0)
  $('input.satuan').val("-")
  var qty_per_pcs = $('#qty').val();
  var convert_pcs_to = $('#satuan').find(':selected').attr('data-convert');
  satuan_kirim = $('#satuan').find(':selected').text();
  id_satuan_kirim = $('#satuan').find(':selected').val();
  sisa = qty_per_pcs%convert_pcs_to;
  nilai = Math.floor(qty_per_pcs/convert_pcs_to);
  $('.after_convert').css('display', 'inline');
  if(sisa != 0){
    $('input.sisa_hasil').val(sisa)
    $('#satuan option').prop('selected', function() {
         return this.defaultSelected;
     });

  }
  $('input.hasil').val(nilai)
  $('input.satuan').val($('#satuan').find(':selected').text())
  $('#satuan option').prop('selected', function() {
       return this.defaultSelected;
   });

}
$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });

   });

var name = "";
var id = "";

function poNumber(a) {
  id_po = $(a).val();
  $.ajax({
    url: '{{URL::to('catalog/pphp/receive_item/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_po" : id_po},
    success: function (a) {

      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);
      //
      // var selectSpj = '<select class="form-control choiceChosen input-sm" name="spj" style="margin-bottom:10px;" id="SPJ">';
      var selectSpj ="";
      selectSpj += '<option disabled selected> -pilih- </option>';
      $.each($(a.do), function(k,v){
        var date = v.datetime.split('-');
          selectSpj += '<option data-id="'+v.id+'" value="'+v.id+'">'+v.no_do+'/SPJ/'+date[1]+'/'+date[0]+'</option>';
      })
          // selectSpj += '</select>';

      $("#SPJ").data("chosen").destroy().chosen();
      $("#SPJ").html(selectSpj).trigger("chosen:updated");

      // $('#divSpj').html(selectSpj);

      $('#SPJ').on('change', function(){
        var id_spj = $(this).val();
        $('#qty').val(0);
        $("#itemKirim tbody tr").remove();
        arr[id] = false;

        $.ajax({
          url: '{{URL::to('catalog/pphp/receive_item/spjItem')}}',
          method: 'POST',
          data: {"_token": "{{ csrf_token() }}", "id_spj": id_spj},

          success: function (a) {

            var select = "";
                select += '<option disabled selected> -pilih- </option>';;
            $.each($(a.orderDetail), function(k,v){
              select += '<option data-do="'+v.id_do+'" data-id="'+v.id+'" value="'+v.id_item+'">'+v.name+'</option>';
            })

            $('.itemDiv').css('display', '');
            $("#item").data("chosen").destroy().chosen();
            $("#item").html(select).trigger("chosen:updated");

            $('#item').on('change', function(){
              dos = $('#item :selected').attr('data-do');
              id = $('#item :selected').attr('data-id');
              id_item = $(this).val();
              name =$('#item :selected').text();
              $.ajax({
                url: '{{URL::to('catalog/pphp/receive_item/detailItem')}}',
                method: 'POST',
                data: {"_token": "{{ csrf_token() }}", "id": dos, "id_item" : id_item},
                success: function (a) {

                  $('#satuan option').prop('selected', function() {
                       return this.defaultSelected;
                   });

                  $.each($('#satuan option'), function(x,y){
                      if($(y).val() == a.satuan){
                        $(y).attr('selected', 'selected');
                        $('input.sisa_satuan').val($(y).text())

                      }
                  });

                  $('#qty').attr('value', a.qty);
                  if(a.qty > 0){
                    $('#qty').attr('max', a.qty);
                    $('#qty').attr('min', 1);
                  }else{
                    $('#qty').attr('max', 0);
                    $('#qty').attr('min', 0);

                  }
                }
              });
            });

          }
        });
      })
    }
  });
}

var arr = [];
function AddItem(a) {
  var hasil = $('input.hasil').val();
  $('input.hasil').val('');
  if((name !== '') || (hasil !== '')  || (hasil !== 0)){
    if(satuan_kirim == ''){
      satuan_kirim = $('#satuan').find(':selected').text();
      id_satuan_kirim = $('#satuan').find(':selected').val();
    }
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+hasil+"<input type='hidden' name='qtyDO[]' value='"+hasil+"'></td>"+
    // "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+satuan_kirim+"<input type='hidden' name='satuanDO[]' value='"+id_satuan_kirim+"'></td>"+
    // "<td>"+$('#satuan').children("option").filter(":selected").text()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
    $(".submitBtnBast").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
    $(".submitBtnBast").attr('type', 'button');
  }

  $(".hasil").val(0)
  $(".satuan").val("")
  $(".sisa_hasil").val(0)
  $("#keterangan").val("")

  $('.keterangan').css('display', 'none');
  $('.oke').css('display', 'none');

}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  $(document).ready(function(){
    $(".choiceChosen, .productChosen").chosen({});

    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });
</script>
@endsection
