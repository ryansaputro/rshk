@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Penerimaan Barang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/pphp/receiveSave')}}" method="post">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="no_po" class="col-md-4 control-label">No Po</label>
                <div class="col-md-8">
                  <select class="form-control input sm" name="no_po"  style="margin-bottom:10px;" id="no_po" onchange="poNumber(this)">
                    <option selected disabled>-pilih-</option>
                    @foreach ($po as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->id}}">{{$v->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}</option>
                    @endforeach
                  </select>
                  {{-- <input style="margin-bottom:10px;" id="no_po"  readonly type="text" class="form-control input-sm" name="no_po" value="{{$po->no_po}}/KU/{{date('m', $time)}}/{{date('Y', $time)}}" required autofocus> --}}
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Penyedia</label>
                <div class="col-md-8">
                  <input type="text" style="margin-bottom:10px;" readonly name="vendor" class="form-control input-sm" id="vendor" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <input type="text" name="address" readonly id="address" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Telepon</label>
                <div class="col-md-8">
                  <input type="text" name="telephone" readonly id="telephone" style="margin-bottom:10px;" class="form-control input-sm" value="" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">No SPJ</label>
                <div class="col-md-8" id="divSpj">
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-md-6 itemDiv" style="display:none;">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="item" class="col-md-4 control-label">Nama Item</label>
                <div class="col-md-8" id="divItem">

                  @if ($errors->has('item'))
                    <span class="help-block">
                      <strong>{{ $errors->first('item') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="qty" class="col-md-4 control-label">Jumlah Item</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="qty"  type="number" class="form-control  input-sm" name="qty" value="{{ old('qty') }}" required autofocus>
                  @if ($errors->has('qty'))
                    <span class="help-block">
                      <strong>{{ $errors->first('qty') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Satuan</label>
                <div class="col-md-8">
                  <select class="form-control input-sm" style="margin-bottom:10px;" id="satuan" name="satuan">
                    {{-- <option value="box">Box</option>
                    <option value="unit">Unit</option>
                    <option value="Pcs">Pcs</option> --}}
                    @foreach ($unit as $k => $v)
                      <option value="{{$v->id}}">{{$v->unit_name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                <div class="col-md-8">
                  <input style="margin-bottom:10px;" id="keterangan"  type="text" class="form-control  input-sm" name="keterangan" value="{{ old('post_code') }}" autofocus>
                  @if ($errors->has('keterangan'))
                    <span class="help-block">
                      <strong>{{ $errors->first('keterangan') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-offset-10 col-sm-2">
                  <button type="button" class="btn btn-sm btn-primary btn-block" onclick="AddItem(this)" name="button">Oke</button>
                </div>
              </div>
            </div>

          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemKirim">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <div class="col-md-6">
                <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button" value="save">Simpan</button>
                {{-- <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button">Simpan & Generate BAST</button> --}}
              </div>
              <div class="col-md-6">
                {{-- <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button">Simpan</button> --}}
                <button  class="btn btn-sm btn-success btn-block submitBtnBast" name="button" value="bast">Simpan & Generate BAST</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">
$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });
   });

var name = "";
var id = "";

function poNumber(a) {
  id_po = $(a).val();
  $.ajax({
    url: '{{URL::to('catalog/pphp/receive_item/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_po" : id_po},
    success: function (a) {
      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);

      var selectSpj = '<select class="form-control input-sm" name="spj" style="margin-bottom:10px;" id="SPJ">';
          selectSpj += '<option disabled selected> -pilih- </option>';
      $.each($(a.do), function(k,v){
        var date = v.datetime.split('-');
          selectSpj += '<option data-id="'+v.id+'" value="'+v.id+'">'+v.no_do+'/SPJ/'+date[1]+'/'+date[0]+'</option>';
      })
          selectSpj += '</select>';
      $('#divSpj').html(selectSpj);

      $('#SPJ').on('change', function(){
        var id_spj = $(this).val();
        $('#qty').val(0);
        $("#itemKirim tbody tr").remove();
        arr[id] = false;

        $.ajax({
          url: '{{URL::to('catalog/pphp/receive_item/spjItem')}}',
          method: 'POST',
          data: {"_token": "{{ csrf_token() }}", "id_spj": id_spj},
          success: function (a) {
            var select = '<select class="form-control input-sm" style="margin-bottom:10px;" id="item">';
            select += '<option disabled selected> -pilih- </option>';
            $.each($(a.orderDetail), function(k,v){
              console.log(v.name);
              select += '<option data-do="'+v.id_do+'" data-id="'+v.id+'" value="'+v.id_item+'">'+v.name+'</option>';
            })
            select += '</select>';
            $('.itemDiv').css('display', '');
            $('#divItem').html(select);

            $('#item').on('change', function(){
              dos = $('#item :selected').attr('data-do');
              id = $('#item :selected').attr('data-id');
              id_item = $(this).val();
              name =$('#item :selected').text();
              $.ajax({
                url: '{{URL::to('catalog/pphp/receive_item/detailItem')}}',
                method: 'POST',
                data: {"_token": "{{ csrf_token() }}", "id": dos, "id_item" : id_item},
                success: function (a) {
                  $('#qty').attr('value', a.qty);
                  if(a.qty > 0){
                    $('#qty').attr('max', a.qty);
                    $('#qty').attr('min', 1);
                  }else{
                    $('#qty').attr('max', 0);
                    $('#qty').attr('min', 0);

                  }
                }
              });
            });

          }
        });
      })
    }
  });
}

var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').children("option").filter(":selected").text()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
    $(".submitBtnBast").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
    $(".submitBtnBast").attr('type', 'button');
  }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  $(document).ready(function(){
    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });
</script>
@endsection
