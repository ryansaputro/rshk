@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">Penerimaan Barang ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/pphp/receiveSave')}}" method="post">
            {{ csrf_field() }}
          <div class="row">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="no_po" class="col-md-4 control-label">No SP</label>
                <div class="col-md-7">
                  <select class="form-control choiceChosen input sm" name="no_po"  id="no_po" onchange="poNumber(this)">
                    <option selected disabled>-pilih-</option>
                    @foreach ($po as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      <option value="{{$v->id}}" style="text-transform:uppercase;">{{$v->no_medik != NULL ? $v->no_medik : $v->no_non_medik}}/{{date('m', $time)}}/{{$v->name_category}}/E-CATALOG{{date('Y', $time)}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="col-md-1">
                  <a href="#" target="_blank" class="btn btn-sm btn-primary btn-block cekPo" disabled><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> cek</a>
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="vendor" class="col-md-4 control-label">Penyedia</label>
                <div class="col-md-8">
                  <input type="text" readonly name="vendor" class="form-control input-sm" id="vendor" value="" required>
                  @if ($errors->has('vendor'))
                    <span class="help-block">
                      <strong>{{ $errors->first('vendor') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="address" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <input type="text" name="address" readonly id="address" class="form-control input-sm" value="" required>
                  @if ($errors->has('address'))
                    <span class="help-block">
                      <strong>{{ $errors->first('address') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="telephone" class="col-md-4 control-label">Telepon</label>
                <div class="col-md-8">
                  <input type="text" name="telephone" readonly id="telephone" class="form-control input-sm" value="" required>
                  @if ($errors->has('telephone'))
                    <span class="help-block">
                      <strong>{{ $errors->first('telephone') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-12">
                <label for="spj" class="col-md-4 control-label">No SPJ</label>
                <div class="col-md-7" id="divSpj">
                  <select class="form-control choiceChosen input-sm" name="spj" id="SPJ">
                  </select>
                  @if ($errors->has('spj'))
                    <span class="help-block">
                      <strong>{{ $errors->first('spj') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="col-md-1">
                  <a href="#" target="_blank" class="btn btn-sm btn-primary btn-block cekSPJ" disabled><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> cek</a>
                </div>
              </div>
          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemTerima">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Deskripsi</th>
                    <th>Jumlah SP</th>
                    <th>Jumlah Kirim</th>
                    <th>Harga Satuan</th>
                    <th>Hasil</th>
                    <th>Satuan</th>
                    <th>Total</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>

              <div class="col-md-3 pull-right">
                <button  class="btn btn-sm btn-success btn-block submitBtnBast" name="button" value="bast">Simpan & Generate BAPB</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">
function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  var vendorData = $(a).attr('data-vendor');
  var AllSubtot = 0;

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)
  $(".hasil_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
            total = (nilai*price);
        $('td.total_'+$(a).attr('data-id')).html(addCommas(Math.ceil(total)));
      }
    });
}


var satuan_kirim = "";
var id_satuan_kirim = "";

function Convert(a) {

  $('.keterangan').css('display', 'block');
  $('.oke').css('display', 'block');
  $('input.sisa_hasil').val(0)
  $('input.hasil').val(0)
  $('input.satuan').val("-")

  var id_detail = $(a).attr('data-id_detail');
  var qty_per_pcs = $('.qty_'+id_detail).val();
  var convert_pcs_to = $(a).find(':selected').attr('data-convert');
  var convert_pcs_id = $(a).find(':selected').val();
  var convert_pcs_awal = $('td.item_'+id_detail).attr('data-satuan');
  var convert_pcs_awal_id = $('td.item_'+id_detail).attr('data-id_satuan');

  if(convert_pcs_id != convert_pcs_awal_id){
    sisa = qty_per_pcs%convert_pcs_to;
      if(convert_pcs_id == 2){
        nilai = Math.floor(qty_per_pcs*convert_pcs_awal);
      }else{
        nilai = Math.floor(qty_per_pcs/convert_pcs_to);
      }
    }else{
      if(convert_pcs_id == convert_pcs_awal_id){
        nilai = qty_per_pcs;
      }else {
        nilai = Math.floor(qty_per_pcs/convert_pcs_to);
      }
    }

  $('input.hasil_'+id_detail).val(nilai)
}

$(function () {
      $( "#qty" ).change(function() {
         var max = parseInt($(this).attr('max'));
         var min = parseInt($(this).attr('min'));
         if ($(this).val() > max)
         {
             $(this).val(max);
         }
         else if ($(this).val() < min)
         {
             $(this).val(min);
         }
       });

   });

var name = "";
var id = "";

function poNumber(a) {
  id_po = $(a).val();
  $.ajax({
    url: '{{URL::to('catalog/pphp/receive_item/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_po" : id_po},
    success: function (a) {

      $('.cekPo').attr('disabled', false);
      $('.cekPo').attr('href', 'po/download/'+a.id_order)

      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);

      var selectSpj ="";
      selectSpj += '<option disabled selected> -pilih- </option>';
      $.each($(a.do), function(k,v){
        var date = v.datetime.split('-');
          selectSpj += '<option data-id="'+v.id+'" value="'+v.id+'">'+v.no_do+'/SPJ/'+date[1]+'/'+date[0]+'</option>';
      })

      $("#SPJ").data("chosen").destroy().chosen();
      $("#SPJ").html(selectSpj).trigger("chosen:updated");

      $('#SPJ').on('change', function(){
        var id_spj = $(this).val();
        $('#qty').val(0);
        $("#itemKirim tbody tr").remove();
        arr[id] = false;

        $.ajax({
          url: '{{URL::to('catalog/pphp/receive_item/spjItem')}}',
          method: 'POST',
          data: {"_token": "{{ csrf_token() }}", "id_spj": id_spj},

          success: function (a) {
            $('.cekSPJ').attr('disabled', false)

            var table = "";
            $.each(a.data, function(k,v){
              var total = parseInt(v.qty_kirim)*parseInt(v.price_gov);
              console.log("id item "+v.id_item);
              if(v.id_item in a.dataSend){
                  table += '<tr>'+
                          '<td>'+v.code+'</td>'+
                          '<td>'+v.name+'</td>'+
                          '<td class="item_'+v.id+'" data-id_satuan="'+a.dataSatuan[v.id_item]+'" data-satuan="'+v.convert_pcs+'">'+addCommas(v.qty)+' '+v.unit_name+'</td>'+
                          '<td>';
                            if(v.id_item in a.dataSend){
                            table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataSend[v.id_item]+'" name="qtyDO[]" value="'+a.dataSend[v.id_item]+'" data-id="'+v.id+'">'
                            table +=  '<input type="hidden" name="idDO[]" value="'+v.id_item+'">'
                            }else{
                                table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" disabled oninput="InputQty(this)" max="0" name="qtyDO[]" value="0" data-id="'+v.id+'">'
                            }

                          table += '</td>'+
                          '<td>'+
                              addCommas(v.price_gov)+
                            '<input type="hidden" name="price_pcs" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id+'">'+
                          '</td>'+
                          '<td><input type="text" readonly name="" value="'+a.dataSend[v.id_item]+'" class="form-control input-sm hasil_'+v.id+'"></td>'+
                          '<td>';

                          table += '<select name="satuanDO[]" onchange="Convert(this)" data-qty="'+v.qty+'"  data-id_detail="'+v.id+'" class="satuan">';

                          $.each(a.allSatuan, function(x,b){
                              if(v.id_item in a.dataSatuan){
                                if (a.dataSatuan[v.id_item] == b.id) {
                                  table += '<option selected data-id_satuan="'+b.id+'"  data-convert="'+b.convert_pcs+'" value="'+b.id+'">'+a.dataSatuanName[v.id_item]+'</option>'
                                }
                              }else
                               if(v.id_satuan == b.id){
                                table += '<option selected data-id_satuan="'+b.id+'"  data-convert="'+b.convert_pcs+'" value="'+b.id+'">'+b.unit_name+'</option>'
                              }else{
                                table += '<option data-id_satuan="'+b.id+'" data-convert="'+b.convert_pcs+'" value="'+b.id+'">'+b.unit_name+'</option>'
                              }
                          });

                          table += '</select>';
                          table += '</td>';
                          table += '<td class="total total_'+v.id+'">';
                              if(v.id_item in a.dataSend){
                                table += addCommas(a.dataSend[v.id_item]*v.price_gov);
                              }else{
                                table += addCommas(0*v.price_gov);
                              }
                          table += '</td>';
                          table += '<td>';
                          if(v.id_item in a.dataDesc){
                            table += '<input type="text" name="keteranganDO[]" class="form-control" value="'+a.dataDesc[v.id_item]+'">';
                          }

                          table += '</td>'+
                          '<td><button class="btn btn-danger btn-sm btn-round" onclick="remove(this)" data-id="'+v.id_order+'"><i class="fa fa-times" aria-hidden="true"></i></button></td>'+
                        '</tr>';
                      }

            });
// console.log(a.data);
            $('table#itemTerima tbody').html(table);
          }
        });
      })
    }
  });
}

var arr = [];
function AddItem(a) {
  var hasil = $('input.hasil').val();
  $('input.hasil').val('');
  if((name !== '') || (hasil !== '')  || (hasil !== 0)){
    if(satuan_kirim == ''){
      satuan_kirim = $('#satuan').find(':selected').text();
      id_satuan_kirim = $('#satuan').find(':selected').val();
    }
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+hasil+"<input type='hidden' name='qtyDO[]' value='"+hasil+"'></td>"+
    // "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+satuan_kirim+"<input type='hidden' name='satuanDO[]' value='"+id_satuan_kirim+"'></td>"+
    // "<td>"+$('#satuan').children("option").filter(":selected").text()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
    $(".submitBtnBast").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
    $(".submitBtnBast").attr('type', 'button');
  }

  $(".hasil").val(0)
  $(".satuan").val("")
  $(".sisa_hasil").val(0)
  $("#keterangan").val("")

  $('.keterangan').css('display', 'none');
  $('.oke').css('display', 'none');

}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  }else{
    $(".submitBtn").attr('type', 'button');
  }}


  $(document).ready(function(){
    $(".choiceChosen, .productChosen").chosen({});

    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });

  function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
</script>
@endsection
