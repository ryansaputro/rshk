<ul>
  <li><a href="{!! route('x.catalog.guest.item') !!}">PRODUK</a></li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Barang
    </a>

    <!-- BEGIN DROPDOWN MENU -->
    <ul class="dropdown-menu">
      <li><a href="{{URL::to('catalog/pphp/index')}}">Terima Barang dari Vendor</a></li>
      <li><a href="{{URL::to('catalog/pphp/bast/index')}}">BAST Manual</a></li>
      <li><a href="{{URL::to('catalog/pphp/to_warehouse/index')}}">Serah Barang ke Gudang</a></li>
    </ul>
    <!-- END DROPDOWN MENU -->
  </li>

  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
      Laporan
    </a>

    <!-- BEGIN DROPDOWN MENU -->
    <ul class="dropdown-menu">
      <li><a href="{{URL::to('catalog/pphp/index')}}">Pembanding</a></li>
    </ul>
    <!-- END DROPDOWN MENU -->
  </li>
</ul>
