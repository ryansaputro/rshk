@extends('catalog.pphp.layouts.app')

@section('title')
  Catalog
@endsection

@section('css')
@endsection

@section('logo')
  <a class="site-logo" href="{!! route('x') !!}" style="text-decoration: none;">
    RSHK
  </a>
@endsection

@section('content')
  @php
    $romanFormat = Fungsi::MonthRoman();
  @endphp
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption caption-md">
            <i class="icon-bar-chart theme-font hide"></i>
            <span class="caption-subject theme-font bold uppercase">Form</span>
            <span class="caption-helper uppercase">BAST Manual ...
            </span>
          </div>
          <br>
        </div><!--/.portlet-title--->
        <div class="portlet-body">
          <form class="" action="{{URL::to('catalog/pphp/bastSave')}}" novalidate method="post">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="no_po" class="col-md-4 control-label">No SPJ</label>
                <div class="col-md-8">
                  <select class="form-control input sm" name="id_do"  style="margin-bottom:10px;" id="no_po" onchange="poNumber(this)">
                    <option selected disabled>-pilih-</option>
                    @foreach ($po as $k => $v)
                      @php
                      $time = strtotime($v->datetime);
                      @endphp
                      @if (isset($idReceive))
                        @if ($v->id == $idReceive)
                          <option value="{{$v->id}}" {{$v->id == $idReceive ? 'selected' : 'disabled'}}>{{$v->no_do}}/SPJ/{{$v->code}}/{{$romanFormat[date('m', $time)]}}/{{date('Y', $time)}}</option>
                        @endif
                        {{-- <option value="{{$v->id}}" {{$v->id == $idReceive ? 'selected' : 'disabled'}}>{{$v->no_do}}/SPJ/{{$v->code}}/{{$romanFormat[date('m', $time)]}}/{{date('Y', $time)}}</option> --}}
                      @else
                        @if (!in_array($v->id, $bastSelected))
                          <option value="{{$v->id}}">{{$v->no_do}}/SPJ/{{$v->code}}/{{$romanFormat[date('m', $time)]}}/{{date('Y', $time)}}</option>
                        @endif
                      @endif
                      {{-- <option value="{{$v->id}}" {{isset($idReceive) && $v->id == $idReceive ? 'selected' : 'disabled'}}>{{$v->no_do}}/SPJ/{{$v->code}}/{{$romanFormat[date('m', $time)]}}/{{date('Y', $time)}}</option> --}}
                      {{-- @if ($v->id_bast == null)
                        <option value="{{$v->id}}" {{$order != null && $v->id == $order->id ? 'selected' : ''}}>{{$v->no_po}}/PO/{{date('m', $time)}}/{{date('Y', $time)}}</option>
                      @endif --}}
                    @endforeach
                  </select>
                  @if ($errors->has('no_po'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_po') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Penyedia</label>
                <div class="col-md-8">
                  <input type="text" style="margin-bottom:10px;" readonly name="vendor" class="form-control input-sm" id="vendor" value="{{isset($VendorDetail) ? $VendorDetail->vendor_name : ''}}" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Alamat</label>
                <div class="col-md-8">
                  <input type="text" name="address" readonly id="address" style="margin-bottom:10px;" class="form-control input-sm" value="{{isset($VendorDetail) ? $VendorDetail->address : ''}}" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="post_code" class="col-md-4 control-label">Telepon</label>
                <div class="col-md-8">
                  <input type="text" name="telephone" readonly id="telephone" style="margin-bottom:10px;" class="form-control input-sm" value="{{isset($VendorDetail) ? $VendorDetail->telephone : ''}}" required>
                  @if ($errors->has('post_code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

          </div>
          <div class="row" style="margin-top:20px;">
              <table class="table table-striped" id="itemKirim">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Deskripsi</th>
                    <th>Jumlah SP</th>
                    <th>Jumlah Kirim</th>
                    <th>Harga Satuan</th>
                    <th>Satuan</th>
                    <th>Total</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($dataPO as $k => $v)
                    <tr>
                      <td>{{$v->code}}</td>
                      <td>{{$v->name}}</td>
                      <td>{{number_format($v->qty, 0, ',', '.')}}</td>
                      <td>
                        @if (array_key_exists($v->id_item, $data))
                          <input type="number" min="0" class="form-control qty qty_{{$v->id_item}}" oninput="InputQty(this)" max="{{$data[$v->id_item]}}" name="qty_receive_item[]" value="{{$data[$v->id_item]}}" data-id="{{$v->id_item}}">
                          <input type="hidden" name="receive_id_item[]" value="{{$v->id_item}}">
                          <input type="hidden" name="receive_satuan[]" value="{{$dataLain[$v->id_item]}}">
                          <input type="hidden" name="id_receive_item" value="{{$IdDataReceive}}">
                          <input type="hidden" name="no_po" value="{{$order->id}}">
                        @else
                          <input readonly type="number" min="0" class="form-control qty qty_{{$v->id_item}}" oninput="InputQty(this)" max="0" name="qty_receive_item[]" value="0" data-id="{{$v->id_item}}">
                          {{-- <p style="color:green;">selesai</p> --}}
                        @endif
                      </td>
                      <td>
                        {{number_format($v->price_gov, 2, ',', '.')}}
                        <input type="hidden" name="price_pcs" value="{{$v->price_gov}}" class="price_pcs" data-id="{{$v->id_item}}">
                        {{-- <input type="hidden" name="price_shipment" value="{{$v->price_shipment}}" class="price_shipment" data-id="{{$v->id}}"> --}}
                      </td>
                      @if (array_key_exists($v->id_item, $data))
                        <td>{{$satuan[$dataLain[$v->id_item]]}}</td>
                      @else
                        <td>{{$ReceiveItemSatuanAll[$v->id_item]}} </td>
                      @endif
                      <td class="total total_{{$v->id_item}}">
                        @if (array_key_exists($v->id_item, $data))
                          {{number_format($data[$v->id_item]*$v->price_gov, 2, ',', '.')}}
                        @else
                          {{number_format(0*$v->price_gov, 2, ',', '.')}}
                        @endif
                      </td>
                      <td>
                        @if (array_key_exists($v->id_item, $data))
                          <input type="text" name="receive_keterangan[]" class="form-control" value="{{$ReceiveItemDesc[$v->id_item]}}">
                        @else
                          <input type="text" name="receive_keterangan[]" class="form-control" readonly value="{{$ReceiveItemDescAll[$v->id_item]}}">
                        @endif

                      </td>
                      <td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='{{$v->id_order}}'><i class='fa fa-times' aria-hidden='true'></i></button></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-md-offset-10 col-sm-2">
                <button  class="btn btn-sm btn-primary btn-block submitBtn" name="button">Submit</button>
              </div>
          </div>

        </form>
        </div><!--/.portlet-body-->
        </div><!--/.portlet-light-->
      </div>
    </div>
@endsection

@section('note')
  {{-- 2018 &copy; RSHK - PT. Nusamart Aulia Mandiri. All Rights Reserved. --}}
@endsection

@section('js')
<script type="text/javascript">
function InputQty(a) {
  var subtot = 0;
  var nilai = $(a).val();
  var nilai_tr = $(a).attr('data-id');
  var vendorData = $(a).attr('data-vendor');
  var AllSubtot = 0;

  $(".qty_"+nilai_tr).removeAttr();
  $(".qty_"+nilai_tr).attr("value",nilai)

    $.each($('.price_pcs'), function(k,v){
      var priceId = $(v).attr('data-id');
      if(priceId == nilai_tr){
        var price = $(v).val();
            total = (nilai*price);
        $('td.total_'+$(a).attr('data-id')).html(addCommas(Math.ceil(total)));
      }
    });
}


var name = "";
var id = "";

function poNumber(a) {
  id_do = $(a).val();
  // if(table >1 ){
    $(".submitBtn").attr('type', 'submit');
  // }else{
  //   $(".submitBtn").attr('type', 'button');
  // }
  $.ajax({
    url: '{{URL::to('catalog/pphp/bast/receive/detail')}}',
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", "id_do" : id_do},
    success: function (a) {
      $('#vendor').val(a.vendor.vendor_name);
      $('#address').val(a.vendor.address);
      $('#telephone').val(a.vendor.telephone);

      var table = "";
      $.each(a.data, function(k,v){
        var total = parseInt(v.qty_kirim)*parseInt(v.price_gov);
        table += '<tr>'+
                    '<td>'+v.code+'</td>'+
                    '<td>'+v.name+'</td>'+
                    '<td>'+addCommas(v.qty)+'</td>'+
                    '<td>';
                      if(v.id_item in a.dataReceive){
                      table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+a.dataReceive[v.id_item]+'" data-id="'+v.id+'">'
                      // table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" oninput="InputQty(this)" max="'+a.dataReceive[v.id_item]+'" name="receive_qty_item[]" value="'+(parseInt(a.dataReceive[v.id_item])+parseInt(a.dataReject[v.id_item]))+'" data-id="'+v.id+'">'
                      table +=  '<input type="hidden" name="receive_satuan[]" value="'+a.dataSatuan[v.id_item]+'">'
                      }else{
                          table +=  '<input type="number" min="0" class="form-control qty qty_'+v.id+'" disabled oninput="InputQty(this)" max="0" name="receive_qty_item[]" value="0" data-id="'+v.id+'">'
                      }
                      table += '<input type="hidden" name="receive_id_item[]" value="'+v.id+'">'+
                      '<input type="hidden" name="id_receive_item" value="'+v.id_receive_item+'">'+
                      '<input type="hidden" name="no_po" value="'+v.id_order+'">'+
                    '</td>'+
                    '<td>'+
                        addCommas(v.price_gov)+
                      '<input type="hidden" name="price_pcs" value="'+v.price_gov+'" class="price_pcs" data-id="'+v.id+'">'+
                      {{-- <input type="hidden" name="price_shipment" value="{{$v->price_shipment}}" class="price_shipment" data-id="{{$v->id}}"> --}}
                    '</td>'+
                    '<td>';
                    if(v.id_item in a.dataSatuan){
                      table += a.dataSatuanName[v.id_item];
                    }else{
                      table += a.ReceiveItemSatuanAll[v.id_item]
                    }
                    table += '</td>';
                    table += '<td class="total total_'+v.id+'">';
                        if(v.id_item in a.dataReceive){
                          table += addCommas(a.dataReceive[v.id_item]*v.price_gov);
                        }else{
                          table += addCommas(0*v.price_gov);
                        }
                    table += '</td>';
                    table += '<td>';
                    if(v.id_item in a.dataDesc){
                      table += '<input type="text" name="receive_keterangan[]" class="form-control" value="'+a.dataDesc[v.id_item]+'">';
                    }else{
                      table += '<input type="text" name="receive_keterangan[]" readonly class="form-control" value="'+a.ReceiveItemDescAll[v.id_item]+'">';
                    }

                    table += '</td>'+
                    '<td><button class="btn btn-danger btn-sm btn-round" onclick="remove(this)" data-id="'+v.id_order+'"><i class="fa fa-times" aria-hidden="true"></i></button></td>'+
                  '</tr>';
      });



      $('table#itemKirim tbody').html(table);

    }
  });
}

var arr = [];
function AddItem(a) {
  if(name !== ''){
    var tr = "<tr id='tr_"+id+"' data-id='"+id+"'>"+
    "<td>"+name+"<input type='hidden' name='idDO[]' value='"+id_item+"'></td>"+
    "<td>"+$('#qty').val()+"<input type='hidden' name='qtyDO[]' value='"+$('#qty').val()+"'></td>"+
    "<td>"+$('#satuan').val()+"<input type='hidden' name='satuanDO[]' value='"+$('#satuan').val()+"'></td>"+
    "<td>"+$('#keterangan').val()+"<input type='hidden' name='keteranganDO[]' value='"+$('#keterangan').val()+"'></td>"+
    "<td><button class='btn btn-danger btn-sm btn-round' onclick='remove(this)' data-id='"+id+"'><i class='fa fa-times' aria-hidden='true'></i></button></td>"+
    "</tr>";
    if((!arr[id]) && $('#qty').val() > 0){
      $("#itemKirim tbody").append(tr);
    }
    arr[id] = true;
  }
  arr[id] = true;
  table = $('#itemKirim tr').length;
  // if(table >1 ){
  //   $(".submitBtn").attr('type', 'submit');
  // }else{
  //   $(".submitBtn").attr('type', 'button');
  // }
}

function remove(a) {
  $(a).attr('data-id');
  $("#itemKirim tbody #tr_"+$(a).attr('data-id')).remove();
  arr[$(a).attr('data-id')] = false;

  table = $('#itemKirim tr').length;
  // if(table >1 ){
  //   $(".submitBtn").attr('type', 'submit');
  // }else{
  //   $(".submitBtn").attr('type', 'button');
  // }
}


  $(document).ready(function(){
    table = $('#itemKirim tr').length;
    if(table >1 ){
      $(".submitBtn").attr('type', 'submit');
    }else{
      $(".submitBtn").attr('type', 'button');
    }
  });

  function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
</script>
@endsection
