<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>
    @yield('title')
  </title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="/assets/logo.png">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"><!--- fonts for slider on the index page -->
  <!-- Fonts END -->

  <!-- Global styles START -->
  <link href="/su_catalog/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/su_catalog/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END -->

  <!-- Page level plugin styles START -->
  <link href="/su_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="/su_catalog/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="/su_catalog/assets/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
  @yield('plugins')
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="/su_catalog/assets/global/css/components.css" rel="stylesheet">
  <link href="/su_catalog/assets/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/su_catalog/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css">
  <link href="/su_catalog/assets/frontend/pages/css/style-layer-slider.css" rel="stylesheet">
  @yield('css')
  <link href="/su_catalog/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="/su_catalog/assets/frontend/layout/css/themes/blue.css" rel="stylesheet" id="style-color">
  <link href="/su_catalog/assets/frontend/layout/css/custom.css" rel="stylesheet">

  <!--DATATABLES-->
  <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">

  <!--SELECT2-->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">

  <!--DATEPICKER-->
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <!-- Theme styles END -->
  @yield('css')
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="ecommerce">
    <!-- BEGIN STYLE CUSTOMIZER -->
    {{-- <div class="color-panel hidden-sm">
      <div class="color-mode-icons icon-color"></div>
      <div class="color-mode-icons icon-color-close"></div>
      <div class="color-mode">
        <p>THEME COLOR</p>
        <ul class="inline">
          <li class="color-red current color-default" data-style="red"></li>
          <li class="color-blue" data-style="blue"></li>
          <li class="color-green" data-style="green"></li>
          <li class="color-orange" data-style="orange"></li>
          <li class="color-gray" data-style="gray"></li>
          <li class="color-turquoise" data-style="turquoise"></li>
        </ul>
      </div>
    </div> --}}
    <!-- END BEGIN STYLE CUSTOMIZER -->

    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            @include('catalog.depo.layouts.pre-header')
        </div>
    </div>
    <!-- END TOP BAR -->

    <!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        {{-- @yield('logo') --}}
        @include('catalog.depo.layouts.logo')
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>


        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation navi" style="float: left; padding-right: 20px; margin-top: 0px;">
          @include('catalog.depo.layouts.header-navigation')
        </div>
        <!-- END NAVIGATION -->
      </div>
      <!-- BEGIN CART -->
      {{-- <div class="" style="padding-right:95px; margin-bottom:10px;">
        <div class="top-cart-block">
          @include('catalog.users.layouts.top-cart-block')
        </div>
      </div> --}}
      <!--END CART -->
    </div>

    <!-- Header END -->

    <!-- BEGIN SLIDER -->
    {{-- @include('catalog.guest.layouts.page-slider') --}}
    <!-- END SLIDER -->

    <div class="main" style="margin-bottom: 75px;">
      <div class="container">
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row">
          <!-- BEGIN SIDEBAR -->
          @yield('sidebar')
          <!-- END SIDEBAR -->
          <!-- BEGIN CONTENT -->
          @yield('content')
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN BRANDS -->
    {{-- @include('catalog.guest.layouts.brands') --}}
    <!-- END BRANDS -->

    <!-- BEGIN STEPS -->
    {{-- @include('catalog.guest.layouts.steps-block') --}}
    <!-- END STEPS -->

    <!-- BEGIN PRE-FOOTER -->
    {{-- @include('catalog.guest.layouts.pre-footer') --}}
    <!-- END PRE-FOOTER -->

    <!-- BEGIN FOOTER -->
    <div class="footer" style="position: fixed; left: 0; bottom: 0; width: 100%; z-index: 99999;">
      <div class="container">
        @yield('note')
        @include('catalog.depo.layouts.note')
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- BEGIN fast view of a product -->
    @yield('product-pop-up')
    <!-- END fast view of a product -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="/su_catalog/assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="/su_catalog/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/su_catalog/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="/su_catalog/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/su_catalog/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <script src="/su_catalog/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/su_catalog/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="/su_catalog/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
    <script src='/su_catalog/assets/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
    <script src="/su_catalog/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
    @yield('js')

    <!-- BEGIN LayerSlider -->
    <script src="/su_catalog/assets/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
    <script src="/su_catalog/assets/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
    <script src="/su_catalog/assets/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
    <script src="/su_catalog/assets/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
    <!-- END LayerSlider -->

    <script src="/su_catalog/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>


        <!--DATATABLES-->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
        {{-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" type="text/javascript"></script> --}}
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>


        <!--DATEPICKER-->
        <script src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


        <!---SELECT-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
        <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            LayersliderInit.initLayerSlider();
            Layout.initImageZoom();
            Layout.initTouchspin();
            Layout.initTwitter();

            Layout.initFixHeaderWithPreHeader();
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
    @yield('xjs')
</body>
<!-- END BODY -->
</html>
