<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#login
	Route::get('/a', function(){
		try {
    dd(DB::connection()->getPdo());

		} catch (\Exception $e) {
    	die("Could not connect to the database.  Please check your configuration. error:" . $e );
		}
	});
	Route::get('/test/api_token', 'TestController@index');
	Route::get('/auth', 'Auth\LoginController@showLoginForm')->name('login');
	// Route::get('/auth', 'Auth\LoginController@showLoginForm')->name('x.auth');
  Route::post('/auth', 'Auth\LoginController@login');
  Route::post('/login/image', 'Auth\LoginController@image');
	Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('/vcs', 'Auth\LoginController@vcs')->name('x.vcs');

#register user
Route::get('/vms', function () { return redirect('/vms/guest'); })->name('x.vms');
Route::get('/vms/guest', function () { return view('vms.guest.dashboard.index'); })->name('x.vms.guest');
Route::get('/vms/guest/register', 'Vms\guest\register\RegisterControllers@showRegistrationForm')->name('x.vms.register');
Route::post('/vms/guest/register', 'Vms\guest\register\RegisterControllers@save');
#new register
Route::get('vms/users/registration/{id?}', 'Vms\guest\GuestVmsController@registrationByNoRegis');
############VENDOR REGIS
Route::get('vendor/register', 'Vms\guest\GuestVmsController@vendorRegis');
Route::post('vendor/register', 'Vms\guest\GuestVmsController@vendorRegisStore');
############VENDOR REGIS

Route::get('vms/users/registration/{id?}', 'Vms\guest\GuestVmsController@registrationByNoRegis');
Route::post('register/getKota', 'Vms\guest\GuestVmsController@getKota');
Route::get('vms/users/invitation/{id?}/attend', 'Vms\guest\GuestVmsController@attend_invitation');
Route::get('vms/users/data_changes/{id?}', 'Vms\guest\GuestVmsController@data_changes');
Route::post('/vms/guest/register/classification', 'Vms\guest\GuestVmsController@classification');
Route::post('/vms/guest/register/create-step1/{id?}', 'Vms\guest\GuestVmsController@postCreateStep1');
Route::get('/vms/guest/register/create-step2/{id?}', 'Vms\guest\GuestVmsController@createStep2');
Route::post('/vms/guest/register/create-step2/{id?}', 'Vms\guest\GuestVmsController@postCreateStep2');
Route::get('/vms/guest/register/create-step3/{id?}', 'Vms\guest\GuestVmsController@createStep3');
Route::post('/vms/guest/register/create-step3/{id?}', 'Vms\guest\GuestVmsController@postCreateStep3');
Route::get('/vms/guest/register/create-step4/{id?}', 'Vms\guest\GuestVmsController@createStep4');
Route::post('/vms/guest/register/create-step4/{id?}', 'Vms\guest\GuestVmsController@postCreateStep4');
Route::get('/vms/guest/register/create-step5/{id?}', 'Vms\guest\GuestVmsController@createStep5');
Route::post('/vms/guest/register/create-step5/{id?}', 'Vms\guest\GuestVmsController@postCreateStep5');
Route::get('/vms/guest/register/create-step6/{id?}', 'Vms\guest\GuestVmsController@createStep6');
Route::post('/vms/guest/register/create-step6/{id?}', 'Vms\guest\GuestVmsController@postCreateStep6');
Route::get('/vms/guest/register/create-step7/{id?}', 'Vms\guest\GuestVmsController@createStep7');
Route::post('/vms/guest/register/create-step7/{id?}', 'Vms\guest\GuestVmsController@postCreateStep7');
Route::get('/vms/guest/register/create-step8/{id?}', 'Vms\guest\GuestVmsController@createStep8');
Route::post('/vms/guest/register/create-step8/logo/{id?}', 'Vms\guest\GuestVmsController@logoUpload');
Route::post('/vms/guest/register/create-step8/logo/delete/{id?}', 'Vms\guest\GuestVmsController@logoDelete');
Route::post('/vms/guest/register/create-step8/npwp/{id?}', 'Vms\guest\GuestVmsController@npwpUpload');
Route::post('/vms/guest/register/create-step8/npwp/delete/{id?}', 'Vms\guest\GuestVmsController@npwpDelete');
Route::post('/vms/guest/register/create-step8/akta/{id?}', 'Vms\guest\GuestVmsController@aktaUpload');
Route::post('/vms/guest/register/create-step8/akta/delete/{id?}', 'Vms\guest\GuestVmsController@aktaDelete');
Route::post('/vms/guest/register/create-step8/surat/{id?}', 'Vms\guest\GuestVmsController@suratUpload');
Route::post('/vms/guest/register/create-step8/surat/delete/{id?}', 'Vms\guest\GuestVmsController@suratDelete');
Route::post('/vms/guest/register/create-step8/pemilik/{id?}', 'Vms\guest\GuestVmsController@pemilikUpload');
Route::post('/vms/guest/register/create-step8/pemilik/delete/{id?}', 'Vms\guest\GuestVmsController@pemilikDelete');
Route::post('/vms/guest/register/create-step8/pengurus/{id?}', 'Vms\guest\GuestVmsController@pengurusUpload');
Route::post('/vms/guest/register/create-step8/pengurus/delete/{id?}', 'Vms\guest\GuestVmsController@pengurusDelete');
Route::post('/vms/guest/register/create-step8/tenaga_ahli/{id?}', 'Vms\guest\GuestVmsController@tenagaAhliUpload');
Route::post('/vms/guest/register/create-step8/tenaga_ahli/delete/{id?}', 'Vms\guest\GuestVmsController@tenagaAhliDelete');
Route::get('/vms/guest/register/review/{id?}', 'Vms\guest\GuestVmsController@review');
Route::post('/vms/guest/register/formRegister/store/{id?}', 'Vms\guest\GuestVmsController@store');
Route::get('/vms/guest/register/done', 'Vms\guest\GuestVmsController@done');

#activation
Route::get('/vms/guest/activation', 'Vms\guest\activation\ActivationController@index')->name('x.vms.activation');
Route::post('activation/check', 'Vms\guest\activation\ActivationController@store');

######################

Route::get('/catalog', function () { return view('catalog.guest.dashboard.index'); })->name('x.catalog');
Route::get('/catalog/guest', function () { return view('catalog.guest.dashboard.index'); })->name('x.catalog.guest.item');//->name('x.catalog.guest');
Route::get('/', 'Catalog\guest\GuestCatalogController@index')->name('x');
Route::get('/catalog/guest/item/detail/{id}','Catalog\guest\GuestCatalogController@detail')->name('x.catalog.guest.item.detail');

Route::get('/catalog/users', function () { return view('catalog.users.dashboard.index'); })->name('x.catalog.users');
Route::get('/catalog/users/item/detail/{id}', function ($id) { return view('catalog.users.item.detail')->with('id', $id); })->name('x.catalog.users.item.detail');
Route::get('/catalog/admin/item/view/{id}', function ($id) { return view('catalog.admin.item.view')->with('id', $id); })->name('x.catalog.admin.item.view');
Route::get('/catalog/admin/item/list/{id}', function ($id) { return view('catalog.admin.item.list')->with('id', $id); })->name('x.catalog.admin.item.list');


Route::group(['namespace' => 'Catalog\pphp','middleware' => 'pphpauth'], function () {
	Route::post('catalog/pphp/action_ganti_password', 'PphpController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/pphp/reset', 'PphpController@reset')->name('reset');
	Route::get('catalog/pphp/dashboard', ['uses'=>'PphpController@dashboard']);
	Route::get('catalog/pphp/index', ['uses'=>'PphpController@index']);
	Route::get('catalog/pphp/preview/{id?}/{id_do?}', ['uses'=>'PphpController@trackItem']);
	Route::get('catalog/pphp/create', ['uses'=>'PphpController@create']);
	Route::post('catalog/pphp/receive_item/detail', ['uses'=>'PphpController@detail']);
	Route::post('catalog/pphp/receive_item/detailItem', ['uses'=>'PphpController@detailItem']);
	Route::post('catalog/pphp/receive_item/spjItem', ['uses'=>'PphpController@spjItem']);
	Route::post('catalog/pphp/receiveSave', ['uses'=>'PphpController@receiveSave']);
	Route::post('catalog/pphp/upload/spj/{id?}', ['uses'=>'PphpController@uploadSpj']);
	Route::get('catalog/pphp/bast/index', ['uses'=>'PphpController@bastIndex']);
	Route::get('catalog/pphp/bapb/preview/{id?}/{id_do?}', ['uses'=>'PphpController@bapbPreview']);
	// Route::get('catalog/pphp/bast/preview/{id?}/{id_do?}', ['uses'=>'PphpController@bastPreview']);
	Route::get('catalog/pphp/bast/create/{id?}/{id_do?}', ['uses'=>'PphpController@createBastFromReceiveModul']);
	Route::post('catalog/pphp/bast/receive/detail', ['uses'=>'PphpController@BastReceiveDetail']);
	Route::post('catalog/pphp/bastSave', ['uses'=>'PphpController@bastSave']);
	Route::get('catalog/pphp/po/download/{id?}', ['uses'=>'PphpController@downloadPo']);
	Route::get('catalog/pphp/to_warehouse/index', ['uses'=>'PphpController@warehouseIndex']);
	Route::get('catalog/pphp/to_warehouse/create', ['uses'=>'PphpController@warehouseCreate']);
	Route::post('catalog/pphp/to_warehouse/conversion', ['uses'=>'PphpController@warehouseConversion']);
});

Route::group(['namespace' => 'Catalog\warehouse','middleware' => 'warehouseauth'], function () {

	Route::post('catalog/warehouse/action_ganti_password', 'WarehouseController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/warehouse/reset', 'WarehouseController@reset')->name('reset');
	Route::get('catalog/warehouse/index', ['uses'=>'WarehouseController@index']);
	Route::get('catalog/warehouse/', ['uses'=>'WarehouseController@create']);
	Route::post('catalog/warehouse/import/data', ['uses'=>'WarehouseController@InventoryAdd']);
	Route::get('/catalog/warehouse/stockin/index', ['uses'=>'WarehouseController@stockIn']);
	Route::get('/catalog/warehouse/stockout/index', ['uses'=>'WarehouseController@stockOut']);
	Route::get('/catalog/warehouse/stockout/create', ['uses'=>'WarehouseController@stockOutCreate']);
	Route::post('/catalog/warehouse/stockout/create', ['uses'=>'WarehouseController@stockOutCreate']);
	Route::post('/catalog/warehouse/stockout/store', ['uses'=>'WarehouseController@stockOutStore']);
	Route::get('/catalog/warehouse/stockout/preview/{id?}', ['uses'=>'WarehouseController@stockOutPreview']);

	############DISTRIBUSI DEPO
	Route::get('catalog/warehouse/distributed/index', ['uses'=>'WarehouseController@depoMonitoring']);
	Route::get('catalog/warehouse/distributed/depo/{id?}', ['uses'=>'WarehouseController@depoCreate']);
	Route::post('catalog/warehouse/distributed/depo/{id?}', ['uses'=>'WarehouseController@depoStore']);

	#######REPORT
	Route::get('catalog/warehouse/report/stock', ['uses'=>'WarehouseController@reportStock']);
	Route::get('catalog/warehouse/report/stockin', ['uses'=>'WarehouseController@reportStockIn']);
	Route::get('catalog/warehouse/report/stockout', ['uses'=>'WarehouseController@reportStockOut']);


});


// Depo
Route::group(['namespace' => 'Catalog\depo','middleware' => 'depoauth'], function () {
	Route::post('catalog/depo/action_ganti_password', 'DepoController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/depo/reset', 'DepoController@reset')->name('reset');
	Route::get('catalog/depo/dashboard', ['as'=>'x.catalog.admin.depo','uses'=>'DepoController@dashboard']);
	Route::get('catalog/depo/index', ['uses'=>'DepoController@index']);
	Route::get('catalog/depo/', ['uses'=>'DepoController@create']);
	Route::post('catalog/depo/import/data', ['uses'=>'DepoController@InventoryAdd']);
	Route::get('/catalog/depo/stockin/index', ['uses'=>'DepoController@stockIn']);
	Route::get('/catalog/depo/stockout/index', ['uses'=>'DepoController@stockOut']);
	Route::get('/catalog/depo/stockout/create', ['uses'=>'DepoController@stockOutCreate']);
	Route::post('/catalog/depo/stockout/create', ['uses'=>'DepoController@stockOutCreate']);
	Route::post('/catalog/depo/stockout/store', ['uses'=>'DepoController@stockOutStore']);
	Route::get('/catalog/depo/stockout/preview/{id?}', ['uses'=>'DepoController@stockOutPreview']);

	############DISTRIBUSI DEPO
	Route::get('catalog/depo/distributed/index', ['uses'=>'DepoController@depoMonitoring']);
	Route::get('catalog/depo/distributed/depo/{id?}', ['uses'=>'DepoController@depoCreate']);
	Route::post('catalog/depo/distributed/depo/{id?}', ['uses'=>'DepoController@depoStore']);

	#######REPORT
	Route::get('catalog/depo/report/stock', ['uses'=>'DepoController@reportStock']);
	Route::get('catalog/depo/report/stockin', ['uses'=>'DepoController@reportStockIn']);
	Route::get('catalog/depo/report/stockout', ['uses'=>'DepoController@reportStockOut']);
});



########################################################CATALOG#####################################################
#AdminCatalog
Route::group(['namespace' => 'Catalog\admin','middleware' => 'adminauth'], function () {
	//Kontrak
	Route::get('catalog/admin/contract', 'AdminUsersCatalogController@contractIndex');
	Route::get('catalog/admin/contract/create', 'AdminUsersCatalogController@contractCreate');
	Route::post('catalog/admin/contract/create', 'AdminUsersCatalogController@contractStore');
	Route::get('catalog/admin/contract/detail/{id?}', 'AdminUsersCatalogController@contractDetail');
	Route::get('catalog/admin/adendum/create/{id?}', ['uses'=>'AdminUsersCatalogController@adendumCreate']);
	Route::post('catalog/admin/adendum/create/{id?}', ['uses'=>'AdminUsersCatalogController@adendumStore']);
	Route::get('catalog/admin/contract/adendum/{id?}', ['uses'=>'AdminUsersCatalogController@adendumIndex']);
	///
	Route::post('catalog/admin/vendor/contract', ['as' => 'x.catalog.admin.item','uses'=>'AdminItemCatalogController@ContractVendor']);
	Route::post('catalog/admin/vendor/adendum', ['as' => 'x.catalog.admin.item','uses'=>'AdminItemCatalogController@AdendumVendor']);
	///
	Route::post('catalog/admin/action_ganti_password', 'AdminCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/admin/reset', 'AdminCatalogController@reset')->name('reset');
	Route::get('catalog/admin/lengkapi', 'AdminCatalogController@lengkapi');
	//master vendor
	Route::get('catalog/admin/users/master_vendor', ['as' => 'x.catalog.admin.users.master_vendor','uses'=>'AdminUsersCatalogController@master_vendor']);
	Route::get('catalog/admin/users/master_vendor_tambah', ['as' => 'x.catalog.admin.users.master_vendor_tambah','uses'=>'AdminUsersCatalogController@master_vendor_tambah']);
	Route::post('catalog/admin/users/master_vendor_simpan', ['as' => 'x.catalog.admin.users.master_vendor_simpan','uses'=>'AdminUsersCatalogController@master_vendor_simpan']);
	Route::get('catalog/admin/users/master_vendor_edit/{id}', ['as' => 'x.catalog.admin.users.master_vendor_edit','uses'=>'AdminUsersCatalogController@master_vendor_edit']);
	Route::put('catalog/admin/users/master_vendor_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.master_vendor_edit_simpan','uses'=>'AdminUsersCatalogController@master_vendor_edit_simpan']);
	Route::delete('catalog/admin/users/master_vendor_delete/{id}', ['as' => 'x.catalog.admin.users.master_vendor_delete','uses'=>'AdminUsersCatalogController@master_vendor_delete']);
	//dashboard
	Route::get('catalog/admin/', ['as'=>'x.catalog.admin','uses'=>'AdminCatalogController@index']);
	//kategori
	Route::get('catalog/admin/category', ['as' => 'x.catalog.admin.category','uses'=>'AdminCatalogController@category']);
	Route::post('catalog/admin/category/add', ['as' => 'x.catalog.admin.category.add','uses'=>'AdminCatalogController@AddCategory']);
	Route::post('catalog/admin/category/update', ['as' => 'x.catalog.admin.category.update','uses'=>'AdminCatalogController@UpdateCategory']);
	Route::post('catalog/admin/category/delete', ['as' => 'x.catalog.admin.category.delete','uses'=>'AdminCatalogController@DeleteCategory']);
	Route::post('catalog/admin/category/spec/add', ['uses'=>'AdminCatalogController@AddSpecCategory']);
	Route::post('catalog/admin/category/spec/update', ['uses'=>'AdminCatalogController@UpdateSpecCategory']);
	Route::post('catalog/admin/category/spec/delete', ['uses'=>'AdminCatalogController@DeleteSpecCategory']);
	Route::get('catalog/admin/category/spec/{id}', ['as' => 'x.catalog.admin.category.spec', 'uses'=>'AdminCatalogController@SpecCategory']);
	//item
	Route::get('catalog/admin/item', ['as' => 'x.catalog.admin.item','uses'=>'AdminItemCatalogController@index']);
	Route::get('catalog/admin/item/create/{id}', ['as' => 'x.catalog.admin.item.create','uses'=>'AdminItemCatalogController@create']);
	Route::post('catalog/admin/item/store', ['as' => 'x.catalog.admin.item.create','uses'=>'AdminItemCatalogController@store']);
	Route::get('catalog/admin/item/update/{id}', ['as' => 'x.catalog.admin.item.update','uses'=>'AdminItemCatalogController@edit']);
	Route::post('catalog/admin/item/update/{id}', ['as' => 'x.catalog.admin.item.update','uses'=>'AdminItemCatalogController@update']);
	Route::get('catalog/admin/item/price/{id}', ['as' => 'x.catalog.admin.item.price','uses'=>'AdminItemCatalogController@historyPrice']);
	Route::get('catalog/admin/item/delete/{id}', ['uses'=>'AdminItemCatalogController@DeleteItem']);
	Route::post('catalog/admin/item/spec/save', ['uses'=>'AdminItemCatalogController@SaveSpec']);
	Route::get('catalog/admin/item/spec/{id}', ['as' => 'x.catalog.admin.item.spec', 'uses'=>'AdminItemCatalogController@spec']);
	Route::get('catalog/admin/item/image/{id}', ['as' => 'x.catalog.admin.item.image', 'uses'=>'AdminItemCatalogController@image']);
	Route::post('catalog/admin/item/image/{id}', ['uses'=>'AdminItemCatalogController@imageSave']);
	Route::get('catalog/admin/item/image/delete/{id}', ['uses'=>'AdminItemCatalogController@DeleteImage']);
	Route::get('catalog/admin/item/doc/{id}', ['as' => 'x.catalog.admin.item.image', 'uses'=>'AdminItemCatalogController@doc']);
	Route::post('catalog/admin/item/doc/{id}', ['uses'=>'AdminItemCatalogController@DocSave']);
	Route::get('catalog/admin/item/doc/delete/{id}', ['uses'=>'AdminItemCatalogController@DeleteDoc']);
	Route::get('catalog/admin/item/doc/delete/{id}', ['uses'=>'AdminItemCatalogController@DeleteDoc']);
	Route::post('catalog/admin/item/doc/status/update', ['as' => 'x.catalog.admin.item.status','uses'=>'AdminItemCatalogController@status']);
	Route::post('catalog/admin/item/upload', ['uses'=>'AdminItemCatalogController@UploadItem']);
	Route::post('catalog/admin/item/notupload', ['uses'=>'AdminItemCatalogController@ListNotUploadItem']);
	Route::get('catalog/admin/item/upload/detail/{id?}', ['uses'=>'AdminItemCatalogController@detailUpload']);
	//users
	Route::get('catalog/admin/users', ['as' => 'x.catalog.admin.users','uses'=>'AdminUsersCatalogController@index']);
	Route::post('catalog/admin/users/status', ['as' => 'x.catalog.admin.users.status','uses'=>'AdminUsersCatalogController@status']);
	Route::post('catalog/admin/users/create', ['as' => 'x.catalog.admin.users.create','uses'=>'AdminUsersCatalogController@create']);
	Route::get('catalog/admin/users/getdata', ['as' => 'x.catalog.admin.users.getdata','uses'=>'AdminUsersCatalogController@getdata']);
	Route::post('catalog/admin/users/update', ['as' => 'x.catalog.admin.users.update','uses'=>'AdminUsersCatalogController@update']);
	Route::post('catalog/admin/users/category', ['as' => 'x.catalog.admin.users.category','uses'=>'AdminUsersCatalogController@category']);
	Route::get('catalog/admin/users/category', ['as' => 'x.catalog.admin.users.category','uses'=>'AdminUsersCatalogController@getCategory']);
	Route::get('catalog/admin/users/direktur_penunjang', ['as' => 'x.catalog.admin.users.direktur_penunjang','uses'=>'AdminUsersCatalogController@direktur_penunjang']);
	//direktur
	Route::get('catalog/admin/users/direktur_penunjang_tambah', ['as' => 'x.catalog.admin.users.direktur_penunjang_tambah','uses'=>'AdminUsersCatalogController@direktur_penunjang_tambah']);
	Route::post('catalog/admin/users/direktur_penunjang_simpan', ['as' => 'x.catalog.admin.users.direktur_penunjang_simpan','uses'=>'AdminUsersCatalogController@direktur_penunjang_simpan']);
	Route::get('catalog/admin/users/direktur_penunjang_edit/{id}', ['as' => 'x.catalog.admin.users.direktur_penunjang_edit','uses'=>'AdminUsersCatalogController@direktur_penunjang_edit']);
	Route::put('catalog/admin/users/direktur_penunjang_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.direktur_penunjang_edit_simpan','uses'=>'AdminUsersCatalogController@direktur_penunjang_edit_simpan']);
	Route::delete('catalog/admin/users/direktur_penunjang_delete/{id}', ['as' => 'x.catalog.admin.users.direktur_penunjang_delete','uses'=>'AdminUsersCatalogController@direktur_penunjang_delete']);
	Route::post('catalog/admin/users/direktur_main_category', ['as' => 'x.catalog.admin.users.direktur_main_category','uses'=>'AdminUsersCatalogController@direktur_main_category']);
	//ppk
	Route::get('catalog/admin/users/ppk', ['as' => 'x.catalog.admin.users.ppk','uses'=>'AdminUsersCatalogController@ppk']);
	Route::get('catalog/admin/users/ppk_tambah', ['as' => 'x.catalog.admin.users.ppk_tambah','uses'=>'AdminUsersCatalogController@ppk_tambah']);
	Route::post('catalog/admin/users/ppk_simpan', ['as' => 'x.catalog.admin.users.ppk_simpan','uses'=>'AdminUsersCatalogController@ppk_simpan']);
	Route::get('catalog/admin/users/ppk_edit/{id}', ['as' => 'x.catalog.admin.users.ppk_edit','uses'=>'AdminUsersCatalogController@ppk_edit']);
	Route::put('catalog/admin/users/ppk_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.ppk_edit_simpan','uses'=>'AdminUsersCatalogController@ppk_edit_simpan']);
	Route::delete('catalog/admin/users/ppk_delete/{id}', ['as' => 'x.catalog.admin.users.ppk_delete','uses'=>'AdminUsersCatalogController@ppk_delete']);
	//pphp
	Route::get('catalog/admin/users/pphp', ['as' => 'x.catalog.admin.users.pphp','uses'=>'AdminUsersCatalogController@pphp']);
	Route::get('catalog/admin/users/pphp_tambah', ['as' => 'x.catalog.admin.users.pphp_tambah','uses'=>'AdminUsersCatalogController@pphp_tambah']);
	Route::post('catalog/admin/users/pphp_simpan', ['as' => 'x.catalog.admin.users.pphp_simpan','uses'=>'AdminUsersCatalogController@pphp_simpan']);
	Route::get('catalog/admin/users/pphp_edit/{id}', ['as' => 'x.catalog.admin.users.pphp_edit','uses'=>'AdminUsersCatalogController@pphp_edit']);
	Route::put('catalog/admin/users/pphp_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.pphp_edit_simpan','uses'=>'AdminUsersCatalogController@pphp_edit_simpan']);
	Route::delete('catalog/admin/users/pphp_delete/{id}', ['as' => 'x.catalog.admin.users.pphp_delete','uses'=>'AdminUsersCatalogController@pphp_delete']);
	//gudang
	Route::get('catalog/admin/users/gudang', ['as' => 'x.catalog.admin.users.gudang','uses'=>'AdminUsersCatalogController@gudang']);
	Route::get('catalog/admin/users/gudang_tambah', ['as' => 'x.catalog.admin.users.gudang_tambah','uses'=>'AdminUsersCatalogController@gudang_tambah']);
	Route::post('catalog/admin/users/gudang_simpan', ['as' => 'x.catalog.admin.users.gudang_simpan','uses'=>'AdminUsersCatalogController@gudang_simpan']);
	Route::get('catalog/admin/users/gudang_edit/{id}', ['as' => 'x.catalog.admin.users.gudang_edit','uses'=>'AdminUsersCatalogController@gudang_edit']);
	Route::put('catalog/admin/users/gudang_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.gudang_edit_simpan','uses'=>'AdminUsersCatalogController@gudang_edit_simpan']);
	Route::delete('catalog/admin/users/gudang_delete/{id}', ['as' => 'x.catalog.admin.users.gudang_delete','uses'=>'AdminUsersCatalogController@gudang_delete']);
	//finance
	Route::get('catalog/admin/users/finance', ['as' => 'x.catalog.admin.users.finance','uses'=>'AdminUsersCatalogController@finance']);
	Route::get('catalog/admin/users/finance_tambah', ['as' => 'x.catalog.admin.users.finance_tambah','uses'=>'AdminUsersCatalogController@finance_tambah']);
	Route::post('catalog/admin/users/finance_simpan', ['as' => 'x.catalog.admin.users.finance_simpan','uses'=>'AdminUsersCatalogController@finance_simpan']);
	Route::get('catalog/admin/users/finance_edit/{id}', ['as' => 'x.catalog.admin.users.finance_edit','uses'=>'AdminUsersCatalogController@finance_edit']);
	Route::put('catalog/admin/users/finance_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.finance_edit_simpan','uses'=>'AdminUsersCatalogController@finance_edit_simpan']);
	Route::delete('catalog/admin/users/finance_delete/{id}', ['as' => 'x.catalog.admin.users.finance_delete','uses'=>'AdminUsersCatalogController@finance_delete']);
	//satuan
	Route::get('catalog/admin/users/satuan', ['as' => 'x.catalog.admin.users.satuan','uses'=>'AdminUsersCatalogController@satuan']);
	Route::get('catalog/admin/users/satuan_tambah', ['as' => 'x.catalog.admin.users.satuan_tambah','uses'=>'AdminUsersCatalogController@satuan_tambah']);
	Route::post('catalog/admin/users/satuan_simpan', ['as' => 'x.catalog.admin.users.satuan_simpan','uses'=>'AdminUsersCatalogController@satuan_simpan']);
	Route::get('catalog/admin/users/satuan_edit/{id}', ['as' => 'x.catalog.admin.users.satuan_edit','uses'=>'AdminUsersCatalogController@satuan_edit']);
	Route::put('catalog/admin/users/satuan_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.satuan_edit_simpan','uses'=>'AdminUsersCatalogController@satuan_edit_simpan']);
	Route::delete('catalog/admin/users/satuan_delete/{id}', ['as' => 'x.catalog.admin.users.satuan_delete','uses'=>'AdminUsersCatalogController@satuan_delete']);
	//jenis_gudang
	Route::get('catalog/admin/users/jenis_gudang', ['as' => 'x.catalog.admin.users.jenis_gudang','uses'=>'AdminUsersCatalogController@jenis_gudang']);
	Route::get('catalog/admin/users/jenis_gudang_tambah', ['as' => 'x.catalog.admin.users.jenis_gudang_tambah','uses'=>'AdminUsersCatalogController@jenis_gudang_tambah']);
	Route::post('catalog/admin/users/jenis_gudang_simpan', ['as' => 'x.catalog.admin.users.jenis_gudang_simpan','uses'=>'AdminUsersCatalogController@jenis_gudang_simpan']);
	Route::get('catalog/admin/users/jenis_gudang_edit/{id}', ['as' => 'x.catalog.admin.users.jenis_gudang_edit','uses'=>'AdminUsersCatalogController@jenis_gudang_edit']);
	Route::put('catalog/admin/users/jenis_gudang_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.jenis_gudang_edit_simpan','uses'=>'AdminUsersCatalogController@jenis_gudang_edit_simpan']);
	Route::delete('catalog/admin/users/jenis_gudang_delete/{id}', ['as' => 'x.catalog.admin.users.jenis_gudang_delete','uses'=>'AdminUsersCatalogController@jenis_gudang_delete']);
	//master_gudang
	Route::get('catalog/admin/users/master_gudang', ['as' => 'x.catalog.admin.users.master_gudang','uses'=>'AdminUsersCatalogController@master_gudang']);
	Route::get('catalog/admin/users/master_gudang_tambah', ['as' => 'x.catalog.admin.users.master_gudang_tambah','uses'=>'AdminUsersCatalogController@master_gudang_tambah']);
	Route::post('catalog/admin/users/master_gudang_simpan', ['as' => 'x.catalog.admin.users.master_gudang_simpan','uses'=>'AdminUsersCatalogController@master_gudang_simpan']);
	Route::get('catalog/admin/users/master_gudang_edit/{id}', ['as' => 'x.catalog.admin.users.master_gudang_edit','uses'=>'AdminUsersCatalogController@master_gudang_edit']);
	Route::put('catalog/admin/users/master_gudang_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.master_gudang_edit_simpan','uses'=>'AdminUsersCatalogController@master_gudang_edit_simpan']);
	Route::delete('catalog/admin/users/master_gudang_delete/{id}', ['as' => 'x.catalog.admin.users.master_gudang_delete','uses'=>'AdminUsersCatalogController@master_gudang_delete']);
	//user_staff_admin
	Route::get('catalog/admin/users/user_staff', ['as' => 'x.catalog.admin.users.user_staff','uses'=>'AdminUsersCatalogController@user_staff']);
	Route::get('catalog/admin/users/user_staff_tambah', ['as' => 'x.catalog.admin.users.user_staff_tambah','uses'=>'AdminUsersCatalogController@user_staff_tambah']);
	Route::post('catalog/admin/users/user_staff_simpan', ['as' => 'x.catalog.admin.users.user_staff_simpan','uses'=>'AdminUsersCatalogController@user_staff_simpan']);
	Route::get('catalog/admin/users/user_staff_edit/{id}', ['as' => 'x.catalog.admin.users.user_staff_edit','uses'=>'AdminUsersCatalogController@user_staff_edit']);
	Route::put('catalog/admin/users/user_staff_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.user_staff_edit_simpan','uses'=>'AdminUsersCatalogController@user_staff_edit_simpan']);
	Route::delete('catalog/admin/users/user_staff_delete/{id}', ['as' => 'x.catalog.admin.users.user_staff_delete','uses'=>'AdminUsersCatalogController@user_staff_delete']);
	//user_staff_admin
	Route::get('catalog/admin/users/inisial_gudang', ['as' => 'x.catalog.admin.users.inisial_gudang','uses'=>'AdminUsersCatalogController@inisial_gudang']);
	Route::get('catalog/admin/users/inisial_gudang_tambah', ['as' => 'x.catalog.admin.users.inisial_gudang_tambah','uses'=>'AdminUsersCatalogController@inisial_gudang_tambah']);
	Route::post('catalog/admin/users/inisial_gudang_simpan', ['as' => 'x.catalog.admin.users.inisial_gudang_simpan','uses'=>'AdminUsersCatalogController@inisial_gudang_simpan']);
	Route::get('catalog/admin/users/inisial_gudang_edit/{id}', ['as' => 'x.catalog.admin.users.inisial_gudang_edit','uses'=>'AdminUsersCatalogController@inisial_gudang_edit']);
	Route::put('catalog/admin/users/inisial_gudang_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.inisial_gudang_edit_simpan','uses'=>'AdminUsersCatalogController@inisial_gudang_edit_simpan']);
	Route::delete('catalog/admin/users/inisial_gudang_delete/{id}', ['as' => 'x.catalog.admin.users.inisial_gudang_delete','uses'=>'AdminUsersCatalogController@inisial_gudang_delete']);
	// master_depo
	Route::get('catalog/admin/users/depo', ['as' => 'x.catalog.admin.users.depo','uses'=>'AdminUsersCatalogController@depo']);
	Route::get('catalog/admin/users/depo_tambah', ['as' => 'x.catalog.admin.users.depo_tambah','uses'=>'AdminUsersCatalogController@depo_tambah']);
	Route::post('catalog/admin/users/depo_simpan', ['as' => 'x.catalog.admin.users.depo_simpan','uses'=>'AdminUsersCatalogController@depo_simpan']);
	Route::get('catalog/admin/users/depo_edit/{id}', ['as' => 'x.catalog.admin.users.depo_edit','uses'=>'AdminUsersCatalogController@depo_edit']);
	Route::put('catalog/admin/users/depo_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.depo_edit_simpan','uses'=>'AdminUsersCatalogController@depo_edit_simpan']);
	Route::delete('catalog/admin/users/depo_delete/{id}', ['as' => 'x.catalog.admin.users.depo_delete','uses'=>'AdminUsersCatalogController@depo_delete']);
	// user_depo
	Route::get('catalog/admin/users/users_depo', ['as' => 'x.catalog.admin.users.users_depo','uses'=>'AdminUsersCatalogController@users_depo']);
	Route::get('catalog/admin/users/users_depo_tambah', ['as' => 'x.catalog.admin.users.users_depo_tambah','uses'=>'AdminUsersCatalogController@users_depo_tambah']);
	Route::post('catalog/admin/users/users_depo_simpan', ['as' => 'x.catalog.admin.users.users_depo_simpan','uses'=>'AdminUsersCatalogController@users_depo_simpan']);
	Route::get('catalog/admin/users/users_depo_edit/{id}', ['as' => 'x.catalog.admin.users.users_depo_edit','uses'=>'AdminUsersCatalogController@users_depo_edit']);
	Route::put('catalog/admin/users/users_depo_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.users_depo_edit_simpan','uses'=>'AdminUsersCatalogController@users_depo_edit_simpan']);
	Route::delete('catalog/admin/users/users_depo_delete/{id}', ['as' => 'x.catalog.admin.users.users_depo_delete','uses'=>'AdminUsersCatalogController@users_depo_delete']);
	// master_konversi
	Route::get('catalog/admin/users/konversi', ['as' => 'x.catalog.admin.users.konversi','uses'=>'AdminUsersCatalogController@konversi']);
	Route::get('catalog/admin/users/konversi_tambah', ['as' => 'x.catalog.admin.users.konversi_tambah','uses'=>'AdminUsersCatalogController@konversi_tambah']);
	Route::post('catalog/admin/users/konversi_simpan', ['as' => 'x.catalog.admin.users.konversi_simpan','uses'=>'AdminUsersCatalogController@konversi_simpan']);
	Route::get('catalog/admin/users/konversi_edit/{id}', ['as' => 'x.catalog.admin.users.konversi_edit','uses'=>'AdminUsersCatalogController@konversi_edit']);
	Route::put('catalog/admin/users/konversi_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.konversi_edit_simpan','uses'=>'AdminUsersCatalogController@konversi_edit_simpan']);
	Route::delete('catalog/admin/users/konversi_delete/{id}', ['as' => 'x.catalog.admin.users.konversi_delete','uses'=>'AdminUsersCatalogController@konversi_delete']);
	// penyedia
	Route::get('catalog/admin/users/vendor', ['as' => 'x.catalog.admin.users.konversi','uses'=>'AdminUsersCatalogController@vendor']);
	Route::get('catalog/admin/users/vendor/create', ['as' => 'x.catalog.admin.users.konversi_tambah','uses'=>'AdminUsersCatalogController@vendorCreate']);
	Route::post('catalog/admin/users/vendor/create', ['as' => 'x.catalog.admin.users.konversi_tambah','uses'=>'AdminUsersCatalogController@vendorStore']);

	Route::get('catalog/admin/users/vendor_edit/{id}', 				['as' => 'x.catalog.admin.users.vendor_edit','uses'				=>'AdminUsersCatalogController@vendor_edit']);
	Route::put('catalog/admin/users/vendor_edit_simpan/{id}', ['as' => 'x.catalog.admin.users.vendor_edit_simpan','uses'=>'AdminUsersCatalogController@vendor_edit_simpan']);
	Route::delete('catalog/admin/users/vendor_delete/{id}', 	['as' => 'x.catalog.admin.users.vendor_delete','uses'	=>'AdminUsersCatalogController@vendor_delete']);

});
#DirecturCatalog
Route::group(['namespace' => 'Catalog\directur','middleware' => 'directurcatalogauth'], function () {
	Route::post('catalog/users/directur/action_ganti_password', 'DirecturCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/users/directur/reset', 'DirecturCatalogController@reset')->name('reset');
	Route::get('catalog/users/directur/cart', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@cart']);
	Route::post('catalog/users/directur/detail_order', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@detail_order']);
	Route::post('catalog/users/directur/detail_bapb', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@detail_bapb']);
	Route::get('catalog/users/directur/order_waiting', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@order_waiting']);
	Route::post('catalog/users/directur/approvement', ['as'=>'catalog.index','uses'=>'DirecturCatalogController@approvement']);
	Route::post('catalog/users/directur/revisi', ['as'=>'catalog.index','uses'=>'DirecturCatalogController@approvement']);
	Route::get('catalog/users/directur/order_approved', ['as'=>'catalog.index','uses'=>'DirecturCatalogController@order_approved']);
	Route::get('catalog/users/directur/track', ['as'=>'x.catalog.users.order','uses'=>'DirecturCatalogController@track']);
	Route::post('catalog/users/directur/track_order', ['uses'=>'DirecturCatalogController@track_order']);
	Route::get('catalog/users/directur/history', ['as'=>'x.catalog.users.order','uses'=>'DirecturCatalogController@history']);
	Route::post('catalog/users/directur/detail_history', ['as'=>'x.catalog.users.order','uses'=>'DirecturCatalogController@detail_history']);
	Route::get('catalog/users/directur/budget', ['uses'=>'DirecturCatalogController@budget']);
	Route::get('catalog/users/directur/budget/detail', ['uses'=>'DirecturCatalogController@budgetDetail']);
	Route::get('catalog/users/directur/proposer/download/{id_proposer?}', ['uses'=>'DirecturCatalogController@downloadProposer']);
	Route::get('catalog/users/directur/po/download/{id?}', ['uses'=>'DirecturCatalogController@downloadPo']);
	Route::get('catalog/users/directur/ppk_review', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@ppk_review']);
	Route::post('catalog/users/directur/bapb/checked/{id?}', ['uses'=>'DirecturCatalogController@bapbChecked']);
	Route::post('catalog/users/directur/detail_bast', ['as'=>'catalog.cart','uses'=>'DirecturCatalogController@detail_bast']);
	Route::get('catalog/users/directur/bapb/preview/{id?}', ['uses'=>'DirecturCatalogController@bapbPreview']);
	Route::get('catalog/users/directur/bast/preview/{id?}', ['uses'=>'DirecturCatalogController@bastPreview']);
	Route::get('catalog/users/directur/invoice/create/{id?}/{id_bast}', ['uses'=>'DirecturCatalogController@createInvoice']);
	Route::get('catalog/users/directur/bap/create/{id?}/{id_bast?}', ['uses'=>'DirecturCatalogController@createBAP']);
	Route::post('catalog/users/directur/bap/create/{id?}/{id_bast?}', ['uses'=>'DirecturCatalogController@storeBAP']);
	#####BAST
	Route::get('catalog/users/directur/bast/create/{id?}', ['uses'=>'DirecturCatalogController@createBAST']);
	Route::post('catalog/users/directur/bast/create/{id?}', ['uses'=>'DirecturCatalogController@storeBAST']);
	#########
	Route::get('catalog/users/directur/bap/preview/{id?}', ['uses'=>'DirecturCatalogController@bapPreview']);
	Route::get('catalog/users/directur/{id}', ['as'=>'catalog.index','uses'=>'DirecturCatalogController@index']);
});

#ManagerCatalog
Route::group(['namespace' => 'Catalog\manager','middleware' => 'managercatalogauth'], function () {
	Route::post('action_ganti_password', 'ManagerCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('reset', 'ManagerCatalogController@reset')->name('reset');
	Route::get('catalog/users/manager/cart', ['as'=>'catalog.cart','uses'=>'ManagerCatalogController@cart']);
	Route::post('catalog/users/manager/detail_order', ['as'=>'catalog.cart','uses'=>'ManagerCatalogController@detail_order']);
	Route::get('catalog/users/manager/order_waiting', ['as'=>'catalog.cart','uses'=>'ManagerCatalogController@order_waiting']);
	Route::post('catalog/users/manager/approvement', ['as'=>'catalog.index','uses'=>'ManagerCatalogController@approvement']);
	Route::post('catalog/users/manager/revisi', ['as'=>'catalog.index','uses'=>'ManagerCatalogController@approvement']);
	Route::get('catalog/users/manager/order_approved', ['as'=>'catalog.index','uses'=>'ManagerCatalogController@order_approved']);
	Route::get('catalog/users/manager/track', ['as'=>'x.catalog.users.order','uses'=>'ManagerCatalogController@track']);
	Route::post('catalog/users/manager/track_order', ['uses'=>'ManagerCatalogController@track_order']);
	Route::get('catalog/users/manager/history', ['as'=>'x.catalog.users.order','uses'=>'ManagerCatalogController@history']);
	Route::post('catalog/users/manager/detail_history', ['as'=>'x.catalog.users.order','uses'=>'ManagerCatalogController@detail_history']);
	Route::get('catalog/users/manager/budget', ['uses'=>'ManagerCatalogController@budget']);
	Route::get('catalog/users/manager/budget/detail', ['uses'=>'ManagerCatalogController@budgetDetail']);
	Route::get('catalog/users/manager/proposer/download/{id_proposer?}', ['uses'=>'ManagerCatalogController@downloadProposer']);
	Route::get('catalog/users/manager/po/download/{id?}', ['uses'=>'ManagerCatalogController@downloadPo']);
	Route::get('catalog/users/manager/{id}', ['as'=>'catalog.index','uses'=>'ManagerCatalogController@index']);
});

#FinanceCatalog
Route::group(['namespace' => 'Catalog\finance','middleware' => 'financeauth'], function () {
	Route::post('catalog/users/finance/action_ganti_password', 'FinanceCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/users/finance/reset', 'FinanceCatalogController@reset')->name('reset');
	Route::get('catalog/users/finance/cart', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@cart']);
	Route::get('catalog/users/finance/payment_waiting', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@payment_waiting']);
	Route::get('catalog/users/finance/bast_waiting', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@bast_waiting']);
	Route::post('catalog/users/finance/approvement', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@approvement']);
	Route::get('catalog/users/finance/payment_approved', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@payment_approved']);
	Route::get('catalog/users/finance/track', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@track']);
	Route::post('catalog/users/finance/track_order', ['uses'=>'FinanceCatalogController@track_order']);
	Route::post('catalog/users/finance/detail_order', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@detail_order']);
	Route::get('catalog/users/finance/history', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@history']);
	Route::post('catalog/users/finance/detail_history', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@detail_history']);
	Route::get('catalog/users/finance/{id}', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@index']);
	Route::get('catalog/users/finance/invoice/create/{id?}', ['uses'=>'FinanceCatalogController@createInvoice']);
	Route::get('catalog/users/finance/proposer/download/{id_proposer?}', ['uses'=>'FinanceCatalogController@downloadProposer']);
	Route::get('catalog/users/finance/po/download/{no_po?}/{bln?}/{thn?}', ['uses'=>'FinanceCatalogController@downloadPo']);
	Route::post('catalog/users/finance/bast/checked', ['uses'=>'FinanceCatalogController@bastChecked']);
	Route::post('catalog/users/finance/detail_bast', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@detail_bast']);
	Route::get('catalog/users/finance/bast/preview/{id?}', ['uses'=>'FinanceCatalogController@bastPreview']);
});


#FinanceAkutansiCatalog
Route::group(['namespace' => 'Catalog\finance','middleware' => 'financeakutansiauth'], function () {
	Route::post('catalog/users/finance/action_ganti_password', 'FinanceCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/users/finance/reset', 'FinanceCatalogController@reset')->name('reset');

	// Route::get('catalog/users/finance/cart', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@cart']);
	// Route::get('catalog/users/finance/payment_waiting', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@payment_waiting']);


	Route::get('catalog/users/finance/bast_waiting', ['as'=>'catalog.cart','uses'=>'FinanceAkutansiController@bast_waiting']);
	// Route::post('catalog/users/finance/approvement', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@approvement']);
	// Route::get('catalog/users/finance/payment_approved', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@payment_approved']);
	// Route::get('catalog/users/finance/track', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@track']);
	// Route::post('catalog/users/finance/track_order', ['uses'=>'FinanceCatalogController@track_order']);
	// Route::post('catalog/users/finance/detail_order', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@detail_order']);
	// Route::post('catalog/users/finance/detail_bast', ['as'=>'catalog.cart','uses'=>'FinanceCatalogController@detail_bast']);
	// Route::get('catalog/users/finance/history', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@history']);
	// Route::post('catalog/users/finance/detail_history', ['as'=>'x.catalog.users.order','uses'=>'FinanceCatalogController@detail_history']);
	// Route::get('catalog/users/finance/{id}', ['as'=>'catalog.index','uses'=>'FinanceCatalogController@index']);
	// Route::get('catalog/users/finance/invoice/create/{id?}', ['uses'=>'FinanceCatalogController@createInvoice']);
	// Route::get('catalog/users/finance/proposer/download/{id_proposer?}', ['uses'=>'FinanceCatalogController@downloadProposer']);
	// Route::get('catalog/users/finance/po/download/{no_po?}/{bln?}/{thn?}', ['uses'=>'FinanceCatalogController@downloadPo']);
	// Route::post('catalog/users/finance/bast/checked', ['uses'=>'FinanceCatalogController@bastChecked']);

});

#SupervisiCatalog
Route::group(['namespace' => 'Catalog\supervisi','middleware' => 'supervisicatalogauth'], function () {
	Route::post('catalog/users/supervisi/action_ganti_password', 'SupervisiCatalogController@action_ganti_password')->name('action_ganti_password');
	Route::get('catalog/users/supervisi/reset', 'SupervisiCatalogController@reset')->name('reset');
	Route::get('catalog/users/supervisi/item', ['uses'=>'SupervisiCatalogController@item']);
	Route::post('catalog/users/supervisi/addcart', ['uses'=>'SupervisiCatalogController@addcart']);
	Route::post('catalog/users/supervisi/order', ['uses'=>'SupervisiCatalogController@order']);
	Route::get('catalog/users/supervisi/cart', ['as'=>'catalog.cart','uses'=>'SupervisiCatalogController@cart']);
	Route::post('catalog/users/supervisi/delete_cart', ['uses'=>'SupervisiCatalogController@delete_cart']);
	Route::post('catalog/users/supervisi/detail_order', ['as'=>'catalog.cart','uses'=>'SupervisiCatalogController@detail_order']);
	Route::get('catalog/users/supervisi/order_waiting', ['as'=>'catalog.cart','uses'=>'SupervisiCatalogController@order_waiting']);
  Route::post('catalog/users/supervisi/approvement', ['as'=>'catalog.index','uses'=>'SupervisiCatalogController@approvement']);
  Route::get('catalog/users/supervisi/order_approved', ['as'=>'catalog.index','uses'=>'SupervisiCatalogController@order_approved']);
  Route::get('catalog/users/supervisi/track', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@track']);
	Route::post('catalog/users/supervisi/track_order', ['uses'=>'SupervisiCatalogController@track_order']);
  Route::get('catalog/users/supervisi/history', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@history']);
  Route::post('catalog/users/supervisi/detail_history', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@detail_history']);
	Route::get('catalog/users/supervisi/proposer/download/{id_proposer?}', ['uses'=>'SupervisiCatalogController@downloadProposer']);
	Route::get('catalog/users/supervisi/po/download/{id?}', ['uses'=>'SupervisiCatalogController@downloadPo']);
	Route::get('catalog/users/supervisi/budget', ['uses'=>'SupervisiCatalogController@budget']);
	Route::get('catalog/users/supervisi/budget/create', ['uses'=>'SupervisiCatalogController@budgetCreate']);
	Route::post('catalog/users/supervisi/budget/save', ['uses'=>'SupervisiCatalogController@budgetSave']);
	Route::get('catalog/users/supervisi/budget/detail', ['uses'=>'SupervisiCatalogController@budgetDetail']);
	//user_staff
	 Route::get('catalog/users/supervisi/user', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user']);
	 Route::get('catalog/users/supervisi/user_tambah', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_tambah']);
	 Route::get('catalog/users/supervisi/user_edit/{id}', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_edit']);
	 Route::put('catalog/users/supervisi/user_edit_simpan/{id}', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_edit_simpan']);
	 Route::delete('catalog/users/supervisi/user_delete/{id}', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_delete']);
	 Route::post('catalog/users/supervisi/user_simpan', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_simpan']);
	 Route::post('catalog/users/user_kategory', ['as'=>'x.catalog.users.order','uses'=>'SupervisiCatalogController@user_kategory']);
	 Route::get('catalog/users/supervisi/{id}', ['as'=>'catalog.index','uses'=>'SupervisiCatalogController@index']);
});

#DivisiCatalog
Route::group(['namespace' => 'Catalog\user','middleware' => 'divisiauth'], function () {
		Route::post('catalog/users/action_ganti_password', 'DivisiCatalogController@action_ganti_password')->name('action_ganti_password');
		Route::get('catalog/users/reset', 'DivisiCatalogController@reset')->name('reset');
    Route::get('catalog/users/', ['as'=>'x.catalog.users','uses'=>'DivisiCatalogController@dashboard']);
    Route::get('catalog/users/item', ['as'=>'x.catalog.users.item','uses'=>'DivisiCatalogController@index']);
    Route::post('catalog/users/addcart', ['uses'=>'DivisiCatalogController@addcart']);
    Route::post('catalog/users/delete_cart', ['uses'=>'DivisiCatalogController@delete_cart']);
    Route::get('catalog/users/cart', ['as'=>'x.catalog.users.cart','uses'=>'DivisiCatalogController@cart']);
    Route::post('catalog/users/order', ['uses'=>'DivisiCatalogController@order']);
		Route::post('catalog/users/detail_order', ['as'=>'catalog.cart','uses'=>'DivisiCatalogController@detail_order']);
		Route::get('catalog/users/history', ['as'=>'x.catalog.users.history','uses'=>'DivisiCatalogController@history']);
		Route::get('catalog/users/order_approved', ['as'=>'catalog.index.order_approved','uses'=>'DivisiCatalogController@order_approved']);
		Route::post('catalog/users/detail_history', ['uses'=>'DivisiCatalogController@detail_history']);
		Route::get('catalog/users/track', ['as'=>'x.catalog.users.track','uses'=>'DivisiCatalogController@track']);
		Route::post('catalog/users/track_order', ['uses'=>'DivisiCatalogController@track_order']);
		Route::get('catalog/users/request/{id?}', ['uses'=>'DivisiCatalogController@request']);
		Route::post('catalog/users/request/{id?}', ['uses'=>'DivisiCatalogController@requestPost']);
		Route::get('catalog/users/request/preview/{id?}', ['uses'=>'DivisiCatalogController@requestPreview']);
		Route::post('catalog/users/chat', ['as'=>'x.catalog.users.chat','uses'=>'DivisiCatalogController@chat']);
		Route::post('catalog/users/Addchat', ['as'=>'x.catalog.users.chat','uses'=>'DivisiCatalogController@Addchat']);
		Route::get('catalog/users/proposer/download/{id_proposer?}', ['uses'=>'DivisiCatalogController@downloadProposer']);
		Route::get('catalog/users/po/download/{id?}', ['uses'=>'DivisiCatalogController@downloadPo']);
		Route::get('/wa', ['uses'=>'DivisiCatalogController@wa']);
});

########################################################VMS#####################################################
#CheckerVMS
Route::group(['namespace' => 'Vms\checker','middleware' => 'checkervmsauth'], function () {
    Route::post('vms/checker/update_status_user_vendor', ['as'=>'vendor.index','uses'=>'CheckerVmsController@update_status_user_vendor']);
    Route::post('vms/checker/update_status_data_user_vendor', ['as'=>'vendor.index','uses'=>'CheckerVmsController@update_status_data_user_vendor']);
    Route::post('vms/checker/detail_vendor', ['as'=>'vendor.index','uses'=>'CheckerVmsController@detail_vendor']);
    Route::post('vms/checker/document_vendor', ['as'=>'vendor.index','uses'=>'CheckerVmsController@document_vendor']);
    Route::post('vms/checker/status_vendor', ['as'=>'vendor.index','uses'=>'CheckerVmsController@status_vendor']);
    Route::post('vms/checker/send_code_verifikasi', ['as'=>'vendor.index','uses'=>'CheckerVmsController@send_code_verifikasi']);
    Route::post('vms/checker/checklist', ['as'=>'vendor.index','uses'=>'CheckerVmsController@checklist']);
    Route::post('vms/checker/invitation', ['as'=>'vendor.index','uses'=>'CheckerVmsController@invitation']);
    Route::post('vms/checker/vendorPassRealData', ['as'=>'vendor.index','uses'=>'CheckerVmsController@vendorPassRealData']);
		Route::get('vms/checker/{id}', ['as'=>'vendor.index','uses'=>'CheckerVmsController@index']);
});

#ActivatorVMS
Route::group(['namespace' => 'Vms\activator','middleware' => 'activatorvmsauth'], function () {
    Route::get('vms/activator/{id}', ['as'=>'vendor.index','uses'=>'ActivatorVmsController@index']);
    Route::post('vms/activator/detail_vendor', ['as'=>'vendor.index','uses'=>'ActivatorVmsController@detail_vendor']);
    Route::post('vms/activator/status_vendor', ['as'=>'vendor.index','uses'=>'ActivatorVmsController@status_vendor']);
});



#vcs
Route::group(['namespace' => 'Vms\users','middleware' => 'vcsauth'], function () {
		Route::get('vcs/users/product', ['as'=>'vendor.product','uses'=>'VcsController@product']);
		Route::get('vcs/users/perubahan_data', ['uses'=>'VcsController@data_changes']);
		Route::post('vcs/users/perubahan_data', ['uses'=>'VcsController@data_changes_save']);
		Route::post('vcs/users/update', ['as'=>'vendor.index','uses'=>'VcsController@update']);
		Route::post('vcs/users/update_izin_usaha', ['as'=>'vendor.index','uses'=>'VcsController@update_izin_usaha']);
		Route::post('vcs/users/delete_izin_usaha', ['as'=>'vendor.index','uses'=>'VcsController@delete_izin_usaha']);
		Route::post('vcs/users/tambahan_izin_usaha', ['as'=>'vendor.index','uses'=>'VcsController@tambahan_izin_usaha']);
		Route::post('vcs/users/delete_akta', ['as'=>'vendor.index','uses'=>'VcsController@delete_akta']);
		Route::post('vcs/users/delete_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VcsController@delete_akta_pembaruan']);
		Route::post('vcs/users/update_akta', ['as'=>'vendor.index','uses'=>'VcsController@update_akta']);
		Route::post('vcs/users/create_akta', ['as'=>'vendor.index','uses'=>'VcsController@create_akta']);
		Route::post('vcs/users/update_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VcsController@update_akta_pembaruan']);
		Route::post('vcs/users/create_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VcsController@create_akta_pembaruan']);
		Route::post('vcs/users/document_upload', ['as'=>'vendor.index','uses'=>'VcsController@akta_upload']);
		Route::get('vcs/users/dokumen_delete/{id}/{menu}', ['as'=>'vendor.index','uses'=>'VcsController@dokumen_delete']);
		Route::post('vcs/users/document_vendor', ['as'=>'vendor.index','uses'=>'VcsController@document_vendor']);
		Route::post('vcs/users/update_pemilik', ['as'=>'vendor.index','uses'=>'VcsController@update_pemilik']);
		Route::post('vcs/users/delete_pemilik', ['as'=>'vendor.index','uses'=>'VcsController@delete_pemilik']);
		Route::post('vcs/users/create_pemilik', ['as'=>'vendor.index','uses'=>'VcsController@create_pemilik']);
		Route::post('vcs/users/update_pengurus', ['as'=>'vendor.index','uses'=>'VcsController@update_pengurus']);
		Route::post('vcs/users/create_pengurus', ['as'=>'vendor.index','uses'=>'VcsController@create_pengurus']);
		Route::post('vcs/users/delete_pengurus', ['as'=>'vendor.index','uses'=>'VcsController@delete_pengurus']);
		Route::post('vcs/users/update_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VcsController@update_tenaga_ahli']);
		Route::post('vcs/users/create_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VcsController@create_tenaga_ahli']);
		Route::post('vcs/users/delete_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VcsController@delete_tenaga_ahli']);
		Route::post('vcs/users/create_tools', ['as'=>'vendor.index','uses'=>'VcsController@create_tools']);
		Route::post('vcs/users/update_tools', ['as'=>'vendor.index','uses'=>'VcsController@update_tools']);
		Route::post('vcs/users/delete_tools', ['as'=>'vendor.index','uses'=>'VcsController@delete_tools']);
		Route::post('vcs/users/create_experiences', ['as'=>'vendor.index','uses'=>'VcsController@create_experiences']);
		Route::post('vcs/users/update_experiences', ['as'=>'vendor.index','uses'=>'VcsController@update_experiences']);
		Route::post('vcs/users/delete_experiences', ['as'=>'vendor.index','uses'=>'VcsController@delete_experiences']);
		Route::post('vcs/users/create_tax', ['as'=>'vendor.index','uses'=>'VcsController@create_tax']);
		Route::post('vcs/users/delete_tax', ['as'=>'vendor.index','uses'=>'VcsController@delete_tax']);
		Route::get('vcs/users/request_order', ['as'=>'vendor.request_order','uses'=>'VcsController@request_order']);
		Route::get('vcs/users/product', ['as'=>'vendor.product','uses'=>'VcsController@product']);
		Route::post('vcs/users/product/add', ['uses'=>'VcsController@productAdd']);
		Route::get('vcs/users/product/edit/{id?}', ['uses'=>'VcsController@productEdit']);
		Route::post('vcs/users/product/update/{id?}', ['uses'=>'VcsController@productUpdate']);
		Route::post('vcs/users/product/spec/add', ['uses'=>'VcsController@productSpecAdd']);
		Route::post('vcs/users/product/spec/update', ['uses'=>'VcsController@productSpecUpdate']);
		Route::post('vcs/users/product/spec/general/update', ['uses'=>'VcsController@productSpecGeneralUpdate']);
		Route::get('vcs/users/product/spec/{id?}', ['uses'=>'VcsController@productSpec']);
		Route::get('vcs/users/product/image/{id?}', ['uses'=>'VcsController@productImage']);
		Route::post('vcs/users/product/image/save/{id?}', ['uses'=>'VcsController@productImageSave']);
		Route::get('vcs/users/product/image/delete/{id}', ['uses'=>'VcsController@DeleteImage']);
		Route::get('vcs/users/product/doc/{id?}', ['uses'=>'VcsController@doc']);
		Route::post('vcs/users/product/doc/save/{id?}', ['uses'=>'VcsController@DocSave']);
		Route::get('vcs/users/product/doc/delete/{id}', ['uses'=>'VcsController@DeleteDoc']);
		Route::post('vcs/users/product/doc/status/update', ['uses'=>'VcsController@status']);
		Route::get('vcs/users/product/price/{id}', ['uses'=>'VcsController@historyPrice']);
		Route::get('vcs/users/contract', ['uses'=>'VcsController@contractIndex']);
		Route::get('vcs/users/contract/create', ['uses'=>'VcsController@contractCreate']);
		Route::post('vcs/users/contract/create', ['uses'=>'VcsController@contractStore']);
		Route::get('vcs/users/adendum/create', ['uses'=>'VcsController@adendumCreate']);
		Route::post('vcs/users/adendum/create', ['uses'=>'VcsController@adendumStore']);
		Route::get('vcs/users/contract/adendum/{id?}', ['uses'=>'VcsController@adendumIndex']);
		// delivery order
		Route::post('vcs/users/detailItem', ['uses'=>'VcsController@detailItem']);
		Route::get('vcs/users/delivery_order/preview/{id?}', ['uses'=>'VcsController@preview']);
		Route::post('vcs/users/submitDo', ['uses'=>'VcsController@submitDo']);
		Route::get('vcs/users/delivery_order/{id}', ['uses'=>'VcsController@deliveryOrder']);
		Route::get('vcs/users/delivery_order/resend/{id}', ['uses'=>'VcsController@deliveryOrderResendReject']);
		Route::post('vcs/users/detailItemReject', ['uses'=>'VcsController@detailItemReject']);
		Route::post('vcs/users/upload/faktur/{id?}', ['uses'=>'VcsController@uploadFaktur']);
		//track item
		Route::get('vcs/users/track_item', ['uses'=>'VcsController@trackItem']);
		Route::get('vcs/users/percakapan', ['as'=>'vendor.chatting','uses'=>'VcsController@chatting']);
		Route::get('vcs/users/percakapan_detail/{id_contract}/{id_user}', ['as'=>'vendor.chatting','uses'=>'VcsController@chatting_detail']);
		Route::post('vcs/users/Addchat', ['as'=>'vendor.chatting','uses'=>'VcsController@Addchat']);
		Route::get('vcs/users/proposer/download/{id_proposer?}', ['uses'=>'VcsController@downloadProposer']);
		Route::get('vcs/users/po/download/{id?}', ['uses'=>'VcsController@downloadPo']);
		Route::get('vcs/users/invoice/create/{id?}/{id_bast?}', ['uses'=>'VcsController@createInvoice']);
		Route::get('vcs/users/dashboard/test1', function(){ return view('vcs.users.test.index1'); })->name('test1');
		Route::get('vcs/users/dashboard/test2', function(){ return view('vcs.users.test.index2'); })->name('test2');
		Route::get('vcs/users/{id}', ['as'=>'vendor.index','uses'=>'VcsController@index']);
		Route::get('vcs/users/bast/preview/{id?}', ['uses'=>'VcsController@bastPreview']);
});

#vendor
Route::group(['namespace' => 'Vms\users','middleware' => 'vendorauth'], function () {
    Route::get('vms/users/perubahan_data', ['uses'=>'VmsController@data_changes']);
    Route::post('vms/users/perubahan_data', ['uses'=>'VmsController@data_changes_save']);
    Route::post('vms/users/update', ['as'=>'vendor.index','uses'=>'VmsController@update']);
    Route::post('vms/users/update_izin_usaha', ['as'=>'vendor.index','uses'=>'VmsController@update_izin_usaha']);
    Route::post('vms/users/delete_izin_usaha', ['as'=>'vendor.index','uses'=>'VmsController@delete_izin_usaha']);
    Route::post('vms/users/tambahan_izin_usaha', ['as'=>'vendor.index','uses'=>'VmsController@tambahan_izin_usaha']);
    Route::post('vms/users/delete_akta', ['as'=>'vendor.index','uses'=>'VmsController@delete_akta']);
    Route::post('vms/users/delete_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VmsController@delete_akta_pembaruan']);
    Route::post('vms/users/update_akta', ['as'=>'vendor.index','uses'=>'VmsController@update_akta']);
    Route::post('vms/users/create_akta', ['as'=>'vendor.index','uses'=>'VmsController@create_akta']);
    Route::post('vms/users/update_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VmsController@update_akta_pembaruan']);
    Route::post('vms/users/create_akta_pembaruan', ['as'=>'vendor.index','uses'=>'VmsController@create_akta_pembaruan']);
    Route::post('vms/users/document_upload', ['as'=>'vendor.index','uses'=>'VmsController@akta_upload']);
    Route::get('vms/users/dokumen_delete/{id}/{menu}', ['as'=>'vendor.index','uses'=>'VmsController@dokumen_delete']);
    Route::post('vms/users/document_vendor', ['as'=>'vendor.index','uses'=>'VmsController@document_vendor']);
    Route::post('vms/users/update_pemilik', ['as'=>'vendor.index','uses'=>'VmsController@update_pemilik']);
    Route::post('vms/users/delete_pemilik', ['as'=>'vendor.index','uses'=>'VmsController@delete_pemilik']);
    Route::post('vms/users/create_pemilik', ['as'=>'vendor.index','uses'=>'VmsController@create_pemilik']);
    Route::post('vms/users/update_pengurus', ['as'=>'vendor.index','uses'=>'VmsController@update_pengurus']);
    Route::post('vms/users/create_pengurus', ['as'=>'vendor.index','uses'=>'VmsController@create_pengurus']);
    Route::post('vms/users/delete_pengurus', ['as'=>'vendor.index','uses'=>'VmsController@delete_pengurus']);
    Route::post('vms/users/update_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VmsController@update_tenaga_ahli']);
    Route::post('vms/users/create_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VmsController@create_tenaga_ahli']);
    Route::post('vms/users/delete_tenaga_ahli', ['as'=>'vendor.index','uses'=>'VmsController@delete_tenaga_ahli']);
    Route::post('vms/users/create_tools', ['as'=>'vendor.index','uses'=>'VmsController@create_tools']);
    Route::post('vms/users/update_tools', ['as'=>'vendor.index','uses'=>'VmsController@update_tools']);
    Route::post('vms/users/delete_tools', ['as'=>'vendor.index','uses'=>'VmsController@delete_tools']);
    Route::post('vms/users/create_experiences', ['as'=>'vendor.index','uses'=>'VmsController@create_experiences']);
    Route::post('vms/users/update_experiences', ['as'=>'vendor.index','uses'=>'VmsController@update_experiences']);
    Route::post('vms/users/delete_experiences', ['as'=>'vendor.index','uses'=>'VmsController@delete_experiences']);
    Route::post('vms/users/create_tax', ['as'=>'vendor.index','uses'=>'VmsController@create_tax']);
    Route::post('vms/users/delete_tax', ['as'=>'vendor.index','uses'=>'VmsController@delete_tax']);
		Route::get('vms/users/request_order', ['as'=>'vendor.request_order','uses'=>'VmsController@request_order']);
		Route::get('vms/users/product', ['as'=>'vendor.product','uses'=>'VmsController@product']);
		Route::post('vms/users/product/add', ['uses'=>'VmsController@productAdd']);
		Route::get('vms/users/product/edit/{id?}', ['uses'=>'VmsController@productEdit']);
		Route::post('vms/users/product/update/{id?}', ['uses'=>'VmsController@productUpdate']);
		Route::post('vms/users/product/spec/add', ['uses'=>'VmsController@productSpecAdd']);
		Route::post('vms/users/product/spec/update', ['uses'=>'VmsController@productSpecUpdate']);
		Route::post('vms/users/product/spec/general/update', ['uses'=>'VmsController@productSpecGeneralUpdate']);
		Route::get('vms/users/product/spec/{id?}', ['uses'=>'VmsController@productSpec']);
		Route::get('vms/users/product/image/{id?}', ['uses'=>'VmsController@productImage']);
		Route::post('vms/users/product/image/save/{id?}', ['uses'=>'VmsController@productImageSave']);
		Route::get('vms/users/product/image/delete/{id}', ['uses'=>'VmsController@DeleteImage']);
		Route::get('vms/users/product/doc/{id?}', ['uses'=>'VmsController@doc']);
		Route::post('vms/users/product/doc/save/{id?}', ['uses'=>'VmsController@DocSave']);
		Route::get('vms/users/product/doc/delete/{id}', ['uses'=>'VmsController@DeleteDoc']);
		Route::post('vms/users/product/doc/status/update', ['uses'=>'VmsController@status']);
		Route::get('vms/users/product/price/{id}', ['uses'=>'VmsController@historyPrice']);
		Route::get('vms/users/contract', ['uses'=>'VmsController@contractIndex']);
		Route::get('vms/users/contract/create', ['uses'=>'VmsController@contractCreate']);
		Route::post('vms/users/contract/create', ['uses'=>'VmsController@contractStore']);
		// delivery order
		Route::post('vms/users/detailItem', ['uses'=>'VmsController@detailItem']);
		Route::get('vms/users/delivery_order/preview/{id?}', ['uses'=>'VmsController@preview']);
		Route::post('vms/users/submitDo', ['uses'=>'VmsController@submitDo']);
		Route::get('vms/users/delivery_order/{id}', ['uses'=>'VmsController@deliveryOrder']);
		Route::get('vms/users/delivery_order/resend/{id}', ['uses'=>'VmsController@deliveryOrderResendReject']);
		Route::post('vms/users/detailItemReject', ['uses'=>'VmsController@detailItemReject']);
		//track item
		Route::get('vms/users/track_item', ['uses'=>'VmsController@trackItem']);
		Route::get('vms/users/percakapan', ['as'=>'vendor.chatting','uses'=>'VmsController@chatting']);
		Route::get('vms/users/percakapan_detail/{id_cart}/{id_user}/{id_item}', ['as'=>'vendor.chatting','uses'=>'VmsController@chatting_detail']);
		Route::post('vms/users/Addchat', ['as'=>'vendor.chatting','uses'=>'VmsController@Addchat']);
		Route::get('vms/users/proposer/download/{id_proposer?}', ['uses'=>'VmsController@downloadProposer']);
		Route::get('vms/users/po/download/{no_po?}/{bln?}/{thn?}', ['uses'=>'VmsController@downloadPo']);
		Route::get('vms/users/invoice/create/{id?}', ['uses'=>'VmsController@createInvoice']);
		Route::get('vms/users/dashboard/test1', function(){ return view('vms.users.test.index1'); })->name('test1');
		Route::get('vms/users/dashboard/test2', function(){ return view('vms.users.test.index2'); })->name('test2');
		Route::get('vms/users/{id}', ['as'=>'vendor.index','uses'=>'VmsController@index']);
});


Route::get('/product', function () { return view('guest.product.index'); })->name('x.product');
Route::get('/proposal', function () { return view('guest.proposal.index'); })->name('x.proposal');
Route::get('/proposal/report', function () { return view('guest.proposal.report'); })->name('x.proposal.report');

Route::get('/review/specification', function () { return view('guest.review.specification'); })->name('x.review.specification');
Route::get('/review/volume', function () { return view('guest.review.volume'); })->name('x.review.volume');
Route::get('/review/price', function () { return view('guest.review.price'); })->name('x.review.price');

############VENDOR REGIS
Route::get('vendor/register', 'Vms\guest\GuestVmsController@vendorRegis');
Route::post('vendor/register', 'Vms\guest\GuestVmsController@vendorRegisStore');
############VENDOR REGIS

Route::get('/clear', function () {
  Artisan::call('cache:clear');
  Artisan::call('config:clear');
  Artisan::call('view:clear');
  return view('welcome');
});

Route::get('/build', function () {
  Artisan::call('migrate');
  Artisan::call('db:seed');
  return view('welcome');
});
