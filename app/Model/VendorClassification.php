<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorClassification extends Model
{
  	protected $table = 'vendor_classification';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_vendor_detail', 'id_classification', 'sub_classification', 'qualification', 'no_letter', 'expired_date', 'status', 'created_at', 'updated_at'];
}
