<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  	protected $table = 'item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'code', 'name', 'merk', 'item_from', 'price_country', 'price_retail', 'price_gov', 'price_shipment', 'price_date', 'release_date', 'expired_date', 'stock', 'stock_min', 'production_origin', 'description', 'status', 'satuan', 'created_at', 'updated_at', 'id_contract', 'id_adendum', 'id_vendor_detail'];
}
