<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDetailBranch extends Model
{
  	protected $table = 'vendor_detail_branch';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_detail', 'address', 'telephone', 'fax', 'mobile_phone'];

}
