<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class CategorySpec extends Model
{
  	protected $table = 'category_spec';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'name', 'id_category', 'status', 'created_at', 'created_by', 'updated_at'];

}
