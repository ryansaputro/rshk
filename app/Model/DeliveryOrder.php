<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class DeliveryOrder extends Model
{
  	protected $table = 'delivery_order';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'no_do', 'datetime', 'status', 'sent_by', 'driver', 'car', 'car_no', 'id_gudang', 'faktur', 'created_at', 'updated_at'];
}
