<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterDataWarehouse extends Model
{
  	protected $table = 'master_data_gudang';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_gudang', 'id_item', 'qty', 'satuan', 'last_date_in', 'last_date_out', 'minim_stock', 'id_user', 'created_at', 'updated_at', 'additional'];

    public function updateData($update) {
      $data = DB::unprepared($update);
      return $data;
    }

    public function inputData($insert) {
      $data = DB::unprepared('INSERT INTO "master_data_gudang"("id_gudang", "id_item", "qty", "satuan", "last_date_in", "minim_stock", "id_user") VALUES '.$insert);
      return $data;
    }

}
