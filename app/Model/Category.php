<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
  	protected $table = 'category';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'name', 'id_parent', 'status', 'created_at', 'updated_at'];

    public function index(){
    	$data = DB::table('category')->select("category.id as category_id",'category.name','category.id_parent as parent_id','category.status','item.*')->Join('category_item','category.id','=','category_item.id_category')->Join('item','category_item.id_item','=','item.id');
    	return $data;
    }

    public function categoryWithSameParent(){
    	$data = DB::table('category as a')->select("a.id",'a.name','a.id_parent','a.status',"b.id as b_id",'b.name as b_name','b.id_parent as b_id_parent','b.status as b_status')->LeftJoin('category as b','a.id','=','b.id_parent');
    	return $data;
    }

}
