<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ItemSpec extends Model
{
  	protected $table = 'spec_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'description', 'id_category_spec', 'id_item', 'created_at', 'updated_at'];

    public function ItemSpec($insert) {
      $data = DB::unprepared('INSERT INTO `spec_item`(`description`, `id_category_spec`, `id_item`, `created_at`, `updated_at`) VALUES ' .$insert);
      return $data;
    }

    public function ItemSpecUpdate($update) {
      $data = DB::unprepared($update);
      return $data;
    }
}
