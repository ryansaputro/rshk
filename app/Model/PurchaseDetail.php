<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class PurchaseDetail extends Model
{
  	protected $table = 'purchase_detail';
    protected $primaryKey = 'id_purchase_detail';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id_item', 'qty', 'status', 'id_purchase_detail', 'id_purchase'];

    public function InsertCart($insert) {
    	$data = DB::unprepared("INSERT INTO `purchase_detail`(`id_purchase`, `id_item`, `qty`, `status`) VALUES ". $insert);
    	return $data;
    }
}
