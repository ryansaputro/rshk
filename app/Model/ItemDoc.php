<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ItemDoc extends Model
{
  	protected $table = 'doc_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'name', 'file', 'description', 'id_item', 'status', 'created_at', 'updated_at'];
}
