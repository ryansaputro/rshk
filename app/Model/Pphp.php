<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pphp extends Model
{
  	protected $table = 'user_catalog_pphp';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_user', 'kode_instalasi', 'nama_instalasi', 'nama', 'posisi', 'id_master_jenis_gudang'];
}
