<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDirectur extends Model
{
  	protected $table = 'vendor_directur';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_vendor_detail', 'name', 'email', 'mobile_phone', 'nik', 'created_at', 'updated_at'];


    public function SaveOwner($insertX) {
    	$data = DB::unprepared("INSERT INTO 'vendor_owner'('id_vendor_detail', 'name_vendor_owner', 'ktp_vendor_owner', 'address_vendor_owner') VALUES  ".$insertX);
    	return $data;
    }

}
