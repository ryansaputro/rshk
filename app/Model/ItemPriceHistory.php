<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemPriceHistory extends Model
{
  	protected $table = 'price_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_item', 'price_retail', 'price_gov', 'price_date', 'created_at', 'updated_at'];
}
