<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReceiveItemDetail extends Model
{
  	protected $table = 'receive_item_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_receive_item', 'id_item', 'id_qty', 'satuan', 'status', 'description'];
}
