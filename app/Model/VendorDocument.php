<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDocument extends Model
{
  	protected $table = 'vendor_document';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_vendor_detail', 'file', 'status', 'id_data', 'table', 'created_at', 'updated_at'];


}
