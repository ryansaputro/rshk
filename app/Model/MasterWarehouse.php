<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterWarehouse extends Model
{
  	protected $table = 'master_gudang';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_jenis_gudang', 'kode_instalasi', 'nama_instalasi'];
}
