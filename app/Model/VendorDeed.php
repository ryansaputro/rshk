<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDeed extends Model
{
  	protected $table = 'vendor_deed';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_detail', 'deed_number', 'date', 'notary_public'];

}
