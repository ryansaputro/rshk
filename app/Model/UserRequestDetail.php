<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserRequestDetail extends Model
{
  	protected $table = 'user_request_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_user_request', 'id_item', 'qty', 'satuan', 'status'];
}
