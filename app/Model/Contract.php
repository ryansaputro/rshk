<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Contract extends Model
{
  	protected $table = 'contract';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'no_contract', 'id_vendor_detail', 'start_date', 'end_date', 'status', 'description', 'data', 'created_at', 'updated_at'];
}
