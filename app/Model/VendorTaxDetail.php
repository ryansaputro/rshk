<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorTaxDetail extends Model
{
  	protected $table = 'vendor_tax_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [ 'id', 'id_vendor_tax', 'evidence_tax', 'evidence_body_tax', 'status'];

}