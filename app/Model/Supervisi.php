<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Supervisi extends Model
{
  	protected $table = 'user_catalog_supervisi';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_user_manager', 'id_user', 'nip', 'name', 'email', 'mobile_phone', 'status', 'created_at', 'updated_at'];
}
