<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorOwner extends Model
{
  	protected $table = 'vendor_owner';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_detail', 'name_vendor_owner', 'ktp_vendor_owner', 'address_vendor_owner'];


    public function SaveOwner($insertX) {
    	$data = DB::unprepared("INSERT INTO `vendor_owner`(`id_vendor_detail`, `name_vendor_owner`, `ktp_vendor_owner`, `address_vendor_owner`) VALUES  ".$insertX);
    	return $data;
    }

}
