<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorAmandementDeed extends Model
{
  	protected $table = 'vendor_amandement_deed';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_deed', 'deed_number', 'date', 'notary_public'];

}
