<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralSpec extends Model
{
  	protected $table = 'general_spec';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_item', 'weight', 'height', 'volume', 'minimal_buy', 'created_at', 'updated_at'];
}
