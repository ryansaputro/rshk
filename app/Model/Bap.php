<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bap extends Model
{
  	protected $table = 'bap';
    protected $primaryKey = 'id';
    public    $incrementing = true;
    public    $timestamps = true;
    protected $fillable = [ 'id', 'id_contract', 'id_order', 'id_bast', 'id_vendor_detail', 'id_ppk', 'no_bap', 'procurement', 'item_not_send', 'datetime', 'status', 'total', 'must_pay', 'created_at', 'updated_at'];
}
