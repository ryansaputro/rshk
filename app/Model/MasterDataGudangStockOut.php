<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterDataGudangStockOut extends Model
{
  	protected $table = 'master_data_gudang_stock_out';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_gudang', 'id_user_requestor', 'datetime', 'no_stock_out', 'id_user_warehouse', 'created_at', 'updated_at'];
}
