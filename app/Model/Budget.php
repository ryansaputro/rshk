<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Budget extends Model
{
  	protected $table = 'budget';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_supervisi', 'periode', 'approved_by', 'budget', 'status', 'created_at', 'updated_at'];
}
