<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cart extends Model
{
  	protected $table = 'cart';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_item', 'qty', 'id_user', 'id_user_manager', 'id_user_supervisi', 'id_user_finance', 'id_vendor_detail', 'status', 'datetime' ,'created_at', 'updated_at', 'id_contract', 'id_adendum'];

    public function CartItem() {
        $data = DB::table('cart')->select('cart.*','item.name','item.price_retail','item.price_gov','item.price_country')->join('item','cart.id_item','=','item.id');
        return $data;
    }

}
