<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorAdministrators extends Model
{
  	protected $table = 'vendor_administrators';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_detail', 'name_name_vendor_administrators', 'ktp_name_vendor_administrators', 'address_name_vendor_administrators', 'position_name_vendor_administrators'];


    public function SaveAdministrators($insertY) {
	$data = DB::unprepared("INSERT INTO `vendor_administrators`(`id_vendor_detail`, `name_name_vendor_administrators`, `ktp_name_vendor_administrators`, `address_name_vendor_administrators`, `position_name_vendor_administrators`) VALUES ".$insertY);
	return $data;
    }


}
