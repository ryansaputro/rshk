<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BastDetail extends Model
{
  	protected $table = 'bast_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_bast', 'id_item', 'qty', 'satuan', 'status', 'description', 'created_at', 'updated_at'];

}
