<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model
{
  	protected $table = 'order_catalog';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_proposer', 'id_golongan', 'id_category', 'no_po', 'no_medik', 'no_non_medik', 'no_order', 'id_user_buyer', 'id_user_supervisi', 'id_vendor_detail', 'status', 'datetime', 'total', 'faktur', 'faktur_date',  'created_at', 'updated_at'];

}
