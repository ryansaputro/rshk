<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorBusinessLicenseDetail extends Model
{
  	protected $table = 'vendor_business_license_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_business_license', 'id_vendor_detail', 'reference_number', 'giver_agency', 'valid_until', 'start_date', 'lisensi_id'];

    public function SaveDetail($insert) {
    	$data = DB::unprepared("INSERT INTO `vendor_business_license_detail`(`id_vendor_business_license`,`id_vendor_detail`, `reference_number`, `giver_agency`, `valid_until`, `qualification`, `classification`) VALUES ".$insert);
    	return $data;
    }

}
