<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterDataDepo extends Model
{
  	protected $table = 'master_data_depo';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_depo', 'id_item', 'qty', 'satuan', 'last_date_in', 'last_date_out', 'minim_stock', 'id_user', 'created_at', 'updated_at', 'additional'];

    public function updateDataDepo($update1) {
      $data = DB::unprepared($update1);
      return $data;
    }
    //
    public function inputDataDepo($insert1) {
      $data = DB::unprepared("INSERT INTO `master_data_depo`(`id_depo`, `id_item`, `qty`, `satuan`, `last_date_in`, `minim_stock`, `id_user`) VALUES ".$insert);
      return $data;
    }

}
