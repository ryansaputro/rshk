<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnitConversion extends Model
{
  	protected $table = 'unit_conversion';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_unit_first', 'id_unit_second', 'value', 'description', 'status', 'created_at', 'updated_at'];
}
