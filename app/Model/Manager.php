<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Manager extends Model
{
  	protected $table = 'user_catalog_manager';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_user_catalog_company', 'id_user', 'name', 'email', 'mobile_phone', 'status', 'created_at', 'updated_at'];

    public function HirarkiDirectur($id_manager) {
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_manager.id', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'cart.id_user_manager', '=', 'user_catalog_manager.id')
              ->whereIn('cart.id_user_manager', $id_manager);
        return $data;
    }

    public function Hirarki($id_manager) {
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_manager.id', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'cart.id_user_manager', '=', 'user_catalog_manager.id')
              ->where('cart.id_user_manager', $id_manager);
        return $data;
    }

    public function HirarkiSupervisi($id_supervisi) {
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_supervisi.id', 'user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone')
              ->join('user_catalog_supervisi', 'cart.id_user_supervisi', '=', 'user_catalog_supervisi.id')
              ->where('cart.id_user_supervisi', $id_supervisi);
        return $data;
    }

    public function HirarkiFinance($id_finance) {
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_finance.id', 'user_catalog_finance.name', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone')
              ->join('user_catalog_finance', 'cart.id_user_finance', '=', 'user_catalog_finance.id')
              ->where('cart.id_user_finance', $id_finance);
        return $data;
    }
}
