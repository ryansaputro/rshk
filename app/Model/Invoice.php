<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Invoice extends Model
{
  	protected $table = 'invoice';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'no_invoice', 'datetime', 'id_vendor_deailt', 'nominal', 'created_at', 'updated_at', 'status'];

}
