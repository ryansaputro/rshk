<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class DeliveryOrderDetail extends Model
{
  	protected $table = 'delivery_order_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_do', 'id_item', 'qty', 'status', 'satuan', 'description'];
}
