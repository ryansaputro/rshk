<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserRequest extends Model
{
  	protected $table = 'user_request';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'no_request', 'id_order', 'datetime', 'request_type', 'department_requestor', 'id_user', 'id_gudang', 'status', 'created_at', 'updated_at'];
}
