<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorInvitation extends Model
{
  	protected $table = 'vendor_invitation';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_vendor_detail', 'date', 'time', 'place', 'description', 'status', 'created_at', 'updated_at'];

}
