<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BapbDetail extends Model
{
  	protected $table = 'bapb_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_bapb', 'id_item', 'qty', 'satuan', 'status', 'description', 'created_at', 'updated_at'];

}
