<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterDataGudangStockOutDetail extends Model
{
  	protected $table = 'master_data_gudang_stock_out_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_master_data_gudang_stock_out', 'id_item', 'qty', 'satuan', 'price', 'status'];

}
