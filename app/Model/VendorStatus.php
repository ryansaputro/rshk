<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorStatus extends Model
{
  	protected $table = 'vendor_status';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_vendor_detail', 'id_admin_vms', 'created_at', 'updated_at','last_status'];


}
