<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BudgetUsed extends Model
{
  	protected $table = 'budget_used';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_budget', 'budget_used', 'used_by', 'datetime', 'id_proposer', 'id_order', 'created_at', 'updated_at'];
}
