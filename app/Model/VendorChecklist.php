<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorChecklist extends Model
{
  	protected $table = 'vendor_checklist';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'data', 'id_vendor', 'description', 'status', 'created_at', 'updated_at'];

}
