<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Adendum extends Model
{
  	protected $table = 'adendum';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_contract', 'no_adendum', 'id_vendor_detail', 'start_date', 'end_date', 'status', 'description', 'data', 'created_at', 'updated_at'];
}
