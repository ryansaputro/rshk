<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorToolsDetail extends Model
{
  	protected $table = 'vendor_tools_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [ 'id', 'id_vendor_tool', 'tool_name', 'qty', 'capacity', 'type', 'condition', 'additional_maker', 'location', 'evidance', 'description', 'created_at', 'updated_at'];

}