<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Finance extends Model
{
  	protected $table = 'user_catalog_finance';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_user_catalog_company', 'id_user', 'name', 'email', 'mobile_phone', 'status', 'created_at', 'updated_at'];
}
