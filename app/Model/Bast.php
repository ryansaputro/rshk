<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bast extends Model
{
  	protected $table = 'bast';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'id_receive_item', 'no_bast', 'datetime', 'status', 'id_pphp', 'approved_by_id_ppk',  'created_at', 'updated_at'];
}
