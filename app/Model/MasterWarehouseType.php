<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterWarehouseType extends Model
{
  	protected $table = 'master_jenis_gudang';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'jenis_gudang'];
}
