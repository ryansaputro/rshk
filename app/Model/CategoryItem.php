<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryItem extends Model
{
  	protected $table = 'category_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_category', 'id_item', 'created_at', 'updated_at'];
}
