<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderDetail extends Model
{
  	protected $table = 'order_catalog_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'id_item', 'qty', 'status', 'id_user_supervisi_approve', 'datetime_supervisi', 'id_user_manager_approve', 'datetime_manager', 'id_user_directur_approve', 'datetime_directur', 'id_user_finance_approve', 'datetime_finace', 'created_at', 'updated_at'];

    public function inputOrder($item) {
      $data = DB::unprepared("INSERT INTO order_catalog_detail(id_order, id_item, qty, status, created_at) VALUES ".$item);
      return $data;
    }

    public function UpdateOrder($update) {
      $data = DB::unprepared($update);
      return $data;
    }
}
