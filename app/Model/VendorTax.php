<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorTax extends Model
{
  	protected $table = 'vendor_tax';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [ 'id', 'id_vendor_detail', 'npwp', 'npwp_scan', 'status', 'created_at', 'updated_at'];

}