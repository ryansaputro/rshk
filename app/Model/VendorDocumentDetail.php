<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDocumentDetail extends Model
{
  	protected $table = 'vendor_document_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_document', 'file', 'status', 'created_at', 'updated_at'];


}
