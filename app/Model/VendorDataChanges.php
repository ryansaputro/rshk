<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorDataChanges extends Model
{
  	protected $table = 'vendor_data_changes';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [ 'id', 'id_vendor_detail', 'data_changes', 'sub_data_changes', 'reason', 'changes_by', 'datetime', 'status', 'created_at', 'updated_at'];

}
