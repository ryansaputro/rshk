<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReceiveItem extends Model
{
  	protected $table = 'receive_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'no_receive_item', 'id_order', 'id_do', 'datetime', 'id_user', 'status', 'id_gudang', 'data_spj', 'uploaded_at',  'created_at', 'updated_at'];
}
