<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorExpertStaff extends Model
{
  	protected $table = 'vendor_expert_staff';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_vendor_detail', 'name_vendor_expert_staff', 'birth_vendor_expert_staff', 'address_vendor_expert_staff', 'gender_vendor_expert_staff', 'education_vendor_expert_staff', 'nationallty_vendor_expert_staff', 'experience_vendor_expert_staff', 'email_vendor_expert_staff', 'expertise_vendor_expert_staff'];

    public function SaveStaff($insertZ) {
	$data = DB::unprepared("INSERT INTO `vendor_expert_staff`(`id_vendor_detail`, `name_vendor_expert_staff`, `birth_vendor_expert_staff`, `address_vendor_expert_staff`, `gender_vendor_expert_staff`, `education_vendor_expert_staff`, `nationallty_vendor_expert_staff`, `experience_vendor_expert_staff`, `email_vendor_expert_staff`, `expertise_vendor_expert_staff`) VALUES ".$insertZ);
	return $data;
    }

}
