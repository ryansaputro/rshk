<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryWarehouseType extends Model
{
  	protected $table = 'category_warehouse_type';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_gudang', 'id_category', 'status', 'description', 'created_at', 'updated_at' ];
}
