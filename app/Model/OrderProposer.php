<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderProposer extends Model
{
  	protected $table = 'order_proposer';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'datetime', 'no_prop', 'proposer', 'created_at', 'updated_at'];

}
