<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ItemReject extends Model
{
  	protected $table = 'receive_item_reject';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'id_do', 'id_item', 'qty', 'description', 'id_user', 'status', 'created_at', 'updated_at', 'datetime'];
}
