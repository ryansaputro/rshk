<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
  	protected $table = 'chat';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_cart', 'id_user', 'id_vendor_detail', 'id_item', 'datetime', 'message', 'status', 'created_at', 'updated_at'];
}
