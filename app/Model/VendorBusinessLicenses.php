<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorBusinessLicenses extends Model
{
  	protected $table = 'vendor_business_license';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'name_vendor_business_license', 'status','created_at', 'updated_at'];

}
