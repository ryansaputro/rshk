<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorExperiencesDetail extends Model
{
  	protected $table = 'vendor_experiences_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [ 'id', 'id_vendor_experience', 'job_order', 'location', 'user', 'address', 'contract_date', 'contract_finish', 'contract_value', 'type', 'data_source', 'created_at', 'updated_at'];

}