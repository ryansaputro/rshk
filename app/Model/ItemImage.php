<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ItemImage extends Model
{
  	protected $table = 'image_item';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'name', 'file', 'id_item', 'created_at', 'updated_at'];
}
