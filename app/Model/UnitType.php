<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UnitType extends Model
{
  	protected $table = 'unit_type';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'unit_name', 'convert_pcs', 'description', 'status', 'created_at', 'updated_at'];
}
