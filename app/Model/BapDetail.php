<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BapDetail extends Model
{
  	protected $table = 'bap_detail';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id', 'id_bap', 'id_item', 'qty', 'satuan', 'price'];

}
