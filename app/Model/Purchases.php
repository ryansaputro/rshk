<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Purchases extends Model
{
  	protected $table = 'purchase';
    protected $primaryKey = 'id_purchase';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id_purchase', 'datetime','no_purchase', 'id_user', 'created_at', 'updated_at'];

    public function InsertCart($insert) {
        $data = DB::unprepared("INSERT INTO `purchase_detail`(`id_purchase`, `id_item`, `qty`, `status`) VALUES ". $insert);
        return $data;
    }

    public function InsertPurchase($PurchaseInsert) {
        // echo "INSERT INTO `purchase`(`datetime`, `id_user`) VALUES ". $PurchaseInsert;
        // die();
    	$data = DB::unprepared("INSERT INTO `purchase`(`datetime`, `id_user`) VALUES ". $PurchaseInsert);
    	return $data;
    }
}
