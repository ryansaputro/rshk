<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorTools extends Model
{
  	protected $table = 'vendor_tools';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [ 'id', 'id_vendor_detail', 'created_by', 'updated_by', 'created_at', 'updated_at'];

}