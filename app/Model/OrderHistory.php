<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderHistory extends Model
{
  	protected $table = 'order_catalog_history';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = ['id', 'id_order', 'id_item', 'qty', 'id_user', 'id_executor', 'status', 'datetime_history', 'event', 'description', 'created_at', 'updated_at'];

    public function OldQty($id) {
      $data = DB::unprepared("INSERT INTO order_catalog_history(id_order, id_item, qty, id_user, id_executor, status, datetime_history, event, created_at) VALUES ".$id);
      return $data;
    }

    public function RejectHistory($insert) {
      // dd($insert);
      #status 1 di reject oleh supervisi 2 manajer 3 finance
      $data = DB::unprepared("INSERT INTO order_catalog_history(id_order, id_item, qty, id_user, id_executor, status, datetime_history, event, description, created_at) VALUES ".$insert);
      return $data;
    }
}
