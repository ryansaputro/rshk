<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DivisiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()){
            return redirect('auth');
        } else {
            $user = Auth::user();
            if ($user->status == 0) {
                Auth::logout();
                return redirect('auth')->with('error', 'Your status has been disabled, please contact the website admin for more information' );
            }
            if ($user->hasRole('divisi')){
                return $next($request);
            } else {
                return abort(404);
            }
        }        return $next($request);
    }
}
