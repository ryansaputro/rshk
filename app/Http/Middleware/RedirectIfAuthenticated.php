<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
          $user = Auth::user();
          if($user->hasRole('admin')) {
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/admin')->with('key', 'value');
          }else if($user->hasRole('finance')) { //finance katalog
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/users/finance/dashboard')->with('key', 'value');
          }else if($user->hasRole('supervisi')) { //supervisi katalog
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/users/supervisi/dashboard')->with('key', 'value');
          }else if($user->hasRole('divisi')) { //user katalog
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/users/item')->with('key', 'value');
          }else if($user->hasRole('managercatalog')) { //manajer katalog
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/users/manager/dashboard')->with('key', 'value');
          }else if($user->hasRole('directurcatalog')) {
              session(['login' => "sukses"]);
              return redirect()->intended('/catalog/users/directur/dashboard')->with('key', 'value');
          }else if($user->hasRole('vendor')) {
              session(['login' => "sukses"]);
              return redirect()->intended('/vms/users/dashboard')->with('key', 'value');
          }else if($user->hasRole('adminvms')) {
              session(['login' => "sukses"]);
              return redirect()->intended('/vms/admin/dashboard')->with('key', 'value');
          }
            // return redirect('/');
        }

        return $next($request);
    }
}
