<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UnitType;

class SatuanController extends Controller
{
    public function index()
    {
        return UnitType::all();
    }

    public function show(UnitType $satuan)
    {
        return $satuan;
    }

    public function store(Request $request)
    {
        $satuan = UnitType::create($request->all());

        return response()->json($satuan, 201);
    }

    public function update(Request $request, UnitType $satuan)
    {
        $satuan->update($request->all());

        return response()->json($satuan, 200);
    }

    public function delete(UnitType $satuan)
    {
        $satuan->delete();

        return response()->json(null, 204);
    }

}
