<?php
namespace App\Http\Controllers\Vms\activator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\VendorDetail;
use App\Model\VendorAdministrators;
use App\Model\VendorBusinessLicenses;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorAmandementDeed;
use App\Model\VendorOwner;
use App\Model\VendorExpertStaff;
use App\Model\VendorDetailBranch;
use App\Model\VendorDocument;
use App\Model\VendorDocumentDetail;
use App\Model\VendorStatus;
use App\Model\VendorClassification;
use App\Model\VendorChecklist;
use App\Model\VendorInvitation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;

class ActivatorVmsControllerOld extends Controller
{

    public function __construct()
    {
        $this->middleware('activatorvmsauth');
    }

    public function index(Request $request, $id){
        if (Auth::check()) {
          $search = (isset($request->search)) ? $request->search : 'xxx';

          $vendor_detail_x =  DB::table('vendor_detail')
          ->select('vendor_detail.id AS id_vendor','vendor_detail.vendor_name','vendor_detail.created_at','vendor_detail.address','vendor_detail.telephone','users.id AS id_user_vendor', 'users.name', 'users.username', 'users.email', 'users.status AS status_vendor', 'vendor_status.last_status' )
          ->join('users','vendor_detail.id_user', '=', 'users.id')
          ->join('role_user', 'users.id', '=', 'role_user.user_id')
          ->leftJoin('vendor_status','vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
          ->where('role_user.role_id', '4');

          $data_vendor_detail = DB::table('vendor_detail')
                              ->select('vendor_detail.id AS id_vendor','vendor_detail.mobile_phone','vendor_detail.vendor_name','vendor_detail.created_at','vendor_detail.address','vendor_detail.telephone','users.id AS id_user_vendor', 'users.name', 'users.username', 'users.email', 'users.status AS status_vendor', 'vendor_status.last_status' )
                              ->join('users','vendor_detail.id_user', '=', 'users.id')
                              ->leftJoin('vendor_status','vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                              ->where('users.id','<>', Auth::user()->id)
                              ->where('vendor_status.last_status',0)
                              ->orderBy('vendor_detail.created_at','DESC');

            if($search != 'xxx'){
                $vendor_detail_x->where('vendor_detail.vendor_name', 'like', '%'. $search. '%');
                // $data_vendor_detail->where('vendor_detail.vendor_name', 'like', '%'. $search. '%');
            }
            $vendor_detail_x->where('users.id','<>', Auth::user()->id);
            $vendor_detail_x->where('users.status',0);
            $vendor_detail_x->where('vendor_status.last_status','5');
            // $vendor_detail_x->where('vendor_status.last_status','<>','2');
            $vendor_detail_x->orderBy('vendor_detail.created_at','DESC');

            $vendor_detail = $data_vendor_detail->paginate(10);
            $vendor_detail_aktif = $vendor_detail_x->paginate(10);
            // dd($vendor_detail_aktif);
            $surat = VendorBusinessLicenses::where('status',1)->get();
            return view('vms.activator.dashboard.index', compact('vendor_detail','surat', 'vendor_detail_aktif', 'search'))->with('id', $id);
        }else{
            return redirect('auth');
        }
    }

    public function update_status_user_vendor(Request $request) {
        $user = User::find($request->id_user)->update(['status' => $request->status]);
         return response()->json(array(
                            'status' => 1));
    }

    public function detail_vendor(Request $request) {
        $data = VendorDetail::where('id_user',$request->id_user);
        $vendor_detail = $data->get();
        $id_vendor = $data->value('id');
        $akta = VendorDeed::where('id_vendor_detail', $id_vendor)->get();
        $klasifikasi = DB::table('vendor_classification')->select('vendor_classification.*', 'classification.code', 'classification.name')
                        ->join('classification', 'vendor_classification.id_classification', '=', 'classification.id')
                        ->where('vendor_classification.id_vendor_detail', $id_vendor)
                        ->get();

        VendorClassification::where('id_vendor_detail', $id_vendor)->get();
        $pemilik = VendorOwner::where('id_vendor_detail', $id_vendor)->get();
        $pengurus = VendorAdministrators::where('id_vendor_detail', $id_vendor)->get();
        $ahli = VendorExpertStaff::where('id_vendor_detail', $id_vendor)->get();
        $dtDok = VendorDocument::where('id_vendor_detail', $id_vendor);
        $dokumen = $dtDok->get();
        $suratDtl = DB::table('vendor_business_license')->select('vendor_business_license.id AS id_surat','vendor_business_license.name_vendor_business_license', 'vendor_business_license_detail.*')
                    ->join('vendor_business_license_detail', 'vendor_business_license.id','=', 'vendor_business_license_detail.id_vendor_business_license')
                    ->where('vendor_business_license_detail.id_vendor_detail', $id_vendor)
                    ->get();
        $vendorCek = VendorChecklist::where('id_vendor', $id_vendor)->get();
        $surat = VendorBusinessLicenses::where('status',1)->get();
        return response()->json(array(
                            'category' => $vendor_detail,
                            'akta' => $akta,
                            'klasifikasi' => $klasifikasi,
                            'pemilik' => $pemilik,
                            'pengurus' => $pengurus,
                            'ahli' => $ahli,
                            'dokumen' => $dokumen,
                            'suratDtl' => $suratDtl,
                            'id_vendor' => $id_vendor,
                            'vendorCek' => $vendorCek,
                            'surat' => $surat,
                            'status' => 1));

    }

    public function document_vendor(Request $request) {
        $vendor = VendorDetail::where('id_user', $request->id_user)->firstOrFail();
        $vendor_detail_x = DB::table('vendor_detail')
                    ->select('vendor_detail.id AS id_vendor','vendor_detail.vendor_name','vendor_detail.created_at','vendor_detail.address','vendor_detail.telephone','users.id AS id_user_vendor', 'users.name', 'users.username', 'users.email', 'users.status AS status_vendor', 'vendor_status.last_status' )
                    ->join('users','vendor_detail.id_user', '=', 'users.id')
                    ->leftJoin('vendor_status','vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                    ->where('users.id',$request->id_user)
                    ->orderBy('vendor_detail.id','DESC')->get();

        $doc = DB::table('vendor_document')
                ->select('vendor_document.*','vendor_document_detail.file','vendor_document_detail.status','vendor_document_detail.created_at')
                ->join('vendor_document_detail','vendor_document.id', '=','vendor_document_detail.id_document')
                ->where('vendor_document.id_vendor_detail',$vendor->id)
                ->get();






        return response()->json(array(
                            'doc' => $doc,
                            'vendor_id' => $vendor->id,
                            'vendor_detail_x' => $vendor_detail_x,
                            'status' => 1));

    }

    public function status_vendor(Request $request) {
        $dataVendor = DB::table('vendor_detail')
                     ->select('vendor_detail.vendor_name','users.email','vendor_detail.id_user')
                     ->join('users','vendor_detail.id_user','=','users.id')
                     ->where('vendor_detail.id',$request->vendor)
                     ->first();
                     $status = ($request->status == 5) ? '6' : '5';
        $user = User::where('id', $dataVendor->id_user)->update(['password' => Hash::make('123456')]);
        $vendor = VendorStatus::updateOrCreate([
                                        'id_vendor_detail' => $request->vendor],[
                                        'id_admin_vms' => Auth::user()->id,
                                        'last_status' => $status]);

        if($request->status == 5){
            $data = array('name'=>$dataVendor->vendor_name,'pesan' => 'Selamat '.$dataVendor->vendor_name,' telah','pesan_2' => 'pada tahap VMS', 'status' => 'LULUS' ,'penerima'=>$dataVendor->email, 'password' => '123456');
        }else{
            $data = array('name'=>$dataVendor->vendor_name,'pesan' => 'Maaf '.$dataVendor->vendor_name,'status'=>'GAGAL','pesan_2'=>'pada tahap VMS' ,'penerima'=>$dataVendor->email);
        }
        $contactEmail = $dataVendor->email;
        $Sendemail = Mail::send('vms.activator.email.index', $data, function($message)  use ($contactEmail) {
        $message->to($contactEmail, 'Pemberitahuan Pengujian Berkas')->subject
        ('Pemberitahuan Pengujian Berkas');
        $message->from('all@mrxoa.com','Admin RSHK');
        });

        return response()->json(array(
                            'id_vendor' =>  $request->vendor,
                            'status' => 1));

    }

    public function send_code_verifikasi(Request $request) {

        $active = User::find($request->id);
        $activation = $active->activation_key;
        $contactEmail = $active->email;
        $username = $active->username;
        $host = URL::to('/vms/users/registration/'.$activation);

        $data = array('host' => $host, 'name'=>$request->name,'pesan' => 'silahkan mengaktivasi akun anda ','penerima'=>$contactEmail, 'activation_key' => $activation, 'username' => $request->name, 'password' => $username.'123');
        $Sendemail = Mail::send('vms.guest.email.index', $data, function($message)  use ($contactEmail) {
        $message->to($contactEmail, 'Aktivasi Akun')->subject
        ('Aktivasi Akun');
        $message->from('all@mrxoa.com','Admin RSHK');
        });
            if($Sendemail){
                $status = 1;
            }else{
                $status = 0;
            }
        return response()->json(array(
                    'status' => $status));

    }

    public function checklist(Request $request){
      $DataLain = ["LOGO","NPWP"];
      $surat = VendorBusinessLicenses::where('status',1)->pluck('name_vendor_business_license')->toArray();
      $ceklisModul = array_merge($DataLain,$surat);
      $vendor = VendorDetail::where('id', $request->id_vendor)->first();
      if($request->ceklis != null){
      foreach ($request->ceklis as $key => $value) {
        $ceklis = VendorChecklist::updateOrCreate(
          [
            'id_vendor' => $request->id_vendor,
            'data' => $key,
          ],
          [
            'description' => 'sukses',
            'status' => 1
          ]);
      }

      $cekVendor = VendorChecklist::where('id_vendor',  $request->id_vendor)->pluck('data')->toArray();
      $result=array_diff($ceklisModul,$cekVendor);
      if(count($result) == 0){
        $status = VendorStatus::where('id_vendor_detail', $request->id_vendor)->updateOrCreate(
    			['id_vendor_detail' => $request->id_vendor],
    			['last_status' => 2]
    		);
      }else{
        $user = User::where('id', $vendor->id_user)->value('email');
        $data = array('berkas' => $result,'name'=>$vendor->vendor_name,'pesan' => 'Silahkan melengkapi berkas ','penerima'=>$user);
        $Sendemail = Mail::send('vms.checker.email.requirements', $data, function($message)  use ($user) {
        $message->to($user, 'Berkas Tidak Lengkap')->subject
        ('Berkas Tidak Lengkap');
        $message->from('all@mrxoa.com','Admin RSHK');
        });
      }
    }

      return redirect('/vms/checker/vendor_cek_berkas');
    }

    public function invitation(Request $request){
    $vendor = VendorDetail::where('id', $request->id_vendor)->first();
    $id = md5($vendor->id);
    $host = URL::to('/vms/users/invitation/'.$id.'/attend');
    $ceklis = VendorInvitation::updateOrCreate(
          [
            'id_vendor_detail' => $request->id_vendor,
          ],
          [
            'date' => $request->invitation_date,
            'time' => $request->invitation_time,
            'place' => $request->place,
            'status' => 0
          ]);

        $user = User::where('id', $vendor->id_user)->value('email');

        $data = array('host' => $host, 'place' => $request->place,'date' => $request->invitation_date,'time' => $request->invitation_time,'name'=>$vendor->vendor_name,'penerima'=>$user);
        $Sendemail = Mail::send('vms.checker.email.invitation', $data, function($message)  use ($user) {
          $message->to($user, 'Undangan')->subject
          ('Undangan');
          $message->from('all@mrxoa.com','Admin RSHK');
        });

      return redirect('/vms/checker/vendor_undang');
    }

}
