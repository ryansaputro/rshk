<?php
namespace App\Http\Controllers\Vms\guest\register;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Fungsi;
use URL;
use Mail;
use App\Model\VendorAdministrators;
use App\Model\VendorAmandementDeed;
use App\Model\VendorBusinessLicense;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorDetail;
use App\Model\VendorDetailBranch;
use App\Model\VendorExpertStaff;
use App\Model\VendorOwner;
use App\Model\VendorBusinessLicenses;
use App\Model\VendorStatus;

class RegisterControllers extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     public function showRegistrationForm(Request $request){
       return view('vms.guest.register.index');
     }

     public function save(Request $request){
       $validatedData = $request->validate([
             'email' => 'required|string|email|max:255|unique:users',
             'vendor_name' => 'required|string',
             'username' => 'required|string',
             'mobile_phone' => 'required|string|max:12|unique:vendor_detail'
          ]);

          $name = $request->jenis." ".$request->vendor_name;
          $user = User::create([
              'name'      => $name,
              'email'     => $request->email,
              'username'     => $request->username,
              'status'    => '0'
          ]);
          #simpan ke user has roles
          $user->roles()->sync('4');

          #simpan ke vendor_detail
          $vendor_detail = VendorDetail::create([
              'id_user'      => $user->id,
              'vendor_name'      => $request->vendor_name,
              'mobile_phone'      => $request->mobile_phone,
          ]);

          $status = VendorStatus::where('id_vendor_detail', $vendor_detail->id)->updateOrCreate(
        			['id_vendor_detail' => $vendor_detail->id],
        			['last_status' => 0]
        		);


          if(($user) && ($vendor_detail)){
              session()->flash('message','Selamat!. Registrasi berhasil tunggu Email konfirmasi untuk mendapatkan kode aktivasi');
          }else{
              session()->flash('message','Gagal!. Silahkan Registrasi Ulang!');
          }
      return redirect('auth');
     }

}
