<?php

namespace App\Http\Controllers\Vms\guest\register;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Fungsi;
use URL;
use Mail;
use App\Model\VendorAdministrators;
use App\Model\VendorAmandementDeed;
use App\Model\VendorBusinessLicense;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorDetail;
use App\Model\VendorDetailBranch;
use App\Model\VendorExpertStaff;
use App\Model\VendorOwner;

class RegisterControllerOld extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'id_user' => 'required|string',
            'vendor_name' => 'required|string',
            'npwp' => 'required|string',
            'address' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
            'post_code' => 'required|string',
            'username' => 'required|string',
            'pkp' => 'required|string',
            'telephone' => 'required|string',
            'fax' => 'required|string',
            'mobile_phone' => 'required|string',
            'email' => 'required|string',
            'website' => 'required|string',
            'branch_office' => 'required|string',
            'username' => 'required|string',
            'image' => 'required|string',
            'deed_number' => 'required|number',
            'date' => 'required|date',
            'notary_public' => 'required|string',
            // 'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function getKota(Request $request)
    {
        $a = Fungsi::IndonesiaProvince();
        if (array_key_exists($request->id,$a)){
              $kota = $a[$request->id];
        }

        return response()->json([ 'kota' => $kota ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {



    }

    protected function save(Request $request)
    {
        #simpan ke user dulu
        $user = User::create([
            'name'      => $request->vendor_name,
            'username'  => $request->username,
            'email'     => $request->email,
            'password'  => Hash::make($request->username."123"),
            'image'     => '',
            'status'    => '0'
        ]);
        #simpan ke user has roles
        $user->roles()->sync('4');

        #simpan ke vendor_detail
        $vendor_detail = VendorDetail::create([
            'id_user'      => $user->id,
            'vendor_name'      => $request->vendor_name,
            'npwp'      => $request->npwp,
            'address'      => $request->address,
            'province'      => $request->province,
            'city'      => $request->city,
            'post_code'      => $request->post_code,
            'username'      => $request->username,
            'pkp'      => $request->pkp,
            'telephone'      => $request->telephone,
            'fax'      => $request->fax,
            'mobile_phone'      => $request->mobile_phone,
            'email'      => $request->email,
            'website'      => $request->website,
            'branch_office' => $request->branch_office
        ]);

        if($request->branch_office == '1') {
         VendorDetailBranch::create([
                'id_vendor_detail' => $vendor_detail->id,
                'address'      => $request->addresscentral,
                'telephone'      => $request->telephonecentral,
                'fax'      => $request->faxcentral,
                'mobile_phone'      => $request->hpcentral
            ]);
        }



        #simpan ke vendor_deed
        $vendor_deed = VendorDeed::create([
            'id_vendor_detail'      => $vendor_detail->id,
            'deed_number'      => $request->deed_number,
            'date'      => $request->date,
            'notary_public'      => $request->notary_public
        ]);

        #kalo ada akta perubahan simpan ke vendor_amandement_deed
        if(($request->deed_number_akta != '') &&
                ($request->date_akta != '') &&
                    ($request->notary_public_akta != '')){

            $vendor_amandement_deed = VendorAmandementDeed::create([
            'id_vendor_deed'      => $vendor_deed->id,
            'deed_number'      => $request->deed_number_akta,
            'date'      => $request->date_akta,
            'notary_public'      => $request->notary_public_akta

            ]);
        }

        #simpan surat surat ke vendor_business_license
        $reference_number = $request->reference_number;
        $giver_agency = $request->giver_agency;
        $valid_until = $request->valid_until;
        $qualification = $request->qualification;
        $classification = $request->classification;

        $insert ="";
        foreach ($reference_number as $k => $value) {
            if($insert) $insert .= ",";
            $insert .= "('".$k."','".$vendor_detail->id."','".$value[0]."','".$giver_agency[$k][0]."','".$valid_until[$k][0]."','".$qualification[$k][0]."','".$classification[$k][0]."')";
        }

        $vendor_business_license = new VendorBusinessLicenseDetail;
        $save_vendor_business_license = $vendor_business_license->SaveDetail($insert);

        #simpan ke pemilik table vendor_owner
        $name_vendor_owner = $request->name_vendor_owner;
        $ktp_vendor_owner = $request->ktp_vendor_owner;
        $address_vendor_owner = $request->address_vendor_owner;

        $insertX ="";
        foreach ($name_vendor_owner as $k => $value) {
            if($insertX) $insertX .=",";
            $insertX .= "('".$vendor_detail->id."','".$value."','".$ktp_vendor_owner[$k]."','".$address_vendor_owner[$k]."')";
        }

        $vendor_owner = new VendorOwner;
        $save_vendor_owner = $vendor_owner->SaveOwner($insertX);

        #simpan ke pemilik table vendor_administrator
        $name_vendor_administrators = $request->name_vendor_administrators;
        $ktp_vendor_administrators = $request->ktp_vendor_administrators;
        $address_vendor_administrators = $request->address_vendor_administrators;
        $position_vendor_administrators = $request->position_vendor_administrators;

        $insertY ="";
        foreach ($name_vendor_administrators as $k => $value) {
            if($insertY) $insertY .=",";
            $insertY .= "('".$vendor_detail->id."','".$value."','".$ktp_vendor_administrators[$k]."','".$address_vendor_administrators[$k]."','".$position_vendor_administrators[$k]."')";
        }

        $vendor_owner = new VendorAdministrators;
        $save_vendor_owner = $vendor_owner->SaveAdministrators($insertY);

        #save ke staf ahli table vendor_expert_staff
        $name_vendor_expert_staff = $request->name_vendor_expert_staff;
        $birth_vendor_expert_staff = $request->birth_vendor_expert_staff;
        $address_vendor_expert_staff = $request->address_vendor_expert_staff;
        $gender_vendor_expert_staff = $request->gender_vendor_expert_staff;
        $education_vendor_expert_staff = $request->education_vendor_expert_staff;
        $education_vendor_expert_staff = $request->education_vendor_expert_staff;
        $nationallty_vendor_expert_staff = $request->nationallty_vendor_expert_staff;
        $experience_vendor_expert_staff = $request->experience_vendor_expert_staff;
        $email_vendor_expert_staff = $request->email_vendor_expert_staff;
        $expertise_vendor_expert_staff = $request->expertise_vendor_expert_staff;

        $insertZ = "";
        foreach($name_vendor_expert_staff as $k => $v){
            if($insertZ) $insertZ .=",";
            $insertZ .= "('".$vendor_detail->id."','".$v."','".$birth_vendor_expert_staff[$k]."','".$address_vendor_expert_staff[$k]."','".$gender_vendor_expert_staff[$k]."','".$education_vendor_expert_staff[$k]."','".$nationallty_vendor_expert_staff[$k]."','".$experience_vendor_expert_staff[$k]."','".$email_vendor_expert_staff[$k]."','".$expertise_vendor_expert_staff[$k]."')";
        }

        $vendor_expert_staff = new VendorExpertStaff;
        $save_vendor_expert_staff = $vendor_expert_staff->SaveStaff($insertZ);


        if(($user) && ($vendor_detail) && ($vendor_deed)  && ($save_vendor_business_license) && ($save_vendor_owner) && ($save_vendor_owner) && ($save_vendor_expert_staff)){
            session()->flash('message','Selamat!. Registrasi berhasil tunggu Email konfirmasi untuk mendapatkan kode aktivasi');
        }else{
            session()->flash('message','Gagal!. Silahkan Registrasi Ulang!');
        }
        return redirect('auth');

    }


}
