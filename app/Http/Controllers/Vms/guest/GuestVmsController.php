<?php
namespace App\Http\Controllers\Vms\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\VendorDetail;
use App\Model\VendorInvitation;
use App\Model\VendorAdministrators;
use App\Model\VendorBusinessLicenses;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorAmandementDeed;
use App\Model\VendorOwner;
use App\Model\VendorExpertStaff;
use App\Model\VendorDetailBranch;
use App\Model\VendorDocument;
use App\Model\VendorDocumentDetail;
use App\Model\VendorTools;
use App\Model\VendorToolsDetail;
use App\Model\VendorExperiences;
use App\Model\VendorExperiencesDetail;
use App\Model\VendorTax;
use App\Model\VendorDirectur;
use App\Model\VendorTaxDetail;
use App\Model\VendorClassification;
use App\Model\VendorStatus;
use App\Model\OrderProposer;
use App\Model\Chat;
use App\Model\Category;
use App\Model\CategoryItem;
use App\Model\Item;
use App\Model\Supplier;
use App\Model\Order;
use App\Model\GeneralSpec;
use App\Model\CategorySpec;
use App\Model\ItemSpec;
use App\Model\ItemImage;
use App\Model\ItemPriceHistory;
use App\Model\ItemDoc;
use App\Model\OrderDetail;
use App\Model\DeliveryOrder;
use App\Model\DeliveryOrderDetail;
use App\Model\Invoice;
use App\Model\Contract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use \PDF;
use File;
use Fungsi;
use Session;
use DB;
use Excel;
use App\User;

class GuestVmsController extends Controller
{
	public function registrationByNoRegis(Request $request, $id) {
		// $user = User::where('activation_key', $id)->first();
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}

		$userDetail = VendorDetail::where('id_user', $user->id)->first();
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$propinsi = Fungsi::propinsi();
		$list_propinsi = Fungsi::propinsi();
		$list_kota = Fungsi::IndonesiaProvince();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();
		return view('vms.guest.register.create-step1',compact('userDetail', $userDetail,'user',$user, 'list_propinsi', $list_propinsi, 'list_kota', $list_kota, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}


	public function postCreateStep1(Request $request, $id){
		$validatedData = $request->validate([
									'npwp' => 'required|numeric',
									'address' => 'required|string',
									'province' => 'required|string',
									'city' => 'required|string',
									'post_code' => 'required|numeric',
									'username' => 'required|string',
									'pkp' => 'required|numeric',
									'telephone' => 'required|string',
									'fax' => 'required|string',
									'mobile_phone' => 'required|string',
									'website' => 'required|string',
									'branch_office' => 'required|string',
									'no_rek' => 'required|numeric',
									'bank' => 'required|string',
									'email' => 'required|email',
			 ]);
			 $arrBranch = array('address' => $request->addresscentral, 'mobile_phone'  => $request->hpcentral, 'telephone'  => $request->telephonecentral, 'fax'  => $request->faxcentral);
						if(($request->session()->get('identitas') == null) || (empty($request->session()->get('identitas'))) && (empty($request->session()->get('identitasBranch')))){
								$vendorDetail = new VendorDetail();
								$vendorDetail->fill($validatedData);
								$request->session()->put('identitas', $vendorDetail);
								$vendorDetailBranch = new VendorDetailBranch();
								$vendorDetailBranch->fill($arrBranch);
								$request->session()->put('identitasBranch', $vendorDetailBranch);
						}else{
								$vendorDetail = $request->session()->get('identitas');
								$vendorDetail->fill($validatedData);
								$request->session()->put('identitas', $vendorDetail);
								$vendorDetailBranch = $request->session()->get('identitasBranch');
								$vendorDetailBranch->fill($arrBranch);
								$request->session()->put('identitasBranch', $vendorDetailBranch);
						}
						return redirect('/vms/guest/register/create-step2/'.$id);
	}

	public function createStep2(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('identitas') == null){
			return abort(404);
		}
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$propinsi = Fungsi::propinsi();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step2',compact('id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}

	public function postCreateStep2(Request $request, $id){
		$validatedData = $request->validate([
									'deed_number' => 'required|numeric',
									'date' => 'required|date',
									'notary_public' => 'required|string',
			 ]);
						if(empty($request->session()->get('akta'))){
								$vendorDeed = new VendorDeed();
								$vendorDeed->fill($validatedData);
								$request->session()->put('akta', $vendorDeed);
						}else{
								$vendorDeed = $request->session()->get('akta');
								$vendorDeed->fill($validatedData);
								$request->session()->put('akta', $vendorDeed);
						}

						return redirect('/vms/guest/register/create-step3/'.$id);
	}

	public function createStep3(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('akta') == null){
			return abort(404);
		}

		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$vendorLisensi = $request->session()->get('vendor_business_license_detail');
		$propinsi = Fungsi::propinsi();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step3',compact('vendorLisensi', $vendorLisensi,'id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}

	public function postCreateStep3(Request $request, $id){
		$validatedData = $request->validate([
			'reference_number' => 'required|array',
			'valid_until' => 'required|array',
			'start_date' => 'required|array',
			'giver_agency' => 'required|array'
		]);
		$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');
		if(empty($request->session()->get('vendor_business_license_detail'))){
			foreach ($request->reference_number as $key => $value) {
				$input = [
					'id_vendor_business_license' => $key,
					'id_vendor_detail' => $id_vendor_detail,
					'reference_number' => $request->reference_number[$key],
					'giver_agency' => $request->giver_agency[$key],
					'valid_until' => $request->valid_until[$key],
					'start_date' => $request->start_date[$key]
				];
				$vendorLisensi = new VendorBusinessLicenseDetail;
				$vendorLisensi->fill($input);
				$request->session()->push('vendor_business_license_detail', $vendorLisensi);
			}
		}else{
			$vendorLisensi = $request->session()->get('vendor_business_license_detail');
			if ($request->session()->has('vendor_business_license_detail')) {
					$request->session()->forget('vendor_business_license_detail');
			}

			foreach ($request->reference_number as $key => $value) {
				$input = [
					'id_vendor_business_license' => $key,
					'id_vendor_detail' => $id_vendor_detail,
					'reference_number' => $request->reference_number[$key],
					'giver_agency' => $request->giver_agency[$key],
					'valid_until' => $request->valid_until[$key],
					'start_date' => $request->start_date[$key]
				];
				$vendorLisensi = new VendorBusinessLicenseDetail;
				$vendorLisensi->fill($input);
				$request->session()->push('vendor_business_license_detail', $vendorLisensi);
			}

		}
		return redirect('/vms/guest/register/create-step4/'.$id);
	}

	public function createStep4(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('vendor_business_license_detail') == null){
			return abort(404);
		}
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$vendorLisensi = $request->session()->get('vendor_business_license_detail');
		$vendorClassification = $request->session()->get('vendor_classification');
		$propinsi = Fungsi::propinsi();
		$klasifikasi = DB::table('classification')->where('status', '1')->get();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step4',compact('vendorClassification' ,$vendorClassification, 'klasifikasi',$klasifikasi,'vendorLisensi', $vendorLisensi, 'id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}

	public function postCreateStep4(Request $request, $id){
		$validatedData = $request->validate([
									'id_classification' => 'required|array',
									'qualification' => 'required|array',
									'no_letter' => 'required|array',
									'expired_date' => 'required|array',
									'sub_classification' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			// foreach ($request->id_classification as $key => $value) {
			// 	print_r($request->sub_classification[$key]);
			// }
			// dd($request->all());

			 if(empty($request->session()->get('vendor_classification'))){
			 	foreach ($request->id_classification as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'id_classification' => $request->id_classification[$key],
			 			'sub_classification' => $request->sub_classification[$key],
			 			'qualification' => $request->qualification[$key],
			 			'no_letter' => $request->no_letter[$key],
			 			'expired_date' => $request->expired_date[$key],
						'status' => '1'
			 		];
			 		$vendorClassification = new VendorClassification;
			 		$vendorClassification->fill($input);
			 		$request->session()->push('vendor_classification', $vendorClassification);
			 	}
			 }else{
			 	$vendorClassification = $request->session()->get('vendor_classification');
			 	if ($request->session()->has('vendor_classification')) {
			 			$request->session()->forget('vendor_classification');
			 	}

			 	foreach ($request->id_classification as $key => $value) {
					$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'id_classification' => $request->id_classification[$key],
						'sub_classification' => $request->sub_classification[$key],
			 			'qualification' => $request->qualification[$key],
			 			'no_letter' => $request->no_letter[$key],
			 			'expired_date' => $request->expired_date[$key],
						'status' => '1'
			 		];
			 		$vendorClassification = new VendorClassification;
			 		$vendorClassification->fill($input);
			 		$request->session()->push('vendor_classification', $vendorClassification);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step5/'.$id);
	}

	public function createStep5(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('vendor_classification') == null){
			return abort(404);
		}
		$vendorOwner = $request->session()->get('vendor_owner');
		return view('vms.guest.register.create-step5',compact('vendorOwner', $vendorOwner, 'id', $id));
	}

	public function postCreateStep5(Request $request, $id){
		$validatedData = $request->validate([
									'name_vendor_owner' => 'required|array',
									'ktp_vendor_owner' => 'required|array',
									'address_vendor_owner' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_owner'))){
			 	foreach ($request->name_vendor_owner as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_owner' => $request->name_vendor_owner[$key],
			 			'ktp_vendor_owner' => $request->ktp_vendor_owner[$key],
			 			'address_vendor_owner' => $request->address_vendor_owner[$key],
			 		];
			 		$vendorOwner = new VendorOwner;
			 		$vendorOwner->fill($input);
			 		$request->session()->push('vendor_owner', $vendorOwner);
			 	}
			 }else{
			 	$vendorOwner = $request->session()->get('vendor_owner');
			 	if ($request->session()->has('vendor_owner')) {
			 			$request->session()->forget('vendor_owner');
			 	}

			 	foreach ($request->name_vendor_owner as $key => $value) {
					$input = [
						'id_vendor_detail' => $id_vendor_detail,
						'name_vendor_owner' => $request->name_vendor_owner[$key],
						'ktp_vendor_owner' => $request->ktp_vendor_owner[$key],
						'address_vendor_owner' => $request->address_vendor_owner[$key],
					];
					$vendorOwner = new VendorOwner;
			 		$vendorOwner->fill($input);
			 		$request->session()->push('vendor_owner', $vendorOwner);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step6/'.$id);
	}

	public function createStep6(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('vendor_owner') == null){
			return abort(404);
		}
		$vendorAdministrators = $request->session()->get('vendor_administrators');
		return view('vms.guest.register.create-step6',compact('vendorAdministrators', $vendorAdministrators, 'id', $id));
	}

	public function postCreateStep6(Request $request, $id){
		$validatedData = $request->validate([
									'name_name_vendor_administrators' => 'required|array',
									'ktp_name_vendor_administrators' => 'required|array',
									'address_name_vendor_administrators' => 'required|array',
									'position_name_vendor_administrators' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_administrators'))){
			 	foreach ($request->name_name_vendor_administrators as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_name_vendor_administrators' => $request->name_name_vendor_administrators[$key],
			 			'ktp_name_vendor_administrators' => $request->ktp_name_vendor_administrators[$key],
			 			'address_name_vendor_administrators' => $request->address_name_vendor_administrators[$key],
			 			'position_name_vendor_administrators' => $request->position_name_vendor_administrators[$key],
			 		];
			 		$vendorAdministrators = new VendorAdministrators;
			 		$vendorAdministrators->fill($input);
			 		$request->session()->push('vendor_administrators', $vendorAdministrators);
			 	}
			 }else{
			 	$vendorAdministrators = $request->session()->get('vendor_administrators');
			 	if ($request->session()->has('vendor_administrators')) {
			 			$request->session()->forget('vendor_administrators');
			 	}

			 	foreach ($request->name_name_vendor_administrators as $key => $value) {
					$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_name_vendor_administrators' => $request->name_name_vendor_administrators[$key],
			 			'ktp_name_vendor_administrators' => $request->ktp_name_vendor_administrators[$key],
			 			'address_name_vendor_administrators' => $request->address_name_vendor_administrators[$key],
			 			'position_name_vendor_administrators' => $request->position_name_vendor_administrators[$key],
			 		];
					$vendorAdministrators = new VendorAdministrators;
			 		$vendorAdministrators->fill($input);
			 		$request->session()->push('vendor_administrators', $vendorAdministrators);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step7/'.$id);
	}

	public function createStep7(Request $request, $id){
		$user = DB::table('users')
						->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
						->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
						->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
						->where('vendor_status.last_status', 0)
						->where('activation_key', $id)
						->first();
		if($user == null){
			return abort(404);
		}
		if($request->session()->get('vendor_administrators') == null){
			return abort(404);
		}
		$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
		return view('vms.guest.register.create-step7',compact('vendorExpertStaff', $vendorExpertStaff, 'id', $id));
	}

	public function postCreateStep7(Request $request, $id){
		$validatedData = $request->validate([
									'name_vendor_expert_staff' => 'required|array',
									'birth_vendor_expert_staff' => 'required|array',
									'address_vendor_expert_staff' => 'required|array',
									'gender_vendor_expert_staff' => 'required|array',
									'education_vendor_expert_staff' => 'required|array',
									'nationallty_vendor_expert_staff' => 'required|array',
									'experience_vendor_expert_staff' => 'required|array',
									'email_vendor_expert_staff' => 'required|array',
									'expertise_vendor_expert_staff' => 'required|array',
			 ]);

	 	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_expert_staff'))){
			 	foreach ($request->name_vendor_expert_staff as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_expert_staff' => $request->name_vendor_expert_staff[$key],
			 			'birth_vendor_expert_staff' => $request->birth_vendor_expert_staff[$key],
			 			'address_vendor_expert_staff' => $request->address_vendor_expert_staff[$key],
			 			'gender_vendor_expert_staff' => $request->gender_vendor_expert_staff[$key],
			 			'education_vendor_expert_staff' => $request->education_vendor_expert_staff[$key],
			 			'nationallty_vendor_expert_staff' => $request->nationallty_vendor_expert_staff[$key],
			 			'experience_vendor_expert_staff' => $request->experience_vendor_expert_staff[$key],
			 			'email_vendor_expert_staff' => $request->email_vendor_expert_staff[$key],
			 			'expertise_vendor_expert_staff' => $request->expertise_vendor_expert_staff[$key],
			 		];
			 		$vendorExpertStaff = new VendorExpertStaff;
			 		$vendorExpertStaff->fill($input);
			 		$request->session()->push('vendor_expert_staff', $vendorExpertStaff);
			 	}
			 }else{
			 	$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			 	if ($request->session()->has('vendor_expert_staff')) {
			 			$request->session()->forget('vendor_expert_staff');
			 	}

				foreach ($request->name_vendor_expert_staff as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_expert_staff' => $request->name_vendor_expert_staff[$key],
			 			'birth_vendor_expert_staff' => $request->birth_vendor_expert_staff[$key],
			 			'address_vendor_expert_staff' => $request->address_vendor_expert_staff[$key],
			 			'gender_vendor_expert_staff' => $request->gender_vendor_expert_staff[$key],
			 			'education_vendor_expert_staff' => $request->education_vendor_expert_staff[$key],
			 			'nationallty_vendor_expert_staff' => $request->nationallty_vendor_expert_staff[$key],
			 			'experience_vendor_expert_staff' => $request->experience_vendor_expert_staff[$key],
			 			'email_vendor_expert_staff' => $request->email_vendor_expert_staff[$key],
			 			'expertise_vendor_expert_staff' => $request->expertise_vendor_expert_staff[$key],
			 		];
					$vendorExpertStaff = new VendorExpertStaff;
			 		$vendorExpertStaff->fill($input);
			 		$request->session()->push('vendor_expert_staff', $vendorExpertStaff);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step8/'.$id);
	}


		public function createStep8(Request $request, $id){
			$user = DB::table('users')
							->select('users.*','vendor_detail.id AS id_vendor','vendor_status.last_status')
							->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
							->join('vendor_status', 'vendor_detail.id','=','vendor_status.id_vendor_detail')
							->where('vendor_status.last_status', 0)
							->where('activation_key', $id)
							->first();
			if($user == null){
				return abort(404);
			}
			if($request->session()->get('vendor_expert_staff') == null){
				return abort(404);
			}
			$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			$vendorAdministrators = $request->session()->get('vendor_administrators');
			$vendorOwner = $request->session()->get('vendor_owner');
			$vendorLisensi = $request->session()->get('vendor_business_license_detail');
			$vendorDocLogo = $request->session()->get('vendor_document_logo');
			$vendorDocNpwp = $request->session()->get('vendor_document_npwp');
			$vendorDocAkta = $request->session()->get('vendor_document_akta');
			$vendorDocSurat = $request->session()->get('vendor_document_surat');
			$vendorDocPemilik = $request->session()->get('vendor_document_pemilik');
			$vendorDocPengurus = $request->session()->get('vendor_document_pengurus');
			$vendorDocTa = $request->session()->get('vendor_document_tenaga');
			$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();
			$users = User::where('activation_key', $id)->value('id');
			$idVendor = VendorDetail::where('id_user',$users)->value('id');
			return view('vms.guest.register.create-step8',compact('vendorDocTa',$vendorDocTa,'vendorDocPengurus', $vendorDocPengurus,'vendorDocPemilik',$vendorDocPemilik,'vendorDocSurat',$vendorDocSurat,'vendorDocAkta',$vendorDocAkta,'vendorDocNpwp',$vendorDocNpwp,'idVendor', $idVendor,'vendorDocLogo',$vendorDocLogo,'vendorOwner', $vendorOwner, 'vendorAdministrators',$vendorAdministrators, 'vendorExpertStaff', $vendorExpertStaff, 'surat_perusahaan',$surat_perusahaan, 'vendorExpertStaff', $vendorExpertStaff, 'id', $id));
		}

		public function logoUpload(Request $request, $id){
			$validatedData = $request->validate([
				'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocLogo = new VendorDocument;

			$fLogo = $request->file('logo');
			$menus = "logo";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileLogo = "LOGO-".date('Ymds').$Vendor_data->id.".".$fLogo->getClientOriginalExtension();
			$data = $fLogo->move($destinationPath, $fileLogo);

			$vendorDocLogo->file = $fileLogo;
			$request->session()->put('vendor_document_logo', $vendorDocLogo);

			return redirect('/vms/guest/register/create-step8/'.$id);
		}

		public function logoDelete(Request $request, $id)
    {
				$vendorDocLogo = $request->session()->get('vendor_document_logo');
        $vendorDocLogo->file = null;
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function npwpDelete(Request $request, $id)
    {
				$vendorDocNpwp = $request->session()->get('vendor_document_npwp');
        $vendorDocNpwp->file = null;
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function aktaDelete(Request $request, $id)
    {
				$vendorDocAkta = $request->session()->get('vendor_document_akta');
        $vendorDocAkta->file = null;
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function suratDelete(Request $request, $id)
    {
				$vendorDocSurat = $request->session()->get('vendor_document_surat');
				unset($vendorDocSurat[$request->id_index]);
				$request->session()->put('vendor_document_surat', $vendorDocSurat);

				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function pemilikDelete(Request $request, $id)
    {
				$vendorDocPemilik = $request->session()->get('vendor_document_pemilik');
				unset($vendorDocPemilik[$request->id_index]);
				$request->session()->put('vendor_document_pemilik', $vendorDocPemilik);
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function pengurusDelete(Request $request, $id)
    {
				$vendorDocPengurus = $request->session()->get('vendor_document_pengurus');
				unset($vendorDocPengurus[$request->id_index]);
				$request->session()->put('vendor_document_pengurus', $vendorDocPengurus);
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function tenagaAhliDelete(Request $request, $id)
    {
				$vendorDocTa = $request->session()->get('vendor_document_tenaga');
				unset($vendorDocTa[$request->id_index]);
				$request->session()->put('vendor_document_tenaga', $vendorDocTa);
				return redirect('/vms/guest/register/create-step8/'.$id);
    }

		public function npwpUpload(Request $request, $id){
			$validatedData = $request->validate([
					'npwp' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocNpwp = new VendorDocument;

			$fNpwp = $request->file('npwp');
			$menus = "npwp";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileNpwp = "NPWP-".date('Ymds').$Vendor_data->id.".".$fNpwp->getClientOriginalExtension();
			$data = $fNpwp->move($destinationPath, $fileNpwp);

			$vendorDocNpwp->file = $fileNpwp;
			$request->session()->put('vendor_document_npwp', $vendorDocNpwp);

			return redirect('/vms/guest/register/create-step8/'.$id);

		}

		public function aktaUpload(Request $request, $id){
			$validatedData = $request->validate([
					'akta' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocAkta = new VendorDocument;

			$fAkta = $request->file('akta');
			$menus = "akta";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileAkta = "AKTA-".date('Ymds').$Vendor_data->id.".".$fAkta->getClientOriginalExtension();
			$data = $fAkta->move($destinationPath, $fileAkta);

			$vendorDocAkta->file = $fileAkta;
			$request->session()->put('vendor_document_akta', $vendorDocAkta);

			return redirect('/vms/guest/register/create-step8/'.$id);

		}

		public function suratUpload(Request $request, $id){
			$validatedData = $request->validate([
					'surat' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocSurat = new VendorDocument;

			$fSurat = $request->file('surat');
			$menus = "surat";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$name = VendorBusinessLicenses::where('id', $request->id_surat)->value('name_vendor_business_license');
			$fileSurat = $name."-".date('Ymds').$Vendor_data->id.".".$fSurat->getClientOriginalExtension();
			$data = $fSurat->move($destinationPath, $fileSurat);

			$vendorDocSurat->file = $fileSurat;
			$request->session()->push('vendor_document_surat.'.$request->id_surat, $vendorDocSurat);
			// $tes = $request->session()->push('user.teams', 'developers');

			return redirect('/vms/guest/register/create-step8/'.$id);
		}

		public function pemilikUpload(Request $request, $id){
			$validatedData = $request->validate([
					'pemilik' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocPemilik = new VendorDocument;

			$fPemilik = $request->file('pemilik');
			$menus = "pemilik";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$filePemilik = "PEMILIK-".$request->id_pemilik."-".date('Ymds').$Vendor_data->id.".".$fPemilik->getClientOriginalExtension();
			$data = $fPemilik->move($destinationPath, $filePemilik);

			$vendorDocPemilik->file = $filePemilik;
			$request->session()->push('vendor_document_pemilik.'.$request->id_pemilik, $vendorDocPemilik);

			return redirect('/vms/guest/register/create-step8/'.$id);
		}

		public function pengurusUpload(Request $request, $id){
			$validatedData = $request->validate([
					'pengurus' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocPengurus = new VendorDocument;

			$fPengurus = $request->file('pengurus');
			$menus = "pengurus";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$filePengurus = "PENGURUS-".$request->id_pengurus."-".date('Ymds').$Vendor_data->id.".".$fPengurus->getClientOriginalExtension();
			$data = $fPengurus->move($destinationPath, $filePengurus);

			$vendorDocPengurus->file = $filePengurus;
			$request->session()->push('vendor_document_pengurus.'.$request->id_pengurus, $vendorDocPengurus);

			return redirect('/vms/guest/register/create-step8/'.$id);
		}

		public function tenagaAhliUpload(Request $request, $id){
			$validatedData = $request->validate([
					'tenaga_ahli' => 'required|mimes:pdf|max:10000',
			]);

			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDocTa = new VendorDocument;

			$fTa = $request->file('tenaga_ahli');
			$menus = "tenaga_ahli";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileTa = "TENAGA_AHLI-".$request->id_tenaga_ahli."-".date('Ymds').$Vendor_data->id.".".$fTa->getClientOriginalExtension();
			$data = $fTa->move($destinationPath, $fileTa);

			$vendorDocTa->file = $fileTa;
			$request->session()->push('vendor_document_tenaga.'.$request->id_tenaga_ahli, $vendorDocTa);

			return redirect('/vms/guest/register/create-step8/'.$id);
		}

		public function review(Request $request, $id){
			$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			$vendorAdministrators = $request->session()->get('vendor_administrators');
			$vendorOwner = $request->session()->get('vendor_owner');
			$vendorDocLogo = $request->session()->get('vendor_document_logo');
			$vendorDocNpwp = $request->session()->get('vendor_document_npwp');
			$vendorDocAkta = $request->session()->get('vendor_document_akta');
			$vendorDocSurat = $request->session()->get('vendor_document_surat');
			$vendorDocPemilik = $request->session()->get('vendor_document_pemilik');
			$vendorDocPengurus = $request->session()->get('vendor_document_pengurus');
			$vendorDocTa = $request->session()->get('vendor_document_tenaga');
			$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();
			$users = User::where('activation_key', $id)->value('id');
			$idVendor = VendorDetail::where('id_user',$users)->value('id');
			$user = User::where('activation_key', $id)->first();
			$identitas = $request->session()->get('identitas');
			$vendorDeed = $request->session()->get('akta');
			$vendorLisensi = $request->session()->get('vendor_business_license_detail');
			$vendorClassification = $request->session()->get('vendor_classification');
			$klasifikasi = DB::table('classification')->where('status', '1')->get();
			$list_propinsi = Fungsi::propinsi();
			$list_kota = Fungsi::IndonesiaProvince();
			return view('vms.guest.register.review',compact('list_propinsi',$list_propinsi, 'list_kota',$list_kota,'klasifikasi',$klasifikasi,'vendorClassification',$vendorClassification,'vendorLisensi',$vendorLisensi,'vendorDeed',$vendorDeed,'user',$user,'identitas',$identitas,'vendorDocTa',$vendorDocTa,'vendorDocPengurus', $vendorDocPengurus,'vendorDocPemilik',$vendorDocPemilik,'vendorDocSurat',$vendorDocSurat,'vendorDocAkta',$vendorDocAkta,'vendorDocNpwp',$vendorDocNpwp,'idVendor', $idVendor,'vendorDocLogo',$vendorDocLogo,'vendorOwner', $vendorOwner, 'vendorAdministrators',$vendorAdministrators, 'vendorExpertStaff', $vendorExpertStaff, 'surat_perusahaan',$surat_perusahaan, 'vendorExpertStaff', $vendorExpertStaff, 'id', $id));
		}

	public function store(Request $request,$id){
		$identitas = $request->session()->get('identitas');
		$vendorDeed = $request->session()->get('akta');
		$vendorLisensi = $request->session()->get('vendor_business_license_detail');
		$vendorClassification = $request->session()->get('vendor_classification');
		$vendorOwner = $request->session()->get('vendor_owner');
		$vendorAdministrators = $request->session()->get('vendor_administrators');
		$vendorExpertStaff = $request->session()->get('vendor_expert_staff');

		$vendorDocLogo = $request->session()->get('vendor_document_logo');
		$vendorDocNpwp = $request->session()->get('vendor_document_npwp');
		$vendorDocAkta = $request->session()->get('vendor_document_akta');
		$vendorDocSurat = $request->session()->get('vendor_document_surat');
		$vendorDocPemilik = $request->session()->get('vendor_document_pemilik');
		$vendorDocPengurus = $request->session()->get('vendor_document_pengurus');
		$vendorDocTa = $request->session()->get('vendor_document_tenaga');

		$user = User::where('activation_key', $id)->first();
		$id_vendor = VendorDetail::where('id_user', $user->id)->value('id');


		$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
			['id_vendor_detail' => $id_vendor,
			'id_data' => NULL,
			'table' => '-',
			],
			[
				'file' => $vendorDocLogo->file,
				'status' => 1
			]);



		foreach ($vendorDocSurat as $key => $v) {
			foreach ($v as $k => $val) {
				$dt = explode('-',$val->file);
				$name = VendorBusinessLicenses::where('name_vendor_business_license', $dt[0])->value('id');
				$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
					['id_vendor_detail' => $id_vendor,
					'id_data' => $name,
					'table' => 'vendor_business_license_detail',
					],
					[
						'file' => $val->file,
						'status' => 1
					]);
			}
		}

		$vendorDtl = VendorDetail::where('id', $id_vendor)->update([
			'npwp' => $identitas->npwp,
			'address'  => $identitas->address,
			'province'  => $identitas->province,
			'city'  => $identitas->city,
			'post_code'  => $identitas->post_code,
			'username'  => $identitas->username,
			'pkp'  => $identitas->pkp,
			'telephone'  => $identitas->telephone,
			'fax'  => $identitas->fax,
			'mobile_phone'  => $identitas->mobile_phone,
			'email'  => $identitas->email,
			'website'  => $identitas->website,
			'branch_office'  => $identitas->branch_office,
			'bank'  => $identitas->bank,
			'no_rek'  => $identitas->no_rek
		]);

		$vendorAkta = VendorDeed::where('id_vendor_detail', $id_vendor)->updateOrCreate(
			['id_vendor_detail' => $id_vendor],
			[
			 'deed_number' => $vendorDeed->deed_number,
			 'date' => $vendorDeed->date,
			 'notary_public' => $vendorDeed->notary_public
		 	]
		);

		$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
			['id_vendor_detail' => $id_vendor,
			'id_data' => $vendorAkta->id,
			'table' => 'vendor_deed',
			],
			[
				'file' => $vendorDocAkta->file,
				'status' => 1
			]);


		foreach ($vendorLisensi as $key => $v) {
			$vendorSurat = VendorBusinessLicenseDetail::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				[
					'id_vendor_detail' => $id_vendor,
					'id_vendor_business_license' => $v->id_vendor_business_license
				],
				[
					'reference_number' => $v->reference_number,
					'giver_agency' => $v->giver_agency,
					'valid_until' => $v->valid_until,
					'start_date' => $v->start_date
				]
			);
		}
		foreach ($vendorClassification as $key => $v) {
			foreach($v->sub_classification as $k => $val){
				$vendorSurat = VendorClassification::where('id_vendor_detail', $id_vendor)->updateOrCreate(
					[
						'id_vendor_detail' => $id_vendor,
						'id_classification' => $v->id_classification[$k],
						'sub_classification' => $val,
						'qualification' => $v->qualification[$k],
						'no_letter' => $v->no_letter[$k],
						'expired_date' => $v->expired_date[$k],
					],
					[
						'status' => 1
					]
				);
			}
		}

		foreach ($vendorOwner as $key => $v) {
			$vendorOwner = VendorOwner::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				[
					'id_vendor_detail' => $id_vendor,
				],
				[
					'name_vendor_owner' => $v->name_vendor_owner,
					'ktp_vendor_owner' => $v->ktp_vendor_owner,
					'address_vendor_owner' => $v->address_vendor_owner,
				]
			);

			$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				['id_vendor_detail' => $id_vendor,
				'id_data' => $vendorOwner->id,
				'table' => 'vendor_owner',
				],
				[
					'file' => $vendorDocPemilik[$key][0]['file'],
					'status' => 1
				]);

		}

		foreach ($vendorAdministrators as $key => $v) {
			$vendorSurat = VendorAdministrators::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				[
					'id_vendor_detail' => $id_vendor,
					'ktp_name_vendor_administrators' => $v->address_name_vendor_administrators,
				],
				[
					'name_name_vendor_administrators' => $v->name_name_vendor_administrators,
					'address_name_vendor_administrators' => $v->position_name_vendor_administrators,
					'position_name_vendor_administrators' => $v->ktp_name_vendor_administrators,
				]
			);

			$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				['id_vendor_detail' => $id_vendor,
				'id_data' => $vendorSurat->id,
				'table' => 'vendor_administrators',
				],
				[
					'file' => $vendorDocPengurus[$key][0]['file'],
					'status' => 1
				]);

		}


		foreach ($vendorExpertStaff as $key => $v) {
			$vendorSA = VendorExpertStaff::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				[
					'id_vendor_detail' => $id_vendor,
					'name_vendor_expert_staff' => $v->name_vendor_expert_staff,
					'birth_vendor_expert_staff' => $v->birth_vendor_expert_staff,
					'address_vendor_expert_staff' => $v->address_vendor_expert_staff,
					'email_vendor_expert_staff' => $v->email_vendor_expert_staff,
				],
				[
					'gender_vendor_expert_staff' => $v->gender_vendor_expert_staff,
					'education_vendor_expert_staff' => $v->education_vendor_expert_staff,
					'nationallty_vendor_expert_staff' => $v->nationallty_vendor_expert_staff,
					'experience_vendor_expert_staff' => $v->experience_vendor_expert_staff,
					'expertise_vendor_expert_staff' => $v->expertise_vendor_expert_staff,
				]
			);
			$vendorDoc = VendorDocument::where('id_vendor_detail', $id_vendor)->updateOrCreate(
				['id_vendor_detail' => $id_vendor,
				'table' => 'vendor_expert_staff',
				'id_data' => $vendorSA->id
				],
				[
					'file' => $vendorDocTa[$key][0]['file'],
					'status' => 1
				]);
		}
		$status = VendorStatus::where('id_vendor_detail', $id_vendor)->updateOrCreate(
			['id_vendor_detail' => $id_vendor],
			['last_status' => 1]
		);

		$request->session()->forget('identitas');
		$request->session()->forget('akta');
		$request->session()->forget('vendor_business_license_detail');
		$request->session()->forget('vendor_classification');
		$request->session()->forget('vendor_owner');
		$request->session()->forget('vendor_administrators');
		$request->session()->forget('vendor_expert_staff');
		$request->session()->forget('vendor_document_logo');
		$request->session()->forget('vendor_document_npwp');
		$request->session()->forget('vendor_document_akta');
		$request->session()->forget('vendor_document_surat');
		$request->session()->forget('vendor_document_pemilik');
		$request->session()->forget('vendor_document_pengurus');
		$request->session()->forget('vendor_document_tenaga');
		return redirect('/vms/guest/register/done/');
	}

	public function done(){
		session()->flash('message','Terima Kasih!. Pengumuman panggilan untuk pengecekan dokumen akan segera dikirim via Email');
		return view('vms.guest.register.done');
	}
	protected function getKota(Request $request){
			$a = Fungsi::IndonesiaProvince();
			if (array_key_exists($request->id,$a)){
						$kota = $a[$request->id];
			}

			return response()->json([ 'kota' => $kota ]);
	}

	protected function classification(Request $request){
			$klasifikasi = DB::table('classification')->where('id', $request->id)->first();
			return response()->json([ 'status' => 1, 'klasifikasi' => $klasifikasi ]);
	}


	public function attend_invitation(Request $request, $id){
		$vendor = VendorDetail::where(DB::raw('md5(id)'),$id)->first();
		$status = VendorStatus::where('id_vendor_detail', $vendor->id)->updateOrCreate(
          [
            'id_vendor_detail' => $vendor->id,
          ],
          [
            'last_status' => 3
          ]);
		$statusinv = VendorInvitation::where('id_vendor_detail', $vendor->id)->updateOrCreate(
          [
            'id_vendor_detail' => $vendor->id,
          ],
          [
            'status' => 1
          ]);
					session()->flash('message','Terima Kasih!. Dimohon datang tepat waktu');
					return view('vms.guest.register.done');
	}

	public function data_changes(Request $request, $id){
		$vendor = VendorDetail::where(DB::raw('md5(id)'),$id)->first();
		$status = VendorStatus::where('id_vendor_detail', $vendor->id)->updateOrCreate(
          [
            'id_vendor_detail' => $vendor->id,
          ],
          [
            'last_status' => 3
          ]);
		$statusinv = VendorInvitation::where('id_vendor_detail', $vendor->id)->updateOrCreate(
          [
            'id_vendor_detail' => $vendor->id,
          ],
          [
            'status' => 1
          ]);
					session()->flash('message','Terima Kasih!. Dimohon datang tepat waktu');
					return view('vms.guest.register.done');
	}


	public function vendorRegis() {
		$propinsi = Fungsi::propinsi();
		return view('catalog.guest.vendor.create', compact('propinsi'));
	}

	public function vendorRegisStore(Request $request) {
		$validate = $request->validate([
							 'code' => 'required',
							 'vendor_name' => 'required',
							 'vendor_directur' => 'required',
							 'address' => 'required',
							 'province' => 'required',
							 'city' => 'required',
							 'telephone' => 'required',
							 'email' => 'required|email',
							 'password' => 'required',
							 'mobile_phone' => 'required',
							 'no_contract' => 'required',
							 'start_date' => 'required|date',
							 'end_date' => 'required|date',
							 'description' => 'required',
					 ]);

		 DB::beginTransaction();

		 try {

				 $users = User::create([
					 'name' => $request->vendor_name,
					 'username' => $request->vendor_name,
					 'email' => $request->email,
					 'password' => bcrypt($request->password),
					 'status' => '1'
				 ]);

				 $users->roles()->sync('4');
				 $users->roles()->sync('12');

				 $vendorDetail = VendorDetail::create([
					 'code' =>  $request->code,
					 'id_user'  =>  $users->id,
					 'vendor_name'  =>  $request->vendor_name,
					 'address'  =>  $request->address,
					 'province'  =>  $request->province,
					 'city'  =>  $request->city,
					 'telephone'  =>  $request->telephone,
					 'mobile_phone'  =>  $request->mobile_phone,
					 'email'  =>  $request->email,
					 'bank'  =>  $request->bank == null ? '-' : $request->bank,
					 'no_rek'  =>  $request->no_rek == null ? '-' : $request->no_rek,
					 'atas_nama'  =>  $request->on_behalft == null ? '-' : $request->on_behalft
				 ]);

				 $contract = Contract::create([
           'no_contract' => $request->no_contract,
           'id_vendor_detail' => $vendorDetail->id,
           'start_date' => $request->start_date,
           'end_date' => $request->end_date,
           'status' => 1,
           'description' => $request->description,
         ]);

				 // Mail::send('catalog.users.email.email_atasan', ['nama' => $request->vendor_name, 'pesan' => 'Silahkan melakukan login dengan email '.$request->email.' dengan password standar '.$request->password], function ($message) use ($request)
				 // {
					// 	 $message->subject('tes');
					// 	 $message->from('donotreply@kiddy.com', 'Kiddy');
					// 	 $message->to('ajie.darmawan106@gmail.com');
				 // });



				 $vendorDirectur = VendorDirectur::create(['id_vendor_detail' => $vendorDetail->id, 'name'  =>  $request->vendor_directur, 'nik'  =>  '-']);

				 $statusVendor = VendorStatus::create(['id_vendor_detail' =>  $vendorDetail->id, 'id_admin_vms' =>  '138', 'last_status' => '6']);

		 } catch (\Illuminate\Database\QueryException $e) {
			 DB::rollback();

			 dd($e->getMessage());
			 session()->flash('message', 'Data Gagal di Simpan');

		 }
		 DB::commit();
		 session()->flash('message', 'Data Berhasil di Simpan');
		return redirect('/');

	}

}
