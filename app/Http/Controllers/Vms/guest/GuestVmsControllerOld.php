<?php
namespace App\Http\Controllers\Vms\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\VendorDetail;
use App\Model\VendorAdministrators;
use App\Model\VendorBusinessLicenses;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorAmandementDeed;
use App\Model\VendorOwner;
use App\Model\VendorExpertStaff;
use App\Model\VendorDetailBranch;
use App\Model\VendorDocument;
use App\Model\VendorDocumentDetail;
use App\Model\VendorTools;
use App\Model\VendorToolsDetail;
use App\Model\VendorExperiences;
use App\Model\VendorExperiencesDetail;
use App\Model\VendorTax;
use App\Model\VendorDirectur;
use App\Model\VendorTaxDetail;
use App\Model\VendorClassification;
use App\Model\OrderProposer;
use App\Model\Chat;
use App\Model\Category;
use App\Model\CategoryItem;
use App\Model\Item;
use App\Model\Supplier;
use App\Model\Order;
use App\Model\GeneralSpec;
use App\Model\CategorySpec;
use App\Model\ItemSpec;
use App\Model\ItemImage;
use App\Model\ItemPriceHistory;
use App\Model\ItemDoc;
use App\Model\OrderDetail;
use App\Model\DeliveryOrder;
use App\Model\DeliveryOrderDetail;
use App\Model\Invoice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use \PDF;
use File;
use Fungsi;
use Session;
use DB;
use Excel;
use App\User;

class GuestVmsControllerOld extends Controller
{
	public function registrationByNoRegis(Request $request, $id) {
		$user = User::where('activation_key', $id)->first();
		$userDetail = VendorDetail::where('id_user', $user->id)->first();
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$propinsi = Fungsi::propinsi();
		$list_propinsi = Fungsi::propinsi();
		$list_kota = Fungsi::IndonesiaProvince();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();
		return view('vms.guest.register.create-step1',compact('userDetail', $userDetail,'user',$user, 'list_propinsi', $list_propinsi, 'list_kota', $list_kota, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}


	public function postCreateStep1(Request $request, $id){
		$validatedData = $request->validate([
									'npwp' => 'required|numeric',
									'address' => 'required|string',
									'province' => 'required|string',
									'city' => 'required|string',
									'post_code' => 'required|numeric',
									'username' => 'required|string',
									'pkp' => 'required|numeric',
									'telephone' => 'required|string',
									'fax' => 'required|string',
									'mobile_phone' => 'required|string',
									'website' => 'required|string',
									'branch_office' => 'required|string',
			 ]);
			 $arrBranch = array('address' => $request->addresscentral, 'mobile_phone'  => $request->hpcentral, 'telephone'  => $request->telephonecentral, 'fax'  => $request->faxcentral);
						if((empty($request->session()->get('identitas'))) && (empty($request->session()->get('identitasBranch')))){
								$vendorDetail = new VendorDetail();
								$vendorDetail->fill($validatedData);
								$request->session()->put('identitas', $vendorDetail);
								$vendorDetailBranch = new VendorDetailBranch();
								$vendorDetailBranch->fill($arrBranch);
								$request->session()->put('identitasBranch', $vendorDetailBranch);
						}else{
								$vendorDetail = $request->session()->get('identitas');
								$vendorDetail->fill($validatedData);
								$request->session()->put('identitas', $vendorDetail);
								$vendorDetailBranch = $request->session()->get('identitasBranch');
								$vendorDetailBranch->fill($arrBranch);
								$request->session()->put('identitasBranch', $vendorDetailBranch);
						}
						return redirect('/vms/guest/register/create-step2/'.$id);
	}

	public function createStep2(Request $request, $id){
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$propinsi = Fungsi::propinsi();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step2',compact('id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}

	public function postCreateStep2(Request $request, $id){
		$validatedData = $request->validate([
									'deed_number' => 'required|numeric',
									'date' => 'required|date',
									'notary_public' => 'required|string',
			 ]);
						if(empty($request->session()->get('akta'))){
								$vendorDeed = new VendorDeed();
								$vendorDeed->fill($validatedData);
								$request->session()->put('akta', $vendorDeed);
						}else{
								$vendorDeed = $request->session()->get('akta');
								$vendorDeed->fill($validatedData);
								$request->session()->put('akta', $vendorDeed);
						}

						return redirect('/vms/guest/register/create-step3/'.$id);
	}

	public function createStep3(Request $request, $id){
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$vendorLisensi = $request->session()->get('vendor_business_license_detail');
		$propinsi = Fungsi::propinsi();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step3',compact('vendorLisensi', $vendorLisensi,'id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}


	public function postCreateStep3(Request $request, $id){
		$validatedData = $request->validate([
			'reference_number' => 'required|array',
			'valid_until' => 'required|array',
			'start_date' => 'required|array',
			'giver_agency' => 'required|array'
		]);
		$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');
		// $file = $request->file('lisensi_id');
		// $menus = "surat";
		// $tgl = date("Ymd");
		// $users = User::where('activation_key', $id)->value('id');
		// $Vendor_data = VendorDetail::where('id_user', $users)->first();
		// $destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
		// if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
		//
		// foreach ($file as $key => $v) {
		// 	$name = VendorBusinessLicenses::where('id', $key)->value('name_vendor_business_license');
		// 		$fileName = $name.date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
		// 		$data = $v->move($destinationPath, $fileName);
		// }

		// $arr = array();
		if(empty($request->session()->get('vendor_business_license_detail'))){
			foreach ($request->reference_number as $key => $value) {
				$input = [
					'id_vendor_business_license' => $key,
					'id_vendor_detail' => $id_vendor_detail,
					'reference_number' => $request->reference_number[$key],
					'giver_agency' => $request->giver_agency[$key],
					'valid_until' => $request->valid_until[$key],
					'start_date' => $request->start_date[$key]
				];
				$vendorLisensi = new VendorBusinessLicenseDetail;
				$vendorLisensi->fill($input);
				$request->session()->push('vendor_business_license_detail', $vendorLisensi);
			}
		}else{
			$vendorLisensi = $request->session()->get('vendor_business_license_detail');
			if ($request->session()->has('vendor_business_license_detail')) {
					$request->session()->forget('vendor_business_license_detail');
			}

			foreach ($request->reference_number as $key => $value) {
				$input = [
					'id_vendor_business_license' => $key,
					'id_vendor_detail' => $id_vendor_detail,
					'reference_number' => $request->reference_number[$key],
					'giver_agency' => $request->giver_agency[$key],
					'valid_until' => $request->valid_until[$key],
					'start_date' => $request->start_date[$key]
				];
				$vendorLisensi = new VendorBusinessLicenseDetail;
				$vendorLisensi->fill($input);
				$request->session()->push('vendor_business_license_detail', $vendorLisensi);
			}

		}
		return redirect('/vms/guest/register/create-step4/'.$id);
	}

	public function createStep4(Request $request, $id){
		$identitas = $request->session()->get('identitas');
		$identitasBranch = $request->session()->get('identitasBranch');
		$vendorDeed = $request->session()->get('akta');
		$vendorLisensi = $request->session()->get('vendor_business_license_detail');
		$vendorClassification = $request->session()->get('vendor_classification');
		$propinsi = Fungsi::propinsi();
		$klasifikasi = DB::table('classification')->where('status', '1')->get();
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step4',compact('vendorClassification' ,$vendorClassification, 'klasifikasi',$klasifikasi,'vendorLisensi', $vendorLisensi, 'id', $id, 'surat_perusahaan',$surat_perusahaan, 'vendorDeed', $vendorDeed, 'identitasBranch', $identitasBranch, 'identitas', $identitas, 'propinsi', $propinsi, 'surat_perusahaan', $surat_perusahaan));
	}

	public function postCreateStep4(Request $request, $id){
		$validatedData = $request->validate([
									'id_classification' => 'required|array',
									'qualification' => 'required|array',
									'no_letter' => 'required|array',
									'expired_date' => 'required|array',
									'sub_classification' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			// foreach ($request->id_classification as $key => $value) {
			// 	print_r($request->sub_classification[$key]);
			// }
			// dd($request->all());

			 if(empty($request->session()->get('vendor_classification'))){
			 	foreach ($request->id_classification as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'id_classification' => $request->id_classification[$key],
			 			'sub_classification' => $request->sub_classification[$key],
			 			'qualification' => $request->qualification[$key],
			 			'no_letter' => $request->no_letter[$key],
			 			'expired_date' => $request->expired_date[$key],
						'status' => '1'
			 		];
			 		$vendorClassification = new VendorClassification;
			 		$vendorClassification->fill($input);
			 		$request->session()->push('vendor_classification', $vendorClassification);
			 	}
			 }else{
			 	$vendorClassification = $request->session()->get('vendor_classification');
			 	if ($request->session()->has('vendor_classification')) {
			 			$request->session()->forget('vendor_classification');
			 	}

			 	foreach ($request->id_classification as $key => $value) {
					$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'id_classification' => $request->id_classification[$key],
						'sub_classification' => $request->sub_classification[$key],
			 			'qualification' => $request->qualification[$key],
			 			'no_letter' => $request->no_letter[$key],
			 			'expired_date' => $request->expired_date[$key],
						'status' => '1'
			 		];
			 		$vendorClassification = new VendorClassification;
			 		$vendorClassification->fill($input);
			 		$request->session()->push('vendor_classification', $vendorClassification);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step5/'.$id);
	}

	public function createStep5(Request $request, $id){
		$vendorOwner = $request->session()->get('vendor_owner');
		return view('vms.guest.register.create-step5',compact('vendorOwner', $vendorOwner, 'id', $id));
	}

	public function postCreateStep5(Request $request, $id){
		$validatedData = $request->validate([
									'name_vendor_owner' => 'required|array',
									'ktp_vendor_owner' => 'required|array',
									'address_vendor_owner' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_owner'))){
			 	foreach ($request->name_vendor_owner as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_owner' => $request->name_vendor_owner[$key],
			 			'ktp_vendor_owner' => $request->ktp_vendor_owner[$key],
			 			'address_vendor_owner' => $request->address_vendor_owner[$key],
			 		];
			 		$vendorOwner = new VendorOwner;
			 		$vendorOwner->fill($input);
			 		$request->session()->push('vendor_owner', $vendorOwner);
			 	}
			 }else{
			 	$vendorOwner = $request->session()->get('vendor_owner');
			 	if ($request->session()->has('vendor_owner')) {
			 			$request->session()->forget('vendor_owner');
			 	}

			 	foreach ($request->name_vendor_owner as $key => $value) {
					$input = [
						'id_vendor_detail' => $id_vendor_detail,
						'name_vendor_owner' => $request->name_vendor_owner[$key],
						'ktp_vendor_owner' => $request->ktp_vendor_owner[$key],
						'address_vendor_owner' => $request->address_vendor_owner[$key],
					];
					$vendorOwner = new VendorOwner;
			 		$vendorOwner->fill($input);
			 		$request->session()->push('vendor_owner', $vendorOwner);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step6/'.$id);
	}

	public function createStep6(Request $request, $id){
		$vendorAdministrators = $request->session()->get('vendor_administrators');
		return view('vms.guest.register.create-step6',compact('vendorAdministrators', $vendorAdministrators, 'id', $id));
	}

	public function postCreateStep6(Request $request, $id){
		$validatedData = $request->validate([
									'name_name_vendor_administrators' => 'required|array',
									'ktp_name_vendor_administrators' => 'required|array',
									'address_name_vendor_administrators' => 'required|array',
									'position_name_vendor_administrators' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_administrators'))){
			 	foreach ($request->name_name_vendor_administrators as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_name_vendor_administrators' => $request->name_name_vendor_administrators[$key],
			 			'ktp_name_vendor_administrators' => $request->ktp_name_vendor_administrators[$key],
			 			'address_name_vendor_administrators' => $request->address_name_vendor_administrators[$key],
			 			'position_name_vendor_administrators' => $request->position_name_vendor_administrators[$key],
			 		];
			 		$vendorAdministrators = new VendorAdministrators;
			 		$vendorAdministrators->fill($input);
			 		$request->session()->push('vendor_administrators', $vendorAdministrators);
			 	}
			 }else{
			 	$vendorAdministrators = $request->session()->get('vendor_administrators');
			 	if ($request->session()->has('vendor_administrators')) {
			 			$request->session()->forget('vendor_administrators');
			 	}

			 	foreach ($request->name_name_vendor_administrators as $key => $value) {
					$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_name_vendor_administrators' => $request->name_name_vendor_administrators[$key],
			 			'ktp_name_vendor_administrators' => $request->ktp_name_vendor_administrators[$key],
			 			'address_name_vendor_administrators' => $request->address_name_vendor_administrators[$key],
			 			'position_name_vendor_administrators' => $request->position_name_vendor_administrators[$key],
			 		];
					$vendorAdministrators = new VendorAdministrators;
			 		$vendorAdministrators->fill($input);
			 		$request->session()->push('vendor_administrators', $vendorAdministrators);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step7/'.$id);
	}

	public function createStep7(Request $request, $id){
		$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
		return view('vms.guest.register.create-step7',compact('vendorExpertStaff', $vendorExpertStaff, 'id', $id));
	}

	public function postCreateStep7(Request $request, $id){
		$validatedData = $request->validate([
									'name_vendor_expert_staff' => 'required|array',
									'birth_vendor_expert_staff' => 'required|array',
									'address_vendor_expert_staff' => 'required|array',
									'gender_vendor_expert_staff' => 'required|array',
									'education_vendor_expert_staff' => 'required|array',
									'nationallty_vendor_expert_staff' => 'required|array',
									'experience_vendor_expert_staff' => 'required|array',
									'email_vendor_expert_staff' => 'required|array',
									'expertise_vendor_expert_staff' => 'required|array',
			 ]);

	 $id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
											->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
											->where('users.activation_key', $id)
											->value('vendor_detail.id');

			 if(empty($request->session()->get('vendor_expert_staff'))){
			 	foreach ($request->name_vendor_expert_staff as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_expert_staff' => $request->name_vendor_expert_staff[$key],
			 			'birth_vendor_expert_staff' => $request->birth_vendor_expert_staff[$key],
			 			'address_vendor_expert_staff' => $request->address_vendor_expert_staff[$key],
			 			'gender_vendor_expert_staff' => $request->gender_vendor_expert_staff[$key],
			 			'education_vendor_expert_staff' => $request->education_vendor_expert_staff[$key],
			 			'nationallty_vendor_expert_staff' => $request->nationallty_vendor_expert_staff[$key],
			 			'experience_vendor_expert_staff' => $request->experience_vendor_expert_staff[$key],
			 			'email_vendor_expert_staff' => $request->email_vendor_expert_staff[$key],
			 			'expertise_vendor_expert_staff' => $request->expertise_vendor_expert_staff[$key],
			 		];
			 		$vendorExpertStaff = new VendorExpertStaff;
			 		$vendorExpertStaff->fill($input);
			 		$request->session()->push('vendor_expert_staff', $vendorExpertStaff);
			 	}
			 }else{
			 	$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			 	if ($request->session()->has('vendor_expert_staff')) {
			 			$request->session()->forget('vendor_expert_staff');
			 	}

				foreach ($request->name_vendor_expert_staff as $key => $value) {
			 		$input = [
			 			'id_vendor_detail' => $id_vendor_detail,
			 			'name_vendor_expert_staff' => $request->name_vendor_expert_staff[$key],
			 			'birth_vendor_expert_staff' => $request->birth_vendor_expert_staff[$key],
			 			'address_vendor_expert_staff' => $request->address_vendor_expert_staff[$key],
			 			'gender_vendor_expert_staff' => $request->gender_vendor_expert_staff[$key],
			 			'education_vendor_expert_staff' => $request->education_vendor_expert_staff[$key],
			 			'nationallty_vendor_expert_staff' => $request->nationallty_vendor_expert_staff[$key],
			 			'experience_vendor_expert_staff' => $request->experience_vendor_expert_staff[$key],
			 			'email_vendor_expert_staff' => $request->email_vendor_expert_staff[$key],
			 			'expertise_vendor_expert_staff' => $request->expertise_vendor_expert_staff[$key],
			 		];
					$vendorExpertStaff = new VendorExpertStaff;
			 		$vendorExpertStaff->fill($input);
			 		$request->session()->push('vendor_expert_staff', $vendorExpertStaff);
			 	}

			 }
			 return redirect('/vms/guest/register/create-step8/'.$id);
	}


		public function createStep8(Request $request, $id){
			$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
			$vendorAdministrators = $request->session()->get('vendor_administrators');
			$vendorOwner = $request->session()->get('vendor_owner');
			$vendorDoc = $request->session()->get('vendor_document');
			// dd($vendorDoc);
			$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();
			$users = User::where('activation_key', $id)->value('id');
			$idVendor = VendorDetail::where('id_user',$users)->value('id');

			return view('vms.guest.register.create-step8',compact('idVendor', $idVendor,'vendorDoc',$vendorDoc,'vendorOwner', $vendorOwner, 'vendorAdministrators',$vendorAdministrators, 'vendorExpertStaff', $vendorExpertStaff, 'surat_perusahaan',$surat_perusahaan, 'vendorExpertStaff', $vendorExpertStaff, 'id', $id));
		}

		public function postCreateStep8(Request $request, $id){
			// $validatedData = $request->validate([
			// 	'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			// 	'npwp' => 'required|mimes:pdf|max:10000',
			// 	'akta' => 'required|mimes:pdf|max:10000',
			// 	'surat' => 'required|array',
			// 	'pemilik' => 'required|array',
			// 	'pengurus' => 'required|array',
			// 	'tenaga_ahli' => 'required|array',
			// ]);



			$id_vendor_detail = DB::table('users')->select('vendor_detail.id AS id_vendor_detail')
			->join('vendor_detail', 'users.id','=','vendor_detail.id_user')
			->where('users.activation_key', $id)
			->value('vendor_detail.id');

			$users = User::where('activation_key', $id)->value('id');
			$Vendor_data = VendorDetail::where('id_user', $users)->first();
			$vendorDoc = new VendorDocument;
			$fileTa = array();
			$filePengurus = array();
			$filePemilik = array();
			$fileName = array();

			// dd($vendorDoc);
###########################################LOGO######################################################################
			$fLogo = $request->file('logo');
			$menus = "logo";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileLogo = "LOGO-".date('Ymds').$Vendor_data->id.".".$fLogo->getClientOriginalExtension();
			$data = $fLogo->move($destinationPath, $fileLogo);
			// $vendorDoc->file = ;
			// $request->session()->push('vendor_document', $vendorDoc);
###########################################NPWP######################################################################
			$fNpwp = $request->file('npwp');
			$menus = "npwp";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileNpwp = "NPWP-".date('Ymds').$Vendor_data->id.".".$fNpwp->getClientOriginalExtension();
			$data = $fNpwp->move($destinationPath, $fileNpwp);
###########################################Akta######################################################################
			$fAkta = $request->file('akta');
			$menus = "akta";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
			$fileAkta = "AKTA-".date('Ymds').$Vendor_data->id.".".$fAkta->getClientOriginalExtension();
			$data = $fAkta->move($destinationPath, $fileAkta);

##################################################################################################################
	// foreach($dataFile as $kDF => $vKDF){
		// $vendorDoc->file = $dataFile;
		// $request->session()->push('vendor_document', $vendorDoc);
		// echo "<pre>";
		// print_r($dataFile);
		// echo "</pre>";
	// }
	// die();
###############################################Surat###################################################################
			$fSurat = $request->file('surat');
			$menus = "surat";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);

			foreach ($fSurat as $key => $v) {
				$name = VendorBusinessLicenses::where('id', $key)->value('name_vendor_business_license');
					$fileName[] = $name."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$fileNames = $name."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$data = $v->move($destinationPath, $fileNames);
					// $vendorDoc->file = $fileName;
			}
###############################################Pemilik###################################################################
			$fPemilik = $request->file('pemilik');
			$menus = "pemilik";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);

			foreach ($fPemilik as $key => $v) {
					$filePemilik[] = "PEMILIK-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$filePemiliks = "PEMILIK-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$data = $v->move($destinationPath, $filePemiliks);
					// $vendorDoc->file = $filePemilik;
			}
###############################################Surat###################################################################
			$fPengurus = $request->file('pengurus');
			$menus = "pengurus";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);

			foreach ($fPengurus as $key => $v) {
					$filePengurus[] ="PENGURUS-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$filePenguruss ="PENGURUS-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$data = $v->move($destinationPath, $filePenguruss);
					// $vendorDoc->file = $filePengurus;
			}
###############################################Surat###################################################################
			$fTenagaAhli = $request->file('tenaga_ahli');
			$menus = "tenaga_ahli";
			$tgl = date("Ymd");
			$destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
			if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);

			foreach ($fTenagaAhli as $key => $v) {
					$fileTa[] = "TENAGA_AHLI-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$fileTas = "TENAGA_AHLI-".$key."-".date('Ymds').$Vendor_data->id.".".$v->getClientOriginalExtension();
					$data = $v->move($destinationPath, $fileTas);
					// $vendorDoc->file = $fileTa;
			}
##################################################################################################################
$vendorDoc1 = $request->session()->get('vendor_document');

			$dataFile = [$fileLogo,$fileNpwp,$fileAkta];
			$Alldata1 = array_merge($fileName,$dataFile);
			$Alldata2 = array_merge($Alldata1,$filePemilik);
			$Alldata3 = array_merge($Alldata2,$filePengurus);
			$Alldata4 = array_merge($Alldata3,$fileTa);
			$vendorDoc->file = $Alldata4;
			$request->session()->push('vendor_document', $vendorDoc);

			// dd($Alldata4);
		return redirect('/vms/guest/register/review/'.$id);
	}

	public function review(Request $request, $id){
		dd("halaman review");
		$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
		$vendorExpertStaff = $request->session()->get('vendor_expert_staff');
		$vendorAdministrators = $request->session()->get('vendor_administrators');
		$vendorOwner = $request->session()->get('vendor_owner');
		$vendorDoc = $request->session()->get('vendor_document');
		// dd($vendorDoc);
		$surat_perusahaan = VendorBusinessLicenses::where('status','Y')->get();

		return view('vms.guest.register.create-step8',compact('vendorDoc',$vendorDoc,'vendorOwner', $vendorOwner, 'vendorAdministrators',$vendorAdministrators, 'vendorExpertStaff', $vendorExpertStaff, 'surat_perusahaan',$surat_perusahaan, 'vendorExpertStaff', $vendorExpertStaff, 'id', $id));
	}


	protected function getKota(Request $request)
	{
			$a = Fungsi::IndonesiaProvince();
			if (array_key_exists($request->id,$a)){
						$kota = $a[$request->id];
			}

			return response()->json([ 'kota' => $kota ]);
	}

	protected function classification(Request $request)
	{
			$klasifikasi = DB::table('classification')->where('id', $request->id)->first();
			return response()->json([ 'status' => 1, 'klasifikasi' => $klasifikasi ]);
	}

}
