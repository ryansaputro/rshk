<?php

namespace App\Http\Controllers\Vms\guest\activation;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;

class ActivationController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $controller = "activation";   

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function store(Request $request)
    {
        $activation_key = $request->activation_key;
        $user = User::where('activation_key', $activation_key)->where('status','0');
        if($user->count() == 0){
            session()->flash('message','Maaf kode Aktivasi anda salah');
            return redirect('activation');           
        }else{
            $updateUser = User::find($user->value('id'));
            $updateUser->status = "1";
            $updateUser->save();
            
            session()->flash('message','Akun telah teraktivasi');
            return redirect('auth');           

        }

    }

    protected function index()
    {
        return view('vms.guest.activation.index');
    }

}
