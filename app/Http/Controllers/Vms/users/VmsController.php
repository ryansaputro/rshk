<?php
namespace App\Http\Controllers\Vms\users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\VendorDetail;
use App\Model\VendorAdministrators;
use App\Model\VendorBusinessLicenses;
use App\Model\VendorBusinessLicenseDetail;
use App\Model\VendorDeed;
use App\Model\VendorAmandementDeed;
use App\Model\VendorOwner;
use App\Model\VendorExpertStaff;
use App\Model\VendorDetailBranch;
use App\Model\VendorDocument;
use App\Model\VendorDocumentDetail;
use App\Model\VendorTools;
use App\Model\VendorToolsDetail;
use App\Model\VendorExperiences;
use App\Model\VendorExperiencesDetail;
use App\Model\VendorTax;
use App\Model\VendorDirectur;
use App\Model\VendorTaxDetail;
use App\Model\OrderProposer;
use App\Model\Chat;
use App\Model\Category;
use App\Model\CategoryItem;
use App\Model\Item;
use App\Model\Supplier;
use App\Model\Order;
use App\Model\GeneralSpec;
use App\Model\CategorySpec;
use App\Model\ItemSpec;
use App\Model\ItemImage;
use App\Model\ItemPriceHistory;
use App\Model\ItemDoc;
use App\Model\OrderDetail;
use App\Model\DeliveryOrder;
use App\Model\DeliveryOrderDetail;
use App\Model\Invoice;
use App\Model\VendorDataChanges;
use App\Model\ItemReject;
use App\Model\Contract;
use App\Model\CategoryWarehouseType;
use App\Model\Pphp;
use App\Model\UnitType;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use \PDF;
use File;
use Fungsi;
use DB;
use Excel;

class VmsController extends Controller
{

	public function __construct()
    {
        $this->middleware('vendorauth');
    }

    public function index($id){
        if (Auth::check()) {
        	$id_user = Auth::user()->id;
        	$vendor = VendorDetail::where('id_user', $id_user)->firstOrFail();
        	$id_vendor = VendorDetail::where('id_user', $id_user)->value('id');
          $pengurus = VendorAdministrators::where('id_vendor_detail', $id_vendor)->get();
        	$dataSurat = DB::table('vendor_business_license')
                    ->select('vendor_business_license.name_vendor_business_license','vendor_business_license.id AS id_license','vendor_business_license_detail.*')
                    ->join('vendor_business_license_detail','vendor_business_license.id','=','vendor_business_license_detail.id_vendor_business_license')
                    ->where('vendor_business_license.status','Y')
                    ->where('id_vendor_detail', $id_vendor);
            $surat =  $dataSurat->get();
            $Arraysurat =  $dataSurat->pluck('vendor_business_license.id_license')->toArray();
            $Dataakta = VendorDeed::where('id_vendor_detail', $id_vendor);
                $akta = $Dataakta->get();
                $id_akta = $Dataakta->value('id');
            $aktaBaru = VendorAmandementDeed::where('id_vendor_deed', $id_akta)->get();
            $pemilik = VendorOwner::where('id_vendor_detail', $id_vendor)->get();
            $staff = VendorExpertStaff::where('id_vendor_detail', $id_vendor)->get();
            $dataVendor = VendorDetailBranch::where('id_vendor_detail', $id_vendor);
            $vendorTot = $dataVendor->count();
            $tools = DB::table('vendor_tools')
                        ->select('vendor_tools.id AS id_vendor_tools','vendor_tools.id_vendor_detail','vendor_tools_detail.*')
                        ->join('vendor_tools_detail','vendor_tools.id','=','vendor_tools_detail.id_vendor_tool')
                        ->where('id_vendor_detail', $id_vendor)
                        ->paginate(10);
            $experiences = DB::table('vendor_experiences')
                        ->select('vendor_experiences.id AS id_vendor_experience', 'vendor_experiences.id_vendor_detail', 'vendor_experiences_detail.*')
                        ->join('vendor_experiences_detail','vendor_experiences.id', '=','vendor_experiences_detail.id_vendor_experience')
                        ->where('id_vendor_detail', $id_vendor)
                        ->paginate(10);
            $tax = VendorTax::where('id_vendor_detail',$id_vendor)->where('status','Y')->count();
            $list_tax =  DB::table('vendor_tax')
                        ->select('vendor_tax.id AS id_vendor_tax', 'vendor_tax.id_vendor_detail', 'vendor_tax.npwp', 'vendor_tax.npwp_scan', 'vendor_tax_detail.*')
                        ->join('vendor_tax_detail','vendor_tax.id', '=','vendor_tax_detail.id_vendor_tax')
                        ->where('id_vendor_detail', $id_vendor)
                        ->paginate(10);
            if($vendorTot >= 1){
                $branch_office_detail = $dataVendor->firstOrFail();
            }
            $vendor_business_license = VendorBusinessLicenses::where('status','Y')->get();
            // $doc = DB::table('vendor_document')
            //         ->select('vendor_document.id', 'vendor_document.id_vendor_detail', 'vendor_document.menu', 'vendor_document_detail.id_document', 'vendor_document_detail.file', 'vendor_document_detail.status', 'vendor_document_detail.created_at', 'vendor_document_detail.updated_at')
            //         ->join('vendor_document_detail','vendor_document.id','=','vendor_document_detail.id_document')
            //         ->paginate(5);

        	$list_propinsi = Fungsi::propinsi();
          $list_kota = Fungsi::IndonesiaProvince();
        	$menu = Fungsi::menu();

            $vendor_detail_x = DB::table('vendor_detail')
                                ->select('vendor_detail.id AS id_vendor','vendor_detail.vendor_name','vendor_detail.created_at','vendor_detail.address','vendor_detail.telephone','users.id AS id_user_vendor', 'users.name', 'users.username', 'users.email', 'users.status AS status_vendor', 'vendor_status.last_status' )
                                ->join('users','vendor_detail.id_user', '=', 'users.id')
                                ->leftJoin('vendor_status','vendor_detail.id', '=', 'vendor_status.id_vendor_detail');
            $vendor_detail_aktif = $vendor_detail_x->where('users.status',1)->where('users.id', Auth::user()->id)->paginate(10);
            $vendor_detail_lulus = $vendor_detail_x->where('users.status',1)->where('users.id', Auth::user()->id)->where('vendor_status.last_status','1')->paginate(10);

            return view('vms.users.dashboard.index', compact('vendorDataChanges', 'vendor','list_propinsi','list_kota','pengurus','surat','akta','aktaBaru','pemilik','staff','branch_office_detail','vendor_business_license','Arraysurat','menu','vendor_detail_lulus','tools', 'experiences','tax','list_tax'))->with('id', $id);
        }else{
            return redirect('auth');
        }
    }

    public function update(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',$request->id_user)->firstOrFail();
        $vendor_detail->pkp = $request->pkp;
        $vendor_detail->telephone = $request->tlp;
        $vendor_detail->fax = $request->fax;
        $vendor_detail->mobile_phone = $request->hp;
        $vendor_detail->website = $request->web;
        $vendor_detail->branch_office = $request->branch_office;
        $vendor_detail->save();

        $addresscentral = ($request->addresscentral != null) ? $request->addresscentral : 'xxx';
        $telephonecentral = ($request->telephonecentral != null) ? $request->telephonecentral : 'xxx';


        $faxcentral = ($request->faxcentral != null) ? $request->faxcentral : 'xxx';
        $hpcentral = ($request->hpcentral != null) ? $request->hpcentral : 'xxx';

        if(($addresscentral !== 'xxx') && ($telephonecentral !== 'xxx') && ($faxcentral !== 'xxx') && ($hpcentral !== 'xxx')) {
            $vendor_detail_branch = VendorDetailBranch::updateOrCreate([
                'id_vendor_detail'=> $vendor_detail->id],[
                'address'=> $addresscentral,
                'telephone'=> $telephonecentral,
                'fax'=> $faxcentral,
                'mobile_phone'=> $hpcentral
            ]);
        }

        $branch = VendorDetailBranch::where('id_vendor_detail',$vendor_detail->id)->count();

        if($branch >=1){
            if($vendor_detail->branch_office == 0){
                $vendor_detail_branch = VendorDetailBranch::where('id_vendor_detail', $vendor_detail->id);
                $vendor_detail_branch->delete();
            }
        }


        return response()->json(array(
                            'email' => $request->email,
                            'pkp' => $request->pkp,
                            'telephone' => $request->tlp,
                            'fax' => $request->fax,
                            'mobile_phone' => $request->hp,
                            'website' => $request->web,
                            'status' => 1));
    }

    public function update_izin_usaha(Request $request) {
        $id_user = $request->id_user;
        $id_bussines_license = $request->id_bussines_license;
        $reference_number = $request->reference_number;
        $valid_until = $request->valid_until;
        $giver_agency = $request->giver_agency;
        $qualification = $request->qualification;
        $classification = $request->classification;

        $vendor = VendorDetail::where('id_user', $id_user)->firstOrFail();
        $vendor_business_license_detail = VendorBusinessLicenseDetail::updateOrCreate([
                'id_vendor_detail'=> $vendor->id,
                'id_vendor_business_license'=> $id_bussines_license],[
                'reference_number' => $reference_number,
                'valid_until' => $valid_until,
                'giver_agency' => $giver_agency,
                'qualification' => $qualification,
                'classification' => $classification,
            ]);

        return response()->json(array(
                            'reference_number' => $reference_number,
                            'valid_until' => $valid_until,
                            'giver_agency' => $giver_agency,
                            'qualification' => $qualification,
                            'classification' => $classification,
                            'status' => 1));

    }

    public function delete_izin_usaha(Request $request) {
        $id_user = $request->id_user;
        $id_bussines_license = $request->id_bussines_license;

        $vendor = VendorDetail::where('id_user', $id_user)->firstOrFail();
        $vendor_business_license_detail = VendorBusinessLicenseDetail::where('id_vendor_detail', $vendor->id)->where('id_vendor_business_license', $id_bussines_license);
        $delete = $vendor_business_license_detail->delete();

        return response()->json(array(
                            'id_bussines_license' => $id_bussines_license,
                            'status' => 1));

    }

    public function tambahan_izin_usaha(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',$request->id_user)->firstOrFail();
        $vendor_business_license_detail = VendorBusinessLicenseDetail::create([
            'id_vendor_business_license' => $request->id_bussines_license,
            'id_vendor_detail' => $vendor_detail->id,
            'reference_number' =>$request->reference_number,
            'giver_agency' =>$request->giver_agency,
            'valid_until' =>$request->valid_until,
            'qualification' =>$request->qualification,
            'classification' => $request->classification
        ]);

        return response()->json(array(
                            'reference_number' => $request->reference_number,
                            'valid_until' => $request->valid_until,
                            'giver_agency' => $request->giver_agency,
                            'qualification' => $request->qualification,
                            'classification' => $request->classification,
                            'status' => 1));
    }

    public function delete_akta(Request $request) {
        $vendor_deed = VendorDeed::find($request->id_deed)->delete();
        return response()->json(array(
                            'status' => 1));
    }

    public function delete_akta_pembaruan(Request $request) {
        $vendor_deed = VendorAmandementDeed::find($request->id_deed)->delete();
        return response()->json(array(
                            'status' => 1));
    }


    public function create_akta(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',$request->id_user)->firstOrFail();
        $vendor_deed = VendorDeed::create([
            'id_vendor_detail' => $vendor_detail->id,
            'deed_number' => $request->deed_number,
            'date' => $request->date,
            'notary_public' => $request->notary_public
        ]);

        return response()->json(array(
                            'deed_number' => $request->deed_number,
                            'date' => $request->date,
                            'notary_public' => $request->notary_public,
                            'status' => 1));
    }

    public function update_akta(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',$request->id_user)->firstOrFail();
        $vendor_deed = VendorDeed::where('id_vendor_detail',$vendor_detail->id)->update([
            'deed_number' => $request->deed_number,
            'date' => $request->date,
            'notary_public' => $request->notary_public
        ]);

        return response()->json(array(
                            'deed_number' => $request->deed_number,
                            'date' => $request->date,
                            'notary_public' => $request->notary_public,
                            'status' => 1));
    }

    public function update_akta_pembaruan(Request $request) {
        $vendor_deed = VendorAmandementDeed::where('id_vendor_deed',$request->id_vendor_deed)->update([
            'deed_number' => $request->deed_number,
            'date' => $request->date,
            'notary_public' => $request->notary_public
        ]);

        return response()->json(array(
                            'deed_number' => $request->deed_number,
                            'date' => $request->date,
                            'notary_public' => $request->notary_public,
                            'status' => 1));
    }

    public function create_akta_pembaruan(Request $request) {
        $vendor_deed = VendorAmandementDeed::create([
            'id_vendor_deed' => $request->id_vendor_deed,
            'deed_number' => $request->deed_number,
            'date' => $request->date,
            'notary_public' => $request->notary_public
        ]);

        return response()->json(array(
                            'deed_number' => $request->deed_number,
                            'date' => $request->date,
                            'notary_public' => $request->notary_public,
                            'status' => 1));
    }

    public function akta_upload(Request $request) {
        //name file Year month day second idvendor
        $file = $request->file('file');
        $list_menu = Fungsi::menu();
        $menus = (array_key_exists($request->menu,$list_menu)) ? $list_menu[$request->menu] : '';
        $tgl = date('Y-m-d');
        $vendor = Auth::user()->id;
        $Vendor_data = VendorDetail::where('id_user', $vendor)->firstOrFail();
        $destinationPath = 'assets/document/'.$Vendor_data->id.'/'.$menus.'/'.$tgl.'/';
        if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
        $vendor_document = VendorDocument::create([
            'id_vendor_detail' => $Vendor_data->id,
            'menu' => $request->menu,
        ]);
        $id_dokumen = $vendor_document->id;
        if($vendor_document){
            $vendor_document_detail = VendorDocumentDetail::create([
                'id_document' => $id_dokumen,
                'file' => date('Ymds').$Vendor_data->id.".".$file->getClientOriginalExtension(),
                'status' => 'Y',
            ]);
        }

        $file->move($destinationPath, date('Ymds').$Vendor_data->id.".".$file->getClientOriginalExtension());

        if($vendor_document_detail){
            session()->flash('message', 'Dokumen berhasil diupload');
           return redirect()->intended('vms/users/dokumen');
        }else{
            session()->flash('message', 'Dokumen gagal diupload');
           return redirect()->intended('vms/users/dokumen');
        }
    }

    public function dokumen_delete($id, $menu) {
        $menues = Fungsi::menu();
        $menus = (array_key_exists($menu, $menues)) ? $menues[$menu] : '';
        $vendor_document = VendorDocument::find($id)->delete();
        $vendor_document_detail = VendorDocumentDetail::where('id_document',$id);
        $data = $vendor_document_detail->firstOrFail();
        $delete = $vendor_document_detail->delete();
        $file = str_replace(".pdf","", $data->file);
        $tanggal = substr($file, 0,10);
        $tahun = substr($file, 0,4);
        $bulan = substr($file, 4,2);
        $hari = substr($file, 6,2);
        $tgl = $tahun."-".$bulan."-".$hari;
        $vendor = substr($file, 10, 11);
        $destinasi = 'assets/document/'.$vendor.'/'.$menus.'/'.$tgl;
        $deleteFile = unlink(public_path($destinasi ."/".$data->file));//File::delete($destinasi);
        if($deleteFile){
            session()->flash('message', 'Dokumen berhasil dihapus');
            return redirect()->intended('/vms/users/dokumen');
        }else{
            session()->flash('message', 'Dokumen gagal dihapus');
            return redirect()->intended('/vms/users/dokumen');
        }
    }

    public function document_vendor(Request $request) {
        $vendor = VendorDetail::where('id_user', $request->id_user)->firstOrFail();
        $doc = DB::table('vendor_document')
                ->select('vendor_document.*','vendor_document_detail.file','vendor_document_detail.status','vendor_document_detail.created_at')
                ->join('vendor_document_detail','vendor_document.id', '=','vendor_document_detail.id_document')
                ->where('vendor_document.id_vendor_detail',$vendor->id)
                ->get();




        return response()->json(array(
                            'doc' => $doc,
                            'vendor_id' => $vendor->id,
                            'status' => 1));
    }

    public function update_pemilik(Request $request) {
        $id_pemilik = $request->id;
        $nama = $request->nama;
        $ktp = $request->ktp;
        $alamat = $request->alamat;
        $id_user = $request->id_user;
        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();

        $vendor_owner = VendorOwner::where('id',$id_pemilik)->where('id_vendor_detail', $vendor_detail->id)->update([
            'name_vendor_owner' =>  $nama,
            'ktp_vendor_owner' =>  $ktp,
            'address_vendor_owner' =>  $alamat
        ]);

        return response()->json(array(
                            'name_vendor_owner' => $nama,
                            'ktp_vendor_owner' => $ktp,
                            'address_vendor_owner' => $alamat,
                            'id' => $id_pemilik,
                            'status' => 1));
    }

    public function delete_pemilik(Request $request) {
        $id_pemilik = $request->id;
        $vendor_owner = VendorOwner::where('id',$id_pemilik)->delete();

        return response()->json(array(
                            'id' => $id_pemilik,
                            'status' => 1));
    }

    public function create_pemilik(Request $request) {
        $id_user = $request->id_user;
        $nama = $request->nama;
        $ktp = $request->ktp;
        $alamat = $request->alamat;

        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();
        $vendor_owner = VendorOwner::create([
            'id_vendor_detail' =>  $vendor_detail->id,
            'name_vendor_owner' =>  $nama,
            'ktp_vendor_owner' =>  $ktp,
            'address_vendor_owner' =>  $alamat

        ]);

        return response()->json(array(
                            'status' => 1));
    }

    public function update_pengurus(Request $request) {
        $id_user = $request->id_user;
        $nama = $request->nama;
        $ktp = $request->ktp;
        $alamat = $request->alamat;
        $jabatan = $request->jabatan;

        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();
        $vendor_administrator = VendorAdministrators::updateOrCreate(['id_vendor_detail' => $vendor_detail->id],
            [
            'name_name_vendor_administrators' => $nama,
            'ktp_name_vendor_administrators' => $ktp,
            'address_name_vendor_administrators' => $alamat,
            'position_name_vendor_administrators' => $jabatan
            ]);

        return response()->json(array(
                            'status' => 1));
    }

    public function create_pengurus(Request $request) {
        $id_user = $request->id_user;
        $nama = $request->nama;
        $ktp = $request->ktp;
        $alamat = $request->alamat;
        $jabatan = $request->jabatan;

        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();
        $vendor_administrator = VendorAdministrators::create([
            'id_vendor_detail' => $vendor_detail->id,
            'name_name_vendor_administrators' => $nama,
            'ktp_name_vendor_administrators' => $ktp,
            'address_name_vendor_administrators' => $alamat,
            'position_name_vendor_administrators' => $jabatan
            ]);

        return response()->json(array(
                            'status' => 1));
    }

    public function delete_pengurus(Request $request) {
        $id_pengurus = $request->id;
        $vendor_owner = VendorAdministrators::where('id',$id_pengurus)->delete();

        return response()->json(array(
                            'id' => $id_pengurus,
                            'status' => 1));
    }

    public function update_tenaga_ahli(Request $request) {
        $nama = $request->nama;
        $ttl = $request->ttl;
        $jk = $request->jk;
        $pendidikan = $request->pendidikan;
        $pengalaman = $request->pengalaman;
        $profesi = $request->profesi;
        $alamat = $request->alamat;
        $warga = $request->warga;
        $email = $request->email;
        $id_tenaga = $request->id_tenaga;
        $id_user = $request->id_user;

        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();
        $vendor_expert_staff = VendorExpertStaff::find($id_tenaga)->update([
            'id_vendor_detail' => $vendor_detail->id,
            'name_vendor_expert_staff' => $nama,
            'birth_vendor_expert_staff' => $ttl,
            'address_vendor_expert_staff' => $alamat,
            'gender_vendor_expert_staff' => $jk,
            'education_vendor_expert_staff' => $pendidikan,
            'nationallty_vendor_expert_staff' => $warga,
            'experience_vendor_expert_staff' => $pengalaman,
            'email_vendor_expert_staff' => $email,
            'expertise_vendor_expert_staff' => $profesi
        ]);

        return response()->json(array(
                            'status' => 1));
    }

    public function create_tenaga_ahli(Request $request) {
        $nama = $request->nama;
        $ttl = $request->ttl;
        $jk = $request->jk;
        $pendidikan = $request->pendidikan;
        $pengalaman = $request->pengalaman;
        $profesi = $request->profesi;
        $alamat = $request->alamat;
        $warga = $request->warga;
        $email = $request->email;
        $id_tenaga = $request->id_tenaga;
        $id_user = $request->id_user;

        $vendor_detail = VendorDetail::where('id_user',$id_user)->firstOrFail();
        $vendor_expert_staff = VendorExpertStaff::create([
            'id_vendor_detail' => $vendor_detail->id,
            'name_vendor_expert_staff' => $nama,
            'birth_vendor_expert_staff' => $ttl,
            'address_vendor_expert_staff' => $alamat,
            'gender_vendor_expert_staff' => $jk,
            'education_vendor_expert_staff' => $pendidikan,
            'nationallty_vendor_expert_staff' => $warga,
            'experience_vendor_expert_staff' => $pengalaman,
            'email_vendor_expert_staff' => $email,
            'expertise_vendor_expert_staff' => $profesi
        ]);

        return response()->json(array(
                            'status' => 1));
    }

    public function delete_tenaga_ahli(Request $request) {
        $id_tenaga = $request->id;
        $vendor_owner = VendorExpertStaff::where('id',$id_tenaga)->delete();

        return response()->json(array(
                            'id' => $id_tenaga,
                            'status' => 1));
    }

    public function create_tools(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();
        $tools = VendorTools::create([
            'id_vendor_detail' => $vendor_detail->id,
            'created_by' => Auth::user()->id
        ]);
        $destinationPath = 'assets/document/'.$vendor_detail->id.'/evidence_tools';
        if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
        $file = $request->file('file');
        if($tools){
            $tools_detail = VendorToolsDetail::create([
                'id_vendor_tool' => $tools->id,
                'tool_name' => $request->tool_name,
                'qty' => $request->qty,
                'capacity' => $request->capacity,
                'type' => $request->type,
                'condition' => $request->condition,
                'additional_maker' => $request->additional_maker,
                'location' => $request->location,
                'evidance' => date('Ymds')."_".$vendor_detail->id.".".$file->getClientOriginalExtension(),
                'description' => $request->description
            ]);
        }else{
            echo "gagal";
        }


        $pindah = $file->move($destinationPath, date('Ymds')."_".$vendor_detail->id.".".$file->getClientOriginalExtension());

        return redirect('vms/users/peralatan');
    }

    public function update_tools(Request $request) {
        $file = $request->file('file');
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();
        $gambar = ($request->file !== null) ? date('Ymds')."_".$vendor_detail->id.".".$file->getClientOriginalExtension() : '';
        $vendor_tools_details = VendorToolsDetail::where('id',$request->id)->value('evidance');
        $destinationPath = 'assets/document/'.$vendor_detail->id.'/evidence_tools';

        $vendor_tools_detail = VendorToolsDetail::find($request->id)->update([
                'tool_name' => $request->tool_name,
                'qty' => $request->qty,
                'capacity' => $request->capacity,
                'type' => $request->type,
                'condition' => $request->condition,
                'additional_maker' => $request->additional_maker,
                'location' => $request->location,
                'description' => $request->description
            ]);

        if($request->file !== null){
            $vendor_tools_detail = VendorToolsDetail::find($request->id)->update([
                'evidance' => $gambar
            ]);
            $file->move($destinationPath, $gambar);
            $deleteFile = unlink(public_path($destinationPath ."/".$vendor_tools_details));
        }

        return redirect('vms/users/peralatan');
    }

    public function delete_tools(Request $request) {
        $id = $request->id;
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();
        $vendor_details = VendorToolsDetail::where('id_vendor_tool',$id)->firstOrFail();

        $destinationPath = 'assets/document/'.$vendor_detail->id.'/evidence_tools';
        $vendor_owner = VendorToolsDetail::where('id',$id)->delete();
        $deleteFile = unlink(public_path($destinationPath ."/".$vendor_details->evidance));

        return response()->json(array(
                            'status' => 1));
    }

    public function create_experiences(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();
        $experiences = VendorExperiences::create([
            'id_vendor_detail' => $vendor_detail->id,
            'created_by' => Auth::user()->id
        ]);
        if($experiences){
            $tools_detail = VendorExperiencesDetail::create([
                'id_vendor_experience' => $experiences->id,
                'job_order' => $request->job_order,
                'location' => $request->location,
                'user' => $request->user,
                'address' => $request->address,
                'contract_date' => $request->contract_date,
                'contract_finish' => $request->contract_finish,
                'contract_value' => $request->contract_value,
                'type' => $request->type,
                'data_source' => $request->data_source
            ]);
        }else{
            echo "gagal";
        }


        return redirect('vms/users/pengalaman');
    }

    public function update_experiences(Request $request) {
        $vendor_tools_detail = VendorExperiencesDetail::find($request->id)->update([
                'job_order' => $request->job_order,
                'location' => $request->location,
                'user' => $request->user,
                'address' => $request->address,
                'contract_date' => $request->contract_date,
                'contract_finish' => $request->contract_finish,
                'contract_value' => $request->contract_value,
                'type' => $request->type,
                'data_source' => $request->data_source
            ]);

        return redirect('vms/users/pengalaman');
    }

    public function delete_experiences(Request $request) {
        $id = $request->id;
        $vendor = VendorExperiencesDetail::where('id',$id);
        $vendor_experiences = $vendor->value('id_vendor_experience');
        $vendor_delete_experiences = VendorExperiences::where('id',$vendor_experiences)->delete();
        $delete_experiences = $vendor->delete();
        return response()->json(array(
                            'status' => 1));
    }

    public function create_tax(Request $request) {
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();

        $destinationPath_npwp_scan = 'assets/document/'.$vendor_detail->id.'/npwp_scan';
        $destinationPath_evidence_tax = 'assets/document/'.$vendor_detail->id.'/evidence_tax';
        $destinationPath_evidence_body_tax = 'assets/document/'.$vendor_detail->id.'/evidence_body_tax';

        if(!File::exists($destinationPath_npwp_scan))
                File::makeDirectory($destinationPath_npwp_scan, 0775, true);
        if(!File::exists($destinationPath_evidence_tax))
                File::makeDirectory($destinationPath_evidence_tax, 0775, true);
        if(!File::exists($destinationPath_evidence_body_tax))
                File::makeDirectory($destinationPath_evidence_body_tax, 0775, true);

        $vendorTax = VendorTax::where('id_vendor_detail', $vendor_detail->id);
        $total = $vendorTax->count();
            if($total == 0){
                $file = $request->file('npwp_scan');
                    $vendor_tax = VendorTax::create([
                        'id_vendor_detail' => $vendor_detail->id,
                        'npwp' => $request->npwp,
                        'npwp_scan' => $request->npwp."_".$vendor_detail->id.".".$file->getClientOriginalExtension(),
                        'status' => 'Y',
                    ]);

                $LASTID = $vendor_detail->id;
                $pindah = $file->move($destinationPath_npwp_scan, $request->npwp."_".$vendor_detail->id.".".$file->getClientOriginalExtension());
            }else{
                $last_id = $vendorTax->orderBy('id','DESC')->first()->id;
                $LASTID = $last_id;

            }

        $fevidence_tax = $request->file('evidence_tax');
        $fevidence_body_tax = $request->file('evidence_body_tax');
            // if($vendor_tax){
                $vendor_tax = VendorTaxDetail::create([
                    'id_vendor_tax' => $LASTID,
                    'evidence_tax' => $request->npwp."_".$vendor_detail->id.".".$fevidence_tax->getClientOriginalExtension(),
                    'evidence_body_tax' => $request->npwp."_".$vendor_detail->id.".".$fevidence_body_tax->getClientOriginalExtension(),
                    'status' => 'Y',
                ]);
            // }


        $pindah = $fevidence_tax->move($destinationPath_evidence_tax, $request->npwp."_".$vendor_detail->id.".".$fevidence_tax->getClientOriginalExtension());
        $pindah = $fevidence_body_tax->move($destinationPath_evidence_body_tax, $request->npwp."_".$vendor_detail->id.".".$fevidence_body_tax->getClientOriginalExtension());

        return redirect('vms/users/pajak');
    }

    public function delete_tax(Request $request) {
        $id = $request->id;
        $vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->firstOrFail();
        $vendor_details = VendorTaxDetail::where('id',$id)->firstOrFail();
        $destinationPath_evidence_tax = 'assets/document/'.$vendor_detail->id.'/evidence_tax';
        $destinationPath_evidence_body_tax = 'assets/document/'.$vendor_detail->id.'/evidence_body_tax';

        $vendor_owner = VendorTaxDetail::where('id',$id)->delete();
        $deleteFile_evidence_tax = unlink(public_path($destinationPath_evidence_tax ."/".$vendor_details->evidence_tax));
        $deleteFile_evidence_body_tax = unlink(public_path($destinationPath_evidence_body_tax ."/".$vendor_details->evidence_body_tax));

        return response()->json(array(
                            'status' => 1));

    }

		public function request_order(Request $request) {
			$filter = ($request->filter != null) ? $request->filter : 'xxx';
			$search = ($request->search != null) ? $request->search : 'xxx';

			$vendor_detail = VendorDetail::where('id_user',Auth::user()->id)->value('id');
			$data = DB::table('order_catalog')
				          ->select('order_proposer.*', 'users.username', 'order_catalog.id AS id_order', 'order_catalog.no_po', 'order_catalog.status AS status_order')
				          ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
				          ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
									->where('order_catalog.id_vendor_detail',$vendor_detail)
									->orderBy('order_catalog.datetime','DESC');
				          // ->paginate(10);
			$order = DB::table('order_catalog')
                  ->select('order_catalog.id AS id_order', 'order_catalog.id_proposer', 'order_catalog.status AS status_order','user_catalog.username_catalog','vendor_detail.vendor_name', 'order_catalog_detail.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
                  ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                  ->join('vendor_detail', 'order_catalog.id_vendor_detail','=','vendor_detail.id')
						      ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
						      ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
									->where('order_catalog.id_vendor_detail',$vendor_detail)
                  ->orderBy('order_catalog.datetime','DESC')
									->paginate(10);

			$delivery = DB::table('delivery_order')
									->select('delivery_order.*', 'delivery_order_detail.*')
									->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
									->pluck('id_order', 'qty')
									->toArray();

			$receiveIdOrder = DB::table('receive_item')
									->select('receive_item.*', 'receive_item_detail.*')
									->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
									->pluck('id_order', 'id_item')
									->toArray();


			$rejectIdItem = ItemReject::where('status', 1)->pluck('id_order', 'qty')->toArray();

			$deliveryData = DB::table('delivery_order')
									->select('delivery_order.*', 'delivery_order_detail.*')
									->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
									->get();

									if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $data->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                    }else{
                      $data->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $data->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                    }else{
                      $data->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $data->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);
                  }
			$request_order = $data->paginate(10);
			$order_detail = OrderDetail::all();

			return view('vms.users.request_order.index', compact('rejectIdItem','receiveIdOrder','order','request_order','delivery', 'deliveryData', 'order_detail'));
		}

		public function chatting(Request $request) {
			$id = VendorDetail::where('id_user', Auth::user()->id)->value('id');
			$chat = DB::table('chat')
							->select('chat.*','username')
							->join('cart', 'chat.id_cart', '=', 'cart.id')
							->join('users','chat.id_user', '=', 'users.id')
							->where('cart.status','1')
							->where('chat.id_vendor_detail', $id)
							->whereRaw('chat.id IN (select MAX(chat.id) FROM chat GROUP BY chat.id_user)');
			$data = $chat->get();

			return view('vms.users.chatting.index', compact('data'));
		}

		public function chatting_detail($id_cart, $id_user, $id_item) {
			$id = VendorDetail::where('id_user', Auth::user()->id)->value('id');
			$chat = DB::table('chat')
							->select('chat.*','username')
							->join('cart', 'chat.id_cart', '=', 'cart.id')
							->join('users','chat.id_user', '=', 'users.id')
							->where('cart.status','1')
							->where('chat.id_vendor_detail', $id)
							->where('chat.id_cart', $id_cart)
							->where('chat.id_user', $id_user)
							->where('chat.id_item', $id_item);
			$data = $chat->get();

			return view('vms.users.chatting.detail', compact('data'));
		}

		public function Addchat(Request $request) {
			$cart = Chat::create([
				'id_cart' => $request->id_cart,
				'id_user' => $request->id_user,
				'id_vendor_detail' => $request->id_vendor_detail,
				'id_item' => $request->id_item,
				'datetime' => date('Y-m-d H:i:s'),
				'message' => $request->message,
				'status' => 0
			]);
			return response()->json(array(
				'status' => 0));
		}

		public function downloadProposer($id_proposer){
			$user = VendorDetail::where('id_user',Auth::user()->id)->value('id');
			$data = DB::select("SELECT *
      from order_proposer
      where md5(CAST(id AS VARCHAR(50))) = '".$id_proposer."'");


      // $dataOrder = DB::table('order_catalog_detail')
      //       ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment','item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
      //       ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
      //       ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
      //       ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
      //       ->where(DB::raw('md5(order_proposer.id)'), $id_proposer)
      //       ->where('order_catalog.id_vendor_detail',$user)
      //       ->get();

      $dataOrder = DB::select("select order_catalog.datetime,order_catalog_detail.*,item.price_shipment,item.code,item.name,item.merk,
      item.price_retail,item.price_gov,item.price_country
      from order_catalog_detail
      join item on order_catalog_detail.id_item=item.id
      join order_catalog on order_catalog_detail.id_order = order_catalog.id
      join order_proposer on order_catalog.id_proposer = order_proposer.id
      where md5(CAST(order_proposer.id AS VARCHAR(50))) = '".$id_proposer."'");

	    $pdf = PDF::loadView('vms.users.doc.usulan', compact('data','dataOrder'));
	    // return $pdf->download('usulan.pdf');
	    return $pdf->stream();
	  }

		public function downloadPo($no_po, $bln, $thn){
      $data = Order::where(DB::raw('md5(no_po)'), $no_po)
              ->where(DB::raw('md5(month(order_catalog.datetime))'), $bln)
              ->where(DB::raw('md5(year(order_catalog.datetime))'), $thn)
              ->first();
      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      // $vendorDirectur = DB::table('vendor_owner')->select()->where('id_vendor_detail', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $directur = DB::table('user_catalog_directur')
                  ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
                  ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
                  ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                  ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
                  ->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(order_catalog.no_po)'), $no_po)
            ->where(DB::raw('md5(month(order_catalog.datetime))'), $bln)
            ->where(DB::raw('md5(year(order_catalog.datetime))'), $thn)
            ->get();

            $list_propinsi = Fungsi::propinsi();
            $list_kota = Fungsi::IndonesiaProvince();

      return view('vms.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));

    }

		public function createInvoice($id){
      $data = Order::where(DB::raw('md5(id)'), $id)->first();
      $dataItem = DB::table('order_catalog_detail')->select('order_catalog_detail.*', 'item.name', 'item.code', 'item.merk', 'item.price_gov', 'item.price_shipment')
                  ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
                  ->where('order_catalog_detail.id_order', $data->id)
                  ->get();
      $vendor = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      return view('vms.users.doc.invoice', compact('dataItem', 'vendor', 'list_propinsi' ,'list_kota', 'data'));
    }


		public function contractIndex(Request $request) {
			$data = DB::table('contract')->select('contract.*')->join('vendor_detail', 'contract.id_vendor_detail', '=', 'vendor_detail.id')->where('vendor_detail.id_user', Auth::user()->id)->where('status', 1)->get();
			return view('vms.users.contract.index', compact('data'));
		}

		public function contractCreate(Request $request) {
			return view('vms.users.contract.create');
		}

		public function contractStore(Request $request) {
			$vendorDetail = VendorDetail::where('id_user', Auth::user()->id)->first();
			$file = $request->file('data');

			$contract = Contract::create([
				'id_vendor_detail' => $vendorDetail->id ,
				'start_date' => $request->start_date,
				'end_date' => $request->end_date,
				'status' => 1,
				'description' => $request->description,
				'data' => "KONTRAK_".$vendorDetail->id.".".$file->getClientOriginalExtension()
			]);

			$destinationPath = 'assets/document/'.$vendorDetail->id.'/contract';

			if(!File::exists($destinationPath))
			  File::makeDirectory($destinationPath, 0775, true);

			$pindah = $file->move($destinationPath, "KONTRAK_".$vendorDetail->id.".".$file->getClientOriginalExtension());


			session()->flash('message', 'Pembuatan kontrak sukses');
			return redirect('vms/users/contract');

		}

		public function product(Request $request) {
			$vendorData = DB::table('vendor_status')
								->select('vendor_status.last_status','vendor_status.id_vendor_detail')
								->join('vendor_detail', 'vendor_status.id_vendor_detail', '=', 'vendor_detail.id')
								->where('vendor_detail.id_user', Auth::user()->id)
								->first();
			$contract = Contract::where('id_vendor_detail', $vendorData->id_vendor_detail)->count();
			if($vendorData->last_status != 0){
				if($contract <= 0){
					return redirect('vms/users/contract');
				}
				$category = Category::where('status', 'Y')->get();
				$vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
				$data = DB::table('item')
				->select('item.created_at', 'item.id', 'item.code', 'item.description', 'item.name', 'item.merk', 'item.status', 'category.name AS name_category')
				->join('supplier_item', 'item.id', '=', 'supplier_item.id_item')
				->join('category_item', 'item.id', '=', 'category_item.id_item')
				->join('category', 'category_item.id_category', '=', 'category.id')
				->orderBy('item.created_at', 'DESC')
				->orderBy('item.id', 'DESC')
				->where('supplier_item.id_supplier', $vendor)
				->paginate(10);


				return view('vms.users.product.index', compact('category', 'data'));
			}else{
				return abort(404);
			}
		}

		public function productAdd(Request $request) {
			$request->validate([
		             'category' => 'required',
		             'import_file' => 'required'
		         ]);
						 $vendorDetail = VendorDetail::where('id_user', Auth::user()->id)->value('id');
		         $path = $request->file('import_file')->getRealPath();
		         $data = Excel::load($path)->get();

		         if($data->count() > 0){
		             foreach ($data as $key => $value) {
									 $item = Item::create([
										 				'name' => ($value->name != null) ? $value->name : 'tidak diketahui',
														'code' => ($value->code != null) ? $value->code : '0000000',
														'merk'  => ($value->merk != null) ? $value->merk : 'merk tidak diketahui',
														'item_from'  => '0',
														'price_country'  => '0',
														'price_retail'  => ($value->price_retail != null) ? $value->price_retail : '0',
														'price_gov'  => ($value->price_gov != null) ? $value->price_gov : '0',
														'price_shipment'  => ($value->price_shipment != null) ? $value->price_shipment : '0',
														'price_date'  =>($value->price_date != null) ?  $value->price_date : date('Y-m-d H:i:s'),
														'release_date'  => ($value->release_date != null) ? $value->release_date : date('Y-m-d H:i:s'),
														'expired_date'  => ($value->expired_date != null) ? $value->expired_date : date('Y-m-d H:i:s'),
														'stock'  => ($value->stock != null) ? $value->stock : '0',
														'stock_min'  => ($value->stock_min != null) ? $value->stock_min : '0',
														'production_origin'  => '0',
														'description'  => ($value->description != null) ? $value->description : 'tidak ada deskripsi',
														'status' => 'Y'
									 				]);
									$general_spec = GeneralSpec::create([
													'id_item' => $item->id,
													'weight' => ($value->weight != null) ?  $value->weight : '0',
													'height' => ($value->height != null) ?  $value->height : '0',
													'volume' => ($value->volume != null) ?  $value->volume : '0',
													'minimal_buy' => ($value->minimal_buy != null) ?  $value->minimal_buy : '0'
													]);
									 $Categoryitem = CategoryItem::create([
										 			'id_category' => $request->category,
													'id_item' => $item->id
									 				]);
									 $suplier = Supplier::create([
										 			'id_supplier' => $vendorDetail,
													'id_item' => $item->id
									 				]);

                    $img = ItemImage::create([
                      'file' => ($value->image != null) ? $value->image : '',
                      'id_item' => $item->id
                    ]);
		             }
		         }

		         return back()->with('message', 'Sukses Import Item');
				}


				public function productEdit($id) {
					$supplier = vendorDetail::where('id_user', Auth::user()->id)->get();
					$data = Item::where('id',$id)->first();
					$category = DB::table('category')->select('category.name', 'category.id')->join('category_item', 'category.id', '=', 'category_item.id_category')->value('name');
					return view('vms.users.product.edit', compact('id','supplier','data', 'category'));
				}

				public function productUpdate(Request $request, $id) {
		      $category = Item::where('id', $id);
		      $cekPrice = $category->first();
		      $listCat = CategoryItem::where('id_item', $id)->value('id_category');
		      if(($cekPrice->price_retail != $request->price_retail) || ($cekPrice->price_gov != $request->price_gov)){
		        $priceHistory = ItemPriceHistory::create([
		          "id_item" => $id,
		          "price_retail" => $request->price_retail,
		          "price_gov" => $request->price_gov,
		          "price_date" => $request->price_date,
		          "price_shipment" => $request->price_shipment,
		        ]);
		      }
		      $update = $category->update([
		             "code" => $request->code,
		             "name" => $request->name,
		             "merk" => $request->merk,
		             "price_retail" => $request->price_retail,
		             "price_shipment" => $request->price_shipment,
		             "price_gov" => $request->price_gov,
		             "price_date" => $request->price_date,
		             "release_date" => $request->release_date,
		             "expired_date" => $request->expired_date,
		             "description" => $request->description,
		             "production_origin" => $request->production_origin
		        ]);

		        return redirect('vms/users/product')->with(array('message'=> 'Sukses Update Item', 'id'=> $listCat));
		    }

				public function productSpec($id) {
					$vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
					$id_category = DB::table('category_item')->where('id_item', $id)->value('id_category');
					$category_name = DB::table('category_item')->select('category.name')->join('category', 'category_item.id_category', '=', 'category.id')->where('id_item', $id)->value('name');
		      $category_spec = DB::table('category_spec')->where('id_category', $id_category)->where('created_by', $vendor)->get();
					$general_spec = GeneralSpec::where('id_item', $id)->first();
		      return view('vms.users.product.spec',compact('id_category','category_spec', 'general_spec', 'category_name'))->with('id', $id);
				}


				public function productSpecAdd(Request $request) {
					$vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
					$catSpec = CategorySpec::create([
							'name' => $request->name,
							'id_category' => $request->id_category_spec,
							'status' => $request->status,
							'created_by' => $vendor
					]);

					return redirect()->back()->with('message', 'Sukses menambahkan spesifikasi');

				}

				public function productSpecUpdate(Request $request) {
					$vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
					$catSpec = CategorySpec::where('id_category', $request->id_category)->where('id', $request->id_category_spec);
					$idSpecItem = $catSpec->value('id');
					$update = $catSpec->update(['name' => $request->name, 'status' => $request->status]);
					$specItem = ItemSpec::updateOrCreate(
													['id_category_spec' => $idSpecItem,
														'id_item' => $request->id_item],
													['description' => $request->description,
													 'created_by' => $vendor]);
					return redirect()->back()->with('message', 'Sukses update spesifikasi');

				}

				public function productSpecGeneralUpdate(Request $request) {
					$specItem = GeneralSpec::where('id_item', $request->id_item)->update(['weight' => $request->weight,
																'height' => $request->height,
																'volume' => $request->volume,
																'minimal_buy' => $request->minimal_buy]);
					return redirect()->back()->with('message', 'Sukses update spesifikasi');

				}

				public function productImage($id) {
					$vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
					$id_category = DB::table('category_item')->where('id_item', $id)->value('id_category');
					$category_name = DB::table('category_item')->select('category.name')->join('category', 'category_item.id_category', '=', 'category.id')->where('id_item', $id)->value('name');
					$category_spec = DB::table('category_spec')->where('id_category', $id_category)->where('created_by', $vendor)->get();
					$general_spec = GeneralSpec::where('id_item', $id)->first();
					return view('vms.users.product.image',compact('id_category','category_spec', 'general_spec', 'category_name'))->with('id', $id);
				}

				public function productImageSave(Request $request, $id) {
					$file = $request->file('file');
					$destinationPath = 'assets/catalog/item';
					$idImg = ItemImage::where('id_item', $id)->orderBy('id', 'desc')->first();
					$Imgno = ($idImg == null) ? 1 : $idImg->id+1;
					if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
					foreach ($file as $key => $value) {
						$number = $Imgno+$key;
						$img = ItemImage::create([
							'file' => "item_".$id."_".$number.".".$value->getClientOriginalExtension(),
							'id_item' => $id
						]);

						$move = $value->move($destinationPath, "item_".$id."_".$number.".".$value->getClientOriginalExtension());
					}
					if($move) {
						$message = 'Sukses mengunggah gambar';
					}else{
						$message = 'Gagal mengunggah gambar';
					}
					return redirect()->back()->with('message', $message);
			}

			public function DeleteImage($id) {
	      $item = ItemImage::where('file', $id)->delete();
	      $destinationPath = 'assets/catalog/item';
	      $deleteFile = unlink(public_path($destinationPath ."/".$id));//File::delete($destinasi);
	      if($deleteFile){
	        $message = 'Sukses menghapus gambar';
	      }else{
	        $message = 'Gagal menghapus gambar';
	      }
	      return redirect()->back()->with('message', $message);
	    }

			public function doc($id) {
	      return view('vms.users.product.doc')->with('id', $id);
	    }

	    public function DocSave(Request $request, $id) {
	      $file = $request->file('file');
	      $destinationPath = 'assets/catalog/doc';
	      if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
	      $idDoc = ItemDoc::where('id_item', $id)->orderBy('id', 'desc')->first();
	      $doCno = ($idDoc == null) ? 1 : $idDoc->id+1;
	      $img = ItemDoc::create([
	        'name' => $request->name,
	        'description' => $request->description,
	        'id_item' => $id,
	        'file' => "d_".$id."_".$doCno.".".$file->getClientOriginalExtension()
	      ]);
	      $move = $file->move($destinationPath, "d_".$id."_".$doCno.".".$file->getClientOriginalExtension());
	      if($move) {
	        $message = 'Sukses mengunggah dokumen';
	      }else{
	        $message = 'Gagal mengunggah dokumen';
	      }

	      return redirect()->back()->with('message', $message);
	    }

	    public function DeleteDoc($id) {
	      $item = ItemDoc::where('file', $id)->delete();
	      $destinationPath = 'assets/catalog/doc';
	      $deleteFile = unlink(public_path($destinationPath ."/".$id));//File::delete($destinasi);
	      if($deleteFile){
	        $message = 'Document Deleted successful';
	      }else{
	        $message = 'Document Deleted fail';
	      }
	      return redirect()->back()->with('message', $message);
	    }

			public function status(Request $res) {
				$updateStatus = DB::table('doc_item')->where('id', $res->id)->update([
					'status' => $res->status,
				]);

				if($updateStatus){
					$message = 'Sukses mengubah status dokumen';
				}else{
					$message = 'Gagal mengubah status dokumen';
				}

				return redirect()->back()->with('message', $message);
			}

			public function historyPrice(Request $request, $id) {
	      $history = DB::table('item')
	                  ->select('item.name', 'price_item.*', 'item.price_country', 'item.price_shipment')
	                  ->join('price_item', 'item.id', '=','price_item.id_item')
	                  ->where('id_item', $id)->get();
	      return view('vms.users.product.price',compact('history'))->with('id', $id);
	    }

			public function deliveryOrder(Request $request, $id) {
				$data = DB::table('order_catalog_detail')
									->select('order_catalog.datetime', 'order_catalog.no_po', 'order_catalog_detail.id', 'order_catalog_detail.id_order','order_catalog_detail.id_item','item.name', 'item.description')
									->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
									->join('order_catalog', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
									->where(DB::raw('md5(order_catalog_detail.id_order)'), $id);
				$ItemReject = ItemReject::where(DB::raw('md5(id_order)'), $id)->pluck('qty', 'id_item')->toArray();
				$satuan = UnitType::where('status', 1)->get();
									// $dataDO = DB::table('delivery_order')
									// 					->select('delivery_order_detail.qty')
									// 					->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
									// 					->where('delivery_order_detail.id_item', $request->id_item)
									// 					->where('delivery_order.id_order', $fisrt->id_order)
									// 					->sum('qty');
									// 					if($dataDO != null){
									// 						$qty = $data->value('qty');
									// 						$AllQty = $qty - $dataDO;
									// 					}else{
									// 						$AllQty = $data->value('qty');
									// 					}
				$dataItem = $data->get();
				$po = $data->first();

	      return view('vms.users.delivery_order.index',compact('satuan', 'ItemReject', 'dataItem', 'VendorDetail', 'po'))->with('id', $id);
	    }

			public function deliveryOrderResendReject(Request $request, $id) {
				$data = DB::table('order_catalog_detail')
									->select('order_catalog.datetime', 'order_catalog.no_po', 'order_catalog_detail.id', 'order_catalog_detail.id_order','order_catalog_detail.id_item','item.name', 'item.description')
									->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
									->join('order_catalog', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
									->where(DB::raw('md5(order_catalog_detail.id_order)'), $id);
				$ItemReject = ItemReject::where(DB::raw('md5(id_order)'), $id)->pluck('qty', 'id_item')->toArray();
				$dataItem = $data->get();
				$po = $data->first();
				$satuan = UnitType::where('status', 1)->get();

	      return view('vms.users.delivery_order.rejectItem',compact('satuan', 'ItemReject', 'dataItem', 'VendorDetail', 'po'))->with('id', $id);
	    }

			public function preview($id) {

				$delivery = DeliveryOrder::where(DB::raw('md5(id)'), $id)->first();
				$DataPo = Order::where('id', $delivery->id_order)->first();
				$deliveryDtl = 	DB::table('delivery_order_detail')
												->select('unit_type.unit_name', 'delivery_order_detail.*', 'item.merk', 'item.code', 'item.name')
												->join('item', 'delivery_order_detail.id_item', '=', 'item.id')
												->join('unit_type', 'delivery_order_detail.satuan', '=', 'unit_type.id')
												->where('delivery_order_detail.id_do', $delivery->id)
												->get();
				$driver = $delivery->driver;
				$car = $delivery->car;
				$car_no = $delivery->car_no;
				$sent_by = $delivery->sent_by;
				$destination = "Rumah Sakit Jantung Harapan Kita";
				$address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";
				$telp = "(021) 5684093";
				$list_propinsi = Fungsi::propinsi();
				$list_kota = Fungsi::IndonesiaProvince();
				$bulanWord = Fungsi::MonthIndonesia();
				$order = Order::where('id', $delivery->id_order)->first();

				$VendorDetail = VendorDetail::where('id_user', Auth::user()->id)->first();
	      return view('vms.users.delivery_order.preview',compact('order', 'telp', 'list_propinsi', 'list_kota', 'bulanWord','sent_by','DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
	    }

		public function detailItem(Request $request){
			$data = OrderDetail::where('id', $request->id);
			$fisrt = $data->first();

			$dataDO = DB::table('delivery_order')
								->select('delivery_order_detail.qty')
								->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
								->where('delivery_order_detail.id_item', $request->id_item)
								->where('delivery_order.id_order', $fisrt->id_order)
								->sum('qty');
								if($dataDO != null){
									$qty = $data->value('qty');
									$AllQty = $qty - $dataDO;
								}else{
									$AllQty = $data->value('qty');
								}
			return response()->json(array(
															 'qty' => $AllQty,
															 'status' => 1));
		}

		public function detailItemReject(Request $request){
			$data = OrderDetail::where('id', $request->id);
			$fisrt = $data->first();

			$dataDO = DB::table('delivery_order')
								->select('delivery_order_detail.qty')
								->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
								->where('delivery_order_detail.id_item', $request->id_item)
								->where('delivery_order.id_order', $fisrt->id_order)
								->sum('qty');

			$ItemReject = ItemReject::where('id_item', $request->id_item)
										->where('id_order', $fisrt->id_order)->sum('qty');


								if($dataDO != null){
									$qty = $data->value('qty');
									$AllQty = ($qty - $dataDO)+$ItemReject;
								}else{
									$AllQty = ($data->value('qty'))+$ItemReject;
								}
			return response()->json(array(
															 'qty' => $AllQty,
															 'status' => 1));
		}

		public function submitDo(Request $request){
			$categoryItem = CategoryItem::whereIn('id_item', $request->idDO)->first();
			$mainCategory = Category::where('id', $categoryItem->id_category)->first();
			$mainCategorys = Category::where('id_parent', $mainCategory->id_parent)->first();
			$mainCategorysI = Category::where('id', $mainCategory->id_parent)->first();
			$warehouseTypeCategory = CategoryWarehouseType::where('id_category', $mainCategorysI->id_parent)->first();

			$Do = DeliveryOrder::create([
					'id_order' => $request->id_po,
					'datetime' => date('Y-m-d H:i:s'),
					'status' => 1,
					'sent_by' => $request->sent_by,
					'driver' => $request->driver,
					'car' => $request->car,
					'car_no' => $request->sent_by == 0 ? '-' : $request->car_no,
					'id_gudang' => $warehouseTypeCategory->id_gudang
			]);
			foreach ($request->idDO as $k => $v) {
				$DoDetail = DeliveryOrderDetail::create([
					'id_do' => $Do->id,
					'id_item' => $v,
					'qty' => $request->qtyDO[$k],
					'satuan' => $request->satuanDO[$k],
					'description' => ($request->keteranganDO[$k] != null) ? $request->keteranganDO[$k] : '-',
					'status' => 1
				]);
			}

			$data = OrderDetail::where('id_order', $Do->id_order)->sum('qty');
			$data2 = DB::table('delivery_order_detail')
							->select('delivery_order_detail.qty')
							->join('delivery_order', 'delivery_order_detail.id_do', '=', 'delivery_order.id')
							->where('delivery_order.id_order',  $Do->id_order)
							->sum('qty');

			if($data == $data2){
				$order = Order::where('id', $Do->id_order)->update(['status' => 5]);
			}else{
				$order = Order::where('id', $Do->id_order)->update(['status' => 4]);
			}
			$destination = $request->rs;
			$address = $request->address;
			$driver = $request->driver;
			$car = $request->car;
			$car_no = $request->car_no;
			$delivery = DeliveryOrder::where('id', $Do->id)->first();
			$deliveryDtl = 	DB::table('delivery_order_detail')
											->select('delivery_order_detail.*', 'item.merk', 'item.code', 'item.name')
											->join('item', 'delivery_order_detail.id_item', '=', 'item.id')
											->where('delivery_order_detail.id_do', $Do->id)
											->get();
			$VendorDetail = VendorDetail::where('id_user', Auth::user()->id)->first();
			return redirect()->intended('vms/users/delivery_order/preview/'.md5($Do->id))->with(['VendorDetail' => $VendorDetail, 'delivery' => $delivery, 'deliveryDtl' => $deliveryDtl,'driver' => $driver, 'car' => $car, 'car_no' => $car_no, 'destination' => $destination ,'address' => $address]);
		}


		public function trackItem(Request $request) {
			$key = ($request->id_po != null) ? $request->id_po : 'xxx';
			$noSPJ = explode('/', $key);

			if($key != 'xxx'){
				$data = DB::table('delivery_order')
								->select('delivery_order.*')
								->join('order_catalog', 'delivery_order.id_order', '=', 'order_catalog.id')
								->where('order_catalog.no_po', $noSPJ[0])
								->where(DB::raw('month(order_catalog.datetime)'), $noSPJ[2])
								->where(DB::raw('year(order_catalog.datetime)'), $noSPJ[3]);
				$deliveryDtl = $data->get();
			}else{
				$deliveryDtl = 'xxx';
			}
			$VendorDetail = VendorDetail::where('id_user', Auth::user()->id)->first();
			$do = Order::where('id_vendor_detail', $VendorDetail->id)->orderBy('datetime', 'DESC')->get();
			return view('vms.users.track_item.index', compact('do', 'deliveryDtl', 'VendorDetail','key'));
		}

	public function data_changes(Request $request) {
		return view('vms.users.data_changes.index');
	}

	public function data_changes_save(Request $request) {
		$id_vendor = VendorDetail::where('id_user', Auth::user()->id)->value('id');
		$vendorDataChanges = VendorDataChanges::create([
			'id_vendor_detail' => $id_vendor,
			'data_changes' => $request->data,
			'reason' => $request->reason,
			'changes_by' => Auth::user()->id,
			'datetime' => date('Y-m-d H:i:s'),
			'status' => '2',
		]);
		session()->flash('message', 'Pengajuan Perubahan Data berhasil dikirim');
		return redirect()->intended('vms/users/perubahan_data');
	}



	function vcs(){
			echo "string";

	}

}
