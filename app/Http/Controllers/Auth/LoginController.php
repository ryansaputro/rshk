<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;
use DB;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('auth.users.dashboard.index');
    }

    public function username()
    {
        return 'email';
    }

    protected function authenticated($request, $user)
    {

       $vcs = $request->vcs;
       if($vcs=='vcs')
       {
                 if ($user->status != 0)
                 {

                     if($user->hasRole('vcs'))
                     {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/vcs/users/product')->with('key', 'value');
                     }
                     else
                     {

                       Auth::logout();
                       session()->flash('message','Username atau password anda salah !!!');
                       return redirect('/vcs');
                     }
                  }
       }else{
               if ($user->status != 0)
               {
                   if($user->hasRole('admin'))
                   {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/admin')->with('key', 'value');
                   }else if($user->hasRole('finance'))
                   { //finance katalog
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/finance/dashboard')->with('key', 'value');
                   }else if($user->hasRole('supervisi'))
                   { //supervisi katalog
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/supervisi/dashboard')->with('key', 'value');
                   }else if($user->hasRole('divisi'))
                   { //user katalog
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/item')->with('key', 'value');
                   }else if($user->hasRole('managercatalog'))
                   { //manajer katalog
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/manager/dashboard')->with('key', 'value');
                    }else if($user->hasRole('directurcatalog'))
                    {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/directur/dashboard')->with('key', 'value');
                    }
                   else if($user->hasRole('vendor'))
                   {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/vms/users/dashboard')->with('key', 'value');
                   }
                   else if($user->hasRole('checkervms'))
                   {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/vms/checker/dashboard')->with('key', 'value');
                    }else if($user->hasRole('aktivatorvms'))
                     {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/vms/activator/dashboard')->with('key', 'value');
                    }else if($user->hasRole('warehouse'))
                    {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/warehouse/index')->with('key', 'value');
                    }else if($user->hasRole('pphp'))
                     {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/pphp/dashboard')->with('key', 'value');
                     }else if($user->hasRole('vcs'))
                     {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/vcs/dashboard')->with('key', 'value');
                     }else if($user->hasRole('depo'))
                     {
                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/depo/index')->with('key', 'value');
                     }else if($user->hasRole('finance_akutansi'))
                     {

                       session(['login' => "sukses"]);
                       return redirect()->intended('/catalog/users/finance/bast_waiting')->with('key', 'value');
                     }
                   else{
                     Auth::logout();
                     session()->flash('message','Username atau password anda salah !!!');
                     // return redirect('/auth');
                    // dd($request->all());
                   }
               }else{

                   Auth::logout();
                   session()->flash('message','Username atau password anda salah !!!');
                   return redirect('/auth');
               }
       }




    }

    public function image(Request $request) {
      $user = User::where('email',$request->email);
      $image = $user->value('image');
      if($image == Null){
        $image = DB::table('users')
        ->select('user_catalog_company.image')
        ->join('user_catalog_company', 'users.id_user_catalog_company', '=', 'user_catalog_company.id')
        ->where('users.email', $request->email)
        ->value('image');
      }
      $image = $image == null ? 'xxx' : $image;
              return response()->json(array(
                                       'image' => $image,
                                       'status' => 1));
    }

    function vcs(){

      return view('vcs.auth.login');
    }

}
