<?php

namespace App\Http\Controllers\Catalog\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use App\Model\VendorDetail;
use App\Model\VendorDirectur;
use App\Model\VendorStatus;
use App\Model\Contract;
use App\Model\Adendum;
use URL;
use Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class AdminUsersCatalogController extends Controller
{

  public function __construct()
  {
      $this->middleware('adminauth');
  }

  public function index(Request $requests){

      if (Auth::check()) {
          // return view('catalog.users.directur.index', compact('data_company','company'))->with('id', $id);
          return view('catalog.admin.users.index');
      }else{
          return redirect('auth');
      }
  }

  public function status(Request $res)
  {
    // dd($res);
    $users = \App\User::find($res->user_id);
    $users->status = $res->status;
    $users->save();
    return view('catalog.admin.users.index');
  }

  public function create(Request $res)
  {
    // dd($res);
    $users = new \App\User;

    $username = explode("@", $res->username);

    $users->name = $res->nama;
    $users->username = $username[0];
    $users->email = $res->username;
    $users->password = Hash::make($res->password."123");
    $users->status = 1;
    $users->id_user_catalog_company = 1;

    $users->save();

    DB::table('role_user')->insert(
      ['role_id' => $res->role_id, 'user_id' => $users->id]
    );

    if ($res->role_id == 3) {
      $id_atasan = DB::table('user_catalog_supervisi')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog')->insert(
        [
          'id_user_catalog_company' => $users->id_user_catalog_company,
          'id_user_catalog_supervisi' => $id_atasan,
          'id_user' => $users->id,
          'username_catalog' => $users->username,
          'address' => '-',
          'telephone' => $res->mobile,
          'email' => $users->email,
        ]
      );
    } elseif ($res->role_id == 7) {
      $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_supervisi')->insert(
        [
          'id_user_manager' => $id_atasan,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 2) {
      $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_finance')->insert(
        [
          'id_user_manager' => $id_atasan,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 6) {
      // $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_manager')->insert(
        [
          // 'id_user_manager' => $id_atasan,
          'id_user_catalog_company' => $users->id_user_catalog_company,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 8) {
      // $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_directur')->insert(
        [
          // 'id_user_manager' => $id_atasan,
          'id_catalog_company' => $users->id_user_catalog_company,
          'id_user' => $users->id,
          'name' => $users->username,
          // 'email' => $users->email,
          'mobile_phone' => $res->mobile,
          // 'status' => "Y",
        ]
      );
    }

    return view('catalog.admin.users.index');
  }

  public function getData(Request $res)
  {

    $user = \App\User::find($res->id);
    $atasan = 1;

    if ($res->role == 8) {
      $mobile = DB::table('user_catalog_directur')->where('id_user', $res->id)->value('mobile_phone');
    } elseif ($res->role == 6) {
      // manager
      $mobile = DB::table('user_catalog_manager')->where('id_user', $res->id)->value('mobile_phone');
    } elseif ($res->role == 2) {
      // finance
      $mobile = DB::table('user_catalog_finance')->where('id_user', $res->id)->value('mobile_phone');
      $atasan = DB::table('user_catalog_finance')->where('id_user', $res->id)->value('id_user_manager');
      $atasan = DB::table('user_catalog_manager')->where('id', $atasan)->value('id_user');
    } elseif ($res->role == 7) {
      // supervisi
      $mobile = DB::table('user_catalog_supervisi')->where('id_user', $res->id)->value('mobile_phone');
      $atasan = DB::table('user_catalog_supervisi')->where('id_user', $res->id)->value('id_user_manager');
      $atasan = DB::table('user_catalog_manager')->where('id', $atasan)->value('id_user');
    } elseif ($res->role == 3) {
      // staff
      $mobile = DB::table('user_catalog')->where('id_user', $res->id)->value('telephone');
      $atasan = DB::table('user_catalog')->where('id_user', $res->id)->value('id_user_catalog_supervisi');
      $atasan = DB::table('user_catalog_supervisi')->where('id', $atasan)->value('id_user');
    }

    return response()
    ->json(array(
      'user' => $user,
      'role' => $res->role,
      'mobile' => $mobile,
      'atasan' => $atasan,
    ));
  }

  public function update(Request $res)
  {
    // dd($res);

    $users = \App\User::find($res->id);

    $username = explode("@", $res->username);

    $users->name = $res->nama;
    $users->username = $username[0];
    $users->email = $res->username;
    if ($res->password != null) {
      // code... update data password apabila ada perbaruan
      $users->password = Hash::make($res->password."123");
    }
    $users->save();

    if ($res->role_id == 3) {
      // code...
      $id_atasan = DB::table('user_catalog_supervisi')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog')->where('id_user', $res->id)->update([
        'id_user_catalog_company' => $users->id_user_catalog_company,
        'id_user_catalog_supervisi' => $id_atasan,
        'id_user' => $users->id,
        'username_catalog' => $users->username,
        'address' => '-',
        'telephone' => $res->mobile,
        'email' => $users->email,
      ]);
    } elseif ($res->role_id == 7) {
      $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_supervisi')->where('id_user', $res->id)->update(
        [
          'id_user_manager' => $id_atasan,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 2) {
      $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_finance')->where('id_user', $res->id)->update(
        [
          'id_user_manager' => $id_atasan,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 6) {
      // $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_manager')->where('id_user', $res->id)->update(
        [
          // 'id_user_manager' => $id_atasan,
          'id_user_catalog_company' => $users->id_user_catalog_company,
          'id_user' => $users->id,
          'name' => $users->username,
          'email' => $users->email,
          'mobile_phone' => $res->mobile,
          'status' => "Y",
        ]
      );
    } elseif ($res->role_id == 8) {
      // $id_atasan = DB::table('user_catalog_manager')->where('id_user', $res->atasan)->value('id');
      DB::table('user_catalog_directur')->where('id_user', $res->id)->update(
        [
          // 'id_user_manager' => $id_atasan,
          'id_catalog_company' => $users->id_user_catalog_company,
          'id_user' => $users->id,
          'name' => $users->username,
          // 'email' => $users->email,
          'mobile_phone' => $res->mobile,
          // 'status' => "Y",
        ]
      );
    }

    return view('catalog.admin.users.index');
  }

  public function category(Request $res)
  {
    dd($res);
  }
  public function getCategory(Request $res)
  {
    $category = DB::table('user_category')->where('user_id', $res->id)->get();

    return response()
    ->json(array(
      'category' => $category,
    ));
  }


  function direktur_penunjang(){

      $data = DB::table('user_catalog_supervisi as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->select('*')
           ->get();



       return view('catalog.admin.users.v_direktur_penunjang',compact('data'));
  }

  function direktur_penunjang_tambah(){
     $data = DB::table('category')
           ->where('id_parent',0)
           ->select('*')
           ->get();


      $data1 = DB::table('user_catalog_directur as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->select('*')
           ->get();




    return view('catalog.admin.users.direktur_penunjang_tambah',compact('data','data1'));
  }

  function direktur_penunjang_simpan(Request $request){

        $res =  $request->id_kategory;
        $id_kategory = implode(",",$res);



   // echo $requests->kode;

    // $data = array(
    //     'kode_instalasi'       =>$request->kode,
    //     'nama_instalasi'       =>$request->nama_instalasi,
    //     'kategory'             =>$request->kategory,
    //     'id_ppk'               =>$request->id_ppk,
    //   );

    //    $id=DB::table('master_direktorat_penunjang')->insertGetId($data);


       $data_lap = array(
                    // 'id_direktur_penunjang' =>  $id,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



    $data = array(
        'user_id'       =>$id_user,
        'role_id'       =>7,
      );

       $id=DB::table('role_user')->insertGetId($data);


        DB::table('user_catalog_supervisi')->insert(
        [
          'id_user_manager'      =>$request->id_ppk,
          'id_user'              => $id_user,
          'name'                 => $request->username,
          'email'                => $request->email,
          'mobile_phone'         => $request->no_tlpn,
          'kode_instalasi'       =>$request->kode,
          'nama_instalasi'       =>$request->nama_instalasi,
          'kategory'             =>$id_kategory,
          'status' => "Y",
        ]
      );



       return redirect()->to('/catalog/admin/users/direktur_penunjang')->with('message','Data Berhasil Disimpan');
  }

  function direktur_penunjang_delete($id){



           $data = DB::table('users')
           ->where('id',$id)
           ->select('id_direktur_penunjang')
           ->first();




        DB::table('user_catalog_supervisi')->where('id_user', $id)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

    return redirect()->to('/catalog/admin/users/direktur_penunjang')->with('message','Data Berhasil Dihapus');

  }

  function direktur_penunjang_edit($id){



     $data = DB::table('category')
           ->where('id_parent',0)
           ->select('*')
           ->get();

     $data1 = DB::table('user_catalog_supervisi as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->where('d.id_user',$id)
           ->select('*')
           ->first();






      $data2 = DB::table('user_catalog_directur')
           ->select('*')
           ->get();





   return view('catalog.admin.users.direktur_penunjang_edit',compact('data','data1','data2'));

  }

  function direktur_penunjang_edit_simpan(Request $request, $id){

    $res =  $request->id_kategory;
    $id_kategory = implode(",",$res);


         $data = DB::table('users')
           ->where('id',$id)
           ->select('id_direktur_penunjang')
           ->first();

           $id_dir = $data->id_direktur_penunjang;

        // $data = array(
        // 'kode_instalasi'       =>$request->kode,
        // 'nama_instalasi'       =>$request->nama_instalasi,
        // 'kategory'             =>$request->kategory,
        // 'id_ppk'               =>$request->id_ppk,
        // );

        // DB::table('master_direktorat_penunjang')->where('id',$id_dir)->update($data);


        if($request->password!=null){
             $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>   bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }



       DB::table('users')->where('id',$id)->update($data_lap);


        DB::table('user_catalog_supervisi')->where('id_user',$id)->update(
        [
          'id_user_manager'      =>$request->id_ppk,
          //'id_user'              => $id_user,
          'name'                 => $request->username,
          'email'                => $request->email,
          'mobile_phone'         => $request->no_tlpn,
          'kode_instalasi'       =>$request->kode,
          'nama_instalasi'       =>$request->nama_instalasi,
          'kategory'             =>$id_kategory,

        ]
      );



       return redirect()->to('/catalog/admin/users/direktur_penunjang')->with('message','Data Berhasil Disimpan');
  }




  function ppk(){

      $data = DB::table('user_catalog_directur as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->Join('master_jenis_gudang as j', 'j.id', '=', 'd.id_golongan')
           ->select('d.*','u.username','j.jenis_gudang')
           ->get();

    return view('catalog.admin.users.v_ppk',compact('data'));
  }

  function ppk_tambah(){


      $data = DB::table('master_direktorat_penunjang as d')
           ->leftJoin('users as u', 'u.id_direktur_penunjang', '=', 'd.id')
           ->select('*')
           ->get();


           $data1 = DB::table('master_jenis_gudang')
                ->select('*')
                ->get();

    return view('catalog.admin.users.ppk_tambah',compact('data','data1'));
  }

  function ppk_simpan(Request $request){
    //  $data = array(
    //     'kode_instansi'        =>$request->kode,
    //     'nama_instalasi'       =>$request->nama_instalasi,
    //     'nama_ppk'             =>$request->nama_ppk,
    //   );

    //    $id=DB::table('master_ppk')->insertGetId($data);


       $data_lap = array(
                    // 'id_ppk'                =>  $id,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



        $data = array(
            'user_id'       =>$id_user,
            'role_id'       =>8,
          );

       $id=DB::table('role_user')->insertGetId($data);


       DB::table('user_catalog_directur')->insert(
        [
          'id_catalog_company'      => '1',
          'id_user'              => $id_user,
          'name'                 => $request->username,
          'email'                => $request->email,
          'mobile_phone'         => $request->no_tlpn,
          'nik'                  => $request->nik,
          'kode_instalasi'       => $request->kode,
          'nama_instalasi'       => $request->nama_instalasi,
          'nama_ppk'             => $request->nama_ppk,
          'id_golongan'          => $request->id_golongan,

        ]
      );






       return redirect()->to('/catalog/admin/users/ppk')->with('message','Data Berhasil Disimpan');
  }

  function ppk_edit($id){

    $data1 = DB::table('user_catalog_directur as d')
           ->leftJoin('users as u', 'u.id', '=', 'd.id_user')
           ->where('d.id',$id)
           ->select('*')
           ->first();






    $data2 = DB::table('master_jenis_gudang')
            ->select('*')
            ->get();




      return view('catalog.admin.users.ppk_edit',compact('data1','data2'));
  }

  function ppk_edit_simpan(Request $request , $id){




        $data = DB::table('users')
           ->where('id',$id)
           ->select('id_ppk')
           ->first();




      //   $data = array(
      //  'kode_instansi'        =>$request->kode,
      //   'nama_instalasi'       =>$request->nama_instalasi,
      //   'nama_ppk'             =>$request->nama_ppk,
      //   );

      //   DB::table('master_ppk')->where('id',$id_dir)->update($data);


        if($request->password!=null){
             $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }

       DB::table('users')->where('id',$id)->update($data_lap);


       DB::table('user_catalog_directur')->where('id_user',$id)->update(
        [
          'id_catalog_company'      => '1',
          //'id_user'              => $id_user,
          'name'                 => $request->username,
          'email'                => $request->email,
          'mobile_phone'         => $request->no_tlpn,
          'nik'                  => $request->nik,
          'kode_instalasi'       => $request->kode,
          'nama_instalasi'       => $request->nama_instalasi,
          'nama_ppk'             => $request->nama_ppk,
          'id_golongan'          => $request->id_golongan,

        ]
      );

       //bcrypt($request->password);


       return redirect()->to('/catalog/admin/users/ppk')->with('message','Data Berhasil Disimpan');

  }

  function ppk_delete(Request $request , $id){


        $data = DB::table('users')
           ->where('id',$id)
           ->select('id_ppk')
           ->first();






        DB::table('user_catalog_directur')->where('id_user', $id)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

    return redirect()->to('/catalog/admin/users/ppk')->with('message','Data Berhasil Dihapus');
  }

  function pphp(){
    $data = DB::table('master_pphp as d')
           ->Join('users as u', 'u.id_pphp', '=', 'd.id')
           ->select('*')
           ->get();

    return view('catalog.admin.users.pphp.v_pphp',compact('data'));
  }

  function pphp_tambah(){
     return view('catalog.admin.users.pphp.pphp_tambah');
  }

  function pphp_simpan(Request $request){

       $data = array(
        'kode_instalasi'        =>$request->kode,
        'nama_instalasi'       =>$request->nama_instalasi,
        'nama'                 =>$request->nama,
        'posisi'               =>$request->posisi,
        'jenis'                =>$request->jenis,

      );

       $id=DB::table('master_pphp')->insertGetId($data);


       $data_lap = array(
                    'id_pphp'                =>  $id,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



        $data = array(
            'user_id'       =>$id_user,
            'role_id'       =>11,
          );

       $id=DB::table('role_user')->insertGetId($data);

       return redirect()->to('/catalog/admin/users/pphp')->with('message','Data Berhasil Disimpan');

  }

  function pphp_edit($id){

      $data1 = DB::table('master_pphp as d')
           ->leftJoin('users as u', 'u.id_pphp', '=', 'd.id')
           ->where('u.id',$id)
           ->select('*')
           ->first();

    return view('catalog.admin.users.pphp.pphp_edit',compact('data1'));
  }

  function pphp_edit_simpan(Request $request,$id){

      $data = DB::table('users')
           ->where('id',$id)
           ->select('id_pphp')
           ->first();

           $id_dir = $data->id_pphp;


    $data = array(
        'kode_instalasi'        =>$request->kode,
        'nama_instalasi'       =>$request->nama_instalasi,
        'nama'                 =>$request->nama,
        'posisi'               =>$request->posisi,
        'jenis'                =>$request->jenis,
        );

        DB::table('master_pphp')->where('id',$id_dir)->update($data);


        if($request->password!=null){
             $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }



       DB::table('users')->where('id',$id)->update($data_lap);


       //bcrypt($request->password);


       return redirect()->to('/catalog/admin/users/pphp')->with('message','Data Berhasil Disimpan');
  }

  function pphp_delete(Request $request,$id){
         $data = DB::table('users')
           ->where('id',$id)
           ->select('id_pphp')
           ->first();




        DB::table('master_pphp')->where('id', $data->id_pphp)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

    return redirect()->to('/catalog/admin/users/pphp')->with('message','Data Berhasil Dihapus');
    }

    function gudang(){
        $data = DB::table('master_gudang as d')
           ->Join('users as u', 'u.id_gudang', '=', 'd.id')
           ->select('*')
           ->get();

    return view('catalog.admin.users.gudang.v_gudang',compact('data'));
    }


    function gudang_tambah(){

         $data = DB::table('master_gudang')
           ->select('*')
           ->get();



      return view('catalog.admin.users.gudang.gudang_tambah',compact('data'));
    }
    function gudang_simpan(Request $request){

      // $data = array(
      //   'kode_instalasi'        =>$request->kode,
      //   'nama_instalasi'       =>$request->nama_instalasi,


      // );

      //  $id=DB::table('master_gudang')->insertGetId($data);


       $data_lap = array(
                    'id_gudang'             =>  $request->id_gudang,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



        $data = array(
            'user_id'       =>$id_user,
            'role_id'       =>9,
          );

       $id=DB::table('role_user')->insertGetId($data);

       return redirect()->to('/catalog/admin/users/gudang')->with('message','Data Berhasil Disimpan');

    }

    function gudang_edit($id){

        $data1 = DB::table('master_gudang as d')
           ->leftJoin('users as u', 'u.id_gudang', '=', 'd.id')
           ->where('u.id',$id)
           ->select('*')
           ->first();



         $data = DB::table('master_gudang')
           ->select('*')
           ->get();


      return view('catalog.admin.users.gudang.gudang_edit',compact('data1','data'));
    }

    function gudang_edit_simpan(Request $request,$id){

    //     $data = DB::table('users')
    //        ->where('id',$id)
    //        ->select('id_gudang')
    //        ->first();

    //        $id_dir = $data->id_gudang;


    // $data = array(
    //     'kode_instalasi'        =>$request->kode,
    //     'nama_instalasi'       =>$request->nama_instalasi,

    //     );

    //     DB::table('master_gudang')->where('id',$id_dir)->update($data);




        if($request->password!=null){
             $data_lap = array(
                    'id_gudang'             =>  $request->id_gudang,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'id_gudang'             =>  $request->id_gudang,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }



       DB::table('users')->where('id',$id)->update($data_lap);




       return redirect()->to('/catalog/admin/users/gudang')->with('message','Data Berhasil Disimpan');

    }

    function gudang_delete(Request $request,$id){
         $data = DB::table('users')
           ->where('id',$id)
           ->select('id_gudang')
           ->first();




        DB::table('master_gudang')->where('id', $data->id_gudang)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

    return redirect()->to('/catalog/admin/users/gudang')->with('message','Data Berhasil Dihapus');
    }




    function finance(){

        $data = DB::table('master_finance as d')
           ->Join('users as u', 'u.id_finance', '=', 'd.id')
           ->select('d.kode_instalasi','d.nama_instalasi','u.username','u.email','u.id')
           ->get();

        return view('catalog.admin.users.finance.v_finance',compact('data'));
    }

    function finance_tambah(){


      return view('catalog.admin.users.finance.finance_tambah');
    }

    function finance_simpan(Request $request){



      if($request->nama_instalasi=="Finance Akutansi"){
            $id_fin = 14;
      }elseif($request->nama_instalasi=="Finance Verifikasi"){
            $id_fin = 2;
      }



      $data = array(
        'kode_instalasi'        =>$request->kode,
        'nama_instalasi'       =>$request->nama_instalasi,
      );

       $id=DB::table('master_finance')->insertGetId($data);


       $data_lap = array(
                    'id_finance'            =>  $id,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    'image'                 =>  ' ',
                    'activation_key'        =>  ' ',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



        $data = array(
            'user_id'       =>$id_user,
            'role_id'       =>$id_fin,
          );

       $id=DB::table('role_user')->insertGetId($data);


       DB::table('user_catalog_finance')->insert(
         [
           'id_user_manager' => 1,
           'id_user' =>  Auth::user()->id,
           'name' => $request->username,
           'email' => $request->email,
           'mobile_phone' => $request->no_tlpn,
           'status' => "Y",
           'created_at'=>date('Y-m-d H:i:s'),
           'updated_at'=>date('Y-m-d H:i:s'),
         ]
       );


       return redirect()->to('/catalog/admin/users/finance')->with('message','Data Berhasil Disimpan');

    }

    function finance_edit($id){


      $data1 = DB::table('master_finance as d')
           ->leftJoin('users as u', 'u.id_finance', '=', 'd.id')
           ->where('u.id',$id)
           ->select('*')
           ->first();



      return view('catalog.admin.users.finance.finance_edit',compact('data1'));
    }

    function finance_edit_simpan(Request $request,$id){


            if($request->nama_instalasi=="Finance Akutansi"){
                  $id_fin = 14;
            }elseif($request->nama_instalasi=="Finance Verifikasi"){
                  $id_fin = 2;
            }



        $data = DB::table('users')
           ->where('id',$id)
           ->select('id_finance')
           ->first();


           echo "<pre>";
           print_r($data);
           echo "</pre>";
           
           $id_dir = $data->id_finance;






    $data = array(
        'kode_instalasi'        =>$request->kode,
        'nama_instalasi'       =>$request->nama_instalasi,

        );

        DB::table('master_finance')->where('id',$id_dir)->update($data);


        if($request->password!=null){
             $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }



       DB::table('users')->where('id',$id)->update($data_lap);


       DB::table('user_catalog_finance')->where('id_user',$id)->update(
         [
           'id_user_manager' => 1,
           'id_user' =>  Auth::user()->id,
           'name' => $request->username,
           'email' => $request->email,
           'mobile_phone' => $request->no_tlpn,
           'status' => "Y",
           'created_at'=>date('Y-m-d H:i:s'),
           'updated_at'=>date('Y-m-d H:i:s'),
         ]
       );


       return redirect()->to('/catalog/admin/users/finance')->with('message','Data Berhasil Disimpan');

    }

    function finance_delete(Request $request,$id){
         $data = DB::table('users')
           ->where('id',$id)
           ->select('id_finance')
           ->first();




        DB::table('master_finance')->where('id', $data->id_finance)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();
        DB::table('user_catalog_finance')->where('id_user', Auth::user()->id)->delete();

    return redirect()->to('/catalog/admin/users/finance')->with('message','Data Berhasil Dihapus');
    }

    function satuan(){

        $data = DB::table('unit_type')
           ->select('*')
           ->get();


       return view('catalog.admin.satuan.v_satuan',compact('data'));
    }

    function satuan_tambah(){
       return view('catalog.admin.satuan.satuan_tambah');
    }

    function satuan_simpan(Request $request){

    $data = array(
        'unit_name'        =>$request->nama_satuan,
        'status'           =>'1',
        'description'      =>$request->deskripsi,
        'convert_pcs'      => '1',
        'created_at'       => date('Y-m-d H:i:s'),
        'updated_at'       => date('Y-m-d H:i:s'),

        );

       DB::table('unit_type')->insert($data);

       return redirect()->to('/catalog/admin/users/satuan')->with('message','Data Berhasil Disimpan');

    }

    function satuan_edit($id){

        $data = DB::table('unit_type')
           ->where('id',$id)
           ->select('*')
           ->first();

       return view('catalog.admin.satuan.satuan_edit',compact('data'));
    }

    function satuan_edit_simpan(Request $request,$id){

    $data = array(
      'unit_name'        =>$request->nama_satuan,
      'status'             =>'1',
      'description'          =>$request->deskripsi,
      'convert_pcs'  => '1',
      'created_at'       => date('Y-m-d H:i:s'),
      'updated_at'       => date('Y-m-d H:i:s'),
    );

       DB::table('unit_type')->where('id',$id)->update($data);

       return redirect()->to('/catalog/admin/users/satuan')->with('message','Data Berhasil Disimpan');
    }

    function satuan_delete(Request $request,$id){

        DB::table('unit_type')->where('id', $id)->delete();

      return redirect()->to('/catalog/admin/users/satuan')->with('message','Data Berhasil Dihapus');
    }

    function jenis_gudang(){

      $data = DB::table('master_jenis_gudang')
           ->select('*')
           ->get();
      return view('catalog.admin.jenis_gudang.v_jenis_gudang',compact('data'));
    }

    function jenis_gudang_tambah(){

      return view('catalog.admin.jenis_gudang.jenis_gudang_tambah');
    }

    function jenis_gudang_simpan(Request $request){
         $data = array(
        'kode'                 =>$request->kode,
        'jenis_gudang'        =>$request->jenis_gudang,
        );

       DB::table('master_jenis_gudang')->insert($data);

       return redirect()->to('/catalog/admin/users/jenis_gudang')->with('message','Data Berhasil Disimpan');
    }

    function jenis_gudang_edit($id){
              $data = DB::table('master_jenis_gudang')
           ->where('id',$id)
           ->select('*')
           ->first();

       return view('catalog.admin.jenis_gudang.jenis_gudang_edit',compact('data'));
      }

    function jenis_gudang_edit_simpan(Request $request,$id){
       $data = array(
         'kode'                 =>$request->kode,
         'jenis_gudang'        =>$request->jenis_gudang,
        );

       DB::table('master_jenis_gudang')->where('id',$id)->update($data);

       return redirect()->to('/catalog/admin/users/jenis_gudang')->with('message','Data Berhasil Disimpan');
    }

    function jenis_gudang_delete(Request $request,$id){

        DB::table('master_jenis_gudang')->where('id', $id)->delete();

      return redirect()->to('/catalog/admin/users/jenis_gudang')->with('message','Data Berhasil Dihapus');
    }

    function master_gudang(){

          $data = DB::table('master_gudang as d')
          ->leftJoin('master_jenis_gudang as u', 'u.id', '=', 'd.id_jenis_gudang')
           ->select('d.*','u.jenis_gudang')
           ->get();



        return view('catalog.admin.master_gudang.v_gudang',compact('data'));
    }

    function master_gudang_tambah(){


      $data = DB::table('master_jenis_gudang')
           ->select('*')
           ->get();

        return view('catalog.admin.master_gudang.master_gudang_tambah',compact('data'));
    }

    function master_gudang_simpan(Request $request){

          $data = array(
          'kode_instalasi'        =>$request->kode,
          'nama_instalasi'       =>$request->nama_instalasi,
         'id_jenis_gudang'       =>$request->id_jenis_gudang,

      );

       DB::table('master_gudang')->insert($data);


        return redirect()->to('/catalog/admin/users/master_gudang')->with('message','Data Berhasil Disimpan');

    }

     function master_gudang_edit($id){

      $data1 = DB::table('master_gudang')
           ->where('id',$id)
           ->select('*')
           ->first();


      $data = DB::table('master_jenis_gudang')
           ->select('*')
           ->get();


        return view('catalog.admin.master_gudang.master_gudang_edit',compact('data1','data'));
    }

    function master_gudang_edit_simpan(Request $request,$id){



          $data = array(
          'kode_instalasi'        =>$request->kode,
          'nama_instalasi'       =>$request->nama_instalasi,
          'id_jenis_gudang'       =>$request->id_jenis_gudang,

      );

       DB::table('master_gudang')->where('id',$id)->update($data);


      return redirect()->to('/catalog/admin/users/master_gudang')->with('message','Data Berhasil Disimpan');



    }

    function master_gudang_delete(Request $request,$id){
           DB::table('master_gudang')->where('id', $id)->delete();

      return redirect()->to('/catalog/admin/users/master_gudang')->with('message','Data Berhasil Dihapus');
    }

    function direktur_main_category(Request $request){

      error_reporting(0);
         $request->id_kategory;

        $a = explode(",",$request->id_kategory);


           $data1 = DB::table('category')
           ->WhereIn('id',$a)
           ->select('name','id')
           ->get();


        $data.="<table class='table table-hover table-bordered' ><thead><tr><th>Kategory</th></tr></thead>";
        foreach ($data1 as $sub) {
            $data.="<tr><td>$sub->name</td></tr> ";

        }
        $data.="</table>";
        echo$data;
    }

    function inisial_gudang(){

      $data = DB::table('category_warehouse_type as cw')

                  ->leftjoin('category as c', 'c.id','=','cw.id_category')
                  ->leftjoin('master_gudang as m','m.id','=','cw.id_gudang')
                  ->select('cw.*','c.name','m.nama_instalasi')
                  ->get();
          return view('catalog.admin.inisial_gudang.v_inisial_gudang',compact('data'));
    }

    function inisial_gudang_tambah(){

      $category = DB::table('category')
            ->where('id_parent',0)
            ->select('*')
            ->get();


      $gudang = DB::table('master_gudang')
                  ->select('*')
                  ->get();

        return view('catalog.admin.inisial_gudang.tambah_inisial_gudang',compact('category','gudang'));
    }

    function inisial_gudang_simpan(Request $request){


      $data = array(
      'id_gudang'        =>$request->id_gudang,
      'id_category'      =>$request->id_category,
      'status'           =>$request->status,
      'description'      =>$request->deskripsi,
      'created_at'       =>date('Y-m-d h:i:s'),
      'updated_at'      =>date('Y-m-d h:i:s'),
      );
      DB::table('category_warehouse_type')->insert($data);
    return redirect()->to('/catalog/admin/users/inisial_gudang')->with('message','Data Berhasil Disimpan');
    }

  function inisial_gudang_edit($id){
    $category = DB::table('category')
          ->where('id_parent',0)
          ->select('*')
          ->get();


    $gudang = DB::table('master_gudang')
                ->select('*')
                ->get();

    $data = DB::table('category_warehouse_type')
    ->where('id',$id)
    ->select('*')
    ->first();


      return view('catalog.admin.inisial_gudang.edit_inisial_gudang',compact('category','gudang','data'));
  }

  function inisial_gudang_edit_simpan(Request $request,$id){
    $data = array(
    'id_gudang'        =>$request->id_gudang,
    'id_category'      =>$request->id_category,
    'status'           =>$request->status,
    'description'      =>$request->deskripsi,
    'created_at'       =>date('Y-m-d h:i:s'),
    'updated_at'      =>date('Y-m-d h:i:s'),
    );
    DB::table('category_warehouse_type')->where('id',$id)->update($data);
   return redirect()->to('/catalog/admin/users/inisial_gudang')->with('message','Data Berhasil Disimpan');

  }

  function inisial_gudang_delete(Request $request,$id){
        DB::table('category_warehouse_type')->where('id', $id)->delete();
        return redirect()->to('/catalog/admin/users/inisial_gudang')->with('message','Data Berhasil Disimpan');

  }

  function master_vendor(){

    $data = DB::table('vendor_detail as v')
                 ->leftjoin('contract as m','m.id_vendor_detail','=','v.id')
                 ->select('v.*','m.data')
                 ->get();

      return view('catalog.admin.master_vendor.v_vendor',compact('data'));
  }

  function master_vendor_tambah(){
        return view('catalog.admin.master_vendor.vendor_tambah');
  }

  function master_vendor_simpan(Request $request){

    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    // generate a pin based on 2 * 7 digits + a random character
    $pin = mt_rand(1000000, 9999999)
        . mt_rand(1000000, 9999999)
        . $characters[rand(0, strlen($characters) - 1)];

    // shuffle the result
$string = str_shuffle($pin);


    $data2 = array(
        'username'           =>$request->nama_vendor,
        'email'              =>$request->email,
        'activation_key'     =>$string,
        'created_at'         =>date('Y-m-d h:i:s'),
        'updated_at'         =>date('Y-m-d h:i:s'),
    );

    $id1 =  DB::table('users')->insertGetId($data2);


               try{
                   Mail::send('catalog.admin.layouts.email_vendor', ['nama' => $request->email, 'pesan' => 'cek'], function ($message) use ($request)
                   {
                       $message->subject('tes');
                       $message->from('donotreply@kiddy.com', 'Kiddy');
                       $message->to('ajie.darmawan106@gmail.com');
                   });
                   return back()->with('alert-success','Berhasil Kirim Email');
                   }
                   catch (Exception $e){
                       return response (['status' => false,'errors' => $e->getMessage()]);
                   }




    $photo = $request->file('upload');
    $fileName = $photo->getClientOriginalName();
    $destination = base_path().'/public/assets/surat_kontrak';
    $photo->move($destination,$fileName);


      $kode             = $request->kode;
      $nama_vendor      = $request->nama_vendor;
      $email            = $request->email;
      $no_hp            = $request->no_hp;
      $no_surat_kontrak = $request->no_surat_kontrak;
      $file             = $request->file;

      $data = array(
          'vendor_name'        =>$nama_vendor,
          'mobile_phone'       =>$no_hp,
          'email'              =>$email,
          'id_user'            =>$id1,
          'kode_vendor'        =>$kode,
          'created_at'         =>date('Y-m-d h:i:s'),
          'updated_at'         =>date('Y-m-d h:i:s'),
      );

       $id=DB::table('vendor_detail')->insertGetId($data);

       $data1 = array(
           'no_contract'        =>$request->no_surat_kontrak,
           'id_vendor_detail'   =>$id,
           'start_date'         =>$request->start,
           'end_date'           =>$request->end,
           'status'             =>1,
           'description'        =>$request->ket,
           'data'               =>$fileName,
           'created_at'         =>date('Y-m-d h:i:s'),
           'updated_at'         =>date('Y-m-d h:i:s'),
       );

        DB::table('contract')->insert($data1);

    return redirect()->to('/catalog/admin/users/master_vendor')->with('message','Data Berhasil Disimpan');



  }

  function master_vendor_edit($id){



    $data = DB::table('vendor_detail')
                ->where('id',$id)
                ->select('*')
                ->first();

    return view('catalog.admin.master_vendor.vendor_edit',compact('data'));
  }

  function depo(){

      $data = DB::table('master_depo as m')
                  ->leftJoin('master_gudang as d', 'm.id_gudang', '=', 'd.id')
                  ->select('m.*','d.nama_instalasi')
                  ->get();

      return view('catalog.admin.master_depo.v_depo',compact('data'));

  }

  function depo_tambah(){

      $data = DB::table('master_gudang as g')

                ->select('*')
                ->get();

      return view('catalog.admin.master_depo.depo_tambah',compact('data'));
  }

  function depo_simpan(Request $request){
    $data = array(
    'id_gudang'        =>$request->id_gudang,
    'nama_depo'      =>$request->nama_depo,
    'kode_depo'        =>$request->kode_depo,
    'status'           =>$request->status,
    );
    DB::table('master_depo')->insert($data);
  return redirect()->to('/catalog/admin/users/depo')->with('message','Data Berhasil Disimpan');

  }

  function depo_edit(Request $request,$id){
    $data = DB::table('master_depo')
              ->where('id','=',$id)
              ->select('*')
              ->first();

              $data1 = DB::table('master_gudang')
                        ->select('*')
                        ->get();




    return view('catalog.admin.master_depo.depo_edit',compact('data','data1'));

  }

  function depo_edit_simpan(Request $request,$id){
    $data = array(
    'id_gudang'        =>$request->id_gudang,
    'nama_depo'      =>$request->nama_depo,
    'kode_depo'        =>$request->kode_depo,
    'status'           =>$request->status,
    );
    DB::table('master_depo')->where('id',$id)->update($data);
    return redirect()->to('/catalog/admin/users/depo')->with('message','Data Berhasil Disimpan');
  }

  function depo_delete(Request $request,$id){
      DB::table('master_depo')->where('id', $id)->delete();
      return redirect()->to('/catalog/admin/users/depo')->with('message','Data Berhasil Dihapus');
  }

  function users_depo(){
    $data = DB::table('user_depo as m')
    ->leftJoin('users as d', 'm.id_users', '=', 'd.id')
    ->select('m.*','d.username')
    ->get();

    return view('catalog.admin.master_depo.v_user_depo',compact('data'));
  }

  function users_depo_tambah(){


    $data = DB::table('master_depo')
                ->select('*')
                ->get();




    return view('catalog.admin.master_depo.user_tambah_depo',compact('data'));
  }

  function users_depo_simpan(Request $request)
  {


    $data_lap = array(
                 // 'id_direktur_penunjang' =>  $id,
                 'name'                  =>  $request->username,
                 'username'              =>  $request->username,
                 'email'                 =>  $request->email,
                 'password'              =>  bcrypt($request->password),
                 'status'                =>  '1',
                 );
    $id_user = DB::table('users')->insertGetId($data_lap);

     $data = array(
         'user_id'       =>$id_user,
         'role_id'       =>13,
       );

    $id=DB::table('role_user')->insertGetId($data);

    $data_depo = array(
    'id_depo'           =>$request->id_depo,
    'id_users'          =>$id_user,
    'nama'              =>$request->nama,
    'nik'               =>$request->nik,
    'email'             =>$request->email,
    'no_tlpn'           =>$request->no_tlpn,
    );
    DB::table('user_depo')->insert($data_depo);
    return redirect()->to('/catalog/admin/users/users_depo')->with('message','Data Berhasil Disimpan');

  }

  function users_depo_edit(Request $request,$id){

    $data1 = DB::table('user_depo as m')
                ->leftJoin('users as d', 'm.id_users', '=', 'd.id')
                ->where('m.id','=',$id)
                ->select('m.*','d.username')
                ->first();

                $data = DB::table('master_depo')
                        ->select('*')
                        ->get();



    return view('catalog.admin.master_depo.user_edit_depo',compact('data','data1'));

  }

  function users_depo_edit_simpan(Request $request,$id){


      $data = DB::table('user_depo')
      ->where('id',$id)
      ->select('id_users')
      ->first();



      $id_us  = $data->id_users;







        if($request->password!=null){
                  $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                         );
             }else{
                $data_lap = array(
                         'name'                  =>  $request->username,
                         'username'              =>  $request->username,
                         'email'                 =>  $request->email,
                         // 'password'              =>  Hash::make($request->password."123"),
                         'status'                =>  '1',
                         );
             }

            DB::table('users')->where('id',$id_us)->update($data_lap);


             DB::table('user_depo')->where('id',$id)->update(
             [
               'id_depo'           =>$request->id_depo,
               //'id_users'          =>$id_user,
               'nama'              =>$request->nama,
               'nik'               =>$request->nik,
               'email'             =>$request->email,
               'no_tlpn'           =>$request->no_tlpn,

             ]
           );

          return redirect()->to('/catalog/admin/users/users_depo')->with('message','Data Berhasil Disimpan');


  }

  function users_depo_delete(Request $request,$id){

    $data = DB::table('user_depo')
    ->where('id',$id)
    ->select('id_users')
    ->first();

    $id1 = $data->id_users;

       DB::table('user_depo')->where('id', $id)->delete();
       DB::table('role_user')->where('user_id', $id1)->delete();
       DB::table('users')->where('id', $id1)->delete();

       return redirect()->to('/catalog/admin/users/users_depo')->with('message','Data Berhasil DiHapus');
  }

  function konversi(){

    $data = DB::table('warehouse_default_unit as w')
          ->leftJoin('master_gudang as g', 'g.id', '=', 'w.id_gudang')
          ->leftJoin('master_satuan as s', 's.id', '=', 'w.satuan')
          ->select('w.*','g.nama_instalasi','s.nama_satuan')
          ->get();


    return view('catalog.admin.konversi.v_konversi',compact('data'));
  }

  function konversi_tambah(){

    $data = DB::table('category')
          ->where('id_parent',0)
          ->select('*')
          ->get();

    $gudang =   DB::table('master_gudang')
                ->select('*')
                ->where('nama_instalasi', 'not like', '%farmasi%')
                ->get();

     $satuan =   DB::table('master_satuan')
                 ->select('*')
                 ->get();


    return view('catalog.admin.konversi.konversi_tambah',compact('data','gudang','satuan'));


  }

  function konversi_simpan(Request $request){

    $res =  $request->id_kategory;
    $id_kategory = implode(",",$res);


   $data_lap = array(

                'id_category'            =>  $id_kategory,
                'id_gudang'              =>  $request->id_gudang,
                'satuan'                 =>  $request->satuan,
                'status'                =>  $request->status,
                );
   DB::table('warehouse_default_unit')->insert($data_lap);
    return redirect()->to('/catalog/admin/users/konversi')->with('message','Data Berhasil DiSimpan');

  }

  function konversi_edit($id){


    $edit = DB::table('warehouse_default_unit as w')
          ->leftJoin('master_gudang as g', 'g.id', '=', 'w.id_gudang')
          ->leftJoin('master_satuan as s', 's.id', '=', 'w.satuan')
          ->select('w.*','g.nama_instalasi','s.nama_satuan')
          ->where('w.id','=',$id)
          ->first();



    $data = DB::table('category')
          ->where('id_parent',0)
          ->select('*')
          ->get();

    $gudang =   DB::table('master_gudang')
                ->select('*')
                ->where('nama_instalasi', 'not like', '%farmasi%')
                ->get();

     $satuan =   DB::table('master_satuan')
                 ->select('*')
                 ->get();


    return view('catalog.admin.konversi.konversi_edit',compact('data','gudang','satuan','edit'));
  }


  function konversi_edit_simpan(Request $request,$id){
    $res =  $request->id_kategory;
    $id_kategory = implode(",",$res);


   $data_lap = array(

                'id_category'            =>  $id_kategory,
                'id_gudang'              =>  $request->id_gudang,
                'satuan'                 =>  $request->satuan,
                'status'                =>  $request->status,
                );
    DB::table('warehouse_default_unit')->where('id',$id)->update($data_lap);

    return redirect()->to('/catalog/admin/users/konversi')->with('message','Data Berhasil DiSimpan');

  }

  function konversi_delete(Request $request,$id){
       DB::table('warehouse_default_unit')->where('id', $id)->delete();
       return redirect()->to('/catalog/admin/users/konversi')->with('message','Data Berhasil DiSimpan');

  }

  // public function vendor(Request $request) {
  //       $data = DB::table('vendor_detail')->select('vendor_detail.id AS id_vendor', 'vendor_detail.code', 'vendor_detail.vendor_name', 'vendor_detail.bank', 'vendor_detail.no_rek', 'vendor_detail.email', 'vendor_directur.name', 'vendor_directur.nik', 'vendor_status.last_status')
  //                ->join('vendor_directur', 'vendor_detail.id', '=', 'vendor_directur.id_vendor_detail')
  //                ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
  //                ->where('vendor_status.last_status', 6)
  //                ->get();
  //
  //       return view('catalog.admin.vendor.index', compact('data'));
  //     }
  //
  //
  //     public function vendorCreate(Request $request) {
  //       $propinsi = Fungsi::propinsi();
  //       return view('catalog.admin.vendor.create', compact('propinsi'));
  //     }
  //
  //     public function vendorStore(Request $request) {
  //       $validate = $request->validate([
  //                  'code' => 'required',
  //                  'vendor_name' => 'required',
  //                  'vendor_directur' => 'required',
  //                  'address' => 'required',
  //                  'province' => 'required',
  //                  'city' => 'required',
  //                  'telephone' => 'required',
  //                  'email' => 'required|email',
  //                  'password' => 'required|',
  //                  'status' => 'required'
  //              ]);
  //
  //        DB::beginTransaction();
  //
  //        try {
  //
  //            $users = User::create([
  //              'name' => $request->vendor_name,
  //              'username' => $request->vendor_name,
  //              'email' => $request->email,
  //              'password' => bcrypt($request->password),
  //              'status' => '1'
  //            ]);
  //
  //            $users->roles()->sync('4');
  //            $users->roles()->sync('12');
  //
  //            $vendorDetail = VendorDetail::create([
  //              'code' =>  $request->code,
  //              'id_user'  =>  $users->id,
  //              'vendor_name'  =>  $request->vendor_name,
  //              'address'  =>  $request->address,
  //              'province'  =>  $request->province,
  //              'city'  =>  $request->city,
  //              'telephone'  =>  $request->telephone,
  //              'email'  =>  $request->email,
  //              'bank'  =>  $request->bank == null ? '-' : $request->bank,
  //              'no_rek'  =>  $request->no_rek == null ? '-' : $request->no_rek
  //            ]);
  //
  //            $vendorDirectur = VendorDirectur::create(['id_vendor_detail' => $vendorDetail->id, 'name'  =>  $request->vendor_directur, 'nik'  =>  '-']);
  //
  //            $statusVendor = VendorStatus::create(['id_vendor_detail' =>  $vendorDetail->id, 'id_admin_vms' =>  '138', 'last_status' => '6']);
  //
  //        } catch (\Illuminate\Database\QueryException $e) {
  //          DB::rollback();
  //          return redirect('/catalog/admin/users/vendor/')->with('message','Data Gagal di Simpan');
  //        }
  //        DB::commit();
  //
  //       return redirect('/catalog/admin/users/vendor/')->with('message','Data Berhasil di Simpan');
  //
  //     }



       public function vendor(Request $request) {
         $data = DB::table('vendor_detail')->select('vendor_detail.id AS id_vendor','vendor_detail.*',
         'vendor_detail.code', 'vendor_detail.vendor_name',
         'vendor_detail.bank', 'vendor_detail.no_rek',
         'vendor_detail.email', 'vendor_directur.name', 'vendor_directur.nik', 'vendor_status.last_status')
                  ->join('vendor_directur', 'vendor_detail.id', '=', 'vendor_directur.id_vendor_detail')
                  ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                  ->where('vendor_status.last_status', 6)
                  ->orderBy('vendor_detail.id', 'desc')
                  ->get();


            //   echo "<pre>";
            //   print_r($data);
            //   echo "</pre>";
            // die;

         return view('catalog.admin.vendor.index', compact('data'));
       }


       public function vendorCreate(Request $request) {
         $propinsi = Fungsi::propinsi();
         return view('catalog.admin.vendor.create', compact('propinsi'));
       }

       public function vendorStore(Request $request) {
         $validate = $request->validate([
                    'code' => 'required',
                    'vendor_name' => 'required',
                    'vendor_directur' => 'required',
                    'address' => 'required',
                    'province' => 'required',
                    'city' => 'required',
                    'telephone' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|',
                    'status' => 'required'
                ]);

          DB::beginTransaction();

          try {

              $users = User::create([
                'name' => $request->vendor_name,
                'username' => $request->vendor_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => '1'
              ]);

              $users->roles()->sync('4');
              $users->roles()->sync('12');

              $vendorDetail = VendorDetail::create([
                'code' =>  $request->code,
                'id_user'  =>  $users->id,
                'vendor_name'  =>  $request->vendor_name,
                'address'  =>  $request->address,
                'province'  =>  $request->province,
                'city'  =>  $request->city,
                'telephone'  =>  $request->telephone,
                'email'  =>  $request->email,
                'bank'  =>  $request->bank == null ? '-' : $request->bank,
                'no_rek'  =>  $request->no_rek == null ? '-' : $request->no_rek,
                'atas_nama'  =>  $request->on_behalft == null ? '-' : $request->on_behalft
              ]);

              // Mail::send('catalog.users.email.email_atasan', ['nama' => $request->vendor_name, 'pesan' => 'Silahkan melakukan login dengan email '.$request->email.' dengan password standar '.$request->password], function ($message) use ($request)
              // {
              //     $message->subject('tes');
              //     $message->from('donotreply@kiddy.com', 'Kiddy');
              //     $message->to('ajie.darmawan106@gmail.com');
              // });

              $vendorDirectur = VendorDirectur::create(['id_vendor_detail' => $vendorDetail->id, 'name'  =>  $request->vendor_directur, 'nik'  =>  '-']);
              $statusVendor = VendorStatus::create(['id_vendor_detail' =>  $vendorDetail->id, 'id_admin_vms' =>  '138', 'last_status' => '6']);

          } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            return redirect('/catalog/admin/users/vendor/')->with('message','Data Gagal di Simpan');
          }
          DB::commit();

         return redirect('/catalog/admin/users/vendor/')->with('message','Data Berhasil di Simpan');

       }

       function vendor_edit(Request $request ,$id)
       {

         $data = DB::table('vendor_detail')->select('vendor_detail.id AS id_vendor','vendor_detail.*',
         'vendor_detail.code', 'vendor_detail.vendor_name',
         'vendor_detail.bank', 'vendor_detail.no_rek',
         'vendor_detail.email', 'vendor_directur.name', 'vendor_directur.nik', 'vendor_status.last_status')
                  ->join('vendor_directur', 'vendor_detail.id', '=', 'vendor_directur.id_vendor_detail')
                  ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                  ->where('vendor_status.last_status', 6)
                  ->where('vendor_detail.id', $id)

                  ->first();

          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";

          $propinsi = Fungsi::propinsi();

          // echo "<pre>";
          // print_r($propinsi[2]);
          // echo "</pre>";
          // die;
          return view('catalog.admin.vendor.edit', compact('propinsi','data'));

       }

       function vendor_edit_simpan(Request $request,$id){

          $id_user1 = $request->id_user;


          if($request->password!=null)
          {

            DB::table('users')->where('id', $id_user1)->update(
              [
                'name' => $request->vendor_name,
                'username' => $request->vendor_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => '1'
              ]
            );


          }else{
            DB::table('users')->where('id', $id_user1)->update(
              [
                'name' => $request->vendor_name,
                'username' => $request->vendor_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => '1'
              ]
            );
          }


         DB::table('vendor_detail')->where('id',$id)->update
         (
          [
            //'code'          =>  $request->code,
          //  'id_user'       =>  $users->id,
            'vendor_name'   =>  $request->vendor_name,
            'address'       =>  $request->address,
            'province'      =>  $request->province,
            'city'          =>  $request->city,
            'telephone'     =>  $request->telephone,
            'mobile_phone'  =>  $request->mobile_phone,
            'atas_nama'     =>  $request->atas_nama,
            'email'         =>  $request->email,
            'bank'          =>  $request->bank == null ? '-' : $request->bank,
            'no_rek'        =>  $request->no_rek == null ? '-' : $request->no_rek,
            'atas_nama'     =>  $request->on_behalft == null ? '-' : $request->on_behalft

          ]
        );


        DB::table('vendor_directur')->where('id_vendor_detail',$id)->update
        (
         [

           'name'   =>  $request->vendor_directur,


         ]
       );


             return redirect('/catalog/admin/users/vendor/')->with('message','Data Berhasil di Simpan');

       }

       function vendor_delete(Request $request,$id){


          //echo $id;


          $data = DB::table('vendor_detail')
          ->where('id',$id)
          ->select('id_user')
          ->first();

          $id_user1 = $data->id_user;

           DB::table('vendor_detail')->where('id', $id)->delete();
           DB::table('role_user')->where('user_id', $id_user1)->delete();
           DB::table('users')->where('id', $id_user1)->delete();


          return redirect('/catalog/admin/users/vendor/')->with('message','Data Berhasil di Hapus');

       }


       public function contractIndex() {


         $contract = DB::table('vendor_detail')
                    ->select('vendor_detail.id AS id_vendor', 'vendor_detail.vendor_name', DB::raw('COUNT(contract.id) AS jumlah_kontrak'))
                    ->join('contract', 'vendor_detail.id', '=', 'contract.id_vendor_detail')
                    ->groupBy('vendor_detail.id', 'vendor_detail.vendor_name')
                    ->get();
         return view('catalog.admin.contract.index', compact('contract'));
       }

       public function contractCreate(){
         $vendor = DB::table('vendor_detail')
                  ->select('vendor_detail.id AS id_vendor', 'vendor_detail.vendor_name')
                  ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                  ->where('vendor_status.last_status', 6)
                  ->get();
         return view('catalog.admin.contract.create', compact('vendor'));
       }

       public function contractStore(Request $request) {
         $file = $request->file('data');
         DB::beginTransaction();

         try {

           foreach ($request->no_contract as $key => $value) {
             $contract = Contract::create([
               'no_contract' => $value,
               'id_vendor_detail' => $request->id_vendor_detail,
               'start_date' => $request->start_date[$key],
               'end_date' => $request->end_date[$key],
               'status' => 1,
               'description' => $request->description[$key] !== null ? $request->description[$key] : '-',
               'data' => "KONTRAK".date('Yms')."".$key."".$request->id_vendor_detail.".".$file[$key]->getClientOriginalExtension()
             ]);

             $destinationPath = 'assets/document/'.$request->id_vendor_detail.'/contract';

             if(!File::exists($destinationPath))
             File::makeDirectory($destinationPath, 0775, true);

             $pindah = $file[$key]->move($destinationPath, "KONTRAK".date('Yms')."".$key."".$request->id_vendor_detail.".".$file[$key]->getClientOriginalExtension());
             // code...
           }
           session()->flash('message', 'Pembuatan kontrak sukses');

         } catch (\Illuminate\QueryException $e) {
           DB::rollback();
           session()->flash('message', 'Pembuatan kontrak gagal');
         }
           DB::commit();


         return redirect('catalog/admin/contract');

       }

       public function contractDetail($id) {
         $contract = Contract::where(DB::raw('md5(CAST(id_vendor_detail AS varchar(50)))'), $id)->get();
         return view('catalog.admin.contract.detail', compact('contract'));

       }


       public function adendumCreate(Request $request, $id) {
         $id_vendor_detail = VendorDetail::where('id_user', Auth::user()->id)->value('id');
         $contract = Contract::where(DB::raw('md5(cast(id as varchar(50)))'), $id)->first();
         return view('catalog.admin.adendum.create', compact('contract', 'id'));
       }

       public function adendumStore(Request $request) {
         $contract = DB::table('adendum')->select(DB::raw('MAX(id) AS jml', 'id_contract'))->where('id_contract', $request->id_contract)->groupBy('id_contract');

         $vendorDetail = Contract::where('id', $request->id_contract)->first();
         $file = $request->file('data');

         DB::beginTransaction();
         $date = date('Yms');
         try {
           $contract = Adendum::create([
             'id_contract' => $request->id_contract,
             'no_adendum' => $request->no_adendum,
             'id_vendor_detail' => $vendorDetail->id_vendor_detail,
             'start_date' => $request->start_date,
             'end_date' => $request->end_date,
             'status' => 1,
             'description' => $request->description,
             'data' => "ADENDUM$date".$vendorDetail->id_vendor_detail.".".$file->getClientOriginalExtension()
           ]);

           $destinationPath = 'assets/document/'.$vendorDetail->id_vendor_detail.'/adendum';

           if(!File::exists($destinationPath))
           File::makeDirectory($destinationPath, 0775, true);

           session()->flash('message', 'Pembuatan adendum sukses');
           $pindah = $file->move($destinationPath, "ADENDUM$date".$vendorDetail->id_vendor_detail.".".$file->getClientOriginalExtension());
         } catch (\Illuminate\QueryException $e) {
           DB::rollback();
           dd($e->getMessage());
           session()->flash('message', 'Pembuatan adendum gagal');
         }
           DB::commit();


         return redirect('catalog/admin/contract/adendum/'.md5($request->id_contract));

       }


         public function adendumIndex(Request $request, $id) {
           $data = Adendum::where(DB::raw('md5(cast(id_contract as varchar(50)))'), $id)->get();
           return view('catalog.admin.adendum.index', compact('data', 'id'));
         }

}
