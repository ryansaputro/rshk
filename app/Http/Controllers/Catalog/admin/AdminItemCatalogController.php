<?php
namespace App\Http\Controllers\Catalog\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Item;
use App\Model\CategoryItem;
use App\Model\ItemPriceHistory;
use App\Model\ItemSpec;
use App\Model\ItemImage;
use App\Model\ItemDoc;
use App\Model\Supplier;
use App\Model\Category;
use App\Model\Contract;
use App\Model\Adendum;
use App\Model\GeneralSpec;
use App\Model\ItemUploadReject;
use App\Model\UnitType;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use Excel;
use Ixudra\Curl\Facades\Curl;


class AdminItemCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('adminauth');
    }

    public function index(Request $requests){
        if (Auth::check()) {
            $vendor = DB::table('vendor_detail')
                        ->select('vendor_detail.id AS id_vendor', 'vendor_detail.vendor_name')
                        ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                        ->where('vendor_status.last_status', 6)
                        ->get();

            $categorys = Category::where('status', 'Y')->get();
            return view('catalog.admin.item.index', compact('vendor', 'categorys'));
        }else{
            return redirect('auth');
        }
    }



    public function create(Request $request, $id) {
      $supplier = DB::table('vendor_detail')
                  ->select('vendor_detail.*','vendor_status.last_status')
                  ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                  ->where('vendor_status.last_status','1')
                  ->get();
      return view('catalog.admin.item.create',compact('supplier'))->with('id', $id);
    }

    public function store(Request $request) {

      $category = Item::create([
         "code" => $request->code,
         "name" => $request->name,
         "merk" => $request->merk,
         "production_origin" => $request->production_origin,
         "price_retail" => str_replace(".","", $request->price_retail),
         "price_gov" => str_replace(".","", $request->price_gov),
         "price_shipment" => str_replace(".","", $request->price_shipment),
         "price_date" => $request->price_date,
         "release_date" => $request->release_date,
         "expired_date" => $request->expired_date,
         "description" => $request->description,
         "status" => "Y"
        ]);

        $catItem = CategoryItem::create([
           "id_category" => $request->id_category,
           "id_item" => $category->id
        ]);

        $priceHistory = ItemPriceHistory::create([
          "id_item" => $category->id,
          "price_retail" => str_replace(".","", $request->price_retail),
          "price_gov" => str_replace(".","", $request->price_gov),
          "price_date" => $request->price_date,
          "price_shipment" => str_replace(".","", $request->price_shipment),
        ]);

        $supplier = Supplier::create([
          "id_supplier" => $request->id_supplier,
          "id_item" => $category->id
        ]);

        return view('catalog.admin.item.index')->with('message', 'Item Created successful');
    }

    public function DeleteItem(Request $request, $id) {
      $listCat = CategoryItem::where('id_item', $id)->value('id_category');
      $category = Item::where('id', $id)->delete();
      $catItem = CategoryItem::where('id_item', $id)->delete();
      return view('catalog.admin.item.list')->with(array('message'=> 'Item Deleted successful', 'id'=> $listCat));
    }

    public function edit(Request $request, $id) {
      $data = Item::where('id',$id)->first();
      $supplier = DB::table('vendor_detail')
                  ->select('vendor_detail.*','vendor_status.last_status')
                  ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                  ->where('vendor_status.last_status','6')
                  ->get();
      $supplierSelected = DB::table('vendor_detail')
                        ->select('vendor_detail.*','vendor_status.last_status')
                        ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                        ->join('supplier_item', 'vendor_detail.id', '=', 'supplier_item.id_supplier')
                        ->where('supplier_item.id_item',$id)
                        ->value('id');
      return view('catalog.admin.item.update', compact('data', 'supplier', 'supplierSelected'))->with('id', $id);
    }

    public function update(Request $request, $id) {
      $category = Item::where('id', $id);
      $cekPrice = $category->first();
      $listCat = CategoryItem::where('id_item', $id)->value('id_category');
      if(($cekPrice->price_retail != $request->price_retail) || ($cekPrice->price_gov != $request->price_gov)){
        $priceHistory = ItemPriceHistory::create([
          "id_item" => $id,
          "price_retail" => str_replace(".","", $request->price_retail),
          "price_gov" => str_replace(".","", $request->price_gov),
          "price_date" => $request->price_date,
          "price_shipment" => str_replace(".","", $request->price_shipment),
        ]);
      }
      $update = $category->update([
             "code" => $request->code,
             "name" => $request->name,
             "merk" => $request->merk,
             "price_retail" => str_replace(".","", $request->price_retail),
             "price_shipment" => str_replace(".","", $request->price_shipment),
             "price_gov" => str_replace(".","", $request->price_gov),
             "price_date" => $request->price_date,
             "release_date" => $request->release_date,
             "expired_date" => $request->expired_date,
             "description" => $request->description,
             "production_origin" => $request->production_origin
        ]);

        return view('catalog.admin.item.list')->with(array('message'=> 'Item Updated successful', 'id'=> $listCat));
    }

    public function historyPrice(Request $request, $id) {
      $history = DB::table('item')
                  ->select('item.name', 'price_item.*', 'item.price_country')
                  ->join('price_item', 'item.id', '=','price_item.id_item')
                  ->where('id_item', $id)->get();
      return view('catalog.admin.item.price',compact('history'))->with('id', $id);
    }

    public function spec($id) {
      $id_category = DB::table('category_item')->where('id_item', $id)->value('id_category');
      $category_spec = DB::table('category_spec')->where('id_category', $id_category)->get();
      return view('catalog.admin.item.spec',compact('id_category','category_spec'))->with('id', $id);
    }

    public function SaveSpec(Request $request) {
      $insert = "";
      $update = "";
      foreach ($request->description as $key => $value) {
        $id_cat_spec[] = $key;
        $des[] = $value;
      }

      $existData = ItemSpec::whereIn('id_category_spec', $id_cat_spec)->where('id_item', $request->id_item)->pluck('id_category_spec')->toArray();//->whereIn('description',$des)->pluck('description','id_category_spec')->toArray();
      foreach ($request->description as $key => $value) {
        if((in_array($key, $existData))){
          if($update) $update .= ";";
          $update .= "UPDATE spec_item SET description= '".$value."' WHERE id_category_spec= '".$key."' AND id_item='".$request->id_item."'";
        }else{
          if($insert) $insert .= ",";
          $insert .= "('".$value."','".$key."','".$request->id_item."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
        }
      }

      $data = New ItemSpec;
      if($insert != ''){
        $save = $data->ItemSpec($insert);
      }
      if($update != ''){
        $update = $data->ItemSpecUpdate($update);
      }

      return redirect()->back()->with('message', 'Spesification Updated successful');
    }

    public function image($id) {
      return view('catalog.admin.item.image')->with('id', $id);
    }

    public function imageSave(Request $request, $id) {
      $file = $request->file('file');
      $destinationPath = 'assets/catalog/item';
      $idImg = ItemImage::where('id_item', $id)->orderBy('id', 'desc')->first();
      $Imgno = ($idImg == null) ? 1 : $idImg->id+1;
      if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
      foreach ($file as $key => $value) {
        $number = $Imgno+$key;
        $img = ItemImage::create([
          'file' => "item_".$id."_".$number.".".$value->getClientOriginalExtension(),
          'id_item' => $id
        ]);

        $move = $value->move($destinationPath, "item_".$id."_".$number.".".$value->getClientOriginalExtension());
      }
      if($move) {
        $message = 'Image Uploaded successful';
      }else{
        $message = 'Image Uploaded fail';
      }
      return redirect()->back()->with('message', $message);
    }

    public function DeleteImage($id) {
      $item = ItemImage::where('file', $id)->delete();
      $destinationPath = 'assets/catalog/item';
      $deleteFile = unlink(public_path($destinationPath ."/".$id));//File::delete($destinasi);
      if($deleteFile){
        $message = 'Image Deleted successful';
      }else{
        $message = 'Image Deleted fail';
      }
      return redirect()->back()->with('message', $message);
    }

    public function doc($id) {
      return view('catalog.admin.item.doc')->with('id', $id);
    }

    public function DocSave(Request $request, $id) {
      $file = $request->file('file');
      $destinationPath = 'assets/catalog/doc';
      if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
      $idDoc = ItemDoc::where('id_item', $id)->orderBy('id', 'desc')->first();
      $doCno = ($idDoc == null) ? 1 : $idDoc->id+1;
      $img = ItemDoc::create([
        'name' => $request->name,
        'description' => $request->description,
        'id_item' => $id,
        'file' => "d_".$id."_".$doCno.".".$file->getClientOriginalExtension()
      ]);
      $move = $file->move($destinationPath, "d_".$id."_".$doCno.".".$file->getClientOriginalExtension());
      if($move) {
        $message = 'Document Uploaded successful';
      }else{
        $message = 'Document Uploaded fail';
      }

      return redirect()->back()->with('message', $message);
    }

    public function DeleteDoc($id) {
      $item = ItemDoc::where('file', $id)->delete();
      $destinationPath = 'assets/catalog/doc';
      $deleteFile = unlink(public_path($destinationPath ."/".$id));//File::delete($destinasi);
      if($deleteFile){
        $message = 'Document Deleted successful';
      }else{
        $message = 'Document Deleted fail';
      }
      return redirect()->back()->with('message', $message);
    }

    public function status(Request $res) {
      // dd($res);
      $updateStatus = DB::table('doc_item')->where('id', $res->id)->update([
        'status' => $res->status,
      ]);

      if($updateStatus){
        $message = 'Document update successful';
      }else{
        $message = 'Document update fail';
      }

      return redirect()->back()->with('message', $message);
    }


    // public function UploadItemindex() {
    //   $contract = DB::table('vendor_detail')
    //              ->select('vendor_detail.id AS id_vendor', 'vendor_detail.vendor_name', DB::raw('COUNT(contract.id) AS jumlah_kontrak'))
    //              ->join('contract', 'vendor_detail.id', '=', 'contract.id_vendor_detail')
    //              ->groupBy('vendor_detail.id', 'vendor_detail.vendor_name')
    //              ->get();
    //   return view('catalog.admin.item.upload', compact('contract'));
    // }


    public function ContractVendor(Request $request) {
      $vendor  = $request->id_vendor;
      $contract = Contract::where('id_vendor_detail', $vendor)->get();
      return response()->json(array(
                               'contract' => $contract,
                               'status' => 1));
    }

    public function AdendumVendor(Request $request) {
      $id_contract  = $request->id_contract;
      $adendum = Adendum::where('id_contract', $id_contract)->get();
      return response()->json(array(
                               'adendum' => $adendum,
                               'status' => 1));
    }


    public function UploadItem(Request $request) {
      $request->validate([
                 'category' => 'required',
                 'import_file' => 'required'
             ]);
             $path = $request->file('import_file')->getRealPath();
             $data = Excel::load($path)->get();
             // dd($data);

             $response = Curl::to('http://ambilaja.jawarahost.com/itemApi.php')->get();
             ##api rumah sakit harapan kita
             // $response = Curl::to('http://10.10.10.8/apiwakatalog/m_barang.php')->get();
             if($response == TRUE){
               $datas = json_decode($response);
                 foreach($datas AS $k => $v){
                   $kode[] = $v->kode_barang;

                 }

                 foreach($data AS $k => $ve){
                   // foreach ($ve as $key => $value) {
                     $code[] = $ve->kode_item;
                     $nama[] = $ve->nama_item;
                     $harga[] = $ve->harga_beli_inv;
                     $spesifikasi[] = $ve->spesifikasi;
                     $satuan[] = $ve->satuan;
                     $kemas[] = strtoupper($ve->satuan);
                     $kemasin[] = $ve->satuan;
                   // }

                 }

                 $idUnitType = UnitType::whereIn('unit_name',array_filter($kemas))->orWhereIn('unit_name', array_filter($kemasin))
                 ->pluck('id', 'unit_name')->toArray();

                 $diff = array_diff($code, $kode);
                 $noInsert = array_diff($code, $diff); //ini yg diinsert
                 $satuanDB = UnitType::whereIn('unit_name', $kemas)->pluck('id', 'unit_name')->toArray();

                 // echo "<pre>".print_r($diff)."</pre>";
                 // echo "<pre>".print_r($noInsert)."</pre>";
                 // echo "<pre>".print_r($kode)."</pre>";
                 //
                 // foreach (array_filter($nama) as $k => $value) {
                 //   $x = strtoupper($satuan[$k]);
                 //   if (!empty($satuanDB[$x])) {
                 //     echo "barang : ".$value."<hr>";
                 //     echo "key : ".$k."<hr>";
                 //     echo "satuan : ".$x."<hr>";
                 //   }else{
                 //     echo "barang non satuan : ".$value."<hr>";
                 //     echo "key  non satuan : ".$k."<hr>";
                 //     echo "satuan non satuan : ".$x."<hr>";
                 //   }
                 //
                 //   // echo "<pre>  key : ".print_r($satuanDB[$x])."</pre>";
                 //   echo "<pre>  key : ".print_r($x)."</pre>";
                 // }
                 // echo "<pre>  key : ".print_r($satuanDB)."</pre>";
                 // dd(array_filter($nama));
               DB::beginTransaction();
               try {
                 if(count(array_filter($nama))){
                   foreach (array_filter($nama) as $key => $value) {
                     $x = strtoupper($satuan[$key]);
                     if ($code[$key] == '') {
                       // if($x != ''){
                         if (!empty($satuanDB[$x])) {
                           $kemasan = $satuanDB[$x];
                           $items = ItemUploadReject::create([
                             'code' => ' ',
                             'id_vendor_detail' => $request->id_vendor_detail,
                             'name' => $value,
                             'merk'  => 'merk tidak diketahui',
                             'item_from'  => '0',
                             'price_country'  => '0',
                             'price_retail'  => '0',
                             'price_gov'  => $harga[$key],
                             'price_shipment'  =>  '0',
                             'price_date'  =>date('Y-m-d H:i:s'),
                             'release_date'  => date('Y-m-d H:i:s'),
                             'expired_date'  => date('Y-m-d H:i:s'),
                             'stock'  => '100',
                             'stock_min'  =>  '100',
                             'production_origin'  => '0',
                             'description'  => $spesifikasi[$key],
                             'status' => 'Y',
                             'id_vendor_detail' => $request->id_vendor_detail,
                             'id_contract' => $request->id_contract,
                             'id_adendum' => $request->id_adendum,
                             'satuan' => $kemasan,
                           ]);
                         }else{
                           $items = ItemUploadReject::create([
                             'code' => ' ',
                             'id_vendor_detail' => $request->id_vendor_detail,
                             'name' => $value,
                             'merk'  => 'merk tidak diketahui',
                             'item_from'  => '0',
                             'price_country'  => '0',
                             'price_retail'  => '0',
                             'price_gov'  => $harga[$key],
                             'price_shipment'  =>  '0',
                             'price_date'  =>date('Y-m-d H:i:s'),
                             'release_date'  => date('Y-m-d H:i:s'),
                             'expired_date'  => date('Y-m-d H:i:s'),
                             'stock'  => '100',
                             'stock_min'  =>  '100',
                             'production_origin'  => '0',
                             'description'  => $spesifikasi[$key],
                             'status' => 'Y',
                             'id_vendor_detail' => $request->id_vendor_detail,
                             'id_contract' => $request->id_contract,
                             'id_adendum' => $request->id_adendum,
                             'satuan_name' => $x,
                           ]);
                         }#endif satuan empty
                       // }
                     }
                   }

                 }


                 if(count($noInsert) > 0){
                   foreach (array_filter($noInsert) as $k => $v) {
                     // if(array_key_exists($satuan[$k], $satuanDB) ){
                       $x = strtoupper($satuan[$k]);
                     // }
                     if (!empty($satuanDB[$x])) {
                       $kemasanTolak = $satuanDB[$x];
                       $item = Item::updateOrCreate([
                         'code' => $v,
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'id_contract' => $request->id_contract,
                         'id_adendum' => $request->id_adendum
                       ],
                       [
                         'name' => $nama[$k],
                         'merk'  => 'merk tidak diketahui',
                         'item_from'  => '0',
                         'price_country'  => '0',
                         'price_retail'  => '0',
                         'price_gov'  => $harga[$k],
                         'price_shipment'  =>  '0',
                         'price_date'  => date('Y-m-d H:i:s'),
                         'release_date'  => date('Y-m-d H:i:s'),
                         'expired_date'  => date('Y-m-d H:i:s'),
                         'stock'  => '100',
                         'stock_min'  =>  '100',
                         'production_origin'  => '0',
                         'description'  => $spesifikasi[$k],
                         'status' => 'Y',
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'id_contract' => $request->id_contract,
                         'id_adendum' => $request->id_adendum,
                         'satuan' => $kemasanTolak,
                       ]);
                       $dataSnya = md5($item->updated_at);
                       $general_spec = GeneralSpec::updateOrCreate([
                         'id_item' => $item->id
                        ],
                        [
                        'id_item' => $item->id,
                         'weight' => '0',
                         'height' => '0',
                         'volume' => '0',
                         'minimal_buy' =>  '0'
                       ]);

                       $Categoryitem = CategoryItem::updateOrCreate([
                         'id_item' => $item->id
                          ],
                          [
                         'id_category' => $request->category,
                         'id_item' => $item->id
                       ]);

                       $suplier = Supplier::updateOrCreate([
                         'id_item' => $item->id
                          ],
                          [
                         'id_supplier' => $request->id_vendor_detail,
                         'id_item' => $item->id
                       ]);
                     }else{
                       $items = ItemUploadReject::create([
                         'code' => $v,
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'name' => $nama[$k],
                         'merk'  => 'merk tidak diketahui',
                         'item_from'  => '0',
                         'price_country'  => '0',
                         'price_retail'  => '0',
                         'price_gov'  => $harga[$k],
                         'price_shipment'  =>  '0',
                         'price_date'  =>date('Y-m-d H:i:s'),
                         'release_date'  => date('Y-m-d H:i:s'),
                         'expired_date'  => date('Y-m-d H:i:s'),
                         'stock'  => '100',
                         'stock_min'  =>  '100',
                         'production_origin'  => '0',
                         'description'  => $spesifikasi[$k],
                         'status' => 'Y',
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'id_contract' => $request->id_contract,
                         'id_adendum' => $request->id_adendum,
                         'satuan_name' => $x,
                       ]);
                     }
                   }
                 }

                 //ini yg item reject yg kodenya ga ada
                 // if($data->count() > 0){

                   foreach (array_filter($diff) as $key => $value) {
                     if(array_key_exists($satuan[$key], $satuanDB) ){
                       $x = strtoupper($satuan[$key]);

                       $kemasan = (!empty($satuanDB[$x]) ? $satuanDB[$x] : '');
                     }
                     // $satuan = UnitType::where('unit_name', $value->satuan)->value('id');
                     if(!empty($satuanDB[$x])){
                       $items = ItemUploadReject::create([
                         'code' => $value,
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'name' => $nama[$key],
                         'merk'  => 'merk tidak diketahui',
                         'item_from'  => '0',
                         'price_country'  => '0',
                         'price_retail'  => '0',
                         'price_gov'  => $harga[$key],
                         'price_shipment'  =>  '0',
                         'price_date'  =>date('Y-m-d H:i:s'),
                         'release_date'  => date('Y-m-d H:i:s'),
                         'expired_date'  => date('Y-m-d H:i:s'),
                         'stock'  => '100',
                         'stock_min'  =>  '100',
                         'production_origin'  => '0',
                         'description'  => $spesifikasi[$key],
                         'status' => 'Y',
                         'id_vendor_detail' => $request->id_vendor_detail,
                         'id_contract' => $request->id_contract,
                         'id_adendum' => $request->id_adendum,
                         'satuan' => $kemasan,
                       ]);
                     }
                   // }
                   $dataSnya = md5($item->updated_at);
                }


               } catch (\Illuminate\QueryException $e) {
                 DB::rollback();
                 dd($e->getMessage());
                 return back()->with('message', 'Gagal Import Item');

               }
               DB::commit();
               // die();
               return redirect('catalog/admin/item/upload/detail/'.$dataSnya)->with('message', 'Sukses Import Item');
               // return back()->with('message', 'Sukses Import Item');
            }else{
              return back()->with('message', 'Jaringan bermasalah');
            }

    }

      public function detailUpload(Request $request, $id) {
        $item = DB::table('item')
                ->select('item.name', 'item.code', 'unit_type.unit_name', 'item.created_at', 'item.satuan')
                ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
                ->where(DB::raw('md5(cast(item.updated_at as varchar(50)))'), $id)
                ->get();

        $item_reject = DB::table('item_reject')
                ->select('item_reject.name', 'item_reject.code', 'item_reject.created_at', 'item_reject.satuan', 'item_reject.satuan_name')
                // ->join('unit_type', 'item_reject.satuan', '=', 'unit_type.id')
                ->where(DB::raw('md5(cast(item_reject.updated_at as varchar(50)))'), $id)
                ->get();
        $satuan = UnitType::where('status', 1)->pluck('unit_name', 'id')->toArray();

        return view('catalog.admin.item.detail', compact('item', 'item_reject', 'satuan'));
      }

}
