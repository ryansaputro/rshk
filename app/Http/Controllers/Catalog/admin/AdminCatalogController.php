<?php
namespace App\Http\Controllers\Catalog\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Category;
use App\Model\CategorySpec;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;

class AdminCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('adminauth');
    }

    public function index(Request $requests){

        if (Auth::check()) {
            // return view('catalog.users.directur.index', compact('data_company','company'))->with('id', $id);
            return view('catalog.admin.dashboard.index');
        }else{
            return redirect('auth');
        }
    }

    public function category(Request $request) {
      return view('catalog.admin.category.index');
    }

    public function AddCategory(Request $request) {
      $category = Category::create([
        'name' => $request->name,
        'id_parent' => $request->id_parent,
        'status' => $request->status
        ]);
        return redirect()->back()->with('message', 'Category Created successful');;
    }

    public function UpdateCategory(Request $request) {
      $category = Category::where('id_parent', $request->id_parent)->where('id', $request->id)->update([
        'name' => $request->name,
        'status' => $request->status
        ]);
        return redirect()->back()->with('message', 'Category Updated successful');;
    }

    public function DeleteCategory(Request $request) {
      $category = Category::where('id_parent', $request->parent)->where('id', $request->id)->delete();
      return response()->json(array(
                               'status' => 1));
    }


    public function SpecCategory(Request $request, $id) {
      $data = DB::table('category_spec')->where('id_category', $id);
      $name = Category::where('id', $id)->value('name');
      $category_spec =  $data->get();
      return view('catalog.admin.category.spec',compact('category_spec' ,'name'))->with('id', $id);
    }

    public function AddSpecCategory(Request $request) {
      $spek = CategorySpec::create([
          'name' => $request->name,
          'id_category' => $request->id_category_spec,
          'status' => $request->status
      ]);
      return redirect()->back()->with('message', 'Spesification Created successful');
    }

    public function UpdateSpecCategory(Request $request) {
      $spek = CategorySpec::where('id', $request->id_category_spec)->update([
          'name' => $request->name,
          'status' => $request->status
      ]);
      return redirect()->back()->with('message', 'Spesification Updated successful');
    }

    public function DeleteSpecCategory(Request $request) {
      $category = CategorySpec::where('id', $request->id)->delete();
      return response()->json(array(
        'status' => 1));
    }

    function reset(){
        return view('catalog.admin.layouts.v_reset');
    }

    function action_ganti_password(Request $request){



       if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
           // The passwords matches
           return redirect()->back()->with("error","
           Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
       }

       if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
           //Current password and new password are same
           return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
       }

       $validatedData = $request->validate([
           'current_password' => 'required',
           'new_password' => 'required|string|min:6|confirmed',
       ]);

       //Change Password
       $user = Auth::user();
       $user->password = bcrypt($request->get('new_password'));
       $user->save();

       return redirect()->back()->with("success","Password changed successfully !");

       die;




        echo $lama1 = $request->lama1;
        echo "<br>";

        if (Hash::needsRehash($lama1)) {
           $lama1 = Hash::make('123456');
           echo "sama";
       }else{
           echo "beda";
       }
      die;

        // echo $lama = bcrypt($request->lama);
         echo $password = Hash::make($request->lama);

        $baru = $request->baru;

      die;
       if($lama==$baru){
           DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
           return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
       }else{
           return redirect()->route('ganti_password')->with('message','Password Anda Salah');
       }

     }

     function lengkapi(){


          return view('catalog.admin.master_vendor.vendor_lengkapi');
     }





}
