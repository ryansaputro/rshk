<?php
namespace App\Http\Controllers\Catalog\depo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\CategorySpec;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use DB;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\DeliveryOrder;
use App\Model\ReceiveItem;
use App\Model\ReceiveItemDetail;
use App\Model\ItemReject;
use App\Model\CategoryWarehouseType;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\VendorDetail;
use App\Model\Item;
use App\Model\GeneralSpec;
use App\Model\Supplier;
use App\Model\ItemImage;
use App\Model\UserRequest;
use App\Model\MasterDataWarehouse;
use App\Model\VendorAdministrators;
use App\Model\Contract;
use App\Model\Supervisi;
use App\Model\UnitType;
use App\Model\MasterDataGudangStockOut;
use App\Model\MasterDataGudangStockOutDetail;
use App\Model\UserRequestDetail;
use App\Model\MasterDataDepo;
use App\User;
use Excel;
use Fungsi;
use URL;
use Mail;


class DepoController extends Controller
{

  public function __construct()
  {
      $this->middleware('depoauth');
  }

  public function index(Request $request){
      $id_depo = DB::table('user_depo')->select('id_depo')->where('id_users', Auth::user()->id)->value('id_depo');
      $data = DB::table('master_data_depo')
              ->select('unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'master_data_depo.last_date_out', 'master_data_depo.last_date_in', 'master_data_depo.qty', 'master_depo.nama_depo')
              ->join('item', 'master_data_depo.id_item', '=', 'item.id')
              ->join('master_depo', 'master_data_depo.id_depo', '=', 'master_depo.id')
              ->join('unit_type', 'master_data_depo.satuan', '=', 'unit_type.id')
              ->where('master_data_depo.id_depo', $id_depo)
              ->get();


      $category = Category::where('status', 'Y')->get();

      return view('catalog.depo.stock.index',compact('data', 'categoryWarehouse', 'category'));
  }

  public function InventoryAdd(Request $request) {
    $request->validate([
               'import_file' => 'required',
               'category' => 'required',
           ]);
           $id_depo = DB::table('user_depo')->select('id_depo')->where('id_users', Auth::user()->id)->value('id_depo');
           $path = $request->file('import_file')->getRealPath();
           $data = Excel::load($path)->get();

           DB::beginTransaction();
           try {

             if($data->count() > 0){
                 foreach ($data as $key => $value) {

                   $id_satuan = UnitType::where('unit_name',  'like', '%' . $value->satuan . '%')
                                ->value('id');

                   $vendorDetail = VendorDetail::where('vendor_name', $value->vendor_name)
                                  ->orWhere('code', $value->code_vendor)
                                  ->updateOrCreate(
                                    ['code' => $value->code_vendor, 'vendor_name' => $value->vendor_name],
                                    ['code' => $value->code_vendor,'vendor_name' => $value->vendor_name]);

                  $ItemCheck = Item::where('code', $value->code)
                                 ->updateOrCreate(
                                   ['code' => $value->code],
                                   [
                                    'name' => ($value->name != null) ? $value->name : 'tidak diketahui',
                                    'code' => ($value->code != null) ? $value->code : '0000000',
                                    'merk'  => ($value->merk != null) ? $value->merk : 'merk tidak diketahui',
                                    'item_from'  => '0',
                                    'price_country'  => '0',
                                    'price_retail'  => ($value->price_retail != null) ? $value->price_retail : '0',
                                    'price_gov'  => ($value->price_gov != null) ? $value->price_gov : '0',
                                    'price_shipment'  => ($value->price_shipment != null) ? $value->price_shipment : '0',
                                    'price_date'  =>($value->price_date != null) ?  $value->price_date : date('Y-m-d H:i:s'),
                                    'release_date'  => ($value->release_date != null) ? $value->release_date : date('Y-m-d H:i:s'),
                                    'expired_date'  => ($value->expired_date != null) ? $value->expired_date : date('Y-m-d H:i:s'),
                                    'stock'  => ($value->stock != null) ? $value->stock : '0',
                                    'stock_min'  => ($value->stock_min != null) ? $value->stock_min : '0',
                                    'production_origin'  => '0',
                                    'description'  => ($value->description != null) ? $value->description : 'tidak ada deskripsi',
                                    'status' => 'Y',
                                    'satuan' => $id_satuan
                                   ]);


                                   $general_spec = GeneralSpec::updateOrCreate(
                                     ['id_item' => $ItemCheck->id],
                                     [
                                          'id_item' => $ItemCheck->id,
                                          'weight' => ($value->weight != null) ?  $value->weight : '0',
                                          'height' => ($value->height != null) ?  $value->height : '0',
                                          'volume' => ($value->volume != null) ?  $value->volume : '0',
                                          'minimal_buy' => ($value->minimal_buy != null) ?  $value->minimal_buy : '0'
                                          ]);
                                   $Categoryitem = CategoryItem::updateOrCreate(
                                     ['id_item' => $ItemCheck->id],
                                     [
                                          'id_category' => $request->category,
                                          'id_item' => $ItemCheck->id
                                          ]);
                                   $suplier = Supplier::updateOrCreate(
                                     ['id_item' => $ItemCheck->id],
                                     [
                                          'id_supplier' => $vendorDetail->id,
                                          'id_item' => $ItemCheck->id
                                          ]);

                                     $img = ItemImage::updateOrCreate(
                                       ['id_item' => $ItemCheck->id],
                                       [
                                       'file' => ($value->image != null) ? $value->image : '',
                                       'id_item' => $ItemCheck->id
                                     ]);

                                     $gudang = MasterDataDepo::updateOrCreate(
                                       ['id_item' => $ItemCheck->id, 'id_depo' => $id_depo],
                                       [
                                       'qty' => ($value->stock != null) ? $value->stock : '0',
                                       'satuan' => $id_satuan,
                                       'last_date_in' => date('Y-m-d H:i:s'),
                                       'minim_stock' => ($value->stock_min != null) ? $value->stock_min : '0',
                                       'id_user' => Auth::user()->id
                                     ]);

                 }
             }

           } catch (\Illuminate\Database\QueryException $e) {
             DB::rollback();
             return back()->with('message', 'Gagal');
             // dd($e->getMessage());

           }
           DB::commit();


           return back()->with('message', 'Sukses Import Item');
      }


  public function stockIn(Request $request){
    $id_gudang = User::where('id', Auth::user()->id)->value('id_gudang');

    $dataQuery = DB::table('receive_item')
            ->select('users.username','master_gudang.nama_instalasi', 'receive_item.datetime', 'unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'receive_item_detail.*')
            ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
            ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
            ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
            ->join('master_gudang', 'receive_item.id_gudang', '=', 'master_gudang.id')
            ->join('users', function($join){
              $join->on('receive_item.id_user', '=', 'users.id');
            })
            ->where('receive_item.id_gudang', $id_gudang)
            ->orderBy('receive_item.datetime', 'DESC');
      $data = $dataQuery->get();
      // $data = $dataQuery->paginate(10);

      return view('catalog.warehouse.stockin.index',compact('data'));
  }

  public function stockOut(Request $request){
    $id_gudang = User::where('id', Auth::user()->id)->value('id_gudang');

    $dataQuery =  DB::table('master_data_gudang_stock_out')
      ->select('master_data_gudang_stock_out.*','users.username', 'master_gudang.nama_instalasi', 'master_gudang.kode_instalasi', 'master_data_gudang_stock_out.id_user_requestor')
      ->join('users', 'master_data_gudang_stock_out.id_user_warehouse', '=', 'users.id')
      ->join('master_gudang', 'master_data_gudang_stock_out.id_gudang', '=', 'master_gudang.id')
      ->where('master_data_gudang_stock_out.id_gudang', $id_gudang)
      ->orderBy('master_data_gudang_stock_out.datetime', 'DESC');
    $data = $dataQuery->get();

    // $user = DB::table('user_catalog')->select('user_catalog.id_user', 'user_catalog.username_catalog')->pluck('username_catalog', 'id_user')->toArray();

    // $data = $dataQuery->paginate(10);

    return view('catalog.warehouse.stockout.index',compact('data'));
  }

  public function stockOutCreate(Request $request){
      $id_gudang = User::where('id', Auth::user()->id)->value('id_gudang');
      $dataQuery = DB::table('user_request')->select('user_request.*', 'user_catalog.kode_instalasi')
          ->join('user_catalog', 'user_request.id_user', '=', 'user_catalog.id_user')
          ->where('user_request.status', 1)
          ->where('user_request.id_gudang', Auth::user()->id_gudang)->get();

      if($request->id_order != null){
          $id_order = $request->id_order;
          $data = Order::where('id', $id_order)->first();
          $userRequests = UserRequest::where('id_order', $data->id)->first();

          $detail = DB::table('user_request')
          ->select('unit_type.id AS id_satuan', 'unit_type.unit_name', 'user_request.*', 'user_request_detail.id_item', 'user_request_detail.qty', 'user_request_detail.satuan', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
          ->join('user_request_detail', 'user_request.id', '=', 'user_request_detail.id_user_request')
          ->join('unit_type', 'user_request_detail.satuan', '=', 'unit_type.id')
          ->join('item', 'user_request_detail.id_item', '=', 'item.id')
          ->where('user_request.id_order', $data->id)
          ->where('user_request.status', '1');

          $detailUserRequest = $detail->pluck('qty', 'id_item')->toArray();
          $detailUserSatuan = $detail->pluck('unit_name', 'id_item')->toArray();
          $detailUserJenis = $detail->pluck('id_satuan')->toArray();

          $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
          $vendorDirectur = VendorAdministrators::where('id_vendor_detail', $data->id_vendor_detail)->first();

          $directur = DB::table('user_catalog_directur')
          ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
          ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
          ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
          ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
          ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
          ->first();

          $dataOrder = DB::table('order_catalog_detail')
          ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
          ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
          ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
          ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
          ->where('order_catalog.id', $data->id)
          ->get();

          ############cara dapetin nomor sesui dengan kategori utama
          $menentukanKate = OrderDetail::where('id_order', $data->id)->pluck('id_item')->toArray();
          $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
          $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
          $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
          $mainCat = Category::whereIn('id', $subCategory)->value('name');

          $list_propinsi = Fungsi::propinsi();
          $list_kota = Fungsi::IndonesiaProvince();

          $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
          $supervisi = Supervisi::where('id', $data->id_user_supervisi)->first();
          $satuan =  UnitType::where('status', 1)->get();


          $categoryWarehouse = DB::table('category_warehouse_type')->select('master_gudang.nama_instalasi','master_gudang.id')
          ->join('master_gudang', 'category_warehouse_type.id_gudang', '=', 'master_gudang.id')
          ->where('category_warehouse_type.id_category', $subCategory)
          ->first();

          $peminta = DB::table('user_catalog')->select('user_catalog.id_user', 'user_catalog.username_catalog', 'user_catalog.nip', 'user_catalog.nama_instalasi')->where('id_user', $data->id_user_buyer)->first();
      }

      return view('catalog.warehouse.stockout.create',compact('id_order','dataQuery','categoryWarehouse', 'mainCat', 'detailUserJenis', 'detailUserSatuan', 'userRequests', 'detailUserRequest', 'satuan', 'peminta', 'contract', 'supervisi', 'data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
  }

  public function stockOutStore(Request $request) {
    $update = "";

    DB::beginTransaction();
    try {
      $stockOut = MasterDataGudangStockOut::create(
        [
          'id_gudang' => $request->id_gudang,
          'id_user_requestor' => $request->id_user,
          'datetime' => date('Y-m-d H:i:s'),
          'id_user_warehouse' => Auth::user()->id
        ]
      );

      foreach ($request->id_item as $key => $value) {
        $stockOutDetail = MasterDataGudangStockOutDetail::create(
          [
            'id_master_data_gudang_stock_out' => $stockOut->id,
            'id_item' => $value,
            'qty' => $request->qty[$key],
            'satuan' => $request->satuan[$key],
            'price' => $request->price[$key],
            'status' => '1'
          ]
        );

          #UPDATE KE MASTER DATA GUDANG
          if($update) $update .= ";";
          $update .= "UPDATE master_data_gudang SET qty =qty-".$request->qty[$key].",last_date_out='".date('Y-m-d H:i:s')."', id_user='".Auth::user()->id."' WHERE id_gudang=".$request->id_gudang." and id_item=".$value;
      }


        $msterDataWarehouse = NEW MasterDataWarehouse;
        $dataWarehouse = $msterDataWarehouse->updateData($update);

        $userRequest = UserRequest::where('id', $request->id_user_request)->update(['status' => 2]);

    } catch (\Exception $e) {
      DB::rollback();
      return redirect('/catalog/warehouse/stockout/index')->with('message', 'Gagal');

    }
    DB::commit();


    return redirect('/catalog/warehouse/stockout/preview/'.md5($stockOut->id))->with('message', 'Sukses');
  }

  public function stockOutPreview(Request $request, $id) {
    $stockOut = DB::table('master_data_gudang_stock_out')
      ->select('master_data_gudang_stock_out.*','users.username')
      ->join('users', 'master_data_gudang_stock_out.id_user_warehouse', '=', 'users.id')
      ->where(DB::raw('md5(master_data_gudang_stock_out.id)'), $id)
      ->first();

    $userRequestUser = DB::table('user_request')
      ->select('master_data_gudang_stock_out.datetime', 'master_data_gudang_stock_out.id AS id_master_stock','master_data_gudang_stock_out.no_stock_out', 'user_request.*', 'master_gudang.kode_instalasi', 'master_gudang.nama_instalasi', 'user_catalog.nip AS nip_peminta', 'user_catalog.username_catalog AS user_peminta', 'user_catalog.kode_instalasi AS kode_user', 'user_catalog.nama_instalasi AS instalasi_user')
      ->join('master_gudang', 'user_request.id_gudang', '=', 'master_gudang.id')
      ->join('user_catalog', 'user_request.id_user', '=', 'user_catalog.id_user')
      ->join('master_data_gudang_stock_out', 'master_data_gudang_stock_out.id_user_requestor', '=', 'user_catalog.id_user')
      ->where(DB::raw('md5(master_data_gudang_stock_out.id)'), $id)
      ->first();

    $userRequestDepo = DB::table('master_data_gudang_stock_out')
      ->select('master_data_gudang_stock_out.datetime', 'master_data_gudang_stock_out.id AS id_master_stock','master_data_gudang_stock_out.no_stock_out',  'master_gudang.kode_instalasi', 'master_gudang.nama_instalasi', 'user_depo.nik AS nip_peminta', 'user_depo.nama as user_peminta', 'user_depo.nama AS instalasi_user')
      ->join('master_gudang', 'master_data_gudang_stock_out.id_gudang', '=', 'master_gudang.id')
      ->join('user_depo', 'master_data_gudang_stock_out.id_user_requestor', '=', 'user_depo.id_depo')
      ->where(DB::raw('md5(master_data_gudang_stock_out.id)'), $id)
      ->first();

      $userRequest = ($userRequestUser == null) ? $userRequestDepo : $userRequestUser;


    $stockOutDetail = DB::table('master_data_gudang_stock_out_detail')
      ->select('master_data_gudang_stock_out_detail.id_item', 'master_data_gudang_stock_out_detail.qty', 'item.description', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
      ->join('item', 'master_data_gudang_stock_out_detail.id_item', '=', 'item.id')
      ->where(DB::raw('md5(master_data_gudang_stock_out_detail.id_master_data_gudang_stock_out)'), $id)
      ->get();

    if ($userRequestUser != null) {
      $dataRequestDetail = DB::table('user_request_detail')
      ->select('user_request_detail.*', 'unit_type.unit_name')
      ->join('user_request', 'user_request_detail.id_user_request', '=', 'user_request.id')
      ->join('unit_type', 'user_request_detail.satuan', '=', 'unit_type.id')
      ->where('user_request.id', $userRequest->id);

      $userRequestDetail = $dataRequestDetail->pluck('qty', 'id_item')->toArray();
      $userRequestDetailSatuan = $dataRequestDetail->pluck('unit_name', 'id_item')->toArray();
    }else{
      $dataRequestDetail = DB::table('master_data_gudang_stock_out_detail')
      ->select('master_data_gudang_stock_out_detail.*', 'unit_type.unit_name')
      ->join('master_data_gudang_stock_out', 'master_data_gudang_stock_out_detail.id_master_data_gudang_stock_out', '=', 'master_data_gudang_stock_out.id')
      ->join('unit_type', 'master_data_gudang_stock_out_detail.satuan', '=', 'unit_type.id')
      ->where('master_data_gudang_stock_out.id', $userRequest->id_master_stock);

      $userRequestDetail = $dataRequestDetail->pluck('qty', 'id_item')->toArray();
      $userRequestDetailSatuan = $dataRequestDetail->pluck('unit_name', 'id_item')->toArray();
    }

    // dd($dataRequestDetail);
    return view('catalog.warehouse.stockout.preview',compact('userRequestDetailSatuan', 'userRequestDetail', 'userRequest', 'stockOut','stockOutDetail'));
  }

  public function reportStock(Request $request) {
    $id_gudang = Auth::user()->id_gudang;
    $item = Item::where('status', 'Y')->get();
    $satuan = UnitType::where('status','1')->get();

    $param_last_in = !isset($request->last_date_in) ? 'xxx' : $request->last_date_in;
    $param_last_out = !isset($request->last_date_out) ? 'xxx' : $request->last_date_out;
    $last_date_in = !isset($request->last_date_in) ? 'xxx' : explode('-',$request->last_date_in);
    $last_date_out = !isset($request->last_date_out) ? 'xxx' : explode('-',$request->last_date_out);
    $id_item = ($request->id_item == 'xxx') ? 'xxx' : $request->id_item;
    $id_satuan = ($request->id_satuan == 'xxx') ? 'xxx' : $request->id_satuan;
    $stock = !isset($request->stock) ? 'xxx' : $request->stock;

    $data = DB::table('master_data_gudang')
            ->select('unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'master_data_gudang.last_date_out', 'master_data_gudang.last_date_in', 'master_data_gudang.qty', 'master_gudang.nama_instalasi')
            ->join('item', 'master_data_gudang.id_item', '=', 'item.id')
            ->join('master_gudang', 'master_data_gudang.id_gudang', '=', 'master_gudang.id')
            ->join('unit_type', 'master_data_gudang.satuan', '=', 'unit_type.id')
            ->where('master_data_gudang.id_gudang', $id_gudang);

    if($last_date_in !== 'xxx'){
      $data->whereBetween('last_date_in', [$last_date_in[0], $last_date_in[1]]);
    }
    if($last_date_out !== 'xxx'){
      $data->whereBetween('last_date_out', [$last_date_out[0], $last_date_out[1]]);
    }
    if($id_item !== 'xxx'){
      $data->where('id_item', $id_item);
    }
    if($id_satuan !== 'xxx'){
      $data->where('satuan', $id_satuan);
    }
    if($stock !== 'xxx'){
      $data->where('qty', $stock);
    }
    $dataMasterGudang = $data->get();

    return view('catalog.warehouse.report.stock', compact('param_last_out', 'param_last_in', 'item', 'satuan', 'last_date_in', 'last_date_out', 'id_item', 'id_satuan', 'stock', 'dataMasterGudang'));
  }

  public function reportStockIn(Request $request) {
    $id_gudang = Auth::user()->id_gudang;
    $item = Item::where('status', 'Y')->get();

    $satuan = UnitType::where('status','1')->get();

    $param_last_in = !isset($request->last_date_in) ? 'xxx' : $request->last_date_in;
    $param_last_out = !isset($request->last_date_out) ? 'xxx' : $request->last_date_out;
    $last_date_in = !isset($request->last_date_in) ? 'xxx' : explode('-',$request->last_date_in);
    $last_date_out = !isset($request->last_date_out) ? 'xxx' : explode('-',$request->last_date_out);
    $id_item = ($request->id_item == 'xxx') ? 'xxx' : $request->id_item;
    $id_satuan = ($request->id_satuan == 'xxx') ? 'xxx' : $request->id_satuan;
    $stock = !isset($request->stock) ? 'xxx' : $request->stock;

    $data = DB::table('receive_item')
            ->select('users.username','master_gudang.nama_instalasi', 'receive_item.datetime', 'unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'receive_item_detail.*')
            ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
            ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
            ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
            ->join('master_gudang', 'receive_item.id_gudang', '=', 'master_gudang.id')
            ->join('users', function($join){
              $join->on('receive_item.id_user', '=', 'users.id');
            })
            ->where('receive_item.id_gudang', $id_gudang)
            ->orderBy('receive_item.datetime', 'DESC');
    if($last_date_in !== 'xxx'){
      $data->whereBetween('datetime', [$last_date_in[0], $last_date_in[1]]);
    }
    if($last_date_out !== 'xxx'){
      $data->whereBetween('last_date_out', [$last_date_out[0], $last_date_out[1]]);
    }
    if($id_item !== 'xxx'){
      $data->where('id_item', $id_item);
    }
    if($id_satuan !== 'xxx'){
      $data->where('satuan', $id_satuan);
    }
    if($stock !== 'xxx'){
      $data->where('qty', $stock);
    }
    $dataMasterGudang = $data->distinct()->get();

    return view('catalog.warehouse.report.stockin', compact('param_last_out', 'param_last_in', 'item', 'satuan', 'last_date_in', 'last_date_out', 'id_item', 'id_satuan', 'stock', 'dataMasterGudang'));
  }

  public function reportStockOut(Request $request) {
    $id_gudang = Auth::user()->id_gudang;
    $item = Item::where('status', 'Y')->get();

    $satuan = UnitType::where('status','1')->get();

    $param_last_in = !isset($request->last_date_in) ? 'xxx' : $request->last_date_in;
    $param_last_out = !isset($request->last_date_out) ? 'xxx' : $request->last_date_out;
    $last_date_out = !isset($request->last_date_out) ? 'xxx' : explode('-',$request->last_date_out);
    $id_item = ($request->id_item == 'xxx') ? 'xxx' : $request->id_item;
    $id_satuan = ($request->id_satuan == 'xxx') ? 'xxx' : $request->id_satuan;
    $stock = !isset($request->stock) ? 'xxx' : $request->stock;

    $data = DB::table('master_data_gudang_stock_out_detail')
            ->select('master_data_gudang_stock_out.id_user_requestor', 'master_depo.nama_depo', 'users.username','master_data_gudang_stock_out.datetime', 'unit_type.unit_name', 'master_gudang.nama_instalasi', 'master_data_gudang_stock_out_detail.id_item', 'master_data_gudang_stock_out_detail.qty', 'item.description', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'master_data_gudang_stock_out_detail.id_item', '=', 'item.id')
            ->join('master_data_gudang_stock_out', 'master_data_gudang_stock_out_detail.id_master_data_gudang_stock_out', '=', 'master_data_gudang_stock_out.id')
            ->join('master_gudang', 'master_data_gudang_stock_out.id_gudang', '=', 'master_gudang.id')
            ->join('unit_type', 'master_data_gudang_stock_out_detail.satuan', '=', 'unit_type.id')
            ->join('users', function($join){
              $join->on('master_data_gudang_stock_out.id_user_warehouse', '=', 'users.id');
            })
            ->leftJoin('master_depo', function($join){
              $join->on('master_gudang.id', '=', 'master_depo.id_gudang');
              $join->on('master_data_gudang_stock_out.id_user_requestor', '=', 'master_depo.id');
            })
            ->where('master_gudang.id', $id_gudang)
            ->orderBy('master_data_gudang_stock_out.datetime', 'DESC');

      $user = DB::table('user_catalog')->select('user_catalog.id_user', 'user_catalog.username_catalog')->pluck('username_catalog', 'id_user')->toArray();

    if($last_date_out !== 'xxx'){
      $data->whereBetween('datetime', [$last_date_out[0]." 00:00:00", $last_date_out[1]." 23:59:59"]);
    }
    if($id_item !== 'xxx'){
      $data->where('id_item', $id_item);
    }
    if($id_satuan !== 'xxx'){
      $data->where('satuan', $id_satuan);
    }
    if($stock !== 'xxx'){
      $data->where('qty', $stock);
    }
    $dataMasterGudang = $data->distinct()->get();

    return view('catalog.warehouse.report.stockout', compact('user', 'param_last_out', 'param_last_in', 'item', 'satuan', 'last_date_in', 'last_date_out', 'id_item', 'id_satuan', 'stock', 'dataMasterGudang'));
  }


  public function depoMonitoring(Request $request) {

    $depoUnderStock = DB::table('master_data_depo')
        ->select(array('master_data_depo.*','master_depo.nama_depo', DB::raw('COUNT(master_data_depo.qty) as stock')))
        ->join('master_depo', 'master_data_depo.id_depo', '=', 'master_depo.id')
        ->whereRaw('qty <= minim_stock')
        ->groupBy('master_depo.id')
        ->orderBy('master_data_depo.last_date_out', 'DESC')
        ->get();

    $item = Item::where('status', 'Y')->get();
    $satuan = UnitType::where('status','1')->get();

    return view('catalog.warehouse.depo_monitoring.index', compact('depoUnderStock', 'item', 'satuan'));
  }

  public function depoCreate(Request $request, $id) {

    $data = DB::table('master_data_depo')
        ->select('unit_type.id AS id_satuan', 'master_data_depo.id_depo', 'master_data_depo.id_item', 'unit_type.unit_name', 'item.code', 'item.name', 'item.price_gov', 'item.merk', 'master_depo.nama_depo', 'master_data_depo.last_date_out', 'master_data_depo.last_date_in', 'master_data_depo.qty', 'master_gudang.nama_instalasi')
        ->join('item', 'master_data_depo.id_item', '=', 'item.id')
        ->join('master_depo', 'master_data_depo.id_depo', '=', 'master_depo.id')
        ->join('master_gudang', 'master_depo.id_gudang', '=', 'master_gudang.id')
        ->join('unit_type', 'master_data_depo.satuan', '=', 'unit_type.id')
        ->whereRaw('qty <= minim_stock')
        ->where(DB::raw('md5(id_depo)'), $id)
        ->orderBy('master_data_depo.last_date_out', 'DESC');

    $masterGudang = MasterDataWarehouse::pluck('qty', 'id_item')->toArray();
    $listData = $data->get();
    $headData =$data->first();

    $item = Item::where('status', 'Y')->get();
    $satuan = UnitType::where('status','1')->get();

    return view('catalog.warehouse.depo_monitoring.create', compact('masterGudang', 'depoUnderStock', 'item', 'satuan', 'listData', 'headData', 'id'));
  }

  public function depoStore(Request $request, $id) {

    $id_depo = DB::table('master_depo')->select('master_depo.*')->where(DB::raw('md5(id)'), $id)->value('id');
    $id_gudang = Auth::user()->id_gudang;
    $update = "";
    $update1 = "";
    $insert1 = "";

    DB::beginTransaction();
    try {
      $stockOut = MasterDataGudangStockOut::create(
        [
          'id_gudang' => $id_gudang,
          'id_user_requestor' => $id_depo,
          'datetime' => date('Y-m-d H:i:s'),
          'id_user_warehouse' => Auth::user()->id
        ]
      );

      foreach ($request->id_item as $key => $value) {
        $stockOutDetail = MasterDataGudangStockOutDetail::create(
          [
            'id_master_data_gudang_stock_out' => $stockOut->id,
            'id_item' => $value,
            'qty' => $request->qty[$key],
            'satuan' => $request->satuan[$key],
            'price' => $request->price[$key],
            'status' => '1'
          ]
        );

          #
          $masterData = MasterDataDepo::where('id_depo', $id_depo)->where('id_item', $value)->count();
          if($masterData > 0){
            #UPDATE KE MASTER DATA GUDANG
            if($update1) $update1 .= ";";
            $update1 .= "UPDATE master_data_depo SET qty =qty+".$request->qty[$key].",last_date_out='".date('Y-m-d H:i:s')."', id_user='".Auth::user()->id."' WHERE id_depo=".$id_depo." and id_item=".$value;
          }else{
            if($insert1) $insert1 .= ",";
            $insert1 .= "('".$id_depo."','".$v."','".$request->qtyDO[$k]."','".$request->satuanDO[$k]."','".date('Y-m-d H:i:s')."','0','".Auth::user()->id."')";
          }


          #UPDATE KE MASTER DATA GUDANG
          if($update) $update .= ";";
          $update .= "UPDATE master_data_gudang SET qty =qty-".$request->qty[$key].",last_date_out='".date('Y-m-d H:i:s')."', id_user='".Auth::user()->id."' WHERE id_gudang=".$id_gudang." and id_item=".$value;
      }


        $msterDataWarehouse = NEW MasterDataWarehouse;
        $dataWarehouse = $msterDataWarehouse->updateData($update);
        $msterDataDepo = NEW MasterDataDepo;

        if($update != ""){
          $dataDepo = $msterDataDepo->updateDataDepo($update1);
        }
        if($insert1 != ""){
          $dataDepo = $msterDataDepo->inputDataDepo($insert1);
        }



    } catch ( Illuminate\Database\QueryException  $e) {
      DB::rollback();
      return redirect('/catalog/warehouse/stockout/index')->with('message', 'Gagal');

    }
    DB::commit();

    return redirect('catalog/warehouse/distributed/index')->with('message', 'Sukses');
  }


    function reset(){


        return view('catalog.depo.v_reset');
    }


    function action_ganti_password(Request $request){



        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","
            Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

        die;




         echo $lama1 = $request->lama1;
         echo "<br>";

         if (Hash::needsRehash($lama1)) {
            $lama1 = Hash::make('123456');
            echo "sama";
        }else{
            echo "beda";
        }
       die;

         // echo $lama = bcrypt($request->lama);
          echo $password = Hash::make($request->lama);

         $baru = $request->baru;

       die;
        if($lama==$baru){
            DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
            return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
        }else{
            return redirect()->route('ganti_password')->with('message','Password Anda Salah');
        }



      }
}
