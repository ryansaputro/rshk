<?php
namespace App\Http\Controllers\Catalog\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Item;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Model\Contract;
use App\Model\Adendum;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;

class GuestCatalogController extends Controller
{

    public function index(Request $request){
            // $date = date('Y-m-d');
            //
            // $getAllContract = Contract::where('status', '1')->update(['status' => 0]);
            // $getAllAdendum = Adendum::where('status', '1')->update(['status' => 0]);
            //
            // $contract = DB::table('contract')
            //             ->select('contract.id', 'contract.id_vendor_detail', 'contract.status')
            //             ->where('contract.start_date', '<=', $date)
            //             ->where('contract.end_date', '>=', $date)
            //             ->distinct();
            //
            // $adendum = DB::table('adendum')
            //             ->select('adendum.id', 'adendum.id_vendor_detail', 'adendum.status')
            //             ->where('adendum.start_date', '<=', $date)
            //             ->where('adendum.end_date', '>=', $date)
            //             ->distinct();
            //
            // $id_contract =  $contract->pluck('id')->toArray();
            // $id_vendor_detail =  $contract->pluck('id_vendor_detail')->toArray();
            //
            // $id_adendum =  $contract->pluck('id')->toArray();
            // $id_vendor_detail_adendum =  $contract->pluck('id_vendor_detail')->toArray();
            //
            // $UpdateContract = Contract::whereIn('id', $id_contract)->where('id_vendor_detail', $id_vendor_detail)->update(['status' => '1']);
            // $UpdateAdendum = Adendum::whereIn('id', $id_adendum)->where('id_vendor_detail', $id_vendor_detail_adendum)->update(['status' => '1']);
            //
            // dd(Contract::all());
            $filterByCategory = ($request->category != null) ? $request->category : 'xxx';
            $search = ($request->search != null) ? $request->search : 'xxx';
            $data = DB::table('item')
                    ->select('unit_type.unit_name', 'item.*', 'category_item.id_item', 'category_item.id_category', 'vendor_detail.vendor_name')
                    ->join('category_item', 'item.id', '=', 'category_item.id_item')
                    ->join('supplier_item', 'item.id', '=', 'supplier_item.id_item')
                    ->join('vendor_detail', 'supplier_item.id_supplier', '=', 'vendor_detail.id')
                    ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                    ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
                    ->where('vendor_status.last_status', 6);

            if($filterByCategory != 'xxx') {
              $data->where('id_category', $filterByCategory);
            }

            if($search != 'xxx'){
              $data->where(function($query) use ($search)
                      {
                          $columns = ['item.code', 'item.name', 'item.merk'];

                          foreach ($columns as $column)
                          {
                              $query->orWhere($column, 'LIKE', '%'.$search.'%');
                          }
                      });
              // $data->orwhere('item.code', 'like', '%' . $search . '%')->orWhere('item.named', 'like', '%' . $search . '%')->orWhere('item.merk', 'like', '%' . $search . '%');
            }
            $item = $data->paginate(12);

            // print_r($search);
            return view('catalog.guest.item.index', compact('item','search','filterByCategory'));
    }

    public function detail($id) {
      return view('catalog.guest.item.detail')->with('id', $id);
    }

}
