<?php
namespace App\Http\Controllers\Catalog\supervisi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\OrderProposer;
use App\Model\VendorDirectur;
use App\Model\VendorDetail;
use App\Model\Manager;
use App\Model\Supervisi;
use App\Model\Finance;
use App\Model\Item;
use App\Model\Supplier;
use App\Model\Budget;
use App\Model\BudgetUsed;
use App\Model\Contract;
use App\Model\CategoryItem;
use App\Model\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;


class SupervisiCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('supervisicatalogauth');
    }

    public function companies(){
      $id_supervisi = DB::table('users')
              ->select('user_catalog_supervisi.id AS id_supervisi')
              ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
              ->where('users.id', Auth::user()->id)
              ->value('id');

      $data = DB::table('cart')
              ->select('cart.*','user_catalog_supervisi.id', 'user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone')
              ->join('user_catalog_supervisi', 'cart.id_user_supervisi', '=', 'user_catalog_supervisi.id')
              ->where('cart.id_user_supervisi', $id_supervisi);

      $companys = DB::table('user_catalog_company')
              ->select('user_catalog_company.*', 'user_catalog_supervisi.id AS id_manager', 'user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_manager.name AS name_manager')
              ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
              ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
              ->where('user_catalog_supervisi.id', $id_supervisi);

      $data_company = $data->get();
      $company = $companys->first();
      return $company;
    }

    public function index(Request $request, $id){
        $company = $this->companies();
        if (Auth::check()) {
          $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

          $Dataorder = DB::table('order_catalog')
                   ->select('category.name As name_category','order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                   ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   ->leftJoin('user_catalog', function($join){
                     $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                     $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   })
                   ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   ->where('id_user_supervisi', $id_supervisi)
                   ->where('order_catalog.status','0')
                   ->orderBy('order_catalog.datetime', 'DESC');

            $order = $Dataorder->count();

            return view('catalog.users.supervisi.index', compact('data_company','company', 'order'))->with('id', $id);
        }else{
            return redirect('auth');
        }
    }

    public function cart(Request $request) {
      error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
      $budget = Budget::where('status', '1')->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      }
      if($budgetUsed != null){
        $budgetYear = ($budgetTotal->budget)-$budgetUsed;
      }else{
        $budgetYear = (count($budgetJml) > 0) ? $budgetTotal->budget : 0;
      }
      $company = $this->companies();
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $data =DB::table('cart')
                  ->select('user_catalog_supervisi.name AS pembeli','user_catalog_supervisi.mobile_phone', 'user_catalog_supervisi.email','cart.*','item.price_shipment','item.code','item.name','item.merk','item.price_country','item.price_retail','item.price_gov','item.price_date','item.release_date','item.expired_date','item.stock','item.production_origin','item.description','item.status', 'image_item.file')
                  ->join('item', 'cart.id_item', '=', 'item.id')
                  ->join('image_item', 'item.id','=','image_item.id_item')
                  ->join('users', 'cart.id_user', '=', 'users.id')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('cart.id_user_supervisi', $id_supervisi)
                  ->where('cart.id_user', Auth::user()->id)
                  ->where('cart.status','1')
                  ->groupBy('image_item.id_item');

      $dataCart = $data->get();

      $supervisi = Supervisi::where('id_user', Auth::user()->id)->first();

      $pembeli = DB::table('users')
                ->select('user_catalog_supervisi.name AS pembeli','user_catalog_supervisi.mobile_phone', 'user_catalog_supervisi.email')
                ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                ->where('users.id', Auth::user()->id)
                ->first();

      $usulan = OrderProposer::all();


      return view('catalog.users.supervisi.cart', compact('budgetYear','dataCart', 'company', 'pembeli', 'supervisi', 'vendor','usulan'));
    }

    public function delete_cart(Request $request) {
      $cart = Cart::find($request->id_cart);
      $id_item = $cart->value('id_item');
      $item_name = Item::where('id', $id_item)->value('name');
      $cart_delete = $cart->delete();

      if($cart) $status = 1;
      else $status =0;
      return response()->json(array(
                               'id_cart' => $request->id_cart,
                               'item' => $item_name,
                               'status' => $status));
    }

    public function order(Request $request) {
      $usulan = $request->usulan;
      $id_item = $request->id_item;
      $qty = $request->qty;
      $subtot = $request->subtot;
      $id_vendor = $request->id_vendor_detail;
      $item = "";
      $id = "";
      $id_order = 1;
      $status_detail = 1;
      $id_user = Auth::user()->id;
      $datetime = date("Y-m-d H:i:s");
      $status = '0';
      $vendor = "";
      $budgetTotal = Budget::where('status', '1')->orderBy('periode', 'DESC')->first();
      $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      if($budgetUsed != null){
        $budgetYear = ($budgetTotal->budget)-$budgetUsed;
      }else{
        $budgetYear = 0;
      }
      if($request->totalAll <= $budgetYear){
          $id_supervisi = Cart::where('id_user', $id_user)->value('id_user_supervisi');
          $id_buyer = DB::table('users')
                      ->select('user_catalog.id AS id_buyer')
                      ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                      ->where('users.id', $id_user)
                      ->value('id');

          $budget = Budget::where('id_supervisi', $id_supervisi)->value('id');
          foreach($id_item as $k => $v){
            $order = Order::create([
              'id_proposer' => $usulan,
              'id_user_buyer' => $id_user,
              'id_user_supervisi' => $id_supervisi,
              'id_vendor_detail' => $id_vendor[$k],
              'status' => $status,
              'datetime' => $datetime,
              'total' => $subtot[$k]
            ]);
            foreach($v as $kItem => $vItem){
              if($item) $item .= ",";
              $item .= "('".$order->id."','".$vItem."','".$qty[$k][$vItem]."','".$status_detail."','".$datetime."')";
              #Insert History
              if($id) $id .= ",";
              $id .= "('".$order->id."','".$vItem."','".$qty[$k][$vItem]."','".$id_buyer."','".$id_user."','1','".$datetime."','NEW','".$datetime."')";
            }
            // $budgetUsed = BudgetUsed::create([
            //   'id_budget' => $budget,
            //   'budget_used' => $subtot[$k],
            //   'datetime' => $datetime,
            //   'used_by' => Auth::user()->id,
            //   'id_proposer' => $usulan,
            //   'id_order' => $order->id,
            // ]);
          }
          $order_detail = New OrderDetail;
          $save = $order_detail->inputOrder($item);
          if($save)
            $deleteCart = Cart::where('id_user', $id_user)->update(['status' => '0']);
            if($deleteCart)
            $history = NEW OrderHistory;
              $insertUpdateOldQty = $history->OldQty($id);
              if($insertUpdateOldQty)
          return redirect()->back()->with('message', 'Pesanan telah dibuat');;
        }else{
          return redirect()->back()->with('message', 'Budget tidak cukup pesanan gagal dibuat');;
        }
    }
    public function order_waiting(Request $request) {


      error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
      $company = $this->companies();
      $id_supervisi = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $budget = Budget::where('status', '1')->where('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      }
      if(($budgetUsed != null)){
        $budgetYear = ($budgetTotal->budget)-$budgetUsed;
      }else{
        $budgetYear = $budgetTotal->budget;
      }
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $Dataorder = DB::table('order_catalog')
                 ->select('user_catalog.kode_instalasi', 'category.name As name_category','order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                 ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                 ->join('category', 'order_catalog.id_category', '=', 'category.id')
                 ->leftJoin('user_catalog', function($join){
                   $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                   $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                 })
                 ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                 ->where('id_user_supervisi', $id_supervisi)
                 ->where('order_catalog.status','0')
                 ->orderBy('order_catalog.datetime', 'DESC');

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->get();

      $item = DB::table('item')
              ->select('item.*', 'category_item.id_item', 'category_item.id_category')
              ->join('category_item', 'item.id', '=', 'category_item.id_item')->get();



      return view('catalog.users.supervisi.order', compact('order','company','item', 'budgetYear'))->render();
    }

    public function detail_order(Request $request) {
      $id_order = $request->id_order;
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog_detail.*', 'item.stock', 'item.price_shipment',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->where('order_catalog_detail.id_order', $id_order);
      $dataItem = Order::where('id', $id_order)->first();
      $proposer = OrderProposer::where('id', $dataItem->id_proposer)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'proposer' => $proposer,
                               'status' => 1));
    }

    public function approvement(Request $request) {

      // $nama = "Nama";
      // $pesan = "pesan";
      // $judul = "judul";
      // $dituju = "ajie.darmawan106@gmail.com";


       $request->proposer1;
       $request->name_buyer1;

       //Notifikasi
       // $userkey = "hfvns6"; //userkey lihat di zenziva
       // $passkey = "3q8wehsntc"; // set passkey di zenziva
       // $telepon = "085649184363";//$no_tlpn; // ke supervisi nya
       // $nama = $unit_intalasi;
       // $message = "$request->name_buyer1 telah melakukan pengusulan pada no Usulan $request->proposer1 Silakan Anda Untuk melakukan Konfirmasi";
       // $url = "https://reguler.zenziva.net/apps/smsapi.php";
       // $curlHandle = curl_init();
       // curl_setopt($curlHandle, CURLOPT_URL, $url);
       // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
       // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
       // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
       // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
       // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
       // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
       // curl_setopt($curlHandle, CURLOPT_POST, 1);
       // $results = curl_exec($curlHandle);
       // curl_close($curlHandle);
       //
       // $XMLdata = new SimpleXMLElement($results);
       // $status = $XMLdata->message[0]->text;
       // // echo $status;
       //

       // //Unit nya
       //
       // $userkey = "hfvns6"; //userkey lihat di zenziva
       // $passkey = "3q8wehsntc"; // set passkey di zenziva
       // $telepon = "089698930980";
       // $nama = "tes";
          // $message = "$request->name_buyer1 telah melakukan pengusulan pada no Usulan $request->proposer1 Telah disetujui oleh kepala Bagian";
       // $url = "https://reguler.zenziva.net/apps/smsapi.php";
       // $curlHandle = curl_init();
       // curl_setopt($curlHandle, CURLOPT_URL, $url);
       // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
       // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
       // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
       // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
       // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
       // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
       // curl_setopt($curlHandle, CURLOPT_POST, 1);
       // $results = curl_exec($curlHandle);
       // curl_close($curlHandle);
       //
       // $XMLdata = new SimpleXMLElement($results);
       // $status = $XMLdata->message[0]->text;


      // try{
      //     Mail::send('catalog.users.email.email_atasan', ['nama' => $request->name_buyer1, 'pesan' => 'telah melakukan pengusulan pada no Usulan '.$request->proposer1.' Silakan Anda Untuk melakukan Konfirmasi'], function ($message) use ($request)
      //     {
      //         $message->subject('tes');
      //         $message->from('donotreply@kiddy.com', 'Kiddy');
      //         $message->to('ajie.darmawan106@gmail.com');
      //     });
      //     return back()->with('alert-success','Berhasil Kirim Email');
      //     }
      //     catch (Exception $e){
      //         return response (['status' => false,'errors' => $e->getMessage()]);
      //     }


          // dd($request->all());

      DB::beginTransaction();
      try {
              $order_id = $request->id_order;
              $id_item = $request->id_item;
              $price_pcs = $request->price_pcs;
              $id_user = $request->id_user;
              $status = $request->status;
              $description = $request->description;
              $qty = $request->qty;
              $datetime = date("Y-m-d H:i:s");
              $id_executor = Auth::user()->id;
              $id_supervisi = DB::table('users')
                          ->select('user_catalog_supervisi.id AS id_supervisi')
                          ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                          ->where('users.id', $id_executor)
                          ->value('id');

              $cekOrder = OrderDetail::where('id_order', $order_id)->get();
              $Orderan = Order::where('id', $order_id);
              $cekIdOrder = $Orderan->value('id_user_buyer');
              $budget = Budget::where('id_supervisi', $id_supervisi)->value('id');
              $id = "";
              $update = "";
              $insert = "";
              $idReject = [];
              foreach($cekOrder AS $k => $v){
                if($v->qty != $qty[$v->id_item]){
                  #INSERT KE TABLE HISTORY
                  if($id) $id .= ",";
                  if($v->qty < $qty[$v->id_item]){
                    $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','ADD','".$datetime."')";
                  }else{
                    $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','REDUCE','".$datetime."')";

                  }
                  #UPDATE KE TABLE ORDER CATALOG DETAIL
                  if($update) $update .= ";";
                    if($status[$v->id_item] == 1){
                      $update .= "UPDATE order_catalog_detail SET qty =".$qty[$v->id_item].",id_user_supervisi_approve='".$id_supervisi."', datetime_supervisi='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
                    }
                }else{
                  if($update) $update .= ";";
                    $update .= "UPDATE order_catalog_detail SET id_user_supervisi_approve='".$id_supervisi."', datetime_supervisi='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
                }

                #status item yg di reject Insert ke history
                if($status[$v->id_item] == 0){
                  if($insert) $insert.=",";
                  $insert .= "('".$order_id."','".$v->id_item."','".$v->qty."','".$id_user."','".$id_executor."','1','".$datetime."','REJECT','".$description[$v->id_item]."','".$datetime."')";
                }else {
                  // $budgetUsed = BudgetUsed::create([
                  //   'id_budget' => $budget,
                  //   'budget_used' => ($qty[$v->id_item]-1)*$price_pcs[$v->id_item],
                  //   'datetime' => $datetime,
                  //   'used_by' => Auth::user()->id,
                  //   'id_proposer' => $Orderan->value('id_proposer'),
                  //   'id_order' => $Orderan->value('id'),
                  // ]);
                }

              }

              foreach($status as $k => $v){
                if($v == 0){
                  $idReject[] = $k;
                }
              }


              $history = NEW OrderHistory;
              $detailOrder = NEW OrderDetail;
              // $approveStatus = Order::where('id', $order_id)->update(['status' => '1']);
              $updateOrderan = $Orderan->update(['total' => $request->subtot, 'status' => '2']);

              if($id != ""){
                $insertUpdateOldQty = $history->OldQty($id);
              }
              if($update != ""){
              $updateDetailOrder = $detailOrder->UpdateOrder($update);
              }
              if($insert != ""){
              $reject = $history->RejectHistory($insert);
              }
              if(count($idReject) > 0){
                $deleteOrder = OrderDetail::where('id_order', $order_id)->whereIn('id_item',$idReject)->delete();
              if($deleteOrder)
                echo "Sukses Insert Ke History";
              else
                return redirect()->back()->with('message', 'Gagal');
              }


              Mail::send('catalog.users.email.email_atasan', ['nama' => $request->name_buyer1, 'pesan' => 'telah melakukan pengusulan pada no Usulan '.$request->proposer1.' Silakan Anda Untuk melakukan Konfirmasi'], function ($message) use ($request)
              {
                  $message->subject('tes');
                  $message->from('donotreply@kiddy.com', 'Kiddy');
                  $message->to('ajie.darmawan106@gmail.com');
              });

              $data = 'Pesanan telah di setujui';
              // return back()->with('alert-success','Berhasil Kirim Email');

      } catch (\Exception $e) {
        DB::rollback();
        $data ='Pesanan gagal di setujui';
        return back()->with('fail', $data);

      }

      DB::commit();
      return redirect()->back()->with('success', $data);
    }

    public function order_approved(Request $request) {
       $company = $this->companies();
       $filter = ($request->filter != null) ? $request->filter : 'xxx';
       $search = ($request->search != null) ? $request->search : 'xxx';
       $id_supervisi = Supervisi::where('id_user', Auth::user()->id)->value('id');
       $Dataorder = DB::table('order_catalog')


                   ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog_supervisi.name')
                   ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   ->leftJoin('user_catalog', function($join){
                     $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                     $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   })
                   ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   ->where('id_user_supervisi', $id_supervisi)
                   ->where('order_catalog.status','2')
                   ->orderBy('order_catalog.datetime','DESC')
                   ->orderBy('order_catalog.no_order','DESC');


                 // ->select('category.name As name_category','order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog_supervisi.name')
                 // ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                 // ->join('category', 'order_catalog.id_category', '=', 'category.id')
                 // ->leftJoin('user_catalog', function($join){
                 //   $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                 //   $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                 // })
                 // ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                 // ->where('id_user_supervisi', $id_supervisi)
                 // ->where('order_catalog.status','2')
                 // ->orderBy('order_catalog.datetime','DESC')
                 // ->orderBy('order_catalog.no_order','DESC');


                   if($filter == 'po'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_catalog.no_po', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                     }
                   }else if($filter == 'usulan'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                     }

                   }else if($filter == 'tanggal'){
                     $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                   }

                   $order = $Dataorder->paginate(10);
       return view('catalog.users.supervisi.approvedorder', compact('order','company'));
     }

    public function track(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');


      $Dataorder = DB::table('order_catalog')
                  ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('category', 'order_catalog.id_category', '=', 'category.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->where('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.datetime','DESC')
                  ->orderBy('order_catalog.no_order', 'DESC');

                  // $Dataorder = DB::table('order_catalog')
                  //              ->select('category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer')
                  //              ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  //              ->join('category', 'order_catalog.id_category', '=', 'category.id')
                  //              ->leftJoin('user_catalog', function($join){
                  //                $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                  //                $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  //              })
                  //              ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  //              ->where('id_user_supervisi', $id_supervisi)
                  //              ->orderBy('order_catalog.datetime','DESC')
                  //              ->orderBy('order_catalog.no_order', 'DESC');


                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }
                  $order = $Dataorder->paginate(10);
      return view('catalog.users.supervisi.listorder', compact('order','company'));
    }

    public function track_order(Request $request) {
      $super = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $order = DB::table('order_catalog')
                  ->select('receive_item.id_user', 'users.name','receive_item.datetime AS date_receive', 'delivery_order.datetime AS date_do', 'delivery_order.driver', 'delivery_order.car_no', 'delivery_order.car','order_catalog.id_user_buyer AS super', 'user_catalog_supervisi.id_user AS users','order_catalog.status AS status_pesan',
                  'order_catalog_detail.*',
                            'order_proposer.id AS id_proposer','order_proposer.no_prop', 'order_proposer.proposer',
                            'user_catalog.telephone AS mobile_user','user_catalog.email as email_user',
                            'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email',
                            'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager',
                            'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager',
                            'user_catalog_directur.name AS name_directur',
                            'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email AS email_finance',
                            'user_catalog_finance.mobile_phone AS mobile_phone_finance')
                  ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                  ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
                  ->leftJoin('user_catalog_supervisi', function($join){
                    $join->orOn('order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user');
                    $join->orOn('order_catalog_detail.id_user_supervisi_approve', '=', 'user_catalog_supervisi.id');
                    $join->orOn('users.id', '=', 'user_catalog_supervisi.id_user');
                  })
                  ->leftJoin('user_catalog_manager', 'order_catalog_detail.id_user_manager_approve', '=', 'user_catalog_manager.id')
                  ->leftJoin('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                  ->leftJoin('user_catalog_finance', 'order_catalog_detail.id_user_finance_approve', '=', 'user_catalog_finance.id')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
                  ->leftJoin('receive_item', function($join){
                    $join->on('order_catalog.id', '=', 'receive_item.id_order');
                    $join->orOn('users.id', '=', 'receive_item.id_user');
                    })
                  ->where('order_catalog.id', $request->id_order)
                  ->where('order_catalog.id_user_supervisi', $super)
                  ->get();
                  $users = DB::table('receive_item')->select('users.name')->join('users', 'receive_item.id_user', '=', 'users.id')->first();
                  return response()->json(array(
                                           'users' => $users,
                                           'id_order' => $request->id_order,
                                           'data' => $order,
                                           'status' => 1));
    }

    public function history(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_supervisi = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $Dataorder = DB::table('order_catalog')

                  ->select('user_catalog.kode_instalasi', 'category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('category', 'order_catalog.id_category', '=', 'category.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->where('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.datetime', 'DESC');

                   // ->select('category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer')
                   // ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   // ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   // ->leftJoin('user_catalog', function($join){
                   //   $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                   //   $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   // })
                   // ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   // ->where('id_user_supervisi', $id_supervisi)
                   // ->orderBy('order_catalog.datetime','DESC')
                   // ->orderBy('order_catalog.no_order', 'DESC');


                    if($filter == 'status'){
                      if($search != '5'){
                        $Dataorder->where('order_catalog.status', $search);
                      }
                    }else if($filter == 'po'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_catalog.no_po', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                      }
                    }else if($filter == 'usulan'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                      }

                    }else if($filter == 'tanggal'){
                      $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                    }

                  $order = $Dataorder->paginate(10);
      return view('catalog.users.supervisi.history', compact('order','company'));
    }
    public function detail_history(Request $request) {
      $id_order = $request->id_order;
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $dataOrder = DB::table('order_catalog_history')
                  ->select('order_catalog_history.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov', 'user_catalog.username_catalog', 'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_manager.name AS name_manager','user_catalog_directur.name AS name_directur')
                  ->join('item', 'order_catalog_history.id_item', '=', 'item.id')
                  ->join('users', 'order_catalog_history.id_executor', '=', 'users.id')
                  ->leftJoin('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->leftJoin('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->leftJoin('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->leftJoin('user_catalog_directur', 'users.id', '=', 'user_catalog_directur.id_user')
                  ->where('order_catalog_history.id_order', $id_order)
                  ->orderBy('order_catalog_history.datetime_history','DESC');

      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

    public function downloadProposer($id_proposer){

      $data = DB::select("SELECT *
      from order_proposer
      where md5(CAST(id AS VARCHAR(50))) = '".$id_proposer."'");


      // echo "<pre>";
      // print_r($posts);
      // echo "</pre>";
      // die;
      //$no_prop = $posts[0]->lpad;






      // $data = DB::table('order_proposer')
      //       ->where('CAST(id AS VARCHAR(50))',$id_proposer)
      //       ->select('*')
      //       ->first();

      //$data =
      //::where(DB::raw('md5(id)'), $id_proposer)->first();





      // $dataOrder = DB::table('order_catalog_detail')
      //       ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
      //       ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
      //       ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
      //       ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
      //       //->where('md5(CAST(order_proposer.id AS VARCHAR(50))', $id_proposer)
      //       //->where(DB::raw('md5(CAST(order_proposer.id AS VARCHAR(50))'),'=', $id_proposer)
      //       //->whereRaw('md5(CAST(order_proposer.id AS VARCHAR(50)) ='.$id_proposer)
      //       ->get();

      $dataOrder = DB::select("select order_catalog.datetime,order_catalog_detail.*,item.price_shipment,item.code,item.name,item.merk,
      item.price_retail,item.price_gov,item.price_country
      from order_catalog_detail
      join item on order_catalog_detail.id_item=item.id
      join order_catalog on order_catalog_detail.id_order = order_catalog.id
      join order_proposer on order_catalog.id_proposer = order_proposer.id
      where md5(CAST(order_proposer.id AS VARCHAR(50))) = '".$id_proposer."'");

        // echo "<pre>";
        // print_r($dataOrder);
        // echo "</pre>";
        // die;


      $pdf = PDF::loadView('catalog.users.doc.usulan', compact('data','dataOrder'));
      // return $pdf->download('usulan.pdf');
      return $pdf->stream();
    }

    public function downloadPo($id){




      $data = Order::where(DB::raw('md5(cast(id as varchar(50)))'), $id)
              // ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $bln)
              // ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $thn)
              ->first();
      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $directur = DB::table('user_catalog_directur')
                  ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
                  ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
                  ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                  ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
                  ->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(cast(order_catalog.id as varchar(50)))'), $id)            // ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $bln)
            // ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $thn)
            ->get();
      // $pdf = PDF::loadView('catalog.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur'));
      // // return $pdf->download('usulan.pdf');
      // return $pdf->stream();
      // $list_propinsi = Fungsi::propinsi();
      // $list_kota = Fungsi::IndonesiaProvince();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
      ############cara dapetin nomor sesui dengan kategori utama
      $menentukanKate = OrderDetail::where('id_order', $data->id)->pluck('id_item')->toArray();
      $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
      $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
      $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
      $mainCat = Category::whereIn('id', $subCategory)->value('name');


      return view('catalog.users.doc.po', compact('contract', 'mainCat', 'data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
    }

    public function item() {
      $budget = Budget::where('status', '1')->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      }
      if($budgetUsed != null){
        $budgetYear = ($budgetTotal->budget)-$budgetUsed;
      }else{
        $budgetYear = 0;
      }
      $item = DB::table('item')
              ->select('item.*', 'category_item.id_item', 'category_item.id_category')
              ->join('category_item', 'item.id', '=', 'category_item.id_item')->paginate(10);
      return view('catalog.users.supervisi.item', compact('budgetYear', 'item'))->render();
    }

    public function addcart(Request $request) {
        $id_user = Auth::user()->id;
        $super =  Supervisi::where('id_user', Auth::user()->id)
                  ->first();
        $id_catalog_company = Auth::user()->id_user_catalog_company;
        $id_manager = Manager::where('id_user_catalog_company',$id_catalog_company)->value('id');
        $id_finance = Finance::where('id_user_manager',$id_catalog_company)->value('id');
        $id_item = $request->id;
        $dataCart = Cart::where('id_user', $id_user);
        $qty = $dataCart->where('id_item', $id_item)->value('qty');
        $item_name = Item::where('id', $id_item)->value('name');
        $supplier = Supplier::where('id_item', $id_item)->first();
        $cart = Cart::updateorCreate(
            [
              'id_item' => $id_item,
              'id_user' => $id_user,
              'id_user_manager' => $id_manager,
              'id_user_supervisi' => $super->id,
              'id_user_finance' => $id_finance,
              'id_vendor_detail' => $supplier->id_supplier,
            ],
            ['qty' => $qty+1,
            'status' => '1']);

        $Cart = Cart::where('id_user', $id_user)->where('status','1')->groupBy('id_item')->get();
        $totCart = count($Cart);
        return response()->json(array(
                                 'item_name' => $item_name,
                                 'cart' => $totCart,
                                 'status' => 1));
    }

    public function budget() {
      $id_supervisi = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $budget = Budget::where('status', '1')->where('budget.id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      }
      $budget = DB::table('budget')
              ->select('budget.*', 'user_catalog_manager.name')
              ->join('user_catalog_manager', 'budget.approved_by', '=', 'user_catalog_manager.id')
              ->where('budget.status', '1')
              ->where('budget.id_supervisi', $id_supervisi)
              ->get();
      $budgetYear = $budgetTotal;
      return view('catalog.users.supervisi.budget', compact('budget', 'budgetYear', 'budgetUsed'));
    }

    public function budgetDetail(Request $request) {
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_supervisi = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $Dataorder = DB::table('budget_used')
                  ->select('budget_used.budget_used','budget_used.datetime','order_catalog.datetime AS date_order', 'order_proposer.datetime AS date_prop','order_catalog.no_po', 'order_proposer.no_prop', 'user_catalog.username_catalog','order_catalog.status', 'order_proposer.no_prop', 'order_proposer.proposer',  'order_proposer.id AS id_proposer', 'user_catalog_supervisi.name')
                  ->join('budget', 'budget_used.id_budget', '=', 'budget.id')
                  ->join('order_proposer', 'budget_used.id_proposer', '=', 'order_proposer.id')
                  ->join('order_catalog', 'budget_used.id_order', '=', 'order_catalog.id')
                  ->leftJoin('user_catalog', 'budget_used.used_by', '=', 'user_catalog.id_user')
                  ->leftJoin('user_catalog_supervisi', 'budget_used.used_by', '=', 'user_catalog_supervisi.id_user')
                  ->where('budget.id_supervisi', $id_supervisi)
                  ->orderBy('budget_used.datetime', 'DESC');

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }
                  $DataDetail = $Dataorder->paginate(10);

      $budget = Budget::where('status', '1')->where('budget.id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
      }
      $budget = DB::table('budget')
              ->select('budget.*', 'user_catalog_manager.name')
              ->join('user_catalog_manager', 'budget.approved_by', '=', 'user_catalog_manager.id')
              ->where('budget.status', '1')
              ->where('budget.id_supervisi', $id_supervisi)
              ->get();
      $budgetYear = Budget::where('status', '1')->orderBy('periode', 'DESC')->first();
      return view('catalog.users.supervisi.budget_detail', compact('DataDetail', 'budget', 'budgetYear', 'budgetUsed'));
    }

    public function budgetCreate(Request $request) {
      // $budgetYear = Budget::where('status', '1')->orderBy('periode', 'DESC')->first();
      $budget = Budget::where('status', '1')->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetYear = $budget->first();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::where('id_budget', $budgetYear->id)->sum('budget_used');
      }
      if($budgetJml->count() == 0){
        if($budgetYear->periode != date('Y-m')){
          $manager = DB::table('user_catalog_supervisi')
                  ->select('user_catalog_manager.name', 'user_catalog_manager.id')
                  ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                  ->where('user_catalog_supervisi.id_user', Auth::user()->id)
                  ->get();
                    return view('catalog.users.supervisi.budget_create', compact('manager'));
        }else{
          return abort(404);
        }
      }else{
        $manager = DB::table('user_catalog_supervisi')
                ->select('user_catalog_manager.name', 'user_catalog_manager.id')
                ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                ->where('user_catalog_supervisi.id_user', Auth::user()->id)
                ->get();
                  return view('catalog.users.supervisi.budget_create', compact('manager'));
      }
    }

    public function budgetSave(Request $request) {
      $super = Supervisi::where('id_user', Auth::user()->id)->value('id');
      $budget = Budget::create([
        'id_supervisi' => $super,
        'periode' => $request->periode,
        'approved_by' => '1',
        'budget' => str_replace(".","", $request->budget),
        'status' => 1,
      ]);
      $budget = Budget::where('status', '1')->get();
      return redirect('catalog/users/supervisi/budget')->with('message', 'Sukses Input Budget!');
    }

   public function user(){

    $id_user = Auth::user()->id;

      $user = DB::table('user_catalog_supervisi as d')
      ->Join('users as u', 'u.id', '=', 'd.id_user')
      ->where('d.id_user','=',$id_user)
      ->select('d.id')
      ->first();


       $data = DB::table('user_catalog as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->where('id_user_catalog_supervisi','=',$user->id)
           ->select('*')
           ->get();


      return view('catalog.users.supervisi.user',compact('data'))->render();
    }

    public function user_tambah(){



       return view('catalog.users.supervisi.user_tambah');
    }

    public function user_simpan(Request $request){



        $request->nama_instalasi;
        $res =  $request->id_kategory;

        // print_r($res);
        // echo "<br>";
         $id_kategory = implode(",",$res);


      //  $data = array(
      //   'kode_instalasi'       =>$request->kode,
      //   'nama_instalasi'       =>$request->nama_instalasi,
      //   'id_kategory'          =>$id_kategory,
      //   'id_direktur_penunjang'=>$request->id_direktur_penunjang,
      // );

      //  $id=DB::table('master_staff')->insertGetId($data);


       $data_lap = array(
                    //'id_staff'                =>  $id,
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>  bcrypt($request->password),
                    'status'                =>  '1',
                    );
       $id_user = DB::table('users')->insertGetId($data_lap);



       DB::table('user_catalog')->insert(
        [
          'id_user_catalog_company'      => '1',
          'id_user_catalog_supervisi'    => $request->id_direktur_penunjang,
          'id_user'                      => $id_user,
          'username_catalog'             => $request->username,
          'email'                        => $request->email,
          'telephone'                 => $request->no_tlpn,
          //'nik'                          => $request->nik,
          'kode_instalasi'               => $request->kode,
          'nama_instalasi'               => $request->nama_instalasi,
          'id_kategory'                  =>$id_kategory,

        ]
      );



        $data = array(
            'user_id'       =>$id_user,
            'role_id'       =>3,
          );

       $id=DB::table('role_user')->insertGetId($data);

       return redirect()->to('/catalog/users/supervisi/user')->with('message','Data Berhasil Disimpan');
    }

    function user_kategory(Request $request){
       error_reporting(0);
         $request->id_kategory;

        $a = explode(",",$request->id_kategory);


         $data1 = DB::table('category')
           ->WhereIn('id',$a)
           ->select('name','id')
           ->get();


        $data.="<table class='table table-hover table-bordered' ><thead><tr><th>Kategory</th></tr></thead>";
        foreach ($data1 as $sub) {
            $data.="<tr><td>$sub->name</td></tr> ";

        }
        $data.="</table>";
        echo$data;



    }

     public function user_edit($id){


     $data1 = DB::table('user_catalog as d')
           ->Join('users as u', 'u.id', '=', 'd.id_user')
           ->where('d.id_user',$id)
           ->select('*')
           ->first();



      $data2 = DB::table('user_catalog')
           ->select('*')
           ->get();


       return view('catalog.users.supervisi.user_edit',compact('data1','data2'));
    }


    public function user_edit_simpan(Request $request,$id){


          $request->nama_instalasi;
        $res =  $request->id_kategory;

        // print_r($res);
        // echo "<br>";
         $id_kategory = implode(",",$res);


        $data = DB::table('users')
           ->where('id',$id)
           ->select('id_staff')
           ->first();

           // echo "<pre>";
           // print_r($data);
           // echo "</pre>";
       $id_dir = $data->id_staff;


        // $data = array(
        // 'kode_instalasi'       =>$request->kode,
        // 'nama_instalasi'       =>$request->nama_instalasi,
        // 'id_kategory'          =>$id_kategory,
        // 'id_direktur_penunjang'=>$request->id_direktur_penunjang,
        // );

        // DB::table('master_staff')->where('id_staff',$id_dir)->update($data);


        if($request->password!=null){
             $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    'password'              =>   bcrypt($request->password),
                    'status'                =>  '1',
                    );
        }else{
           $data_lap = array(
                    'name'                  =>  $request->username,
                    'username'              =>  $request->username,
                    'email'                 =>  $request->email,
                    // 'password'              =>  Hash::make($request->password."123"),
                    'status'                =>  '1',
                    );
        }



       DB::table('users')->where('id',$id)->update($data_lap);


       DB::table('user_catalog')->where('id_user',$id)->update(
        [
          'id_user_catalog_company'      => '1',
          'id_user_catalog_supervisi'    => $request->id_direktur_penunjang,
          //'id_user'                      => $id_user,
          'username_catalog'             => $request->username,
          'email'                        => $request->email,
          'telephone'                    => $request->no_tlpn,
          //'nik'                          => $request->nik,
          'kode_instalasi'               => $request->kode,
          'nama_instalasi'               => $request->nama_instalasi,
          'id_kategory'                  =>$id_kategory,

        ]
      );



   return redirect()->to('/catalog/users/supervisi/user')->with('message','Data Berhasil Disimpan');

    }

    function user_delete(Request $request,$id){
      $data = DB::table('users')
           ->where('id',$id)
           ->select('id_staff')
           ->first();




        DB::table('user_catalog')->where('id_user', $id)->delete();
        DB::table('role_user')->where('user_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

        return redirect()->to('/catalog/users/supervisi/user')->with('message','Data Berhasil DiHapus');
    }


    function reset(){

        return view('catalog.users.supervisi.v_reset');
    }


    function action_ganti_password(Request $request){



        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","
            Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

        die;




         echo $lama1 = $request->lama1;
         echo "<br>";

         if (Hash::needsRehash($lama1)) {
            $lama1 = Hash::make('123456');
            echo "sama";
        }else{
            echo "beda";
        }
       die;

         // echo $lama = bcrypt($request->lama);
          echo $password = Hash::make($request->lama);

         $baru = $request->baru;

       die;
        if($lama==$baru){
            DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
            return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
        }else{
            return redirect()->route('ganti_password')->with('message','Password Anda Salah');
        }



      }



  }
