<?php
namespace App\Http\Controllers\Catalog\finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\OrderProposer;
use App\Model\VendorDetail;
use App\Model\VendorDirectur;
use App\Model\Supervisi;
use App\Model\Contract;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\Bast;
use App\Model\BastDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;


class FinanceAkutansiController extends Controller
{
  public function __construct()
  {
      $this->middleware('financeakutansiauth');

  }

  public function companies(){
    $id_finance = DB::table('users')
                ->select('user_catalog_finance.id AS id_manager')
                ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                ->where('users.id', Auth::user()->id)
                ->value('id');
    $data = DB::table('cart')
            ->select('cart.*','user_catalog_finance.id', 'user_catalog_finance.name', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone')
            ->join('user_catalog_finance', 'cart.id_user_manager', '=', 'user_catalog_finance.id')
            ->where('cart.id_user_finance', $id_finance);
    $companys = DB::table('user_catalog_company')
            ->select('user_catalog_company.*', 'user_catalog_finance.id AS id_finance', 'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone', 'user_catalog_manager.name AS name_manager')
            ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
            ->join('user_catalog_finance', 'user_catalog_manager.id', '=', 'user_catalog_finance.id_user_manager')
            ->where('user_catalog_finance.id', $id_finance);

    $data_company = $data->get();
    $company = $companys->first();
    return $company;
  }

  public function bast_waiting(Request $request) {
    $company = $this->companies();
    $filter = ($request->filter != null) ? $request->filter : 'xxx';
    $search = ($request->search != null) ? $request->search : 'xxx';

    $id_finance = DB::table('users')
                ->select('user_catalog_finance.id AS id_finance')
                ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                ->where('users.id', Auth::user()->id)
                ->value('id');

    $id_supervisi = DB::table('user_catalog_finance')
                ->select('user_catalog_supervisi.id AS id_supervisi')
                ->join('user_catalog_supervisi', 'user_catalog_finance.id_user_manager', '=', 'user_catalog_supervisi.id_user_manager')
                ->where('user_catalog_finance.id', $id_finance)
                ->pluck('user_catalog_supervisi.id_supervisi')
                ->toArray();

    $Dataorder = DB::table('order_catalog')
                ->select('bast.id AS id_bast', 'bast.status AS status_bast', 'order_proposer.no_prop','order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_manager.name AS name_manager', 'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager')
                ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                ->join('bast', 'order_catalog.id', '=', 'bast.id_order')
                ->whereIn('id_user_supervisi', $id_supervisi)
                ->where(function($q) {
                     $q->where('order_catalog.status', 6)
                       ->orWhere('order_catalog.status', 5);
                 })

                ->where('bast.status', '1')
                ->orderBy('order_catalog.datetime', 'DESC');

                // dd($Dataorder->get());

                if($filter == 'po'){
                  $pencarian = explode('/',$search);
                  if(count($pencarian) > 1){
                    $Dataorder->where('order_catalog.no_po', $pencarian[0])
                    ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                    ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                  }else{
                    $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                  }
                }else if($filter == 'usulan'){
                  $pencarian = explode('/',$search);
                  if(count($pencarian) > 1){
                    $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                    ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                    ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                  }else{
                    $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                  }

                }else if($filter == 'tanggal'){
                  $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                }

                $order = $Dataorder->paginate(10);

    return view('catalog.users.financeakutansi.bast', compact('order','company'));
  }


}
