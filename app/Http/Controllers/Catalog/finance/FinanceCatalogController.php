<?php
namespace App\Http\Controllers\Catalog\finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\OrderProposer;
use App\Model\VendorDetail;
use App\Model\VendorDirectur;
use App\Model\Supervisi;
use App\Model\Contract;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\Bast;
use App\Model\BastDetail;
use App\Model\ReceiveItem;
use App\Model\DeliveryOrder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;

class FinanceCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('financeauth');
    }

    public function companies(){
      $id_finance = DB::table('users')
                  ->select('user_catalog_finance.id AS id_manager')
                  ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_finance.id', 'user_catalog_finance.name', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone')
              ->join('user_catalog_finance', 'cart.id_user_manager', '=', 'user_catalog_finance.id')
              ->where('cart.id_user_finance', $id_finance);
      $companys = DB::table('user_catalog_company')
              ->select('user_catalog_company.*', 'user_catalog_finance.id AS id_finance', 'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone', 'user_catalog_manager.name AS name_manager')
              ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
              ->join('user_catalog_finance', 'user_catalog_manager.id', '=', 'user_catalog_finance.id_user_manager')
              ->where('user_catalog_finance.id', $id_finance);

      $data_company = $data->get();
      $company = $companys->first();
      return $company;
    }

    public function index(Request $request, $id){
        if (Auth::check()) {
          $id_finance = DB::table('users')
                      ->select('user_catalog_finance.id AS id_finance')
                      ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                      ->where('users.id', Auth::user()->id)
                      ->value('id_finance');
          $data = DB::table('cart')
                  ->select('cart.*','user_catalog_finance.id', 'user_catalog_finance.name', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone')
                  ->join('user_catalog_finance', 'cart.id_user_manager', '=', 'user_catalog_finance.id')
                  ->where('cart.id_user_finance', $id_finance);
          $companys = DB::table('user_catalog_company')
                  ->select('user_catalog_company.*', 'user_catalog_finance.id AS id_finance', 'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email', 'user_catalog_finance.mobile_phone', 'user_catalog_manager.name AS name_manager')
                  ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                  ->join('user_catalog_finance', 'user_catalog_company.id', '=', 'user_catalog_finance.id_user_manager')
                  ->where('user_catalog_finance.id', $id_finance);

          $data_company = $data->get();
          $company = $companys->first();
            return view('catalog.users.finance.index', compact('data_company','company'))->with('id', $id);
        }else{
            return redirect('auth');
        }
    }
    public function cart(Request $request) {
      $company = $this->companies();
      $id_finance = DB::table('users')
            ->select('user_catalog_finance.id AS id_supervisi')
            ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
            ->where('users.id', Auth::user()->id)
            ->value('id');
      $data =DB::table('cart')
            ->select('user_catalog.username_catalog AS pembeli','user_catalog.telephone', 'user_catalog.email','cart.*','item.code','item.name','item.merk','item.price_country','item.price_retail','item.price_gov','item.price_date','item.release_date','item.expired_date','item.stock','item.production_origin','item.description','item.status', 'image_item.file')
            ->join('item', 'cart.id_item', '=', 'item.id')
            ->join('image_item', 'item.id','=','image_item.id_item')
            ->join('users', 'cart.id_user', '=', 'users.id')
            ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
            ->where('cart.id_user_finance', $id_finance)
            ->groupBy('image_item.id_item');

      $dataCart = $data->get();

      return view('catalog.users.finance.cart', compact('dataCart','company'));
    }

    public function bast_waiting(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_finance = DB::table('users')
                  ->select('user_catalog_finance.id AS id_finance')
                  ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $id_supervisi = DB::table('user_catalog_finance')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'user_catalog_finance.id_user_manager', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_finance.id', $id_finance)
                  ->pluck('user_catalog_supervisi.id_supervisi')
                  ->toArray();


      $Dataorder = DB::table('order_catalog')
                  ->select('bast.id AS id_bast', 'bast.status AS status_bast', 'order_proposer.no_prop','order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_directur.name AS name_manager', 'user_catalog_directur.email AS email_manager', 'user_catalog_directur.mobile_phone AS mobile_phone_manager')
                  ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                  ->join('user_catalog_directur', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_directur.id')
                  ->join('bast', 'order_catalog.id', '=', 'bast.id_order')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->where(function($q) {
                       $q->where('order_catalog.status', 6)
                         ->orWhere('order_catalog.status', 5);
                   })

                  ->where('bast.status', '1')
                  ->orderBy('order_catalog.datetime', 'DESC');

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->paginate(10);

      return view('catalog.users.finance.bast', compact('order','company'));
    }

    public function payment_waiting(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_finance = DB::table('users')
                  ->select('user_catalog_finance.id AS id_finance')
                  ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $id_supervisi = DB::table('user_catalog_finance')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'user_catalog_finance.id_user_manager', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_finance.id', $id_finance)
                  ->pluck('user_catalog_supervisi.id_supervisi')
                  ->toArray();

      $Dataorder = DB::table('order_catalog')
                  ->select('order_proposer.no_prop','order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_manager.name AS name_manager', 'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager')
                  ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                  ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->where('order_catalog.status', '<>', '0')
                  ->orderBy('order_catalog.datetime', 'DESC');

      $bastIDS = Bast::where('status','2')->orderBy('id_order', 'DESC')->pluck('id_order', 'id')->toArray();
      // dd($bastIDS);
      $bastStatus = Bast::where('status','2')->pluck('status', 'id_order')->toArray();
      $bastID = Bast::where('status','2')->pluck('id_order')->toArray();
      $bastDtl = BastDetail::all();

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->paginate(10);

      return view('catalog.users.finance.payment', compact('order','company', 'bastStatus', 'bastID', 'bastDtl', 'bastIDS'));
    }

    public function bastPreview(Request $request, $id) {
      $dataBast = Bast::where(DB::raw('md5(id)'), $id)->first();
      $id_order = $dataBast->id_order;
      $id_receive = $dataBast->id_receive_item;
      $dataReceive = ReceiveItem::where('id', $id_receive)->first();
      $id_do = $dataReceive->id_do;
      $receive_item = $dataReceive;

      $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where('receive_item.id_order', $id_order)->first();
      $DataPo = Order::where('id', $id_order)->first();

      $directur = DB::table('order_catalog_detail')
                  ->select('order_catalog_detail.*', 'user_catalog_directur.name')
                  ->join('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                  ->where('id_order', $id_order)
                  ->first();

      $delivery = DeliveryOrder::where('id', $id_do)->first();
      $order = Order::where('id', $id_order)->first();

      $destination = "Rumah Sakit Jantung Harapan Kita";
      $telp = "(021) 5684093";
      $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";

      $VendorDetail = VendorDetail::where('id', $DataPo->id_vendor_detail)->first();
      $contract = Contract::where('id_vendor_detail', $VendorDetail->id)->first();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      $bulanWord = Fungsi::MonthIndonesia();
      $bulanRoman = Fungsi::MonthRoman();

      $deliveryDtl = 	DB::table('receive_item_detail')
                      ->select('receive_item_reject.qty AS qty_reject', 'receive_item.id', 'receive_item.id_do', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'receive_item_detail.description', 'item.price_gov')
                      ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
                      ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
                      ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                      ->join('bast', 'receive_item.id', '=', 'bast.id_receive_item')
                      ->join('order_catalog_detail', function($join){
                        $join->on('order_catalog_detail.id_order', '=', 'receive_item.id_order');
                        $join->on('order_catalog_detail.id_order', '=', 'bast.id_order');
                        $join->on('item.id', '=', 'order_catalog_detail.id_item');
                      })
                      ->leftJoin('receive_item_reject', function($join){
                        $join->on('receive_item.id_do', '=', 'receive_item_reject.id_do');
                        $join->on('receive_item_detail.id_item', '=', 'receive_item_reject.id_item');
                      })
                      ->where('bast.id',  $dataBast->id)
                      ->where('receive_item.id_order',  $id_order)
                      ->where('receive_item.id_do', $id_do)
                      ->where('receive_item.id', $dataReceive->id)
                      ->get();

      $driver = $delivery->driver;
      $car = $delivery->car;
      $car_no = $delivery->car_no;
      $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();
      $bast = Bast::where('id_order', $id_order)->where('id_receive_item', $id_receive)->first();

      return view('catalog.users.finance.bast_preview',compact('directur', 'pphp', 'contract', 'bulanRoman', 'bast', 'order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
    }

    public function detail_order(Request $request) {
      $id_order = $request->id_order;
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->where('order_catalog_detail.id_order', $id_order);
      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer)->first();
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

    public function detail_bast(Request $request) {
      $id_bast = $request->id_bast;
      $id_order = $request->id_order;
      $dataOrder = DB::table('bast_detail')
            ->select('bast.id_order', 'unit_type.unit_name', 'bast_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'bast_detail.id_item', '=', 'item.id')
            ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
            ->join('bast', 'bast.id', '=', 'bast_detail.id_bast')
            ->where('bast_detail.id_bast', $id_bast);

      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer)->first();
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }



    public function approvement(Request $request) {
      $order_id = $request->id_order;
      $id_executor = Auth::user()->id;
      $id_finance = DB::table('users')
                  ->select('user_catalog_finance.id AS id_supervisi')
                  ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                  ->where('users.id', $id_executor)
                  ->value('id');

      $approveStatus = Order::where('id', $order_id)->update(['status' => '4']);
      $approveStatus = OrderDetail::where('id_order', $order_id)->update(['id_user_finance_approve' => $id_finance]);

      return redirect()->back()->with('message', 'Pesanan Telah Di approve silahkan cek di history');
    }

    public function payment_approved(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_finance = DB::table('users')
                  ->select('user_catalog_finance.id AS id_finance')
                  ->join('user_catalog_finance', 'users.id', '=', 'user_catalog_finance.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $id_supervisi = DB::table('user_catalog_finance')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'user_catalog_finance.id_user_manager', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_finance.id', $id_finance)
                  ->pluck('user_catalog_supervisi.id_supervisi')
                  ->toArray();
      $Dataorder = DB::table('order_catalog')
                  ->select('order_proposer.no_prop','order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_manager.name AS name_manager', 'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager')
                  ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                  ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->where('order_catalog.status', '6')
                  ->orderBy('order_catalog.datetime', 'DESC');

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->paginate(10);

      return view('catalog.users.finance.approvedorder', compact('order','company'));
    }

    public function track(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_finance.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->join('user_catalog_finance', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_finance.id_user_manager')
                  ->where('user_catalog_finance.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'order_proposer.no_prop', 'order_proposer.proposer')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.datetime','DESC')
                  ->orderBy('order_catalog.no_order', 'DESC');

                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }
                  $order = $Dataorder->paginate(10);
          return view('catalog.users.finance.listorder', compact('order', 'company'));
    }

    public function track_order(Request $request) {
      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_finance.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->join('user_catalog_finance', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_finance.id_user_manager')
                  ->where('user_catalog_finance.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $order = DB::table('order_catalog')
                  ->select('receive_item.id_user', 'users.name','receive_item.datetime AS date_receive', 'delivery_order.datetime AS date_do', 'delivery_order.driver', 'delivery_order.car_no', 'delivery_order.car','order_catalog.id_user_buyer AS super', 'user_catalog_supervisi.id_user AS users','order_catalog.status AS status_pesan',
                  'order_catalog_detail.*',
                            'order_proposer.id AS id_proposer','order_proposer.no_prop', 'order_proposer.proposer',
                            'user_catalog.telephone AS mobile_user','user_catalog.email as email_user',
                            'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email',
                            'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager',
                            'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager',
                            'user_catalog_directur.name AS name_directur',
                            'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email AS email_finance',
                            'user_catalog_finance.mobile_phone AS mobile_phone_finance')
                  ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                  ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
                  ->leftJoin('user_catalog_supervisi', function($join){
                    $join->orOn('order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user');
                    $join->orOn('order_catalog_detail.id_user_supervisi_approve', '=', 'user_catalog_supervisi.id');
                    $join->orOn('users.id', '=', 'user_catalog_supervisi.id_user');
                  })
                  ->leftJoin('user_catalog_manager', 'order_catalog_detail.id_user_manager_approve', '=', 'user_catalog_manager.id')
                  ->leftJoin('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                  ->leftJoin('user_catalog_finance', 'order_catalog_detail.id_user_finance_approve', '=', 'user_catalog_finance.id')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
                  ->leftJoin('receive_item', function($join){
                    $join->on('order_catalog.id', '=', 'receive_item.id_order');
                    $join->orOn('users.id', '=', 'receive_item.id_user');
                    })
                  ->where('order_catalog.id', $request->id_order)
                  ->whereIn('order_catalog.id_user_supervisi', $id_supervisi)
                  ->get();

        $gudang = DB::table('receive_item')->select('receive_item.id_order','item.merk', 'item.code', 'item.name', 'users.id', 'users.username','receive_item.datetime', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim')
                  ->join('master_data_gudang', function($join){
                    $join->on('receive_item.id_gudang', '=', 'master_data_gudang.id_gudang');
                  })
                  ->join('receive_item_detail', function($join){
                    $join->on('receive_item.id', '=', 'receive_item_detail.id_receive_item');
                  })
                  ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                  ->join('item', function($join){
                    $join->on('item.id', '=', 'receive_item_detail.id_item');
                    $join->on('receive_item_detail.id_item', '=', 'master_data_gudang.id_item');
                    $join->on('master_data_gudang.id_item', '=', 'item.id');
                  })
                  ->join('users', 'master_data_gudang.id_user', '=', 'users.id')
                  ->where('receive_item.id_order', $request->id_order)
                  ->get();


                  $users = DB::table('receive_item')->select('users.name')->join('users', 'receive_item.id_user', '=', 'users.id')->first();
                  return response()->json(array(
                                           'users' => $users,
                                           'gudang' => $gudang,
                                           'id_order' => $request->id_order,
                                           'data' => $order,
                                           'status' => 1));
    }

    public function history(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_finance.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->join('user_catalog_finance', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_finance.id_user_manager')
                  ->where('user_catalog_finance.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.id','DESC')
                  ->orderBy('order_catalog.datetime', 'DESC');

                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                $order = $Dataorder->paginate(10);
      return view('catalog.users.finance.history', compact('order','company'));
    }

    public function detail_history(Request $request) {
      $id_order = $request->id_order;
      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_finance.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->join('user_catalog_finance', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_finance.id_user_manager')
                  ->where('user_catalog_finance.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $dataOrder = DB::table('order_catalog_history')
                  ->select('order_catalog_history.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov', 'user_catalog.username_catalog', 'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_manager.name AS name_manager','user_catalog_directur.name AS name_directur')
                  ->join('item', 'order_catalog_history.id_item', '=', 'item.id')
                  ->join('users', 'order_catalog_history.id_executor', '=', 'users.id')
                  ->leftJoin('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->leftJoin('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->leftJoin('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->leftJoin('user_catalog_directur', 'users.id', '=', 'user_catalog_directur.id_user')
                  ->where('order_catalog_history.id_order', $id_order)
                  ->orderBy('order_catalog_history.datetime_history','DESC');

                  $dataItem = Order::where('id', $id_order)->first();
                  $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
                  $user_catalog = $user_catalog_data->first();
                  $count = count($user_catalog_data->get());
                  if($count == 0){
                    $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
                  }
                  $data = $dataOrder->get();
                  return response()->json(array(
                                           'id_order' => $id_order,
                                           'data' => $data,
                                           'user_catalog' => $user_catalog,
                                           'dataItem' => $dataItem,
                                           'status' => 1));
    }

    public function createInvoice($id){
      $data = Order::where(DB::raw('md5(id)'), $id)->first();
      $dataItem = DB::table('order_catalog_detail')->select('order_catalog_detail.*', 'item.name', 'item.code', 'item.merk', 'item.price_gov', 'item.price_shipment')
                  ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
                  ->where('order_catalog_detail.id_order', $data->id)
                  ->get();
      $vendor = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      return view('catalog.users.doc.invoice', compact('dataItem', 'vendor', 'list_propinsi' ,'list_kota', 'data'));
    }

    public function downloadProposer($id_proposer){

      $data = OrderProposer::where(DB::raw('md5(id)'), $id_proposer)->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(order_proposer.id)'), $id_proposer)
            ->get();
      $pdf = PDF::loadView('catalog.users.doc.usulan', compact('data','dataOrder'));
      // return $pdf->download('usulan.pdf');
      return $pdf->stream();
    }

    public function downloadPo($no_po, $bln, $thn){
      $data = Order::where(DB::raw('md5(no_po)'), $no_po)
              ->where(DB::raw('md5(EXTRACT(MONTH FROM order_catalog.datetime))'), $bln)
              ->where(DB::raw('md5(EXTRACT(YEAR FROM order_catalog.datetime))'), $thn)
              ->first();


      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $directur = DB::table('user_catalog_directur')
                  ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
                  ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
                  ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                  ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
                  ->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(order_catalog.no_po)'), $no_po)
            ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $bln)
            ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $thn)
            ->get();
            $list_propinsi = Fungsi::propinsi();
            $list_kota = Fungsi::IndonesiaProvince();

            $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
            ############cara dapetin nomor sesui dengan kategori utama
            $menentukanKate = OrderDetail::where('id_order', $data->id)->pluck('id_item')->toArray();
            $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
            $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
            $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
            $mainCat = Category::whereIn('id', $subCategory)->value('name');

      return view('catalog.users.doc.po', compact('mainCat', 'contract','data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
    }

    function reset(){

      return view('catalog.users.finance.v_reset');

    }

    function action_ganti_password(Request $request){



        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","
            Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

      //   die;
      //
      //
      //
      //
      //    echo $lama1 = $request->lama1;
      //    echo "<br>";
      //
      //    if (Hash::needsRehash($lama1)) {
      //       $lama1 = Hash::make('123456');
      //       echo "sama";
      //   }else{
      //       echo "beda";
      //   }
      //  die;
      //
      //    // echo $lama = bcrypt($request->lama);
      //     echo $password = Hash::make($request->lama);
      //
      //    $baru = $request->baru;
      //
      //  die;
      //   if($lama==$baru){
      //       DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
      //       return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
      //   }else{
      //       return redirect()->route('ganti_password')->with('message','Password Anda Salah');
      //   }
      //
      //
      //
      }

      public function bastChecked(Request $request) {
        DB::beginTransaction();
        try {
          $bast = Bast::where('id', $request->id_bast)->update(['status' => '2']);

        } catch (\Exception $e) {
          DB::rollback();
          return redirect('catalog/users/finance/bast_waiting')->with('message','Bast gagal diperiksa');

        }
        DB::commit();
        return redirect('catalog/users/finance/bast_waiting')->with('message','BAST telah sukses diperiksa');
      }


      // public function bastPreview(Request $request, $id, $id_do) {
      //   $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where(DB::raw('md5(receive_item.id_order)'), $id)->first();
      //   $DataPo = Order::where(DB::raw('md5(id)'), $id)->first();
      //
      //   $directur = DB::table('order_catalog_detail')
      //               ->select('order_catalog_detail.*', 'user_catalog_directur.name')
      //               ->join('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
      //               ->where('id_order', $DataPo->id)
      //               ->first();
      //
      //   $delivery = DeliveryOrder::where(DB::raw('md5(id_order)'), $id)->first();
      //
      //   $receive_item = ReceiveItem::where('id_order', $delivery->id_order)->where('id_do', $delivery->id)->first();
      //   $order = Order::where(DB::raw('md5(id)'), $id)->first();
      //
      //   $destination = "Rumah Sakit Jantung Harapan Kita";
      //   $telp = "(021) 5684093";
      //   $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";
      //
      //   $VendorDetail = VendorDetail::where('id', $DataPo->id_vendor_detail)->first();
      //   $contract = Contract::where('id_vendor_detail', $VendorDetail->id)->first();
      //   $list_propinsi = Fungsi::propinsi();
      //   $list_kota = Fungsi::IndonesiaProvince();
      //   $bulanWord = Fungsi::MonthIndonesia();
      //   $bulanRoman = Fungsi::MonthRoman();
      //
      //   $deliveryDtl = 	DB::table('receive_item_detail')
      //                   ->select('receive_item_reject.qty AS qty_reject', 'receive_item.id', 'receive_item.id_do', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'receive_item_detail.description', 'item.price_gov')
      //                   ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
      //                   ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
      //                   ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
      //                   ->join('order_catalog_detail', function($join){
      //                     $join->on('order_catalog_detail.id_order', '=', 'receive_item.id_order');
      //                     $join->on('item.id', '=', 'order_catalog_detail.id_item');
      //                   })
      //                   ->leftJoin('receive_item_reject', function($join){
      //                     $join->on('receive_item.id_do', '=', 'receive_item_reject.id_do');
      //                     $join->on('receive_item_detail.id_item', '=', 'receive_item_reject.id_item');
      //                   })
      //                   ->where('receive_item.id_order',  $delivery->id_order)
      //                   ->where('receive_item.id_do', $delivery->id)
      //                   ->get();
      //
      //   $driver = $delivery->driver;
      //   $car = $delivery->car;
      //   $car_no = $delivery->car_no;
      //   // dd($receive_item);
      //   $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();
      //   $bast = Bast::where('status', 1)->where(DB::raw('md5(bast.id_order)'), $id)->where('id_receive_item', $receive_item->id)->first();
      //   return view('catalog.pphp.bast.preview',compact('directur', 'pphp', 'contract', 'bulanRoman', 'bast', 'order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
      // }

}
