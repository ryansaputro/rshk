<?php
namespace App\Http\Controllers\Catalog\pphp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\DeliveryOrder;
use App\Model\VendorDetail;
use App\Model\ReceiveItem;
use App\Model\ReceiveItemDetail;
use App\Model\ItemReject;
use App\Model\UnitType;
use App\Model\Bast;
use App\Model\BastDetail;
use App\Model\Contract;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\CategoryWarehouseType;
use App\Model\Pphp;
use App\Model\MasterWarehouseType;
use App\Model\MasterWarehouse;
use App\Model\MasterDataWarehouse;
use App\Model\UnitConversion;
use App\Model\Item;
use App\Model\Bapb;
use App\Model\BapbDetail;
use App\Model\VendorDirectur;
use Fungsi;
use File;
use Illuminate\Support\Facades\Hash;

class PphpController extends Controller
{

    public function __construct()
    {
        $this->middleware('pphpauth');
    }

    public function dashboard() {
      return view('catalog.pphp.dashboard.index',compact('data'));
    }

    public function index(Request $request){
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $pphp = DB::table('master_gudang')
                ->select('master_gudang.*', 'category_warehouse_type.*')
                ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                ->where('id_user', Auth::user()->id)
                ->first();

      $dataQuery = DB::table('order_catalog')->select('order_catalog.id AS id_order','order_catalog.no_non_medik', 'order_catalog.no_medik', 'order_catalog.id_category', 'category.name AS name_category', 'bapb.id AS id_bapb','delivery_order.id_order', 'delivery_order.no_do', 'delivery_order.datetime AS timeDO', 'order_catalog.no_po', 'order_catalog.datetime AS timePO', 'vendor_detail.id AS id_vendor_detail', 'vendor_detail.vendor_name', 'vendor_detail.code', 'receive_item.*')
              ->join('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
              ->join('receive_item', function($join){
                $join->on('order_catalog.id', '=', 'receive_item.id_order');
                $join->on('delivery_order.id', '=', 'receive_item.id_do');
              })
              ->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')
              ->join('category', 'order_catalog.id_category', '=', 'category.id')
              ->leftJoin('bapb', function($join){
                $join->on('order_catalog.id', '=', 'bapb.id_order');
                $join->on('receive_item.id', '=', 'bapb.id_receive_item');
              })
              //->where('delivery_order.id_gudang', $pphp->id_gudang)
              ->orderBy('receive_item.datetime', 'DESC');

              if($filter == 'po'){
                $pencarian = explode('/',$search);
                if(count($pencarian) > 1){
                  $dataQuery->where('order_catalog.no_po', $pencarian[0])
                  ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                  ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                }else{
                  $dataQuery->where('order_catalog.no_po', $pencarian[0]);
                }
              }else if($filter == 'spj'){
                $pencarian = explode('/',$search);
                if(count($pencarian) > 1){
                  $dataQuery->where('delivery_order.no_do', $pencarian[0])
                  ->where(DB::raw('month(delivery_order.datetime)'), $pencarian[3])
                  ->where(DB::raw('year(delivery_order.datetime)'), $pencarian[4]);
                }else{
                  $dataQuery->where('delivery_order.no_do', $pencarian[0]);
                }

              }else if($filter == 'tanggal'){
                $dataQuery->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);
              }

        $data = $dataQuery->get();
        return view('catalog.pphp.receive_item.index',compact('data'));
    }

    public function create(Request $request){
      $pphp = DB::table('master_gudang')
                ->select('master_gudang.*', 'category_warehouse_type.*')
                ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                ->where('id_user', Auth::user()->id)
                ->first();

      $po = DB::table('delivery_order')
                ->select('category.name AS name_category' ,'order_catalog.*')
                // ->select('order_catalog.*')
                ->join('order_catalog', 'delivery_order.id_order', '=', 'order_catalog.id')
                ->join('category', 'order_catalog.id_category', '=', 'category.id')
                ->where('delivery_order.id_gudang', $pphp->id_gudang)
                ->where(function($q) {
                   $q->where('order_catalog.status', 5)
                     ->orWhere('order_catalog.status', 4);
                })
                ->where('delivery_order.status', 1)
                ->distinct()
                ->get();

      $data = DB::table('order_catalog_detail')
                ->select('order_catalog.datetime', 'order_catalog.no_po', 'order_catalog_detail.id', 'order_catalog_detail.id_order','order_catalog_detail.id_item','item.name')
                ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
                ->join('order_catalog', 'order_catalog.id', '=', 'order_catalog_detail.id_order');

      $dataItem = $data->get();
      $unit = UnitType::where('status', '1')->orderBy('unit_name', 'ASC')->get();
        return view('catalog.pphp.receive_item.receive',compact('unit', 'dataItem', 'VendorDetail', 'po', 'do'));
    }

    public function detail(Request $request){
      $id_po = $request->id_po;
      $do = DeliveryOrder::where('id_order', $id_po)->where('status', 1)->get();
      $id_order = md5($id_po);
      $vendor = DB::table('order_catalog')->select('vendor_detail.vendor_name', 'vendor_detail.address', 'vendor_detail.telephone')->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')->where('order_catalog.id', $id_po)->first();
        return response()->json(array(
                            'vendor' => $vendor,
                            'do' => $do,
                            'id_order' => $id_order,
                            'status' => 1));
    }

    public function detailItem(Request $request){
      $cekTotReceive = DB::table('receive_item')
      ->select('receive_item_detail.qty')
      ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
      ->where('receive_item.id_do', $request->id)
      ->where('receive_item_detail.id_item', $request->id_item)
      ->sum('receive_item_detail.id_qty');

			$data = DB::table('delivery_order')
								->select('delivery_order_detail.qty')
								->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
								->where('delivery_order_detail.id_item', $request->id_item)
								->where('delivery_order_detail.id_do', $request->id)
								->value('qty');
      $dataDO = (($data)-($cekTotReceive));

      $satuan = Item::where('id', $request->id_item)->first();
			return response()->json(array(
															 'qty' => $dataDO,
															 'item' => $request->id_item,
                               'satuan' => $satuan->satuan,
															 'status' => 1));
		}

    public function spjItem(Request $request){
      $do = DeliveryOrder::where('id', $request->id_spj)->first();
      $data = DB::table('order_catalog')
              ->select('unit_type.convert_pcs', 'unit_type.id AS id_satuan', 'unit_type.unit_name', 'order_catalog_detail.*','item.merk', 'item.code', 'item.name', 'item.price_gov', 'item.id AS id_item')
              ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
              ->join('item', 'item.id', '=', 'order_catalog_detail.id_item')
              ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
              ->where('order_catalog.id', $do->id_order)
              ->distinct()
              ->get();


      $dataSatuanUnit = DB::table('delivery_order_detail')
            ->select('delivery_order_detail.qty', 'delivery_order_detail.id_item','unit_type.id AS id_satuan', 'unit_type.unit_name', 'delivery_order_detail.description')
            ->join('delivery_order', 'delivery_order_detail.id_do', '=', 'delivery_order.id')
            ->join('unit_type', 'delivery_order_detail.satuan', '=', 'unit_type.id')
            ->where('delivery_order_detail.id_do', $request->id_spj);

      $dataSend = $dataSatuanUnit->pluck('qty', 'id_item')->toArray();
      $dataSatuan = $dataSatuanUnit->pluck('id_satuan', 'id_item')->toArray();
      $dataSatuanName = $dataSatuanUnit->pluck('unit_name', 'id_item')->toArray();
      $allSatuan = UnitType::where('status', 1)->get();
      $dataDesc = $dataSatuanUnit->pluck('receive_item_detail.description', 'id_item')->toArray();


      $id_spj = $request->id_spj;
      $orderDetail = DB::table('delivery_order_detail')->select('delivery_order_detail.*', 'item.name')->join('item', 'delivery_order_detail.id_item', '=', 'item.id')->join('delivery_order', 'delivery_order.id', '=', 'delivery_order_detail.id_do')->where('delivery_order_detail.id_do', $id_spj)->orderBy('id', 'DESC')->get();

      return response()->json(array(
                          'orderDetail' => $orderDetail,
                          'dataSend' => $dataSend,
                          'dataSatuan' => $dataSatuan,
                          'dataSatuanName' => $dataSatuanName,
                          'allSatuan' => $allSatuan,
                          'dataDesc' => $dataDesc,
                          'data' => $data,
                          'status' => 1));
    }

    public function receiveSave(Request $request){
      $update = "";
      $insert = "";
      $cekTotReceive = 0;

      $pphp = DB::table('master_gudang')
            ->select('master_gudang.*', 'category_warehouse_type.id_gudang', 'user_catalog_pphp.id AS id_pphp')
            ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
            ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
            ->where('user_catalog_pphp.id_user', Auth::user()->id)
            ->first();

      $userWarehouse = DB::table('users')->select('users.id')
            ->join('role_user', 'users.id', 'role_user.user_id')
            ->where('role_user.role_id', 9)->where('id_gudang', $pphp->id)->first();

      $order = DB::table('order_catalog')
            ->select('order_catalog_detail.qty')
            ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
            ->where('order_catalog.id', $request->no_po)
            // ->whereIn('order_catalog_detail.id_item', $request->idDO)
            // ->groupby('order_catalog_detail.id_item')
            ->sum('order_catalog_detail.qty');


      $orderData = DB::table('order_catalog')
            ->select('unit_type.convert_pcs', 'order_catalog_detail.qty', 'item.satuan', 'order_catalog_detail.id_item')
            ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
            ->join('item', 'item.id', '=', 'order_catalog_detail.id_item')
            ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
            ->where('order_catalog.id', $request->no_po);

      #(SP)di sinkronisasi satuan sesuai dengan sp agar kita bisa cek barang yg dikirim sesuai dgn sp atau tidaknya
      $itemSatuan = $orderData->pluck('satuan','id_item')->toArray();
      $itemQty = $orderData->pluck('qty', 'id_item')->toArray();
      $itemConvert = $orderData->pluck('convert_pcs', 'id_item')->toArray();

      $do = DB::table('delivery_order')
            ->select('delivery_order_detail.id_item', 'delivery_order_detail.qty', 'delivery_order_detail.satuan')
            ->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
            ->where('delivery_order.id', $request->spj)
            ->whereIn('delivery_order_detail.id_item', $request->idDO)
            ->groupBy('delivery_order_detail.id_item', 'delivery_order_detail.qty', 'delivery_order_detail.satuan')
            ->pluck('delivery_order_detail.qty','delivery_order_detail.id_item')
            ->toArray();

      $receiveAll = DB::table('receive_item')
            ->select('receive_item.id_order', 'unit_type.convert_pcs','receive_item_detail.*')
            ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item.id_do')
            ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
            ->whereIn('receive_item_detail.id_item', $request->idDO)
            ->distinct();

      #(Receive Item)di sinkronisasi satuan sesuai dengan sp agar kita bisa cek barang yg dikirim sesuai dgn sp atau tidaknya
      $itemSatuanReceives = $receiveAll->pluck('id_item','id')->toArray();
      $itemSatuanReceive = $receiveAll->pluck('satuan','id')->toArray();
      $itemQtyReceive = $receiveAll->pluck('id_qty', 'id')->toArray();
      $itemConvertReceive = $receiveAll->pluck('convert_pcs', 'id')->toArray();


      $totReject = 0;
      $allJumlahReceive = 0;

      $satuan = $request->satuanDO;
      $qty = $request->qtyDO;

      foreach($request->idDO as $k => $v){
        foreach ($itemSatuanReceives as $key => $value) {
          if ($value == $v) {

            if($satuan[$k] != $itemSatuanReceive[$key]){
              if ($itemSatuanReceive[$key] == '2') {
                $sisaItemReceive = $itemQtyReceive[$key]/$itemConvert[$v];
              }else{
                $sisaItemReceive = $itemQtyReceive[$key]/$itemConvert[$v];
              }
            }else{
              if($itemSatuan[$v] == $itemSatuanReceive[$key]){
                $sisaItemReceive = $itemQtyReceive[$key];
              }else{
                $sisaItemReceive = $itemQtyReceive[$key]/$itemConvert[$v];
              }
            }

            $allJumlahReceive += $sisaItemReceive;

          }

        }

          ###logika di SP
          if($satuan[$k] != $itemSatuan[$v]){
            $sisaItemNew = $qty[$k]/$itemConvert[$v];

          }else{
            $sisaItemNew = $qty[$k];
          }

          #jumlah yg diterima dan yg sedang Diterima
          $jumlahSisa = $allJumlahReceive+$sisaItemNew;

          $cekTotReceive += $jumlahSisa;

      }

      $BapbData = Bapb::whereYear("datetime",  "=", date('Y'))->get();
      $bapb = count($BapbData);


      if($bapb == 0){
        $no_bapb = 1;
        $no_bapb_with_o = "00".$no_bapb;
      }else{
        $no_bapb = $bapb+1;
        if($no_bapb <=9 ){
          $no_bapb_with_o = "00".$no_bapb;
        }else if($no_bapb <=99 ){
          $no_bapb_with_o = "0".$no_bapb;
        }else{
          $no_bapb_with_o = $no_bapb;
        }
      }

      $rcvData = ReceiveItem::whereYear("datetime",  "=", date('Y'))->get();
      $rcv = count($rcvData);


      if($rcv == 0){
        $no_rcv = 1;
        $no_rcv_with_o = "00".$no_rcv;
      }else{
        $no_rcv = $rcv+1;
        if($no_rcv <=9 ){
          $no_rcv_with_o = "00".$no_rcv;
        }else if($no_rcv <=99 ){
          $no_rcv_with_o = "0".$no_rcv;
        }else{
          $no_rcv_with_o = $no_rcv;
        }
      }


      DB::beginTransaction();

      try {

        $receive = ReceiveItem::create([
          'id_order' => $request->no_po,
          'id_do' => $request->spj,
          'no_receive_item' => $no_rcv_with_o,
          'datetime' => date('Y-m-d H:i:s'),
          'id_user' => Auth::user()->id,
          'status' => 1,
          'id_gudang' => $pphp->id_gudang,
        ]);

        foreach($request->idDO as $k => $v){

          $totReject = $do[$v]-$request->qtyDO[$k];
          $receiveDtl = ReceiveItemDetail::create([
            'id_receive_item' => $receive->id,
            'id_item' => $v,
            'id_qty' => $request->qtyDO[$k],
            'satuan' => $request->satuanDO[$k],
            'status' => 1,
            'description' => ($request->keteranganDO[$k] != null) ? $request->keteranganDO[$k] : '-'
          ]);

          if(array_key_exists($v, $do)){
            if($request->qtyDO[$k] == $do[$v]){
              $updatedSPJ = DeliveryOrder::where('id', $request->spj)->update(['status' => 2]);
            }else{
              $rejectItem = ItemReject::create([
                'id_order' => $request->no_po,
                'id_do' => $request->spj,
                'id_item' => $v,
                'qty' => $totReject,
                'description' => ($request->keteranganDO[$k] != null) ? $request->keteranganDO[$k] : '-',
                'id_user' => Auth::user()->id,
                'status' => 1,
                'datetime' => date("Y-m-d H:i:s")
              ]);

              $updateStatusOrder = Order::where('id', $request->no_po)->update(['status' => 7]);
            }
          }

        }


        $cekTotReceive = DB::table('receive_item')
            ->select('receive_item_detail.qty')
            ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
            ->where('receive_item.id_order', $request->no_po)
            ->sum('receive_item_detail.id_qty');

        $do = DB::table('delivery_order')
            ->select('delivery_order_detail.*')
            ->join('delivery_order_detail', 'delivery_order.id', '=', 'delivery_order_detail.id_do')
            ->where('delivery_order.id', $request->spj)
            ->sum('delivery_order_detail.qty');


        if($order == $cekTotReceive) {
          $status = Order::where('id', $request->no_po)->update(['status' => 6]);
        }else{
          $reject = ItemReject::where('id_order', $request->no_po)->count();
          if($reject > 0){
            $status = Order::where('id', $request->no_po)->update(['status' => 7]);
          }
        }

        if($request->button == 'bast'){

          $bapb = Bapb::create([
            'id_order' => $request->no_po,
            'id_receive_item' => $receive->id,
            'no_bapb' => $no_bapb_with_o,
            'datetime' => date("Y-m-d H:i:s"),
            'status' => '1',
            'id_pphp' =>  $pphp->id_pphp
          ]);

          foreach($request->idDO as $k => $v){
            $receiveDtl = BapbDetail::create([
              'id_bapb' => $bapb->id,
              'id_item' => $v,
              'qty' => $request->qtyDO[$k],
              'satuan' => $request->satuanDO[$k],
              'status' => 1,
              'description' => ($request->keteranganDO[$k] != null) ? $request->keteranganDO[$k] : '-'
            ]);
          }

        }


        $updatedSPJ = DeliveryOrder::where('id', $request->spj)->update(['status' => 2]);
        session()->flash('message', 'BAPB berhasil dibuat');


      } catch (\Illuminate\Database\QueryException $e) {
        DB::rollback();

        dd($e->getMessage());
        session()->flash('message', 'BAPB gagal dibuat');
      }

      DB::commit();

      return redirect()->intended('catalog/pphp/index');
    }

    // public function trackItem(Request $request, $id, $id_do) {
    //   $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where(DB::raw('md5(receive_item.id_order)'), $id)->first();
    //   $DataPo = Order::where(DB::raw('md5(id)'), $id)->first();
    //   $receive_item = ReceiveItem::where(DB::raw('md5(id_order)'), $id)->first();
    //   $order = Order::where(DB::raw('md5(id)'), $id)->first();
    //   $delivery = DeliveryOrder::where(DB::raw('md5(id)'), $id_do)->first();
    //   $destination = "Rumah Sakit Jantung Harapan Kita";
    //   $telp = "(021) 5684093";
    //   $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";
    //   $VendorDetail = VendorDetail::where('id', $DataPo->id_vendor_detail)->first();
    //   $list_propinsi = Fungsi::propinsi();
    //   $list_kota = Fungsi::IndonesiaProvince();
    //   $bulanWord = Fungsi::MonthIndonesia();
    //
    //   $deliveryDtl = 	DB::table('delivery_order_detail')
    //                   ->select('delivery_order.id_order', 'unit_type.unit_name', 'delivery_order_detail.*', 'item.merk', 'item.code', 'item.name')
    //                   ->join('item', 'delivery_order_detail.id_item', '=', 'item.id')
    //                   ->join('unit_type', 'delivery_order_detail.satuan', '=', 'unit_type.id')
    //                   ->join('delivery_order', 'delivery_order_detail.id_do', '=', 'delivery_order.id')
    //                   ->join('receive_item', 'receive_item.id_do', '=', 'delivery_order.id')
    //                   ->where('receive_item.id', $receive_item->id)
    //                   ->where('delivery_order.id_order', $DataPo->id)
    //                   ->get();
    //
    //   $delivery = DeliveryOrder::where(DB::raw('md5(id_order)'), $id)->first();
    //   $driver = $delivery->driver;
    //   $car = $delivery->car;
    //   $car_no = $delivery->car_no;
    //
    //   return view('catalog.pphp.receive_item.preview',compact('order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
		// }

    public function bapbPreview(Request $request, $id, $id_receive) {
      $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where(DB::raw('md5(cast(receive_item.id_order AS varchar(50)))'), $id)->first();
      // $delivery = DeliveryOrder::where(DB::raw('md5(id_order)'), $id)->where(DB::raw('md5(id)'), $id_do)->first();
      $receive_item = ReceiveItem::where(DB::raw('md5(cast(id_order AS varchar(50)))'), $id)->where(DB::raw('md5(cast(id AS varchar(50)))'), $id_receive)->first();
      $delivery = DeliveryOrder::where('id', $receive_item->id_do)->first();
      $order = Order::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();

      $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $order->id_user_buyer)->first();

      $directur = DB::table('order_catalog_detail')
        ->select('order_catalog_detail.*', 'user_catalog_directur.name')
        ->join('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
        ->where('id_order', $order->id)
        ->first();

      $destination = "Rumah Sakit Jantung Harapan Kita";
      $telp = "(021) 5684093";
      $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";

      $VendorDetail = VendorDetail::where('id', $order->id_vendor_detail)->first();
      $contract = Contract::where('id_vendor_detail', $VendorDetail->id)->first();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      $bulanWord = Fungsi::MonthIndonesia();
      $bulanRoman = Fungsi::MonthRoman();

      $deliveryDtl = 	DB::table('receive_item_detail')
                      ->select('receive_item_reject.qty AS qty_reject', 'receive_item.id', 'receive_item.id_do', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'receive_item_detail.description', 'item.price_gov')
                      ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
                      ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
                      ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                      ->join('order_catalog_detail', function($join){
                        $join->on('order_catalog_detail.id_order', '=', 'receive_item.id_order');
                        $join->on('item.id', '=', 'order_catalog_detail.id_item');
                      })
                      ->leftJoin('receive_item_reject', function($join){
                        $join->on('receive_item.id_do', '=', 'receive_item_reject.id_do');
                        $join->on('receive_item_detail.id_item', '=', 'receive_item_reject.id_item');
                      })
                      ->where('receive_item.id_order',  $delivery->id_order)
                      ->where('receive_item.id_do', $delivery->id)
                      ->get();

      $item = DB::table('receive_item_detail')->select('receive_item_detail.*')
                      ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
                      ->where('receive_item.id_order',  $delivery->id_order)
                      ->where('receive_item.id_do', $delivery->id)
                      ->pluck('id_item')
                      ->toArray();

      $categoryItem = CategoryItem::whereIn('id_item', $item)->first();
      $mainCategorySubSub = Category::where('id', $categoryItem->id_category)->first();
      $mainCategorySub = Category::where('id', $mainCategorySubSub->id_parent)->first();
      $mainCategory = Category::where('id', $mainCategorySub->id_parent)->first();

      $driver = $delivery->driver;
      $car = $delivery->car;
      $car_no = $delivery->car_no;
      $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();
      $bast = Bapb::where('id_order', $order->id)->where('id_receive_item', $receive_item->id)->first();

      return view('catalog.pphp.bapb.preview',compact('mainCategory', 'userCode', 'directur', 'pphp', 'contract', 'bulanRoman', 'bast', 'order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
    }


    public function createBastFromReceiveModul(Request $request, $id, $id_receive) {
      //id order (PO yang sama)
      //ambil semua barang dgn po yg sama dr table order_catalog_detail
      //ambil semua barang dgn po yg sama dr table receive_item_detail
      // error_reporting (0);
      $id = $id == 'new' ? '0' : $id;
      $order = Order::where(DB::raw('md5(id)'), $id)->first();
      if($id !== '0'){
        $VendorDetail = VendorDetail::where('id', $order->id_vendor_detail)->first();
      }

      $po = DB::table('delivery_order')
              ->select('delivery_order.*', 'vendor_detail.code')
              ->join('receive_item', 'delivery_order.id', '=', 'receive_item.id_do')
              ->join('order_catalog',  function($join){
                  $join->on('order_catalog.id', '=','delivery_order.id_order');
                  $join->on('order_catalog.id', '=', 'receive_item.id_order');
                })
              ->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')
              ->join('master_gudang', 'receive_item.id_gudang', '=', 'master_gudang.id')
              ->join('master_jenis_gudang', function($join){
                  $join->on('master_gudang.id_jenis_gudang', '=', 'master_jenis_gudang.id');
                  $join->on('order_catalog.id_golongan', '=', 'master_jenis_gudang.id');
                })

              ->where('receive_item.id_user', Auth::user()->id)
              ->orderBy('delivery_order.datetime', 'DESC')
              ->get();

      $dataTerima = DB::table('receive_item')
              ->select('receive_item_detail.id_item','receive_item.id AS id_receive_item','unit_type.id AS id_satuan','item.id', 'receive_item_detail.description', 'receive_item_detail.id_qty AS qty_kirim', 'receive_item.id_order','unit_type.unit_name','item.merk', 'item.code', 'item.name', 'item.price_gov')
              ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
              ->join('unit_type', 'unit_type.id', '=', 'receive_item_detail.satuan')
              ->join('item', function($join){
                $join->on('item.id', '=', 'receive_item_detail.id_item');
              });

        if($id !== '0'){
          $dataTerima->where(DB::raw('md5(receive_item.id)'), $id_receive);
          $dataTerima->where(DB::raw('md5(receive_item.id_order)'), $id);
        }
        $data = $dataTerima->pluck('qty_kirim', 'id_item')->toArray();
        $IdDataReceive = $dataTerima->value('receive_item.id');

        if($id !== '0'){
        $dataLain = DB::table('receive_item')
                ->select('receive_item_detail.id_item','receive_item.id AS id_receive_item','unit_type.id AS id_satuan','item.id', 'receive_item_detail.description', 'receive_item_detail.id_qty AS qty_kirim', 'receive_item.id_order','unit_type.unit_name','item.merk', 'item.code', 'item.name', 'item.price_gov')
                ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
                //->join('unit_type', 'unit_type.id', '=', 'receive_item_detail.satuan')
                ->join('item', function($join){
                  $join->on('item.id', '=', 'receive_item_detail.id_item');
                })
                ->where(DB::raw('md5(receive_item.id_order)'), $id)
                ->where(DB::raw('md5(receive_item.id)'), $id_receive)
                ->pluck('id_satuan', 'id_item')->toArray();
                  $idReceive = ReceiveItem::where(DB::raw('md5(receive_item.id)'), $id_receive)->value('id_do');
                }

        $ReceiveItem = DB::table('receive_item')
              ->select('receive_item_detail.description', 'receive_item_detail.id_item','receive_item.id AS id_receive_item','unit_type.id AS id_satuan','item.id', 'receive_item_detail.description', 'receive_item_detail.id_qty AS qty_kirim', 'receive_item.id_order','unit_type.unit_name','item.merk', 'item.code', 'item.name', 'item.price_gov')
              ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
              ->join('unit_type', 'unit_type.id', '=', 'receive_item_detail.satuan')
              ->join('item', function($join){
                $join->on('item.id', '=', 'receive_item_detail.id_item');
              });

        if($id !== '0'){
          $ReceiveItem->where('receive_item.id_order', $order->id);
          $ReceiveItemSatuanAll = $ReceiveItem->pluck('unit_type.unit_name','id_item')->toArray();
          $ReceiveItemDescAll = $ReceiveItem->pluck('receive_item_detail.description','id_item')->toArray();
          $ReceiveItem->where(DB::raw('md5(receive_item.id)'), $id_receive);
          $ReceiveItemSatuan = $ReceiveItem->pluck('unit_type.unit_name','id_item')->toArray();
          $ReceiveItemDesc = $ReceiveItem->pluck('receive_item_detail.description','id_item')->toArray();
        }

        $satuan = UnitType::where('status', 1)->pluck('unit_name','id')->toArray();

        $dataPO = DB::table('order_catalog')
                ->select('order_catalog_detail.*','item.merk', 'item.code', 'item.name', 'item.price_gov', 'item.id AS id_item')
                ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                ->join('item', 'item.id', '=', 'order_catalog_detail.id_item')
                ->where(DB::raw('md5(order_catalog.id)'), $id)
                ->distinct()
                ->get();

        $bastSelected = DB::table('bast')
              ->select('receive_item.*')
              ->join('receive_item', 'bast.id_receive_item', '=', 'receive_item.id')
              ->join('delivery_order', 'delivery_order.id', '=', 'receive_item.id_do')
              ->pluck('delivery_order.id')
              ->toArray();

      return view('catalog.pphp.bast.create', compact('ReceiveItemSatuanAll', 'ReceiveItemDescAll', 'ReceiveItemSatuan', 'ReceiveItemDesc', 'bastSelected', 'IdDataReceive', 'ReceiveItemDesc', 'ReceiveItemSatuan', 'idReceive', 'satuan', 'dataLain', 'VendorDetail', 'order', 'po', 'data', 'dataPO'));
    }

    // public function bastIndex(Request $request) {
    //   $pphp = Pphp::where('id_user', Auth::user()->id)
    //             ->first();
    //   $data = DB::table('order_catalog')
    //           ->select('bast.*', 'order_catalog.datetime AS date_order', 'order_catalog.no_po', 'receive_item.id_do')
    //           ->join('bast', 'order_catalog.id', '=', 'bast.id_order')
    //           ->join('receive_item', 'receive_item.id', '=', 'bast.id_receive_item')
    //           ->where('bast.id_pphp', $pphp->id)
    //           ->orderBy('bast.datetime', 'DESC')
    //           ->get();
    //   return view('catalog.pphp.bast.index', compact('data'));
    // }
    //
    // public function BastReceiveDetail(Request $request) {
    //   $id_do = $request->id_do;
    //   $dataDo = DeliveryOrder::where('id', $id_do)->where('status', 2);
    //   $dataDofirst = $dataDo->first();
    //   $do = $dataDo->get();
    //
    //   $data = DB::table('order_catalog')
    //           ->select('order_catalog_detail.*','item.merk', 'item.code', 'item.name', 'item.price_gov', 'item.id AS id_item')
    //           ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
    //           ->join('item', 'item.id', '=', 'order_catalog_detail.id_item')
    //           ->where('order_catalog.id', $dataDofirst->id_order)
    //           ->distinct()
    //           ->get();
    //
    //   $dataReceiveAll = DB::table('receive_item')
    //           ->select('receive_item_reject.qty AS qty_reject', 'receive_item_detail.id_item','receive_item.id AS id_receive_item','unit_type.id AS id_satuan','item.id', 'receive_item_detail.description', 'receive_item_detail.id_qty AS qty_kirim', 'receive_item.id_order','unit_type.unit_name','item.merk', 'item.code', 'item.name', 'item.price_gov')
    //           ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
    //           ->join('unit_type', 'unit_type.id', '=', 'receive_item_detail.satuan')
    //           ->join('item', function($join){
    //             $join->on('item.id', '=', 'receive_item_detail.id_item');
    //           })
    //           ->leftJoin('receive_item_reject', function($join){
    //             $join->on('receive_item.id_do', '=', 'receive_item_reject.id_do');
    //             $join->on('receive_item_detail.id_item', '=', 'receive_item_reject.id_item');
    //           })
    //           ->where('receive_item.id_do', $id_do);
    //
    //   $dataReceive = $dataReceiveAll->pluck('qty_kirim', 'id_item')->toArray();
    //   $dataReject = $dataReceiveAll->pluck('qty_reject', 'id_item')->toArray();
    //
    //   $dataSatuanUnit = DB::table('receive_item_detail')
    //         ->select('receive_item_detail.id_item','unit_type.id AS id_satuan', 'unit_type.unit_name', 'receive_item_detail.description')
    //         ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
    //         ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
    //         ->where('receive_item.id_do', $id_do);
    //
    //   $dataSatuan = $dataSatuanUnit->pluck('id_satuan', 'id_item')->toArray();
    //   $dataSatuanName = $dataSatuanUnit->pluck('unit_name', 'id_item')->toArray();
    //   $allSatuan = UnitType::where('status', 1)->get();
    //   $dataDesc = $dataSatuanUnit->pluck('receive_item_detail.description', 'id_item')->toArray();
    //
    //   $ReceiveItem = DB::table('receive_item')
    //         ->select('receive_item_detail.description', 'receive_item_detail.id_item','receive_item.id AS id_receive_item','unit_type.id AS id_satuan','item.id', 'receive_item_detail.description', 'receive_item_detail.id_qty AS qty_kirim', 'receive_item.id_order','unit_type.unit_name','item.merk', 'item.code', 'item.name', 'item.price_gov')
    //         ->join('receive_item_detail', 'receive_item.id', '=', 'receive_item_detail.id_receive_item')
    //         ->join('unit_type', 'unit_type.id', '=', 'receive_item_detail.satuan')
    //         ->join('item', function($join){
    //           $join->on('item.id', '=', 'receive_item_detail.id_item');
    //         });
    //
    //   $ReceiveItemSatuanAll = $ReceiveItem->pluck('unit_type.unit_name','id_item')->toArray();
    //   $ReceiveItemDescAll = $ReceiveItem->pluck('receive_item_detail.description','id_item')->toArray();
    //
    //   $vendor = DB::table('order_catalog')->select('vendor_detail.vendor_name', 'vendor_detail.address', 'vendor_detail.telephone')->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')->where('order_catalog.id', $dataDofirst->id_order)->first();
    //
    //   return response()->json(array(
    //                       'vendor' => $vendor,
    //                       'do' => $do,
    //                       'data' => $data,
    //                       'dataReceive' => $dataReceive,
    //                       'dataSatuan' => $dataSatuan,
    //                       'dataSatuanName' => $dataSatuanName,
    //                       'dataDesc' => $dataDesc,
    //                       'dataReject' => $dataReject,
    //                       'ReceiveItemSatuanAll' => $ReceiveItemSatuanAll,
    //                       'allSatuan' => $allSatuan,
    //                       'ReceiveItemDescAll' => $ReceiveItemDescAll,
    //                       'status' => 1));
    // }
    //
    // public function bastSave(Request $request) {
    //   $categoryItem = CategoryItem::whereIn('id_item', $request->receive_id_item)->first();
    //   $mainCategory = Category::where('id', $categoryItem->id_category)->first();
    //
    //   $pphp = DB::table('master_gudang')
    //             ->select('master_gudang.*', 'category_warehouse_type.*', 'user_catalog_pphp.id AS id_pphp')
    //             ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
    //             ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
    //             ->where('id_user', Auth::user()->id)
    //             ->first();
    //
    //   $id_recive = DB::table('delivery_order')->select('receive_item.id')->join('receive_item', 'delivery_order.id', '=', 'receive_item.id_do')->where('delivery_order.id', $request->id_do)->value('id');
    //
    //   $bast = Bast::create([
    //     'id_order' => $request->no_po,
    //     'id_receive_item' => $id_recive,
    //     'datetime' => date("Y-m-d H:i:s"),
    //     'status' => '1',
    //     'id_pphp' => $pphp->id_pphp
    //   ]);
    //
    //   foreach($request->receive_id_item as $k => $v){
    //     $receiveDtl = BastDetail::create([
    //       'id_bast' => $bast->id,
    //       'id_item' => $v,
    //       'qty' => $request->qty_receive_item[$k],
    //       'satuan' => $request->receive_satuan[$k],
    //       'status' => 1,
    //       'description' => ($request->receive_keterangan[$k] != null) ? $request->receive_keterangan[$k] : '-',
    //     ]);
    //   }
    //   session()->flash('message','BAST berhasil dibuat');
    //   return redirect()->intended('catalog/pphp/bast/index');
    // }


    public function warehouseIndex(Request $request) {
      $pphp = DB::table('master_gudang')
                ->select('master_gudang.*', 'category_warehouse_type.*', 'user_catalog_pphp.id AS id_pphp')
                ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                ->where('id_user', Auth::user()->id)
                ->first();

      $data = DB::table('receive_item_detail')
                ->select('vendor_detail.code as code_vendor', 'receive_item.no_receive_item','receive_item.datetime', 'unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'receive_item_detail.id_item', 'receive_item_detail.id_qty', 'receive_item_detail.status', 'receive_item_detail.satuan', 'master_gudang.nama_instalasi')
                ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
                ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
                ->join('master_gudang', 'receive_item.id_gudang', '=', 'master_gudang.id')
                ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                ->join('order_catalog', 'receive_item.id_order', '=', 'order_catalog.id')
                ->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')
                ->where('receive_item.id_gudang', $pphp->id_gudang)
                ->orderBy('receive_item.datetime', 'DESC')
                ->paginate(10);

      return view('catalog.pphp.to_warehouse.index', compact('data'));
    }

    public function uploadSpj(Request $request, $id){
			$validatedData = $request->validate([
				'data_spj' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);
      DB::beginTransaction();

      try {
        $a = ReceiveItem::where(DB::raw('md5(id)'), $id)->first();
        $vendor = Order::where('id', $a->id_order)->first();

        $fSpj = $request->file('data_spj');
        $tgl = date("Ymd");
        $destinationPath = 'assets/document/'.$vendor->id_vendor_detail.'/spj/'.$tgl.'/';
        if(!File::exists($destinationPath)) File::makeDirectory($destinationPath, 0775, true);
        $fileSpj = "SPJ".date('Ymds').".".$fSpj->getClientOriginalExtension();
        $id_spj = DB::table('receive_item')->where('id', $a->id)->update(['data_spj' => $fileSpj, 'uploaded_at' => date('Y-m-d')]);
        $data = $fSpj->move($destinationPath, $fileSpj);
        session()->flash('message','Upload SPJ basah berhasil');

      } catch (\Exception $e) {
        DB::rollback();
        session()->flash('message','Upload SPJ basah gagal');

      }

      DB::commit();

      return redirect()->intended('catalog/pphp/index');
		}

      function reset(){
        return view('catalog.pphp.layouts.v_reset');
      }

      function action_ganti_password(Request $request){



          if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
              // The passwords matches
              return redirect()->back()->with("error","
              Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
          }

          if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
              //Current password and new password are same
              return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
          }

          $validatedData = $request->validate([
              'current_password' => 'required',
              'new_password' => 'required|string|min:6|confirmed',
          ]);

          //Change Password
          $user = Auth::user();
          $user->password = bcrypt($request->get('new_password'));
          $user->save();

          return redirect()->back()->with("success","Password changed successfully !");

          die;




           echo $lama1 = $request->lama1;
           echo "<br>";

           if (Hash::needsRehash($lama1)) {
              $lama1 = Hash::make('123456');
              echo "sama";
          }else{
              echo "beda";
          }
         die;

           // echo $lama = bcrypt($request->lama);
            echo $password = Hash::make($request->lama);

           $baru = $request->baru;

         die;
          if($lama==$baru){
              DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
              return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
          }else{
              return redirect()->route('ganti_password')->with('message','Password Anda Salah');
          }



        }


        public function downloadPo($id){
          $data = Order::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();
          $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
          $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
          $directur = DB::table('user_catalog_directur')
                ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
                ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
                ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                ->where('user_catalog_supervisi.id', $data[0]->id_user_supervisi)
                ->first();

          $dataOrder = DB::table('order_catalog_detail')
                ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
                ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
                ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
                ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                ->where(DB::raw('md5(cast(order_catalog.id AS varchar(50)))'), $id)

                ->get();

          $list_propinsi = Fungsi::propinsi();
          $list_kota = Fungsi::IndonesiaProvince();
          $contract = Contract::where('id_vendor_detail', $data[0]->id_vendor_detail)->first();
          ############cara dapetin nomor sesui dengan kategori utama
          $menentukanKate = OrderDetail::where('id_order', $data[0]->id)->pluck('id_item')->toArray();
          $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
          $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
          $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
          $mainCat = Category::whereIn('id', $subCategory)->value('name');

          return view('catalog.users.doc.po', compact('mainCat', 'contract','data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
        }

}
