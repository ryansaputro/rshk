<?php
namespace App\Http\Controllers\Catalog\manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Supervisi;
use App\Model\Manager;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\Budget;
use App\Model\BudgetUsed;
use App\Model\OrderProposer;
use App\Model\VendorDetail;
use App\Model\VendorDirectur;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;

class ManagerCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('managercatalogauth');
    }

    public function companies(){
      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_manager.id', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'cart.id_user_manager', '=', 'user_catalog_manager.id')
              ->where('cart.id_user_manager', $id_manager);
      $companys = DB::table('user_catalog_company')
              ->select('user_catalog_company.*', 'user_catalog_manager.id AS id_manager', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
              ->where('user_catalog_manager.id', $id_manager);

      $data_company = $data->get();
      $company = $companys->first();
      return $company;
    }

    public function index(Request $request, $id){

        if (Auth::check()) {
            $company = $this->companies();
            return view('catalog.users.manager.index', compact('data_company','company'))->with('id', $id);
        }else{
            return redirect('auth');
        }
    }

    public function cart(Request $request) {
      $company = $this->companies();
      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $data =DB::table('cart')
                  ->select('user_catalog.username_catalog AS pembeli','user_catalog.telephone', 'user_catalog.email','cart.*','item.code','item.name','item.merk','item.price_country','item.price_retail','item.price_gov','item.price_date','item.release_date','item.expired_date','item.stock','item.production_origin','item.description','item.status', 'image_item.file')
                  ->join('item', 'cart.id_item', '=', 'item.id')
                  ->join('image_item', 'item.id','=','image_item.id_item')
                  ->join('users', 'cart.id_user', '=', 'users.id')
                  ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->where('cart.id_user_manager', $id_manager)
                  ->groupBy('image_item.id_item');

      $dataCart = $data->get();


      return view('catalog.users.manager.cart', compact('dataCart','company'));
    }

    public function order_waiting(Request $request) {
      $company = $this->companies();

      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $id_supervisi = Supervisi::where('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();
      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  // ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                  // ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->where('order_catalog.status','1')
                  ->orderBy('order_catalog.datetime', 'DESC');
                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->paginate(10);

                  $budget = Budget::where('status', '1')->whereIn('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
                  $budgetJml = $budget->get();
                  $budgetTotal = $budget->pluck('id')->toArray();

                  if(count($budgetJml) == 0){
                    $budgetUsed = 0;
                  }else{
                    $budgetUsed =  BudgetUsed::whereIn('id_budget', $budgetTotal)->sum('budget_used');
                  }
                  if($budgetUsed != null){
                    $budgetYear = ($budget->sum('budget'))-$budgetUsed;
                  }else{
                    $budgetYear = 0;
                  }

      return view('catalog.users.manager.order', compact('order','company','budgetYear'));
    }

    public function detail_order(Request $request) {
      $id_order = $request->id_order;
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->where('order_catalog_detail.id_order', $id_order);
      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

    public function approvement(Request $request) {
      $order_id = $request->id_order;
      $id_item = $request->id_item;
      $id_user = $request->id_user;
      $status = $request->status;
      $subtot = $request->subtot;
      $description = $request->description;
      $qty = $request->qty;
      $datetime = date("Y-m-d H:i:s");
      $id_executor = Auth::user()->id;
      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', $id_executor)
                  ->value('id');
      $detail = OrderDetail::where('id_order', $order_id);
      $cekOrder = $detail->get();
      $id_supervisi_change_qty = $detail->value('id_user_supervisi_approve');
      $supervisi = DB::table('users')
      ->select('users.id')
      ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
      ->where('user_catalog_supervisi.id', $id_supervisi_change_qty)
      ->value('id');
      $id = "";
      $update = "";
      $insert = "";
      $idReject = [];
      foreach($cekOrder AS $k => $v){
        if($v->qty != $qty[$v->id_item]){
          #INSERT KE TABLE HISTORY
          if($id) $id .= ",";
          if($v->qty < $qty[$v->id_item]){
            $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','ADD','".$datetime."')";
          }else{
            $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','REDUCE','".$datetime."')";
          }
          #UPDATE KE TABLE ORDER CATALOG DETAIL
          if($update) $update .= ";";
            if($status[$v->id_item] == 1){
              $update .= "UPDATE order_catalog_detail SET qty =".$qty[$v->id_item].",id_user_manager_approve='".$id_manager."', datetime_manager='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
            }
        }else{
              if($update) $update .= ";";
              $update .= "UPDATE order_catalog_detail SET id_user_manager_approve='".$id_manager."', datetime_manager='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
        }

        #status item yg di reject Insert ke history
        if($status[$v->id_item] == 0){
          if($insert) $insert.=",";
          $insert .= "('".$order_id."','".$v->id_item."','".$v->qty."','".$id_user."','".$id_executor."','1','".$datetime."','REJECT','".$description[$v->id_item]."','".$datetime."')";
        }

      }

      foreach($status as $k => $v){
        if($v == 0){
          $idReject[] = $k;
        }
      }

      $approveStatus = Order::where('id', $order_id)->update(['status' => '2', 'total' => $subtot]);
      $history = NEW OrderHistory;
      $detailOrder = NEW OrderDetail;
      if($id != ""){
        $insertUpdateOldQty = $history->OldQty($id);
      }
      if($update != ""){
      $updateDetailOrder = $detailOrder->UpdateOrder($update);
      }
      if($insert != ""){
      $reject = $history->RejectHistory($insert);
      }
      if(count($idReject) > 0){
        $deleteOrder = OrderDetail::where('id_order', $order_id)->whereIn('id_item',$idReject)->delete();
      }
      return redirect()->back()->with('message', 'Pesanan telah disetujui');
    }

    public function order_approved(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $id_supervisi = Supervisi::where('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();
      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->where('order_catalog.status','2');

                  if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                  $order = $Dataorder->paginate(10);
      return view('catalog.users.manager.approvedorder', compact('order','company'));
    }

    public function track(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_manager = Manager::where('id_user', Auth::user()->id)->value('id');
      $id_supervisi = Supervisi::where('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();
      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'order_proposer.no_prop', 'order_proposer.proposer')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.datetime','DESC')
                  ->orderBy('order_catalog.no_order', 'DESC');

                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }
      $order = $Dataorder->paginate(10);

      return view('catalog.users.manager.listorder', compact('order','company'));
    }

    public function track_order(Request $request) {
      $id_manager = Manager::where('id_user', Auth::user()->id)->value('id');
      $super = Supervisi::where('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();
      $order = DB::table('order_catalog')
                  ->select('receive_item.id_user', 'users.name','receive_item.datetime AS date_receive', 'delivery_order.datetime AS date_do', 'delivery_order.driver', 'delivery_order.car_no', 'delivery_order.car','order_catalog.id_user_buyer AS super', 'user_catalog_supervisi.id_user AS users','order_catalog.status AS status_pesan',
                  'order_catalog_detail.*',
                            'order_proposer.id AS id_proposer','order_proposer.no_prop', 'order_proposer.proposer',
                            'user_catalog.telephone AS mobile_user','user_catalog.email as email_user',
                            'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email',
                            'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager',
                            'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager',
                            'user_catalog_directur.name AS name_directur',
                            'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email AS email_finance',
                            'user_catalog_finance.mobile_phone AS mobile_phone_finance')
                  ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                  ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
                  ->leftJoin('user_catalog_supervisi', function($join){
                    $join->orOn('order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user');
                    $join->orOn('order_catalog_detail.id_user_supervisi_approve', '=', 'user_catalog_supervisi.id');
                    $join->orOn('users.id', '=', 'user_catalog_supervisi.id_user');
                  })
                  ->leftJoin('user_catalog_manager', 'order_catalog_detail.id_user_manager_approve', '=', 'user_catalog_manager.id')
                  ->leftJoin('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                  ->leftJoin('user_catalog_finance', 'order_catalog_detail.id_user_finance_approve', '=', 'user_catalog_finance.id')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
                  ->leftJoin('receive_item', function($join){
                    $join->on('order_catalog.id', '=', 'receive_item.id_order');
                    $join->orOn('users.id', '=', 'receive_item.id_user');
                    })
                  ->where('order_catalog.id', $request->id_order)
                  ->whereIn('order_catalog.id_user_supervisi', $super)
                  ->get();
                  $users = DB::table('receive_item')->select('users.name')->join('users', 'receive_item.id_user', '=', 'users.id')->first();
                  return response()->json(array(
                                           'users' => $users,
                                           'id_order' => $request->id_order,
                                           'data' => $order,
                                           'status' => 1));
    }

    public function history(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';
      $id_manager = DB::table('users')
                  ->select('user_catalog_manager.id AS id_manager')
                  ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');
      $id_supervisi = Supervisi::where('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();
      $Dataorder = DB::table('order_catalog')
                  ->select('order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)->orderBy('order_catalog.id','DESC')
                  ->orderBy('order_catalog.datetime', 'DESC');
                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                      ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                $order = $Dataorder->paginate(10);
          return view('catalog.users.manager.history', compact('order','company'));
    }

    public function detail_history(Request $request) {
      $id_order = $request->id_order;
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $dataOrder = DB::table('order_catalog_history')
                  ->select('order_catalog_history.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov', 'user_catalog.username_catalog', 'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_manager.name AS name_manager','user_catalog_directur.name AS name_directur')
                  ->join('item', 'order_catalog_history.id_item', '=', 'item.id')
                  ->join('users', 'order_catalog_history.id_executor', '=', 'users.id')
                  ->leftJoin('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->leftJoin('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->leftJoin('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  ->leftJoin('user_catalog_directur', 'users.id', '=', 'user_catalog_directur.id_user')
                  ->where('order_catalog_history.id_order', $id_order)
                  ->orderBy('order_catalog_history.datetime_history','DESC');

      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

    public function budget() {
      $AllSupervisi = DB::table('user_catalog_manager')
                      ->select('user_catalog_supervisi.id')
                      ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                      ->where('user_catalog_manager.id_user', Auth::user()->id)
                      ->pluck('user_catalog_supervisi.id')
                      ->toArray();

      $budget = Budget::where('status', '1')->whereIn('budget.id_supervisi', $AllSupervisi)->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->pluck('id')->toArray();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::whereIn('id_budget', $budgetTotal)->sum('budget_used');
      }

      $budget = DB::table('budget')
              ->select('budget.*', 'user_catalog_manager.name', 'user_catalog_supervisi.name AS name_supervisi')
              ->join('user_catalog_manager', 'budget.approved_by', '=', 'user_catalog_manager.id')
              ->join('user_catalog_supervisi', 'budget.id_supervisi', '=', 'user_catalog_supervisi.id')
              ->where('budget.status', '1')
              ->whereIn('budget.id_supervisi', $AllSupervisi)
              ->get();

      $budgetYear = $budgetTotal;
      return view('catalog.users.manager.budget', compact('budget', 'budgetYear', 'budgetUsed'));
    }

    public function budgetDetail(Request $request) {
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $AllSupervisi = DB::table('user_catalog_manager')
                      ->select('user_catalog_supervisi.id')
                      ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                      ->where('user_catalog_manager.id_user', Auth::user()->id)
                      ->pluck('user_catalog_supervisi.id')
                      ->toArray();

      $Dataorder = DB::table('budget_used')
                      ->select('budget_used.budget_used','budget_used.datetime','order_catalog.datetime AS date_order', 'order_proposer.datetime AS date_prop','order_catalog.no_po', 'order_proposer.no_prop', 'user_catalog.username_catalog','order_catalog.status', 'order_proposer.no_prop', 'order_proposer.proposer',  'order_proposer.id AS id_proposer', 'user_catalog_supervisi.name')
                      ->join('budget', 'budget_used.id_budget', '=', 'budget.id')
                      ->join('order_proposer', 'budget_used.id_proposer', '=', 'order_proposer.id')
                      ->join('order_catalog', 'budget_used.id_order', '=', 'order_catalog.id')
                      ->leftJoin('user_catalog', 'budget_used.used_by', '=', 'user_catalog.id_user')
                      ->leftJoin('user_catalog_supervisi', 'budget_used.used_by', '=', 'user_catalog_supervisi.id_user')
                      ->whereIn('budget.id_supervisi', $AllSupervisi)
                      ->orderBy('budget_used.datetime', 'DESC');

                      if($filter == 'po'){
                        $pencarian = explode('/',$search);
                        if(count($pencarian) > 1){
                          $Dataorder->where('order_catalog.no_po', $pencarian[0])
                          ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                          ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                        }else{
                          $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                        }
                      }else if($filter == 'usulan'){
                        $pencarian = explode('/',$search);
                        if(count($pencarian) > 1){
                          $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                          ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                          ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                        }else{
                          $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                        }

                      }else if($filter == 'tanggal'){
                        $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                      }
                      $DataDetail = $Dataorder->paginate(10);

      $budget = Budget::where('status', '1')->whereIn('budget.id_supervisi', $AllSupervisi)->orderBy('periode', 'DESC');
      $budgetJml = $budget->get();
      $budgetTotal = $budget->pluck('id')->toArray();
      if(count($budgetJml) == 0){
        $budgetUsed = 0;
      }else{
        $budgetUsed =  BudgetUsed::whereIn('id_budget', $budgetTotal)->sum('budget_used');
      }
      $budget = DB::table('budget')
              ->select('budget.*', 'user_catalog_manager.name')
              ->join('user_catalog_manager', 'budget.approved_by', '=', 'user_catalog_manager.id')
              ->where('budget.status', '1')
              ->where('budget.id_supervisi', $AllSupervisi)
              ->get();
      $budgetYear = Budget::where('status', '1')->orderBy('periode', 'DESC')->first();
      return view('catalog.users.manager.budget_detail', compact('DataDetail', 'budget', 'budgetYear', 'budgetUsed'));
    }

    public function downloadProposer($id_proposer){
      $data = OrderProposer::where(DB::raw('md5(id)'), $id_proposer)->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(order_proposer.id)'), $id_proposer)
            ->get();
      $pdf = PDF::loadView('catalog.users.doc.usulan', compact('data','dataOrder'));
      // return $pdf->download('usulan.pdf');
      return $pdf->stream();
    }

    public function downloadPo($no_po, $bln, $thn){
      $data = Order::where(DB::raw('md5(no_po)'), $no_po)
              ->where(DB::raw('md5(month(order_catalog.datetime))'), $bln)
              ->where(DB::raw('md5(year(order_catalog.datetime))'), $thn)
              ->first();
      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorDirectur::where('id', $data->id_vendor_detail)->first();
      $directur = DB::table('user_catalog_directur')
                  ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
                  ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
                  ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
                  ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
                  ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
                  ->first();
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(order_catalog.no_po)'), $no_po)
            ->where(DB::raw('md5(month(order_catalog.datetime))'), $bln)
            ->where(DB::raw('md5(year(order_catalog.datetime))'), $thn)
            ->get();

      // $pdf = PDF::loadView('catalog.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur'));
      // return $pdf->download('usulan.pdf');
      // return $pdf->stream();
      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      return view('catalog.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
    }

}
