<?php
namespace App\Http\Controllers\Catalog\directur;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Supervisi;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\Budget;
use App\Model\BudgetUsed;
use App\Model\OrderProposer;
use App\Model\VendorDetail;
use App\Model\VendorDirectur;
use App\Model\Contract;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\Bast;
use App\Model\BastDetail;
use App\Model\Bapb;
use App\Model\BapbDetail;
use App\Model\ReceiveItem;
use App\Model\DeliveryOrder;
use App\Model\Adendum;
use App\Model\Bap;
use App\Model\UnitType;
use App\Model\Pphp;
use App\Model\MasterDataWarehouse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;

class DirecturCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('directurcatalogauth');
    }

    public function companies(){
      $id_manager = DB::table('user_catalog_manager')
                      ->select('user_catalog_manager.id')
                      ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                      ->where('user_catalog_directur.id_user', Auth::user()->id)
                      ->pluck('id')
                      ->toArray();
      $data = DB::table('cart')
              ->select('cart.*','user_catalog_manager.id', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'cart.id_user_manager', '=', 'user_catalog_manager.id')
              ->whereIn('cart.id_user_manager', $id_manager);
      $companys = DB::table('user_catalog_company')
              ->select('user_catalog_company.*', 'user_catalog_manager.id AS id_manager', 'user_catalog_manager.name', 'user_catalog_manager.email', 'user_catalog_manager.mobile_phone')
              ->join('user_catalog_manager', 'user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
              ->whereIn('user_catalog_manager.id', $id_manager);

      $data_company = $data->get();
      $company = $companys->first();
      return $company;
    }

    public function index(Request $request, $id){

        if (Auth::check()) {
          $id  = DB::table('user_catalog_directur')
                      ->select('user_catalog_supervisi.*')
                      ->join('user_catalog_supervisi', 'user_catalog_directur.id', '=', 'user_catalog_supervisi.id_user_manager')
                      ->where('user_catalog_directur.id_user', Auth::user()->id)
                      ->first();
           $id_m   =$id->id_user_manager;

           $id_supervisi = Supervisi::where('id_user_manager',$id_m)
                       ->pluck('id')
                       ->toArray();

          // $Dataorder = DB::table('order_catalog')
          //             ->select('user_catalog_supervisi.id_user_manager AS id_ppk', 'order_catalog.*', 'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog.username_catalog','user_catalog_supervisi.name','user_catalog_supervisi.id_user_manager')
          //             // ->select('user_catalog_supervisi.id_user_manager AS id_ppk', 'order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name','user_catalog_supervisi.id_user_manager')
          //             ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
          //             ->leftJoin('user_catalog', function($join){
          //               $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
          //               $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
          //             })
          //             ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
          //             ->whereIn('id_user_supervisi', $id_supervisi)
          //             ->where('order_catalog.status','2')
          //             ->where('user_catalog_supervisi.id_user_manager',$id_m)
          //             ->orderBy('order_catalog.datetime', 'DESC');
                      // ->distinct();

          $Dataorder = DB::table('order_catalog')
                      ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'user_catalog_supervisi.id_user_manager AS id_ppk', 'order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name','user_catalog_supervisi.id_user_manager')
                      ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                      ->join('category', 'order_catalog.id_category', '=', 'category.id')
                      ->leftJoin('user_catalog', function($join){
                        $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                        $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                      })
                      ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                      ->whereIn('id_user_supervisi', $id_supervisi)
                      ->where('order_catalog.status','2')
                      ->where('user_catalog_supervisi.id_user_manager',$id_m)
                      ->orderBy('order_catalog.datetime', 'DESC');

            $order = $Dataorder->count();
            // return view('catalog.users.directur.index', compact('data_company','company'))->with('id', $id);
            return view('catalog.users.directur.index', compact('order'));
        }else{
            return redirect('auth');
        }
    }


    public function order_waiting(Request $request) {


        $id  = DB::table('user_catalog_directur')
                    ->select('user_catalog_supervisi.*')
                    ->join('user_catalog_supervisi', 'user_catalog_directur.id', '=', 'user_catalog_supervisi.id_user_manager')
                    ->where('user_catalog_directur.id_user', Auth::user()->id)
                    ->first();
         $id_m   =$id->id_user_manager;

        $company = $this->companies();

        $filter = ($request->filter != null) ? $request->filter : 'xxx';
        $search = ($request->search != null) ? $request->search : 'xxx';

        // $id_manager = DB::table('user_catalog_manager')
        //             ->select('user_catalog_manager.id')
        //             ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
        //             ->where('user_catalog_directur.id_user', Auth::user()->id)
        //             ->pluck('id')
        //             ->toArray();
        //             dd();
        $id_supervisi = Supervisi::where('id_user_manager',$id_m)
                    ->pluck('id')
                    ->toArray();
        //             dd($id_m);
        // $order = DB::table('order_catalog')
        //             ->select('order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager', 'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager')
        //             ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
        //             ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
        //             ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
        //             ->whereIn('id_user_supervisi', $id_supervisi)
        //             ->where('order_catalog.status','2')
        //             ->get();




        $Dataorder = DB::table('order_catalog')
                    ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'user_catalog_supervisi.id_user_manager AS id_ppk', 'order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name','user_catalog_supervisi.id_user_manager')
                    ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->leftJoin('user_catalog', function($join){
                      $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                      $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                    })
                    ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                    //>leftJoin('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                    // ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                    // ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                    ->whereIn('id_user_supervisi', $id_supervisi)
                    ->where('order_catalog.status','2')
                    ->where('user_catalog_supervisi.id_user_manager',$id_m)
                    ->orderBy('order_catalog.datetime', 'DESC');

                    if($filter == 'po'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_catalog.no_po', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                      }
                    }else if($filter == 'usulan'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                      }

                    }else if($filter == 'tanggal'){
                      $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                    }

                    $order = $Dataorder->paginate(10);

                    $budget = Budget::where('status', '1')->whereIn('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
                    $budgetJml = $budget->get();
                    $budgetTotal = $budget->pluck('id')->toArray();

                    if(count($budgetJml) == 0){
                      $budgetUsed = 0;
                    }else{
                      $budgetUsed =  BudgetUsed::whereIn('id_budget', $budgetTotal)->sum('budget_used');
                    }

                    if($budgetUsed != null){
                      $budgetYear = ($budget->sum('budget'))-$budgetUsed;
                    }else{
                      $budgetYear = $budget->sum('budget');
                    }
        return view('catalog.users.directur.order', compact('order','company', 'budgetYear'));
      }

    public function detail_order(Request $request) {
      $id_order = $request->id_order;
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->where('order_catalog_detail.id_order', $id_order);
      $dataItem = Order::where('id', $id_order)->first();
      $proposer = OrderProposer::where('id', $dataItem->id_proposer)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'proposer' => $proposer,
                               'status' => 1));
    }

    public function detail_bapb(Request $request) {
      $id_order = $request->id_order;
      $id_bapb = $request->id_bapb;
      $dataOrder = DB::table('bapb_detail')
            ->select('unit_type.id AS id_satuan', 'unit_type.unit_name', 'bapb_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('unit_type', 'bapb_detail.satuan', '=', 'unit_type.id')
            ->join('item', 'bapb_detail.id_item', '=', 'item.id')
            ->where('bapb_detail.id_bapb', $id_bapb);
      $dataItem = Order::where('id', $id_order)->first();
      $dataItem = Order::where('id', $id_order)->first();
      $proposer = OrderProposer::where('id', $dataItem->id_proposer)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                                'id_order' => $id_order,
                                'id_bapb' => $id_bapb,
                                'data' => $data,
                                'user_catalog' => $user_catalog,
                                'dataItem' => $dataItem,
                                'proposer' => $proposer,
                                'status' => 1));
    }

    public function approvement(Request $request) {



      // try{
          // Mail::send('catalog.users.email.email_vendor', ['nama' =>'Rshk', 'pesan' => 'Barang Anda Dipesan oleh pengusul dan Telah disetujui oleh PPK . Harap untuk Segera Mengirim Barang Sesuai Permintaan'], function ($message) use ($request)
          // {
          //     $message->subject('tes');
          //     $message->from('donotreply@kiddy.com', 'Kiddy');
          //     $message->to('ajie.darmawan106@gmail.com');
          // });
      //     return back()->with('alert-success','Berhasil Kirim Email');
      //     }
      //     catch (Exception $e){
      //         return response (['status' => false,'errors' => $e->getMessage()]);
      //     }

          //Notifikasi
          // $userkey = "hfvns6"; //userkey lihat di zenziva
          // $passkey = "3q8wehsntc"; // set passkey di zenziva
          // $telepon = "085649184363";//$no_tlpn; // ke unit nya
          // $nama = $unit_intalasi;
          // $message = "Selamat $request->name_buyer1 telah disetujui Oleh PPK";
          // $url = "https://reguler.zenziva.net/apps/smsapi.php";
          // $curlHandle = curl_init();
          // curl_setopt($curlHandle, CURLOPT_URL, $url);
          // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
          // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
          // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
          // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
          // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
          // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
          // curl_setopt($curlHandle, CURLOPT_POST, 1);
          // $results = curl_exec($curlHandle);
          // curl_close($curlHandle);
          //
          // $XMLdata = new SimpleXMLElement($results);
          // $status = $XMLdata->message[0]->text;
          // // echo $status;
          //

          // //Unit nya
          //
          // $userkey = "hfvns6"; //userkey lihat di zenziva
          // $passkey = "3q8wehsntc"; // set passkey di zenziva
          // $telepon = "089698930980";
          // $nama = "tes";
             // $message = "Barang Anda Dipesan oleh pengusul dan Telah disetujui oleh PPK . Harap untuk Segera Mengirim Barang Sesuai Permintaan";
          // $url = "https://reguler.zenziva.net/apps/smsapi.php";
          // $curlHandle = curl_init();
          // curl_setopt($curlHandle, CURLOPT_URL, $url);
          // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
          // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
          // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
          // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
          // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
          // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
          // curl_setopt($curlHandle, CURLOPT_POST, 1);
          // $results = curl_exec($curlHandle);
          // curl_close($curlHandle);
          //
          // $XMLdata = new SimpleXMLElement($results);
          // $status = $XMLdata->message[0]->text;





      DB::beginTransaction();

      try {

        $order_id = $request->id_order;
        $description = $request->desc;
        $id_item = $request->id_item;
        $id_user = $request->id_user;
        $status = $request->status;
        $subtot = $request->subtot;
        $qty = $request->qty;
        $datetime = date("Y-m-d H:i:s");
        $id_executor = Auth::user()->id;
        $id_directur = DB::table('users')
                    ->select('user_catalog_directur.id AS id_directur')
                    ->join('user_catalog_directur', 'users.id', '=', 'user_catalog_directur.id_user')
                    ->where('users.id', $id_executor)
                    ->value('id_directur');
        $detail = OrderDetail::where('id_order', $order_id);
        $cekOrder = $detail->get();
        $id_manager_change_qty = $detail->value('id_user_manager_approve');
        $manager = DB::table('users')
        ->select('users.id')
        ->join('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
        ->where('user_catalog_manager.id', $id_manager_change_qty)
        ->value('id');
        $id = "";
        $update = "";
        $insert = "";
        $idReject = [];
        foreach($cekOrder AS $k => $v){
          if($v->qty != $qty[$v->id_item]){
            #INSERT KE TABLE HISTORY
              if($id) $id .= ",";
              if($v->qty < $qty[$v->id_item]){
                $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','ADD','".$datetime."')";
              }else{
                $id .= "('".$order_id."','".$v->id_item."','".$qty[$v->id_item]."','".$id_user."','".$id_executor."','".$status[$v->id_item]."','".$datetime."','REDUCE','".$datetime."')";
              }
            #UPDATE KE TABLE ORDER CATALOG DETAIL
            if($update) $update .= ";";
              if($status[$v->id_item] == 1){
                $update .= "UPDATE order_catalog_detail SET qty =".$qty[$v->id_item].",id_user_directur_approve='".$id_directur."', datetime_directur='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
              }
          }else{
                if($update) $update .= ";";
                $update .= "UPDATE order_catalog_detail SET qty =".$qty[$v->id_item].",id_user_directur_approve='".$id_directur."', datetime_directur='".$datetime."', status=1 WHERE id_order=".$order_id." and id_item=".$v->id_item;
          }

          #status item yg di reject Insert ke history
          if($status[$v->id_item] == 0){
            if($insert) $insert.=",";
            $insert .= "('".$order_id."','".$v->id_item."','".$v->qty."','".$id_user."','".$id_executor."','1','".$datetime."','REJECT','".$description[$v->id_item]."','".$datetime."')";
          }

        }

        foreach($status as $k => $v){
          if($v == 0){
            $idReject[] = $k;
          }
        }

        $approveStatus = Order::where('id', $order_id)->update(['status' => '3', 'total' => $subtot]);
        $history = NEW OrderHistory;
        $detailOrder = NEW OrderDetail;
        if($id != ""){
          $insertUpdateOldQty = $history->OldQty($id);
        }
        if($update != ""){
        $updateDetailOrder = $detailOrder->UpdateOrder($update);
        }
        if($insert != ""){
        $reject = $history->RejectHistory($insert);
        }
        if(count($idReject) > 0){
          $deleteOrder = OrderDetail::where('id_order', $order_id)->whereIn('id_item',$idReject)->delete();
        }

        Mail::send('catalog.users.email.email_vendor', ['nama' =>'Rshk', 'pesan' => 'Barang Anda Dipesan oleh pengusul dan Telah disetujui oleh PPK . Harap untuk Segera Mengirim Barang Sesuai Permintaan'], function ($message) use ($request)
        {
            $message->subject('tes');
            $message->from('donotreply@kiddy.com', 'Kiddy');
            $message->to('ajie.darmawan106@gmail.com');
        });

      } catch (\Exception $e) {
        DB::rollback();
        return back()->with('alert-danger','Gagal');

      }

      DB::commit();

      return redirect()->back()->with('message', 'Pesanan telah disetujui');
    }

    public function order_approved(Request $request) {
       $company = $this->companies();
       $filter = ($request->filter != null) ? $request->filter : 'xxx';
       $search = ($request->search != null) ? $request->search : 'xxx';

       $id_manager = DB::table('user_catalog_manager')
                   ->select('user_catalog_manager.id')
                   ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                   ->where('user_catalog_directur.id_user', Auth::user()->id)
                   ->pluck('id')
                   ->toArray();

       $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                   ->pluck('id')
                   ->toArray();

       $Dataorder = DB::table('order_catalog')
                   ->select('user_catalog.kode_instalasi', 'category.name AS name_category', 'order_catalog.*', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone')
                   ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   ->leftJoin('user_catalog', function($join){
                     $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                     $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   })
                   ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   ->leftJoin('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                   ->whereIn('id_user_supervisi', $id_supervisi)
                   ->where('order_catalog.status', '3')
                   ->orderBy('order_catalog.datetime', 'DESC');

                   if($filter == 'po'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_catalog.no_po', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                     }
                   }else if($filter == 'usulan'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                     }

                   }else if($filter == 'tanggal'){
                     $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                   }

                   $order = $Dataorder->paginate(10);


       return view('catalog.users.directur.approvedorder', compact('order','company'));
     }

     public function track(Request $request) {
        $company = $this->companies();
        $filter = ($request->filter != null) ? $request->filter : 'xxx';
        $search = ($request->search != null) ? $request->search : 'xxx';
        $id_manager = DB::table('user_catalog_manager')
                    ->select('user_catalog_manager.id')
                    ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                    ->where('user_catalog_directur.id_user', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

        $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                    ->pluck('id')
                    ->toArray();

        $Dataorder = DB::table('order_catalog')
                    ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'order_proposer.no_prop', 'order_proposer.proposer')
                    ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->leftJoin('user_catalog', function($join){
                      $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                      $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                    })
                    ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                    ->whereIn('id_user_supervisi', $id_supervisi)
                    ->orderBy('order_catalog.datetime','DESC')
                    ->orderBy('order_catalog.no_order', 'DESC');

                    if($filter == 'status'){
                      if($search != '5'){
                        $Dataorder->where('order_catalog.status', $search);
                      }
                    }else if($filter == 'po'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_catalog.no_po', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                      }
                    }else if($filter == 'usulan'){
                      $pencarian = explode('/',$search);
                      if(count($pencarian) > 1){
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                        ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                        ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                      }else{
                        $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                      }

                    }else if($filter == 'tanggal'){
                      $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                    }
                    $order = $Dataorder->paginate(10);
          return view('catalog.users.directur.listorder', compact('order','company'));
      }

    public function track_order(Request $request) {
      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_manager.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->where('user_catalog_directur.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $order = DB::table('order_catalog')
                  ->select('receive_item.id_user', 'users.name','receive_item.datetime AS date_receive', 'delivery_order.datetime AS date_do', 'delivery_order.driver', 'delivery_order.car_no', 'delivery_order.car','order_catalog.id_user_buyer AS super', 'user_catalog_supervisi.id_user AS users','order_catalog.status AS status_pesan',
                  'order_catalog_detail.*',
                            'order_proposer.id AS id_proposer','order_proposer.no_prop', 'order_proposer.proposer',
                            'user_catalog.telephone AS mobile_user','user_catalog.email as email_user',
                            'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email',
                            'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager',
                            'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager',
                            'user_catalog_directur.name AS name_directur',
                            'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email AS email_finance',
                            'user_catalog_finance.mobile_phone AS mobile_phone_finance')
                  ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                  ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
                  ->leftJoin('user_catalog_supervisi', function($join){
                    $join->orOn('order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user');
                    $join->orOn('order_catalog_detail.id_user_supervisi_approve', '=', 'user_catalog_supervisi.id');
                    $join->orOn('users.id', '=', 'user_catalog_supervisi.id_user');
                  })
                  ->leftJoin('user_catalog_manager', 'order_catalog_detail.id_user_manager_approve', '=', 'user_catalog_manager.id')
                  ->leftJoin('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                  ->leftJoin('user_catalog_finance', 'order_catalog_detail.id_user_finance_approve', '=', 'user_catalog_finance.id')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
                  ->leftJoin('receive_item', function($join){
                    $join->on('order_catalog.id', '=', 'receive_item.id_order');
                    $join->orOn('users.id', '=', 'receive_item.id_user');
                    })
                  ->where('order_catalog.id', $request->id_order)
                  ->whereIn('order_catalog.id_user_supervisi', $id_supervisi)
                  ->get();

        $gudang = DB::table('receive_item')->select('receive_item.id_order','item.merk', 'item.code', 'item.name', 'users.id', 'users.username','receive_item.datetime', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim')
                  ->join('master_data_gudang', function($join){
                    $join->on('receive_item.id_gudang', '=', 'master_data_gudang.id_gudang');
                  })
                  ->join('receive_item_detail', function($join){
                    $join->on('receive_item.id', '=', 'receive_item_detail.id_receive_item');
                  })
                  ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                  ->join('item', function($join){
                    $join->on('item.id', '=', 'receive_item_detail.id_item');
                    $join->on('receive_item_detail.id_item', '=', 'master_data_gudang.id_item');
                    $join->on('master_data_gudang.id_item', '=', 'item.id');
                  })
                  ->join('users', 'master_data_gudang.id_user', '=', 'users.id')
                  ->where('receive_item.id_order', $request->id_order)
                  ->get();

        $users = DB::table('receive_item')->select('users.name')->join('users', 'receive_item.id_user', '=', 'users.id')->first();

        return response()->json(array(
                                 'users' => $users,
                                 'id_order' => $request->id_order,
                                 'data' => $order,
                                 'gudang' => $gudang,
                                 'status' => 1));
    }

    public function history(Request $request) {
      $company = $this->companies();
      $filter = ($request->filter != null) ? $request->filter : 'xxx';
      $search = ($request->search != null) ? $request->search : 'xxx';

      $id_manager = DB::table('user_catalog_manager')
                  ->select('user_catalog_manager.id')
                  ->join('user_catalog_directur', 'user_catalog_manager.id_user_catalog_company', '=', 'user_catalog_directur.id_catalog_company')
                  ->where('user_catalog_directur.id_user', Auth::user()->id)
                  ->pluck('id')
                  ->toArray();

      $id_supervisi = Supervisi::whereIn('id_user_manager',$id_manager)
                  ->pluck('id')
                  ->toArray();

      $Dataorder = DB::table('order_catalog')
                  ->select('user_catalog.kode_instalasi', 'category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('category', 'order_catalog.id_category', '=', 'category.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  ->whereIn('id_user_supervisi', $id_supervisi)
                  ->orderBy('order_catalog.id','DESC')
                  ->orderBy('order_catalog.datetime', 'DESC');

                  if($filter == 'status'){
                    if($search != '5'){
                      $Dataorder->where('order_catalog.status', $search);
                    }
                  }else if($filter == 'po'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_catalog.no_po', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                    }
                  }else if($filter == 'usulan'){
                    $pencarian = explode('/',$search);
                    if(count($pencarian) > 1){
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                      ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                      ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                    }else{
                      $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                    }

                  }else if($filter == 'tanggal'){
                    $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                  }

                $order = $Dataorder->get();
      return view('catalog.users.directur.history', compact('order','company'));
    }

    public function detail_history(Request $request) {
      $id_order = $request->id_order;
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

                  $dataOrder = DB::table('order_catalog_history')
                              ->select('order_catalog_history.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov', 'user_catalog.username_catalog', 'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_manager.name AS name_manager','user_catalog_directur.name AS name_directur')
                              ->join('item', 'order_catalog_history.id_item', '=', 'item.id')
                              ->join('users', 'order_catalog_history.id_executor', '=', 'users.id')
                              ->leftJoin('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                              ->leftJoin('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                              ->leftJoin('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                              ->leftJoin('user_catalog_directur', 'users.id', '=', 'user_catalog_directur.id_user')
                              ->where('order_catalog_history.id_order', $id_order)
                              ->orderBy('order_catalog_history.datetime_history','DESC');

                  $dataItem = Order::where('id', $id_order)->first();
                  $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
                  $user_catalog = $user_catalog_data->first();
                  $count = count($user_catalog_data->get());
                  if($count == 0){
                    $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
                  }
                  $data = $dataOrder->get();
                  return response()->json(array(
                                           'id_order' => $id_order,
                                           'data' => $data,
                                           'user_catalog' => $user_catalog,
                                           'dataItem' => $dataItem,
                                           'status' => 1));
    }

    public function downloadProposer($id_proposer){

    $data = DB::table('order_proposer')->select('user_catalog_supervisi.id AS id_spv', 'user_catalog_supervisi.name AS name_supervisi','user_catalog_supervisi.nip AS nip_spv', 'order_proposer.*', 'order_catalog.id', 'user_catalog.kode_instalasi', 'user_catalog.nama_instalasi', 'user_catalog.username_catalog', 'user_catalog.nip')
          ->join('order_catalog', 'order_proposer.id', '=', 'order_catalog.id_proposer')
          ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
          ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog.id')
          ->where(DB::RAW('md5(CAST(order_proposer.id AS VARCHAR(50)))'), $id_proposer)
          ->first();

    $dataOrder = DB::select("
            select
                order_catalog.datetime,order_catalog_detail.*,
                item.price_shipment,item.code,item.name,item.merk,
                item.price_retail,item.price_gov,item.price_country
            from order_catalog_detail
            join
              item on order_catalog_detail.id_item=item.id
            join
              order_catalog on order_catalog_detail.id_order = order_catalog.id
            join
              order_proposer on order_catalog.id_proposer = order_proposer.id
            where
              md5(CAST(order_proposer.id AS VARCHAR(50))) = '".$id_proposer."'");

      $pdf = PDF::loadView('catalog.users.doc.usulan', compact('data','dataOrder'));
      return $pdf->stream();
    }

    public function downloadPo($id){
      $data = Order::where(DB::raw('md5(cast(id as varchar(50)))'), $id)->first();
      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $directur = DB::table('user_catalog_directur')
            ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
            ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
            ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
            ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
            ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
            ->first();

      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(cast(order_catalog.id as varchar(50)))'), $id)
            ->get();

      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();
      $contract = DB::table('contract')->select('contract.*')->join('item', 'contract.id', '=', 'item.id_contract')->where('contract.id_vendor_detail', $data->id_vendor_detail)->first();
      ############cara dapetin nomor sesui dengan kategori utama
      $menentukanKate = OrderDetail::where('id_order', $data->id)->pluck('id_item')->toArray();
      $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
      $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
      $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
      $mainCat = Category::whereIn('id', $subCategory)->value('name');


      return view('catalog.users.doc.po', compact('mainCat', 'contract','data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
    }

    function reset(){


        return view('catalog.users.directur.v_reset');
    }


    function action_ganti_password(Request $request){



        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","
            Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

      }


        public function ppk_review(Request $request) {
          $company = $this->companies();
          $filter = ($request->filter != null) ? $request->filter : 'xxx';
          $search = ($request->search != null) ? $request->search : 'xxx';

          $id_supervisi = DB::table('user_catalog_directur')->select('user_catalog_supervisi.id')->join('user_catalog_supervisi', 'user_catalog_directur.id', '=', 'user_catalog_supervisi.id_user_manager')->pluck('id')->toArray();

          $Dataorder = DB::table('order_catalog')
                      ->select('category.name AS name_category', 'order_catalog.faktur', 'order_proposer.no_prop','order_catalog.*','user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email', 'user_catalog_supervisi.mobile_phone', 'user_catalog_manager.name AS name_manager', 'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager')
                      ->join('category', 'order_catalog.id_category', '=', 'category.id')
                      ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
                      ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                      ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
                      ->join('user_catalog_manager', 'user_catalog_supervisi.id_user_manager', '=', 'user_catalog_manager.id')
                      ->whereIn('id_user_supervisi', $id_supervisi)
                      ->where('order_catalog.status', '<>', '0')
                      ->orderBy('order_catalog.datetime', 'DESC');

          $bapbIDS = Bapb::orderBy('id_order', 'DESC')->pluck('id_order', 'id')->toArray();
          $bapbStatus = Bapb::pluck('status', 'id')->toArray();
          $bapbID = Bapb::pluck('id_order')->toArray();
          $bastIDS = Bast::orderBy('id_order', 'DESC')->pluck('id_order', 'id')->toArray();
          $bastStatus = Bast::pluck('status', 'id')->toArray();
          $bastID = Bast::pluck('id_order')->toArray();

                      if($filter == 'po'){
                        $pencarian = explode('/',$search);
                        if(count($pencarian) > 1){
                          $Dataorder->where('order_catalog.no_po', $pencarian[0])
                          ->where(DB::raw('month(order_catalog.datetime)'), $pencarian[2])
                          ->where(DB::raw('year(order_catalog.datetime)'), $pencarian[3]);
                        }else{
                          $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                        }
                      }else if($filter == 'usulan'){
                        $pencarian = explode('/',$search);
                        if(count($pencarian) > 1){
                          $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                          ->where(DB::raw('month(order_proposer.datetime)'), $pencarian[2])
                          ->where(DB::raw('year(order_proposer.datetime)'), $pencarian[3]);
                        }else{
                          $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                        }

                      }else if($filter == 'tanggal'){
                        $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                      }

                      $order = $Dataorder->get();


          $bap = Bap::where('status', '1')->pluck('id_bast', 'id')->toArray();
          // dd($bap);
          $bapOrder = Bap::where('status', '1')->pluck('id_order','id')->toArray();
          $bapOrders = Bap::where('status', '1')->pluck('id','id_bast')->toArray();

          return view('catalog.users.directur.ppk_review', compact('bapOrders', 'bapOrder', 'bap', 'bastIDS', 'bastStatus', 'bastID' ,'order','company', 'bapbStatus', 'bapbID', 'bapbDtl', 'bapbIDS'));
        }


      public function bapbPreview(Request $request, $id) {
        $dataBapb = Bapb::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();
        $id_order = $dataBapb->id_order;
        $id_receive = $dataBapb->id_receive_item;
        $dataReceive = ReceiveItem::where('id', $id_receive)->first();
        $id_do = $dataReceive->id_do;
        $receive_item = $dataReceive;

        $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where('receive_item.id_order', $id_order)->first();
        $DataPo = Order::where('id', $id_order)->first();

        $directur = DB::table('order_catalog_detail')
                    ->select('order_catalog_detail.*', 'user_catalog_directur.name')
                    ->join('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                    ->where('id_order', $id_order)
                    ->first();

        $delivery = DeliveryOrder::where('id', $id_do)->first();
        $order = Order::where('id', $id_order)->first();
        $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $order->id_user_buyer)->first();

        $destination = "Rumah Sakit Jantung Harapan Kita";
        $telp = "(021) 5684093";
        $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";

        $VendorDetail = VendorDetail::where('id', $DataPo->id_vendor_detail)->first();
        $contract = Contract::where('id_vendor_detail', $VendorDetail->id)->first();
        $list_propinsi = Fungsi::propinsi();
        $list_kota = Fungsi::IndonesiaProvince();
        $bulanWord = Fungsi::MonthIndonesia();
        $bulanRoman = Fungsi::MonthRoman();

        $deliveryDtl = 	DB::table('receive_item_detail')
                        ->select('receive_item_reject.qty AS qty_reject', 'receive_item.id', 'receive_item.id_do', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim', 'item.satuan', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'receive_item_detail.description', 'item.price_gov')
                        ->join('item', 'receive_item_detail.id_item', '=', 'item.id')
                        ->join('receive_item', 'receive_item_detail.id_receive_item', '=', 'receive_item.id')
                        ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                        ->join('bapb', 'receive_item.id', '=', 'bapb.id_receive_item')
                        ->join('order_catalog_detail', function($join){
                          $join->on('order_catalog_detail.id_order', '=', 'receive_item.id_order');
                          $join->on('order_catalog_detail.id_order', '=', 'bapb.id_order');
                          $join->on('item.id', '=', 'order_catalog_detail.id_item');
                        })
                        ->leftJoin('receive_item_reject', function($join){
                          $join->on('receive_item.id_do', '=', 'receive_item_reject.id_do');
                          $join->on('receive_item_detail.id_item', '=', 'receive_item_reject.id_item');
                        })
                        ->where('bapb.id',  $dataBapb->id)
                        ->where('receive_item.id_order',  $id_order)
                        ->where('receive_item.id_do', $id_do)
                        ->where('receive_item.id', $dataReceive->id)
                        ->get();

        $item = BapbDetail::where('id_bapb',  $dataBapb->id)
                        ->pluck('id_item')
                        ->toArray();


        $driver = $delivery->driver;
        $car = $delivery->car;
        $car_no = $delivery->car_no;
        $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();
        $bapb = Bapb::where('id_order', $id_order)->where('id_receive_item', $id_receive)->first();

        $categoryItem = CategoryItem::whereIn('id_item', $item)->first();
        $mainCategorySubSub = Category::where('id', $categoryItem->id_category)->first();
        $mainCategorySub = Category::where('id', $mainCategorySubSub->id_parent)->first();
        $mainCategory = Category::where('id', $mainCategorySub->id_parent)->first();

        return view('catalog.users.directur.bapb_preview',compact('userCode', 'mainCategory', 'directur', 'pphp', 'contract', 'bulanRoman', 'bapb', 'order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
      }

      public function bastPreview(Request $request, $id) {
        $dataBast = Bast::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();
        $id_order = $dataBast->id_order;
        $id_receive = $dataBast->id_receive_item;
        // $dataReceive = ReceiveItem::whereIn('ids', $id_receive)->get();
        // dd($dataBast);
        // $id_do = $dataReceive->id_do;
        // $receive_item = $dataReceive;


        $receiveList = DB::table('users')->select('users.username')->join('receive_item', 'receive_item.id_user', '=', 'users.id')->where('receive_item.id_order', $id_order)->first();
        $DataPo = Order::where('id', $id_order)->first();

        $directur = DB::table('order_catalog_detail')
                    ->select('order_catalog_detail.*', 'user_catalog_directur.name')
                    ->join('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                    ->where('id_order', $id_order)
                    ->first();

        // $delivery = DeliveryOrder::where('id', $id_do)->first();
        $order = Order::where('id', $id_order)->first();
        $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $order->id_user_buyer)->first();

        $destination = "Rumah Sakit Jantung Harapan Kita";
        $telp = "(021) 5684093";
        $address = "Jl. Letjen S. Parman No.Kav.87, RT.1/RW.8, Kota Bambu Utara, Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11420";

        $VendorDetail = VendorDetail::where('id', $DataPo->id_vendor_detail)->first();
        $contract = Contract::where('id_vendor_detail', $VendorDetail->id)->first();
        $list_propinsi = Fungsi::propinsi();
        $list_kota = Fungsi::IndonesiaProvince();
        $bulanWord = Fungsi::MonthIndonesia();
        $bulanRoman = Fungsi::MonthRoman();

        $deliveryDtl = 	DB::table('bast')
                        ->select('bast_detail.qty AS qty_terima', 'unit_type.unit_name', 'bast_detail.satuan', 'item.satuan', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'bast_detail.description', 'item.price_gov')
                        ->join('bast_detail', 'bast.id', '=', 'bast_detail.id_bast')
                        ->join('item', 'bast_detail.id_item', '=', 'item.id')
                        ->join('unit_type', 'bast_detail.satuan', '=', 'unit_type.id')
                        ->join('order_catalog_detail', function($join){
                          $join->on('order_catalog_detail.id_order', '=', 'bast.id_order');
                          $join->on('item.id', '=', 'order_catalog_detail.id_item');
                        })
                        ->where('bast.id',  $dataBast->id)
                        ->where('bast.id_order',  $id_order)
                        ->orderBy('item.id', 'DESC')
                        ->get();

        $item = BastDetail::where('id_bast',  $dataBast->id)
                        ->pluck('id_item')
                        ->toArray();

        // $driver = $delivery->driver;
        // $car = $delivery->car;
        // $car_no = $delivery->car_no;
        $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();
        $bast = Bast::where('id_order', $id_order)->where('id_receive_item', $id_receive)->first();

        $categoryItem = CategoryItem::whereIn('id_item', $item)->first();
        $mainCategorySubSub = Category::where('id', $categoryItem->id_category)->first();
        $mainCategorySub = Category::where('id', $mainCategorySubSub->id_parent)->first();
        $mainCategory = Category::where('id', $mainCategorySub->id_parent)->first();
        $adendum = Adendum::where('id_contract', $contract->id)->get();
        return view('catalog.users.directur.bast_preview',compact('adendum', 'userCode', 'mainCategory', 'directur', 'pphp', 'contract', 'bulanRoman', 'bast', 'order','receiveList','driver','bulanWord','telp','receive_item','list_propinsi', 'list_kota', 'DataPo', 'VendorDetail', 'delivery', 'deliveryDtl','driver', 'car', 'car_no', 'destination' ,'address'))->with('id', $id);
      }


      public function detail_bast(Request $request) {
        $id_bast = $request->id_bast;
        $id_order = $request->id_order;
        $dataOrder = DB::table('bast_detail')
              ->select('bast.id_order', 'unit_type.unit_name', 'bast_detail.*','item.price_shipment', 'item.stock',  'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
              ->join('item', 'bast_detail.id_item', '=', 'item.id')
              ->join('unit_type', 'item.satuan', '=', 'unit_type.id')
              ->join('bast', 'bast.id', '=', 'bast_detail.id_bast')
              ->where('bast_detail.id_bast', $id_bast);

        $dataItem = Order::where('id', $id_order)->first();
        $user_catalog = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer)->first();
        $data = $dataOrder->get();
        return response()->json(array(
                                 'id_order' => $id_order,
                                 'data' => $data,
                                 'user_catalog' => $user_catalog,
                                 'dataItem' => $dataItem,
                                 'status' => 1));
      }

      public function bapbChecked(Request $request, $id) {
        $id_ppk = DB::table('user_catalog_directur')->select('user_catalog_directur.id')->where('id_user', Auth::user()->id)->value('id');
        DB::beginTransaction();
        try {
          $dataBapb = Bapb::where(DB::raw('md5(id)'), $id)->first();
          $categoryItem = CategoryItem::whereIn('id_item', $request->id_item)->first();
          $mainCategory = Category::where('id', $categoryItem->id_category)->first();

          $pphp = DB::table('master_gudang')
                    ->select('master_gudang.*', 'category_warehouse_type.*', 'user_catalog_pphp.id AS id_pphp')
                    ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                    ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                    ->where('id_user', Auth::user()->id)
                    ->first();

          $bast = Bast::create([
            'id_order' => $dataBapb->id_order,
            'id_receive_item' => $dataBapb->id_receive_item,
            'datetime' => date("Y-m-d H:i:s"),
            'status' => '1',
            'id_pphp' => $dataBapb->id_pphp,
            'approved_by_id_ppk' => $id_ppk
          ]);

          foreach($request->id_item as $k => $v){
            $receiveDtl = BastDetail::create([
              'id_bast' => $bast->id,
              'id_item' => $v,
              'qty' => $request->qty[$k],
              'satuan' => $request->satuan[$k],
              'status' => 1,
              'description' =>  '-'
            ]);
          }

          $bapb = Bapb::where('id', $dataBapb->id)->update(['status' => '2', 'approved_by_id_ppk' => $id_ppk]);

        } catch (\Illuminate\Database\QueryException $e) {
          DB::rollback();
          return redirect('catalog/users/directur/ppk_review')->with('message','BAPB gagal diperiksa');

        }

        DB::commit();
        return redirect('catalog/users/directur/ppk_review')->with('message','BAPB telah sukses diperiksa');
      }


      public function createInvoice($id, $id_bast){
        $data = DB::table('order_catalog')->select('order_catalog.*', 'category.name AS name_category')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where(DB::raw('md5(cast(order_catalog.id AS varchar(50)))'), $id)->first();

        $dataItem = DB::table('bast')
                    ->select('item.price_shipment','bast_detail.qty AS qty', 'unit_type.unit_name', 'bast_detail.satuan', 'item.satuan', 'order_catalog_detail.qty AS qty_po', 'item.merk', 'item.code', 'item.name', 'bast_detail.description', 'item.price_gov')
                    ->join('bast_detail', 'bast.id', '=', 'bast_detail.id_bast')
                    ->join('item', 'bast_detail.id_item', '=', 'item.id')
                    ->join('unit_type', 'bast_detail.satuan', '=', 'unit_type.id')
                    ->join('order_catalog_detail', function($join){
                      $join->on('order_catalog_detail.id_order', '=', 'bast.id_order');
                      $join->on('item.id', '=', 'order_catalog_detail.id_item');
                    })
                    ->where(DB::raw('md5(cast(bast.id AS varchar(50)))'), $id_bast)
                    ->where(DB::raw('md5(cast(bast.id_order AS varchar(50)))'), $id)
                    ->orderBy('item.id', 'DESC')
                    ->get();


        $vendor = VendorDetail::where('id', $data->id_vendor_detail)->first();
        $list_propinsi = Fungsi::propinsi();
        $list_kota = Fungsi::IndonesiaProvince();
        return view('catalog.users.doc.invoice', compact('dataItem', 'vendor', 'list_propinsi' ,'list_kota', 'data'));
      }


      public function createBAP($id, $id_bast){

        $data = DB::table('order_catalog')->select('order_catalog.*', 'category.name AS name_category')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where(DB::raw('md5(cast(order_catalog.id AS varchar(50)))'), $id)
                    ->first();

        $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $data->id_user_buyer)->first();

        $vendor = VendorDetail::where('id', $data->id_vendor_detail)->first();
        $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
        $adendum = Adendum::where('id_contract', $contract->id)->get();
        $Databast = DB::table('bast')
                    ->select('bast.*', 'delivery_order.datetime AS datetime_kirim', 'category.name AS name_category')
                    ->join('receive_item', 'bast.id_receive_item', '=', DB::raw('cast(receive_item.id AS varchar(50))'))
                    // ->join('receive_item', 'bast.id_receive_item', '=', CAST('receive_item.id' AS varchar(50)))
                    ->join('delivery_order', 'receive_item.id_do', '=', 'delivery_order.id')
                    ->join('order_catalog', 'order_catalog.id', '=', 'bast.id_order')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where('order_catalog.id_vendor_detail', $data->id_vendor_detail)
                    ->where('order_catalog.id', $data->id)
                    ->where(DB::raw('md5(cast(bast.id AS varchar(50)))'), $id_bast);

        $bast  = $Databast->get();

        $order = Order::where('id', $data->id)->first();
        $bastDetail = DB::table('bast_detail')->select('bast_detail.id', 'bast_detail.id_bast','bast_detail.id_item', 'bast_detail.qty', 'bast_detail.satuan', 'item.price_gov')
                    ->join('item', 'bast_detail.id_item', '=', 'item.id');
        $CodeBast =  $bastDetail->pluck('id_bast', 'id')->toArray();
        $ItemBast =  $bastDetail->pluck('id_item', 'id')->toArray();
        $QtyBast =  $bastDetail->pluck('qty', 'id')->toArray();
        $PriceBast =  $bastDetail->pluck('price_gov', 'id')->toArray();
        return view('catalog.users.directur.bap_create', compact('id', 'ItemBast','CodeBast', 'QtyBast', 'PriceBast', 'adendum', 'userCode', 'order', 'bastID', 'bast', 'vendor', 'list_propinsi' ,'list_kota', 'data', 'contract'));
      }

      public function storeBAP(Request $request, $id) {
        $bapData = Bap::whereYear("datetime",  "=", date('Y'))->get();
        $bap = count($bapData);
        $id_ppk = DB::table('user_catalog_directur')->select('id')->where('id_user', Auth::user()->id)->value('id');
        if($bap == 0){
          $no_bap = 1;
          $no_bap_with_o = "00".$no_bap;
        }else{
          $no_bap = $bap+1;
          if($no_bap <=9 ){
            $no_bap_with_o = "00".$no_bap;
          }else if($no_bap <=99 ){
            $no_bap_with_o = "0".$no_bap;
          }else{
            $no_bap_with_o = $no_bap;
          }
        }
        DB::beginTransaction();
        try {
          $bap = Bap::create(
            [
              'procurement' => $request->procurement,
              'id_contract' => $request->id_contract,
              'id_order' => $request->id_order,
              'id_bast' => $request->id_bast,
              'id_vendor_detail' => $request->id_vendor_detail,
              'id_ppk' => $id_ppk,
              'no_bap' => $no_bap_with_o,
              'total' => $request->total,
              'item_not_send' => $request->item_not_send,
              'must_pay' => $request->must_pay,
              'datetime' => date('Y-m-d H:i:s'),
              'status' => '1'
            ]
          );

          $bast = Bast::where('id', $request->id_bast)->update(['status' => '2']);

        } catch (\Illuminate\Database\QueryException $e) {
          DB::rollback();
          return redirect('catalog/users/directur/ppk_review')->with('message', 'BAP gagal dibuat');
        }
        DB::commit();
        return redirect('catalog/users/directur/ppk_review')->with('message', 'BAP berhasil dibuat');
      }


      public function bapPreview(Request $request, $id) {
        $Bap = DB::table('bap')->select('category.name AS name_category', 'bap.*', 'order_catalog.datetime AS datetime_order', 'order_catalog.no_medik', 'order_catalog.no_non_medik', 'vendor_detail.vendor_name', 'contract.no_contract', 'order_catalog.total')
                    ->join('order_catalog', 'bap.id_order', '=', 'order_catalog.id')
                    ->join('contract', 'bap.id_contract', '=', 'contract.id')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->join('vendor_detail', 'order_catalog.id_vendor_detail', '=', 'vendor_detail.id')
                    ->where(DB::raw('md5(cast(bap.id AS varchar(50)))'), $id)
                    ->first();

        $data = DB::table('order_catalog')->select('order_catalog.*', 'category.name AS name_category')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where('order_catalog.id', $Bap->id_order)
                    ->first();

        $vendor = VendorDetail::where('id', $data->id_vendor_detail)->first();
        $contract = Contract::where('id', $Bap->id_contract)->first();
        $adendum = Adendum::where('id_contract', $Bap->id_contract)->get();

        $Databast = DB::table('bast')
                    ->select('bast.*', 'delivery_order.datetime AS datetime_kirim', 'category.name AS name_category')
                    // ->join('receive_item', 'bast.id_receive_item', '=', 'receive_item.id')
                    ->join('receive_item', 'bast.id_receive_item', '=', DB::raw('CAST(receive_item.id AS varchar(50))'))
                    ->join('delivery_order', 'receive_item.id_do', '=', 'delivery_order.id')
                    ->join('order_catalog', 'order_catalog.id', '=', 'bast.id_order')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where('order_catalog.id_vendor_detail', $data->id_vendor_detail)
                    ->where('order_catalog.id', $data->id)
                    ->where('bast.id', $Bap->id_bast);

        $bast  = $Databast->get();
        $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $data->id_user_buyer)->first();
        $directur = DB::table('user_catalog_directur')->select('name', 'nik')->where('id_user', Auth::user()->id)->first();
        $pphp = DB::table('role_user')->select('users.username')->join('users', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', 11)->get();

        return view('catalog.users.directur.bap_preview', compact('pphp', 'directur', 'Bap', 'userCode', 'bast', 'data', 'vendor', 'contract', 'adendum'));
      }


      public function createBAST($id) {
        $order = Order::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();

        $bapb = DB::table('bapb')
                    ->select('unit_type.convert_pcs', 'item.price_gov', 'unit_type.id AS id_satuan','unit_type.unit_name', 'item.code', 'item.name', 'item.merk', DB::raw("SUM(bapb_detail.qty) as qty"), 'bapb_detail.id_item', 'bapb_detail.satuan', 'bapb.id_order')
                    ->join('bapb_detail', 'bapb.id', '=', 'bapb_detail.id_bapb')
                    ->join('item', 'bapb_detail.id_item', '=', 'item.id')
                    ->join('unit_type', function($join){
                      $join->on('bapb_detail.satuan', '=', 'unit_type.id');
                      $join->on('item.satuan', '=', 'unit_type.id');
                    })
                    ->where(DB::raw('md5(cast(id_order AS varchar(50)))'), $id)
                    ->where('bapb.status', 1)
                    ->groupBy('unit_type.convert_pcs', 'item.price_gov', 'unit_type.id','unit_type.unit_name', 'item.code', 'item.name', 'item.merk', 'bapb_detail.id_item', 'bapb_detail.satuan', 'bapb.id_order')
                    ->distinct()
                    ->get();


        // $dataSatuanUnit = DB::table('delivery_order_detail')
        //             ->select('delivery_order_detail.qty', 'delivery_order_detail.id_item','unit_type.id AS id_satuan', 'unit_type.unit_name', 'delivery_order_detail.description')
        //             ->join('delivery_order', 'delivery_order_detail.id_do', '=', 'delivery_order.id')
        //             ->join('unit_type', 'delivery_order_detail.satuan', '=', 'unit_type.id')
        //             ->where('delivery_order.id', $request->id_spj);
        //
        // $dataSend = $dataSatuanUnit->pluck('qty', 'id_item')->toArray();
        // $dataSatuan = $dataSatuanUnit->pluck('id_satuan', 'id_item')->toArray();
        // $dataSatuanName = $dataSatuanUnit->pluck('unit_name', 'id_item')->toArray();
        $allSatuan = UnitType::where('status', 1)->get();


        $contract = Contract::where('id_vendor_detail', $order->id_vendor_detail)->first();
        $adendum = Adendum::where('id_contract', $contract->id)->get();
        $vendor = VendorDetail::where('id', $order->id_vendor_detail)->first();
        $userCode = DB::table('user_catalog')->select('kode_instalasi', 'nama_instalasi')->where('id_user', $order->id_user_buyer)->first();

        $data = DB::table('order_catalog')->select('order_catalog.*', 'category.name AS name_category')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->where('order_catalog.id', $order->id)
                    ->first();

        return view('catalog.users.directur.bast_create', compact('allSatuan', 'userCode', 'adendum', 'vendor', 'id', 'bapb', 'contract', 'data'));
      }

      function storeBAST(Request $request, $id) {
        $id_ppk = DB::table('user_catalog_directur')->select('user_catalog_directur.id')->where('id_user', Auth::user()->id)->value('id');
        $order = Order::where(DB::raw('md5(cast(id AS varchar(50)))'), $id)->first();
        $bapbData = DB::table('bapb')
                    ->select('bapb.id','bapb.id_pphp','bapb.id_receive_item', 'bapb_detail.id_item')
                    ->join('bapb_detail', 'bapb.id', '=', 'bapb_detail.id_bapb')
                    ->where(DB::raw('md5(cast(id_order AS varchar(50)))'), $id)
                    ->whereIn('id_item', $request->id_item);

        $bapb =     $bapbData->pluck('id_item', 'id_receive_item')
                    ->toArray();

        $IDbapb =     $bapbData->pluck('id')
                    ->toArray();

        $bapbPphp = $bapbData->pluck('id_pphp', 'id_receive_item')
                    ->toArray();
        $IDPphp = $bapbData->pluck('id_pphp')
                    ->toArray();
        $id_userPPhp = Pphp::whereIn('id', $IDPphp)->pluck('id_user')->toArray();

        $pphp = DB::table('master_gudang')
                  ->select('master_gudang.*', 'category_warehouse_type.*')
                  ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                  ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                  ->whereIn('id_user', $id_userPPhp)
                  ->first();

        $userWarehouse = DB::table('users')->select('users.id', 'users.id_gudang')
                  ->join('role_user', 'users.id', 'role_user.user_id')
                  ->where('role_user.role_id', 9)
                  ->where('id_gudang', $pphp->id_gudang)
                  ->first();

          $receive = "";
          $pphpId = "";
          $insert = "";
          $update = "";
          foreach($bapb as $k => $v){
            if (in_array($v, $request->id_item)) {
              if($receive) $receive .= ",";
              $receive .= $k;
              if($pphpId) $pphpId .= ",";
              $pphpId .= $bapbPphp[$k];
            }
          }

          $masterData = MasterDataWarehouse::where('id_gudang', $pphp->id_gudang)->whereIn('id_item', $request->id_item)->count();
          $id_gudang = $pphp->id_gudang;

          $BastData = Bast::whereYear("datetime",  "=", date('Y'))->get();
          $bast = count($BastData);

          if($bast == 0){
            $no_bast = 1;
            $no_bast_with_o = "00".$no_bast;
          }else{
            $no_bast = $bast+1;
            if($no_bast <=9 ){
              $no_bast_with_o = "00".$no_bast;
            }else if($no_bast <=99 ){
              $no_bast_with_o = "0".$no_bast;
            }else{
              $no_bast_with_o = $no_bast;
            }
          }

        DB::beginTransaction();
        try {
          $categoryItem = CategoryItem::whereIn('id_item', $request->id_item)->first();
          $mainCategory = Category::where('id', $categoryItem->id_category)->first();

          $pphp = DB::table('master_gudang')
                    ->select('master_gudang.*', 'category_warehouse_type.*', 'user_catalog_pphp.id AS id_pphp')
                    ->join('category_warehouse_type', 'master_gudang.id', '=', 'category_warehouse_type.id_gudang')
                    ->join('user_catalog_pphp', 'master_gudang.id_jenis_gudang', '=', 'user_catalog_pphp.id_master_jenis_gudang')
                    ->where('id_user', Auth::user()->id)
                    ->first();

          $bast = Bast::create([
            'id_order' => $order->id,
            'no_bast' => $no_bast_with_o,
            'datetime' => date("Y-m-d H:i:s"),
            'status' => '1',
            'approved_by_id_ppk' => $id_ppk
          ]);

          $insertPPhp = Bast::find($bast->id)->update(['id_pphp' => $pphpId, 'id_receive_item' => $receive]);

          foreach($request->id_item as $k => $v){
            $receiveDtl = BastDetail::create([
              'id_bast' => $bast->id,
              'id_item' => $v,
              'qty' => $request->hasil[$k],
              'satuan' => $request->satuanDO[$k],
              'status' => 1,
              'description' =>  '-'
            ]);

            if($masterData > 0){
              #UPDATE KE MASTER DATA GUDANG
              if($update) $update .= ";";
              $update .= "UPDATE master_data_gudang SET qty =qty+".$request->hasil[$k].",last_date_in='".date('Y-m-d H:i:s')."', id_user='".$userWarehouse->id."' WHERE id_gudang=".$id_gudang." and id_item=".$v;
            }else{
              if($insert) $insert .= ",";
              $insert .= "('".$id_gudang."','".$v."','".$request->hasil[$k]."','".$request->satuanDO[$k]."','".date('Y-m-d H:i:s')."','0','".$userWarehouse->id."')";
            }

          }



          $msterDataWarehouse = NEW MasterDataWarehouse;
          if($update != ""){
            $dataWarehouse = $msterDataWarehouse->updateData($update);
          }
          if($insert != ""){
            $dataWarehouse = $msterDataWarehouse->inputData($insert);
          }

          $bapb = Bapb::whereIn('id', $IDbapb)->update(['status' => '2', 'approved_by_id_ppk' => $id_ppk]);

        } catch (\Illuminate\Database\QueryException $e) {
          DB::rollback();
          dd($e->getMessage());
          // return redirect('catalog/users/directur/ppk_review')->with('message','BAST gagal dibuat');

        }

        DB::commit();
        return redirect('catalog/users/directur/ppk_review')->with('message','BAST telah sukses dibuat');
      }





}
