<?php
namespace App\Http\Controllers\Catalog\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cart;
use App\Model\Item;
use App\Model\Manager;
use App\Model\Supervisi;
use App\Model\Finance;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderHistory;
use App\Model\Supplier;
use App\Model\VendorDetail;
use App\Model\Chat;
use App\Model\OrderProposer;
use App\Model\VendorDirectur;
use App\Model\Budget;
use App\Model\BudgetUsed;
use App\Model\ItemReject;
use App\Model\UnitType;
use App\Model\Contract;
use App\Model\VendorAdministrators;
use App\Model\UserRequest;
use App\Model\UserRequestDetail;
use App\Model\CategoryItem;
use App\Model\Category;
use App\Model\CategoryWarehouseType;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;
use Fungsi;
use DB;
use App\User;
use URL;
use Mail;
use \PDF;
use SimpleXMLElement;
// use ConsoleTVs\Charts\Facades\Charts;
use Charts;

class DivisiCatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware('divisiauth');
    }

    public function index(Request $request){

      error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));



        if (Auth::check()) {
          $id_supervisi = DB::table('user_catalog')->select('user_catalog.id_user_catalog_supervisi')->where('user_catalog.id_user', Auth::user()->id)->value('id_user_catalog_supervisi');
          $budget = Budget::where('status', '1')->where('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
          $budgetJml = $budget->get();
          $budgetTotal = $budget->first();
          if($budget->count() == 0){
            $budgetUsed = 0;
          }else{
            $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
          }

          if(($budgetUsed != null)||($budgetUsed == 0)){
            $budgetYear = ($budgetTotal->budget)-$budgetUsed;
          }else{
            $budgetYear = 0;
          }

            // $data = DB::table('users')
            //         ->select('user_catalog_company.company','user_catalog_company.address AS address_company', 'user_catalog_company.telephone AS telephone_company', 'user_catalog_company.fax AS fax_company', 'user_catalog_company.email AS email_company',
            //             'user_catalog_manager.name AS name_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager','user_catalog_manager.email AS email_manager','user_catalog_manager.status AS status_manager',
            //
            //             'user_catalog.id_user_catalog_company', 'user_catalog.username_catalog','user_catalog.address AS address_catalog', 'user_catalog.telephone AS telephone_catalog','user_catalog.email AS email_catalog',
            //             'user_catalog_finance.name AS name_finance', 'user_catalog_finance.mobile_phone AS mobile_phone_finance','user_catalog_finance.email AS email_finance','user_catalog_finance.status AS status_finance',
            //             'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_supervisi.mobile_phone AS mobile_phone_supervisi','user_catalog_supervisi.email AS email_supervisi','user_catalog_supervisi.status AS status_supervisi'
            //             )
            //         ->join('user_catalog_company', 'users.id_user_catalog_company','=','user_catalog_company.id')
            //         ->join('user_catalog_manager', 'user_catalog_company.id','=', 'user_catalog_manager.id_user_catalog_company')
            //         ->join('user_catalog_finance', 'user_catalog_company.id', '=', 'user_catalog_finance.id_user_manager')
            //         ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
            //         ->join('user_catalog', function($join){
            //             $join->on('user_catalog_supervisi.id', '=', 'user_catalog.id_user_catalog_supervisi');
            //             $join->on('users.id','=' ,'user_catalog.id_user');
            //             $join->on('user_catalog_company.id','=' ,'user_catalog.id_user_catalog_company');
            //         })
            //         ->where('users.status','1')
            //         ->where('users.id',Auth::user()->id);
            //
            //         $data_company = $data->get();
            //         $company = $data->first();

                $filterByCategory = ($request->category != null) ? $request->category : 'xxx';
                $search = ($request->search != null) ? $request->search : 'xxx';
                $data = DB::table('item')
                        ->select('item.*', 'category_item.id_item', 'category_item.id_category', 'vendor_detail.vendor_name')
                        ->join('category_item', 'item.id', '=', 'category_item.id_item')
                        ->join('supplier_item', 'item.id', '=', 'supplier_item.id_item')
                        ->join('vendor_detail', 'supplier_item.id_supplier', '=', 'vendor_detail.id')
                        ->join('vendor_status', 'vendor_detail.id', '=', 'vendor_status.id_vendor_detail')
                        // ->join('contract', 'item.id_contract', '=', 'contract.id')
                        // ->join('adendum', function($join){
                        //     $join->on('contract.id', '=', 'adendum.id_contract');
                        //     $join->on('item.id_adendum', '=', 'adendum.id');
                        // })
                        ->where('vendor_status.last_status', 6)
                        // ->where('contract.status', 1)
                        // ->whereBetween('contract.start_date', [$from, $to])
                        ->distinct();

                            // ->where('item.status','Y');
                    // Item::where('status', 'Y');
                    if($filterByCategory != 'xxx') {
                      $data->where('id_category', $filterByCategory);
                    }

                    if($search != 'xxx'){
                      $data->orwhere('code', 'like', '%' . $search . '%')->orWhere('name', 'like', '%' . $search . '%')->orWhere('merk', 'like', '%' . $search . '%');
                    }
                    $item = $data->paginate(10);

            $cate = $request->category;
            return view('catalog.users.item.index', compact('filterByCategory','data_company','company', 'item','search', 'budgetUsed', 'budgetYear','cate'));
        }else{
            return redirect('auth');
        }
    }

    public function addcart(Request $request) {
       $id_user = Auth::user()->id;
       $super =  DB::table('users')
                 ->select('user_catalog.username_catalog', 'user_catalog.telephone', 'user_catalog.email', 'user_catalog.id_user_catalog_supervisi')
                 ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                 ->where('users.id', Auth::user()->id)
                 ->first();

       $id_catalog_company = Auth::user()->id_user_catalog_company;
       $id_manager = 1;
       $id_finance = Finance::where('id_user_manager',$id_manager)->value('id');
       $id_item = $request->id;
       $dataCart = Cart::where('id_user', $id_user)->where('status','1');
       $qty = $dataCart->where('id_item', $id_item)->value('qty');
       $items = Item::where('id', $id_item);
       $item_name = $items->value('name');
       $id_contract = $items->value('id_contract');
       $id_adendum = $items->value('id_adendum');
       $supplier = Supplier::where('id_item', $id_item)->first();

       $cart = Cart::where('status','1')->where('id_user',$id_user)->where('id_item',$id_item)->updateorCreate(
           [
             'id_item' => $id_item,
             'id_user' => $id_user,
             'id_user_manager' => $id_manager,
             'id_user_supervisi' => $super->id_user_catalog_supervisi,
             'id_user_finance' => $id_finance,
             'id_vendor_detail' => $supplier->id_supplier,
             'datetime' => date('Y-m-d 00:00:00'),
             'id_contract' => $id_contract,
             'id_adendum' => $id_adendum
           ],
           ['qty' => $qty+1,
           'status' => '1']);


       $Cart = DB::table('cart')
             ->where('id_user',$id_user)
             ->where('cart.status',1)
             ->groupBy('cart.id_item')
             ->select('cart.id_item')
             ->get();


       $totCart = count($Cart);
       return response()->json(array(
                                'item_name' => $item_name,
                                'cart' => $totCart,
                                'status' => 1));
   }

    public function cart(Request $request) {
        $vendor = VendorDetail::pluck('id','vendor_name')->toArray();
        $id_user = Auth::user()->id;
        $dataCart = DB::table('cart')
                    ->select('vendor_detail.vendor_name', 'item.satuan', 'unit_type.unit_name', 'cart.*','item.price_shipment' ,'item.id AS id_item' ,'item.code','item.name','item.merk','item.price_country','item.price_retail','item.price_gov','item.price_date','item.release_date','item.expired_date','item.stock','item.production_origin','item.description','item.status')
                    // ->select('item.satuan', 'unit_type.unit_name', 'cart.*','item.price_shipment' ,'item.id AS id_item' ,'item.code','item.name','item.merk','item.price_country','item.price_retail','item.price_gov','item.price_date','item.release_date','item.expired_date','item.stock','item.production_origin','item.description','item.status', 'image_item.file')
                    ->join('vendor_detail', 'cart.id_vendor_detail', '=', 'vendor_detail.id')
                    ->join('item', 'cart.id_item', '=', 'item.id')
                    ->join('contract', function($join){
                        $join->on('item.id_contract', '=', 'contract.id');
                        $join->on('cart.id_contract', '=', 'contract.id');
                      })
                    ->join('unit_type', 'unit_type.id', '=', 'item.satuan')
                    ->leftjoin('image_item', 'item.id','=','image_item.id_item')
                    ->where('cart.id_user', $id_user)
                    ->where('cart.status','1')
                    // ->groupBy('item.id')
                    // ->groupBy('image_item.id_item')
                    ->orderBy('cart.id_contract', 'DESC')
                    ->distinct()
                    ->get();


        // $id_supervisi = Cart::where('cart.id_user', $id_user)->value('id_user_supervisi');
        $id_supervisi = DB::table('user_catalog')->select('id_user_catalog_supervisi')->where('user_catalog.id_user', $id_user)->value('id_user_catalog_supervisi');
        $supervisi = Supervisi::where('id', $id_supervisi)->first();

        $budget = Budget::where('status', '1')->where('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC');
        $budgetJml = $budget->get();
        $budgetTotal = $budget->first();
        if($budget->count() == 0){
          $budgetUsed = 0;
          $budgetYear = 0;
        }else{
          $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
          if(($budgetUsed != null)||($budgetUsed == 0)){
            $budgetYear = ($budgetTotal->budget)-$budgetUsed;
          }else{
            $budgetYear = 0;
          }
        }
        $pembeli = DB::table('users')
                  ->select('user_catalog.username_catalog', 'user_catalog.telephone', 'user_catalog.email')
                  ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->first();
        return view('catalog.users.cart.index', compact('dataCart', 'pembeli', 'supervisi', 'vendor', 'budgetUsed','budgetYear'));
    }

    public function delete_cart(Request $request) {
      $cart = Cart::find($request->id_cart);
      $id_item = $cart->value('id_item');
      $item_name = Item::where('id', $id_item)->value('name');
      $cart_delete = $cart->delete();

      if($cart) $status = 1;
      else $status =0;
      return response()->json(array(
                               'id_cart' => $request->id_cart,
                               'item' => $item_name,
                               'status' => $status));
    }

    public function order(Request $request) {


      //trigger postgres
         $posts = DB::select("SELECT lpad(CAST(Count(datetime)+1 AS VARCHAR(11)), 3, '00') from order_proposer
                   where
                   -- EXTRACT(month FROM datetime)=EXTRACT(month FROM CURRENT_DATE)
                   -- and
                   EXTRACT(year FROM datetime)=EXTRACT(year FROM CURRENT_DATE)
                   ");
         $no_prop = $posts[0]->lpad;


       $id_item = $request->id_item;
       $qty = $request->qty;
       $subtot = $request->subtot;
       $id_vendor = $request->id_vendor_detail;
       $item = "";
       $id = "";
       $id_order = 1;
       $status_detail = 1;
       $id_user = Auth::user()->id;
       $datetime = date("Y-m-d H:i:s");
       $status = '0';
       $vendor = "";
       $id_supervisi = DB::table('user_catalog')->select('id_user_catalog_supervisi')->where('id_user', $id_user)->value('id');
       // $budgetTotal = Budget::where('status', '1')->where('id_supervisi', $id_supervisi)->orderBy('periode', 'DESC')->first();
       // $budgetUsed =  BudgetUsed::where('id_budget', $budgetTotal->id)->sum('budget_used');
       // if(($budgetUsed != null)||($budgetUsed == 0)){
       //   $budgetYear = ($budgetTotal->budget)-$budgetUsed;
       // }else{
       //   $budgetYear = 0;
       // }



         $unit = DB::table('user_catalog')
                     ->select('*')
                     ->where('id_user','=',Auth::user()->id)
                     ->first();
         $unit_intalasi = $unit->nama_instalasi;

         $id_sup = $unit->id_user_catalog_supervisi;
         $kepala_bagian = DB::table('user_catalog_supervisi')
                     ->select('*')
                     ->where('id','=',$id_sup)
                     ->first();

         $no_tlpn =  $kepala_bagian->mobile_phone;
      //atasan

      // $userkeyanda = "hfvns6";
      // $passkeyanda = "3q8wehsntc";
      // $nohptujuan  = "089698930980";

      // $a = "https://reguler.zenziva.net/apps/smsapi.php?";
      //
      // $data = $a.'userkey='.$userkeyanda.'&passkey='.$passkeyanda.'&nohp='.$nohptujuan.'&pesan=Mohon Untuk melakukan Approve';
      // return redirect($data);

      // $userkey = "hfvns6"; //userkey lihat di zenziva
      // $passkey = "3q8wehsntc"; // set passkey di zenziva
      // $telepon = "089698930980";
      // $nama = "tes";
      // $message = "Unit $nama telah melakukan pengusulan pada no Usulan . Silakan Anda Untuk melakukan Konfirmasi";
      // $url = "https://reguler.zenziva.net/apps/smsapi.php";
      // $curlHandle = curl_init();
      // curl_setopt($curlHandle, CURLOPT_URL, $url);
      // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
      // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
      // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
      // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
      // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
      // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
      // curl_setopt($curlHandle, CURLOPT_POST, 1);
      // $results = curl_exec($curlHandle);
      // curl_close($curlHandle);
      //
      // $XMLdata = new SimpleXMLElement($results);
      // $status = $XMLdata->message[0]->text;
      // echo $status;


      //vendor

      // $userkey = "hfvns6"; //userkey lihat di zenziva
      // $passkey = "3q8wehsntc"; // set passkey di zenziva
      // $telepon = "089698930980";
      // $nama = "tes";
      // $message = "Barang Anda telah dipesan dan sedang menunggu persetujuan Kepala Bagian.";
      // $url = "https://reguler.zenziva.net/apps/smsapi.php";
      // $curlHandle = curl_init();
      // curl_setopt($curlHandle, CURLOPT_URL, $url);
      // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
      // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
      // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
      // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
      // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
      // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
      // curl_setopt($curlHandle, CURLOPT_POST, 1);
      // $results = curl_exec($curlHandle);
      // curl_close($curlHandle);

      // $XMLdata = new SimpleXMLElement($results);
      // $status = $XMLdata->message[0]->text;



      $ppk = Supervisi::where('id', $id_supervisi)->value('id_user_manager');
      $id_golongan = DB::table('user_catalog_directur')->select('id_golongan')->where('id', $ppk)->value('id_golongan');

      // if($request->totalAll <= $budgetYear){
      $id_buyer = DB::table('users')
                  ->select('user_catalog.id AS id_buyer')
                  ->join('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->where('users.id', $id_user)
                  ->value('id');

        if($id_golongan==3){
          //triiger medis & non medis;
            $mn = DB::select("SELECT lpad(CAST(Count(datetime)+1 AS VARCHAR(11)), 3, '00') from order_catalog
                    where
                    EXTRACT(year FROM datetime)=EXTRACT(year FROM CURRENT_DATE)
                    and id_golongan = '3'");
            $nonmedis = $mn[0]->lpad;
            $medis = " ";
        }elseif($id_golongan==1){
          //triiger medis & non medis;
            $mn = DB::select("SELECT lpad(CAST(Count(datetime)+1 AS VARCHAR(11)), 3, '00') from order_catalog
                           where
                          --EXTRACT(month FROM datetime)=EXTRACT(month FROM CURRENT_DATE)
                          -- and
                          EXTRACT(year FROM datetime)=EXTRACT(year FROM CURRENT_DATE)
                          and id_golongan = '1'");
            $medis = $mn[0]->lpad;
            $nonmedis = " ";
        }

      DB::beginTransaction();
      try {

        $pengusul1 = array(
                    'datetime'                  =>  $datetime,
                    'no_prop'                   =>  $no_prop,
                    'proposer'                  =>  $id_user,
                    'created_at'                =>  date('Y-m-d H:i:s'),
                    'updated_at'                =>  date('Y-m-d H:i:s'),
                    );

       $pengusul_id = DB::table('order_proposer')->insertGetId($pengusul1);

       $no = 1;
       $no1 = 1;
       foreach($id_item as $k => $v){

        $idCategoryItem = CategoryItem::whereIn('id_item', $v)->pluck('id_category')->toArray();
        $category = Category::whereIn('id', $idCategoryItem)->pluck('id_parent')->toArray();
        $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
        $mainCat = Category::whereIn('id', $subCategory)->value('id');

        $mds = (int)$medis;
        $dataMds = $mds+$no;
        $nonmds = (int)$nonmedis;
        $dataNonMds = $nonmds+$no1;
        if ($id_golongan ==1) {
          if ($dataMds < 10) {
            $nomorMedis = "00".$dataMds;
            $nomorNonMedis = "";
          }else if ($dataMds < 100){
            $nomorMedis = "0".$dataMds;
            $nomorNonMedis = "";
          }else{
            $nomorMedis = $dataMds;
            $nomorNonMedis = "";
          }
        }else{
          if ($dataNonMds < 10) {
            $nomorNonMedis = "00".$dataNonMds;
            $nomorMedis = "";
          }else if ($dataNonMds < 100){
            $nomorNonMedis = "0".$dataNonMds;
            $nomorMedis = "";
          }else{
            $nomorNonMedis = $dataNonMds;
            $nomorMedis = "";
          }
        }
        $no ++;
        $no1 ++;


         $order = Order::create([
           'id_proposer' => $pengusul_id,
           'no_po'       =>  $no_prop,
           'no_order'    => rand(0,100000000),
           'id_user_buyer' => $id_user,
           'id_user_supervisi' => $id_supervisi,
           'id_vendor_detail' => $id_vendor[$k],
           'id_golongan' => $id_golongan,
           'no_medik'     => $nomorMedis,
           'no_non_medik' => $nomorNonMedis,
           'status' => $status,
           'datetime' => $datetime,
           'total' => $subtot[$k],
           'id_category' => $mainCat
         ]);

         foreach($v as $kItem => $vItem){
           if($item) $item .= ",";
           $item .= "('".$order->id."','".$vItem."','".$qty[$k][$vItem]."','".$status_detail."','".$datetime."')";
           #Insert History
           if($id) $id .= ",";
           $id .= "('".$order->id."','".$vItem."','".$qty[$k][$vItem]."','".$id_buyer."','".$id_user."','1','".$datetime."','NEW','".$datetime."')";
         }

         // $budgetUsed = BudgetUsed::create([
         //   'id_budget' => $budgetTotal->id,
         //   'budget_used' => $subtot[$k],
         //   'datetime' => $datetime,
         //   'used_by' => Auth::user()->id,
         //   'id_proposer' => $pengusul->id,
         //   'id_order' => $order->id,
         // ]);

       }

        $order_detail = New OrderDetail;

        $save = $order_detail->inputOrder($item);
        if($save)
          $deleteCart = Cart::where('id_user', $id_user)->update(['status' => '0']);
          if($deleteCart)
          $history = NEW OrderHistory;
            $insertUpdateOldQty = $history->OldQty($id);
            $proposer = OrderProposer::where('id',$order->id_proposer)->first();

            // Mail::send('catalog.users.email.email_atasan', ['nama' => $unit_intalasi, 'pesan' => 'telah melakukan pengusulan pada no Usulan '.$proposer->no_prop.' Silakan Anda Untuk melakukan Konfirmasi'], function ($message) use ($request)
            // {
            //     $message->subject('tes');
            //     $message->from('donotreply@kiddy.com', 'Kiddy');
            //     $message->to('ajie.darmawan106@gmail.com');
            // });

            // $userkey = "hfvns6"; //userkey lihat di zenziva
            // $passkey = "3q8wehsntc"; // set passkey di zenziva
            // $telepon = "085649184363";//$no_tlpn; // ke supervisi nya
            // // $telepon = "0811898259";//$no_tlpn; // ke supervisi nya
            // $nama = $unit_intalasi;
            // $message = "$nama telah melakukan pengusulan pada no Usulan $proposer->no_prop Silakan Anda Untuk melakukan Konfirmasi";
            // $url = "https://reguler.zenziva.net/apps/smsapi.php";
            // $curlHandle = curl_init();
            // curl_setopt($curlHandle, CURLOPT_URL, $url);
            // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
            // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            // curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
            // curl_setopt($curlHandle, CURLOPT_POST, 1);
            // $results = curl_exec($curlHandle);
            // curl_close($curlHandle);
            //
            // $XMLdata = new SimpleXMLElement($results);
            // $status = $XMLdata->message[0]->text;

            #whatsapp
            // $nama = "tes";
            // $my_apikey = "TPED26H4E4B72RTX8AU9";
            // $number = "+629698930980";
            // $type = "TYPE OF MESSAGE: IN or OUT";
            // $markaspulled = "1 or 0";
            // $getnotpulledonly = "1 or 0";
            // $api_url  = "http://panel.apiwha.com/get_messages.php";
            // $api_url .= "?apikey=". urlencode ($my_apikey);
            // $api_url .=	"&number=". urlencode ($number);
            // $api_url .= "&type=". urlencode ($type);
            // $api_url .= "&markaspulled=". urlencode ($markaspulled);
            // $api_url .= "&getnotpulledonly=". urlencode ($getnotpulledonly);
            // $my_json_result = file_get_contents($api_url, false);
            // $my_php_arr = json_decode($my_json_result);
            // foreach($my_php_arr as $item)
            // {
            //   $from_temp = $item->from;
            //   $to_temp = $item->to;
            //   $text_temp = $item->text;
            //   $type_temp = $item->type;
            // }
            //
            // // Send Message
            // $url_default = "http://e-catalog.nusa-mart.id/auth";
            // $my_apikey = "TPED26H4E4B72RTX8AU9";
            // $destination = "+628563263022";
            // $message = "$nama telah melakukan pengusulan pada no Usulan $proposer->no_prop Silakan ".$url_default." untuk melakukan Konfirmasi";
            // $api_url = "http://panel.apiwha.com/send_message.php";
            // $api_url .= "?apikey=". urlencode ($my_apikey);
            // $api_url .= "&number=". urlencode ($destination);
            // $api_url .= "&text=". urlencode ($message);
            // $my_result_object = json_decode(file_get_contents($api_url, false));


      } catch (\Exception $e) {
        DB::rollback();
        return response (['status' => false,'errors' => $e->getMessage()]);
      }
        DB::commit();


          return redirect()->back()->with('message', 'Pesanan telah diteruskan ke Supervisi');
    }

    public function order_approved(Request $request) {



       $filter = ($request->filter != null) ? $request->filter : 'xxx';
       $search = ($request->search != null) ? $request->search : 'xxx';
       $id_supervisi = Order::where('id_user_buyer', Auth::user()->id)->value('id_user_supervisi');
       $Dataorder = DB::table('order_catalog')

                   // ->select('category.name AS name_category', 'order_catalog.*', 'user_catalog.kode_instalasi', 'user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog_supervisi.name')
                   // ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   // ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   // ->leftJoin('user_catalog', function($join){
                   //   $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                   //   $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   // })
                   // ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   // // ->where('id_user', Auth::user()->id)
                   // ->where('id_user_supervisi', $id_supervisi)
                   // ->where('order_catalog.status','<>', '0')
                   // ->orderBy('order_catalog.datetime','DESC')
                   // ->orderBy('order_catalog.no_order','DESC');

                  ->select('category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer','user_catalog_supervisi.name')
                  ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                  ->join('category', 'order_catalog.id_category', '=', 'category.id')
                  ->leftJoin('user_catalog', function($join){
                    $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                    $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                  })
                  ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                  // ->where('id_user', Auth::user()->id)
                  ->where('id_user_supervisi', $id_supervisi)
                  ->where('order_catalog.status','<>', '0')
                  ->orderBy('order_catalog.datetime','DESC')
                  ->orderBy('order_catalog.no_order','DESC');


                   if($filter == 'status'){
                     if($search != '5'){
                       $Dataorder->where('order_catalog.status', $search);
                     }
                   }else if($filter == 'po'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_catalog.no_po', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                     }
                   }else if($filter == 'usulan'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                     }

                   }else if($filter == 'tanggal'){
                     $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                   }


                   $order = $Dataorder->get();
       return view('catalog.users.order.approvedorder', compact('order','company'));
     }

    public function detail_order(Request $request) {
      $id_order = $request->id_order;
      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog_detail.*', 'item.code', 'item.price_shipment', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->where('order_catalog_detail.id_order', $id_order);
      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }
      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

    public function track(Request $request) {
     $filter = ($request->filter != null) ? $request->filter : 'xxx';
     $search = ($request->search != null) ? $request->search : 'xxx';
     $super = DB::table('order_catalog')
             ->select('user_catalog_supervisi.id_user')
             ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog_supervisi.id')
             ->where('id_user_buyer', Auth::user()->id)
             ->pluck('id_user')
             ->toArray();

     $Dataorder = DB::table('order_catalog')
             ->select('category.name AS name_category', 'user_catalog.kode_instalasi', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
             ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
             ->join('category', 'order_catalog.id_category', '=', 'category.id')
             ->leftJoin('user_catalog', function($join){
               $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
               $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
             })
             ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
             ->where('order_catalog.id_user_buyer', Auth::user()->id)
             ->orWhereIn('order_catalog.id_user_buyer', $super)
             ->orderBy('order_catalog.datetime', 'DESC')
             ->orderBy('order_catalog.no_po', 'DESC');

             // $Dataorder = DB::table('order_catalog')
             //          ->select('category.name AS name_category','order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
             //          ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
             //          ->join('category', 'order_catalog.id_category', '=', 'category.id')
             //          ->leftJoin('user_catalog', function($join){
             //            $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
             //            $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
             //          })
             //          ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
             //          ->where('order_catalog.id_user_buyer', Auth::user()->id)
             //          ->orWhereIn('order_catalog.id_user_buyer', $super)
             //          ->orderBy('order_catalog.datetime', 'DESC')
             //          ->orderBy('order_catalog.no_po', 'DESC');


     if($filter == 'status'){
       if($search != '5'){
         $Dataorder->where('order_catalog.status', $search);
       }
     }else if($filter == 'po'){
       $pencarian = explode('/',$search);
       if(count($pencarian) > 1){
         $Dataorder->where('order_catalog.no_po', $pencarian[0])
         ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
         ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
       }else{
         $Dataorder->where('order_catalog.no_po', $pencarian[0]);
       }
     }else if($filter == 'usulan'){
       $pencarian = explode('/',$search);
       if(count($pencarian) > 1){
         $Dataorder->where('order_proposer.no_prop', $pencarian[0])
         ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
         ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
       }else{
         $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
       }

     }else if($filter == 'tanggal'){
       $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

     }

     $requestUser = UserRequest::where('id_user', Auth::user()->id)->pluck('id_order')->toArray();


     $order = $Dataorder->paginate(10);
     return view('catalog.users.order.track', compact('order', 'requestUser'));
   }
   public function track_order(Request $request) {
      $super = Order::where('id_user_buyer', Auth::user()->id)->value('id_user_supervisi');
      $order = DB::table('order_catalog')
                ->select('receive_item.id_user', 'users.name','receive_item.datetime AS date_receive', 'delivery_order.datetime AS date_do', 'delivery_order.driver', 'delivery_order.car_no', 'delivery_order.car','order_catalog.id_user_buyer AS super', 'user_catalog_supervisi.id_user AS users','order_catalog.status AS status_pesan',
                'order_catalog_detail.*',
                          'order_proposer.id AS id_proposer','order_proposer.no_prop', 'order_proposer.proposer',
                          'user_catalog.telephone AS mobile_user','user_catalog.email as email_user',
                          'user_catalog.username_catalog','user_catalog_supervisi.name', 'user_catalog_supervisi.email',
                          'user_catalog_supervisi.mobile_phone','user_catalog_manager.name AS name_manager',
                          'user_catalog_manager.email AS email_manager', 'user_catalog_manager.mobile_phone AS mobile_phone_manager',
                          'user_catalog_directur.name AS name_directur',
                          'user_catalog_finance.name AS name_finance', 'user_catalog_finance.email AS email_finance',
                          'user_catalog_finance.mobile_phone AS mobile_phone_finance')
                ->join('order_catalog_detail', 'order_catalog.id', '=', 'order_catalog_detail.id_order')
                ->join('users', 'order_catalog.id_user_buyer', '=', 'users.id')
                ->leftJoin('user_catalog_supervisi', function($join){
                  $join->orOn('order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user');
                  $join->orOn('order_catalog_detail.id_user_supervisi_approve', '=', 'user_catalog_supervisi.id');
                  $join->orOn('users.id', '=', 'user_catalog_supervisi.id_user');
                })
                ->leftJoin('user_catalog_manager', 'order_catalog_detail.id_user_manager_approve', '=', 'user_catalog_manager.id')
                ->leftJoin('user_catalog_directur', 'order_catalog_detail.id_user_directur_approve', '=', 'user_catalog_directur.id')
                ->leftJoin('user_catalog_finance', 'order_catalog_detail.id_user_finance_approve', '=', 'user_catalog_finance.id')
                ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                ->leftJoin('user_catalog', function($join){
                  $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                  $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                })
                ->leftJoin('delivery_order', 'order_catalog.id', '=', 'delivery_order.id_order')
                ->leftJoin('receive_item', function($join){
                  $join->on('order_catalog.id', '=', 'receive_item.id_order');
                  $join->orOn('users.id', '=', 'receive_item.id_user');
                  })

                  ->where('order_catalog.id', $request->id_order)
                  ->where('order_catalog.id_user_supervisi', $super)
                  ->get();

                  $gudang = DB::table('receive_item')->select('receive_item.id_order','item.merk', 'item.code', 'item.name', 'users.id', 'users.username','receive_item.datetime', 'unit_type.unit_name', 'receive_item_detail.satuan','receive_item_detail.id_qty AS qty_kirim')
                            ->join('master_data_gudang', function($join){
                              $join->on('receive_item.id_gudang', '=', 'master_data_gudang.id_gudang');
                            })
                            ->join('receive_item_detail', function($join){
                              $join->on('receive_item.id', '=', 'receive_item_detail.id_receive_item');
                            })
                            ->join('unit_type', 'receive_item_detail.satuan', '=', 'unit_type.id')
                            ->join('item', function($join){
                              $join->on('item.id', '=', 'receive_item_detail.id_item');
                              $join->on('receive_item_detail.id_item', '=', 'master_data_gudang.id_item');
                              $join->on('master_data_gudang.id_item', '=', 'item.id');
                            })
                            ->join('users', 'master_data_gudang.id_user', '=', 'users.id')
                            ->where('receive_item.id_order', $request->id_order)
                            ->get();
                  $users = DB::table('receive_item')->select('users.name')->join('users', 'receive_item.id_user', '=', 'users.id')->first();
                  return response()->json(array(
                                           'gudang' => $gudang,
                                           'users' => $users,
                                           'id_order' => $request->id_order,
                                           'data' => $order,
                                           'status' => 1));
    }
    public function history(Request $request) {
       $filter = ($request->filter != null) ? $request->filter : 'xxx';
       $search = ($request->search != null) ? $request->search : 'xxx';
       $id_supervisi = Order::where('id_user_buyer', Auth::user()->id)->value('id_user_supervisi');
       $Dataorder = DB::table('order_catalog')

                   // ->select('user_catalog.kode_instalasi', 'category.name AS name_category', 'order_catalog.*','user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                   // ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                   // ->join('category', 'order_catalog.id_category', '=', 'category.id')
                   // ->leftJoin('user_catalog', function($join){
                   //   $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                   //   $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                   // })
                   // ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                   // ->where('id_user_supervisi', $id_supervisi)
                   // ->orderBy('order_catalog.datetime', 'DESC')
                   // ->orderBy('order_catalog.no_po', 'DESC');

                    ->select('category.name AS name_category', 'order_catalog.*', 'user_catalog.kode_instalasi', 'user_catalog.username_catalog', 'order_proposer.no_prop', 'order_proposer.proposer', 'user_catalog_supervisi.name')
                    ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
                    ->join('category', 'order_catalog.id_category', '=', 'category.id')
                    ->leftJoin('user_catalog', function($join){
                      $join->on('order_catalog.id_user_buyer', '=', 'user_catalog.id_user');
                      $join->on('order_proposer.proposer', '=', 'user_catalog.id_user');
                    })
                    ->leftJoin('user_catalog_supervisi', 'order_catalog.id_user_buyer', '=', 'user_catalog_supervisi.id_user')
                    ->where('id_user_supervisi', $id_supervisi)
                    ->orderBy('order_catalog.datetime', 'DESC')
                    ->orderBy('order_catalog.no_po', 'DESC');



                   if($filter == 'status'){
                     if($search != '5'){
                       $Dataorder->where('order_catalog.status', $search);
                     }
                   }else if($filter == 'po'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_catalog.no_po', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_catalog.no_po', $pencarian[0]);
                     }
                   }else if($filter == 'usulan'){
                     $pencarian = explode('/',$search);
                     if(count($pencarian) > 1){
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0])
                       ->where(DB::raw('md5(cast(EXTRACT(month FROM order_proposer.datetime)as varchar(50)))'), $pencarian[2])
                       ->where(DB::raw('md5(cast(EXTRACT(year FROM order_proposer.datetime)as varchar(50)))'), $pencarian[3]);
                     }else{
                       $Dataorder->where('order_proposer.no_prop', $pencarian[0]);
                     }

                   }else if($filter == 'tanggal'){
                     $Dataorder->where(DB::raw('date_format(order_catalog.datetime,"%Y-%m-%d")'), $search);

                   }

       // $order = $Dataorder->paginate(10);
       $order = $Dataorder->get();
       return view('catalog.users.history.index', compact('order'));
     }

    public function detail_history(Request $request) {
      $id_order = $request->id_order;
      $id_supervisi = DB::table('users')
                  ->select('user_catalog_supervisi.id AS id_supervisi')
                  ->join('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->where('users.id', Auth::user()->id)
                  ->value('id');

      $dataOrder = DB::table('order_catalog_history')
                  ->select('order_catalog_history.*', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov', 'user_catalog.username_catalog', 'user_catalog_supervisi.name AS name_supervisi', 'user_catalog_manager.name AS name_manager')
                  ->join('item', 'order_catalog_history.id_item', '=', 'item.id')
                  ->join('users', 'order_catalog_history.id_executor', '=', 'users.id')
                  ->leftJoin('user_catalog', 'users.id', '=', 'user_catalog.id_user')
                  ->leftJoin('user_catalog_supervisi', 'users.id', '=', 'user_catalog_supervisi.id_user')
                  ->leftJoin('user_catalog_manager', 'users.id', '=', 'user_catalog_manager.id_user')
                  // ->where('order_catalog_history.id_executor', $id_supervisi)
                  ->where('order_catalog_history.id_order', $id_order)
                  ->orderBy('order_catalog_history.datetime_history','DESC');


      $dataItem = Order::where('id', $id_order)->first();
      $user_catalog_data = DB::table('user_catalog')->select('user_catalog.*')->where('id_user', $dataItem->id_user_buyer);
      $user_catalog = $user_catalog_data->first();
      $count = count($user_catalog_data->get());
      if($count == 0){
        $user_catalog = DB::table('user_catalog_supervisi')->select('user_catalog_supervisi.name AS username_catalog', 'user_catalog_supervisi.mobile_phone AS telephone','user_catalog_supervisi.email')->where('id_user',$dataItem->id_user_buyer)->first();
      }      $data = $dataOrder->get();
      return response()->json(array(
                               'id_order' => $id_order,
                               'data' => $data,
                               'user_catalog' => $user_catalog,
                               'dataItem' => $dataItem,
                               'status' => 1));
    }

  public function chat(Request $request) {
    $name = VendorDetail::where('id', $request->id_vendor_detail)->value('vendor_name');
    $chat = DB::table('chat')
            ->select('chat.*')
            ->join('cart', 'chat.id_cart', '=', 'cart.id_contract')
            ->where('cart.status','1')
            ->where('chat.id_user', $request->id_user)
            ->where('chat.id_cart', $request->id_contract)
            ->where('chat.id_vendor_detail', $request->id_vendor_detail)
            ->orderBy('chat.datetime', 'ASC')
            ->distinct();

    $data = $chat->get();

    $item = DB::table('item')
            ->select('item.name', 'item.id', 'cart.id AS id_cart')
            ->join('cart', function($join){
              $join->on('item.id_contract', '=', 'cart.id_contract');
              $join->on('item.id', '=', 'cart.id_item');
            })
            ->where('cart.status','1')
            ->where('cart.id_user', $request->id_user)
            ->where('item.id_contract', $request->id_contract)
            ->where('cart.id_vendor_detail', $request->id_vendor_detail)
            ->distinct();
    $dataItem = $item->get();


    return response()->json(array(
      'name' => $name,
      'data' => $data,
      'dataItem' => $dataItem,
      'status' => 1));
  }

  public function Addchat(Request $request) {
    //id_cart ganti id_contract
    $cart = Chat::create([
      'id_cart' => $request->id_contract,
      'id_user' => $request->id_user,
      'id_vendor_detail' => $request->id_vendor_detail,
      'datetime' => date('Y-m-d H:i:s'),
      'message' => $request->message,
      'status' => 1
    ]);
    return response()->json(array(
      'status' => 1));
  }

  public function downloadProposer($id_proposer){
    // $data = OrderProposer::where(DB::raw('md5(id)'), $id_proposer)->first();
    // $dataOrder = DB::table('order_catalog_detail')
    //       ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
    //       ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
    //       ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
    //       ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
    //       ->where(DB::raw('md5(order_proposer.id)'), $id_proposer)
    //       ->get();
    // $data = DB::select("SELECT *
    // from order_proposer
    // where md5(CAST(id AS VARCHAR(50))) = '".$id_proposer."'");

    $data = DB::table('order_proposer')->select('user_catalog_supervisi.id AS id_spv', 'user_catalog_supervisi.name AS name_supervisi','user_catalog_supervisi.nip AS nip_spv', 'order_proposer.*', 'order_catalog.id', 'user_catalog.kode_instalasi', 'user_catalog.nama_instalasi', 'user_catalog.username_catalog', 'user_catalog.nip')
          ->join('order_catalog', 'order_proposer.id', '=', 'order_catalog.id_proposer')
          ->join('user_catalog', 'order_catalog.id_user_buyer', '=', 'user_catalog.id_user')
          ->join('user_catalog_supervisi', 'order_catalog.id_user_supervisi', '=', 'user_catalog.id')
          ->where(DB::RAW('md5(CAST(order_proposer.id AS VARCHAR(50)))'), $id_proposer)
          ->first();

    // $dataOrder = DB::table('order_catalog_detail')
    //       ->select('order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment','item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
    //       ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
    //       ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
    //       ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
    //       ->where(DB::raw('md5(order_proposer.id)'), $id_proposer)
    //       ->where('order_catalog.id_vendor_detail',$user)
    //       ->get();

    $dataOrder = DB::select("select order_catalog.datetime,order_catalog_detail.*,item.price_shipment,item.code,item.name,item.merk,
    item.price_retail,item.price_gov,item.price_country
    from order_catalog_detail
    join item on order_catalog_detail.id_item=item.id
    join order_catalog on order_catalog_detail.id_order = order_catalog.id
    join order_proposer on order_catalog.id_proposer = order_proposer.id
    where md5(CAST(order_proposer.id AS VARCHAR(50))) = '".$id_proposer."'");
    $pdf = PDF::loadView('catalog.users.doc.usulan', compact('data','dataOrder'));
    // return $pdf->download('usulan.pdf');
    return $pdf->stream();
  }

  public function downloadPo($id){

      $data = Order::where(DB::raw('md5(cast(id as varchar(50)))'), $id)
              // ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $bln)
              // ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $thn)
              ->first();



      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      // $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorAdministrators::where('id_vendor_detail', $data->id_vendor_detail)->first();

      $directur = DB::table('user_catalog_directur')
            ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
            ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
            ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
            ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
            ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
            ->first();

      $dataOrder = DB::table('order_catalog_detail')
            ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
            ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
            ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
            ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
            ->where(DB::raw('md5(cast(order_catalog.id as varchar(50)))'), $id)
            // ->where(DB::raw('md5(cast(EXTRACT(month FROM order_catalog.datetime)as varchar(50)))'), $bln)
            // ->where(DB::raw('md5(cast(EXTRACT(year FROM order_catalog.datetime)as varchar(50)))'), $thn)
            ->get();

      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();

      $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
      ############cara dapetin nomor sesui dengan kategori utama
      $menentukanKate = OrderDetail::where('id_order', $data->id)->pluck('id_item')->toArray();
      $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
      $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
      $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
      $mainCat = Category::whereIn('id', $subCategory)->value('name');


    // return view('catalog.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
    // $list_propinsi = Fungsi::propinsi();
    // $list_kota = Fungsi::IndonesiaProvince();

    // $pdf = PDF::loadView('catalog.users.doc.po', compact('data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur'));
    // return $pdf->download('usulan.pdf');
    // return $pdf->stream();

    return view('catalog.users.doc.po', compact('mainCat', 'contract', 'data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
  }


  function reset(){

   return view('catalog.users.item.v_reset');
}


function action_ganti_password(Request $request){



   if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
       // The passwords matches
       return redirect()->back()->with("error","
       Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
   }

   if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
       //Current password and new password are same
       return redirect()->back()->with("error","Kata Sandi Baru tidak bisa sama dengan kata sandi Anda saat ini. Silakan pilih kata sandi yang berbeda.");
   }

   $validatedData = $request->validate([
       'current_password' => 'required',
       'new_password' => 'required|string|min:6|confirmed',
   ]);

   //Change Password
   $user = Auth::user();
   $user->password = bcrypt($request->get('new_password'));
   $user->save();

   return redirect()->back()->with("success","Password changed successfully !");

   die;




    echo $lama1 = $request->lama1;
    echo "<br>";

    if (Hash::needsRehash($lama1)) {
       $lama1 = Hash::make('123456');
       echo "sama";
   }else{
       echo "beda";
   }
  die;

    // echo $lama = bcrypt($request->lama);
     echo $password = Hash::make($request->lama);

    $baru = $request->baru;

  die;
   if($lama==$baru){
       DB::table('users')->where('id',$request->id)->update(['password' => bcrypt($request->baru)]);
       return redirect()->route('ganti_password')->with('message','Password Berhasil Diganti');
   }else{
       return redirect()->route('ganti_password')->with('message','Password Anda Salah');
   }



 }

 public function request(Request $request, $id) {
   $data = Order::where(DB::raw('md5(id)'), $id)->first();

   $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();

   $vendorDirectur = VendorAdministrators::where('id_vendor_detail', $data->id_vendor_detail)->first();

// $vendorDirectur = VendorDirectur::where('id_vendor_detail', $data->id_vendor_detail)->first();
   $directur = DB::table('user_catalog_directur')
         ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
         ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
         ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
         ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
         ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
         ->first();

    $dataOrder = DB::table('order_catalog_detail')
         ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
         ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
         ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
         ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
         ->where(DB::raw('md5(order_catalog.id)'), $id)
         ->get();

    $list_propinsi = Fungsi::propinsi();
    $list_kota = Fungsi::IndonesiaProvince();

    $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
    $supervisi = Supervisi::where('id', $data->id_user_supervisi)->first();
    $satuan =  UnitType::where('status', 1)->get();

    ############cara dapetin nomor sesui dengan kategori utama
    $menentukanKate = OrderDetail::where(DB::raw('md5(id_order)'), $id)->pluck('id_item')->toArray();
    $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
    $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
    $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
    $mainCat = Category::whereIn('id', $subCategory)->value('name');

    $categoryWarehouse = DB::table('category_warehouse_type')->select('master_gudang.nama_instalasi','master_gudang.id')
          ->join('master_gudang', 'category_warehouse_type.id_gudang', '=', 'master_gudang.id')
          ->where('category_warehouse_type.id_category', $subCategory)
          ->first();

    $peminta = DB::table('user_catalog')->select('user_catalog.username_catalog', 'user_catalog.nip', 'user_catalog.nama_instalasi')->where('id_user', $data->id_user_buyer)->first();

    return view('catalog.users.request.index', compact('categoryWarehouse', 'mainCat', 'satuan', 'peminta', 'contract', 'supervisi', 'data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));
 }

 public function requestPost(Request $request, $id) {
   $data = Order::where(DB::raw('md5(order_catalog.id)'), $id)->first();
   $peminta = DB::table('user_catalog')->select('user_catalog.username_catalog', 'user_catalog.nip', 'user_catalog.nama_instalasi')->where('id_user', $data->id_user_buyer)->first();
   $requests = UserRequest::create(
     [
       'id_order' => $data->id,
       'datetime' => date('Y-m-d'),
       'request_type' => $request->request_type,
       'department_requestor' => $peminta->nama_instalasi,
       'id_user' => Auth::user()->id,
       'id_gudang' => $request->id_gudang
     ]);
     foreach($request->id_item as $key => $value) {
       $requestDetail = UserRequestDetail::create(
         [
           'id_user_request' => $requests->id,
           'id_item' => $value,
           'qty' => $request->qty[$key],
           'satuan' => $request->satuan[$key],
           'status' => '1'
         ]
       );
     }

     return redirect('catalog/users/track')->with('message', 'Sukses membuat Surat Permintaan');
 }

   public function requestPreview(Request $request, $id){
      $data = Order::where(DB::raw('md5(id)'), $id)->first();

      $userRequests = UserRequest::where(DB::raw('md5(id_order)'), $id)->first();

      $detail = DB::table('user_request')
          ->select('unit_type.id AS id_satuan', 'unit_type.unit_name', 'user_request.*', 'user_request_detail.id_item', 'user_request_detail.qty', 'user_request_detail.satuan', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
          ->join('user_request_detail', 'user_request.id', '=', 'user_request_detail.id_user_request')
          ->join('unit_type', 'user_request_detail.satuan', '=', 'unit_type.id')
          ->join('item', 'user_request_detail.id_item', '=', 'item.id')
          ->where(DB::raw('md5(user_request.id_order)'), $id);

      $detailUserRequest = $detail->pluck('qty', 'id_item')->toArray();
      $detailUserSatuan = $detail->pluck('unit_name', 'id_item')->toArray();
      $detailUserJenis = $detail->pluck('id_satuan')->toArray();

      $vendorDetail = VendorDetail::where('id', $data->id_vendor_detail)->first();
      $vendorDirectur = VendorAdministrators::where('id_vendor_detail', $data->id_vendor_detail)->first();

      $directur = DB::table('user_catalog_directur')
           ->select('user_catalog_directur.*', 'user_catalog_company.company','user_catalog_company.address','user_catalog_company.npwp')
           ->join('user_catalog_company','user_catalog_directur.id_catalog_company', '=', 'user_catalog_company.id')
           ->join('user_catalog_manager','user_catalog_company.id', '=', 'user_catalog_manager.id_user_catalog_company')
           ->join('user_catalog_supervisi', 'user_catalog_manager.id', '=', 'user_catalog_supervisi.id_user_manager')
           ->where('user_catalog_supervisi.id', $data->id_user_supervisi)
           ->first();

      $dataOrder = DB::table('order_catalog_detail')
           ->select('order_catalog.no_po','order_catalog.datetime', 'order_catalog_detail.*', 'item.price_shipment', 'item.code', 'item.name', 'item.merk', 'item.price_country', 'item.price_retail', 'item.price_gov')
           ->join('item', 'order_catalog_detail.id_item', '=', 'item.id')
           ->join('order_catalog', 'order_catalog_detail.id_order', '=', 'order_catalog.id')
           ->join('order_proposer', 'order_catalog.id_proposer', '=', 'order_proposer.id')
           ->where(DB::raw('md5(order_catalog.id)'), $id)
           ->get();

      ############cara dapetin nomor sesui dengan kategori utama
      $menentukanKate = OrderDetail::where(DB::raw('md5(id_order)'), $id)->pluck('id_item')->toArray();
      $categoryItem = CategoryItem::whereIn('id_item', $menentukanKate)->pluck('id_category')->toArray();
      $category = Category::whereIn('id', $categoryItem)->pluck('id_parent')->toArray();
      $subCategory = Category::whereIn('id', $category)->pluck('id_parent')->toArray();
      $mainCat = Category::whereIn('id', $subCategory)->value('name');

      $list_propinsi = Fungsi::propinsi();
      $list_kota = Fungsi::IndonesiaProvince();

      $contract = Contract::where('id_vendor_detail', $data->id_vendor_detail)->first();
      $supervisi = Supervisi::where('id', $data->id_user_supervisi)->first();
      $satuan =  UnitType::where('status', 1)->get();


      $categoryWarehouse = DB::table('category_warehouse_type')->select('master_gudang.nama_instalasi','master_gudang.id')
          ->join('master_gudang', 'category_warehouse_type.id_gudang', '=', 'master_gudang.id')
          ->where('category_warehouse_type.id_category', $subCategory)
          ->first();

      $peminta = DB::table('user_catalog')->select('user_catalog.username_catalog', 'user_catalog.nip', 'user_catalog.nama_instalasi')->where('id_user', $data->id_user_buyer)->first();

      return view('catalog.users.request.preview', compact('categoryWarehouse', 'mainCat', 'detailUserJenis', 'detailUserSatuan', 'userRequests', 'detailUserRequest', 'satuan', 'peminta', 'contract', 'supervisi', 'data','dataOrder', 'vendorDetail', 'vendorDirectur', 'directur', 'list_propinsi' ,'list_kota'));

   }

   public function wa(){
  }


  public function dashboard() {

    $users = Order::all();//User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))->get();
    $chart = Charts::database($users, 'bar', 'highcharts')
	      ->title("Pesanan dalam Sebulan")
	      ->elementLabel("Total Pesanan")
	      ->dimensions(1000, 500)
	      ->responsive(false)
	      ->groupByMonth(date('Y'), true);

    return view('catalog.users.dashboard.index',compact('chart'));
  }


}
