AmCharts.themes.black = {

	themeName: "black",

	AmChart: {
		color: "#e7e7e7", backgroundColor: "#222222"
	},

	AmCoordinateChart: {
		colors: ["#de4c4f", "#d8854f", "#eea638", "#a7a737", "#86a965", "#8aabb0", "#69c8ff", "#cfd27e", "#9d9888", "#916b8a", "#724887", "#7256bc"]
	},

	AmStockChart: {
		colors: ["#de4c4f", "#d8854f", "#eea638", "#a7a737", "#86a965", "#8aabb0", "#69c8ff", "#cfd27e", "#9d9888", "#916b8a", "#724887", "#7256bc"]
	},

	AmSlicedChart: {
		outlineAlpha: 1,
		outlineThickness: 2,
		labelTickColor: "#ffffff",
		labelTickAlpha: 0.3,
		colors: ["#de4c4f", "#d8854f", "#eea638", "#a7a737", "#86a965", "#8aabb0", "#69c8ff", "#cfd27e", "#9d9888", "#916b8a", "#724887", "#7256bc"]
	},

	AmRectangularChart: {
		zoomOutButtonColor: '#ffffff',
		zoomOutButtonRollOverAlpha: 0.15,
		zoomOutButtonImage: "lensWhite.png"
	},

	AxisBase: {
		axisColor: "#ffffff",
		axisAlpha: 0.3,
		gridAlpha: 0.1,
		gridColor: "#ffffff",
		dashLength: 3
	},

	ChartScrollbar: {
		backgroundColor: "#000000",
		backgroundAlpha: 0.2,
		graphFillAlpha: 0.2,
		graphLineAlpha: 0,
		graphFillColor: "#ffffff",
		selectedGraphFillColor: "#ffffff",
		selectedGraphFillAlpha: 0.4,
		selectedGraphLineColor: "#ffffff",
		selectedBackgroundColor: "#ffffff",
		selectedBackgroundAlpha: 0.09,
		gridAlpha: 0.15
	},

	ChartCursor: {
		cursorColor: "#ffffff",
		color: "#000000",
		cursorAlpha: 0.5
	},

	AmLegend: {
		color: "#e7e7e7"
	},

	AmGraph: {
		lineAlpha: 0.9
	},


	GaugeArrow: {
		color: "#ffffff",
		alpha: 0.8,
		nailAlpha: 0,
		innerRadius: "40%",
		nailRadius: 15,
		startWidth: 15,
		borderAlpha: 0.8,
		nailBorderAlpha: 0
	},

	GaugeAxis: {
		tickColor: "#ffffff",
		tickAlpha: 1,
		tickLength: 15,
		minorTickLength: 8,
		axisThickness: 3,
		axisColor: '#ffffff',
		axisAlpha: 1,
		bandAlpha: 0.8
	},

	TrendLine: {
		lineColor: "#c03246",
		lineAlpha: 0.8
	},

	// ammap
	AreasSettings: {
		alpha: 0.8,
		color: "#ffffff",
		colorSolid: "#000000",
		unlistedAreasAlpha: 0.4,
		unlistedAreasColor: "#ffffff",
		outlineColor: "#000000",
		outlineAlpha: 0.5,
		outlineThickness: 0.5,
		rollOverColor: "#3c5bdc",
		rollOverOutlineColor: "#000000",
		selectedOutlineColor: "#000000",
		selectedColor: "#f15135",
		unlistedAreasOutlineColor: "#000000",
		unlistedAreasOutlineAlpha: 0.5
	},

	LinesSettings: {
		color: "#ffffff",
		alpha: 0.8
	},

	ImagesSettings: {
		alpha: 0.8,
		labelColor: "#ffffff",
		color: "#ffffff",
		labelRollOverColor: "#3c5bdc"
	},

	ZoomControl: {
		buttonRollOverColor: "#3c5bdc",
		buttonFillColor: "#738f58",
		buttonBorderColor: "#738f58",
		buttonFillAlpha: 0.8,
		gridBackgroundColor: "#ffffff",
		buttonBorderAlpha:0,
		buttonCornerRadius:2,
		gridAlpha:0.5,
		gridBackgroundColor:"#ffffff",
		homeIconFile:"homeIconWhite.gif",
		buttonIconAlpha:0.6,
		gridAlpha: 0.2,
		buttonSize:20
	},

	SmallMap: {
		mapColor: "#ffffff",
		rectangleColor: "#ffffff",
		backgroundColor: "#000000",
		backgroundAlpha: 0.7,
		borderThickness: 1,
		borderAlpha: 0.8
	},

	// the defaults below are set using CSS syntax, you can use any existing css property
	// if you don't use Stock chart, you can delete lines below
	PeriodSelector: {
		color: "#e7e7e7"
	},

	PeriodButton: {
		color: "#e7e7e7",
		background: "transparent",
		opacity: 0.7,
		border: "1px solid rgba(255, 255, 255, .15)",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none",
		boxSizing: "border-box"
	},

	PeriodButtonSelected: {
		color: "#e7e7e7",
		backgroundColor: "rgba(255, 255, 255, 0.1)",
		border: "1px solid rgba(255, 255, 255, .3)",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none",
		opacity: 1,
		boxSizing: "border-box"
	},

	PeriodInputField: {
		color: "#e7e7e7",
		background: "transparent",
		border: "1px solid rgba(255, 255, 255, .15)",
		outline: "none"
	},

	DataSetSelector: {
		color: "#e7e7e7",
		selectedBackgroundColor: "rgba(255, 255, 255, .25)",
		rollOverBackgroundColor: "rgba(255, 255, 255, .15)"
	},

	DataSetCompareList: {
		color: "#e7e7e7",
		lineHeight: "100%",
		boxSizing: "initial",
		webkitBoxSizing: "initial",
		border: "1px solid rgba(255, 255, 255, .15)"
	},

	DataSetSelect: {
		border: "1px solid rgba(255, 255, 255, .15)",
		outline: "none"
	}

};