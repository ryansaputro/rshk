//###################################################DETAIL PERUSAHAAN############################################
var additional = [];
function Edit(a) {
  var menu = $(a).attr('data-id')
  //untuk kantor cabang
  if((menu == 'penyedia') || (menu == 7)){
    $.each($('.form_input_left'), function(k, v){
      if(k == 7){
          input =
                  '<div class="custom-control custom-radio">'+
                    '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked1" {{($vendor->branch_office == 1) ? 'checked' : ''}} onclick="branch" name="branch_office" value="1">'+
                    '<label class="custom-control-label" for="defaultUnchecked1">Ya</label>'+
                  '</div>'+
                  '<div class="custom-control custom-radio">'+
                    '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked" {{($vendor->branch_office == 0) ? 'checked' : ''}} onclick="branch" name="branch_office" value="0">'+
                    '<label class="custom-control-label" for="defaultUnchecked">Tidak</label>'+
                  '</div>';

          $('#form_input_left_'+k).html(input);
      }

    })
    //data sebelah kanan
    $.each($('.form_input'), function(k, v){
      if(($(v).attr('data-form') != 0) && ($(v).attr('data-form') !=1)){
        if (typeof data[k] === "undefined") {
            var nilai = $(v).attr('data-id');
        }else{
            var nilai = data[k];
        }
        input = "<input type='text' class='form-control input-lg form_data' value='"+nilai+"' style='height:24px !important; min-height:0px;'>";
        $('#'+$(v).attr('data-form')).html(input);
      }

    })
    var tombol = '<button type="button" class="btn btn-success btn-rounded" onclick="Update(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
    $('#tombol').html(tombol)

    branch();

    additional[0] = (typeof $('#addresscentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->address)) ? $branch_office_detail->address : ''; ?>" : $('#addresscentral').val();

    additional[1] =  (typeof $('#telephonecentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->telephone)) ? $branch_office_detail->telephone : ''; ?>" : $('#telephonecentral').val();
    additional[2] = (typeof $('#faxcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->fax)) ? $branch_office_detail->fax : ''; ?>" : $('#faxcentral').val();

    additional[3] = (typeof $('#hpcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->mobile_phone)) ? $branch_office_detail->mobile_phone : ''; ?>" : $('#hpcentral').val();
  }else if(menu == 'izin_usaha'){
    console.log("loading ...")
  }
}

var data = [];

function Update(a) {
  var radio = $('input[name=branch_office]:checked').val();
  if(radio == 1)$('#row_branch').css('display','none');

  var pkp = $($('.form_data')[0]).val();
  var email = $($('.form_data')[1]).val();
  var tlp = $($('.form_data')[2]).val();
  var fax = $($('.form_data')[3]).val();
  var hp = $($('.form_data')[4]).val();
  var web = $($('.form_data')[5]).val();
  var id_user = "{{Auth::user()->id}}";

  if(($('#form_input_left_7').text() != 'Tidak')){
    var addresscentral = $('#addresscentral').val();
    var telephonecentral = $('#telephonecentral').val();
    var faxcentral = $('#faxcentral').val();
    var hpcentral = $('#hpcentral').val();
    var branch_office = $('input[name=branch_office]:checked').val();
  }

  var url = "{{URL::to('vms/users/update')}}";
    $.ajax({
        type: "post",
        url: url,
      data: {
          "_token": "{{ csrf_token() }}",
          "id_user": id_user,
          "pkp": pkp,
          "email": email,
          "tlp": tlp,
          "fax": fax,
          "hp": hp,
          "web": web,
          "branch_office": branch_office,
          "addresscentral": addresscentral,
          "telephonecentral": telephonecentral,
          "faxcentral": faxcentral,
          "hpcentral": hpcentral,
          },
        success: function (a) {
          data[2] = a.pkp;
          data[3] = a.email;
          data[4] = a.telephone;
          data[5] = a.fax;
          data[6] = a.mobile_phone;
          data[7] = a.website;

          $.each($('.form_input'), function(k, v){
            if((k != 0) && (k != 1)){
              $('#'+k).html(data[k]);
              var tombol = '<button type="button" class="btn btn-info btn-rounded ButtonEdit" onclick="Edit(this)" data-id="'+k+'"><i class="fa fa-pencil"></i> Ubah</button>';
              $('#tombol').html(tombol);
              if(a.status == 1){
               var notif = '<div class="alert alert-success alert-dismissible">'+
                              '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                              '<strong>Sukses!</strong> Update Data Perusahaan!.'+
                            '</div>';
              }else{
               var notif = '<div class="alert alert-danger alert-dismissible">'+
                              '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                              '<strong>Gagal!</strong> Update Data Perusahaan!.'+
                            '</div>';
              }
                $('div.page-alerts').html(notif);

            }
          })
                var radiotext = (radio == 1) ? 'Ya' : 'Tidak';
                $('#form_input_left_7').html(radiotext);


        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(thrownError);
        }
   });
}

//#################################################PERUSAHAAN CABANG############################################
function branch(){
  $('.branch_office').on('change', function(){
      var data =
              '<h4>Kantor Pusat</h4><hr>'+
              '<div class="form-group{{ $errors->has('addresscentral') ? ' has-error' : '' }}">'+
                  '<label for="addresscentral" class="col-md-2 control-label">Alamat</label>'+
                  '<div class="col-md-12">'+
                    '<textarea name="addresscentral" class="form-control" id="addresscentral" required="">'+additional[0]+'</textarea>'+
                      '@if ($errors->has('addresscentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('addresscentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                 ' </div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('telephonecentral') ? ' has-error' : '' }}">'+
                  '<label for="telephonecentral" class="col-md-2 control-label">Telepon</label>'+
                  '<div class="col-md-12">'+
                      '<input id="telephonecentral" required="" type="text" class="form-control" name="telephonecentral" value="'+additional[1]+'" required autofocus>'+
                      '@if ($errors->has('telephonecentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('telephonecentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                  '</div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('faxcentral') ? ' has-error' : '' }}">'+
                  '<label for="faxcentral" class="col-md-2 control-label">Fax</label>'+
                  '<div class="col-md-12">'+
                      '<input id="faxcentral" type="text" required="" class="form-control" name="faxcentral" value="'+additional[2]+'" required autofocus>'+
                      '@if ($errors->has('faxcentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('faxcentral') }}</strong>'+
                          '</span>'+
                      '@endif'+
                  '</div>'+
              '</div>'+
              '<div class="form-group{{ $errors->has('hpcentral') ? ' has-error' : '' }}">'+
                  '<label for="hpcentral" class="col-md-2 control-label">Hp</label>'+
                  '<div class="col-md-12">'+
                      '<input id="hpcentral" type="text" required="" class="form-control" name="hpcentral" value="'+additional[3]+'" required autofocus>'+
                      '@if ($errors->has('hpcentral'))'+
                          '<span class="help-block">'+
                              '<strong>{{ $errors->first('hpcentral') }}</strong>'+
                         ' </span>'+
                      '@endif'+
                  '</div>'+
              '</div>';
      if($(this).val() == 1){
          $('#row_branch').css('display','block');
          $('#branch_office_additional').html(data);
      }else{
          $('#row_branch').css('display','none');
          $('#branch_office_additional').html('');
      }
  });
}
