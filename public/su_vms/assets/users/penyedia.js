var additional = [];
function Edit(a) {
  var menu = $(a).attr('data-id')
  //untuk kantor cabang
  if((menu == 'penyedia') || (menu == 7)){
    $.each($('.form_input_left'), function(k, v){
      if(k == 7){
          input =
                  '<div class="custom-control custom-radio">'+
                    '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked1" {{($vendor->branch_office == 1) ? 'checked' : ''}} onclick="branch" name="branch_office" value="1">'+
                    '<label class="custom-control-label" for="defaultUnchecked1">Ya</label>'+
                  '</div>'+
                  '<div class="custom-control custom-radio">'+
                    '<input type="radio" class="custom-control-input branch_office" id="defaultUnchecked" {{($vendor->branch_office == 0) ? 'checked' : ''}} onclick="branch" name="branch_office" value="0">'+
                    '<label class="custom-control-label" for="defaultUnchecked">Tidak</label>'+
                  '</div>';

          $('#form_input_left_'+k).html(input);
      }

    })
    //data sebelah kanan
    $.each($('.form_input'), function(k, v){
      if(($(v).attr('data-form') != 0) && ($(v).attr('data-form') !=1)){
        if (typeof data[k] === "undefined") {
            var nilai = $(v).attr('data-id');
        }else{
            var nilai = data[k];
        }
        input = "<input type='text' class='form-control input-lg form_data' value='"+nilai+"' style='height:24px !important; min-height:0px;'>";
        $('#'+$(v).attr('data-form')).html(input);
      }

    })
    var tombol = '<button type="button" class="btn btn-success btn-rounded" onclick="Update(this)" data-id="{{$id}}"><i class="fa fa-check"></i> Selesai</button>';
    $('#tombol').html(tombol)

    branch();

    additional[0] = (typeof $('#addresscentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->address)) ? $branch_office_detail->address : ''; ?>" : $('#addresscentral').val();

    additional[1] =  (typeof $('#telephonecentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->telephone)) ? $branch_office_detail->telephone : ''; ?>" : $('#telephonecentral').val();
    additional[2] = (typeof $('#faxcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->fax)) ? $branch_office_detail->fax : ''; ?>" : $('#faxcentral').val();

    additional[3] = (typeof $('#hpcentral').val() === "undefined") ? "<?php echo (isset($branch_office_detail->mobile_phone)) ? $branch_office_detail->mobile_phone : ''; ?>" : $('#hpcentral').val();
  }else if(menu == 'izin_usaha'){
    console.log("loading ...")
  }
}
